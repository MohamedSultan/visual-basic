Attribute VB_Name = "Types"
Enum SMS_Node_Name
    ams05 = 1
    ams06 = 2
    ams07 = 3
    ams08 = 4
    cm01 = 5
    eci01 = 6
    eci02 = 7
    lgp01 = 8
    rtr01 = 9
    rtr02 = 10
    rtr03 = 11
    rtr04 = 12
    rtr05 = 13
    rtr06 = 14
End Enum
Enum SMS_Node_Type
    Router = 1
    AMS = 2
    CM = 3
    ECI = 4
    LGP = 5
End Enum
Type SMS_RouterData_Type
    DF As String
    VmStat As String
    tp_status As String
    ss7_link As String
    m3ua_link As String
    mtpLinkRxUtilisation As String
    mtpLinkTxUtilisation As String
    smsCntMoTimeoutCounter As String
    smsCntMtTimeoutCounter As String
    smsCntSriSmTimeoutCounter As String
End Type
Type SMS_LGPData_Type
    DF As String
    VmStat As String
    tp_status As String
End Type
Type SMS_CMData_Type
    DF As String
    VmStat As String
    tp_status As String
    Poll As String
End Type
Type SMS_ECIData_Type
    DF As String
    VmStat As String
    tp_status As String
    pbcChainsRet As String
    diamTransAdminState As String
    diamTransState As String
End Type
Type SMS_AMSData_Type
    DF As String
    VmStat As String
    tp_status As String
    amsCntTotalStored As String
    QueueSizeExceeded As String
End Type
Enum SMS_Stats_Type
    DF_ = 1
    VmStat_ = 2
    tp_status_ = 3
    ss7_link_ = 4
    m3ua_link_ = 5
    mtpLinkRxUtilisation_ = 6
    mtpLinkTxUtilisation_ = 7
    smsCntMoTimeoutCounter_ = 8
    smsCntMtTimeoutCounter_ = 9
    smsCntSriSmTimeoutCounter_ = 10
    Poll_ = 11
    pbcChainsRet_ = 12
    diamTransAdminState_ = 13
    diamTransState_ = 14
    amsCntTotalStored_ = 15
    QueueSizeExceeded_ = 16
End Enum
