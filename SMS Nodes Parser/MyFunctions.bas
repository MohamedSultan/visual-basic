Attribute VB_Name = "MyFunctions"
Public Function PrepareNewData(ValidatingData As String) As String
    ValidatingData = Replace(ValidatingData, "TRC STATE", "TRC-STATE")
    ValidatingData = Replace(ValidatingData, "Operational State", "Operational-State")
    PrepareNewData = Trim(ValidatingData)
End Function
Public Function GetFileName(NodeName As SMS_Node_Name, DataDate As String, DataTime As Integer) As String
    Dim Suffix As String
    Suffix = Trim(DataDate) + "_" + Trim(Str(DataTime)) + ".txt"
    Select Case NodeName
        Case ams05
            GetFileName = "mbegvf-ams05_" + Suffix
        Case ams06
            GetFileName = "mbegvf-ams06_" + Suffix
        Case ams07
            GetFileName = "mbegvf-ams07_" + Suffix
        Case ams08
            GetFileName = "mbegvf-ams08_" + Suffix
        Case cm01
            GetFileName = "mbegvf-cm01_" + Suffix
        Case eci01
            GetFileName = "mbegvf-eci01_" + Suffix
        Case eci02
            GetFileName = "mbegvf-eci02_" + Suffix
        Case lgp01
            GetFileName = "mbegvf-lgp01_" + Suffix
        Case rtr01
            GetFileName = "mbegvf-rtr01_" + Suffix
        Case rtr02
            GetFileName = "mbegvf-rtr02_" + Suffix
        Case rtr03
            GetFileName = "mbegvf-rtr03_" + Suffix
        Case rtr04
            GetFileName = "mbegvf-rtr04_" + Suffix
        Case rtr05
            GetFileName = "mbegvf-rtr05_" + Suffix
        Case rtr06
            GetFileName = "mbegvf-rtr06_" + Suffix
    End Select
End Function


