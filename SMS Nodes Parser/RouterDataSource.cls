VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 1  'vbDataSource
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RouterDataSource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum Router_Data_Locations_Enum
    DF = 1
    VmStat = 2
    tp_status = 3
    ss7_link = 4
    m3ua_link = 5
    mtpLinkRxUtilisation = 6
    mtpLinkTxUtilisation = 7
    smsCntMoTimeoutCounter = 8
    smsCntMtTimeoutCounter = 9
    smsCntSriSmTimeoutCounter = 10
End Enum
'Type RouterData_Type
'    System As ADODB.Recordset
'End Type


Private Router_DF As ADODB.Recordset
Private Router_vmstat As ADODB.Recordset
Private Router_tp_status As ADODB.Recordset
Private Router_ss7_link As ADODB.Recordset
Private Router_m3ua_link As ADODB.Recordset
Private Router_mtpLinkRxUtilisation As ADODB.Recordset
Private Router_mtpLinkTxUtilisation As ADODB.Recordset
Private Router_smsCntMoTimeoutCounter As ADODB.Recordset
Private Router_smsCntMtTimeoutCounter As ADODB.Recordset
Private Router_smsCntSriSmTimeoutCounter As ADODB.Recordset
Private SMS_Node_Type1 As SMS_Node_Type
Public Property Set Set_SMS_Node(SMS_Node_Name As Variant)
'    Set SMS_Node_Type1 = Val(SMS_Node_Name)
'    Select Case SMS_Node_Type
'        Case Router
'            Call Router_Data_Initialize
'    End Select
End Property

Public Property Get Memory() As ADODB.Recordset
'    Select Case SMS_Node_Type
'        Case Router
            Set Memory = Router_vmstat
'    End Select
End Property
Public Property Get System() As ADODB.Recordset
   Set System = Router_DF
End Property
Public Property Get Proccesses() As ADODB.Recordset
   Set Proccesses = Router_tp_status
End Property
Public Property Get LinkSets() As ADODB.Recordset
   Set LinkSets = Router_ss7_link
End Property
Public Property Get SPCs() As ADODB.Recordset
   Set SPCs = Router_m3ua_link
End Property
Public Property Get MTP_TX() As ADODB.Recordset
   Set MTP_TX = Router_mtpLinkTxUtilisation
End Property
Public Property Get MTP_RX() As ADODB.Recordset
   Set MTP_RX = Router_mtpLinkRxUtilisation
End Property

Public Property Get MoTimeoutCounter() As ADODB.Recordset
   Set MoTimeoutCounter = Router_smsCntMoTimeoutCounter
End Property
Public Property Get MtTimeoutCounter() As ADODB.Recordset
   Set MtTimeoutCounter = Router_smsCntMtTimeoutCounter
End Property
Public Property Get SriTimeoutCounter() As ADODB.Recordset
   Set SriTimeoutCounter = Router_smsCntSriSmTimeoutCounter
End Property

Private Sub Class_GetDataMember(DataMember As String, Data As Object)
    'Set Data = RouterData
End Sub

Private Sub Class_Initialize()
    'SMS_Node_Type = Router
End Sub

Public Sub Initialize(NodeName As SMS_Node_Name, DataDate As String, DataTime As Integer, Optional Directory As String = "D:\FTP\SMS")
    'Call Class_Initialize
    
    Dim SystemData As SMS_RouterData_Type
    SystemData = FormatFile(Directory + "\" + GetFileName(NodeName, DataDate, DataTime))
    
    Call Router_Initialize
    Call Router_Data_Initialize(SystemData)
End Sub
Private Sub Router_Data_Initialize(SystemData As SMS_RouterData_Type)
    Call GetData(SystemData, DF)
    Call GetData(SystemData, VmStat)
    Call GetData(SystemData, tp_status)
    Call GetData(SystemData, ss7_link)
    Call GetData(SystemData, m3ua_link)
    Call GetData(SystemData, mtpLinkRxUtilisation)
    Call GetData(SystemData, mtpLinkTxUtilisation)
    Call GetData(SystemData, smsCntMoTimeoutCounter)
    Call GetData(SystemData, smsCntMtTimeoutCounter)
    Call GetData(SystemData, smsCntSriSmTimeoutCounter)
End Sub

Private Sub GetData(SMS_Details As SMS_RouterData_Type, RecordSetName As Router_Data_Locations_Enum)
   Dim SystemLines As Parser_Return
   
    Select Case RecordSetName
        Case DF
            SystemLines = Parser(SMS_Details.DF + vbCrLf, vbCrLf)
        Case VmStat
            SystemLines = Parser(SMS_Details.VmStat + vbCrLf, vbCrLf)
        Case tp_status
            SystemLines = Parser(SMS_Details.tp_status + vbCrLf, vbCrLf)
        Case ss7_link
            SystemLines = Parser(SMS_Details.ss7_link + vbCrLf, vbCrLf)
        Case m3ua_link
            SystemLines = Parser(SMS_Details.m3ua_link + vbCrLf, vbCrLf)
        Case mtpLinkRxUtilisation
            SystemLines = Parser(SMS_Details.mtpLinkRxUtilisation + vbCrLf, vbCrLf)
        Case mtpLinkTxUtilisation
            SystemLines = Parser(SMS_Details.mtpLinkTxUtilisation + vbCrLf, vbCrLf)
        Case smsCntMoTimeoutCounter
            SystemLines = Parser(SMS_Details.smsCntMoTimeoutCounter + vbCrLf, vbCrLf)
        Case smsCntMtTimeoutCounter
            SystemLines = Parser(SMS_Details.smsCntMtTimeoutCounter + vbCrLf, vbCrLf)
        Case smsCntSriSmTimeoutCounter
            SystemLines = Parser(SMS_Details.smsCntSriSmTimeoutCounter + vbCrLf, vbCrLf)
    End Select
   
    Dim strRow As String
    
    For i = 0 To UBound(SystemLines.Ret) - 1
        strRow = Trim(SystemLines.Ret(i))
        Dashes = strRow
        Call PrepareLine(strRow, " ")
        
        Select Case RecordSetName
            Case DF
                Call AddData(Router_DF, strRow)
            Case VmStat
                Call AddData(Router_vmstat, strRow)
            Case tp_status
                Call AddData(Router_tp_status, strRow)
            Case ss7_link
                Call AddData(Router_ss7_link, strRow)
            Case m3ua_link
                Call AddData(Router_m3ua_link, strRow)
            Case mtpLinkRxUtilisation
                Call AddData(Router_mtpLinkRxUtilisation, strRow)
            Case mtpLinkTxUtilisation
                Call AddData(Router_mtpLinkTxUtilisation, strRow)
            Case smsCntMoTimeoutCounter
                Call AddData(Router_smsCntMoTimeoutCounter, strRow)
            Case smsCntMtTimeoutCounter
                Call AddData(Router_smsCntMtTimeoutCounter, strRow)
            Case smsCntSriSmTimeoutCounter
                Call AddData(Router_smsCntSriSmTimeoutCounter, strRow)
        End Select
    Next i
 '      Close
End Sub
Private Sub AddData(ByRef DataName As ADODB.Recordset, strRow As String)
   Dim fld As ADODB.Field
   Dim strField As String
   Dim intPos As Integer
   
    With DataName
        .AddNew
        For Each fld In .Fields
           ' If a tab delimiter is found, field text is to the
           ' left of the delimiter.
           If InStr(strRow, "|") <> 0 Then
              ' Move position to tab delimiter.
              intPos = InStr(strRow, "|")
              ' Assign field text to strField variable.
              strField = Left(strRow, intPos - 1)
           Else
              ' If a tab delimiter isn't found, field text is the
              ' last field in the row.
              strField = strRow
           End If

           ' Strip off quotation marks.
           If Left(strField, 1) = "|" Then
              strField = Left(strField, Len(strField) - 1)
              strField = Right(strField, Len(strField) - 1)
           End If

           fld.Value = strField

           ' Strip off field value text from text row.
           strRow = Right(strRow, Len(strRow) - intPos)
           intPos = 0

        Next
        .Update
        .MoveFirst
    End With
End Sub
Private Sub PrepareLine(ByRef Line As String, Delimiter As String)
    Dim Pos As Integer
    Line = Replace(Line, Delimiter, "|")
    Pos = InStr(1, Line, "||")
    
    While Pos <> 0
        Line = Replace(Line, "||", "|")
        Pos = InStr(1, Line, "||")
    Wend
End Sub

Private Function FormatFile(FileName As String) As SMS_RouterData_Type
    Dim strRow As String
    'Dim I_FileName As Double
    Dim Data_Locations As Router_Data_Locations_Enum
    Dim FilePartitions() As String
    Data_Locations = Data_Locations + 1
    ReDim Preserve FilePartitions(Data_Locations)
    
    Open FileName For Input As #1
        Do Until EOF(1)
            Line Input #1, strRow
            If InStr(1, strRow, "----") = 1 Then
                Data_Locations = Data_Locations + 1
                ReDim Preserve FilePartitions(Data_Locations)
                Line Input #1, strRow
            End If
            If Trim(strRow) <> "" Then FilePartitions(Data_Locations) = FilePartitions(Data_Locations) + vbCrLf + PrepareNewData(strRow)
        Loop
    Close 1
    
    With FormatFile
        For i = 1 To UBound(FilePartitions)
            Select Case i
                Case DF
                    .DF = FilePartitions(i)
                Case VmStat
                    .VmStat = FilePartitions(i)
                Case tp_status
                    .tp_status = FilePartitions(i)
                Case ss7_link
                    .ss7_link = FilePartitions(i)
                Case m3ua_link
                    .m3ua_link = FilePartitions(i)
                Case mtpLinkRxUtilisation
                    .mtpLinkRxUtilisation = FilePartitions(i)
                Case mtpLinkTxUtilisation
                    .mtpLinkTxUtilisation = FilePartitions(i)
                Case smsCntMoTimeoutCounter
                    .smsCntMoTimeoutCounter = FilePartitions(i)
                Case smsCntMtTimeoutCounter
                    .smsCntMtTimeoutCounter = FilePartitions(i)
                Case smsCntSriSmTimeoutCounter
                    .smsCntSriSmTimeoutCounter = FilePartitions(i)
            End Select
        Next i
    End With
End Function

Private Sub Router_Initialize()
    Set Router_DF = New ADODB.Recordset
    Set Router_vmstat = New ADODB.Recordset
    Set Router_tp_status = New ADODB.Recordset
    Set Router_ss7_link = New ADODB.Recordset
    Set Router_m3ua_link = New ADODB.Recordset
    Set Router_mtpLinkRxUtilisation = New ADODB.Recordset
    Set Router_mtpLinkTxUtilisation = New ADODB.Recordset
    Set Router_smsCntMoTimeoutCounter = New ADODB.Recordset
    Set Router_smsCntMtTimeoutCounter = New ADODB.Recordset
    Set Router_smsCntSriSmTimeoutCounter = New ADODB.Recordset


   With Router_DF
      ' Set CustomerID as the primary key.
      .Fields.Append "Filesystem", adChar, Len("/platform/sun4u-us3/lib/sparcv9/libc_psr/libc_psr_hwcap1.so.1"), adFldUpdatable
      .Fields.Append "size", adChar, 40, adFldUpdatable
      .Fields.Append "used", adChar, 30, adFldUpdatable
      .Fields.Append "avail", adChar, 30, adFldUpdatable
      .Fields.Append "capacity", adChar, 60, adFldUpdatable
      .Fields.Append "Mounted", adChar, Len("/platform/sun4u-us3/lib/sparcv9/libc_psr.so.1"), adFldUpdatable
      ' Use Keyset cursor type to allow updating records.
      .CursorType = adOpenKeyset
      .LockType = adLockOptimistic
      .Open
   End With
   
    With Router_vmstat
        .Fields.Append "r", adChar, 10, adFldUpdatable
        .Fields.Append "b", adChar, 10, adFldUpdatable
        .Fields.Append "w", adChar, 10, adFldUpdatable
        .Fields.Append "swap", adChar, 10, adFldUpdatable
        .Fields.Append "free", adChar, 10, adFldUpdatable
        .Fields.Append "re", adChar, 10, adFldUpdatable
        .Fields.Append "mf", adChar, 10, adFldUpdatable
        .Fields.Append "pi", adChar, 10, adFldUpdatable
        .Fields.Append "po", adChar, 10, adFldUpdatable
        .Fields.Append "fr", adChar, 10, adFldUpdatable
        .Fields.Append "de", adChar, 10, adFldUpdatable
        .Fields.Append "sr", adChar, 10, adFldUpdatable
        .Fields.Append "m0", adChar, 10, adFldUpdatable
        .Fields.Append "m1", adChar, 10, adFldUpdatable
        .Fields.Append "m3", adChar, 10, adFldUpdatable
        .Fields.Append "m4", adChar, 10, adFldUpdatable
        .Fields.Append "in", adChar, 10, adFldUpdatable
        .Fields.Append "syFaults", adChar, 10, adFldUpdatable
        .Fields.Append "cs", adChar, 10, adFldUpdatable
        .Fields.Append "us", adChar, 10, adFldUpdatable
        .Fields.Append "syCPU", adChar, 10, adFldUpdatable
        .Fields.Append "id", adChar, 10, adFldUpdatable
      ' Use Keyset cursor type to allow updating records.
      .CursorType = adOpenKeyset
      .LockType = adLockOptimistic
      .Open
   End With
   
    With Router_tp_status
        .Fields.Append "PROCESS", adChar, Len("textpass"), adFldUpdatable
        .Fields.Append "STATE", adChar, Len("Not operating"), adFldUpdatable
        .Fields.Append "UPTIME", adChar, Len("16 days, 21:11:18.23"), adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_ss7_link
        .Fields.Append "SLC", adChar, 10, adFldUpdatable
        .Fields.Append "TRK", adChar, 10, adFldUpdatable
        .Fields.Append "TS", adChar, 10, adFldUpdatable
        .Fields.Append "TRC-STATE", adChar, Len("Not available"), adFldUpdatable
        .Fields.Append "ERRORS", adChar, 50, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_m3ua_link
        .Fields.Append "NAME", adChar, Len("MSCS11_2_VPC"), adFldUpdatable
        .Fields.Append "Operational-State", adChar, Len("Operational State"), adFldUpdatable
        .Fields.Append "VPC", adChar, Len("inactive"), adFldUpdatable
        .Fields.Append "APC", adChar, Len("inactive"), adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_mtpLinkRxUtilisation
        .Fields.Append "NAME", adChar, Len("mtpLinkRxUtilisation.1.1"), adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Gauge", adChar, Len("Gauge32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 5, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_mtpLinkTxUtilisation
        .Fields.Append "NAME", adChar, Len("mtpLinkRxUtilisation.1.1"), adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Gauge", adChar, Len("Gauge32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 10, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_smsCntMoTimeoutCounter
        .Fields.Append "NAME", adChar, Len("smsCntMoTimeoutCounter.0"), adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Gauge", adChar, Len("Counter32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 10, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_smsCntMtTimeoutCounter
        .Fields.Append "NAME", adChar, Len("smsCntMtTimeoutCounter.0"), adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Gauge", adChar, Len("Counter32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 10, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With Router_smsCntSriSmTimeoutCounter
        .Fields.Append "NAME", adChar, Len("smsCntSriSmTimeoutCounter.0"), adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Gauge", adChar, Len("Counter32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 10, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
End Sub
