VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 1  'vbDataSource
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMDataSource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum CM_Data_Locations_Enum
    DF = 1
    VmStat = 2
    tp_status = 3
    Poll = 4
End Enum
'Type ECIData_Type
'    System As ADODB.Recordset
'End Type


Private CM_DF As ADODB.Recordset
Private CM_vmstat As ADODB.Recordset
Private CM_tp_status As ADODB.Recordset
Private CM_Poll As ADODB.Recordset

Private SMS_Node_Type1 As SMS_Node_Type
Public Property Set Set_SMS_Node(SMS_Node_Name As Variant)
'    Set SMS_Node_Type1 = Val(SMS_Node_Name)
'    Select Case SMS_Node_Type
'        Case ECI
'            Call CM_Data_Initialize
'    End Select
End Property

Public Property Get Memory() As ADODB.Recordset
'    Select Case SMS_Node_Type
'        Case ECI
            Set Memory = CM_vmstat
'    End Select
End Property
Public Property Get System() As ADODB.Recordset
   Set System = CM_DF
End Property
Public Property Get Proccesses() As ADODB.Recordset
   Set Proccesses = CM_tp_status
End Property
Public Property Get Polls() As ADODB.Recordset
   Set Polls = CM_Poll
End Property


Private Sub Class_GetDataMember(DataMember As String, Data As Object)
    'Set Data = ECIData
End Sub

Private Sub Class_Initialize()
    'SMS_Node_Type = ECI
End Sub

Public Sub Initialize(NodeName As SMS_Node_Name, DataDate As String, DataTime As Integer, Optional Directory As String = "D:\FTP\SMS")
    'Call Class_Initialize
    
    Dim SystemData As SMS_CMData_Type
    SystemData = FormatFile(Directory + "\" + GetFileName(NodeName, DataDate, DataTime))
    
    Call CM_Initialize
    Call CM_Data_Initialize(SystemData)

End Sub
Private Sub CM_Data_Initialize(SystemData As SMS_CMData_Type)
    Call GetData(SystemData, DF)
    Call GetData(SystemData, VmStat)
    Call GetData(SystemData, tp_status)
    Call GetData(SystemData, Poll)
End Sub
Private Sub GetData(SMS_Details As SMS_CMData_Type, RecordSetName As CM_Data_Locations_Enum)
   Dim SystemLines As Parser_Return
   
    Select Case RecordSetName
        Case DF
            SystemLines = Parser(SMS_Details.DF + vbCrLf, vbCrLf)
        Case VmStat
            SystemLines = Parser(SMS_Details.VmStat + vbCrLf, vbCrLf)
        Case tp_status
            SystemLines = Parser(SMS_Details.tp_status + vbCrLf, vbCrLf)
        Case Poll
            SystemLines = Parser(SMS_Details.Poll + vbCrLf, vbCrLf)
    End Select
   
    Dim strRow As String
    
    For i = 0 To UBound(SystemLines.Ret) - 1
        strRow = Trim(SystemLines.Ret(i))
        Dashes = strRow
        Call PrepareLine(strRow, " ")
        
        Select Case RecordSetName
            Case DF
                Call AddData(CM_DF, strRow)
            Case VmStat
                Call AddData(CM_vmstat, strRow)
            Case tp_status
                Call AddData(CM_tp_status, strRow)
            Case Poll
                Call AddData(CM_Poll, strRow)
        End Select
    Next i
 '      Close
End Sub
Private Sub AddData(ByRef DataName As ADODB.Recordset, strRow As String)
   Dim fld As ADODB.Field
   Dim strField As String
   Dim intPos As Integer
   
    With DataName
        .AddNew
        For Each fld In .Fields
           ' If a tab delimiter is found, field text is to the
           ' left of the delimiter.
           If InStr(strRow, "|") <> 0 Then
              ' Move position to tab delimiter.
              intPos = InStr(strRow, "|")
              ' Assign field text to strField variable.
              strField = Left(strRow, intPos - 1)
           Else
              ' If a tab delimiter isn't found, field text is the
              ' last field in the row.
              strField = strRow
           End If

           ' Strip off quotation marks.
           If Left(strField, 1) = "|" Then
              strField = Left(strField, Len(strField) - 1)
              strField = Right(strField, Len(strField) - 1)
           End If

           fld.Value = strField

           ' Strip off field value text from text row.
           strRow = Right(strRow, Len(strRow) - intPos)
           intPos = 0

        Next
        .Update
        .MoveFirst
    End With
End Sub
Private Sub PrepareLine(ByRef Line As String, Delimiter As String)
    Dim Pos As Integer
    Line = Replace(Line, Delimiter, "|")
    Pos = InStr(1, Line, "||")
    
    While Pos <> 0
        Line = Replace(Line, "||", "|")
        Pos = InStr(1, Line, "||")
    Wend
End Sub

Private Function FormatFile(FileName As String) As SMS_CMData_Type
    Dim strRow As String
    'Dim I_FileName As Double
    Dim Data_Locations As CM_Data_Locations_Enum
    Dim FilePartitions() As String
    Data_Locations = Data_Locations + 1
    ReDim Preserve FilePartitions(Data_Locations)
    
    Open FileName For Input As #1
        Do Until EOF(1)
            Line Input #1, strRow
            If InStr(1, strRow, "----") = 1 Then
                Data_Locations = Data_Locations + 1
                ReDim Preserve FilePartitions(Data_Locations)
                Line Input #1, strRow
            End If
            If Trim(strRow) <> "" Then FilePartitions(Data_Locations) = FilePartitions(Data_Locations) + vbCrLf + PrepareNewData(strRow)
        Loop
    Close 1
    
    With FormatFile
        For i = 1 To UBound(FilePartitions)
            Select Case i
                Case DF
                    .DF = FilePartitions(i)
                Case VmStat
                    .VmStat = FilePartitions(i)
                Case tp_status
                    .tp_status = FilePartitions(i)
                Case Poll
                    .Poll = FilePartitions(i)
            End Select
        Next i
    End With
End Function

Private Sub CM_Initialize()
    Set CM_DF = New ADODB.Recordset
    Set CM_vmstat = New ADODB.Recordset
    Set CM_tp_status = New ADODB.Recordset
    Set CM_Poll = New ADODB.Recordset


   With CM_DF
      ' Set CustomerID as the primary key.
      .Fields.Append "Filesystem", adChar, Len("/platform/sun4u-us3/lib/sparcv9/libc_psr/libc_psr_hwcap1.so.1"), adFldUpdatable
      .Fields.Append "size", adChar, 40, adFldUpdatable
      .Fields.Append "used", adChar, 30, adFldUpdatable
      .Fields.Append "avail", adChar, 30, adFldUpdatable
      .Fields.Append "capacity", adChar, 60, adFldUpdatable
      .Fields.Append "Mounted", adChar, Len("/platform/sun4u-us3/lib/sparcv9/libc_psr.so.1"), adFldUpdatable
      ' Use Keyset cursor type to allow updating records.
      .CursorType = adOpenKeyset
      .LockType = adLockOptimistic
      .Open
   End With
   
    With CM_vmstat
        .Fields.Append "r", adChar, 10, adFldUpdatable
        .Fields.Append "b", adChar, 10, adFldUpdatable
        .Fields.Append "w", adChar, 10, adFldUpdatable
        .Fields.Append "swap", adChar, 10, adFldUpdatable
        .Fields.Append "free", adChar, 10, adFldUpdatable
        .Fields.Append "re", adChar, 10, adFldUpdatable
        .Fields.Append "mf", adChar, 10, adFldUpdatable
        .Fields.Append "pi", adChar, 10, adFldUpdatable
        .Fields.Append "po", adChar, 10, adFldUpdatable
        .Fields.Append "fr", adChar, 10, adFldUpdatable
        .Fields.Append "de", adChar, 10, adFldUpdatable
        .Fields.Append "sr", adChar, 10, adFldUpdatable
        .Fields.Append "m0", adChar, 10, adFldUpdatable
        .Fields.Append "m1", adChar, 10, adFldUpdatable
        .Fields.Append "m3", adChar, 10, adFldUpdatable
        .Fields.Append "m4", adChar, 10, adFldUpdatable
        .Fields.Append "in", adChar, 10, adFldUpdatable
        .Fields.Append "syFaults", adChar, 10, adFldUpdatable
        .Fields.Append "cs", adChar, 10, adFldUpdatable
        .Fields.Append "us", adChar, 10, adFldUpdatable
        .Fields.Append "syCPU", adChar, 10, adFldUpdatable
        .Fields.Append "id", adChar, 10, adFldUpdatable
      ' Use Keyset cursor type to allow updating records.
      .CursorType = adOpenKeyset
      .LockType = adLockOptimistic
      .Open
   End With
   
    With CM_tp_status
        .Fields.Append "PROCESS", adChar, Len("textpass"), adFldUpdatable
        .Fields.Append "STATE", adChar, Len("Not operating"), adFldUpdatable
        .Fields.Append "UPTIME", adChar, Len("16 days, 21:11:18.23"), adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With CM_Poll
        .Fields.Append "Poll", adChar, Len("textpass") + 5, adFldUpdatable
        .Fields.Append "Proccess_ID", adChar, Len("17866"), adFldUpdatable
        .Fields.Append "Parent_ID", adChar, 5, adFldUpdatable
        .Fields.Append "Child_ID", adChar, Len("17866"), adFldUpdatable
        .Fields.Append "Month", adChar, Len("12:00:18"), adFldUpdatable
        .Fields.Append "Day", adChar, Len("12:00:18"), adFldUpdatable
        .Fields.Append "Teminal", adChar, 10, adFldUpdatable
        .Fields.Append "Started_Since", adChar, Len("1000:1000"), adFldUpdatable
        .Fields.Append "Perl", adChar, Len("/usr/bin/perl"), adFldUpdatable
        .Fields.Append "W", adChar, Len("-w") + Len("poll"), adFldUpdatable
        .Fields.Append "App_Path", adChar, Len("/usr/TextPass/TS/bin/poll_mgr2"), adFldUpdatable
        .Fields.Append "Continous", adChar, Len("--continuous"), adFldUpdatable
        
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
End Sub
