VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 1  'vbDataSource
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AMSDataSource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum AMS_Data_Locations_Enum
    DF = 1
    VmStat = 2
    tp_status = 3
    QueueSizeExceeded = 4
    amsCntTotalStored_Enum = 5
End Enum
'Type ECIData_Type
'    System As ADODB.Recordset
'End Type


Private AMS_DF As ADODB.Recordset
Private AMS_vmstat As ADODB.Recordset
Private AMS_tp_status As ADODB.Recordset
Private AMS_QueueSizeExceeded As ADODB.Recordset
Private AMS_amsCntTotalStored As ADODB.Recordset

Private SMS_Node_Type1 As SMS_Node_Type
Public Property Set Set_SMS_Node(SMS_Node_Name As Variant)
'    Set SMS_Node_Type1 = Val(SMS_Node_Name)
'    Select Case SMS_Node_Type
'        Case ECI
'            Call AMS_Data_Initialize
'    End Select
End Property

Public Property Get Memory() As ADODB.Recordset
'    Select Case SMS_Node_Type
'        Case ECI
            Set Memory = AMS_vmstat
'    End Select
End Property
Public Property Get System() As ADODB.Recordset
   Set System = AMS_DF
End Property
Public Property Get Proccesses() As ADODB.Recordset
   Set Proccesses = AMS_tp_status
End Property
Public Property Get RejectedMaxQueue() As ADODB.Recordset
   Set RejectedMaxQueue = AMS_QueueSizeExceeded
End Property
Public Property Get amsCntTotalStored() As ADODB.Recordset
   Set amsCntTotalStored = AMS_amsCntTotalStored
End Property


Private Sub Class_GetDataMember(DataMember As String, Data As Object)
    'Set Data = ECIData
End Sub

Private Sub Class_Initialize()
    'SMS_Node_Type = ECI
End Sub

Public Sub Initialize(NodeName As SMS_Node_Name, DataDate As String, DataTime As Integer, Optional Directory As String = "D:\FTP\SMS")
    'Call Class_Initialize
    
    Dim SystemData As SMS_AMSData_Type
    SystemData = FormatFile(Directory + "\" + GetFileName(NodeName, DataDate, DataTime))
    
    Call AMS_Initialize
    Call AMS_Data_Initialize(SystemData)

End Sub
Private Sub AMS_Data_Initialize(SystemData As SMS_AMSData_Type)
    Call GetData(SystemData, DF)
    Call GetData(SystemData, VmStat)
    Call GetData(SystemData, tp_status)
    Call GetData(SystemData, QueueSizeExceeded)
    Call GetData(SystemData, amsCntTotalStored_Enum)
End Sub
Private Sub GetData(SMS_Details As SMS_AMSData_Type, RecordSetName As AMS_Data_Locations_Enum)
   Dim SystemLines As Parser_Return
   
    Select Case RecordSetName
        Case DF
            SystemLines = Parser(SMS_Details.DF + vbCrLf, vbCrLf)
        Case VmStat
            SystemLines = Parser(SMS_Details.VmStat + vbCrLf, vbCrLf)
        Case tp_status
            SystemLines = Parser(SMS_Details.tp_status + vbCrLf, vbCrLf)
        Case QueueSizeExceeded
            SystemLines = Parser(SMS_Details.QueueSizeExceeded + vbCrLf, vbCrLf)
        Case amsCntTotalStored_Enum
            SystemLines = Parser(SMS_Details.amsCntTotalStored + vbCrLf, vbCrLf)
    End Select
   
    Dim strRow As String
    
    For i = 0 To UBound(SystemLines.Ret) - 1
        strRow = Trim(SystemLines.Ret(i))
        Dashes = strRow
        Call PrepareLine(strRow, " ")
        
        Select Case RecordSetName
            Case DF
                Call AddData(AMS_DF, strRow)
            Case VmStat
                Call AddData(AMS_vmstat, strRow)
            Case tp_status
                Call AddData(AMS_tp_status, strRow)
            Case QueueSizeExceeded
                Call AddData(AMS_QueueSizeExceeded, strRow)
            Case amsCntTotalStored_Enum
                Call AddData(AMS_amsCntTotalStored, strRow)
        End Select
    Next i
 '      Close
End Sub
Private Sub AddData(ByRef DataName As ADODB.Recordset, strRow As String)
   Dim fld As ADODB.Field
   Dim strField As String
   Dim intPos As Integer
   
    With DataName
        .AddNew
        For Each fld In .Fields
           ' If a tab delimiter is found, field text is to the
           ' left of the delimiter.
           If InStr(strRow, "|") <> 0 Then
              ' Move position to tab delimiter.
              intPos = InStr(strRow, "|")
              ' Assign field text to strField variable.
              strField = Left(strRow, intPos - 1)
           Else
              ' If a tab delimiter isn't found, field text is the
              ' last field in the row.
              strField = strRow
           End If

           ' Strip off quotation marks.
           If Left(strField, 1) = "|" Then
              strField = Left(strField, Len(strField) - 1)
              strField = Right(strField, Len(strField) - 1)
           End If

           fld.Value = strField

           ' Strip off field value text from text row.
           strRow = Right(strRow, Len(strRow) - intPos)
           intPos = 0

        Next
        .Update
        .MoveFirst
    End With
End Sub
Private Sub PrepareLine(ByRef Line As String, Delimiter As String)
    Dim Pos As Integer
    Line = Replace(Line, Delimiter, "|")
    Pos = InStr(1, Line, "||")
    
    While Pos <> 0
        Line = Replace(Line, "||", "|")
        Pos = InStr(1, Line, "||")
    Wend
End Sub

Private Function FormatFile(FileName As String) As SMS_AMSData_Type
    Dim strRow As String
    'Dim I_FileName As Double
    Dim Data_Locations As AMS_Data_Locations_Enum
    Dim FilePartitions() As String
    Data_Locations = Data_Locations + 1
    ReDim Preserve FilePartitions(Data_Locations)
    
    Open FileName For Input As #1
        Do Until EOF(1)
            Line Input #1, strRow
            If InStr(1, strRow, "----") = 1 Then
                Data_Locations = Data_Locations + 1
                ReDim Preserve FilePartitions(Data_Locations)
                Line Input #1, strRow
            End If
            If Trim(strRow) <> "" Then FilePartitions(Data_Locations) = FilePartitions(Data_Locations) + vbCrLf + PrepareNewData(strRow)
        Loop
    Close 1
    
    With FormatFile
        For i = 1 To UBound(FilePartitions)
            Select Case i
                Case DF
                    .DF = FilePartitions(i)
                Case VmStat
                    .VmStat = FilePartitions(i)
                Case tp_status
                    .tp_status = FilePartitions(i)
                Case QueueSizeExceeded
                    .QueueSizeExceeded = FilePartitions(i)
                Case amsCntTotalStored_Enum
                    .amsCntTotalStored = FilePartitions(i)
            End Select
        Next i
    End With
End Function

Private Sub AMS_Initialize()
    Set AMS_DF = New ADODB.Recordset
    Set AMS_vmstat = New ADODB.Recordset
    Set AMS_tp_status = New ADODB.Recordset
    Set AMS_QueueSizeExceeded = New ADODB.Recordset
    Set AMS_amsCntTotalStored = New ADODB.Recordset
    Set AMS_diamTransState = New ADODB.Recordset


   With AMS_DF
      ' Set CustomerID as the primary key.
      .Fields.Append "Filesystem", adChar, Len("/platform/sun4u-us3/lib/sparcv9/libc_psr/libc_psr_hwcap1.so.1"), adFldUpdatable
      .Fields.Append "size", adChar, 40, adFldUpdatable
      .Fields.Append "used", adChar, 30, adFldUpdatable
      .Fields.Append "avail", adChar, 30, adFldUpdatable
      .Fields.Append "capacity", adChar, 60, adFldUpdatable
      .Fields.Append "Mounted", adChar, Len("/platform/sun4u-us3/lib/sparcv9/libc_psr.so.1"), adFldUpdatable
      ' Use Keyset cursor type to allow updating records.
      .CursorType = adOpenKeyset
      .LockType = adLockOptimistic
      .Open
   End With
   
    With AMS_vmstat
        .Fields.Append "r", adChar, 10, adFldUpdatable
        .Fields.Append "b", adChar, 10, adFldUpdatable
        .Fields.Append "w", adChar, 10, adFldUpdatable
        .Fields.Append "swap", adChar, 10, adFldUpdatable
        .Fields.Append "free", adChar, 10, adFldUpdatable
        .Fields.Append "re", adChar, 10, adFldUpdatable
        .Fields.Append "mf", adChar, 10, adFldUpdatable
        .Fields.Append "pi", adChar, 10, adFldUpdatable
        .Fields.Append "po", adChar, 10, adFldUpdatable
        .Fields.Append "fr", adChar, 10, adFldUpdatable
        .Fields.Append "de", adChar, 10, adFldUpdatable
        .Fields.Append "sr", adChar, 10, adFldUpdatable
        .Fields.Append "m0", adChar, 10, adFldUpdatable
        .Fields.Append "m1", adChar, 10, adFldUpdatable
        .Fields.Append "m3", adChar, 10, adFldUpdatable
        .Fields.Append "m4", adChar, 10, adFldUpdatable
        .Fields.Append "in", adChar, 10, adFldUpdatable
        .Fields.Append "syFaults", adChar, 10, adFldUpdatable
        .Fields.Append "cs", adChar, 10, adFldUpdatable
        .Fields.Append "us", adChar, 10, adFldUpdatable
        .Fields.Append "syCPU", adChar, 10, adFldUpdatable
        .Fields.Append "id", adChar, 10, adFldUpdatable
      ' Use Keyset cursor type to allow updating records.
      .CursorType = adOpenKeyset
      .LockType = adLockOptimistic
      .Open
   End With
   
    With AMS_tp_status
        .Fields.Append "PROCESS", adChar, Len("textpass"), adFldUpdatable
        .Fields.Append "STATE", adChar, Len("Not operating"), adFldUpdatable
        .Fields.Append "UPTIME", adChar, Len("16 days, 21:11:18.23"), adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With AMS_QueueSizeExceeded
        .Fields.Append "QueueSizeExceeded", adChar, Len("queueCntRejectedMaxQueueSizeExceeded.1") + 5, adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Counter32", adChar, Len("Counter32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 10, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
    
    With AMS_amsCntTotalStored
        .Fields.Append "NAME", adChar, Len("amsCntTotalStored.0"), adFldUpdatable
        .Fields.Append "Temp1", adChar, 1, adFldUpdatable
        .Fields.Append "Counter32", adChar, Len("Counter32:"), adFldUpdatable
        .Fields.Append "Value", adChar, 10, adFldUpdatable
        ' Use Keyset cursor type to allow updating records.
        .CursorType = adOpenKeyset
        .LockType = adLockOptimistic
        .Open
    End With
End Sub
