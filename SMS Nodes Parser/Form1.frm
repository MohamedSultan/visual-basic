VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "SMS E2E Stats Collector"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton LGP_Command 
      Caption         =   "LGP"
      Height          =   255
      Left            =   3000
      TabIndex        =   4
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton CM_Command1 
      Caption         =   "CM"
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton AMS_Command 
      Caption         =   "AMS"
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.CommandButton ECI_Command 
      Caption         =   "ECI"
      Height          =   255
      Left            =   3000
      TabIndex        =   1
      Top             =   1560
      Width           =   1095
   End
   Begin VB.CommandButton Router 
      Caption         =   "Router1"
      Height          =   315
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub AMS_Command_Click()
    Call Generate_SMS_Data_N_Insert(ams05, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(ams06, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(ams07, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(ams08, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
End Sub

Private Sub CM_Command1_Click()
    Call Generate_SMS_Data_N_Insert(cm01, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
End Sub

Private Sub ECI_Command_Click()
    Call Generate_SMS_Data_N_Insert(eci01, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(eci02, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
End Sub

Private Sub Form_Load()
    Call Router_Click
    Call AMS_Command_Click
    Call CM_Command1_Click
    Call LGP_Command_Click
    Call ECI_Command_Click
    End
End Sub

Private Sub LGP_Command_Click()
    Call Generate_SMS_Data_N_Insert(lgp01, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
End Sub

Private Sub Router_Click()
    Call Generate_SMS_Data_N_Insert(rtr01, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(rtr02, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(rtr03, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(rtr04, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(rtr05, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
    Call Generate_SMS_Data_N_Insert(rtr06, Format(Date, "YYYYMMDD"), Format(Time, "HH"))
End Sub
Private Sub Generate_SMS_Data_N_Insert(NodeName As SMS_Node_Name, DataDate As String, Time As Integer)
    Select Case NodeName
        Case rtr01, rtr02, rtr03, rtr04, rtr05, rtr06
            Dim Router_Details As RouterDataSource
            Set Router_Details = New RouterDataSource
            Call Router_Details.Initialize(NodeName, DataDate, Time, "D:\FTP\SMS")
            Call DB_Insert(NodeName, Router_Details, DataDate, Time)
            Set Router_Details = Nothing
        Case ams05, ams06, ams07, ams08
            Dim AMS_Details As AMSDataSource
            Set AMS_Details = New AMSDataSource
            Call AMS_Details.Initialize(NodeName, DataDate, Time, "D:\FTP\SMS")
            Call DB_Insert(NodeName, AMS_Details, DataDate, Time)
        Case lgp01
            Dim LGP_Details As LGPDataSource
            Set LGP_Details = New LGPDataSource
            Call LGP_Details.Initialize(NodeName, DataDate, Time, "D:\FTP\SMS")
            Call DB_Insert(NodeName, LGP_Details, DataDate, Time)
        Case eci01, eci02
            Dim ECI_Details As ECIDataSource
            Set ECI_Details = New ECIDataSource
            Call ECI_Details.Initialize(NodeName, DataDate, Time, "D:\FTP\SMS")
            Call DB_Insert(NodeName, ECI_Details, DataDate, Time)
        Case cm01
            Dim CM_Details As CMDataSource
            Set CM_Details = New CMDataSource
            Call CM_Details.Initialize(NodeName, DataDate, Time, "D:\FTP\SMS")
            Call DB_Insert(NodeName, CM_Details, DataDate, Time)
    End Select
End Sub
