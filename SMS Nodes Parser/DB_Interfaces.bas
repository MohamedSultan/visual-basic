Attribute VB_Name = "DB_Interfaces"
Public Sub DB_Open(ByRef Conn As ADODB.Connection)
    On Error GoTo LocalHandler

    If Conn.State <> 1 Then
        Set Conn = New ADODB.Connection
        
        Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=SMS_E2E_User;PWD=SMS_E2E_User;DATABASE=SMS_E2E;SERVER=EN7131D;PORT=3306"
        Conn.Open
    End If
    

LocalHandler:
    If Err.Number = 20 Or Err.Number = 0 Or Err.Number = 91 Then
        X = 1
        Resume Next
    Else
        'MsgBox Err.Description
    End If
End Sub

Public Function DB_Insert(NodeName As SMS_Node_Name, ByRef SMS_Type_Data As Variant, DataDate As String, Time As Integer)
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn)
             
    Dim Statement As String
    Dim DB_Query As ADODB.Recordset
    Set DB_Query = New ADODB.Recordset
    
    Dim SMS_NodeName_String As String
    SMS_NodeName_String = NodeName_Mapper(NodeName)

    'Inserting TP_STaus Data
    With SMS_Type_Data.Proccesses
        .MoveNext
        While Not .EOF
            Statement = Generate_Statement(SMS_NodeName_String, tp_status_, SMS_Type_Data.Proccesses, DataDate, Time)
            DB_Query.Open Statement, Conn
            .MoveNext
        Wend
     End With
    'Inserting System Data
    With SMS_Type_Data.System
        .MoveNext
        While Not .EOF
            Statement = Generate_Statement(SMS_NodeName_String, DF_, SMS_Type_Data.System, DataDate, Time)
            DB_Query.Open Statement, Conn
            .MoveNext
        Wend
    End With
    'Inserting memory Data
    Dim ID As Integer
    ID = 1
    With SMS_Type_Data.Memory
        .MoveNext
        .MoveNext
        While Not .EOF
            Statement = Generate_Statement(SMS_NodeName_String, VmStat_, SMS_Type_Data.Memory, DataDate, Time, ID)
            DB_Query.Open Statement, Conn
            .MoveNext
            ID = ID + 1
        Wend
    End With
    
    Select Case NodeName
        Case rtr01, rtr02, rtr03, rtr04, rtr05, rtr06

            'SS7
            With SMS_Type_Data.LinkSets
                Dim LinkSetName As String
                'Dim Temp As String
                While Not .EOF
                    'Temp = .Fields("SLC").Value
                    If InStr(1, .Fields("SLC").Value, "Linkset") <> 0 Then
                        LinkSetName = .Fields("TRK").Value
                        .MoveNext
                        .MoveNext
                    End If
                    Statement = Generate_Statement(SMS_NodeName_String, ss7_link_, SMS_Type_Data.LinkSets, DataDate, Time, , LinkSetName)
                    DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering router_mtplinkrxutilisation
            With SMS_Type_Data.MTP_RX
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, mtpLinkRxUtilisation_, SMS_Type_Data.MTP_RX, DataDate, Time)
                    DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering SPCs
            With SMS_Type_Data.SPCs
                .MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, m3ua_link_, SMS_Type_Data.SPCs, DataDate, Time)
                    DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering Router_mtpLinkTxUtilisation
            With SMS_Type_Data.MTP_TX
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, mtpLinktxUtilisation_, SMS_Type_Data.MTP_TX, DataDate, Time)
                    DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering smsCntMoTimeoutCounter
            With SMS_Type_Data.MoTimeoutCounter
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, smsCntMoTimeoutCounter_, SMS_Type_Data.MoTimeoutCounter, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering smsCntMtTimeoutCounter
            With SMS_Type_Data.MtTimeoutCounter
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, smsCntMtTimeoutCounter_, SMS_Type_Data.MtTimeoutCounter, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering smsCntSriTimeoutCounter
            With SMS_Type_Data.SriTimeoutCounter
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, smsCntSriSmTimeoutCounter_, SMS_Type_Data.SriTimeoutCounter, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
        Case ams05, ams06, ams07, ams08
            'Insering queueCntRejectedMaxQueueSizeExceeded
            With SMS_Type_Data.RejectedMaxQueue
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, QueueSizeExceeded_, SMS_Type_Data.RejectedMaxQueue, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering amsCntTotalStored
            With SMS_Type_Data.amsCntTotalStored
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, amsCntTotalStored_, SMS_Type_Data.amsCntTotalStored, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
        Case lgp01
            'Do Nothing
        Case eci01, eci02
            'Insering ECI_pbcChainsRet
            With SMS_Type_Data.Chains
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, pbcChainsRet_, SMS_Type_Data.Chains, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering ECI_diamTransAdminState
            With SMS_Type_Data.TransAdminState
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, diamTransAdminState_, SMS_Type_Data.TransAdminState, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
            'Insering ECI_diamTransState
            With SMS_Type_Data.TransState
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, diamTransState_, SMS_Type_Data.TransState, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
        Case cm01
            With SMS_Type_Data.Polls
                '.MoveNext
                While Not .EOF
                    Statement = Generate_Statement(SMS_NodeName_String, Poll_, SMS_Type_Data.Polls, DataDate, Time)
                   DB_Query.Open Statement, Conn
                    .MoveNext
                Wend
             End With
    End Select
    

End Function
Private Function Generate_Statement(NodeName As String, SMS_Stats As SMS_Stats_Type, ByRef SMS_Type_Data As ADODB.Recordset, DataDate As String, Time As Integer, _
                                        Optional ID As Integer, Optional LinkSetName As String) As String
    With SMS_Type_Data
        Select Case SMS_Stats
            Case DF_
                Generate_Statement = "INSERT INTO system(avail,capacity,Date,FileSystem,Mounted,Size,SMS_Node_Name,Time,used) " + _
                                        "VALUES (" + _
                                        "'" + Trim(.Fields("Avail").Value) + "'," + _
                                        "'" + Trim(.Fields("capacity").Value) + "'," + _
                                        "'" + DataDate + "'," + _
                                        "'" + Trim(.Fields("FileSystem").Value) + "'," + _
                                        "'" + Trim(.Fields("Mounted").Value) + "'," + _
                                        "'" + Trim(.Fields("Size").Value) + "'," + _
                                        "'" + NodeName + "'," + _
                                        "'" + Trim(Str(Time)) + "'," + _
                                        "'" + Trim(.Fields("used").Value) + "'" + _
                                        ")"
            Case VmStat_
                Dim Part1, Part2 As String
                Part1 = "INSERT INTO vmstat(b,cs,de,fr,free,id,in_,m0,m1,m3,m4,mf,pi,po,r,re,SMS_Node_NAme,sr,swap,syCPU,syFaults,us,w,Date,Time,Check_ID) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("b").Value) + "'," + _
                            "'" + Trim(.Fields("cs").Value) + "'," + _
                            "'" + Trim(.Fields("de").Value) + "'," + _
                            "'" + Trim(.Fields("fr").Value) + "'," + _
                            "'" + Trim(.Fields("free").Value) + "'," + _
                            "'" + Trim(.Fields("id").Value) + "'," + _
                            "'" + Trim(.Fields("in").Value) + "'," + _
                            "'" + Trim(.Fields("m0").Value) + "'," + _
                            "'" + Trim(.Fields("m1").Value) + "'," + _
                            "'" + Trim(.Fields("m3").Value) + "'," + _
                            "'" + Trim(.Fields("m4").Value) + "'," + _
                            "'" + Trim(.Fields("mf").Value) + "'," + _
                            "'" + Trim(.Fields("pi").Value) + "'," + _
                            "'" + Trim(.Fields("po").Value) + "'," + _
                            "'" + Trim(.Fields("r").Value) + "'," + _
                            "'" + Trim(.Fields("re").Value) + "',"
                Part2 = "'" + NodeName + "'," + _
                        "'" + Trim(.Fields("sr").Value) + "'," + _
                        "'" + Trim(.Fields("swap").Value) + "'," + _
                        "'" + Trim(.Fields("syCPU").Value) + "'," + _
                        "'" + Trim(.Fields("syFaults").Value) + "'," + _
                        "'" + Trim(.Fields("us").Value) + "'," + _
                        "'" + Trim(.Fields("w").Value) + "'," + _
                        "'" + DataDate + "'," + _
                        "'" + Trim(Str(Time)) + "'," + _
                        "'" + Trim(Str(ID)) + "'" + _
                        ")"
                Generate_Statement = Part1 + Part2
            Case tp_status_
                Generate_Statement = "INSERT INTO tp_status(PROCESS,STATE,Date,UPTIME,SMS_Node_Name,Time) " + _
                                        "VALUES (" + _
                                        "'" + Trim(.Fields("PROCESS").Value) + "'," + _
                                        "'" + Trim(.Fields("STATE").Value) + "'," + _
                                        "'" + DataDate + "'," + _
                                        "'" + Trim(.Fields("UPTIME").Value) + "'," + _
                                        "'" + NodeName + "'," + _
                                        "'" + Trim(Str(Time)) + "'" + _
                                        ")"
            Case ss7_link_
                Generate_Statement = "INSERT INTO router_ss7_link(ERRORS,LinkSetName,Date,SLC,SMS_Node_Name,Time,TRC_STATE,TRK,TS) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("ERRORS").Value) + "'," + _
                            "'" + Trim(LinkSetName) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("SLC").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'," + _
                            "'" + Trim(.Fields("TRC-STATE").Value) + "'," + _
                            "'" + Trim(.Fields("TRK").Value) + "'," + _
                            "'" + Trim(.Fields("TS").Value) + "'" + _
                            ")"
            Case m3ua_link_
                Generate_Statement = "INSERT INTO router_m3ua_link(APC,Date,NAME,SMS_Node_Name,Time,Operational_State,VPC) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("APC").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'," + _
                            "'" + Trim(.Fields("Operational-State").Value) + "'," + _
                            "'" + Trim(.Fields("VPC").Value) + "'" + _
                            ")"
            Case mtpLinkRxUtilisation_
                Generate_Statement = "INSERT INTO router_mtplinkrxutilisation(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case mtpLinktxUtilisation_
                Generate_Statement = "INSERT INTO router_mtplinktxutilisation(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case smsCntMoTimeoutCounter_
                Generate_Statement = "INSERT INTO router_smscntmotimeoutcounter(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case smsCntMtTimeoutCounter_
                Generate_Statement = "INSERT INTO router_smscntmttimeoutcounter(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case smsCntSriSmTimeoutCounter_
                Generate_Statement = "INSERT INTO router_smscntsrismtimeoutcounter(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case Poll_
                Generate_Statement = "INSERT INTO cm_polls(App_Path,Date,Child_ID,SMS_Node_Name,Time,Continous,Parent_ID,Perl,Poll,Proccess_ID) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("App_Path").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("Child_ID").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'," + _
                            "'" + Trim(.Fields("Continous").Value) + "'," + _
                            "'" + Trim(.Fields("Parent_ID").Value) + "'," + _
                            "'" + Trim(.Fields("Perl").Value) + "'," + _
                            "'" + Trim(.Fields("Poll").Value) + "'," + _
                            "'" + Trim(.Fields("Proccess_ID").Value) + "'" + _
                            ")"
            Case pbcChainsRet_
                Generate_Statement = "INSERT INTO ECI_pbcChainsRet(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("pbcChainsRet").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case diamTransAdminState_
                Generate_Statement = "INSERT INTO ECI_diamTransAdminState(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case diamTransState_
                Generate_Statement = "INSERT INTO ECI_diamTransState(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case amsCntTotalStored_
                Generate_Statement = "INSERT INTO AMS_amsCntTotalStored(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("NAME").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
            Case QueueSizeExceeded_
                Generate_Statement = "INSERT INTO AMS_QueueSizeExceeded(Value,Date,NAME,SMS_Node_Name,Time) " + _
                            "VALUES (" + _
                            "'" + Trim(.Fields("Value").Value) + "'," + _
                            "'" + DataDate + "'," + _
                            "'" + Trim(.Fields("QueueSizeExceeded").Value) + "'," + _
                            "'" + NodeName + "'," + _
                            "'" + Trim(Str(Time)) + "'" + _
                            ")"
        End Select
    End With
End Function
Private Function NodeName_Mapper(NodeName As SMS_Node_Name) As String
    Select Case NodeName
        Case rtr01
            NodeName_Mapper = "Router1"
        Case rtr02
            NodeName_Mapper = "Router2"
        Case rtr03
            NodeName_Mapper = "Router3"
        Case rtr04
            NodeName_Mapper = "Router4"
        Case rtr05
            NodeName_Mapper = "Router5"
        Case rtr06
            NodeName_Mapper = "Router6"
        Case ams05
            NodeName_Mapper = "AMS05"
        Case ams06
            NodeName_Mapper = "AMS06"
        Case ams07
            NodeName_Mapper = "AMS07"
        Case ams08
            NodeName_Mapper = "AMS08"
        Case lgp01
            NodeName_Mapper = "LGP01"
        Case eci01
            NodeName_Mapper = "ECI01"
        Case eci02
            NodeName_Mapper = "ECI02"
        Case cm01
            NodeName_Mapper = "CM01"
    End Select
End Function
