VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form SS7_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SS7 Creator"
   ClientHeight    =   12390
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9525
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   12390
   ScaleWidth      =   9525
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   1
      Left            =   840
      TabIndex        =   129
      Top             =   360
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   0
         ItemData        =   "SS7_Form.frx":0000
         Left            =   3120
         List            =   "SS7_Form.frx":0002
         TabIndex        =   133
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   0
         ItemData        =   "SS7_Form.frx":0004
         Left            =   2280
         List            =   "SS7_Form.frx":0006
         TabIndex        =   132
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   0
         ItemData        =   "SS7_Form.frx":0008
         Left            =   1440
         List            =   "SS7_Form.frx":000A
         TabIndex        =   131
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   0
         ItemData        =   "SS7_Form.frx":000C
         Left            =   600
         List            =   "SS7_Form.frx":000E
         TabIndex        =   130
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   134
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   0
      Left            =   840
      TabIndex        =   135
      Top             =   720
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   1
         ItemData        =   "SS7_Form.frx":0010
         Left            =   3120
         List            =   "SS7_Form.frx":0012
         TabIndex        =   139
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   1
         ItemData        =   "SS7_Form.frx":0014
         Left            =   2280
         List            =   "SS7_Form.frx":0016
         TabIndex        =   138
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   1
         ItemData        =   "SS7_Form.frx":0018
         Left            =   1440
         List            =   "SS7_Form.frx":001A
         TabIndex        =   137
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   1
         ItemData        =   "SS7_Form.frx":001C
         Left            =   600
         List            =   "SS7_Form.frx":001E
         TabIndex        =   136
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   140
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   2
      Left            =   840
      TabIndex        =   141
      Top             =   1080
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   2
         ItemData        =   "SS7_Form.frx":0020
         Left            =   600
         List            =   "SS7_Form.frx":0022
         TabIndex        =   145
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   2
         ItemData        =   "SS7_Form.frx":0024
         Left            =   1440
         List            =   "SS7_Form.frx":0026
         TabIndex        =   144
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   2
         ItemData        =   "SS7_Form.frx":0028
         Left            =   2280
         List            =   "SS7_Form.frx":002A
         TabIndex        =   143
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   2
         ItemData        =   "SS7_Form.frx":002C
         Left            =   3120
         List            =   "SS7_Form.frx":002E
         TabIndex        =   142
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   146
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   3
      Left            =   840
      TabIndex        =   147
      Top             =   1440
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   3
         ItemData        =   "SS7_Form.frx":0030
         Left            =   600
         List            =   "SS7_Form.frx":0032
         TabIndex        =   151
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   3
         ItemData        =   "SS7_Form.frx":0034
         Left            =   1440
         List            =   "SS7_Form.frx":0036
         TabIndex        =   150
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   3
         ItemData        =   "SS7_Form.frx":0038
         Left            =   2280
         List            =   "SS7_Form.frx":003A
         TabIndex        =   149
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   3
         ItemData        =   "SS7_Form.frx":003C
         Left            =   3120
         List            =   "SS7_Form.frx":003E
         TabIndex        =   148
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   152
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   4
      Left            =   840
      TabIndex        =   153
      Top             =   1800
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   4
         ItemData        =   "SS7_Form.frx":0040
         Left            =   600
         List            =   "SS7_Form.frx":0042
         TabIndex        =   157
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   4
         ItemData        =   "SS7_Form.frx":0044
         Left            =   1440
         List            =   "SS7_Form.frx":0046
         TabIndex        =   156
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   4
         ItemData        =   "SS7_Form.frx":0048
         Left            =   2280
         List            =   "SS7_Form.frx":004A
         TabIndex        =   155
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   4
         ItemData        =   "SS7_Form.frx":004C
         Left            =   3120
         List            =   "SS7_Form.frx":004E
         TabIndex        =   154
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   158
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   5
      Left            =   840
      TabIndex        =   159
      Top             =   2160
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   5
         ItemData        =   "SS7_Form.frx":0050
         Left            =   600
         List            =   "SS7_Form.frx":0052
         TabIndex        =   163
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   5
         ItemData        =   "SS7_Form.frx":0054
         Left            =   1440
         List            =   "SS7_Form.frx":0056
         TabIndex        =   162
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   5
         ItemData        =   "SS7_Form.frx":0058
         Left            =   2280
         List            =   "SS7_Form.frx":005A
         TabIndex        =   161
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   5
         ItemData        =   "SS7_Form.frx":005C
         Left            =   3120
         List            =   "SS7_Form.frx":005E
         TabIndex        =   160
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   164
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   6
      Left            =   840
      TabIndex        =   165
      Top             =   2520
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   6
         ItemData        =   "SS7_Form.frx":0060
         Left            =   600
         List            =   "SS7_Form.frx":0062
         TabIndex        =   169
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   6
         ItemData        =   "SS7_Form.frx":0064
         Left            =   1440
         List            =   "SS7_Form.frx":0066
         TabIndex        =   168
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   6
         ItemData        =   "SS7_Form.frx":0068
         Left            =   2280
         List            =   "SS7_Form.frx":006A
         TabIndex        =   167
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   6
         ItemData        =   "SS7_Form.frx":006C
         Left            =   3120
         List            =   "SS7_Form.frx":006E
         TabIndex        =   166
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   170
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   7
      Left            =   840
      TabIndex        =   171
      Top             =   2880
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   7
         ItemData        =   "SS7_Form.frx":0070
         Left            =   3120
         List            =   "SS7_Form.frx":0072
         TabIndex        =   175
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   7
         ItemData        =   "SS7_Form.frx":0074
         Left            =   2280
         List            =   "SS7_Form.frx":0076
         TabIndex        =   174
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   7
         ItemData        =   "SS7_Form.frx":0078
         Left            =   1440
         List            =   "SS7_Form.frx":007A
         TabIndex        =   173
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   7
         ItemData        =   "SS7_Form.frx":007C
         Left            =   600
         List            =   "SS7_Form.frx":007E
         TabIndex        =   172
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   176
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   8
      Left            =   840
      TabIndex        =   177
      Top             =   3240
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   8
         ItemData        =   "SS7_Form.frx":0080
         Left            =   3120
         List            =   "SS7_Form.frx":0082
         TabIndex        =   181
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   8
         ItemData        =   "SS7_Form.frx":0084
         Left            =   2280
         List            =   "SS7_Form.frx":0086
         TabIndex        =   180
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   8
         ItemData        =   "SS7_Form.frx":0088
         Left            =   1440
         List            =   "SS7_Form.frx":008A
         TabIndex        =   179
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   8
         ItemData        =   "SS7_Form.frx":008C
         Left            =   600
         List            =   "SS7_Form.frx":008E
         TabIndex        =   178
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   182
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   9
      Left            =   840
      TabIndex        =   183
      Top             =   3600
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   9
         ItemData        =   "SS7_Form.frx":0090
         Left            =   3120
         List            =   "SS7_Form.frx":0092
         TabIndex        =   187
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   9
         ItemData        =   "SS7_Form.frx":0094
         Left            =   2280
         List            =   "SS7_Form.frx":0096
         TabIndex        =   186
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   9
         ItemData        =   "SS7_Form.frx":0098
         Left            =   1440
         List            =   "SS7_Form.frx":009A
         TabIndex        =   185
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   9
         ItemData        =   "SS7_Form.frx":009C
         Left            =   600
         List            =   "SS7_Form.frx":009E
         TabIndex        =   184
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   188
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   10
      Left            =   840
      TabIndex        =   189
      Top             =   3960
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   10
         ItemData        =   "SS7_Form.frx":00A0
         Left            =   3120
         List            =   "SS7_Form.frx":00A2
         TabIndex        =   193
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   10
         ItemData        =   "SS7_Form.frx":00A4
         Left            =   2280
         List            =   "SS7_Form.frx":00A6
         TabIndex        =   192
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   10
         ItemData        =   "SS7_Form.frx":00A8
         Left            =   1440
         List            =   "SS7_Form.frx":00AA
         TabIndex        =   191
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   10
         ItemData        =   "SS7_Form.frx":00AC
         Left            =   600
         List            =   "SS7_Form.frx":00AE
         TabIndex        =   190
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   194
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   11
      Left            =   840
      TabIndex        =   195
      Top             =   4320
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   11
         ItemData        =   "SS7_Form.frx":00B0
         Left            =   600
         List            =   "SS7_Form.frx":00B2
         TabIndex        =   199
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   11
         ItemData        =   "SS7_Form.frx":00B4
         Left            =   1440
         List            =   "SS7_Form.frx":00B6
         TabIndex        =   198
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   11
         ItemData        =   "SS7_Form.frx":00B8
         Left            =   2280
         List            =   "SS7_Form.frx":00BA
         TabIndex        =   197
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   11
         ItemData        =   "SS7_Form.frx":00BC
         Left            =   3120
         List            =   "SS7_Form.frx":00BE
         TabIndex        =   196
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   200
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "SDP B"
      Height          =   8055
      Left            =   5760
      TabIndex        =   0
      Top             =   480
      Width           =   2895
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   31
         Left            =   1680
         TabIndex        =   128
         Top             =   7680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   31
         Left            =   960
         TabIndex        =   127
         Top             =   7680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   31
         Left            =   240
         TabIndex        =   126
         Top             =   7680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   31
         Left            =   2400
         TabIndex        =   125
         Top             =   7680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   30
         Left            =   2400
         TabIndex        =   124
         Top             =   7440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   29
         Left            =   2400
         TabIndex        =   123
         Top             =   7200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   28
         Left            =   2400
         TabIndex        =   122
         Top             =   6960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   27
         Left            =   2400
         TabIndex        =   121
         Top             =   6720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   26
         Left            =   2400
         TabIndex        =   120
         Top             =   6480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   25
         Left            =   2400
         TabIndex        =   119
         Top             =   6240
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   24
         Left            =   2400
         TabIndex        =   118
         Top             =   6000
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   23
         Left            =   2400
         TabIndex        =   117
         Top             =   5760
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   22
         Left            =   2400
         TabIndex        =   116
         Top             =   5520
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   21
         Left            =   2400
         TabIndex        =   115
         Top             =   5280
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   20
         Left            =   2400
         TabIndex        =   114
         Top             =   5040
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   19
         Left            =   2400
         TabIndex        =   113
         Top             =   4800
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   18
         Left            =   2400
         TabIndex        =   112
         Top             =   4560
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   17
         Left            =   2400
         TabIndex        =   111
         Top             =   4320
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   16
         Left            =   2400
         TabIndex        =   110
         Top             =   4080
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   15
         Left            =   2400
         TabIndex        =   109
         Top             =   3840
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   14
         Left            =   2400
         TabIndex        =   108
         Top             =   3600
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   13
         Left            =   2400
         TabIndex        =   107
         Top             =   3360
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   12
         Left            =   2400
         TabIndex        =   106
         Top             =   3120
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   11
         Left            =   2400
         TabIndex        =   105
         Top             =   2880
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   10
         Left            =   2400
         TabIndex        =   104
         Top             =   2640
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   9
         Left            =   2400
         TabIndex        =   103
         Top             =   2400
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   8
         Left            =   2400
         TabIndex        =   102
         Top             =   2160
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   7
         Left            =   2400
         TabIndex        =   101
         Top             =   1920
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   6
         Left            =   2400
         TabIndex        =   100
         Top             =   1680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   5
         Left            =   2400
         TabIndex        =   99
         Top             =   1440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   4
         Left            =   2400
         TabIndex        =   98
         Top             =   1200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   3
         Left            =   2400
         TabIndex        =   97
         Top             =   960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   2
         Left            =   2400
         TabIndex        =   96
         Top             =   720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortD 
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   95
         Top             =   480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   30
         Left            =   1680
         TabIndex        =   94
         Top             =   7440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   29
         Left            =   1680
         TabIndex        =   93
         Top             =   7200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   28
         Left            =   1680
         TabIndex        =   92
         Top             =   6960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   27
         Left            =   1680
         TabIndex        =   91
         Top             =   6720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   26
         Left            =   1680
         TabIndex        =   90
         Top             =   6480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   25
         Left            =   1680
         TabIndex        =   89
         Top             =   6240
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   24
         Left            =   1680
         TabIndex        =   88
         Top             =   6000
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   23
         Left            =   1680
         TabIndex        =   87
         Top             =   5760
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   22
         Left            =   1680
         TabIndex        =   86
         Top             =   5520
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   21
         Left            =   1680
         TabIndex        =   85
         Top             =   5280
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   20
         Left            =   1680
         TabIndex        =   84
         Top             =   5040
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   19
         Left            =   1680
         TabIndex        =   83
         Top             =   4800
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   18
         Left            =   1680
         TabIndex        =   82
         Top             =   4560
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   17
         Left            =   1680
         TabIndex        =   81
         Top             =   4320
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   16
         Left            =   1680
         TabIndex        =   80
         Top             =   4080
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   15
         Left            =   1680
         TabIndex        =   79
         Top             =   3840
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   14
         Left            =   1680
         TabIndex        =   78
         Top             =   3600
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   13
         Left            =   1680
         TabIndex        =   77
         Top             =   3360
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   12
         Left            =   1680
         TabIndex        =   76
         Top             =   3120
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   11
         Left            =   1680
         TabIndex        =   75
         Top             =   2880
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   10
         Left            =   1680
         TabIndex        =   74
         Top             =   2640
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   9
         Left            =   1680
         TabIndex        =   73
         Top             =   2400
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   8
         Left            =   1680
         TabIndex        =   72
         Top             =   2160
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   7
         Left            =   1680
         TabIndex        =   71
         Top             =   1920
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   6
         Left            =   1680
         TabIndex        =   70
         Top             =   1680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   5
         Left            =   1680
         TabIndex        =   69
         Top             =   1440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   4
         Left            =   1680
         TabIndex        =   68
         Top             =   1200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   3
         Left            =   1680
         TabIndex        =   67
         Top             =   960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   2
         Left            =   1680
         TabIndex        =   66
         Top             =   720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortC 
         Height          =   255
         Index           =   1
         Left            =   1680
         TabIndex        =   65
         Top             =   480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   30
         Left            =   960
         TabIndex        =   64
         Top             =   7440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   29
         Left            =   960
         TabIndex        =   63
         Top             =   7200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   28
         Left            =   960
         TabIndex        =   62
         Top             =   6960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   27
         Left            =   960
         TabIndex        =   61
         Top             =   6720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   26
         Left            =   960
         TabIndex        =   60
         Top             =   6480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   25
         Left            =   960
         TabIndex        =   59
         Top             =   6240
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   24
         Left            =   960
         TabIndex        =   58
         Top             =   6000
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   23
         Left            =   960
         TabIndex        =   57
         Top             =   5760
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   22
         Left            =   960
         TabIndex        =   56
         Top             =   5520
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   21
         Left            =   960
         TabIndex        =   55
         Top             =   5280
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   20
         Left            =   960
         TabIndex        =   54
         Top             =   5040
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   19
         Left            =   960
         TabIndex        =   53
         Top             =   4800
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   18
         Left            =   960
         TabIndex        =   52
         Top             =   4560
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   17
         Left            =   960
         TabIndex        =   51
         Top             =   4320
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   16
         Left            =   960
         TabIndex        =   50
         Top             =   4080
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   15
         Left            =   960
         TabIndex        =   49
         Top             =   3840
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   14
         Left            =   960
         TabIndex        =   48
         Top             =   3600
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   13
         Left            =   960
         TabIndex        =   47
         Top             =   3360
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   12
         Left            =   960
         TabIndex        =   46
         Top             =   3120
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   11
         Left            =   960
         TabIndex        =   45
         Top             =   2880
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   10
         Left            =   960
         TabIndex        =   44
         Top             =   2640
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   9
         Left            =   960
         TabIndex        =   43
         Top             =   2400
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   8
         Left            =   960
         TabIndex        =   42
         Top             =   2160
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   7
         Left            =   960
         TabIndex        =   41
         Top             =   1920
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   6
         Left            =   960
         TabIndex        =   40
         Top             =   1680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   5
         Left            =   960
         TabIndex        =   39
         Top             =   1440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   4
         Left            =   960
         TabIndex        =   38
         Top             =   1200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   3
         Left            =   960
         TabIndex        =   37
         Top             =   960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   2
         Left            =   960
         TabIndex        =   36
         Top             =   720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortB 
         Height          =   255
         Index           =   1
         Left            =   960
         TabIndex        =   35
         Top             =   480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   30
         Left            =   240
         TabIndex        =   34
         Top             =   7440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   29
         Left            =   240
         TabIndex        =   33
         Top             =   7200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   28
         Left            =   240
         TabIndex        =   32
         Top             =   6960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   27
         Left            =   240
         TabIndex        =   31
         Top             =   6720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   26
         Left            =   240
         TabIndex        =   30
         Top             =   6480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   25
         Left            =   240
         TabIndex        =   29
         Top             =   6240
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   24
         Left            =   240
         TabIndex        =   28
         Top             =   6000
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   23
         Left            =   240
         TabIndex        =   27
         Top             =   5760
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   22
         Left            =   240
         TabIndex        =   26
         Top             =   5520
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   21
         Left            =   240
         TabIndex        =   25
         Top             =   5280
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   20
         Left            =   240
         TabIndex        =   24
         Top             =   5040
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   19
         Left            =   240
         TabIndex        =   23
         Top             =   4800
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   18
         Left            =   240
         TabIndex        =   22
         Top             =   4560
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   17
         Left            =   240
         TabIndex        =   21
         Top             =   4320
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   16
         Left            =   240
         TabIndex        =   20
         Top             =   4080
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   15
         Left            =   240
         TabIndex        =   19
         Top             =   3840
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   14
         Left            =   240
         TabIndex        =   18
         Top             =   3600
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   13
         Left            =   240
         TabIndex        =   17
         Top             =   3360
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   12
         Left            =   240
         TabIndex        =   16
         Top             =   3120
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   11
         Left            =   240
         TabIndex        =   15
         Top             =   2880
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   14
         Top             =   2640
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   13
         Top             =   2400
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   12
         Top             =   2160
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   11
         Top             =   1920
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   10
         Top             =   1680
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   9
         Top             =   1440
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   8
         Top             =   1200
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   7
         Top             =   960
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   6
         Top             =   720
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton SDPB_PortA 
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   5
         Top             =   480
         Width           =   255
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "450;450"
         Value           =   "0"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Label8 
         Caption         =   "Port A"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label7 
         Caption         =   "Port B"
         Height          =   255
         Left            =   840
         TabIndex        =   3
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "Port C"
         Height          =   255
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Port D"
         Height          =   255
         Left            =   2280
         TabIndex        =   1
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   12
      Left            =   840
      TabIndex        =   201
      Top             =   4680
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   12
         ItemData        =   "SS7_Form.frx":00C0
         Left            =   3120
         List            =   "SS7_Form.frx":00C2
         TabIndex        =   205
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   12
         ItemData        =   "SS7_Form.frx":00C4
         Left            =   2280
         List            =   "SS7_Form.frx":00C6
         TabIndex        =   204
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   12
         ItemData        =   "SS7_Form.frx":00C8
         Left            =   1440
         List            =   "SS7_Form.frx":00CA
         TabIndex        =   203
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   12
         ItemData        =   "SS7_Form.frx":00CC
         Left            =   600
         List            =   "SS7_Form.frx":00CE
         TabIndex        =   202
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   206
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   13
      Left            =   840
      TabIndex        =   207
      Top             =   5040
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   13
         ItemData        =   "SS7_Form.frx":00D0
         Left            =   3120
         List            =   "SS7_Form.frx":00D2
         TabIndex        =   211
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   13
         ItemData        =   "SS7_Form.frx":00D4
         Left            =   2280
         List            =   "SS7_Form.frx":00D6
         TabIndex        =   210
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   13
         ItemData        =   "SS7_Form.frx":00D8
         Left            =   1440
         List            =   "SS7_Form.frx":00DA
         TabIndex        =   209
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   13
         ItemData        =   "SS7_Form.frx":00DC
         Left            =   600
         List            =   "SS7_Form.frx":00DE
         TabIndex        =   208
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   212
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   14
      Left            =   840
      TabIndex        =   213
      Top             =   5400
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   14
         ItemData        =   "SS7_Form.frx":00E0
         Left            =   3120
         List            =   "SS7_Form.frx":00E2
         TabIndex        =   217
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   14
         ItemData        =   "SS7_Form.frx":00E4
         Left            =   2280
         List            =   "SS7_Form.frx":00E6
         TabIndex        =   216
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   14
         ItemData        =   "SS7_Form.frx":00E8
         Left            =   1440
         List            =   "SS7_Form.frx":00EA
         TabIndex        =   215
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   14
         ItemData        =   "SS7_Form.frx":00EC
         Left            =   600
         List            =   "SS7_Form.frx":00EE
         TabIndex        =   214
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   218
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   15
      Left            =   840
      TabIndex        =   219
      Top             =   5760
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   15
         ItemData        =   "SS7_Form.frx":00F0
         Left            =   3120
         List            =   "SS7_Form.frx":00F2
         TabIndex        =   223
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   15
         ItemData        =   "SS7_Form.frx":00F4
         Left            =   2280
         List            =   "SS7_Form.frx":00F6
         TabIndex        =   222
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   15
         ItemData        =   "SS7_Form.frx":00F8
         Left            =   1440
         List            =   "SS7_Form.frx":00FA
         TabIndex        =   221
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   15
         ItemData        =   "SS7_Form.frx":00FC
         Left            =   600
         List            =   "SS7_Form.frx":00FE
         TabIndex        =   220
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   224
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   16
      Left            =   840
      TabIndex        =   225
      Top             =   6120
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   16
         ItemData        =   "SS7_Form.frx":0100
         Left            =   600
         List            =   "SS7_Form.frx":0102
         TabIndex        =   229
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   16
         ItemData        =   "SS7_Form.frx":0104
         Left            =   1440
         List            =   "SS7_Form.frx":0106
         TabIndex        =   228
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   16
         ItemData        =   "SS7_Form.frx":0108
         Left            =   2280
         List            =   "SS7_Form.frx":010A
         TabIndex        =   227
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   16
         ItemData        =   "SS7_Form.frx":010C
         Left            =   3120
         List            =   "SS7_Form.frx":010E
         TabIndex        =   226
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   16
         Left            =   120
         TabIndex        =   230
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   17
      Left            =   840
      TabIndex        =   231
      Top             =   6480
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   17
         ItemData        =   "SS7_Form.frx":0110
         Left            =   3120
         List            =   "SS7_Form.frx":0112
         TabIndex        =   235
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   17
         ItemData        =   "SS7_Form.frx":0114
         Left            =   2280
         List            =   "SS7_Form.frx":0116
         TabIndex        =   234
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   17
         ItemData        =   "SS7_Form.frx":0118
         Left            =   1440
         List            =   "SS7_Form.frx":011A
         TabIndex        =   233
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   17
         ItemData        =   "SS7_Form.frx":011C
         Left            =   600
         List            =   "SS7_Form.frx":011E
         TabIndex        =   232
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   236
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   18
      Left            =   840
      TabIndex        =   237
      Top             =   6840
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   18
         ItemData        =   "SS7_Form.frx":0120
         Left            =   3120
         List            =   "SS7_Form.frx":0122
         TabIndex        =   241
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   18
         ItemData        =   "SS7_Form.frx":0124
         Left            =   2280
         List            =   "SS7_Form.frx":0126
         TabIndex        =   240
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   18
         ItemData        =   "SS7_Form.frx":0128
         Left            =   1440
         List            =   "SS7_Form.frx":012A
         TabIndex        =   239
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   18
         ItemData        =   "SS7_Form.frx":012C
         Left            =   600
         List            =   "SS7_Form.frx":012E
         TabIndex        =   238
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   242
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   19
      Left            =   840
      TabIndex        =   243
      Top             =   7200
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   19
         ItemData        =   "SS7_Form.frx":0130
         Left            =   3120
         List            =   "SS7_Form.frx":0132
         TabIndex        =   247
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   19
         ItemData        =   "SS7_Form.frx":0134
         Left            =   2280
         List            =   "SS7_Form.frx":0136
         TabIndex        =   246
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   19
         ItemData        =   "SS7_Form.frx":0138
         Left            =   1440
         List            =   "SS7_Form.frx":013A
         TabIndex        =   245
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   19
         ItemData        =   "SS7_Form.frx":013C
         Left            =   600
         List            =   "SS7_Form.frx":013E
         TabIndex        =   244
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   248
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   20
      Left            =   840
      TabIndex        =   249
      Top             =   7560
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   20
         ItemData        =   "SS7_Form.frx":0140
         Left            =   3120
         List            =   "SS7_Form.frx":0142
         TabIndex        =   253
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   20
         ItemData        =   "SS7_Form.frx":0144
         Left            =   2280
         List            =   "SS7_Form.frx":0146
         TabIndex        =   252
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   20
         ItemData        =   "SS7_Form.frx":0148
         Left            =   1440
         List            =   "SS7_Form.frx":014A
         TabIndex        =   251
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   20
         ItemData        =   "SS7_Form.frx":014C
         Left            =   600
         List            =   "SS7_Form.frx":014E
         TabIndex        =   250
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   20
         Left            =   120
         TabIndex        =   254
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   21
      Left            =   840
      TabIndex        =   255
      Top             =   7920
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   21
         ItemData        =   "SS7_Form.frx":0150
         Left            =   600
         List            =   "SS7_Form.frx":0152
         TabIndex        =   259
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   21
         ItemData        =   "SS7_Form.frx":0154
         Left            =   1440
         List            =   "SS7_Form.frx":0156
         TabIndex        =   258
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   21
         ItemData        =   "SS7_Form.frx":0158
         Left            =   2280
         List            =   "SS7_Form.frx":015A
         TabIndex        =   257
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   21
         ItemData        =   "SS7_Form.frx":015C
         Left            =   3120
         List            =   "SS7_Form.frx":015E
         TabIndex        =   256
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   21
         Left            =   120
         TabIndex        =   260
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   22
      Left            =   840
      TabIndex        =   261
      Top             =   8280
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   22
         ItemData        =   "SS7_Form.frx":0160
         Left            =   3120
         List            =   "SS7_Form.frx":0162
         TabIndex        =   265
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   22
         ItemData        =   "SS7_Form.frx":0164
         Left            =   2280
         List            =   "SS7_Form.frx":0166
         TabIndex        =   264
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   22
         ItemData        =   "SS7_Form.frx":0168
         Left            =   1440
         List            =   "SS7_Form.frx":016A
         TabIndex        =   263
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   22
         ItemData        =   "SS7_Form.frx":016C
         Left            =   600
         List            =   "SS7_Form.frx":016E
         TabIndex        =   262
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   22
         Left            =   120
         TabIndex        =   266
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   23
      Left            =   840
      TabIndex        =   267
      Top             =   8640
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   23
         ItemData        =   "SS7_Form.frx":0170
         Left            =   3120
         List            =   "SS7_Form.frx":0172
         TabIndex        =   271
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   23
         ItemData        =   "SS7_Form.frx":0174
         Left            =   2280
         List            =   "SS7_Form.frx":0176
         TabIndex        =   270
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   23
         ItemData        =   "SS7_Form.frx":0178
         Left            =   1440
         List            =   "SS7_Form.frx":017A
         TabIndex        =   269
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   23
         ItemData        =   "SS7_Form.frx":017C
         Left            =   600
         List            =   "SS7_Form.frx":017E
         TabIndex        =   268
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   23
         Left            =   120
         TabIndex        =   272
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   24
      Left            =   840
      TabIndex        =   273
      Top             =   9000
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   24
         ItemData        =   "SS7_Form.frx":0180
         Left            =   3120
         List            =   "SS7_Form.frx":0182
         TabIndex        =   277
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   24
         ItemData        =   "SS7_Form.frx":0184
         Left            =   2280
         List            =   "SS7_Form.frx":0186
         TabIndex        =   276
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   24
         ItemData        =   "SS7_Form.frx":0188
         Left            =   1440
         List            =   "SS7_Form.frx":018A
         TabIndex        =   275
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   24
         ItemData        =   "SS7_Form.frx":018C
         Left            =   600
         List            =   "SS7_Form.frx":018E
         TabIndex        =   274
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   24
         Left            =   120
         TabIndex        =   278
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   25
      Left            =   840
      TabIndex        =   279
      Top             =   9360
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   25
         ItemData        =   "SS7_Form.frx":0190
         Left            =   3120
         List            =   "SS7_Form.frx":0192
         TabIndex        =   283
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   25
         ItemData        =   "SS7_Form.frx":0194
         Left            =   2280
         List            =   "SS7_Form.frx":0196
         TabIndex        =   282
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   25
         ItemData        =   "SS7_Form.frx":0198
         Left            =   1440
         List            =   "SS7_Form.frx":019A
         TabIndex        =   281
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   25
         ItemData        =   "SS7_Form.frx":019C
         Left            =   600
         List            =   "SS7_Form.frx":019E
         TabIndex        =   280
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   25
         Left            =   120
         TabIndex        =   284
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   26
      Left            =   840
      TabIndex        =   285
      Top             =   9720
      Width           =   3855
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   26
         ItemData        =   "SS7_Form.frx":01A0
         Left            =   600
         List            =   "SS7_Form.frx":01A2
         TabIndex        =   289
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   26
         ItemData        =   "SS7_Form.frx":01A4
         Left            =   1440
         List            =   "SS7_Form.frx":01A6
         TabIndex        =   288
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   26
         ItemData        =   "SS7_Form.frx":01A8
         Left            =   2280
         List            =   "SS7_Form.frx":01AA
         TabIndex        =   287
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   26
         ItemData        =   "SS7_Form.frx":01AC
         Left            =   3120
         List            =   "SS7_Form.frx":01AE
         TabIndex        =   286
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   26
         Left            =   120
         TabIndex        =   290
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   27
      Left            =   840
      TabIndex        =   291
      Top             =   10080
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   27
         ItemData        =   "SS7_Form.frx":01B0
         Left            =   3120
         List            =   "SS7_Form.frx":01B2
         TabIndex        =   295
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   27
         ItemData        =   "SS7_Form.frx":01B4
         Left            =   2280
         List            =   "SS7_Form.frx":01B6
         TabIndex        =   294
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   27
         ItemData        =   "SS7_Form.frx":01B8
         Left            =   1440
         List            =   "SS7_Form.frx":01BA
         TabIndex        =   293
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   27
         ItemData        =   "SS7_Form.frx":01BC
         Left            =   600
         List            =   "SS7_Form.frx":01BE
         TabIndex        =   292
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   27
         Left            =   120
         TabIndex        =   296
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   28
      Left            =   840
      TabIndex        =   297
      Top             =   10440
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   28
         ItemData        =   "SS7_Form.frx":01C0
         Left            =   3120
         List            =   "SS7_Form.frx":01C2
         TabIndex        =   301
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   28
         ItemData        =   "SS7_Form.frx":01C4
         Left            =   2280
         List            =   "SS7_Form.frx":01C6
         TabIndex        =   300
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   28
         ItemData        =   "SS7_Form.frx":01C8
         Left            =   1440
         List            =   "SS7_Form.frx":01CA
         TabIndex        =   299
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   28
         ItemData        =   "SS7_Form.frx":01CC
         Left            =   600
         List            =   "SS7_Form.frx":01CE
         TabIndex        =   298
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   28
         Left            =   120
         TabIndex        =   302
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   29
      Left            =   840
      TabIndex        =   303
      Top             =   10800
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   29
         ItemData        =   "SS7_Form.frx":01D0
         Left            =   3120
         List            =   "SS7_Form.frx":01D2
         TabIndex        =   307
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   29
         ItemData        =   "SS7_Form.frx":01D4
         Left            =   2280
         List            =   "SS7_Form.frx":01D6
         TabIndex        =   306
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   29
         ItemData        =   "SS7_Form.frx":01D8
         Left            =   1440
         List            =   "SS7_Form.frx":01DA
         TabIndex        =   305
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   29
         ItemData        =   "SS7_Form.frx":01DC
         Left            =   600
         List            =   "SS7_Form.frx":01DE
         TabIndex        =   304
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   29
         Left            =   120
         TabIndex        =   308
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Index           =   30
      Left            =   840
      TabIndex        =   309
      Top             =   11160
      Width           =   3855
      Begin VB.ComboBox SDPA_PortD 
         Height          =   315
         Index           =   30
         ItemData        =   "SS7_Form.frx":01E0
         Left            =   3120
         List            =   "SS7_Form.frx":01E2
         TabIndex        =   313
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortC 
         Height          =   315
         Index           =   30
         ItemData        =   "SS7_Form.frx":01E4
         Left            =   2280
         List            =   "SS7_Form.frx":01E6
         TabIndex        =   312
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortB 
         Height          =   315
         Index           =   30
         ItemData        =   "SS7_Form.frx":01E8
         Left            =   1440
         List            =   "SS7_Form.frx":01EA
         TabIndex        =   311
         Top             =   120
         Width           =   560
      End
      Begin VB.ComboBox SDPA_PortA 
         Height          =   315
         Index           =   30
         ItemData        =   "SS7_Form.frx":01EC
         Left            =   600
         List            =   "SS7_Form.frx":01EE
         TabIndex        =   310
         Top             =   120
         Width           =   560
      End
      Begin VB.Label SDL 
         Caption         =   "2"
         Height          =   255
         Index           =   30
         Left            =   120
         TabIndex        =   314
         Top             =   170
         Width           =   255
      End
   End
   Begin VB.Label Label4 
      Caption         =   "Port D"
      Height          =   255
      Left            =   3960
      TabIndex        =   318
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Port C"
      Height          =   255
      Left            =   3120
      TabIndex        =   317
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Port B"
      Height          =   255
      Left            =   2280
      TabIndex        =   316
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Port A"
      Height          =   255
      Left            =   1440
      TabIndex        =   315
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "SS7_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim SDPA_PortA_TS(31), SDPA_PortB_TS(31), SDPA_PortC_TS(31), SDPA_PortD_TS(31) As Integer
Dim SDPB_PortA_TS(31), SDPB_PortB_TS(31), SDPB_PortC_TS(31), SDPB_PortD_TS(31) As Integer

Private Sub Form_Load()
    For i = 0 To SDL.Count - 1
        SDL(i) = i + 1
        'SDL2(i) = i + 1
    Next i
    
    'Initilalizing Time Slots
    For i = 0 To 30
        SDPA_PortA_TS(i) = i + 1
        SDPA_PortB_TS(i) = i + 1
        SDPA_PortC_TS(i) = i + 1
        SDPA_PortD_TS(i) = i + 1
        For j = 0 To 30
            SDPA_PortA(i).AddItem j + 1
            SDPA_PortB(i).AddItem j + 1
            SDPA_PortC(i).AddItem j + 1
            SDPA_PortD(i).AddItem j + 1
        Next j
    Next i
    
End Sub
Private Sub SDL_Pressed(Except As String, Index As Integer, SDP As String, Pressed As Boolean)
    Select Case UCase(SDP)
        Case "A", "SDPA"
            If Pressed Then
                If Except <> "SDPA_PortA" Then SDPA_PortA(Index).Enabled = False
                If Except <> "SDPA_PortB" Then SDPA_PortB(Index).Enabled = False
                If Except <> "SDPA_PortC" Then SDPA_PortC(Index).Enabled = False
                If Except <> "SDPA_PortD" Then SDPA_PortD(Index).Enabled = False
            Else
                If Except <> "SDPA_PortA" Then SDPA_PortA(Index).Enabled = True
                If Except <> "SDPA_PortB" Then SDPA_PortB(Index).Enabled = True
                If Except <> "SDPA_PortC" Then SDPA_PortC(Index).Enabled = True
                If Except <> "SDPA_PortD" Then SDPA_PortD(Index).Enabled = True
            End If
        Case "B", "SDPB"
            If Pressed Then
                If Except <> "SDPB_PortA" Then SDPB_PortA(Index).Enabled = False
                If Except <> "SDPB_PortB" Then SDPB_PortB(Index).Enabled = False
                If Except <> "SDPB_PortC" Then SDPB_PortC(Index).Enabled = False
                If Except <> "SDPB_PortD" Then SDPB_PortD(Index).Enabled = False
            Else
                If Except <> "SDPB_PortA" Then SDPB_PortA(Index).Enabled = True
                If Except <> "SDPB_PortB" Then SDPB_PortB(Index).Enabled = True
                If Except <> "SDPB_PortC" Then SDPB_PortC(Index).Enabled = True
                If Except <> "SDPB_PortD" Then SDPB_PortD(Index).Enabled = True
            End If
        End Select
End Sub

Private Sub SDPA_PortA_Click(Index As Integer)
    'Call SDL_Pressed(SDPA_PortA(Index).Name, Index, "SDPA", True)
    
    'If SDPA_PortA(Index).Value Then
    '    SDPA_PortA_TS = SDPA_PortA_TS + 1
    '    SDPA_PortA(Index).Caption = SDPA_PortA_TS
    'Else
    '
    'End If
End Sub

Private Sub SDPA_PortB_Click(Index As Integer)
    'Call SDL_Pressed(SDPA_PortB(Index).Name, Index, "SDPA", SDPA_PortB(Index).Value)
End Sub


