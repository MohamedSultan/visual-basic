Attribute VB_Name = "Functions"
Public N1_txt As String
Public N2_txt As String
Public N3_txt As String
Public N4_txt As String
Public LinkSets_Txt As String
Public Routes_txt As String
Public RouteSets_Txt As String

Public N1() As String
Public N2() As String
Public N3() As String
Public N4() As String
Public LinkSets() As String
Public Routes() As String
Public RouteSets() As String

Public PCs_N1() As String
Public PCs_N2() As String
Public PCs_Link_Sets() As String
Public PCs_Route_Sets() As String
Public found As Boolean
Public Step As Integer
Public Number_Of_Errors, Number_Of_Warnings As Integer
Public Sub Convert_Format(FileName As String)
    Dim SS7FileName As Double
    Dim Temp As String
    Dim Sultan As Parser_Return
    SS7FileName = FreeFile
    Open FileName For Input As SS7FileName
    Do Until EOF(SS7FileName)
            Line Input #SS7FileName, Temp
            Sultan = Parser(Temp, Chr(10))
        Loop
    Close SS7FileName
    SS7_Paths = FreeFile
    Open App.Path + "\Temp.txt" For Output As SS7_Paths
    For i = 0 To UBound(Sultan.Ret)
        Print #SS7_Paths, Sultan.Ret(i)
    Next i
    Close SS7_Paths
    
End Sub
Public Sub Reset_All_Vars()
    N1_txt = ""
    N2_txt = ""
    N3_txt = ""
    N4_txt = ""
    LinkSets_Txt = ""
    Routes_txt = ""
    RouteSets_Txt = ""
    
    Number_Of_Errors = 0
    Number_Of_Warnings = 0
    found = False
    Step = 0

    Erase N1
    Erase N2
    Erase N3
    Erase N4
    Erase LinkSets
    Erase Routes
    Erase RouteSets
    
    Erase PCs_N1
    Erase PCs_N2
    Erase PCs_Link_Sets
    Erase PCs_Route_Sets
    
    With Result
        .Report_List.Clear
        .Errors = 0
        .Warnings = 0
    End With
    
End Sub
Public Function ID_Space(ByRef Line As String) As String
    Dim Chr As Integer
    Dim Ret_Txt As String
    'Data Type
    '------------------------------------------------------
    Chr = 1
    If Len(Line) <> 1 And Len(Line) <> 0 Then
        While Mid(Line, Chr, 1) <> " " And Asc(Mid(Line, Chr, 1)) <> 9
            Ret_Txt = Ret_Txt & Trim(Mid(Line, Chr, 1))
            Chr = Chr + 1
        Wend
    Else
        Ret_Txt = Line
    End If
    ID_Space = Trim(Int(Val(Ret_Txt)))
End Function
Public Function Sort(ByVal Array_ As Variant) As Variant
    Dim Sorted_Array(), Max As String
    Dim Max_Pos As Integer
    
    For i = 1 To UBound(Array_)
        'Getting max value and positioin
        For j = 1 To UBound(Array_)
            If Val(Max) <= Val(Array_(j)) Then
                Max = Val(Array_(j))
                Max_Pos = j
            End If
        Next j
        
        ReDim Preserve Sorted_Array(i)
        Sorted_Array(i) = Max
        Max = "0"
        Array_(Max_Pos) = 0
    Next i
    Sort = Sorted_Array
End Function
