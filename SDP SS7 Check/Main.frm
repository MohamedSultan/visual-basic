VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Main_Form 
   Caption         =   "SS7 Checker"
   ClientHeight    =   7920
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   6930
   LinkTopic       =   "Form1"
   ScaleHeight     =   7920
   ScaleWidth      =   6930
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog SetPath 
      Left            =   6240
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Open_File_Command 
      Caption         =   "open File"
      Height          =   315
      Left            =   5640
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
   Begin VB.ComboBox Path_Combo 
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   5415
   End
   Begin VB.ListBox Check_Display 
      ForeColor       =   &H00000000&
      Height          =   6690
      ItemData        =   "Main.frx":0000
      Left            =   120
      List            =   "Main.frx":0002
      TabIndex        =   1
      Top             =   840
      Width           =   6615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Start SS7 Checks"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   6615
   End
   Begin VB.Menu Test 
      Caption         =   "Test"
      Index           =   1
   End
   Begin VB.Menu About 
      Caption         =   "About"
      Index           =   0
   End
End
Attribute VB_Name = "Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Display(ByVal Data As String)
    Check_Display.AddItem Data
    Check_Display.ListIndex = (Check_Display.ListCount - 1)
End Sub
Public Sub Status(Title As String, Error_Rased As Boolean, Optional Error_Display As String)
    If Error_Rased Then
        If InStr(1, Error_Display, "Current SLC not in Serial") <> 0 Then
            'Number_Of_Warnings = Number_Of_Warnings + 1
            Result.Warnings = Val(Result.Warnings) + 1
        Else
            'Number_Of_Errors = Number_Of_Errors + 1
            Result.Errors = Val(Result.Errors) + 1
        End If
        Result.Report_List.AddItem Title + Gen_Pts(88 - Len(Title) - Len(Error_Display)) + Error_Display
        Display (Title + Gen_Pts(88 - Len(Title) - Len(Error_Display)) + Error_Display)
    Else
        Display (Title + Gen_Pts(88 - Len(Title) - Len("Passed")) + "Passed")
    End If
End Sub
Private Function Gen_Pts(Number_Of_Points As Integer) As String
    If Number_Of_Points > 0 Then
        For i = 1 To Number_Of_Points
            If Len(Gen_Pts) < 50 Then Gen_Pts = Gen_Pts + "."
        Next i
    End If
End Function
Private Sub Check_Data()
    
    Check_Display.AddItem "N1 Checks Started"
    Check_Display.AddItem "---------------------------------"

    Call N1_Checks
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    
    Check_Display.AddItem "N2 Checks Started"
    Check_Display.AddItem "---------------------------------"
    
    Call N2_Checks
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    
    'If UBound(N3) <> N3(0) * 1 Then MsgBox Error
    'If UBound(N4) <> N4(0) * 1 Then MsgBox Error
    
    Check_Display.AddItem "LinkSets Checks Started"
    Check_Display.AddItem "---------------------------------"
    
    Call LinkSets_Checks
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    
    Check_Display.AddItem "Routes Checks Started"
    Check_Display.AddItem "---------------------------------"
    

    Call Route_Checks
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"
    Check_Display.AddItem "----------------------------------------------------------------------------------------------------------------------------"

    Check_Display.AddItem "Routesets Checks Started"
    Check_Display.AddItem "---------------------------------"
    
    Call RouteSets_Checks

    
End Sub

Private Sub Parse_Data(ByRef New_Data As Variant, ByRef Old_Data As String)
    Dim Parsed_data As Parser_Return
    Dim Number_Of_Records As Integer
    Dim Temp As String
    
    Parsed_data = Parser(Old_Data, vbCrLf)

    Temp = Parsed_data.Ret(0)
    Number_Of_Records = ID_Space(Temp)

    ReDim Preserve New_Data(UBound(Parsed_data.Ret) - 1)
    New_Data(0) = Str(Number_Of_Records)
    
    If Number_Of_Records > 0 Then
        
        'Creating Data Type
        For i = 1 To UBound(Parsed_data.Ret) - 1
            New_Data(i) = Parsed_data.Ret(i)
        Next i
    End If
End Sub
Private Function Open_File(FileName As String) As Boolean
    Dim SS7FileName As Double
    Dim Temp As String
    On Error GoTo Local_ErrorHandler
    SS7FileName = FreeFile
    Open_File = True
    Open FileName For Input As SS7FileName
        Do Until EOF(SS7FileName)
            Line Input #SS7FileName, Temp
            If InStr(1, LCase(Temp), "n1") <> 0 Then
                N1_txt = Temp
                Do Until InStr(1, LCase(Temp), "n2") <> 0
                    Line Input #SS7FileName, Temp
                    N1_txt = N1_txt + vbCrLf + Temp
                Loop
            End If
            If InStr(1, LCase(Temp), "n2") <> 0 Then
                N2_txt = Temp
                Do Until InStr(1, LCase(Temp), "n3") <> 0
                    Line Input #SS7FileName, Temp
                    N2_txt = N2_txt + vbCrLf + Temp
                Loop
            End If
            If InStr(1, LCase(Temp), "n3") <> 0 Then
                N3_txt = Temp
                Do Until InStr(1, LCase(Temp), "n4") <> 0
                    Line Input #SS7FileName, Temp
                    N3_txt = N3_txt + vbCrLf + Temp
                Loop
            End If
            If InStr(1, LCase(Temp), "n4") <> 0 Then
                N4_txt = Temp
                Do Until Left(Temp, 2) = "CA"
                    Line Input #SS7FileName, Temp
                    N4_txt = N4_txt + vbCrLf + Temp
                Loop
            End If
            If InStr(1, Temp, "209") Then
                X = 1
            End If
            If InStr(1, LCase(Temp), "number of link sets") <> 0 Or InStr(1, LCase(Temp), "number of linksets") <> 0 Then
                LinkSets_Txt = Temp
                Do Until InStr(1, LCase(Temp), "number of routes") <> 0
                    Line Input #SS7FileName, Temp
                    LinkSets_Txt = LinkSets_Txt + vbCrLf + Temp
                Loop
            End If
            If InStr(1, LCase(Temp), "number of routes") <> 0 Then
                Routes_txt = Temp
                Do Until InStr(1, LCase(Temp), "number of routesets") <> 0
                    Line Input #SS7FileName, Temp
                    Routes_txt = Routes_txt + vbCrLf + Temp
                Loop
            End If
            If InStr(1, LCase(Temp), "number of routesets") <> 0 Then
                RouteSets_Txt = Temp
                Do Until InStr(1, LCase(Temp), "back-end") <> 0
                    Line Input #SS7FileName, Temp
                    RouteSets_Txt = RouteSets_Txt + vbCrLf + Temp
                Loop
            End If
        Loop
    Close SS7FileName
    'Dim Parser_ret As Parser_Return
    'Parser_ret = Parser(Temp, vbTab)
Local_ErrorHandler:
    If Err.Number = 62 Then
        Dim Format_File As Integer
        Format_File = MsgBox("PLZ reformat the File In DOS Format", vbYesNoCancel, "File Format Error")
        Select Case Format_File
            Case vbYes
                Call Convert_Format(Path_Combo.Text)
                Call Open_File(App.Path + "\Temp.txt")
            Case vbNo
                Open_File = False
            Case vbCancel
                Open_File = False
        End Select
    Else
        Open_File = False
        If Err.Number <> 0 Then
            Call MsgBox("Error: " + Err.Description, vbCritical, "Error:" + Str(Err.Number))
        Else
            Open_File = True
        End If
       ' Call Err.Raise(Err.Number, Err.Source, Err.Description)
    End If
End Function
Private Sub Get_Paths()
    Dim SS7_Paths As Double
    SS7_Paths = FreeFile
    Dim Paths() As String
    Step = 0
    Path_Combo.Clear
    On Error GoTo Local_Handler
    Open App.Path + "\Paths.txt" For Input As SS7_Paths
        Do Until EOF(SS7_Paths)
            ReDim Preserve Paths(Step)
            Line Input #SS7_Paths, Paths(Step)
            Step = Step + 1
        Loop
    Close SS7_Paths
    
    For i = 0 To UBound(Paths)
        Path_Combo.AddItem Paths(i)
    Next i
    Path_Combo.ListIndex = 0
    
Local_Handler:
    If Err.Number = 53 Then
        SS7_Paths = FreeFile
        Open App.Path + "\Paths.txt" For Output As SS7_Paths
        Close SS7_Paths
    End If
End Sub

Private Sub About_Click(Index As Integer)
    MsgBox "This Program is Developed By Mohamed Sultan" + _
            vbCrLf + "Senior IN Charging" + _
            vbCrLf + "VAS, Network Development" + _
            vbCrLf + "25 Jul 2008", , "About SS7 Checker"
End Sub

Private Sub Command1_Click()
    Dim Correct_Format As Boolean
    'On Error Resume Next
    Call Reset_All_Vars
    Check_Display = ""
    Result.Visible = False
    Call Check_FileName(Path_Combo.Text)
    Correct_Format = Open_File(Path_Combo.Text)
    If Correct_Format Then
        Call Parse_Data(N1, N1_txt)
        Call Parse_Data(N2, N2_txt)
        Call Parse_Data(N3, N3_txt)
        Call Parse_Data(N4, N4_txt)
        Call Parse_Data(LinkSets, LinkSets_Txt)
        Call Parse_Data(Routes, Routes_txt)
        Call Parse_Data(RouteSets, RouteSets_Txt)
        
        Call Check_Data
       
        Result.Left = Main_Form.Left + Command1.Left
        Result.Top = Main_Form.Top + Command1.Height + Command1.Top + Path_Combo.Top + Path_Combo.Height
        Result.Show
    End If
End Sub
Private Sub Check_FileName(ByVal FileName As String)
'This is used to check whether the the file name exists in the combo list or not and add in case of not exist
    found = False
    Dim Paths() As String
    ReDim Preserve Paths(0)
    For i = 0 To Path_Combo.ListCount - 1
        If FileName = Path_Combo.List(i) Then
            found = True
            Paths(0) = FileName
        Else
            ReDim Preserve Paths(i + 1)
            Paths(i + 1) = Path_Combo.List(i)
        End If
    Next i
    If Not found Then
        Paths(0) = FileName
    Else
        
    End If
        Call Save_Paths(Paths)
        Call Get_Paths
End Sub
Private Sub Save_Paths(ByVal Paths As Variant)
    Dim SS7_Paths As Double
    SS7_Paths = FreeFile
    Open App.Path + "\Paths.txt" For Output As SS7_Paths
        For i = 0 To UBound(Paths)
            If Paths(i) <> "" Then Print #SS7_Paths, Paths(i)
        Next i
    Close SS7_Paths
End Sub

Private Sub Form_Load()
    Call Get_Paths
    Result.Height = 1275
End Sub
Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Open_File_Command_Click()
    Dim Directory As String
    Directory = Get_Dir(Path_Combo.Text)
    SetPath.InitDir = Directory
    SetPath.CancelError = False
    SetPath.Filter = "All Files (*.*)|*.*|Configuration Files(*.cnf)|*.cnf|Text Files(*.txt)|*.txt"
    SetPath.FilterIndex = 2
    SetPath.ShowOpen
    If SetPath.FileName <> "" Then Path_Combo.Text = SetPath.FileName
End Sub

Private Sub Path_Combo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Command1_Click
End Sub

Private Sub Test_Click(Index As Integer)
    Test_Form.Show
End Sub
