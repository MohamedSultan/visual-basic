VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form Result 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Full Report"
   ClientHeight    =   870
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8355
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   870
   ScaleWidth      =   8355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Top             =   360
      Width           =   975
   End
   Begin VB.ListBox Report_List 
      Height          =   1035
      Left            =   240
      TabIndex        =   4
      Top             =   960
      Width           =   8055
   End
   Begin MSForms.ToggleButton Details 
      Height          =   375
      Left            =   3600
      TabIndex        =   5
      Top             =   360
      Width           =   975
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "1720;661"
      Value           =   "0"
      Caption         =   "Details"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin VB.Label Warnings 
      Caption         =   "1234"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   495
   End
   Begin VB.Label Errors 
      Caption         =   "1234"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "Warnings Occured"
      Height          =   375
      Left            =   735
      TabIndex        =   1
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   " Error(s) Occured"
      Height          =   495
      Left            =   735
      TabIndex        =   0
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "Result"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const Form_Small = 1230
Const Form_Tall = 2475

Private Sub Details_Click()
    If Not Details.Value = False Then
        Me.Height = Form_Tall
    Else
        Me.Height = Form_Small
    End If
End Sub

Private Sub OK_Click()
    Me.Hide
    Me.Height = Form_Small
    Details.Value = False
End Sub
