VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stop Watch 2"
   ClientHeight    =   2985
   ClientLeft      =   7635
   ClientTop       =   3600
   ClientWidth     =   5775
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   5775
   Begin VB.CommandButton Pause 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4080
      TabIndex        =   14
      Top             =   2160
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1680
      TabIndex        =   13
      Text            =   "Write Here What U want to be Save"
      Top             =   2400
      Width           =   2175
   End
   Begin VB.TextBox History 
      Height          =   1095
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   12
      Top             =   960
      Width           =   5535
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   3600
      TabIndex        =   11
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   2880
      TabIndex        =   10
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   2160
      TabIndex        =   9
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   480
      Top             =   1320
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   1440
      TabIndex        =   1
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.CommandButton Start_ 
      Caption         =   "Start/Restart"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   2160
      Width           =   1335
   End
   Begin VB.Label Label9 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4320
      TabIndex        =   16
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   8
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label7 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3360
      TabIndex        =   7
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   6
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label5 
      Caption         =   "Milli Sec"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3600
      TabIndex        =   5
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "Sec"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2880
      TabIndex        =   4
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Min"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   3
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Hour"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1440
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Hour_, Minute_, Sec_, MilliSec_, Step As Integer
Dim Time_Now, Time_Diff As Date

Private Sub Pause_Click()
    Append
    Timer1.Enabled = True
End Sub

Private Sub Start__Click()
    Append
    Time_Now = Time
    Label1.Caption = "Start Time: " + vbCrLf + vbCrLf + Str(Time_Now)
    Timer1.Enabled = True
End Sub
Private Sub Append()
    If Not (Hour_ = 0 And Minute_ = 0 And Sec_ = 0 And MilliSec_ = 0) Then _
        History = "Step: " + Zero(Step) + "    " + Text5(0) + " : " + Text5(1) + " : " + Text5(2) + " : " + Text5(3) + "      " + Text1 + vbCrLf + History
    Step = Step + 1
End Sub

Private Sub Text1_Click()
Text1 = ""
End Sub

Private Sub Timer1_Timer()
    MilliSec_ = MilliSec_ + 1
    If MilliSec_ = 100 Then MilliSec_ = 0
    Display
End Sub

Private Sub Display()
    Label9.Caption = "Current Time: " + vbCrLf + vbCrLf + Str(Time)
    Time_Diff = Time - Time_Now
    Text5(0) = Zero(Hour(Time_Diff))
    Text5(1) = Zero(Minute(Time_Diff))
    Text5(2) = Zero(Second(Time_Diff))
    Text5(3) = Zero(MilliSec_)
End Sub

Private Function Zero(ByVal Input_Number As Integer) As String
    If Input_Number < 10 Then
        Zero = "0" + Trim(Str(Input_Number))
    Else
        Zero = Trim(Str(Input_Number))
    End If
End Function
