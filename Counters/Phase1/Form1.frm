VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stop Watch"
   ClientHeight    =   2985
   ClientLeft      =   7635
   ClientTop       =   3600
   ClientWidth     =   5100
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   5100
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1560
      TabIndex        =   14
      Text            =   "Write Here What U want to be Save"
      Top             =   2400
      Width           =   1815
   End
   Begin VB.CommandButton Pause 
      Caption         =   "Save"
      Height          =   495
      Left            =   3600
      TabIndex        =   13
      Top             =   2280
      Width           =   1215
   End
   Begin VB.TextBox History 
      Height          =   1095
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   12
      Top             =   960
      Width           =   4815
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   3240
      TabIndex        =   11
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   2520
      TabIndex        =   10
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   1800
      TabIndex        =   9
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   120
      Top             =   1320
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   1080
      TabIndex        =   1
      Text            =   "00"
      Top             =   480
      Width           =   375
   End
   Begin VB.CommandButton Start_ 
      Caption         =   "Start/Restart"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   2280
      Width           =   1095
   End
   Begin VB.Label Label8 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   8
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label7 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   7
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   6
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label5 
      Caption         =   "Milli Sec"
      Height          =   255
      Left            =   3240
      TabIndex        =   5
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "Sec"
      Height          =   255
      Left            =   2520
      TabIndex        =   4
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Min"
      Height          =   255
      Left            =   1800
      TabIndex        =   3
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Hour"
      Height          =   255
      Left            =   1080
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Hour_, Minute_, Sec_, MilliSec_, Step As Integer

Private Sub Pause_Click()
    Append
    Timer1.Enabled = True
End Sub

Private Sub Start__Click()
    Append
    
    Hour_ = 0
    Minute_ = 0
    Sec_ = 0
    MilliSec_ = 0
    Timer1.Enabled = True
End Sub
Private Sub Append()
    If Not (Hour_ = 0 And Minute_ = 0 And Sec_ = 0 And MilliSec_ = 0) Then _
        History = "Step: " + Zero(Step) + "    " + Text5(0) + " : " + Text5(1) + " : " + Text5(2) + " : " + Text5(3) + "      " + Text1 + vbCrLf + History
    Step = Step + 1
End Sub

Private Sub Text1_Click()
Text1 = ""
End Sub

Private Sub Text5_Change(Index As Integer)
    Watch
End Sub

Private Sub Watch()
    If Minute_ = 60 Then
        Hour_ = Hour_ + 1
        Minute_ = 0
    End If
    
    If Sec_ = 60 Then
        Minute_ = Minute_ + 1
        Sec_ = 0
    End If
    
    If MilliSec_ = 100 Then
        Sec_ = Sec_ + 1
        MilliSec_ = 0
    End If
    
End Sub

Private Sub Timer1_Timer()
    MilliSec_ = MilliSec_ + 1
    Display
End Sub

Private Sub Display()
    Text5(0) = Zero(Hour_)
    Text5(1) = Zero(Minute_)
    Text5(2) = Zero(Sec_)
    Text5(3) = Zero(MilliSec_)

End Sub

Private Function Zero(ByVal Input_Number As Integer) As String
    If Input_Number < 10 Then
        Zero = "0" + Trim(Str(Input_Number))
    Else
        Zero = Trim(Str(Input_Number))
    End If
End Function
