VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Windows Password"
   ClientHeight    =   2070
   ClientLeft      =   8610
   ClientTop       =   4395
   ClientWidth     =   3855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   3855
   Begin VB.CommandButton Command3 
      Caption         =   "Copy"
      Height          =   255
      Left            =   3360
      TabIndex        =   11
      Top             =   600
      Width           =   495
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Copy"
      Height          =   255
      Left            =   3360
      TabIndex        =   10
      Top             =   1320
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Copy"
      Height          =   255
      Left            =   3360
      TabIndex        =   9
      Top             =   1680
      Width           =   495
   End
   Begin VB.TextBox UserName 
      Height          =   285
      Left            =   1440
      TabIndex        =   7
      Top             =   1320
      Width           =   1815
   End
   Begin VB.CommandButton GetPassword 
      Caption         =   "Get Password"
      Height          =   255
      Left            =   1680
      TabIndex        =   6
      Top             =   960
      Width           =   1215
   End
   Begin VB.TextBox Password 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1440
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   1680
      Width           =   1815
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Another Computer"
      Height          =   255
      Left            =   2040
      TabIndex        =   2
      Top             =   240
      Width           =   1695
   End
   Begin VB.OptionButton Option1 
      Caption         =   "This Computer"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Value           =   -1  'True
      Width           =   1695
   End
   Begin VB.TextBox Computer_Name 
      Height          =   285
      Left            =   1440
      TabIndex        =   0
      Top             =   600
      Width           =   1815
   End
   Begin VB.Label Label3 
      Caption         =   "User Name"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Password"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Computer Name"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Clipboard.Clear
    Clipboard.SetText Password.Text
End Sub

Private Sub Command2_Click()
    Clipboard.Clear
    Clipboard.SetText UserName.Text
End Sub

Private Sub Command3_Click()
    Clipboard.Clear
    Clipboard.SetText Computer_Name.Text
End Sub

Private Sub Form_Load()
    Call Option1_Click
End Sub

Private Sub GetPassword_Click()
    Computer_Name = UCase(Computer_Name)
    UserName = Computer_Name + "\vf_local"
    Password = LCase(Mid(Computer_Name, 3, 4)) + "*" + LCase(Right(Computer_Name, 1)) + UCase(Left(Computer_Name, 2)) + "-4VFe"
End Sub

Private Sub Option1_Click()
    Computer_Name.Enabled = False
    Label1.Enabled = False
    'GetPassword.Enabled = False
    Computer_Name = ComputerName
End Sub

Private Sub Option2_Click()
    Computer_Name.Enabled = True
    Label1.Enabled = True
    GetPassword.Enabled = True
    Computer_Name = ""
End Sub
