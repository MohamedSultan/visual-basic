VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Class1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private WithEvents myReminders As Outlook.Reminders
Attribute myReminders.VB_VarHelpID = -1
Const taskSubject As String = "Trigger Task"
Const amountOfTime As Long = 10

Private Sub myReminders_BeforeReminderShow(Cancel As Boolean)

Dim remind As Outlook.Reminder

  ' check to make sure we're working on the correct reminder
 Set remind = myReminders.Item(1)
  If remind.Caption = taskSubject Then
    ' put code here that you want to run every N minutes
 End If

  ' cancel the reminder
 Cancel = True

End Sub

Private Sub myReminders_ReminderFire(ByVal ReminderObject As Reminder)
Dim tsk As Outlook.TaskItem

  ' create task again
 Set tsk = Application.CreateItem(olTaskItem)
  With tsk
    .Subject = taskSubject
    .StartDate = Format(Now, "mm/dd/yyyy")
    .ReminderSet = True
    .ReminderTime = DateAdd("n", amountOfTime, Now)
    .Save
  End With

End Sub
