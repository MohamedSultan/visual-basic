Attribute VB_Name = "Module2"
Const taskSubject As String = "Trigger Task"
Const amountOfTime As Long = 1
Public Sub StartRemindingMe()
Dim olApp As Outlook.Application
Dim olNS As Outlook.NameSpace
Dim tsk As Outlook.TaskItem
Dim tasksFolder As Outlook.MAPIFolder
Dim tasks As Outlook.Items
Dim matchingTasks As Outlook.Items
Dim i As Long
Dim task As Outlook.TaskItem

  ' only start watching reminders when I say so
 Set olApp = Outlook.Application
  Set myReminders = olApp.Reminders

  ' delete any existing tasks
 Set olNS = olApp.GetNamespace("MAPI")
  Set tasksFolder = olNS.GetDefaultFolder(olFolderTasks)
  Set tasks = tasksFolder.Items
  Set matchingTasks = tasks.Restrict("[Subject] = '" & taskSubject & "'")

  For i = matchingTasks.Count To 1 Step -1
    Set task = matchingTasks.Item(i)
    If task.Subject = taskSubject Then
      With task
        .MarkComplete
        .Delete
      End With
    End If
  Next i

  ' create initial task
 Set tsk = Application.CreateItem(olTaskItem)
  With tsk
    .Subject = taskSubject
    .StartDate = Format(Now, "mm/dd/yyyy")
    .ReminderSet = True
    .ReminderTime = DateAdd("n", amountOfTime, Now)
    .Save
  End With
End Sub
Public Sub StopRemindingMe()
Dim olApp As Outlook.Application
Dim olNS As Outlook.NameSpace
Dim tasksFolder As Outlook.MAPIFolder
Dim tasks As Outlook.Items
Dim task As Outlook.TaskItem
Dim matchingTasks As Outlook.Items
Dim i As Long

  ' delete any existing tasks
 Set olApp = Outlook.Application
  Set olNS = olApp.GetNamespace("MAPI")
  Set tasksFolder = olNS.GetDefaultFolder(olFolderTasks)
  Set tasks = tasksFolder.Items
  Set matchingTasks = tasks.Restrict("[Subject] = '" & taskSubject & "'")

  For i = matchingTasks.Count To 1 Step -1
    Set task = matchingTasks.Item(i)
    If task.Subject = taskSubject Then
      With task
        .MarkComplete
        .Delete
      End With
    End If
  Next i
End Sub
