Attribute VB_Name = "Parser_module"
Public Type Parser_Return
    Ret As Variant
    Found As Boolean
End Type
Public Function Parser(ByVal Whole_String As String, ByVal Delimiter As String) As Parser_Return
    Dim Vars() As String
    Dim Locations(), step  As Integer
    Dim Loc As Double
    
    Whole_String = Replace(Whole_String, Delimiter, "|")
    If Left(Whole_String, 1) = "|" Then Whole_String = Mid(Whole_String, 2, Len(Whole_String))
    Loc = 1
    'Finding Number Of Vars
        While Loc <> 0
            Loc = InStr(Loc + 1, Whole_String, "|")
            ReDim Preserve Locations(step)
            Locations(step) = Loc
            step = step + 1
        Wend
    
    Locations(UBound(Locations)) = Len(Whole_String)
    ReDim Preserve Vars(step - 1)
    
    If step > 0 Then
        Parser.Found = True
        Vars(0) = Mid(Whole_String, 1, Locations(0) - 1)
        For i = 1 To UBound(Vars) - 1
            If Locations(i - 1) <> Locations(i) Then Vars(i) = Mid(Whole_String, Locations(i - 1) + Len("|"), Locations(i) - Locations(i - 1) - 1)
        Next i
    End If
    Parser.Ret = Vars
    
End Function
