Attribute VB_Name = "Global_Variables"
Enum Stats_Category_Enum
    Launcher = 4
    FTD = 1
    Family = 2
    SDPs = 3
End Enum

Type Mo_SMS_Type
    MO_Success_SMS As Double
    MO_Decimated_SMS As Double
    MO_Unknown_SMS As Double
    MO_misc_Error_SMS As Double
    MO_TimeOut_SMS As Double
End Type
Type Mt_SMS_Type
    Mt_Success_SMS As Double
    Mt_Abs_Sub As Double
    Mt_Mem_Full As Double
    Mt_misc_Error_SMS As Double
    Mt_TimeOut_SMS As Double
End Type
Type Switch_type
    SwitchName As String
    SwitchDataMO As Mo_SMS_Type
    SwitchDataMT As Mt_SMS_Type
End Type
Type Op_SMS_Type
    Mo_SMS As Mo_SMS_Type
    Mt_SMS As Mt_SMS_Type
    HourlySMS() As Mo_SMS_Type
    SwitchSMS() As Switch_type
End Type
Type SMS_Stats_Type
    VF_All As Double
    VF_MN As Double
    VF_ET As Double
    VF_Int As Double
    
    Int_VF As Double
    'MN_VF As Double
    'ET_VF As Double
    MN_ET_VF As Double
    App_VF As Double
    VF_VF As Double
    
    Retrials As Double
    Temp As Double
End Type
Public Enum Operators
    Vodafone = 0 '
    Mobinil = 1 '"'EG-Mobinil'"
    Etisalat = 2 ' "'EG-Etisalat'"
End Enum
Public Enum Statistic_Types
    SMS_Hourly = 0
    MO_VF_Local = 1
    MO_VF_Roaming = 2
    'MO_VF_MT_MN = 3
    'MO_VF_MT_ET = 4
    MO_VF_MT_Int = 5
    'MO_VF_MT_Applications = 6
    AO = 7
    MT_MN = 8
    MT_ET = 9
    MO_Int_MT_VF = 10
    MO_VF_Mt_VF = 11
    MO_MN_ET_Int_MT_VF = 12
    MO_Devices = 13
    MT_Devices = 14
    MO_MN_MT_VF = 15
    MO_ET_MT_VF = 16
    SMS_Retrials = 17
    MSC_Messages_MC = 1000
    Inbound_Roamers = 18
    Device_Checker = 1001
    MSC_Messages_MTC = 21
End Enum
Public Enum FTD_Side_Type
    Other = 0
    Originating = 1
    Terminating = 2
End Enum
Public Type FAF_Status_Type
    Count As Double
    Status As String
End Type
Public Type FAF_Statuses_Type
    Failed_Count As Double
    Success_Count As Double
    FAF_Type_ID As Integer
    FAF_Type_Name As String
    FAF_Details() As FAF_Status_Type
End Type
Public Type TransactionClient_Type
    Client_ID As Integer
    Client_Name As String
End Type
Public Type ALL_FAF_Statuses_Type
    Trans_Date As String
    ClientName As String
    Trans_Type As String
    Error_Code As String
    Other_Error As String
    Count As String
End Type
Type Packages
    PKG1 As Double
    PKG2 As Double
    PKG3 As Double
    Other As Double
End Type
Type NumberOfMembers
    NumberOf_VF_Members As Double
    NumberOf_VF_Members2 As Double
    NumberOfNoN_VF_Members As Double
    NumberOf_ET_Members As Double
    NumberOf_MN_Members As Double
    NumberOf_LL_Members As Double
End Type
Type FamilyCounts
    FamilyOf2 As Double
    FamilyOf3 As Double
    FamilyOf4 As Double
End Type
Public Type OP_PKGs
    VF_Packages As FamilyCounts
    MN_Packages As FamilyCounts
    LL_Packages As FamilyCounts
    ET_Packages As FamilyCounts
End Type
Type SDP_Rejection_Type
    SDPName As String
    RejectedCalls As String
    RejectedEvents As String
    BHCA As Double
    DateTime As String
End Type
Type SDP_Stats_Type
    RejectionFound As Boolean
    SDP_Rejections() As SDP_Rejection_Type
End Type
Public Type FAF_Stats_Type
    NumberOfFAFs As Double
    Number_Of_Members As NumberOfMembers
    Families As FamilyCounts
    PKGs As Packages
    OperatorPackages As OP_PKGs
    Stats() As ALL_FAF_Statuses_Type
End Type
Public Enum DB_Types
        MDB = 0
        TextStat = 1
        DailySMS_MDB = 2
        Newfamily_MDB = 3
        NewFamily_Server = 4
        Newfamily_MySQL = 5
        SMS_MySQL = 6
        VNPP = 7
        SDP_MySQL = 8
        OSS_SQL = 9
End Enum
Enum SwitchType
    MOC = 0
    MTC = 1
End Enum
