VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Main_Form 
   Caption         =   "TSP Stats Collector"
   ClientHeight    =   5490
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13005
   LinkTopic       =   "Form1"
   ScaleHeight     =   5490
   ScaleWidth      =   13005
   StartUpPosition =   3  'Windows Default
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
   Begin VB.DirListBox Dir1 
      Height          =   1890
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   1695
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   375
      Left            =   2160
      TabIndex        =   2
      Top             =   840
      Width           =   1695
   End
   Begin VB.CommandButton GetFileName 
      Caption         =   "_"
      Enabled         =   0   'False
      Height          =   255
      Left            =   4200
      TabIndex        =   1
      Top             =   120
      Width           =   255
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4200
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Start2 
      Caption         =   "Start"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2040
      TabIndex        =   0
      Top             =   360
      Width           =   1815
   End
   Begin MSWinsockLib.Winsock wsData 
      Left            =   600
      Top             =   2520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock wsCommand 
      Left            =   120
      Top             =   2520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox txtCommand 
      Height          =   1695
      Left            =   2040
      TabIndex        =   5
      Top             =   2040
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   2990
      _Version        =   393217
      BackColor       =   16777215
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"Form1.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   5115
      Width           =   13005
      _ExtentX        =   22939
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pbBar 
      Height          =   375
      Left            =   3000
      TabIndex        =   7
      Top             =   4560
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label lblUploadStatus 
      Alignment       =   1  'Right Justify
      Caption         =   "Upload Not Started"
      Height          =   255
      Left            =   1440
      TabIndex        =   8
      Top             =   4680
      Width           =   1455
   End
End
Attribute VB_Name = "Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public RemoteHost As String
Public UserName As String
Public Password As String
Dim sent As Boolean
Dim strBuffer As String
Dim strFileData As String
Dim NextComAck As Boolean
Dim UploadRequest As Boolean
Dim UploadFile As String
Dim PassiveEnabled As Boolean
Dim Code As String
Dim UserLoggedIn As Boolean
Dim DataArrived As Boolean
Dim strDownloadData As Variant
Dim sizeofdata As Long
Private Sub Open_FTP(UserName As String, Password As String, RemoteHost As String)
        If Not wsCommand.State = sckClosed Then
            wsCommand.Close
            Do Until wsCommand.State = sckClosed
                DoEvents
            Loop
        End If
        
        txtCommand.Text = txtCommand.Text & Chr(10) & " OK done, the Socket is closed. Opening new connection..."
        'StatusBar.Panels(1).Text = ""
        wsCommand.Connect RemoteHost, 21
        txtCommand.Text = txtCommand.Text & Chr(10) & "I am attempting connection..."
        While Not Code = "220"
            DoEvents
        Wend
        wsCommand.SendData "USER " & UserName & vbCrLf
        While Not Code = "331"
            DoEvents
        Wend
        wsCommand.SendData "PASS " & Password & vbCrLf
        While Not Code = "230"
            DoEvents
        Wend
        wsCommand.SendData "PASV" & vbCrLf
        While Not Code = "227"
            DoEvents
        Wend
        strDownloadData = ""
        DataArrived = False
        PassiveEnabled = False
End Sub
Private Function UploadOneFile(FileName As String, FileTitle As String)
    'This function uploads the file that is provided to it
        On Error GoTo ignoreerror
    Dim IChar As String
    Dim strData As String
    Dim strFileData As String
    UploadFile = FileTitle
    IFile = FreeFile
    Open FileName For Binary As #IFile
    IChar = String(1024, " ")
    'If Len(IFile) > 104858 Then IChar = String(104857, " ") Else
    While Not EOF(IFile)
            'If (FileLen(IFileName) - Len(OutputFile)) < 1048576 Then f_size = String(1024, " ")
            Get #IFile, , IChar
            strFileData = strFileData & IChar
    Wend
    'MsgBox strFileData
    Close IFile
        UploadRequest = True
    wsCommand.SendData "PASV" & vbCrLf
    While Not Code = "227"
        DoEvents
    Wend
    PassiveEnabled = False
    wsCommand.SendData "STOR " & UploadFile & vbCrLf
    While Not (Code = "125" Or Code = "150")
        DoEvents
    Wend
    NextComAck = False
    wsData.SendData strFileData
    lblUploadStatus.Caption = "Uploading " & FileTitle
ignoreerror:
    'do nothing
End Function

Private Sub Command1_Click()

End Sub

Private Sub wsCommand_Connect()
With StatusBar
    .Panels(1).Text = "wsCommand : Connect"
    Me.Caption = "Winsock Terminal (" & wsCommand.RemoteHost & " : " & wsCommand.RemotePort & ")"
End With
End Sub
Private Sub wsData_SendComplete()
    wsData.Close
    lblUploadStatus.Caption = "Upload Complete."
    UploadRequest = False
End Sub

Private Sub wsData_SendProgress(ByVal bytesSent As Long, ByVal bytesRemaining As Long)
   'Label1.Caption = bytesSent
   pbBar.Max = bytesSent + bytesRemaining
   pbBar.Value = bytesSent
End Sub
Private Sub wsCommand_DataArrival(ByVal bytesTotal As Long)
    Dim strData As String
    StatusBar.Panels(1).Text = "wsCommand: DataArrival(" & bytesTotal & ") bytes"
    
    lbytesres = lbytesres + bytesTotal
    'Toolbar.Caption = "Recieved: " & lbytesres & " Sent: " & lbytessent & " "
    wsCommand.GetData strData
    Code = Left(strData, 3)
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & strData
        .SelStart = Len(.Text)
    End With
    If Left(strData, 3) = "227" Then
        PassiveEnabled = True
        MakeDataConnection strData
    ElseIf Left(strData, 3) = "354" Then
        bMailData = True
    End If
    If Left(strData, 3) = "125" Or Left(strData, 3) = "150" Then
        NextComAck = True
    End If
End Sub
Private Sub MakeDataConnection(sData As String)
'������ ������ ������������ � �������� ���������
'227 Entering Passive Mode (194,220,224,2,7,189)
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & "Extracting IP and Port for Data transfer..."
        .SelStart = Len(.Text)
    End With
    Dim iPos As Integer, iPos2 As Integer
    Dim strDataAddress
    iPos = InStr(1, sData, "(") + 1
    For i = 1 To 4
        iPos2 = InStr(iPos, sData, ",")
        strDataAddress = strDataAddress & Mid(sData, iPos, iPos2 - iPos) & "."
        iPos = iPos2 + 1
    Next
    strDataAddress = Left(strDataAddress, Len(strDataAddress) - 1)
    Debug.Print strDataAddress
    
    Dim i1 As Single, i2 As Single, inPort As Single
    iPos = iPos2 + 1
    iPos2 = InStr(iPos, sData, ",")
    
    i1 = CSng(Mid(sData, iPos, iPos2 - iPos))
    iPos = iPos2 + 1
    iPos2 = InStr(iPos, sData, ")")
    i2 = CSng(Mid(sData, iPos, iPos2 - iPos))
    inPort = i1 * 256 + i2
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & "Estabilishing connection..."
        .SelStart = Len(.Text)
    End With
    Debug.Print "Port: " & inPort
'    Winsock1(1).Bind 624, Winsock1(0).LocalIP
    If Not wsData.State = sckConnected Then
        wsData.Close
    End If
    wsData.Connect strDataAddress, inPort
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & "Done."
        .SelStart = Len(.Text)
    End With
End Sub
Private Sub wsData_Close()
    wsData.Close
    DataArrived = True
End Sub

Private Sub wsData_DataArrival(ByVal bytesTotal As Long)
    Dim strData As String
    wsData.GetData strData
    strDownloadData = strDownloadData & strData
    sizeofdata = sizeofdata + bytesTotal
    'If Len(strDownloadData) = sizeofdata Then MsgBox "same" Else MsgBox "not same"
    'MsgBox sizeofdata
    'txtCommand.Text = txtCommand.Text & strData
    'End If
End Sub
Private Sub Drive1_Change()
    Dir1.Path = Drive1.Drive
End Sub

Private Sub GetFileName_Click()
    Dim Directory As String
    If File_Path = "" Then File_Path = "D:\"
    Directory = Get_Dir(File_Path.Text)
    CommonDialog1.InitDir = Directory
    CommonDialog1.CancelError = False
    CommonDialog1.Filter = "All Files (*.*)|*.*|Text Files(*.txt)|*.txt"
    CommonDialog1.FilterIndex = 1
    CommonDialog1.ShowOpen
    
    If CommonDialog1.FileName <> "" Then
        File_Path.Text = CommonDialog1.FileName
    End If
End Sub

Private Sub Start_Click()
    Start.Enabled = False
        
    Dim FileName_Loc, TSP_Name_Loc As Integer
    Dim TSP_Name As String
    Dim FileName As String
    
    Dir1.Path = "D:\FTP\export\home\FtpFolder\TSP\TSP_Logs"
    
    FileName = Dir(Dir1.Path + "\")
'------------------------------------------------------------------------------------------------------------------------
    'Reformating Files
    While FileName <> ""
        If InStr(1, FileName, ".") = 0 Then
            TSP_Name_Loc = InStr(1, FileName, "_")
            TSP_Name = Mid(FileName, 1, TSP_Name_Loc - 1)
            If TSP_Name = "TSP" Then TSP_Name = "TSP" + Mid(FileName, 5, 2)
    
            Call ConvertFile(Dir1.Path + "\" + FileName, Dir1.Path + "\Working\" + FileName, TSP_Name)
            Call FileOperation(FO_MOVE, Dir1.Path + "\" + FileName, Dir1.Path + "\Received Files\" + FileName)
        End If
        FileName = Dir
    Wend
'------------------------------------------------------------------------------------------------------------------------
    'Concatinating Files
    
    Dim Config_File_Name As Double
    Config_File_Name = FreeFile
    Dim Line As String
    Dim Files As Parser_Return
    Dim ConvertedFiles(2) As String
    
    Open Dir1.Path + "\File Naming.Config" For Input As Config_File_Name
        Do Until EOF(Config_File_Name)
            Line Input #Config_File_Name, Line
            If Left(Trim(Line), 1) <> "#" Then
                Files = Parser(Line + ",", ",")
                For i = 0 To 2
                    ConvertedFiles(i) = Files.Ret(i)
                Next i

                Call ConcatinateFiles(Dir1.Path + "\Working\" + ConvertedFiles(0) + Format(Date - 1, "YYYYMMDD"), _
                                        Dir1.Path + "\Working\" + ConvertedFiles(1) + Format(Date - 1, "YYYYMMDD"), _
                                        Dir1.Path + "\To FTP\" + ConvertedFiles(2) + Format(Date - 1, "YYYYMMDD"))
                
                Call FileOperation(FO_DELETE, Dir1.Path + "\Working\" + ConvertedFiles(0) + Format(Date - 1, "YYYYMMDD"), _
                                                Dir1.Path + "\Working\" + ConvertedFiles(1) + Format(Date - 1, "YYYYMMDD"))
            End If
        Loop
    Close Config_File_Name
'------------------------------------------------------------------------------------------------------------------------
    'FTP Files
    FileName = Dir(Dir1.Path + "\To FTP\")
    Call Open_FTP("peri", "peri", "172.28.6.15")
    While FileName <> ""
        If InStr(1, FileName, ".") = 0 Then
    
            UploadOneFile Dir1.Path + "\To FTP\" + FileName, FileName
            While Not Code = "226"
                DoEvents
            Wend
            Call FileOperation(FO_MOVE, Dir1.Path + "\To FTP\" + FileName, Dir1.Path + "\Transferred\" + FileName)
        End If
        FileName = Dir
    Wend
    
    Start.Enabled = True
End Sub
Private Sub ConcatinateFiles(From_File1 As String, From_File2 As String, To_File3 As String)
    Dim I_File_Name, O_File_Name As Double
    I_File_Name = FreeFile
    O_File_Name = FreeFile + 1
    Dim Line As String
    
    On Error GoTo LocalHandler
    Open From_File1 For Input As I_File_Name
    Open To_File3 For Output As O_File_Name
        Do Until EOF(I_File_Name)
            Line Input #I_File_Name, Line
            Print #O_File_Name, Line
        Loop
    Close I_File_Name
    Open From_File2 For Input As I_File_Name
        Do Until EOF(I_File_Name)
            Line Input #I_File_Name, Line
            If InStr(1, Line, "TimeStamp") = 0 Then Print #O_File_Name, Line
        Loop
    Close I_File_Name
    Close O_File_Name
LocalHandler:
    If Err.Number = 53 Then
        Close I_File_Name
        Close O_File_Name
        Exit Sub
    End If
End Sub
Private Sub FileOperation(FileFunction As File_Functions_Enum, FromFile As String, Optional ToFile As String)
    
    On Error GoTo LocalHandler
    If FileFunction = FO_DELETE Then
        Kill FromFile
        If ToFile <> "" Then Kill ToFile
    Else
        
        Dim lResult As Long, SHF As SHFILEOPSTRUCT
        SHF.hwnd = hwnd
        SHF.wFunc = FileFunction
        SHF.pFrom = FromFile
        SHF.pTo = ToFile
        SHF.fFlags = FOF_FILESONLY
        lResult = SHFileOperation(SHF)
        'If lResult Then
        '    MsgBox "Error occurred!", vbInformation, "SHCOPY"
        'End If
    End If
LocalHandler:
    If Err.Number = 53 Then
        If ToFile <> "" Then Kill ToFile
        Exit Sub
    End If
End Sub

Private Sub ConvertFile(FromFileName As String, ToFileName As String, TSP_Name As String)
    Dim I_File_Name, O_File_Name As Double
    I_File_Name = FreeFile
    O_File_Name = FreeFile + 1
    Dim Line, NewLine As String

    Open FromFileName For Input As I_File_Name
    Open ToFileName For Output As O_File_Name
        Line Input #I_File_Name, Line
        FileName_Loc = InStr(1, Line, ",")
        NewLine = Left(Line, FileName_Loc) + "NE" + Mid(Line, FileName_Loc, Len(Line) - FileName_Loc + 1)
        NewLine = Replace(NewLine, ",", " ")
        Print #O_File_Name, NewLine
        Do Until EOF(I_File_Name)
            Line Input #I_File_Name, Line

            If Line <> "" Then
                NewLine = Left(Line, 19) + TSP_Name + " " + Mid(Line, 20, Len(Line) - 19)
                NewLine = Replace(NewLine, ",", " ")
                Print #O_File_Name, NewLine
            End If
        Loop
    Close I_File_Name
    Close O_File_Name
End Sub
Private Function GetNewFileName(CurrentFileName) As String
    If InStr(1, CurrentFileName, "PlatformMeas") <> 0 Then
        GetNewFileName = "TSP_CPLoad"
    End If
    'If InStr(1, CurrentFileName, "PlatformMeas") <> 0 Then
    '    GetNewFileName = "TSP_CPLoad"
    'End If
    'If InStr(1, CurrentFileName, "PlatformMeas") <> 0 Then
    '    GetNewFileName = "TSP_CPLoad"
    'End If
End Function

Private Function GetInsertStatement(TSP_Stats As TSP_STATS_TYPE, ByRef Data As Parser_Return, ByRef TSP_Name As String) As String
    With Data
        If .Found Then
            Select Case TSP_Stats
                Case PlatformMesures
                    GetInsertStatement = "insert into TSP_CPLoad_Temp(Time,NE,Source,CpuLoad,MemUsed,MemFree,MemUsage,ProcessorUptime,ClusterUptime,Date) " + _
                                            "VALUES ('" + .Ret(0) + "','" + TSP_Name + "','" + .Ret(1) + "'," + .Ret(2) + "," + .Ret(3) + "," + Str(Int(Val(.Ret(4)))) + "," + Str(Int(Val(.Ret(5)))) + "," + Str(Int(Val(.Ret(6)))) + "," + Str(Int(Val(.Ret(7)))) + ",'" + .Ret(8) + "')"
            End Select
        End If
    End With
End Function
Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function
Private Sub Start2_Click()
        
    Start.Enabled = False
        
    Dim FileName_Loc, TSP_Name_Loc As Integer
    Dim TSP_Name As String
    Dim FileName As String
    
    FileName_Loc = InStrRev(File_Path.Text, "\")
    FileName = Mid(File_Path.Text, FileName_Loc + 1, Len(File_Path) - FileName_Loc + 1)
    TSP_Name_Loc = InStr(1, FileName, "_")
    TSP_Name = Mid(FileName, 1, TSP_Name_Loc - 1)
    
    Dim I_File_Name As Double
    I_File_Name = FreeFile
    Dim Line, Statement As String
    Dim Data As Parser_Return
    Dim DataNames As Parser_Return
    Dim Time_Date As Parser_Return
    Dim Times_Temp As Parser_Return
    Dim Times(2) As Date
    
    Dim Conn As ADODB.Connection
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    Call DB_Open(Conn, OSS_SQL)
    
    Open File_Path For Input As I_File_Name
        Line Input #I_File_Name, Line
        DataNames = Parser(Line + ",", ",")
        Do Until EOF(I_File_Name)
            'ReDim Preserve File_List(Step)
            Line Input #I_File_Name, Line
            'If File_List(Step) <> "" Then
            '    Step = Step + 1
            'End If
            Data = Parser(Line + ",", ",")
            If Data.Found And Data.Ret(0) <> "" Then
                Time_Date = Parser(Data.Ret(0) & ".", ".")
                Data.Ret(8) = Mid(Time_Date.Ret(0), 5, 2) + "/" + Mid(Time_Date.Ret(0), 7, 2) + "/" + Mid(Time_Date.Ret(0), 1, 4)
                
                Times(1) = Format(Left(Time_Date.Ret(1), 2) + ":" + Mid(Time_Date.Ret(1), 3, 2), "Long Time") 'Format(Left(Time_Date.Ret(1), 4), "hh:mm")
                Times(2) = Format(Mid(Time_Date.Ret(1), 6, 2) + ":" + Mid(Time_Date.Ret(1), 8, 2), "hh:nn") 'Format(Right(Time_Date.Ret(1), 4), "hh:mm")
                
                Times(0) = Times(2) - Times(1) + Format("00:01", "nn")
                Data.Ret(0) = Format(Data.Ret(8) & " " & Times(2), "mm/dd/yyyy hh:mm")
                Data.Ret(8) = Format(Data.Ret(8) & " " & Times(1), "mm/dd/yyyy hh:00")
                Statement = GetInsertStatement(PlatformMesures, Data, TSP_Name)
                Result.Open Statement, Conn
            End If
            
        Loop
    Close I_File_Name
    Conn.Close
    Start.Enabled = True
End Sub
