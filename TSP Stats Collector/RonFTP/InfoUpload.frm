VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Begin VB.Form InfoUpload 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Uploading Info to Database"
   ClientHeight    =   5100
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   6210
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Close"
      Height          =   375
      Left            =   2400
      TabIndex        =   2
      Top             =   4560
      Width           =   1215
   End
   Begin SHDocVwCtl.WebBrowser browser 
      Height          =   3375
      Left            =   240
      TabIndex        =   0
      Top             =   960
      Width           =   5655
      ExtentX         =   9975
      ExtentY         =   5953
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.Label Label1 
      Caption         =   "Uploading Information to Database at server..."
      BeginProperty Font 
         Name            =   "Baskerville Old Face"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   5775
   End
End
Attribute VB_Name = "InfoUpload"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Unload InfoUpload
End Sub

Private Sub Form_Load()
    Dim navURL As String
    navURL = "http://ron/FileUpload/updateinfo.asp?title="
    navURL = navURL + Form1.txtTitle.Text
    navURL = navURL & "&type=" & Form1.txtType.Text & "&desc=" & Form1.txtDesc.Text
    MsgBox navURL
    browser.Navigate (navURL)
End Sub
