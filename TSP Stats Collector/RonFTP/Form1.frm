VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "File Transfer Protocol"
   ClientHeight    =   7605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8850
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   8850
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command4 
      Caption         =   "Download->"
      Height          =   375
      Left            =   5760
      TabIndex        =   17
      Top             =   4560
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "FTP Interface"
      Height          =   4335
      Left            =   120
      TabIndex        =   5
      Top             =   2520
      Width           =   8655
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   255
         Left            =   5640
         TabIndex        =   18
         Top             =   1680
         Width           =   975
      End
      Begin VB.Timer Timer1 
         Left            =   1200
         Top             =   1680
      End
      Begin MSFlexGridLib.MSFlexGrid lsTransaction 
         Height          =   3015
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   5318
         _Version        =   393216
         FixedCols       =   0
      End
      Begin VB.TextBox lblRemotePath 
         BackColor       =   &H8000000C&
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   240
         Width           =   2415
      End
      Begin VB.CommandButton cmdRemUp 
         Caption         =   "/.."
         Height          =   255
         Left            =   5160
         TabIndex        =   14
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton Command2 
         Caption         =   "<--Upload--"
         Height          =   375
         Left            =   5640
         TabIndex        =   13
         Top             =   2520
         Width           =   975
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   960
         Top             =   3600
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Form1.frx":0442
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Form1.frx":1494
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Form1.frx":216E
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView tv1 
         Height          =   3015
         Left            =   2760
         TabIndex        =   12
         Top             =   600
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   5318
         _Version        =   393217
         HideSelection   =   0   'False
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         OLEDragMode     =   1
      End
      Begin VB.DriveListBox Drive1 
         Height          =   315
         Left            =   6720
         TabIndex        =   11
         Top             =   240
         Width           =   1815
      End
      Begin VB.FileListBox File1 
         Height          =   1845
         Left            =   6720
         MultiSelect     =   1  'Simple
         OLEDropMode     =   1  'Manual
         TabIndex        =   10
         Top             =   1920
         Width           =   1815
      End
      Begin VB.DirListBox Dir1 
         Height          =   1215
         Left            =   6720
         TabIndex        =   9
         Top             =   600
         Width           =   1815
      End
      Begin MSComctlLib.ProgressBar pbBar 
         Height          =   375
         Left            =   3240
         TabIndex        =   7
         Top             =   3840
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   661
         _Version        =   393216
         Appearance      =   1
      End
      Begin VB.Label Label1 
         Height          =   255
         Left            =   7320
         TabIndex        =   8
         Top             =   2880
         Width           =   855
      End
      Begin VB.Label lblUploadStatus 
         Alignment       =   1  'Right Justify
         Caption         =   "Upload Not Started"
         Height          =   255
         Left            =   1680
         TabIndex        =   6
         Top             =   3960
         Width           =   1455
      End
   End
   Begin MSComDlg.CommonDialog cmdShow 
      Left            =   3360
      Top             =   6840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "JPEG Image|*.jpg"
   End
   Begin MSWinsockLib.Winsock wsData 
      Left            =   600
      Top             =   6840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox txtCommand 
      Height          =   1695
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   2990
      _Version        =   393217
      BackColor       =   16777215
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"Form1.frx":2DF5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   7230
      Width           =   8850
      _ExtentX        =   15610
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8850
      _ExtentX        =   15610
      _ExtentY        =   1111
      ButtonWidth     =   609
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   393216
      Begin VB.CommandButton Command3 
         Caption         =   "Disconnect"
         Height          =   375
         Left            =   1320
         TabIndex        =   3
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton cmdConnect 
         Caption         =   "Connect"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   120
         Width           =   1095
      End
   End
   Begin MSWinsockLib.Winsock wsCommand 
      Left            =   120
      Top             =   6840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public remotehost As String
Public username As String
Public password As String
Dim sent As Boolean
Dim strBuffer As String
Dim strFileData As String
Dim NextComAck As Boolean
Dim UploadRequest As Boolean
Dim UploadFile As String
Dim PassiveEnabled As Boolean
Dim Code As String
Dim UserLoggedIn As Boolean
Dim DataArrived As Boolean
Dim strDownloadData As Variant
Dim sizeofdata As Long

Private Function UploadOneFile(FileName As String, FileTitle As String)
    'This function uploads the file that is provided to it
        On Error GoTo ignoreerror
    Dim IChar As String
    Dim strData As String
    Dim strFileData As String
    UploadFile = FileTitle
    IFile = FreeFile
    Open FileName For Binary As #IFile
    IChar = String(1024, " ")
    'If Len(IFile) > 104858 Then IChar = String(104857, " ") Else
    While Not EOF(IFile)
            'If (FileLen(IFileName) - Len(OutputFile)) < 1048576 Then f_size = String(1024, " ")
            Get #IFile, , IChar
            strFileData = strFileData & IChar
    Wend
    'MsgBox strFileData
    Close IFile
        UploadRequest = True
    wsCommand.SendData "PASV" & vbCrLf
    While Not Code = "227"
        DoEvents
    Wend
    PassiveEnabled = False
    wsCommand.SendData "STOR " & UploadFile & vbCrLf
    While Not (Code = "125" Or Code = "150")
        DoEvents
    Wend
    NextComAck = False
    wsData.SendData strFileData
    lblUploadStatus.Caption = "Uploading " & FileTitle
ignoreerror:
    'do nothing
End Function
Private Function DownloadOneFile(FileName As String, Size As Long)
    strDownloadData = ""
    sizeofdata = 0
    wsCommand.SendData "PASV" & vbCrLf
    While Not PassiveEnabled
        DoEvents
    Wend
    PassiveEnabled = False
    wsCommand.SendData "RETR " & FileName & vbCrLf
    'While Not TransferComplete
    While sizeofdata <> Size
        DoEvents
    Wend
    DataArrived = False
    ofile = FreeFile
    strtmp = Dir1.Path & "\" & FileName
    'MsgBox strDownloadData
    ofile = FreeFile
    Open strtmp For Output As ofile
    Print #ofile, strDownloadData
    Close #ofile
    
    strDownloadData = ""
    'MsgBox sizeofdata
End Function
Private Sub cmdConnect_Click()

Dim frm As New frmConnect
    frm.Show vbModal
    If cmdConnect.Tag = "connect" Then
        If Not wsCommand.State = sckClosed Then
            wsCommand.Close
            Do Until wsCommand.State = sckClosed
                DoEvents
            Loop
        End If
        
        txtCommand.Text = txtCommand.Text & Chr(10) & " OK done, the Socket is closed. Opening new connection..."
        StatusBar.Panels(1).Text = ""
        wsCommand.Connect remotehost, 21
        txtCommand.Text = txtCommand.Text & Chr(10) & "I am attempting connection..."
        While Not Code = "220"
            DoEvents
        Wend
        wsCommand.SendData "USER " & username & vbCrLf
        While Not Code = "331"
            DoEvents
        Wend
        wsCommand.SendData "PASS " & password & vbCrLf
        While Not Code = "230"
            DoEvents
        Wend
        wsCommand.SendData "PASV" & vbCrLf
        While Not Code = "227"
            DoEvents
        Wend
        strDownloadData = ""
        DataArrived = False
        PassiveEnabled = False
        wsCommand.SendData "LIST" & vbCrLf
        While Not DataArrived
            DoEvents
        Wend
        strListPath = App.Path & "\list.txt"
        Kill (strListPath)
        lfile = FreeFile
        Open strListPath For Output As lfile
        Print #lfile, Trim(strDownloadData)
        Close #lfile
        strDownloadData = ""
        lfile = FreeFile
        tv1.Nodes.Clear
        Open strListPath For Input As lfile
        i = 0
        strtmp = ""
        Do While Not EOF(lfile)
            Line Input #lfile, strtmp
            If strtmp <> "" Then
            'tmpItem = Trim(Right(strtmp, Len(strtmp) - 58))
            tmpItemSize = Trim(Mid(strtmp, 30, 16))
            If Left(strtmp, 1) = "d" Then
                tv1.Nodes.Add , , , tmpItem, 1
                tv1.Nodes.Item(tv1.Nodes.Count).Tag = "folder"
            Else
                tv1.Nodes.Add , , , tmpItem, 3
                tv1.Nodes.Item(tv1.Nodes.Count).Tag = tmpItemSize
            End If
            i = i + 1
            End If
        Loop
        Close lfile
        lblRemotePath = "/"
    End If
End Sub



Private Sub cmdRemUp_Click()
     If lblRemotePath.Text <> "/" Then
        wsCommand.SendData "CWD .." & vbCrLf
        While Not Code = "250"
            DoEvents
        Wend
        tmpCount = tv1.Nodes.Count
        For k = tmpCount To 1 Step -1
            tv1.Nodes.Remove (k)
        Next k
        wsCommand.SendData "PASV" & vbCrLf
        While Not Code = "227"
            DoEvents
        Wend
        strDownloadData = ""
        DataArrived = False
        PassiveEnabled = False
        wsCommand.SendData "LIST" & vbCrLf
        While Not DataArrived
            DoEvents
        Wend
        DataArrived = False
        strListPath = App.Path & "\list.txt"
        Kill (strListPath)
        lfile = FreeFile
        Open strListPath For Output As lfile
        Print #lfile, strDownloadData
        Close #lfile
        strDownloadData = ""
        lfile = FreeFile
        Open strListPath For Input As lfile
        i = 0
        Do While Not EOF(lfile)
            Line Input #lfile, strtmp
            'tmpPos = InStr(1, strTmp, ":", vbTextCompare)
            If strtmp <> "" Then
            tmpItem = Trim(Right(strtmp, Len(strtmp) - 58))
            tmpItemSize = Trim(Mid(strtmp, 30, 16))
            If Left(strtmp, 1) = "d" Then
                tv1.Nodes.Add , , , tmpItem, 1
                tv1.Nodes.Item(tv1.Nodes.Count).Tag = "folder"
            Else
                tv1.Nodes.Add , , , tmpItem, 3
                tv1.Nodes.Item(tv1.Nodes.Count).Tag = tmpItemSize
            End If
            i = i + 1
            End If
        Loop

        Close lfile
        tmpPos = InStrRev(lblRemotePath, "/", Len(lblRemotePath) - 1, vbTextCompare)
        lblRemotePath = Left(lblRemotePath, tmpPos)
    End If
End Sub

Private Sub Command1_Click()
    Dim tmpFileName As String
    File1.Selected(2) = True
    For i = 1 To (File1.ListCount - 1)
       'MsgBox "pass"
       
       If File1.Selected(i) = True Then
            tmpFileName = File1.Path & "\" & File1.List(i)
            UploadOneFile tmpFileName, File1.List(i)
            While Not Code = "226"
                DoEvents
            Wend
        End If
    Next
End Sub

Public Sub Command2_Click()
    Dim tmpFileName As String
    For i = 1 To (File1.ListCount - 1)
       'MsgBox "pass"
       If File1.Selected(i) = True Then
            tmpFileName = File1.Path & "\" & File1.List(i)
            UploadOneFile tmpFileName, File1.List(i)
            While Not Code = "226"
                DoEvents
            Wend
        End If
    Next
End Sub

Private Sub Command3_Click()
If Not wsCommand.State = sckClosed Then
    wsCommand.Close
    Do Until wsCommand.State = sckClosed
        DoEvents
    Loop
    
End If
tv1.Nodes.Clear
End Sub

Private Sub Command4_Click()
    If IsNumeric(CLng(tv1.SelectedItem.Tag)) And tv1.SelectedItem.Tag <> "folder" Then
    DownloadOneFile tv1.SelectedItem.Text, CLng(tv1.SelectedItem.Tag)
    Else
    MsgBox "Please select a file to download", vbOKOnly + vbExclamation, "RonFTP"
    End If
End Sub

Private Sub Command6_Click()
    Call Command4_Click
    lblFileName.Caption = cmdShow.FileName
    Image1.Picture = LoadPicture(cmdShow.FileName)
End Sub

Private Sub Command7_Click()
    UploadRequest = True
    wsCommand.SendData "PASV" & vbCrLf
    lblUploadStatus.Caption = "Uploading..."
    
End Sub

Private Sub Dir1_Change()
    File1.Path = Dir1.Path
End Sub

Private Sub Drive1_Change()
    Dir1.Path = Left(Drive1.Drive, 2) & "\"
End Sub

Private Sub File1_DragDrop(Source As Control, X As Single, Y As Single)
    Call Command4_Click
End Sub

Private Sub Form_Load()
    UploadRequest = False
    DataArrived = False
    NextComAck = False
    PassiveEnabled = False
    Code = ""
    lsTransaction.Row = 0
    lsTransaction.Text = "File Path"
    lsTransaction.Col = 1
    lsTransaction.Text = "File Name"
End Sub


Private Sub tv1_DblClick()
    If tv1.SelectedItem.Tag = "folder" Then
        tmpText = tv1.SelectedItem.Text
        wsCommand.SendData "CWD " & tmpText & vbCrLf
        While Not Code = "250"
            DoEvents
        Wend
        tmpCount = tv1.Nodes.Count
        For k = tmpCount To 1 Step -1
            tv1.Nodes.Remove (k)
        Next k
        wsCommand.SendData "PASV" & vbCrLf
        While Not Code = "227"
            DoEvents
        Wend
        strDownloadData = ""
        DataArrived = False
        PassiveEnabled = False
        wsCommand.SendData "LIST" & vbCrLf
        While Not DataArrived
            DoEvents
        Wend
        DataArrived = False
        strListPath = App.Path & "\list.txt"
        Kill (strListPath)
        lfile = FreeFile
        Open strListPath For Output As lfile
        Print #lfile, strDownloadData
        Close lfile
        strDownloadData = ""
        lfile = FreeFile
        Open strListPath For Input As lfile
        i = 0
        Do While Not EOF(lfile)
            Line Input #lfile, strtmp
            'tmpPos = InStr(1, strTmp, ":", vbTextCompare)
            If strtmp <> "" Then
            tmpItem = Trim(Right(strtmp, Len(strtmp) - 58))
            tmpItemSize = Trim(Mid(strtmp, 30, 16))
            If Left(strtmp, 1) = "d" Then
                tv1.Nodes.Add , , , tmpItem, 1
                tv1.Nodes.Item(tv1.Nodes.Count).Tag = "folder"
            Else
                tv1.Nodes.Add , , , tmpItem, 3
                tv1.Nodes.Item(tv1.Nodes.Count).Tag = tmpItemSize
            End If
            i = i + 1
            End If
        Loop
        Close lfile
        lblRemotePath = lblRemotePath & tmpText & "/"
    'End If
    End If
End Sub

Private Sub txtCommand_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode > 36 And KeyCode < 41 Then
        KeyCode = 0
    End If
End Sub

Private Sub txtCommand_KeyPress(KeyAscii As Integer)
    
    
    On Error Resume Next
    
    If KeyAscii = 8 Then
        strBuffer = Left$(strBuffer, Len(strBuffer) - 1)
    Else
        strBuffer = strBuffer & Chr$(KeyAscii)
    End If
    
    If KeyAscii = 13 Then
        wsCommand.SendData strBuffer & vbCrLf
        'txtCommand.Text = txtCommand.Text + Chr(10) + "Issuing command " + strBuffer + "..."
        'txtCommand = txtCommand + " Sending command " + strBuffer + "..."
        strBuffer = ""
    End If
    
End Sub


Private Sub txtCommand_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    txtCommand.SelStart = Len(txtCommand.Text)
    
End Sub

Private Sub RichTextBox1_Change()
    
End Sub

Private Sub wsCommand_Connect()
With StatusBar
    .Panels(1).Text = "wsCommand : Connect"
    Me.Caption = "Winsock Terminal (" & wsCommand.remotehost & " : " & wsCommand.RemotePort & ")"
End With
End Sub

Private Sub wsCommand_DataArrival(ByVal bytesTotal As Long)
    Dim strData As String
    StatusBar.Panels(1).Text = "wsCommand: DataArrival(" & bytesTotal & ") bytes"
    
    lbytesres = lbytesres + bytesTotal
    'Toolbar.Caption = "Recieved: " & lbytesres & " Sent: " & lbytessent & " "
    wsCommand.GetData strData
    Code = Left(strData, 3)
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & strData
        .SelStart = Len(.Text)
    End With
    If Left(strData, 3) = "227" Then
        PassiveEnabled = True
        MakeDataConnection strData
    ElseIf Left(strData, 3) = "354" Then
        bMailData = True
    End If
    If Left(strData, 3) = "125" Or Left(strData, 3) = "150" Then
        NextComAck = True
    End If
End Sub

Private Sub wsData_Close()
    wsData.Close
    DataArrived = True
End Sub

Private Sub wsData_DataArrival(ByVal bytesTotal As Long)
    Dim strData As String
    wsData.GetData strData
    strDownloadData = strDownloadData & strData
    sizeofdata = sizeofdata + bytesTotal
    'If Len(strDownloadData) = sizeofdata Then MsgBox "same" Else MsgBox "not same"
    'MsgBox sizeofdata
    'txtCommand.Text = txtCommand.Text & strData
    'End If
End Sub
Private Sub MakeDataConnection(sData As String)
'������ ������ ������������ � �������� ���������
'227 Entering Passive Mode (194,220,224,2,7,189)
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & "Extracting IP and Port for Data transfer..."
        .SelStart = Len(.Text)
    End With
    Dim iPos As Integer, iPos2 As Integer
    Dim strDataAddress
    iPos = InStr(1, sData, "(") + 1
    For i = 1 To 4
        iPos2 = InStr(iPos, sData, ",")
        strDataAddress = strDataAddress & Mid(sData, iPos, iPos2 - iPos) & "."
        iPos = iPos2 + 1
    Next
    strDataAddress = Left(strDataAddress, Len(strDataAddress) - 1)
    Debug.Print strDataAddress
    
    Dim i1 As Single, i2 As Single, inPort As Single
    iPos = iPos2 + 1
    iPos2 = InStr(iPos, sData, ",")
    
    i1 = CSng(Mid(sData, iPos, iPos2 - iPos))
    iPos = iPos2 + 1
    iPos2 = InStr(iPos, sData, ")")
    i2 = CSng(Mid(sData, iPos, iPos2 - iPos))
    inPort = i1 * 256 + i2
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & "Estabilishing connection..."
        .SelStart = Len(.Text)
    End With
    Debug.Print "Port: " & inPort
'    Winsock1(1).Bind 624, Winsock1(0).LocalIP
    If Not wsData.State = sckConnected Then
        wsData.Close
    End If
    wsData.Connect strDataAddress, inPort
    With txtCommand
        .SetFocus
        .Text = .Text & Chr(10) & "Done."
        .SelStart = Len(.Text)
    End With
End Sub

Private Sub wsData_SendComplete()
    wsData.Close
    lblUploadStatus.Caption = "Upload Complete."
    UploadRequest = False
End Sub

Private Sub wsData_SendProgress(ByVal bytesSent As Long, ByVal bytesRemaining As Long)
   'Label1.Caption = bytesSent
   pbBar.Max = bytesSent + bytesRemaining
   pbBar.Value = bytesSent
End Sub
Private Function CheckAcknowledge()
    
End Function
