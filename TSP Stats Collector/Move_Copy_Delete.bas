Attribute VB_Name = "Move_Copy_Delete"


Option Explicit

' Shcopy sample from BlackBeltVB.com
' http://blackbeltvb.com
'
' Written by Matt Hart
' Copyright 1999 by Matt Hart
'
' This software is FREEWARE. You may use it as you see fit for
' your own projects but you may not re-sell the original or the
' source code. Do not copy this sample to a collection, such as
' a CD-ROM archive. You may link directly to the original sample
' using "http://blackbeltvb.com/shcopy.htm"
'
' No warranty express or implied, is given as to the use of this
' program. Use at your own risk.
'
' This program shows how to implement the SHFileOperation API.
' You can use it to delete, move, or copy multiple or single files,
' and it can send files to the recycle bin.

Public Type SHFILEOPSTRUCT
    hwnd As Long
    wFunc As Long
    pFrom As String
    pTo As String
    fFlags As Integer
    fAnyOperationsAborted As Boolean
    hNameMappings As Long
    lpszProgressTitle As String '  only used if FOF_SIMPLEPROGRESS
End Type

Public Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" _
   (lpFileOp As SHFILEOPSTRUCT) As Long

' // Shell File Operations
Public Enum File_Functions_Enum
    FO_MOVE = &H1
    FO_COPY = &H2
    FO_DELETE = &H3
    FO_RENAME = &H4
End Enum

Public Const FOF_MULTIDESTFILES = &H1
Public Const FOF_CONFIRMMOUSE = &H2
Public Const FOF_SILENT = &H4                      '  don't create progress/report
Public Const FOF_RENAMEONCOLLISION = &H8
Public Const FOF_NOCONFIRMATION = &H10             '  Don't prompt the user.
Public Const FOF_WANTMAPPINGHANDLE = &H20          '  Fill in SHFILEOPSTRUCT.hNameMappings
                                      '  Must be freed using SHFreeNameMappings
Public Const FOF_ALLOWUNDO = &H40
Public Const FOF_FILESONLY = &H80                  '  on *.*, do only files - not directories
Public Const FOF_SIMPLEPROGRESS = &H100            '  means don't show names of files
Public Const FOF_NOCONFIRMMKDIR = &H200            '  don't confirm making any needed dirs

Public Const PO_DELETE = &H13           '  printer is being deleted
Public Const PO_RENAME = &H14           '  printer is being renamed
Public Const PO_PORTCHANGE = &H20       '  port this printer connected to is being changed
                                '  if this id is set, the strings received by
                                '  the copyhook are a doubly-null terminated
                                '  list of strings.  The first is the printer
                                '  name and the second is the printer port.
Public Const PO_REN_PORT = &H34         '  PO_RENAME and PO_PORTCHANGE at same time.


