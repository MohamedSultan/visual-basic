VERSION 5.00
Begin VB.Form MINSAT_Form 
   Caption         =   "MINSAT"
   ClientHeight    =   5295
   ClientLeft      =   6405
   ClientTop       =   2955
   ClientWidth     =   7110
   LinkTopic       =   "Form1"
   ScaleHeight     =   5295
   ScaleWidth      =   7110
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   2520
      TabIndex        =   21
      Text            =   "Text1"
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox Text7 
      Height          =   285
      Left            =   2280
      TabIndex        =   16
      Text            =   "Text1"
      Top             =   1440
      Width           =   1695
   End
   Begin VB.TextBox Text6 
      Height          =   285
      Left            =   2280
      TabIndex        =   14
      Text            =   "Text1"
      Top             =   1800
      Width           =   1695
   End
   Begin VB.TextBox Text5 
      Height          =   285
      Left            =   2280
      TabIndex        =   13
      Text            =   "Text1"
      Top             =   2160
      Width           =   1695
   End
   Begin VB.TextBox Text4 
      Height          =   285
      Left            =   2280
      TabIndex        =   12
      Text            =   "Text1"
      Top             =   2520
      Width           =   1695
   End
   Begin VB.TextBox Text3 
      Height          =   285
      Left            =   2280
      TabIndex        =   11
      Text            =   "Text1"
      Top             =   2880
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1680
      TabIndex        =   10
      Text            =   "Text1"
      Top             =   1080
      Width           =   735
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   1080
      TabIndex        =   7
      Text            =   "Combo1"
      Top             =   240
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   5175
      Left            =   4920
      TabIndex        =   0
      Top             =   0
      Width           =   2055
      Begin VB.CommandButton Command6 
         Caption         =   "Command6"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   2040
         Width           =   1815
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Command5"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1680
         Width           =   1815
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Command4"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   1320
         Width           =   1815
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Command3"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   1815
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton General 
         Caption         =   "General"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Label Label6 
      Caption         =   "Dedicated Account 2"
      Height          =   375
      Left            =   600
      TabIndex        =   20
      Top             =   1800
      Width           =   2055
   End
   Begin VB.Label Label5 
      Caption         =   "Dedicated Account 3"
      Height          =   375
      Left            =   600
      TabIndex        =   19
      Top             =   2160
      Width           =   2055
   End
   Begin VB.Label Label4 
      Caption         =   "Dedicated Account 4"
      Height          =   375
      Left            =   600
      TabIndex        =   18
      Top             =   2520
      Width           =   2055
   End
   Begin VB.Label Label3 
      Caption         =   "Dedicated Account 5"
      Height          =   375
      Left            =   600
      TabIndex        =   17
      Top             =   2880
      Width           =   2055
   End
   Begin VB.Label Label8 
      Caption         =   "Dedicated Account1"
      Height          =   375
      Left            =   600
      TabIndex        =   15
      Top             =   1440
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Account Value"
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "MSISDN"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "MINSAT_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
