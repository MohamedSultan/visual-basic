VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1245
   ClientLeft      =   5280
   ClientTop       =   2085
   ClientWidth     =   10530
   LinkTopic       =   "Form1"
   ScaleHeight     =   1245
   ScaleWidth      =   10530
   Begin VB.TextBox ID 
      Height          =   375
      Left            =   8760
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   480
      Width           =   1215
   End
   Begin VB.CommandButton GetPath 
      Caption         =   "---"
      Height          =   375
      Left            =   9960
      TabIndex        =   3
      Top             =   0
      Width           =   495
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   120
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   495
      Left            =   3960
      TabIndex        =   1
      Top             =   480
      Width           =   2295
   End
   Begin VB.TextBox File_Path 
      Height          =   285
      Left            =   480
      TabIndex        =   0
      Text            =   "C:\Documents and Settings\msultan\My Documents\MSISDN_RANGES_PARAMETERS_20100114.zip.xlsx"
      Top             =   0
      Width           =   9375
   End
   Begin VB.Label Label1 
      Caption         =   "Path"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub GetPath_Click()
    Dim Directory As String
    If File_Path = "" Then File_Path = "D:\"
    Directory = Get_Dir(File_Path.Text)
    CommonDialog1.InitDir = Directory
    CommonDialog1.CancelError = False
    CommonDialog1.Filter = "All Files (*.*)|*.*|Excel Files(*.xls)|*.xls|Excel Files(*.xlsx)|*.xlsx|Excel Files(*.xls)|*.xlxm"
    CommonDialog1.FilterIndex = 3
    CommonDialog1.ShowOpen
    If CommonDialog1.FileName <> "" Then
        File_Path.Text = CommonDialog1.FileName
    End If
End Sub

Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function

Private Sub Start_Click()
    Dim AppExcel As Excel.Application
    Set AppExcel = New Excel.Application
    
    Dim Result As ADODB.Recordset
    Dim Conn As ADODB.Connection
    
    Set Conn = New ADODB.Connection
    Set Result = New ADODB.Recordset
    
    Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=en7131d;UID=root;PWD=root;DATABASE=group_stats;PORT=3306"
    
    AppExcel.Workbooks.Open File_Path
    
    Dim Step As Integer
    Dim Statement As String
    
    Step = 4
    
    Conn.Open
    
    Dim MSISDN_Data_Loc As MSISDN_Data_Locations
    
    For i = 1 To 12
        With MSISDN_Data_Loc
            Select Case AppExcel.Sheets("SDP").Cells(3, i)
                Case "MSISDN Range From:"
                    .MSISDN_From = i
                Case "MSISDN Range to:"
                    .MSISDN_to = i
                Case "SDP"
                    .SDP = i
                Case "Prepaid OICK/TICK"
                    If AppExcel.Sheets("SDP").Cells(2, i) = "HLR" Then .OICK = i
                Case "Service_key"
                    .Service_key = i
            End Select
        End With
    Next i
    
    While AppExcel.Sheets("SDP").Cells(Step, 1) <> ""
        With MSISDN_Data_Loc
            On Error GoTo LocalHandler
            Statement = QueryBuilder(Insert, AppExcel.Sheets("SDP").Cells(Step, .MSISDN_From), AppExcel.Sheets("SDP").Cells(Step, .MSISDN_to), _
                                    AppExcel.Sheets("SDP").Cells(Step, .Service_key), AppExcel.Sheets("SDP").Cells(Step, .OICK), AppExcel.Sheets("SDP").Cells(Step, .SDP))
            Result.Open Statement, Conn

            Step = Step + 1
            ID = Step
        End With
    Wend
    AppExcel.Close
    Conn.Close
    MsgBox "Done"
LocalHandler:
    If Err.Number = -2147217900 Then
            With MSISDN_Data_Loc
                Statement = QueryBuilder(Update, AppExcel.Sheets("SDP").Cells(Step, .MSISDN_From), AppExcel.Sheets("SDP").Cells(Step, .MSISDN_to), _
                                AppExcel.Sheets("SDP").Cells(Step, .Service_key), AppExcel.Sheets("SDP").Cells(Step, .OICK), AppExcel.Sheets("SDP").Cells(Step, .SDP))
            End With
            Result.Open Statement, Conn
            Resume Next
    End If
    If Err.Number = 0 Or Err.Number = 20 Then Resume Next
End Sub
Private Function QueryBuilder(QueryType As QueryTypes, MSISDN_Start As String, MSISDN_End As String, Service_key As String, OICK As String, SDP_Name As String) As String
    Select Case QueryType
        Case Insert
            QueryBuilder = "INSERT INTO msisdn_ranges(Start,End,SDP_Name,OICK,Service_Key) VALUES ('" + _
                            MSISDN_Start + "','" + MSISDN_End + "','" + SDP_Name + "','" + OICK + "','" + Service_key + "')"
        Case Update
            QueryBuilder = "UPDATE msisdn_ranges SET " + _
                            "SDP_Name='" + SDP_Name + "'," + _
                            "OICK='" + OICK + "'," + _
                            "Service_Key='" + Service_key + "'" + _
                            "WHERE START='" + MSISDN_Start + "' AND END='" + MSISDN_End + "'"
    End Select
End Function

