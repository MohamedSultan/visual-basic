VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form Op_MO_SMS_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "From VF"
   ClientHeight    =   4995
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   18330
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4995
   ScaleWidth      =   18330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   4815
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   5295
      Begin MSChart20Lib.MSChart OP_SMS_Chart 
         Height          =   4575
         Index           =   0
         Left            =   120
         OleObjectBlob   =   "Op_MO_SMS_Form.frx":0000
         TabIndex        =   10
         Top             =   120
         Width           =   4935
      End
   End
   Begin VB.Frame Frame4 
      Height          =   855
      Left            =   16080
      TabIndex        =   5
      Top             =   240
      Width           =   2055
      Begin VB.Timer minutes 
         Interval        =   60000
         Left            =   -120
         Top             =   600
      End
      Begin VB.Timer Seconds 
         Interval        =   1000
         Left            =   1800
         Top             =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.Frame Legend_Frame 
      Height          =   4815
      Index           =   0
      Left            =   15960
      TabIndex        =   4
      Top             =   120
      Width           =   2295
      Begin MSChart20Lib.MSChart Legend 
         Height          =   2655
         Left            =   240
         OleObjectBlob   =   "Op_MO_SMS_Form.frx":1FFD
         TabIndex        =   8
         Top             =   1080
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4815
      Index           =   2
      Left            =   10680
      TabIndex        =   2
      Top             =   120
      Width           =   5295
      Begin MSChart20Lib.MSChart OP_SMS_Chart 
         Height          =   4575
         Index           =   2
         Left            =   120
         OleObjectBlob   =   "Op_MO_SMS_Form.frx":4353
         TabIndex        =   3
         Top             =   120
         Width           =   4935
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4815
      Index           =   1
      Left            =   5400
      TabIndex        =   0
      Top             =   120
      Width           =   5295
      Begin MSChart20Lib.MSChart OP_SMS_Chart 
         Height          =   4575
         Index           =   1
         Left            =   120
         OleObjectBlob   =   "Op_MO_SMS_Form.frx":6350
         TabIndex        =   1
         Top             =   120
         Width           =   4935
      End
   End
End
Attribute VB_Name = "Op_MO_SMS_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer

Private Sub Form_Load()
    CurrentSeconds = 60
End Sub
Private Sub minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    If GetDataInterval = 20 Then
        Call GetOpStats
        GetDataInterval = 0
    End If
End Sub


Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub

