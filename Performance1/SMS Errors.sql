--Sum
select Value,sum(V_Count)
from DC_MO_FAILURE_RES
where to_char(date_time,'YYYY-MM-DD') = '2009-06-01' 
group by Value
order by Sum(V_Count) desc;

select Value,sum(V_Count)
from DC_MO_REJECT_CAUSE
where to_char(date_time,'YYYY-MM-DD') = '2009-06-01' 
group by Value
order by Sum(V_Count) desc;

select Value,sum(V_Count)
from DC_MO_ROUTING_ACTION
where to_char(date_time,'YYYY-MM-DD') = '2009-06-01' 
group by Value
order by Sum(V_Count) desc;

select Value,sum(V_Count)
from DC_MO_SUB_RES
where to_char(date_time,'YYYY-MM-DD') = '2009-06-01' 
group by Value
order by Sum(V_Count) desc;

--Hourly
select to_Char(Date_Time,'HH24'),Value,V_Count
from DC_MO_FAILURE_RES
where to_char(Date_Time,'YYYY-MM-DD') = '2009-06-01' ;


select to_Char(Date_Time,'HH24'),Value,V_Count
from DC_MO_REJECT_CAUSE
where to_char(to_Char(Date_Time,'HH24'),'YYYY-MM-DD') = '2009-06-01' ;


select to_Char(Date_Time,'hh24'),Value,V_Count
from DC_MO_ROUTING_ACTION
where to_char(to_Char(Date_Time,'HH24'),'YYYY-MM-DD') = '2009-06-01' ;


select to_Char(Date_Time,'HH24'),Value,V_Count
from DC_MO_SUB_RES
where to_char(to_Char(Date_Time,'HH24'),'YYYY-MM-DD') = '2009-06-01' ;
