VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form HourlySMS_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MO SMS per Hour"
   ClientHeight    =   9975
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9975
   ScaleWidth      =   15195
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Legend_Frame 
      Height          =   4215
      Index           =   0
      Left            =   12840
      TabIndex        =   31
      Top             =   0
      Width           =   2295
      Begin MSChart20Lib.MSChart Legend 
         Height          =   2775
         Left            =   120
         OleObjectBlob   =   "HourlySMS_Form.frx":0000
         TabIndex        =   32
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame Frame4 
      Height          =   855
      Left            =   5520
      TabIndex        =   28
      Top             =   9120
      Width           =   2055
      Begin VB.Timer Seconds 
         Interval        =   1000
         Left            =   600
         Top             =   0
      End
      Begin VB.Timer minutes 
         Interval        =   60000
         Left            =   -120
         Top             =   0
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   10200
      TabIndex        =   25
      Top             =   9120
      Width           =   2655
      Begin VB.Label Total_Failed_Label 
         Height          =   375
         Left            =   360
         TabIndex        =   26
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame Frame2 
      Height          =   615
      Left            =   720
      TabIndex        =   0
      Top             =   3960
      Width           =   11535
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   15
         Left            =   7200
         TabIndex        =   33
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   375
         Index           =   23
         Left            =   11040
         TabIndex        =   23
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   22
         Left            =   10560
         TabIndex        =   22
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   21
         Left            =   10080
         TabIndex        =   21
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   20
         Left            =   9600
         TabIndex        =   20
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   19
         Left            =   9120
         TabIndex        =   19
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   18
         Left            =   8640
         TabIndex        =   18
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   17
         Left            =   8160
         TabIndex        =   17
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   16
         Left            =   7680
         TabIndex        =   16
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   14
         Left            =   6720
         TabIndex        =   15
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   13
         Left            =   6240
         TabIndex        =   14
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   12
         Left            =   5760
         TabIndex        =   13
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   11
         Left            =   5320
         TabIndex        =   12
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   10
         Left            =   4840
         TabIndex        =   11
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   9
         Left            =   4360
         TabIndex        =   10
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   8
         Left            =   3880
         TabIndex        =   9
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   7
         Left            =   3420
         TabIndex        =   8
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   6
         Left            =   2920
         TabIndex        =   7
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   5
         Left            =   2440
         TabIndex        =   6
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   4
         Left            =   2040
         TabIndex        =   5
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   3
         Left            =   1560
         TabIndex        =   4
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   2
         Left            =   1020
         TabIndex        =   3
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   1
         Left            =   520
         TabIndex        =   2
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   160
         Width           =   255
      End
   End
   Begin MSChart20Lib.MSChart Success_Messages_Chart 
      Height          =   4215
      Left            =   0
      OleObjectBlob   =   "HourlySMS_Form.frx":2356
      TabIndex        =   24
      Top             =   0
      Width           =   12855
   End
   Begin MSChart20Lib.MSChart Failed_SMS_Chart 
      Height          =   4215
      Left            =   120
      OleObjectBlob   =   "HourlySMS_Form.frx":4A9B
      TabIndex        =   27
      Top             =   4920
      Width           =   12855
   End
End
Attribute VB_Name = "HourlySMS_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer

Private Sub Form_Load()
    CurrentSeconds = 60
End Sub

Private Sub minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    If GetDataInterval = 20 Then
        Call HourlyStats
        GetDataInterval = 0
    End If
End Sub


Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub
