VERSION 5.00
Begin VB.Form Watcher 
   Caption         =   "Form1"
   ClientHeight    =   1065
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4470
   LinkTopic       =   "Form1"
   ScaleHeight     =   1065
   ScaleWidth      =   4470
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.Frame Frame5 
      Height          =   855
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   2055
      Begin VB.Timer minutes 
         Interval        =   60000
         Left            =   -120
         Top             =   600
      End
      Begin VB.Timer Seconds 
         Interval        =   1000
         Left            =   1800
         Top             =   600
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   1815
      End
   End
End
Attribute VB_Name = "Watcher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer

Private Sub Form_Load()
    CurrentSeconds = 60
End Sub

Private Sub minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    
    If GetDataInterval = 20 Then
        Days = ""
        If IsProcessRunning("BackGroundStatsCollector_Launcher.exe") = False Then Call Starter.RunStats(Launcher)
        GetDataInterval = 0
    End If
End Sub

Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub
