Attribute VB_Name = "Short_Code_Quaries"
Public Sub GetALL_ShortCode_Stats(Optional DataDate As Date)
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Dim ShortCode_Stats As ShortCode_Stats_Type
    ShortCode_Stats = Get_ShortCode_Transactions(DataDate)
    Call StoreShortCodes(ShortCode_Stats, DataDate)
End Sub
Public Function Get_ShortCode_Transactions(Optional DataDate As Date) As ShortCode_Stats_Type  'Quary As String, conn As Connection) As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn, OSS_SQL)
    Dim Result As ADODB.Recordset
    Dim ShortCode_Stats As ShortCode_Stats_Type
    Dim Step As Integer
    
    Step = 0
    If Year(DataDate) = 1899 Then DataDate = Date
    Dim NumberOfDays As Integer
    NumberOfDays = DateDiff("d", DataDate, Date)
     
    Dim Quary As String
    
    Quary = "select CONVERT(VARCHAR(10), time, 120) Date,Service,sum(Ncalls) Ncalls,sum(nunsucc) nunsucc,sum(nansw) nansw " + _
            "From IVR_services " + _
            "where time >= '" + Format(Date - NumberOfDays, "MM/DD/YYYY") + " 00:00' and time <= '" + Format(Date - NumberOfDays, "MM/DD/YYYY") + " 23:00' " + _
            "group by CONVERT(VARCHAR(10), time, 120),Service "
            
    Quary = "select CONVERT(VARCHAR(10), time, 120) Date,Service,sum(Ncalls) Ncalls,sum(nunsucc) nunsucc,sum(nansw) nansw " + _
            "From IVR_services " + _
            "where time >= '" + Format(Date - NumberOfDays, "MM/DD/YYYY") + "'" + _
            "group by CONVERT(VARCHAR(10), time, 120),Service "
    
    Quary = "select CONVERT(VARCHAR(10), time, 120) Date,Service,sum(Ncalls) Ncalls,sum(nunsucc) nunsucc,sum(nansw) nansw " + _
            "From cnp_oss_db..IVR_services " + _
            "where convert(varchar(10),TIME,120)='2009-06-30' " + _
            "group by CONVERT(VARCHAR(10), time, 120),Service"
    
    
    Set Result = New ADODB.Recordset
    
    
    If Result.State = 1 Then Result.Close
    
    Result.Open Quary, Conn
    
    On Error GoTo LocalHandler
    
    While Not Result.EOF
        ShortCode_Stats.DataFound = True
        ReDim Preserve ShortCode_Stats.ShortCode_Data(Step)
        With ShortCode_Stats.ShortCode_Data(Step)
            .Answered_Calls = Result.Fields("nansw").Value
            .Calls = Result.Fields("Ncalls").Value
            .Date_ = Result.Fields("Date").Value
            .Service = Result.Fields("Service").Value
            .UnSuccessful_Calls = Result.Fields("nunsucc").Value
        End With
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    Conn.Close
    Get_ShortCode_Transactions = ShortCode_Stats
LocalHandler:
    If Err.Number = 94 Or Err.Number = 20 Or Err.Number = 0 Then 'Or Err.Number = -2147217871 Then
        Resume Next
    Else
        MsgBox Err.Description
    End If
    '"Invalid use of Null"
End Function
Public Sub StoreShortCodes(ByRef ShortCode_Data As ShortCode_Stats_Type, Optional DataDate As Date)
    Dim Conn As New ADODB.Connection
    Dim SDPTable, StatementInsert, StatementUpdate As String

    Call DB_Open(Conn, ShortCode_MySQL)
    DataDate_S = Format(DataDate, "YYYY-MM-DD-HH")
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    If Result.State = 1 Then Result.Close
    On Error GoTo LocalHandler
    
    ShortCodeTable = "Short_Codes.services"
    If ShortCode_Data.DataFound Then
        For i = 0 To UBound(ShortCode_Data.ShortCode_Data)
            With ShortCode_Data.ShortCode_Data(i)
                StatementInsert = "INSERT INTO " + ShortCodeTable + " (Date,Service,Ncalls,Nunsucc,Nansw,Not_Answered) " + _
                            "VALUES ('" + .Date_ + "','" + Trim(.Service) + "'," + Trim(Str(.Calls)) + "," + Trim(Str(.UnSuccessful_Calls)) + "," + Trim(Str(.Answered_Calls)) + "," + Trim(Str(.Calls - .Answered_Calls - .UnSuccessful_Calls)) + ")"

                StatementUpdate = "UPDATE " + ShortCodeTable + " SET " + "Ncalls=" + Trim(Str(.Calls)) + _
                                                                            ",Nunsucc=" + Trim(Str(.UnSuccessful_Calls)) + _
                                                                            ",Nansw=" + Trim(Str(.Answered_Calls)) + _
                                                                            ",Not_Answered=" + Trim(Str(.Calls - .UnSuccessful_Calls - .Answered_Calls)) + _
                                                    " WHERE DATE = '" + .Date_ + "' AND service='" + Trim(.Service) + "'"
                Result.Open StatementInsert, Conn
                If Result.State = 1 Then Result.Close
            End With
        Next i
    End If
    Conn.Close
LocalHandler:
    If Err.Number = -2147217900 Then    'Duplicate Data
        Result.Open StatementUpdate, Conn
        Resume Next
    Else
        If Err.Number = 0 Or Err.Number = 20 Then
            Resume Next
        Else
            MsgBox Err.Description
        End If
    End If
End Sub
