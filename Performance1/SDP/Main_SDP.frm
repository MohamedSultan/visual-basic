VERSION 5.00
Begin VB.Form Main_SDP 
   Caption         =   "SDP Form"
   ClientHeight    =   1185
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5490
   LinkTopic       =   "Form1"
   ScaleHeight     =   1185
   ScaleWidth      =   5490
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   1
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "Main_SDP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub Auto_Stats_Click()
    Auto_Stats.Enabled = False
    Auto_Stats.Caption = Format(Date - Val(Days), "dd/mm/yy")
    Call GetALLSDPStats(Date - Val(Days))
    Auto_Stats.Enabled = True
    Auto_Stats.Caption = "All"
End Sub

Private Sub Form_Load()
    'Call Auto_Stats_Click
    'End
End Sub
