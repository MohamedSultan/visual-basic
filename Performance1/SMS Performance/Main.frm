VERSION 5.00
Begin VB.Form Main 
   Caption         =   "SMS Stats"
   ClientHeight    =   4755
   ClientLeft      =   9060
   ClientTop       =   270
   ClientWidth     =   12240
   LinkTopic       =   "Form1"
   ScaleHeight     =   4755
   ScaleWidth      =   12240
   Begin VB.Frame Frame2 
      Height          =   4695
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   12135
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Switches MOC"
         Height          =   375
         Index           =   18
         Left            =   1320
         TabIndex        =   30
         Top             =   3960
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Inbound Roamers"
         Height          =   375
         Index           =   19
         Left            =   5520
         TabIndex        =   29
         Top             =   3480
         Width           =   1695
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "AO"
         Height          =   375
         Index           =   7
         Left            =   5520
         TabIndex        =   13
         Top             =   3240
         Width           =   1455
      End
      Begin VB.Frame Frame3 
         Caption         =   "Othres"
         Height          =   975
         Left            =   5400
         TabIndex        =   28
         Top             =   3000
         Width           =   1935
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MT_MN"
         Enabled         =   0   'False
         Height          =   375
         Index           =   8
         Left            =   1320
         TabIndex        =   27
         Top             =   3240
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MT_ET"
         Enabled         =   0   'False
         Height          =   375
         Index           =   9
         Left            =   1320
         TabIndex        =   26
         Top             =   3600
         Width           =   2055
      End
      Begin VB.Timer Close_Timer 
         Interval        =   5000
         Left            =   120
         Top             =   120
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "SMS Retrials"
         Height          =   375
         Index           =   17
         Left            =   8640
         TabIndex        =   25
         Top             =   2160
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO ET MT VF"
         Height          =   375
         Index           =   16
         Left            =   8640
         TabIndex        =   24
         Top             =   3240
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO MN MT VF"
         Height          =   375
         Index           =   15
         Left            =   8640
         TabIndex        =   23
         Top             =   2880
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Hourly SMS"
         Height          =   375
         Index           =   0
         Left            =   1320
         TabIndex        =   1
         Top             =   2880
         Width           =   2055
      End
      Begin VB.Frame Frame1 
         Height          =   735
         Left            =   5280
         TabIndex        =   20
         Top             =   120
         Width           =   2055
         Begin VB.Timer minutes 
            Interval        =   60000
            Left            =   -240
            Top             =   -240
         End
         Begin VB.Timer Seconds 
            Interval        =   1000
            Left            =   1920
            Top             =   -120
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            Caption         =   "Minutes : Seconds"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Caption         =   "Minutes : Seconds"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   360
            Width           =   1815
         End
      End
      Begin VB.Frame Frame8 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   7800
         TabIndex        =   19
         Top             =   2040
         Width           =   615
         Begin VB.Line Line2 
            BorderWidth     =   2
            DrawMode        =   1  'Blackness
            X1              =   0
            X2              =   1080
            Y1              =   120
            Y2              =   120
         End
      End
      Begin VB.Frame Frame7 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   3720
         TabIndex        =   18
         Top             =   2040
         Width           =   975
         Begin VB.Line Line1 
            BorderWidth     =   2
            DrawMode        =   1  'Blackness
            X1              =   0
            X2              =   1080
            Y1              =   120
            Y2              =   120
         End
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_Int_MT_VF"
         Height          =   375
         Index           =   10
         Left            =   8640
         TabIndex        =   17
         Top             =   1080
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_VF"
         Height          =   375
         Index           =   11
         Left            =   8640
         TabIndex        =   16
         Top             =   1440
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_MN_And_ET_MT_VF"
         Enabled         =   0   'False
         Height          =   375
         Index           =   12
         Left            =   8640
         TabIndex        =   15
         Top             =   1800
         Width           =   2415
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Devices"
         Height          =   375
         Index           =   13
         Left            =   8640
         TabIndex        =   14
         Top             =   2520
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Devices"
         Height          =   375
         Index           =   14
         Left            =   1320
         TabIndex        =   11
         Top             =   2520
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_Applications"
         Enabled         =   0   'False
         Height          =   375
         Index           =   6
         Left            =   1320
         TabIndex        =   10
         Top             =   2160
         Width           =   2175
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_Int"
         Height          =   375
         Index           =   5
         Left            =   1320
         TabIndex        =   9
         Top             =   1800
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_ET"
         Enabled         =   0   'False
         Height          =   375
         Index           =   4
         Left            =   1320
         TabIndex        =   8
         Top             =   1440
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_MN"
         Enabled         =   0   'False
         Height          =   375
         Index           =   3
         Left            =   1320
         TabIndex        =   7
         Top             =   1080
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_Roaming"
         Height          =   375
         Index           =   2
         Left            =   1320
         TabIndex        =   6
         Top             =   720
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO(VF_Local) MT(All)"
         Height          =   375
         Index           =   1
         Left            =   1320
         TabIndex        =   5
         Top             =   360
         Width           =   2055
      End
      Begin VB.Frame Frame5 
         Caption         =   "MO SMS"
         Height          =   4455
         Left            =   1200
         TabIndex        =   12
         Top             =   120
         Width           =   2535
      End
      Begin VB.Frame Frame6 
         Caption         =   "MT SMS"
         Height          =   3255
         Left            =   8400
         TabIndex        =   3
         Top             =   840
         Width           =   2775
      End
      Begin VB.CommandButton OK 
         Caption         =   "OK"
         Height          =   375
         Left            =   5640
         TabIndex        =   2
         Top             =   4000
         Width           =   1215
      End
      Begin VB.Label Label5 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "FTD"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   72
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   4760
         TabIndex        =   4
         Top             =   1320
         Width           =   3015
      End
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer

Private Function DisplayTableData(ByVal TableTime As String, Optional Today As Boolean) As String

    Dim conn As New ADODB.Connection
    Dim MN, ET, VF, All_In, Int_In As New ADODB.Recordset
    
    Call MySQL_DB_Open(conn)
    
    ' Execute the statement.
    Set MN = CreateObject("ADODB.Recordset")
    Set ET = New ADODB.Recordset
    Set VF = New ADODB.Recordset
    Set All_In = New ADODB.Recordset
    Set Int_In = New ADODB.Recordset
    
    On Error GoTo Local_Handler
    
    'Call Get_All_Today_Records
    'VF.Open "select * from current_samples where name = 'MOC ALL messages' ", Conn
    'VF.Open "select * from daily_samples where name = 'MOC ALL messages' and date(timestamp) = date('1/25/2009') ", Conn

    'Todays samples
    If Today Then
        TableTime = ToUnixTime(Date) & "_" & ToUnixTime(Date + 1)
        VF.Open "select * from " & Trim("OP_" & TableTime) & " where operator = 'EG-Vodafone' ", conn
        MN.Open "select * from " & Trim("OP_" & TableTime) & " where operator = 'EG-Mobinil' ", conn
        ET.Open "select * from " & Trim("OP_" & TableTime) & " where operator = 'EG-Etisalat' ", conn
        All_In.Open "select * from " & Trim("MT_FIREWALL_" & TableTime), conn
        Int_In.Open "select * from " & Trim("MT_ECI_" & TableTime), conn
    Else
        VF.Open "select * from " & Trim("OP_" & TableTime) & " where operator = 'EG-Vodafone' ", conn
        MN.Open "select * from " & Trim("OP_" & TableTime) & " where operator = 'EG-Mobinil' ", conn
        ET.Open "select * from " & Trim("OP_" & TableTime) & " where operator = 'EG-Etisalat' ", conn
        All_In.Open "select * from " & Trim("MT_FIREWALL_" & TableTime), conn
        Int_In.Open "select * from " & Trim("MT_ECI_" & TableTime), conn
    End If
    
    DisplayTableData = ""
    If Not VF.EOF Then
        DisplayTableData = "-----------------------------MO-----------------------"
        DisplayTableData = DisplayTableData + vbCrLf + "Total Success VF-Other      = " + CommaSeparator(VF.Fields("mo_ok").Value)
        DisplayTableData = DisplayTableData + vbCrLf + "Error Rate                = " + Str(Round(100 - VF.Fields("mo_ok_rate").Value, 4)) + " %"
    End If
    Dim Record As ADODB.Recordset
    Set Record = VF
    DisplayTableData = DisplayTableData + vbCrLf + DisplayReport(Record, "MT-VF")
    Set Record = MN
    DisplayTableData = DisplayTableData + vbCrLf + DisplayReport(Record, "MT-MN")
    Set Record = ET
    DisplayTableData = DisplayTableData + vbCrLf + DisplayReport(Record, "MT-ET")
    
    Dim All_In_Value As Double
    While Not All_In.EOF
        All_In_Value = All_In.Fields("mo_ok").Value + All_In_Value
        All_In.MoveNext
    Wend
    
    DisplayTableData = DisplayTableData + vbCrLf + "-----------------------------All IN-----------------------"
    DisplayTableData = DisplayTableData + vbCrLf + "From All to VF              = " + CommaSeparator(All_In_Value)
    
    Dim Int_In_Value As Double
    While Not Int_In.EOF
        Int_In_Value = Int_In.Fields("mo_ok").Value + Int_In_Value
        Int_In.MoveNext
    Wend
    DisplayTableData = DisplayTableData + vbCrLf + "-----------------------------All International-----------------------"
    DisplayTableData = DisplayTableData + vbCrLf + "From International to VF              = " + CommaSeparator(Int_In_Value)
    
    DisplayTableData = DisplayTableData + vbCrLf + "-----------------------------All MN and ET-----------------------"
    DisplayTableData = DisplayTableData + vbCrLf + "From ET and MN to VF              = " + CommaSeparator(All_In_Value - Int_In_Value)
    
Local_Handler:
    If Err.Number = -2147217865 Or Err.Number = 0 Or Err.Number = 20 Then
        Resume Next
    Else
        MsgBox Err.Description, , "Error"
    End If
End Function
Private Function HourlySMS_Success() As String
    Dim conn As New ADODB.Connection
    Dim SMS_Record(23) As New ADODB.Recordset
    Dim TempValue, SMS(23, 0) As Double
    
    
    
    Call MySQL_DB_Open(conn)
    
    ' Execute the statement.
    Dim NewI As String
    For i = 0 To 23
        Set SMS_Record(i) = New ADODB.Recordset
        NewI = Trim(Str(i))
        If i < 10 Then NewI = "0" & Trim(Str(i))
        SMS_Record(i).Open "select * from current_samples where name = 'SMS " & NewI & "' ", conn
        TempValue = 0
        While Not SMS_Record(i).EOF
            TempValue = Val(SMS_Record(i).Fields("mo_ok").Value) + TempValue
            SMS_Record(i).MoveNext
        Wend
        SMS(i, 0) = TempValue
    Next i
    Messages_Chart.ChartData = SMS
End Function
Private Function DisplayReport(ByRef Record As ADODB.Recordset, DisplayName As String) As String
    Dim Success, Error_ As Double
    DisplayReport = DisplayReport + vbCrLf + "-----------------------------" + DisplayName + "-----------------------"
    If Not Record.EOF Then
        Success = Record.Fields("mt_ok").Value
        DisplayReport = DisplayReport + vbCrLf + "Number of Successful SMS             = " + CommaSeparator(Success)
        DisplayReport = DisplayReport + vbCrLf + "Error Absance Subs.                  = " + CommaSeparator(Record.Fields("mt_err_as").Value)
        DisplayReport = DisplayReport + vbCrLf + "Error Memory Full                    = " + CommaSeparator(Record.Fields("mt_err_mf").Value)
        
        Error_ = Record.Fields("mt_err_total").Value - Record.Fields("mt_err_mf").Value - Record.Fields("mt_err_as").Value
        DisplayReport = DisplayReport + vbCrLf + "Undefined Error                      = " + CommaSeparator(Error_)
        
        DisplayReport = DisplayReport + vbCrLf + "Error Rate                         = " + Str(Round((Error_ * 100) / Success, 4)) + " %"
    End If
End Function
Private Function CommaSeparator(ByVal Number As Double) As String
    Dim step As Integer
    Dim LongNumber, CommaSeparator_Reversed As String
    LongNumber = Trim(Str(Number))
    
    For i = Len(LongNumber) To 1 Step -1
        If step Mod 3 = 0 Then
            CommaSeparator_Reversed = CommaSeparator_Reversed + "," + Mid(LongNumber, i, 1)
        Else
            CommaSeparator_Reversed = CommaSeparator_Reversed + Mid(LongNumber, i, 1)
        End If
        step = step + 1
    Next i
    
    For i = 2 To Len(CommaSeparator_Reversed)
        CommaSeparator = Mid(CommaSeparator_Reversed, i, 1) + CommaSeparator
    Next i
    CommaSeparator = Trim(CommaSeparator)
End Function

Private Function DISPLAY_Data(Date_ As Date) As Double()
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim TempValue As Double
    Dim SMS_Array(23) As Double
   
   'updating Local Data Base
    Call DB_Open(conn, "XXX")
    
    Set SMS_Record = New ADODB.Recordset
    SMS_Record.Open "select * from HourlySMS_Success where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    
    If Not SMS_Record.EOF Then
        For i = 0 To 23
            NewI = Trim(Str(i))
            If i < 10 Then NewI = "0" & Trim(Str(i))
            SMS_Array(i) = Val(SMS_Record.Fields("SMS" + Trim(NewI)).Value)
        Next i
    End If
    DISPLAY_Data = SMS_Array
End Function

Private Sub Close_Timer_Timer()
    Me.Left = Screen.Width - Me.Width * 1 / 32
    Close_Timer.Enabled = False
End Sub

Private Sub Form_Load()
    Me.Top = 0
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.Left = Screen.Width - Me.Width
    Close_Timer.Enabled = False
    Close_Timer.Enabled = True
    Me.Show
End Sub

Private Sub Form_Terminate()
    End
End Sub
Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Form_MouseMove(Button, Shift, X, Y)
End Sub

Private Sub OK_Click()
    Dim i As Integer
    For i = 0 To Stats_Option.UBound
        If Stats_Option(i).Value = True Then
            
            Select Case i
                Case 0     'Hourly SMS
                    Call DisplayStats(SMS_Hourly, Originating)
                Case 1, 2, 3, 4, 5, 6, 14, 12, 18
                    Call DisplayStats(StatsMap(i), Originating)
                Case 11     'VF-VF
                    Call DisplayStats(StatsMap(i), Other)
                Case 17     'SMS Retrials
                    Call DisplayStats(StatsMap(i), Other)
                Case 10      'Mo Int MT VF
                    Call DisplayStats(StatsMap(i), Originating)
                Case 19     'Inbound Roamers
                    Call DisplayStats(StatsMap(i), Other)
                Case 7, 8, 9, 13, 15, 16
                    Call DisplayStats(StatsMap(i), Terminating)
            End Select
        End If
    Next i
End Sub

Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub
Private Sub minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    If GetDataInterval = 20 Then
        Call OK_Click
        GetDataInterval = 0
    End If
End Sub




