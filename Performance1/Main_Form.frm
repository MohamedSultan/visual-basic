VERSION 5.00
Begin VB.Form Main_Form_ 
   Caption         =   "Background SMS Stats Collector"
   ClientHeight    =   3330
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   12180
   Icon            =   "Main_Form.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3330
   ScaleWidth      =   12180
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton All_command 
      Caption         =   "All"
      Height          =   375
      Left            =   9480
      TabIndex        =   13
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton AO_Stats_Command 
      Caption         =   "AO Stats"
      Height          =   495
      Left            =   9480
      TabIndex        =   12
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Networks_Command 
      Caption         =   "Networks"
      Height          =   495
      Left            =   9480
      TabIndex        =   11
      Top             =   480
      Width           =   1215
   End
   Begin VB.CommandButton AO_Stats_Command1 
      Caption         =   "AO Stats"
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   1920
      Width           =   1215
   End
   Begin VB.CommandButton Operators_Command_IN 
      Caption         =   "Operators_IN"
      Height          =   495
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton Minimizing 
      Caption         =   "_"
      Height          =   255
      Left            =   8760
      TabIndex        =   8
      Top             =   0
      Width           =   255
   End
   Begin VB.CommandButton All_Stats_Command 
      Caption         =   "All Stats"
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   2880
      Width           =   1215
   End
   Begin VB.CommandButton Operators_Command 
      Caption         =   "Operators_Out"
      Height          =   495
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Test 
      Caption         =   "Test"
      Height          =   495
      Left            =   3000
      TabIndex        =   5
      Top             =   360
      Width           =   1215
   End
   Begin VB.CommandButton LocalYesterday 
      Caption         =   "Yesterday Local"
      Height          =   495
      Left            =   120
      TabIndex        =   4
      Top             =   2400
      Width           =   1215
   End
   Begin VB.ListBox Temp_List 
      Height          =   255
      Left            =   1560
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox Status 
      Height          =   2010
      Left            =   2520
      TabIndex        =   2
      Top             =   1080
      Width           =   6255
   End
   Begin VB.CommandButton Hourly_Command 
      Caption         =   "Hourly"
      Height          =   495
      Left            =   9480
      TabIndex        =   1
      Top             =   0
      Width           =   1215
   End
   Begin VB.CommandButton Yesterday 
      Caption         =   "Yesterday Hourly"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   1215
   End
End
Attribute VB_Name = "Main_Form_"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Function DISPLAY_Data(Date_ As Date) As Double()
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim TempValue As Double
    Dim SMS_Array(23) As Double
   
   'updating Local Data Base
    Call DB_Open(conn, "XXX")
    
    Set SMS_Record = New ADODB.Recordset
    SMS_Record.Open "select * from HourlySMS_Success where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    
    If Not SMS_Record.EOF Then
        For i = 0 To 23
            NewI = Trim(Str(i))
            If i < 10 Then NewI = "0" & Trim(Str(i))
            SMS_Array(i) = Val(SMS_Record.Fields("SMS" + Trim(NewI)).Value)
        Next i
    End If
    DISPLAY_Data = SMS_Array
End Function

Public Sub Get_N_Store_Stats(statsType As Statistic_Types, Optional DateOfData As Date)
    Dim SMS As Op_SMS_Type
    SMS = GetMySQL_Stats(statsType, DateOfData)
    Call StoreData_MDB(statsType, SMS, DateOfData)
End Sub
Private Sub All_Stats_Command_Click()
    Call Hourly_Command_Click
    Call Operators_Command_Click
    Call Operators_Command_IN_Click
    Call AO_Stats_Command_Click
End Sub

Private Sub AO_Stats_Command_Click()
    Call AO_Stats
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Me.Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub



Private Sub Hourly_Command_Click()
    Call HourlyStats
End Sub

Private Sub Minimizing_Click()
            'Call Cope_All("Minimized")
            Me.WindowState = 1
            Minimized = True

            Dim i As Integer
            Dim s As String
            Dim nid As NOTIFYICONDATA

            s = "Shortcut program is in System Tray"
            nid = setNOTIFYICONDATA(Me.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Me.Icon, s)
            i = Shell_NotifyIconA(NIM_ADD, nid)

            WindowState = vbMinimized
            Visible = False
End Sub

Private Sub Networks_Command_Click()
    Dim VF_All As Op_SMS_Type
    Dim MN_VF As Op_SMS_Type
    Dim ET_VF As Op_SMS_Type
    
    'VF_All = GetMySQL_Network_Stats("EG-Vodafone")
    'MN_VF = GetMySQL_Network_Stats("EG-Mobinil")
    'ET_VF = GetMySQL_Network_Stats("EG-Etisalat")
    
End Sub

Private Sub Operators_Command_Click()
    Call GetOpStats
End Sub
Private Sub AddSeries(ByRef Series() As String, ByVal Data As String)
    ReDim Preserve Series(UBound(Series) + 1)
    Series(UBound(Series)) = Data
End Sub


Private Sub Operators_Command_IN_Click()
    Call MO_Operators
End Sub

Public Sub DimUndimAll(Dim_ As Boolean)
    For Each Control In Main_Form
        Main_Form.Enabled = Not Dim_
    Next Control
End Sub

Private Sub Test_Click()
    'Dim Test As Application_Stats_Class
    'Set Test = New Application_Stats_Class
End Sub

Private Sub Yesterday_Click()
    Dim conn As ADODB.Connection
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    Dim TableTime As String
    TableTime = "MO_CNT_RULE_" + Trim(Str(ToUnixTime(Date - 1))) & "_" & Trim(Str(ToUnixTime(Date)))
    
    Dim SMS_Record(23) As New ADODB.Recordset
    Dim SMS_Record_2(23) As New ADODB.Recordset
    Dim TempValue As Double
    Dim SMS(23, 1) As Mo_SMS_Type
    Call DB_Open(conn, "my SQL")
    
    ' Execute the statement.
    Dim NewI As String
    For i = 0 To 23
        Set SMS_Record(i) = New ADODB.Recordset
        Set SMS_Record_2(i) = New ADODB.Recordset
        NewI = Trim(Str(i))
        If i < 10 Then NewI = "0" & Trim(Str(i))
        SMS_Record(i).Open "select * from " + TableTime + " where name = 'SMS " & NewI & "' ", conn
        'SMS_Record_2(i).Open "select * from daily_samples where name = 'SMS " & NewI & "' ", Conn
        TempValue = 0
        While Not SMS_Record(i).EOF
            TempValue = Val(SMS_Record(i).Fields("mo_ok").Value) + TempValue
            SMS_Record(i).MoveNext
        Wend
        SMS(i, 1).MO_Success_SMS = TempValue
    Next i
    
    'Messages_Chart.ChartData = SMS
     
    conn.Close
    Call GetLocalStats(SMS, Date - 1, 0)
    Call StoreData("HourlySMS_Success", SMS, 1, Date - 1)
End Sub

