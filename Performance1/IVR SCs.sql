-- IVR Short Codes
select SERVICE,sum(CALLS),sum(UNSUCCESSFUL_CALLS),sum(ANSWERED_CALLS)
from DC_IVR_SERVICE_UTILIZATION
where TO_CHAR(date_time,'YYYY-MM-DD')='2009-05-09'
--order by service asc;
group by SERVICE
order by SERVICE desc

--More SCs
select date_time,name,MSC_Calls,traffic,MSC_UNSUCC_CALLS,TRANSIT_UNSUCC_CALLS,answered_calls,success_Rate,asr,mht--,sum(msc_calls)
from DC_IVR
where to_char(date_time,'YYYY-MM-DD')='2009-04-07'
and name='IVR868'

select name,sum(MSC_Calls),sum(traffic),sum(MSC_UNSUCC_CALLS),sum(TRANSIT_UNSUCC_CALLS),sum(answered_calls),sum(success_Rate),avg(asr),avg(mht)--,sum(msc_calls)
from DC_IVR
where to_char(date_time,'YYYY-MM-DD')='2009-04-07'
group by name
order by sum(MSC_Calls) desc;

select name,sum(MSC_Calls),sum(traffic),sum(MSC_UNSUCC_CALLS),sum(TRANSIT_UNSUCC_CALLS),sum(answered_calls),sum(success_Rate),avg(asr),avg(mht)--,sum(msc_calls)
from DC_IVR
where to_char(date_time,'YYYY-MM')='2009-04'
group by name
order by sum(MSC_Calls) desc;

--Dash Board
select name,concat(round(100-avg(success_rate),2),'%')
from DC_IVR
where to_char(date_time,'YYYY-MM')='2009-04'
group by name
order by sum(MSC_Calls) desc;


select date_time,name,MSC_Calls,traffic,MSC_UNSUCC_CALLS,TRANSIT_UNSUCC_CALLS,answered_calls,success_Rate,asr,mht
from DC_IVR
where to_char(date_time,'YYYY-MM-DD')='2009-05-10'
and name='IVR858'
--and TRANSIT_UNSUCC_CALLS <> '0'
--order by sum(MSC_Calls) desc;