VERSION 5.00
Begin VB.Form Main_Form_RG 
   Caption         =   "RG Stats Collector"
   ClientHeight    =   1320
   ClientLeft      =   13020
   ClientTop       =   1365
   ClientWidth     =   5610
   LinkTopic       =   "Form1"
   ScaleHeight     =   1320
   ScaleWidth      =   5610
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "Main_Form_RG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub Auto_Stats_Click()
    Auto_Stats.Enabled = False
    Auto_Stats.Caption = Format(Date - Val(Days), "dd/mm/yy")
    Call GetALL_RG_Stats(Date - Val(Days))
    Auto_Stats.Enabled = True
    Auto_Stats.Caption = "All"
End Sub

Private Sub Form_Load()
    'Call Me.Auto_Stats_Click
    'Me.Visible = False
    'End
End Sub
Public Sub GetALL_RG_Stats(Optional DataDate As Date)
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Dim RG_Stats As RG_Stats_Type
    RG_Stats = Get_RG_Transactions(DataDate)
    Call StoreRG(RG_Stats, DataDate)
End Sub
Private Function Get_RG_Transactions(DataDate As Date) As RG_Stats_Type
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn, OSS_SQL)
    Dim Result As ADODB.Recordset
    Dim RG_Stats As RG_Stats_Type
    Dim Step As Integer
    
    Step = 0
    If Year(DataDate) = 1899 Then DataDate = Date
    Dim NumberOfDays As Integer
    NumberOfDays = DateDiff("d", DataDate, Date)
     
    Dim Quary As String
    
    Quary = "select * from CNP_Application_DB..RG_Vas_Routes_Data_view " + _
                "where convert(varchar(10),TIME,120)='" + Format(DataDate, "YYYY-MM-DD") + "'"

    Set Result = New ADODB.Recordset
    Result.Open Quary, Conn

    On Error GoTo LocalHandler
    While Not Result.EOF
        ReDim Preserve RG_Stats.Data(Step)
        With RG_Stats.Data(Step)
            .Time = Result.Fields("Time").Value
            .Date = Result.Fields("Date").Value
            .Switch = Result.Fields("Switch").Value
            .Terminating_Node = Result.Fields("Terminating_Node").Value
            .Route = Result.Fields("Route").Value
            .Devices = Result.Fields("Devices").Value
            .Blocked = Result.Fields("Blocked").Value
            .OGTraffic = Result.Fields("OGTraffic").Value
            .ICTraffic = Result.Fields("ICTraffic").Value
            .OGCalls = Result.Fields("OGCalls").Value
            .ICCalls = Result.Fields("ICCalls").Value
            .OGAnswered = Result.Fields("OGAnswered").Value
            .ICAnswered = Result.Fields("ICAnswered").Value
            .Congested = Result.Fields("Congested").Value
            .Traffic = Result.Fields("Traffic").Value
            .Calls = Result.Fields("Calls").Value
            .Answered = Result.Fields("Answered").Value
            .Blocking = Result.Fields("Blocking").Value
            .Utilization = Result.Fields("Utilization").Value
            .IC_ASR = Result.Fields("IC_ASR").Value
            .OG_ASR = Result.Fields("OG_ASR").Value
            .ASR = Result.Fields("ASR").Value
            .Congestion = Result.Fields("Congestion").Value
            .IC_MHT = Result.Fields("IC_MHT").Value
            .OG_MHT = Result.Fields("OG_MHT").Value
            .MHT = Result.Fields("MHT").Value
            .OG_ConnectedCalls = Result.Fields("OG_ConnectedCalls").Value
            .OG_ConnectionRatio = Result.Fields("OG_ConnectionRatio").Value
        End With
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    
    
    Conn.Close
    Get_RG_Transactions = RG_Stats
LocalHandler:
    If Err.Number = 94 Then Resume Next
    '"Invalid use of Null"
End Function
Private Sub StoreRG(ByRef RG_Data As RG_Stats_Type, Optional DataDate As Date)
    Dim Conn As New ADODB.Connection

    Call DB_Open(Conn, RG_MySQL)
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    'Result.Open "DELETE FROM Packages ", Conn
    
    'If Result.State = 1 Then Result.Close
    
    On Error GoTo LocalHandler
    For i = 0 To UBound(RG_Data.Data)
        With RG_Data.Data(i)
            'Statement = "INSERT INTO Packages (PACKAGE_NAME,SUBSCRIPTIONS,Status,ISRENEWABLE) " + _
            '            "VALUES ('" + .PACKAGE_NAME + "','" + Trim(Str(.SUBSCRIPTIONS)) + "','" + .Status + "','" + .ISRENEWABLE + "')"
            Result.Open "INSERT INTO Routes (Time,Date,Switch,Terminating_Node,Route,Devices,Blocked,OGTraffic,ICTraffic,OGCalls,ICCalls,OGAnswered,ICAnswered,Congested,Traffic,Calls,Answered,Blocking,Utilization,IC_ASR,OG_ASR,ASR,Congestion,IC_MHT,OG_MHT,MHT,OG_ConnectedCalls,OG_ConnectionRatio) " + _
                        "VALUES ('" + .Time + "','" + .Date + "','" + .Switch + "','" + .Terminating_Node + "','" + .Route + "','" + .Devices + "','" + .Blocked + "','" + .OGTraffic + "','" + .ICTraffic + "','" + .OGCalls + "','" + .ICCalls + "','" + .OGAnswered + "','" + .ICAnswered + "','" + .Congested + "','" + .Traffic + "','" + .Calls + "','" + .Answered + "','" + .Blocking + "','" + .Utilization + "','" + .IC_ASR + "','" + .OG_ASR + "','" + .ASR + "','" + .Congestion + "','" + .IC_MHT + "','" + .OG_MHT + "','" + .MHT + "','" + .OG_ConnectedCalls + "','" + .OG_ConnectionRatio + "')", Conn
            If Result.State = 1 Then Result.Close
        End With
    Next i


    Conn.Close
LocalHandler:
    Dim Statement1, Statement2 As String
    If Err.Number = -2147217900 Then
        With RG_Data.Data(i)
            Statement1 = "UPDATE routes SET " + _
                            "Devices='" + .Devices + "'," + _
                            "Blocked='" + .Blocked + "'," + _
                            "OGTraffic='" + .OGTraffic + "'," + _
                            "ICTraffic='" + .ICTraffic + "'," + _
                            "OGCalls='" + .OGCalls + "'," + _
                            "ICCalls='" + .ICCalls + "'," + _
                            "OGAnswered='" + .OGAnswered + "'," + _
                            "ICAnswered='" + .ICAnswered + "'," + _
                            "Congested='" + .Congested + "'," + _
                            "Traffic='" + .Traffic + "'," + _
                            "Calls='" + .Calls + "'," + _
                            "Answered='" + .Answered + "'," + _
                            "Blocking='" + .Blocking + "'," + _
                            "Utilization='" + .Utilization + "'," + _
                            "IC_ASR='" + .IC_ASR + "'," + _
                            "OG_ASR='" + .OG_ASR + "'," + _
                            "ASR='" + .ASR + "'," + _
                            "Congestion='" + .Congestion + "'," + _
                            "IC_MHT='" + .IC_MHT + "'," + _
                            "OG_MHT='" + .OG_MHT + "'," + _
                            "MHT='" + .MHT + "'," + _
                            "OG_ConnectedCalls='" + .OG_ConnectedCalls + "'," + _
                            "OG_ConnectionRatio='" + .OG_ConnectionRatio + "' "
            Statement2 = "WHERE " + _
                            "Time='" + .Time + "'" + _
                            "AND Date='" + .Date + "'" + _
                            "AND Switch='" + .Switch + "'" + _
                            "AND Terminating_Node='" + .Terminating_Node + "'" + _
                            "AND Route='" + .Route + "'"
            
            Result.Open Statement1 + Statement2, Conn
        End With
        Resume Next
    Else
        'no nothing
    End If
End Sub



