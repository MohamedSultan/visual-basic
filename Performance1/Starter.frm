VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form Starter 
   Caption         =   "Launcher"
   ClientHeight    =   3795
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   ScaleHeight     =   3795
   ScaleWidth      =   4785
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Display 
      Height          =   1935
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   7
      Top             =   1800
      Width           =   4575
   End
   Begin VB.CommandButton GetStats 
      Caption         =   "Get Stats"
      Height          =   315
      Left            =   3480
      TabIndex        =   6
      Top             =   1440
      Width           =   855
   End
   Begin VB.ComboBox Applications_Combo 
      Height          =   315
      Left            =   480
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1440
      Width           =   2535
   End
   Begin VB.CommandButton Minimize_all 
      Caption         =   "_"
      Height          =   255
      Left            =   4440
      TabIndex        =   3
      Top             =   0
      Width           =   255
   End
   Begin VB.Frame Frame5 
      Height          =   855
      Left            =   1320
      TabIndex        =   0
      Top             =   0
      Width           =   2055
      Begin VB.Timer Seconds 
         Interval        =   1000
         Left            =   1800
         Top             =   600
      End
      Begin VB.Timer minutes 
         Interval        =   60000
         Left            =   -120
         Top             =   600
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1695
      End
   End
   Begin MSForms.ToggleButton Pause 
      Height          =   375
      Left            =   1680
      TabIndex        =   5
      Top             =   960
      Width           =   1215
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "2143;661"
      Value           =   "0"
      Caption         =   "Pause"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "Starter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer
Dim CurrentDate As Date
Dim DataIsCollected As String

Private Sub Form_Load()
    CurrentSeconds = 60
    Me.Icon = Main_Form_FTD.Icon
    Call RunAll
End Sub
Private Sub FileOperation(FileFunction As File_Functions_Enum, FromFile As String, ToFile As String, Optional WhichFiles As ConcatinationStatus_Enum)
    
    
    On Error GoTo LocalHandler
    If FileFunction = FO_DELETE Then
        Select Case WhichFiles
            Case File1
                Kill FromFile
            Case file2
                Kill ToFile
            Case both
                Kill ToFile
                Kill FromFile
        End Select
    Else
        
        Dim lResult As Long, SHF As SHFILEOPSTRUCT
        SHF.hwnd = hwnd
        SHF.wFunc = FileFunction
        SHF.pFrom = FromFile
        SHF.pTo = ToFile
        SHF.fFlags = FOF_FILESONLY
        lResult = SHFileOperation(SHF)
        'If lResult Then
        '    MsgBox "Error occurred!", vbInformation, "SHCOPY"
        'End If
    End If
LocalHandler:
    'Debug.Print Err.Number
    If Err.Number = 53 Then
        If ToFile <> "" And FileFunction = FO_DELETE And CurrentFile = FromFile Then Resume Next
        Exit Sub
    End If
End Sub
Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub GetStats_Click()
    'Dim CurrentStats As Double
    Call RunStats(Applications_Combo.Text)
End Sub
Private Sub RunAll()
    Dim I_File As Double
    Dim ApplicationName() As String
    Dim step As Integer
    I_File = FreeFile
    
    Open App.Path + "\Applications_To_Run.txt" For Input As I_File
        Do While Not EOF(I_File)
            ReDim Preserve ApplicationName(step)
            Line Input #I_File, ApplicationName(step)
            step = step + 1
        Loop
    Close I_File
    
    DoEvents
    DoEvents
    DoEvents
    DoEvents
    
    For i = 0 To UBound(ApplicationName)
        Applications_Combo.AddItem ApplicationName(i)
    Next i
    
    DoEvents
    DoEvents
    
    For i = 0 To UBound(ApplicationName)
        DoEvents
        DoEvents
        DoEvents
        Call RunStats(ApplicationName(i))
        DoEvents
        DoEvents
        DoEvents
    Next i
   'Call RunStats(FTD)
   'Call RunStats(Family)
   'Call RunStats(SDPs)
   'Call RunStats(ShortCodes)
End Sub
Private Sub Add_Comment(NewData As String)
    Display = Display + vbCrLf + Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + "        " + NewData
    Display.SelStart = Len(Display)
End Sub

Public Sub RunStats(ApplicationName As String)
    'FileOperation is executed as symantic deletes the file because its behavior similar to that of a Trojan horse
    
    If Not IsProcessRunning(ApplicationName + ".exe") Then
        If Not FileExists(App.Path + "\Temp\" + ApplicationName + ".exe") Then _
            Call FileOperation(FO_COPY, App.Path + "\" + ApplicationName + ".exe", App.Path + "\Temp\" + ApplicationName + ".exe")
        Call Add_Comment("Getting " + ApplicationName + " Data")
        Call Shell(App.Path + "\Temp\" + ApplicationName + ".exe")
        Call Add_Comment("Finished " + ApplicationName + " Data")
        
    End If
        
        'Case Launcher
         '   Call Shell(App.Path + "\BackGroundStatsCollector_Launcher.exe")
End Sub

Private Sub Pause_Click()
    If Pause.Value = True Then
        Seconds.Enabled = False
        Pause.Caption = "Resume"
    Else
        Seconds.Enabled = True
        Pause.Caption = "Pause"
    End If
End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Me.Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Minimize_all_Click()
    Call Minimizing_Form(Me)
End Sub

Private Sub minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    
    'If Format(CurrentDate, "YYYY-MM-DD-HH") = Format(Date - 1, "YYYY-MM-DD" & "-01") And Not DataIsCollected Then 'Day has Passed
    '    DataIsCollected = True
    '    Call Shell("D:\Work\My Tools\Performance\BackGroundStatsCollectorApplications\BackGroundStatsCollector_Yesterday.exe")
    'Else
    'End If
    
    If GetDataInterval = 20 Then
        Call RunAll
        GetDataInterval = 0
    End If
End Sub

Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub
