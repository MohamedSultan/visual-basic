Attribute VB_Name = "SDP_Quaries"
Public Sub GetALLSDPStats(Optional DataDate As Date)
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Dim SDP_Stats As SDP_Stats_Type
    SDP_Stats = Get_SDP_Transactions(DataDate)
    Call StoreSDP(SDP_Stats, DataDate)
End Sub
Public Sub StoreSDP(ByRef SDP_Data As SDP_Stats_Type, Optional DataDate As Date)
    Dim Conn As New ADODB.Connection
    Dim SDPTable, Statement As String

    Call DB_Open(Conn, SDP_MySQL)
    DataDate_S = Format(DataDate, "YYYY-MM-DD-HH")
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    If Result.State = 1 Then Result.Close
    
    SDPTable = "sdp.sdp_rejections"
    If SDP_Data.RejectionFound Then
        For i = 0 To UBound(SDP_Data.SDP_Rejections)
            With SDP_Data.SDP_Rejections(i)
                Statement = "INSERT INTO " + SDPTable + " (Date,Time,SDP_Name,Rejected_Calls,Events_Calls,BHCA) " + _
                            "VALUES ('" + Mid(.DateTime, 1, Len(.DateTime) - 3) + "','" + Right(.DateTime, 2) + "','" + .SDPName + "'," + .RejectedCalls + "," + .RejectedEvents + "," + Trim(Str(.BHCA)) + ")"
                Result.Open Statement, Conn
                If Result.State = 1 Then Result.Close
            End With
        Next i
    End If
    Conn.Close
End Sub
Public Function Get_SDP_Transactions(Optional DataDate As Date) As SDP_Stats_Type  'Quary As String, conn As Connection) As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn, VNPP)
    Dim Result As ADODB.Recordset
    Dim SDP_Stats As SDP_Stats_Type
    Dim step As Integer
    
    step = 0
    If Year(DataDate) = 1899 Then DataDate = Date
    Dim NumberOfDays As Integer
    NumberOfDays = DateDiff("d", DataDate, Date)
     
    Dim Quary As String
    
    Quary = "select dsc.sdp_name,to_char(dsc.start_date_time,'yyyy-mm-dd-hh24') When,sum(dsc.org_atmp) BHCA,tbl.REJECTED_CALLS,tbl.REJECTION_EVENTS " + _
            "from DC_SDP_CALLS dsc , (select node,to_char(date_time,'yyyy-mm-dd hh24') dte,REJECTED_CALLS,REJECTION_EVENTS " + _
                "From DC_SDP_LOADREGULATION " + _
                "where to_char(date_time,'yyyy-mm-dd') = to_char(sysdate-" + Str(NumberOfDays) + ",'yyyy-mm-dd') and rejected_calls<>0) tbl " + _
            "where to_char(dsc.start_date_time,'yyyy-mm-dd hh24')= tbl.dte  AND dsc.sdp_name=tbl.node " + _
            "group by dsc.sdp_name,dsc.start_date_time,tbl.REJECTED_CALLS,tbl.REJECTION_EVENTS " + _
            "order by dsc.sdp_name,dsc.start_date_time desc"

    Set Result = New ADODB.Recordset
    Result.Open Quary, Conn

    On Error GoTo LocalHandler
    While Not Result.EOF
        SDP_Stats.RejectionFound = True
        ReDim Preserve SDP_Stats.SDP_Rejections(step)
        With SDP_Stats.SDP_Rejections(step)
            .BHCA = Result.Fields("BHCA").Value
            .RejectedCalls = Result.Fields("REJECTED_CALLS").Value
            .RejectedEvents = Result.Fields("REJECTION_EVENTS").Value
            .SDPName = Result.Fields("sdp_name").Value
            .DateTime = Result.Fields("When").Value
        End With
        Result.MoveNext
        step = step + 1
    Wend
    Result.Close
    Conn.Close
    Get_SDP_Transactions = SDP_Stats
LocalHandler:
    If Err.Number = 94 Then Resume Next
    '"Invalid use of Null"
End Function
