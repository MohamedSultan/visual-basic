Attribute VB_Name = "FAF_Module"
Public Sub GetALLFAFStats(Optional DataDate As Date)
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Dim FAF_Stats As FAF_Stats_Type
    FAF_Stats = Get_FAF_Transactions(DataDate)
    Call StoreFAF(FAF_Stats, DataDate)
End Sub
Public Sub StoreFAF(ByRef FAF_Data As FAF_Stats_Type, Optional DataDate As Date)
    Dim Conn As New ADODB.Connection
    Dim FamilyTable As String
    
    'Call DB_Open(conn, Newfamily_MDB)
    Call DB_Open(Conn, Newfamily_MySQL)
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    'If FAF_Data(0).Trans_Date = Format(Date, "yyyy-mm-dd") Then     'Today's Data
    '    FamilyTable = "Todays_Family_Stats"
    '    Result.Open "DELETE * FROM " + FamilyTable, conn
    'Else
        FamilyTable = "FamilyPerformance.Family_Stats"
        Result.Open "DELETE  FROM " + FamilyTable + " WHERE Day='" + FAF_Data.Stats(0).Trans_Date + "'", Conn
        'Result.Open "DELETE * FROM FamilyPerformance.Family_Stats;"
    'End If
    
    If Result.State = 1 Then Result.Close
    
    For i = 0 To UBound(FAF_Data.Stats)
        With FAF_Data.Stats(i)
            Result.Open "INSERT INTO " + FamilyTable + " (Day,Client_Name,Trans_Type,ERROR_CODE,OTHER_ERRORS,Counts) " + _
                        "VALUES ('" + .Trans_Date + "','" + .ClientName + "','" + .Trans_Type + "','" + .Error_Code + "','" + .Other_Error + "','" + .Count + "')", Conn
            If Result.State = 1 Then Result.Close
        End With
    Next i
    Result.Open "UPDATE FAFs SET NumberOfFAFs=" + Str(FAF_Data.NumberOfFAFs) + _
                ",NumberOf_VF_Members=" + Str(FAF_Data.Number_Of_Members.NumberOf_VF_Members) + _
                ",FamilyOf2=" + Str(FAF_Data.Families.FamilyOf2) + _
                ",FamilyOf3=" + Str(FAF_Data.Families.FamilyOf3) + _
                ",FamilyOf4=" + Str(FAF_Data.Families.FamilyOf4) + _
                ",NumberOfNon_VF_Members=" + Str(FAF_Data.Number_Of_Members.NumberOfNoN_VF_Members), Conn
                
    Result.Open "UPDATE Packages SET PKG1=" + Str(FAF_Data.PKGs.PKG1) + _
                ",PKG2=" + Str(FAF_Data.PKGs.PKG2) + _
                ",PKG3=" + Str(FAF_Data.PKGs.PKG3) + _
                ",Others=" + Str(FAF_Data.PKGs.Other)
                
    Result.Open "UPDATE Family_Members SET VF_Members=" + Str(FAF_Data.Number_Of_Members.NumberOf_VF_Members2) + _
                ",MN_Members=" + Str(FAF_Data.Number_Of_Members.NumberOf_MN_Members) + _
                ",ET_Members=" + Str(FAF_Data.Number_Of_Members.NumberOf_ET_Members) + _
                ",LL_Members=" + Str(FAF_Data.Number_Of_Members.NumberOf_LL_Members)
                
    Result.Open "UPDATE members_per_operator SET FamilyOf2=" + Str(FAF_Data.OperatorPackages.VF_Packages.FamilyOf2) + _
                ",FamilyOf3=" + Str(FAF_Data.OperatorPackages.VF_Packages.FamilyOf3) + _
                ",FamilyOf4=" + Str(FAF_Data.OperatorPackages.VF_Packages.FamilyOf4) + _
                " WHERE Operator='VF'"
                
    Result.Open "UPDATE members_per_operator SET FamilyOf2=" + Str(FAF_Data.OperatorPackages.MN_Packages.FamilyOf2) + _
                ",FamilyOf3=" + Str(FAF_Data.OperatorPackages.MN_Packages.FamilyOf3) + _
                ",FamilyOf4=" + Str(FAF_Data.OperatorPackages.MN_Packages.FamilyOf4) + _
                " WHERE Operator='MN'"
                
    Result.Open "UPDATE members_per_operator SET FamilyOf2=" + Str(FAF_Data.OperatorPackages.ET_Packages.FamilyOf2) + _
                ",FamilyOf3=" + Str(FAF_Data.OperatorPackages.ET_Packages.FamilyOf3) + _
                ",FamilyOf4=" + Str(FAF_Data.OperatorPackages.ET_Packages.FamilyOf4) + _
                " WHERE Operator='ET'"
                
    Result.Open "UPDATE members_per_operator SET FamilyOf2=" + Str(FAF_Data.OperatorPackages.LL_Packages.FamilyOf2) + _
                ",FamilyOf3=" + Str(FAF_Data.OperatorPackages.LL_Packages.FamilyOf3) + _
                ",FamilyOf4=" + Str(FAF_Data.OperatorPackages.LL_Packages.FamilyOf4) + _
                " WHERE Operator='LL'"
    Conn.Close
End Sub
Public Function Get_FAF_Transactions(Optional DataDate As Date) As FAF_Stats_Type  'Quary As String, conn As Connection) As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn, NewFamily_Server)
    Dim Result As ADODB.Recordset
    Dim FAF_Stats As FAF_Stats_Type
    Dim step As Integer
    
    step = 0
    If Year(DataDate) = 1899 Then DataDate = Date
    Dim NumberOfDays As Integer
    NumberOfDays = DateDiff("d", DataDate, Date)
     
    Dim Quary As String
    
    Quary = "SELECT TO_CHAR (newall.creation_date, 'YYYY-MM-DD') AS Trans_Date,ftt.transaction_type_name AS Trans_Type," + _
                    "fc.client_name AS Client_Name,COUNT (*) AS Counts, newall.ERROR_CODE AS Errors,newall.other_errors AS Other_Errors " + _
            "FROM family_request_transaction newall," + _
                 "family_transaction_type ftt," + _
                 "family_client fc " + _
            "WHERE fc.Client_ID = newall.Client_ID " + _
                    "AND ftt.transaction_type_id = newall.transaction_type_id " + _
                    "AND TO_CHAR (newall.creation_date, 'YYYY-MM-DD') =" + _
                                                            "TO_CHAR (SYSDATE - " + Str(NumberOfDays) + ", 'YYYY-MM-DD') " + _
            "GROUP BY ftt.transaction_type_name, " + _
                        "fc.client_name, " + _
                        "newall.ERROR_CODE, " + _
                        "TO_CHAR (newall.creation_date, 'YYYY-MM-DD'), " + _
                        "newall.other_errors " + _
            "ORDER BY fc.client_name, ftt.transaction_type_name"

    Set Result = New ADODB.Recordset
    Result.Open Quary, Conn

    On Error GoTo LocalHandler
    While Not Result.EOF
        ReDim Preserve FAF_Stats.Stats(step)
        With FAF_Stats.Stats(step)
            .ClientName = Result.Fields("Client_Name").Value
            .Count = Result.Fields("Counts").Value
            .Error_Code = Result.Fields("Errors").Value
            .Other_Error = Result.Fields("Other_Errors").Value
            .Trans_Date = Result.Fields("Trans_Date").Value
            .Trans_Type = Result.Fields("Trans_Type").Value
        End With
        Result.MoveNext
        step = step + 1
    Wend
    Result.Close
    
    'getting Number Of Current Families
    Result.Open "SELECT COUNT(*) from FAMILY_STRUCTURE", Conn
    If Not Result.EOF Then FAF_Stats.NumberOfFAFs = Val(Result.Fields("COUNT(*)"))
    If Result.State = 1 Then Result.Close
    
    'getting Number Of VF_Members and non VF_Members
    Result.Open "SELECT sum(NON_VODAFONE_COUNT),sum(FAMILY_COUNT-1) from FAMILY_STRUCTURE", Conn
    If Not Result.EOF Then FAF_Stats.Number_Of_Members.NumberOf_VF_Members = Val(Result.Fields("sum(FAMILY_COUNT-1)"))
    If Not Result.EOF Then FAF_Stats.Number_Of_Members.NumberOfNoN_VF_Members = Val(Result.Fields("sum(NON_VODAFONE_COUNT)"))
    If Result.State = 1 Then Result.Close
    
    '-----------------------Members / Operator --------------------------------------------------------------------------
    'VF
    Result.Open "select count(*) " + _
                "From FAMILY_MEMBER " + _
                "where member_msisdn like '2010%' " + _
                "or member_msisdn like '2016%' " + _
                "or member_msisdn like '2019%'", Conn
    If Not Result.EOF Then FAF_Stats.Number_Of_Members.NumberOf_VF_Members2 = Val(Result.Fields("count(*)"))
    If Result.State = 1 Then Result.Close
    
    'MN
    Result.Open "select count(*) " + _
                "From FAMILY_MEMBER " + _
                "where member_msisdn like '2012%' " + _
                "or member_msisdn like '2017%' " + _
                "or member_msisdn like '2018%'", Conn
    If Not Result.EOF Then FAF_Stats.Number_Of_Members.NumberOf_MN_Members = Val(Result.Fields("count(*)"))
    If Result.State = 1 Then Result.Close
    
    'ET
    Result.Open "select count(*) " + _
                "From FAMILY_MEMBER " + _
                "where member_msisdn like '2011%' " + _
                "or member_msisdn like '2014%'", Conn
    If Not Result.EOF Then FAF_Stats.Number_Of_Members.NumberOf_ET_Members = Val(Result.Fields("count(*)"))
    If Result.State = 1 Then Result.Close
    
    'LL
    Result.Open "select count(*) " + _
                "From FAMILY_MEMBER " + _
                "where member_msisdn not like '2010%' " + _
                "and member_msisdn not like '2016%' " + _
                "and member_msisdn not like '2019%' " + _
                "and member_msisdn not like '2012%' " + _
                "and member_msisdn not like '2017%' " + _
                "and member_msisdn not like '2018%' " + _
                "and member_msisdn not like '2011%'", Conn
    If Not Result.EOF Then FAF_Stats.Number_Of_Members.NumberOf_LL_Members = Val(Result.Fields("count(*)"))
    If Result.State = 1 Then Result.Close
    
    '-----------------------MemberCount / Operator --------------------------------------------------------------------------
    'VF
    Result.Open "select FS.family_count,count(FS.family_count) " + _
                "From FAMILY_STRUCTURE FS,FAMILY_MEMBER FM " + _
                "where FM.Family_ID=FS.Family_ID " + _
                "AND (FM.member_msisdn like '2010%' " + _
                "or FM.member_msisdn like '2016%' " + _
                "or FM.member_msisdn like '2019%') " + _
                "group by FS.family_count", Conn
    While Not Result.EOF
        Select Case Val(Result.Fields("family_count"))
            Case 2
                FAF_Stats.OperatorPackages.VF_Packages.FamilyOf2 = Val(Result.Fields("count(FS.family_count)"))
            Case 3
                FAF_Stats.OperatorPackages.VF_Packages.FamilyOf3 = Val(Result.Fields("count(FS.family_count)"))
            Case 4
                FAF_Stats.OperatorPackages.VF_Packages.FamilyOf4 = Val(Result.Fields("count(FS.family_count)"))
        End Select
        Result.MoveNext
    Wend
    If Result.State = 1 Then Result.Close
    
    'MN
    Result.Open "select FS.family_count,count(FS.family_count) " + _
                "From FAMILY_STRUCTURE FS,FAMILY_MEMBER FM " + _
                "where FM.Family_ID=FS.Family_ID " + _
                "AND (FM.member_msisdn like '2012%' " + _
                "or FM.member_msisdn like '2017%' " + _
                "or FM.member_msisdn like '2018%') " + _
                "group by FS.family_count", Conn
    While Not Result.EOF
        Select Case Val(Result.Fields("family_count"))
            Case 2
                FAF_Stats.OperatorPackages.MN_Packages.FamilyOf2 = Val(Result.Fields("count(FS.family_count)"))
            Case 3
                FAF_Stats.OperatorPackages.MN_Packages.FamilyOf3 = Val(Result.Fields("count(FS.family_count)"))
            Case 4
                FAF_Stats.OperatorPackages.MN_Packages.FamilyOf4 = Val(Result.Fields("count(FS.family_count)"))
        End Select
        Result.MoveNext
    Wend
    If Result.State = 1 Then Result.Close
    
    'ET
    Result.Open "select FS.family_count,count(FS.family_count) " + _
                "From FAMILY_STRUCTURE FS,FAMILY_MEMBER FM " + _
                "where FM.Family_ID=FS.Family_ID " + _
                "AND (FM.member_msisdn like '2011%' " + _
                "or FM.member_msisdn like '2014%') " + _
                "group by FS.family_count", Conn
    While Not Result.EOF
        Select Case Val(Result.Fields("family_count"))
            Case 2
                FAF_Stats.OperatorPackages.ET_Packages.FamilyOf2 = Val(Result.Fields("count(FS.family_count)"))
            Case 3
                FAF_Stats.OperatorPackages.ET_Packages.FamilyOf3 = Val(Result.Fields("count(FS.family_count)"))
            Case 4
                FAF_Stats.OperatorPackages.ET_Packages.FamilyOf4 = Val(Result.Fields("count(FS.family_count)"))
        End Select
        Result.MoveNext
    Wend
    If Result.State = 1 Then Result.Close
    
    'LL
    Result.Open "select FS.family_count,count(FS.family_count) " + _
                "From FAMILY_STRUCTURE FS,FAMILY_MEMBER FM " + _
                "where FM.Family_ID=FS.Family_ID " + _
                "AND (FM.member_msisdn not like '2010%' " + _
                "and FM.member_msisdn not like '2016%' " + _
                "and FM.member_msisdn not like '2019%' " + _
                "and FM.member_msisdn not like '2012%' " + _
                "and FM.member_msisdn not like '2017%' " + _
                "and FM.member_msisdn not like '2018%' " + _
                "and FM.member_msisdn not like '2011%') " + _
                "group by FS.family_count", Conn
    While Not Result.EOF
        Select Case Val(Result.Fields("family_count"))
            Case 2
                FAF_Stats.OperatorPackages.LL_Packages.FamilyOf2 = Val(Result.Fields("count(FS.family_count)"))
            Case 3
                FAF_Stats.OperatorPackages.LL_Packages.FamilyOf3 = Val(Result.Fields("count(FS.family_count)"))
            Case 4
                FAF_Stats.OperatorPackages.LL_Packages.FamilyOf4 = Val(Result.Fields("count(FS.family_count)"))
        End Select
        Result.MoveNext
    Wend
    If Result.State = 1 Then Result.Close
    
    '---------------------------------------------------------------------------------------------------------------
    'Getting Packages
    Result.Open "SELECT faf_indicator,count(*) " + _
                "FROM FAMILY_STRUCTURE " + _
                "GROUP BY faf_indicator", Conn
    While Not Result.EOF
        Select Case Result.Fields("faf_indicator")
            Case 701
                FAF_Stats.PKGs.PKG1 = Val(Result.Fields("count(*)"))
            Case 702
                FAF_Stats.PKGs.PKG2 = Val(Result.Fields("count(*)"))
            Case 703
                FAF_Stats.PKGs.PKG3 = Val(Result.Fields("count(*)"))
            Case Else
                FAF_Stats.PKGs.Other = Val(Result.Fields("count(*)"))
        End Select
        Result.MoveNext
    Wend
    If Result.State = 1 Then Result.Close
    
    'Getting Family Counts
    Result.Open "select family_count,count(family_count) " + _
                "From FAMILY_STRUCTURE " + _
                "GROUP BY family_count", Conn
    While Not Result.EOF
        Select Case Result.Fields("family_count")
            Case 2
                FAF_Stats.Families.FamilyOf2 = Val(Result.Fields("count(family_count)"))
            Case 3
                FAF_Stats.Families.FamilyOf3 = Val(Result.Fields("count(family_count)"))
            Case 4
                FAF_Stats.Families.FamilyOf4 = Val(Result.Fields("count(family_count)"))
        End Select
        Result.MoveNext
    Wend
    
    Get_FAF_Transactions = FAF_Stats
    Conn.Close
LocalHandler:
    If Err.Number = 94 Then Resume Next
    '"Invalid use of Null"
End Function
