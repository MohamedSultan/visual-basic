Attribute VB_Name = "Minimizing_Module"
    Public Type NOTIFYICONDATA
        cbSize As Long
        hwnd As Long
        uID As Long
        uFlags As Long
        uCallbackMessage As Long
        hIcon As Long
        szTip As String * 64
    End Type
       
    Public Const NIM_ADD = 0
    Public Const NIM_MODIFY = 1
    Public Const NIM_DELETE = 2
    Public Const NIF_MESSAGE = 1
    Public Const NIF_ICON = 2
    Public Const NIF_TIP = 4

    Public Const WM_MOUSEMOVE = &H200
    Public Const WM_LBUTTONDOWN = &H201
    Public Const WM_LBUTTONUP = &H202
    Public Const WM_LBUTTONDBLCLK = &H203
    Public Const WM_RBUTTONDOWN = &H204
    Public Const WM_RBUTTONUP = &H205
    Public Const WM_RBUTTONDBLCLK = &H206
    Public Const WM_MBUTTONDOWN = &H207
    Public Const WM_MBUTTONUP = &H208
    Public Const WM_MBUTTONDBLCLK = &H209
Public Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Public Declare Function Shell_NotifyIconA Lib "SHELL32" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Integer
Public Function setNOTIFYICONDATA(hwnd As Long, ID As Long, Flags As Long, CallbackMessage As Long, Icon As Long, Tip As String) As NOTIFYICONDATA
    Dim nidTemp As NOTIFYICONDATA

    nidTemp.cbSize = Len(nidTemp)
    nidTemp.hwnd = hwnd
    nidTemp.uID = ID
    nidTemp.uFlags = Flags
    nidTemp.uCallbackMessage = CallbackMessage
    nidTemp.hIcon = Icon
    nidTemp.szTip = Tip & Chr$(0)

    setNOTIFYICONDATA = nidTemp
End Function
Public Sub Cope_All(ByVal Case_ As String)
    Select Case Case_
        Case "Minimized"
            Commands.WindowState = 1
            Config.WindowState = 1
            Display_Form.WindowState = 1
            Favorite_File.WindowState = 1
            Favorite_File.WindowState = 1
            Get_Len_Form.WindowState = 1
            Organized.WindowState = 1
            Peri_Form.WindowState = 1
            Set_Wait_Time.WindowState = 1
            Set_Wait_Time_Detailed.WindowState = 1
            Start_Path.WindowState = 1
            VASP.WindowState = 1
            VASP_Types.WindowState = 1
        Case "Normal"
            Commands.WindowState = 0
            Config.WindowState = 0
            Display_Form.WindowState = 0
            Favorite_File.WindowState = 0
            Favorite_File.WindowState = 0
            Get_Len_Form.WindowState = 0
            Organized.WindowState = 0
            Peri_Form.WindowState = 0
            Set_Wait_Time.WindowState = 0
            Set_Wait_Time_Detailed.WindowState = 0
            Start_Path.WindowState = 0
            VASP.WindowState = 0
            VASP_Types.WindowState = 0
    End Select
End Sub
