VERSION 5.00
Begin VB.Form Main_Form_TITO 
   Caption         =   "TITO Stats Collector"
   ClientHeight    =   1320
   ClientLeft      =   13020
   ClientTop       =   1365
   ClientWidth     =   5610
   LinkTopic       =   "Form1"
   ScaleHeight     =   1320
   ScaleWidth      =   5610
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "Main_Form_TITO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub Auto_Stats_Click()
    Auto_Stats.Enabled = False
    Auto_Stats.Caption = Format(Date - Val(Days), "dd/mm/yy")
    Call GetALL_TITO_Stats(Date - Val(Days))
    Auto_Stats.Enabled = True
    Auto_Stats.Caption = "All"
End Sub

Private Sub Form_Load()
'    Call Me.Auto_Stats_Click
'    Me.Visible = False
'    End
End Sub
Public Sub GetALL_TITO_Stats(Optional DataDate As Date)
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Dim TITO_Stats As TITO_Stats_Type
    TITO_Stats = Get_TITO_Transactions(DataDate)
    Call StoreTITO(TITO_Stats, DataDate)
End Sub
Private Function Get_TITO_Transactions(DataDate As Date) As TITO_Stats_Type
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn, TITO_Oracle)
    Dim Result As ADODB.Recordset
    Dim TITO_Stats As TITO_Stats_Type
    Dim Step As Integer
    
    Step = 0
    If Year(DataDate) = 1899 Then DataDate = Date
    Dim NumberOfDays As Integer
    NumberOfDays = DateDiff("d", DataDate, Date)
     
    Dim Quary As String
    
    Quary = "select to_char(transaction_date,'YYYY-MM-DD') Data_Date,TYPE_ID,Type_Description,Status,response_Code,error_reason,response_description,Client_ID, count(*) Count " + _
            "From TRANSACTIONSVIEW " + _
            "where to_char(transaction_date,'YYYY-MM-DD')>='" + Format(DataDate, "YYYY-MM-DD") + "' " + _
            "group by to_char(transaction_date,'YYYY-MM-DD'),TYPE_ID,Type_Description,Status,response_Code,error_reason,response_description,Client_ID"

    Set Result = New ADODB.Recordset
    Result.Open Quary, Conn

    On Error GoTo LocalHandler
    While Not Result.EOF
        ReDim Preserve TITO_Stats.Data(Step)
        With TITO_Stats.Data(Step)
            .CLIENT_ID = Result.Fields("CLIENT_ID").Value
            .Count = Result.Fields("Count").Value
            .ERROR_REASON = Result.Fields("ERROR_REASON").Value
            .RESPONSE_CODE = Result.Fields("RESPONSE_CODE").Value
            .RESPONSE_DESCRIPTION = Result.Fields("RESPONSE_DESCRIPTION").Value
            .Status = Result.Fields("Status").Value
            .TRANSACTION_DATE = Result.Fields("Data_Date").Value
            .TYPE_DESCRIPTION = Result.Fields("TYPE_DESCRIPTION").Value
            .TYPE_ID = Result.Fields("TYPE_ID").Value
        End With
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    
    Step = 0
    Quary = "select * " + _
            "From PACKAGES "
            
    Result.Open Quary, Conn

    'On Error GoTo LocalHandler
    While Not Result.EOF
        ReDim Preserve TITO_Stats.TITO_Packages(Step)
        With TITO_Stats.TITO_Packages(Step)
            .ISRENEWABLE = Result.Fields("ISRENEWABLE").Value
            .PACKAGE_NAME = Result.Fields("PACKAGE_NAME").Value
            .Status = Result.Fields("Status").Value
            .SUBSCRIPTIONS = Result.Fields("SUBSCRIPTIONS").Value
        End With
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    
    Conn.Close
    Get_TITO_Transactions = TITO_Stats
LocalHandler:
    If Err.Number = 94 Then Resume Next
    '"Invalid use of Null"
End Function
Private Sub StoreTITO(ByRef TITO_Data As TITO_Stats_Type, Optional DataDate As Date)
    Dim Conn As New ADODB.Connection

    Call DB_Open(Conn, TITO_MySql)
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    
    Result.Open "DELETE FROM Packages ", Conn
    
    If Result.State = 1 Then Result.Close
    
    For i = 0 To UBound(TITO_Data.TITO_Packages)
        With TITO_Data.TITO_Packages(i)
            'Statement = "INSERT INTO Packages (PACKAGE_NAME,SUBSCRIPTIONS,Status,ISRENEWABLE) " + _
            '            "VALUES ('" + .PACKAGE_NAME + "','" + Trim(Str(.SUBSCRIPTIONS)) + "','" + .Status + "','" + .ISRENEWABLE + "')"
            Result.Open "INSERT INTO Packages (PACKAGE_NAME,SUBSCRIPTIONS,Status,ISRENEWABLE) " + _
                        "VALUES ('" + .PACKAGE_NAME + "','" + Trim(Str(.SUBSCRIPTIONS)) + "','" + .Status + "','" + .ISRENEWABLE + "')", Conn
            If Result.State = 1 Then Result.Close
        End With
    Next i
    If Result.State = 1 Then Result.Close
    
    Dim Statement As String
    On Error GoTo LocalHandler
    For i = 0 To UBound(TITO_Data.Data)
        With TITO_Data.Data(i)
            Statement = "INSERT INTO TITO_Stats (CLIENT_ID,Count,ERROR_REASON,RESPONSE_CODE,RESPONSE_DESCRIPTION,Status,TRANSACTION_DATE,TYPE_DESCRIPTION,TYPE_ID) " + _
                        "VALUES ('" + .CLIENT_ID + "','" + Trim(Str(.Count)) + "','" + .ERROR_REASON + "','" + .RESPONSE_CODE + "','" + .RESPONSE_DESCRIPTION + "','" + _
                                .Status + "','" + .TRANSACTION_DATE + "','" + .TYPE_DESCRIPTION + "','" + .TYPE_ID + "')"
            Result.Open Statement, Conn
        End With
    Next i

    Conn.Close
LocalHandler:
    If Err.Number = -2147217900 Then
        With TITO_Data.Data(i)
            Statement = "UPDATE TITO_Stats SET " + _
                            "Count='" + Trim(Str(.Count)) + "'," + _
                            "ERROR_REASON='" + .ERROR_REASON + "'," + _
                            "RESPONSE_DESCRIPTION='" + .RESPONSE_DESCRIPTION + "'," + _
                            "TYPE_DESCRIPTION='" + .TYPE_DESCRIPTION + "'" + _
                        "WHERE TRANSACTION_DATE='" + .TRANSACTION_DATE + "' " + _
                                "AND RESPONSE_CODE='" + .RESPONSE_CODE + "' " + _
                                "AND Status='" + .Status + "' " + _
                                "AND TYPE_ID='" + .TYPE_ID + "' " + _
                                "AND CLIENT_ID='" + .CLIENT_ID + "' "
            Result.Open Statement, Conn
        End With
        Resume Next
    Else
        'no nothing
    End If
End Sub



