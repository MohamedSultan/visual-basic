Attribute VB_Name = "DB_Quaries_Enhanced"
Private Sub Add_FAF_Status(ByRef AllStatuses() As FAF_Statuses_Type, New_FAF_Data As FAF_Status_Type, _
                            TRANSACTION_TYPE_ID As Integer) ', ClientID As Integer)
   
    Dim Found As Boolean
    Dim FoundID As Integer
'    On Error GoTo LocalHandler
    
    For j = 0 To UBound(AllStatuses)
        If TRANSACTION_TYPE_ID = AllStatuses(j).FAF_Type_ID Then
            For i = 0 To UBound(AllStatuses(j).FAF_Details)
                If AllStatuses(j).FAF_Details(i).Status = New_FAF_Data.Status And _
                    AllStatuses(j).FAF_Details(i).Status <> "" Then
                    Found = True
                    FoundID = i
                End If
            Next i
            If Found Then
                AllStatuses(j).FAF_Details(FoundID).Count = AllStatuses(j).FAF_Details(FoundID).Count + 1
            Else
                ReDim Preserve AllStatuses(j).FAF_Details(UBound(AllStatuses(j).FAF_Details) + 1)
                AllStatuses(j).FAF_Details(UBound(AllStatuses(j).FAF_Details)).Count = 1
                AllStatuses(j).FAF_Details(UBound(AllStatuses(j).FAF_Details)).Status = New_FAF_Data.Status
            End If
            If New_FAF_Data.Status = "0" Then   'Success
                AllStatuses(j).Success_Count = AllStatuses(j).Success_Count + 1
            Else
                AllStatuses(j).Failed_Count = AllStatuses(j).Failed_Count + 1
            End If
        End If
    Next j

End Sub
Public Sub DB_Open(ByRef Conn As ADODB.Connection, ByVal DB_Type As DB_Types)
    Set Conn = New ADODB.Connection
    Select Case DB_Type
        Case 0
            Dim DB_File_Name As String
            ' Get the data.
            DB_File_Name = App.Path
            If Right$(DB_File_Name, 1) <> "\" Then DB_File_Name = DB_File_Name & "\"
            DB_File_Name = DB_File_Name & "Daily SMS.mdb"
            
            If App.Path <> "D:\Work\My Tools\Performance" Then DB_File_Name = "D:\Work\My Tools\Performance\Daily SMS.mdb"
            ' Open a connection.
            Conn.ConnectionString = _
                "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=" & DB_File_Name & ";" '& _
                '"Persist Security Info=False"
            ''Conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & Transaction_File_Name & ";Persist Security Info=False"
            ''Conn.ConnectionString = "Provider=MSDAORA.1 ; Password=myPassword; User ID=myUser ; Data Source = ORCL; Persist Security Info=True"
            ''Conn = "DRIVER={ORACLE ODBC DRIVER};SERVER=Service name;UID=id;PWD=password;DBQ=Service name;DBA=W;APA=T;FEN=T;QTO=T;FRC=10;FDL=10;LOB=T;RST=T;FRL=F;MTS=F;CSR=F;PFC=10;TLO=O;"
            'Conn.ConnectionString = "Provider=MSDAORA.1 ; Data Source=VNPP; Password=Sulsat_DB; User ID=Sulsat_DB ; Persist Security Info=True"
        Case TextStat
            Conn.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=TextStat"
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=root;PWD=lokal$;DATABASE=stats;PORT=3306;Server=172.30.37.113"
        Case DailySMS_MDB
            Conn.ConnectionString = _
                "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=D:\Work\My Tools\Performance\Daily SMS.mdb"
        Case Newfamily_MDB
            Conn.ConnectionString = _
                "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=D:\Work\My Tools\Performance\Family Performance\New family.mdb"
        Case NewFamily_Server
            Conn.ConnectionString = "Provider=MSDAORA.1 ; Data Source=NEW_FAMLY; Password=FAMILY; User ID=FAMILY ; Persist Security Info=True"
        Case Newfamily_MySQL
            Conn.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=Family"
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=Family;PWD=Family;DATABASE=FamilyPerformance;PORT=3306"
        Case SMS_MySQL
            Conn.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=SMS"
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=SMS;PWD=SMS;DATABASE=sms;PORT=3306"
        Case VNPP
           Conn.ConnectionString = "Provider=MSDAORA.1 ; Password=VFE_VAS_PP; User ID=VFE_VAS_PP ; Data Source = VNPP; Persist Security Info=True"
        Case SDP_MySQL
            Conn.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=SDP"
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=SDP;PWD=SDP;DATABASE=SDP;PORT=3306"
        Case ShortCode_MySQL
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=Short_Codes;PWD=Short_Codes;DATABASE=Short_Codes;PORT=3306"
        Case OSS_SQL
            Conn.ConnectionString = "DRIVER=SQL Server;SERVER=10.230.97.31;DATABASE=CNP_OSS_DB;Trusted_Connection=Yes"
            Conn.CommandTimeout = 20 * 60   '10 Min
        Case TITO_Oracle
            Conn.ConnectionString = "Provider=MSDAORA.1 ; Password=TITO_User; User ID=TITO_User ; Data Source = TITO; Persist Security Info=True"
            'Conn.ConnectionString = "Provider=MSDAORA.1 ; Password=TITO_User; User ID=TITO_User ; Host = 172.28.201.21;SID = AIWADRD1; Persist Security Info=True"
        Case TITO_MySql
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=TITO;PWD=TITO;DATABASE=TITO;PORT=3306"
        Case RG_MySQL
            Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;UID=Routes;PWD=Routes;DATABASE=Routes;PORT=3306"
    End Select
    Conn.Open

'LocalHandler:
'    If Err.Number = -2147467259 Or Err.Number = 20 Or Err.Number = 0 Then
'        Resume Next
'    End If
End Sub
Private Function MySqlDateFunction(DateOfData As Date)
    MySqlDateFunction = "(DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + _
                    Str(DateDiff("d", DateOfData, Date)) + _
                    " DAY)))"
End Function
Private Function Get_MySQL_MO_MT_SelectStatement() As String
    Get_MySQL_MO_MT_SelectStatement = "mo_ok," + _
                                        "mo_dropped," + _
                                        "mo_err_to, " + _
                                        "mo_err_mc, " + _
                                        "mo_err_total, " + _
                                        "mo_total, " + _
                                        "mo_unknown, " + _
                                        "mt_ok, " + _
                                        "mt_err_to, " + _
                                        "mt_err_as, " + _
                                        "mt_err_mf, " + _
                                        "mt_err_mc "
End Function
Public Function GetSelectStatement(Stats_Type As Statistic_Types, DateOfData As Date) As String
    Dim Statement As String
    Select Case Stats_Type
        Case Inbound_Roamers
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                                "FROM stats.daily_samples daily_samples " + _
                                "WHERE name='inbound roamers msgs' AND type='MT_CNT_RULE' AND " + MySqlDateFunction(DateOfData)
        Case MO_VF_Local
        
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                                "FROM daily_samples WHERE (operator='EG-Vodafone') AND " + MySqlDateFunction(DateOfData)
        Case MO_VF_Roaming
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                                "FROM stats.daily_samples daily_samples " + _
                                "WHERE (name='PBC') AND (type = 'ECI_MO_COUNTER') AND " + MySqlDateFunction(DateOfData)
        Case MO_VF_MT_MN
        Case MO_VF_MT_ET
        Case MO_VF_MT_Int
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                            "FROM daily_samples WHERE (name='MOC all mo to International') " + _
                            "AND (type='MO_CNT_RULE') " + _
                            "AND (name='MOC all mo to International') " + _
                            "AND " + MySqlDateFunction(DateOfData)
        Case MO_VF_MT_Applications
        Case AO
            Statement = "SELECT " + _
                Get_MySQL_MO_MT_SelectStatement + _
                "FROM daily_samples " + _
                "WHERE (code IS NOT NULL) " + _
                "AND (type = 'COUNTER') " + _
                "AND (operator IS NULL) " + _
                "AND (country IS NULL) " + _
                "AND " + MySqlDateFunction(DateOfData)
        Case MT_MN
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                        "FROM daily_samples WHERE (operator='EG-Mobinil') AND " + MySqlDateFunction(DateOfData)
        Case MT_ET
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                        "FROM daily_samples WHERE (operator='EG-Etisalat') AND " + MySqlDateFunction(DateOfData)
        Case MO_Int_MT_VF        'MT_Int_VF
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                    "FROM daily_samples " + _
                    "WHERE (type = 'ECI_MT_COUNTER') AND " + MySqlDateFunction(DateOfData)
        'Case MO_VF_Mt_VF
        Case MO_MN_ET_Int_MT_VF
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                            "FROM daily_samples " + _
                            "WHERE (type = 'TPF_MT_COUNTER') AND " + MySqlDateFunction(DateOfData)
        Case MO_Devices, MT_Devices
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                            "FROM daily_samples " + _
                            "WHERE (name = '_TOTAL') AND " + _
                            "(type='COUNTER') AND " + MySqlDateFunction(DateOfData)
        Case MO_MN_MT_VF
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                            "FROM daily_samples " + _
                            "WHERE (name = 'CNT messages from Mobinil') AND " + MySqlDateFunction(DateOfData)
        Case MO_ET_MT_VF
            Statement = "SELECT " + Get_MySQL_MO_MT_SelectStatement + _
                            "FROM daily_samples " + _
                            "WHERE (name = 'CNT messages from Etisalat') AND " + MySqlDateFunction(DateOfData)
        Case Device_Checker
            Statement = "select count(distinct(device)) " + _
                        "From daily_samples"
    End Select
    GetSelectStatement = Statement
End Function
Public Function GetMySQL_Stats(Optional Stats_Type As Statistic_Types, Optional DateOfData As Date) As Op_SMS_Type

    'Gets details about each operator (Mo and MT)

    Dim Conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim Statement As String
    
    Call DB_Open(Conn, TextStat)
    
    Set SMS_Record_Temp = New ADODB.Recordset
    
    If Year(DateOfData) = 1899 Then DateOfData = Date

    Set SMS_Record = New ADODB.Recordset

    Statement = GetSelectStatement(Stats_Type, DateOfData)
    
    SMS_Record.Open Statement, Conn
            
    While Not SMS_Record.EOF
        GetMySQL_Stats.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("mo_ok").Value) + GetMySQL_Stats.Mo_SMS.MO_Success_SMS
        GetMySQL_Stats.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("mo_dropped").Value) + GetMySQL_Stats.Mo_SMS.MO_Decimated_SMS
        GetMySQL_Stats.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("mo_err_mc").Value) + GetMySQL_Stats.Mo_SMS.MO_misc_Error_SMS
        GetMySQL_Stats.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("mo_err_to").Value) + GetMySQL_Stats.Mo_SMS.MO_TimeOut_SMS
        GetMySQL_Stats.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("mo_unknown").Value) + GetMySQL_Stats.Mo_SMS.MO_Unknown_SMS
        GetMySQL_Stats.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("mt_ok").Value) + GetMySQL_Stats.Mt_SMS.Mt_Success_SMS
        GetMySQL_Stats.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("mt_err_as").Value) + GetMySQL_Stats.Mt_SMS.Mt_Abs_Sub
        GetMySQL_Stats.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("mt_err_mf").Value) + GetMySQL_Stats.Mt_SMS.Mt_Mem_Full
        GetMySQL_Stats.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("mt_err_mc").Value) + GetMySQL_Stats.Mt_SMS.Mt_misc_Error_SMS
        GetMySQL_Stats.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("mt_err_to").Value) + GetMySQL_Stats.Mt_SMS.Mt_TimeOut_SMS
        SMS_Record.MoveNext
    Wend
'    Next I
    If SMS_Record.State = 1 Then SMS_Record.Close
    Conn.Close
    
End Function
Public Function StatsMap(StatValue As Integer) As Statistic_Types
    Select Case StatValue
        Case 1
            StatsMap = MO_VF_Local
        Case 2
            StatsMap = MO_VF_Roaming
        Case 3
            'StatsMap =MO_VF_MT_MN
        Case 4
            'StatsMap =MO_VF_MT_ET
        Case 5
            StatsMap = MO_VF_MT_Int
        Case 6
            'StatsMap =MO_VF_MT_Applications
        Case 7
            StatsMap = AO
        Case 8
            StatsMap = MT_MN
        Case 9
            StatsMap = MT_ET
        Case 10
            StatsMap = MO_Int_MT_VF
        Case 11
            StatsMap = MO_VF_Mt_VF
        Case 12
             StatsMap = MO_MN_ET_Int_MT_VF
        Case 13
            StatsMap = MO_Devices
        Case 14
            StatsMap = MT_Devices
        Case 15
            StatsMap = MO_MN_MT_VF
        Case MO_ET_MT_VF
            StatsMap = MO_ET_MT_VF
        Case 17
            StatsMap = SMS_Retrials
        Case 18
            StatsMap = Inbound_Roamers
        'Case 19
        '    StatsMap = Inbound_Roamers
    End Select
End Function
Public Function StatsName(Stats_Type As Statistic_Types) As String
    Select Case Stats_Type
        Case MO_VF_Local
            StatsName = "MO_VF_Local"
        Case MO_VF_Roaming
            StatsName = "MO_VF_Roaming"
        Case 3
            'StatsName ="MO_VF_MT_MN
        Case 4
            'StatsName ="MO_VF_MT_ET
        Case MO_VF_MT_Int
            StatsName = "MO_VF_MT_Int"
        Case 6
            'StatsName ="MO_VF_MT_Applications
        Case AO
            StatsName = "AO"
        Case MT_MN
            StatsName = "MT_MN"
        Case MT_ET
            StatsName = "MT_ET"
        Case MO_MN_ET_Int_MT_VF
            StatsName = "MO_MN_ET_Int_MT_VF"
        Case MO_Int_MT_VF
            StatsName = "MO_Int_MT_VF"
        Case MO_VF_Mt_VF
            StatsName = "MO_VF_Mt_VF"
        Case MO_Devices, MT_Devices
            StatsName = "Devices"
        Case MO_MN_MT_VF
            StatsName = "MO_MN_MT_VF"
        Case MO_ET_MT_VF
            StatsName = "MO_ET_MT_VF"
        'Case 18
        '    StatsName = "MOC_Switches"
        Case Inbound_Roamers
            StatsName = "Inbound_Roamers"
    End Select
End Function
Public Sub StoreData_MDB(statsType As Statistic_Types, ByRef SMS As Op_SMS_Type, Optional DataDate As Date)
    'Storing Data
    Dim Conn As New ADODB.Connection
    Dim TableName, DataDate_S, Statment_Insert, Statment_Update, SET_, Fields, SetValues, Values As String
    Dim TableNames(4), Statment_Inserts(), Statment_Updates() As String
                
    TableName = StatsName(statsType)
    
    If Year(DataDate) = 1899 Then
        DataDate_S = Format(Date, "yyyy-mm-dd")
    Else
        DataDate_S = Format(DataDate, "yyyy-mm-dd")
    End If
    
    Select Case statsType
        Case MSC_Messages_MC
            Fields = "Date, " + _
                        "Switch_Name, " + _
                        "MO_Decimated_SMS, " + _
                        "MO_misc_Error_SMS, " + _
                        "MO_Success_SMS, " + _
                        "MO_TimeOut_SMS, " + _
                        "MO_Unknown_SMS, " + _
                        "Mt_Abs_Sub, " + _
                        "Mt_Mem_Full, " + _
                        "Mt_misc_Error_SMS, " + _
                        "Mt_Success_SMS, " + _
                        "Mt_TimeOut_SMS "
            
            On Error GoTo Switches_Handler
            ReDim Statment_Inserts(UBound(SMS.SwitchSMS) - 1)
            ReDim Statment_Updates(UBound(SMS.SwitchSMS) - 1)
            
            For i = 1 To UBound(SMS.SwitchSMS)
                Values = "'" + DataDate_S + "', " + _
                            "'" + SMS.SwitchSMS(i).SwitchName + "', " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMO.MO_Decimated_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMO.MO_misc_Error_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMO.MO_Success_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMO.MO_TimeOut_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMO.MO_Unknown_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_Abs_Sub) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_Mem_Full) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_misc_Error_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_Success_SMS) + ", " + _
                             Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_TimeOut_SMS)
                                     
                 SET_ = "MO_Success_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMO.MO_Success_SMS) + "," + _
                             "MO_misc_Error_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMO.MO_misc_Error_SMS) + ", " + _
                             "MO_TimeOut_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMO.MO_TimeOut_SMS) + ", " + _
                             "MO_Unknown_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMO.MO_Unknown_SMS) + ", " + _
                             "MO_Decimated_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMO.MO_Decimated_SMS) + ", " + _
                             "Mt_Abs_Sub =" + Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_Abs_Sub) + "," + _
                             "Mt_Mem_Full =" + Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_Mem_Full) + ", " + _
                             "Mt_misc_Error_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_misc_Error_SMS) + ", " + _
                             "Mt_Success_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_Success_SMS) + ", " + _
                             "Mt_TimeOut_SMS =" + Str(SMS.SwitchSMS(i).SwitchDataMT.Mt_TimeOut_SMS)
                
                Statment_Inserts(i - 1) = "INSERT INTO switches ( " + Fields + ") " + _
                                            "VALUES (" + Values + ")"
                Statment_Updates(i - 1) = "UPDATE switches SET " + SET_ + " WHERE Date = '" + DataDate_S + "' AND Switch_Name = '" + SMS.SwitchSMS(i).SwitchName + "'"
            
            Next i
        Case SMS_Hourly
            ReDim Statment_Inserts(4)
            ReDim Statment_Updates(4)
            TableNames(0) = "HourlySMS_Decimated"
            TableNames(1) = "HourlySMS_Misc"
            TableNames(2) = "HourlySMS_Success"
            TableNames(3) = "HourlySMS_TimeOut"
            TableNames(4) = "HourlySMS_Unknown"
            
            Dim Tables, Hours As Integer
            Dim NewHours As String
            
            For Tables = 0 To 4
                Fields = "Date_"
                Values = "'" + DataDate_S + "'"
                SET_ = ""
                For Hours = 0 To 23
                    NewHours = Trim(Str(Hours))
                    If Hours < 10 Then NewHours = "0" + Trim(Str(NewHours))
                    Fields = Fields + ",SMS" + NewHours
                    Select Case Tables
                        Case 0
                            Values = Values + "," + Str(SMS.HourlySMS(Hours).MO_Decimated_SMS)
                            SET_ = SET_ + "SMS" + NewHours + "=" + Str(SMS.HourlySMS(Hours).MO_Decimated_SMS) + ","
                        Case 1
                            Values = Values + "," + Str(SMS.HourlySMS(Hours).MO_misc_Error_SMS)
                            SET_ = SET_ + "SMS" + NewHours + "=" + Str(SMS.HourlySMS(Hours).MO_misc_Error_SMS) + ","
                        Case 2
                            Values = Values + "," + Str(SMS.HourlySMS(Hours).MO_Success_SMS)
                            SET_ = SET_ + "SMS" + NewHours + "=" + Str(SMS.HourlySMS(Hours).MO_Success_SMS) + ","
                        Case 3
                            Values = Values + "," + Str(SMS.HourlySMS(Hours).MO_TimeOut_SMS)
                            SET_ = SET_ + "SMS" + NewHours + "=" + Str(SMS.HourlySMS(Hours).MO_TimeOut_SMS) + ","
                        Case 4
                            Values = Values + "," + Str(SMS.HourlySMS(Hours).MO_Unknown_SMS)
                            SET_ = SET_ + "SMS" + NewHours + "=" + Str(SMS.HourlySMS(Hours).MO_Unknown_SMS) + ","
                    End Select
                Next Hours
                SET_ = Left(SET_, Len(SET_) - 1)
                Statment_Inserts(Tables) = "INSERT INTO " + TableNames(Tables) + " ( " + Fields + ") " + _
                                            "VALUES (" + Values + ")"
                Statment_Updates(Tables) = "UPDATE " + TableNames(Tables) + " SET " + SET_ + " WHERE Date_ = '" + DataDate_S + "'"
            Next Tables
            
        Case Else
            Fields = "Date_, " + _
                        "MO_Success_SMS, " + _
                        "MO_Decimated_SMS, " + _
                        "MO_Unknown_SMS, " + _
                        "MO_misc_Error_SMS, " + _
                        "MO_TimeOut_SMS, " + _
                        "Mt_Success_SMS, " + _
                        "Mt_Abs_Sub, " + _
                        "Mt_Mem_Full, " + _
                        "Mt_misc_Error_SMS, " + _
                        "Mt_TimeOut_SMS"
        
            Values = Str(SMS.Mo_SMS.MO_Success_SMS) + ", " + _
                        Str(SMS.Mo_SMS.MO_Decimated_SMS) + ", " + _
                        Str(SMS.Mo_SMS.MO_Unknown_SMS) + ", " + _
                        Str(SMS.Mo_SMS.MO_misc_Error_SMS) + ", " + _
                        Str(SMS.Mo_SMS.MO_TimeOut_SMS) + ", " + _
                        Str(SMS.Mt_SMS.Mt_Success_SMS) + ", " + _
                        Str(SMS.Mt_SMS.Mt_Abs_Sub) + ", " + _
                        Str(SMS.Mt_SMS.Mt_Mem_Full) + ", " + _
                        Str(SMS.Mt_SMS.Mt_misc_Error_SMS) + ", " + _
                        Str(SMS.Mt_SMS.Mt_TimeOut_SMS)
                                
            SET_ = "MO_Decimated_SMS =" + Str(SMS.Mo_SMS.MO_Decimated_SMS) + "," + _
                        "MO_misc_Error_SMS =" + Str(SMS.Mo_SMS.MO_misc_Error_SMS) + "," + _
                        "MO_Success_SMS =" + Str(SMS.Mo_SMS.MO_Success_SMS) + ", " + _
                        "MO_TimeOut_SMS =" + Str(SMS.Mo_SMS.MO_TimeOut_SMS) + ", " + _
                        "MO_Unknown_SMS =" + Str(SMS.Mo_SMS.MO_Unknown_SMS) + ", " + _
                        "Mt_Success_SMS =" + Str(SMS.Mt_SMS.Mt_Success_SMS) + ", " + _
                        "Mt_Abs_Sub =" + Str(SMS.Mt_SMS.Mt_Abs_Sub) + ", " + _
                        "Mt_Mem_Full =" + Str(SMS.Mt_SMS.Mt_Mem_Full) + ", " + _
                        "Mt_TimeOut_SMS =" + Str(SMS.Mt_SMS.Mt_TimeOut_SMS) + ", " + _
                        "Mt_misc_Error_SMS =" + Str(SMS.Mt_SMS.Mt_misc_Error_SMS)
                    
            Statment_Insert = "INSERT INTO " + TableName + " (" + Fields + ") " + _
                                "VALUES ( '" + DataDate_S + "'," + Values + ") "
            Statment_Update = "UPDATE " + TableName + " SET " + SET_ + " WHERE DATE_ = '" + DataDate_S + "'"
    End Select
    'updating Local Data Base
    'Call DB_Open(conn, DailySMS_MDB)
    Call DB_Open(Conn, SMS_MySQL)
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset

    On Error GoTo Handler
    Select Case statsType
        'Case MSC_Messages
        Case SMS_Hourly, MSC_Messages_MC
            For i = 0 To UBound(Statment_Inserts) ' - 1
                Result.Open Statment_Inserts(i), Conn
            Next i
        Case Else
            Result.Open Statment_Insert, Conn
    End Select

    'Call Add_Status("Data 'VF-All' from FTD is Successfully Stored ")

'    Call Main_Form_FTD.DimUndimAll(False)

Switches_Handler:
    If Err.Number = 9 Then
        Exit Sub
    Else
        If Err.Number = 20 Or Err.Number = 0 Then
            Resume Next
        Else
            MsgBox Err.Description
        End If
    End If

Handler:
    If Err.Number = -2147467259 Or Err.Number = -2147217900 Then   'row exist

        Select Case statsType
            Case SMS_Hourly, MSC_Messages_MC
                Result.Open Statment_Updates(i), Conn
                Resume Next
            Case Else
                Result.Open Statment_Update, Conn
        End Select

        Conn.Close
'        Call Add_Status("Data " + TableName + " from FTD is Successfully Updated ")
        
'        Call Main_Form_FTD.DimUndimAll(False)
        
    Else
        If Err.Number = 20 Or Err.Number = 0 Then
            Resume Next
        Else
            MsgBox Err.Description
'            Call Main_Form_FTD.DimUndimAll(False)
        End If
    End If
End Sub
Public Sub Generate_Data_Report(statsType As Statistic_Types, ByRef SMS() As Op_SMS_Type, FromDate As Date, ToDate As Date)
    Dim SMSQuary As ADODB.Recordset
    Dim NumberOfDays As Integer
    Dim TableName, DataDate_S As String
    Dim Conn As New ADODB.Connection
    Set Conn = New ADODB.Connection
    Set SMSQuary = New ADODB.Recordset
    Dim proceed As Boolean
    
    NumberOfDays = DateDiff("d", FromDate, ToDate)
    'ReDim Preserve SMSQuary(NumberOfDays)
    ReDim SMS(NumberOfDays)
    
    TableName = StatsName(statsType)
    
    Call DB_Open(Conn, DailySMS_MDB)
    For i = 0 To NumberOfDays
        'Set SMSQuary(i) = New ADODB.Recordset
        DataDate_S = Format(FromDate + i, "dd/mm/yy")
        Select Case statsType
            Case SMS_Hourly
                Dim TableNames(4) As String
                Dim SMS_Hours(23, 4) As Double
                
                TableNames(0) = "HourlySMS_Decimated"
                TableNames(1) = "HourlySMS_Misc"
                TableNames(2) = "HourlySMS_Success"
                TableNames(3) = "HourlySMS_TimeOut"
                TableNames(4) = "HourlySMS_Unknown"
                
                For Tables = 0 To UBound(TableNames)
                    SMSQuary.Open "SELECT * FROM " + TableNames(Tables) + " WHERE Date_='" + DataDate_S + "'", Conn
                    While Not SMSQuary.EOF
                        For Hours = 0 To 23
                            NewHours = Trim(Str(Hours))
                            If Hours < 10 Then NewHours = "0" + Trim(Str(Hours))
                            SMS_Hours(Hours, Tables) = Val(SMSQuary.Fields("SMS" + Trim(NewHours)).Value)
                        Next Hours
                        SMSQuary.MoveNext
                    Wend
                    SMSQuary.Close
                Next Tables
                ReDim Preserve SMS(i).HourlySMS(23)
                'Dim Hour_Failed As Double
                For Hours = 0 To 23
                '    Hour_Failed = 0
                '    For j = 0 To 4
                '        If j <> 2 Then Hour_Failed = SMS_Hours(Hours, j) + Hour_Failed
                '    Next j
                    SMS(i).HourlySMS(Hours).MO_Decimated_SMS = SMS_Hours(Hours, 0)
                    SMS(i).HourlySMS(Hours).MO_misc_Error_SMS = SMS_Hours(Hours, 1)
                    SMS(i).HourlySMS(Hours).MO_Success_SMS = SMS_Hours(Hours, 2)
                    SMS(i).HourlySMS(Hours).MO_TimeOut_SMS = SMS_Hours(Hours, 3)
                    SMS(i).HourlySMS(Hours).MO_Unknown_SMS = SMS_Hours(Hours, 4)
                Next Hours
            Case SMS_Retrials
                Dim FW_Quary_, DevicesMT, MN, ET, International, AO_VF_Quary, VF_Local_Quary, VF_Roamer_Quary, VF_Int_Quary, VF_MN_Quary, VF_ET_Quary As ADODB.Recordset
                Set VF_Local_Quary = New ADODB.Recordset
                Set AO_VF_Quary = New ADODB.Recordset
                Set VF_Roamer_Quary = New ADODB.Recordset
                Set VF_Int_Quary = New ADODB.Recordset
                Set VF_MN_Quary = New ADODB.Recordset
                Set VF_ET_Quary = New ADODB.Recordset
                Set FW_Quary_ = New ADODB.Recordset
                Set MN = New ADODB.Recordset
                Set ET = New ADODB.Recordset
                Set International = New ADODB.Recordset
                Set DevicesMT = New ADODB.Recordset
                
                DevicesMT.Open "SELECT * FROM " + StatsName(MT_Devices) + " WHERE Date_='" + DataDate_S + "'", Conn
                FW_Quary_.Open "SELECT * FROM " + StatsName(MO_MN_ET_Int_MT_VF) + " WHERE Date_='" + DataDate_S + "'", Conn
                'MN.Open "SELECT * FROM " + StatsName(MO_MN_MT_VF) + " WHERE Date_='" + DataDate_S + "'", conn
                'ET.Open "SELECT * FROM " + StatsName(MO_ET_MT_VF) + " WHERE Date_='" + DataDate_S + "'", conn
                'International.Open "SELECT * FROM " + StatsName(MO_Int_MT_VF) + " WHERE Date_='" + DataDate_S + "'", conn
                AO_VF_Quary.Open "SELECT * FROM " + StatsName(AO) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_Local_Quary.Open "SELECT * FROM " + StatsName(MO_VF_Local) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_Roamer_Quary.Open "SELECT * FROM " + StatsName(MO_VF_Roaming) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_Int_Quary.Open "SELECT * FROM " + StatsName(MO_VF_MT_Int) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_MN_Quary.Open "SELECT * FROM " + StatsName(MT_MN) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_ET_Quary.Open "SELECT * FROM " + StatsName(MT_ET) + " WHERE Date_='" + DataDate_S + "'", Conn
                
                'SMS Retrials=(MT Devices)-[(AO)+(MO MN-MT VF)+(MO ET-MT VF)+(vf local)+(vf roamer)-(vf-int)-(vf-mn)-(vf-et)-(vf-app "missing")]
                '=======================================================================================================================
                If Not DevicesMT.EOF And Not AO_VF_Quary.EOF And Not FW_Quary_.EOF _
                     And Not VF_Roamer_Quary.EOF And Not VF_Int_Quary.EOF And Not VF_MN_Quary.EOF And Not VF_ET_Quary.EOF Then
                    SMS(i).Mo_SMS.MO_Success_SMS = Val(DevicesMT.Fields("MT_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(AO_VF_Quary.Fields("Mt_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(FW_Quary_.Fields("Mt_Success_SMS").Value)
                    'SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(MN.Fields("Mt_Success_SMS").Value)
                    'SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(ET.Fields("Mt_Success_SMS").Value)
                    'SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(International.Fields("MO_Success_SMS").Value)
                
                    'Manupulating VF-VF
                    Dim VF_VF_Temp As Double
                    VF_VF_Temp = Val(VF_Local_Quary.Fields("MO_Success_SMS").Value)
                    VF_VF_Temp = VF_VF_Temp + Val(VF_Roamer_Quary.Fields("MO_Success_SMS").Value)
                    VF_VF_Temp = VF_VF_Temp - Val(VF_Int_Quary.Fields("Mo_Success_SMS").Value)
                    VF_VF_Temp = VF_VF_Temp - Val(VF_MN_Quary.Fields("Mt_Success_SMS").Value)
                    VF_VF_Temp = VF_VF_Temp - Val(VF_ET_Quary.Fields("Mt_Success_SMS").Value)
                
                    SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - VF_VF_Temp
                Else
                    SMS(i).Mo_SMS.MO_Success_SMS = 0
                End If
                VF_Local_Quary.Close
                VF_Roamer_Quary.Close
                VF_Int_Quary.Close
                VF_MN_Quary.Close
                VF_ET_Quary.Close
                FW_Quary_.Close
                'MN.Close
                'ET.Close
                'International.Close
                DevicesMT.Close
            Case Inbound_Roamers
                Dim FW As ADODB.Recordset
                Set FW = New ADODB.Recordset
                Set VF_Int_Quary = New ADODB.Recordset
                Set VF_MN_Quary = New ADODB.Recordset
                Set VF_ET_Quary = New ADODB.Recordset
                
                FW.Open "SELECT * FROM " + StatsName(MO_MN_ET_Int_MT_VF) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_Int_Quary.Open "SELECT * FROM " + StatsName(MO_VF_MT_Int) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_MN_Quary.Open "SELECT * FROM " + StatsName(MO_MN_MT_VF) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_ET_Quary.Open "SELECT * FROM " + StatsName(MO_ET_MT_VF) + " WHERE Date_='" + DataDate_S + "'", Conn
                
                'Inbound Roamers=(Total FireWall)-(vf-int)-(vf-mn)-(vf-et)
                '=========================================================
                If Not FW.EOF And Not VF_Int_Quary.EOF And Not VF_MN_Quary.EOF And Not VF_ET_Quary.EOF Then
                    SMS(i).Mo_SMS.MO_Success_SMS = Val(FW.Fields("MO_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(VF_Int_Quary.Fields("Mo_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(VF_MN_Quary.Fields("Mt_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(VF_ET_Quary.Fields("Mt_Success_SMS").Value)
                Else
                    SMS(i).Mo_SMS.MO_Success_SMS = 0
                End If
                
                FW.Close
                VF_Int_Quary.Close
                VF_MN_Quary.Close
                VF_ET_Quary.Close
            Case MO_VF_Mt_VF
                'Dim VF_Local_Quary, VF_Roamer_Quary, VF_Int_Quary, VF_MN_Quary, VF_ET_Quary As adodb.Recordset
                Set VF_Local_Quary = New ADODB.Recordset
                Set VF_Roamer_Quary = New ADODB.Recordset
                Set VF_Int_Quary = New ADODB.Recordset
                Set VF_MN_Quary = New ADODB.Recordset
                Set VF_ET_Quary = New ADODB.Recordset
                
                VF_Local_Quary.Open "SELECT * FROM " + StatsName(MO_VF_Local) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_Roamer_Quary.Open "SELECT * FROM " + StatsName(MO_VF_Roaming) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_Int_Quary.Open "SELECT * FROM " + StatsName(MO_VF_MT_Int) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_MN_Quary.Open "SELECT * FROM " + StatsName(MT_MN) + " WHERE Date_='" + DataDate_S + "'", Conn
                VF_ET_Quary.Open "SELECT * FROM " + StatsName(MT_ET) + " WHERE Date_='" + DataDate_S + "'", Conn
                
'                While Not VF_Local_Quary.EOF And VF_Roamer_Quary.EOF And VF_Int_Quary.EOF And VF_MN_Quary.EOF And VF_ET_Quary.EOF
                'Dim Temp_SMS As Double
                
                'VF-VF=(vf local)+(vf roamer)-(vf-int)-(vf-mn)-(vf-et)-(vf-app "missing")
                '=========================================================================
                If Not VF_Local_Quary.EOF Then SMS(i).Mo_SMS.MO_Success_SMS = Val(VF_Local_Quary.Fields("MO_Success_SMS").Value)
                If Not VF_Roamer_Quary.EOF Then SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS + Val(VF_Roamer_Quary.Fields("MO_Success_SMS").Value)
                If Not VF_Int_Quary.EOF Then SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(VF_Int_Quary.Fields("Mo_Success_SMS").Value)
                If Not VF_MN_Quary.EOF Then SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(VF_MN_Quary.Fields("Mt_Success_SMS").Value)
                If Not VF_ET_Quary.EOF Then SMS(i).Mo_SMS.MO_Success_SMS = SMS(i).Mo_SMS.MO_Success_SMS - Val(VF_ET_Quary.Fields("Mt_Success_SMS").Value)
                
                VF_Local_Quary.Close
                VF_Roamer_Quary.Close
                VF_Int_Quary.Close
                VF_MN_Quary.Close
                VF_ET_Quary.Close
'                Wend
'                SMSQuary.Close
            Case MSC_Messages_MC
                SMSQuary.Open "SELECT * FROM " + TableName + " WHERE Date_SwitchName LIKE '" + DataDate_S + "'", Conn
                While Not SMSQuary.EOF
                    SMS(i).Mo_SMS.MO_Success_SMS = Val(SMSQuary.Fields("MO_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Decimated_SMS = Val(SMSQuary.Fields("MO_Decimated_SMS").Value)
                    SMS(i).Mo_SMS.MO_misc_Error_SMS = Val(SMSQuary.Fields("MO_misc_Error_SMS").Value)
                    SMS(i).Mo_SMS.MO_TimeOut_SMS = Val(SMSQuary.Fields("MO_TimeOut_SMS").Value)
                    SMS(i).Mo_SMS.MO_Unknown_SMS = Val(SMSQuary.Fields("MO_Unknown_SMS").Value)
                    SMS(i).Mt_SMS.Mt_Success_SMS = Val(SMSQuary.Fields("Mt_Success_SMS").Value)
                    SMS(i).Mt_SMS.Mt_Abs_Sub = Val(SMSQuary.Fields("Mt_Abs_Sub").Value)
                    SMS(i).Mt_SMS.Mt_Mem_Full = Val(SMSQuary.Fields("Mt_Mem_Full").Value)
                    SMS(i).Mt_SMS.Mt_misc_Error_SMS = Val(SMSQuary.Fields("Mt_misc_Error_SMS").Value)
                    SMS(i).Mt_SMS.Mt_TimeOut_SMS = Val(SMSQuary.Fields("Mt_TimeOut_SMS").Value)
                    SMSQuary.MoveNext
                Wend
                SMSQuary.Close
            Case Else
                SMSQuary.Open "SELECT * FROM " + TableName + " WHERE Date_='" + DataDate_S + "'", Conn
                While Not SMSQuary.EOF
                    SMS(i).Mo_SMS.MO_Success_SMS = Val(SMSQuary.Fields("MO_Success_SMS").Value)
                    SMS(i).Mo_SMS.MO_Decimated_SMS = Val(SMSQuary.Fields("MO_Decimated_SMS").Value)
                    SMS(i).Mo_SMS.MO_misc_Error_SMS = Val(SMSQuary.Fields("MO_misc_Error_SMS").Value)
                    SMS(i).Mo_SMS.MO_TimeOut_SMS = Val(SMSQuary.Fields("MO_TimeOut_SMS").Value)
                    SMS(i).Mo_SMS.MO_Unknown_SMS = Val(SMSQuary.Fields("MO_Unknown_SMS").Value)
                    SMS(i).Mt_SMS.Mt_Success_SMS = Val(SMSQuary.Fields("Mt_Success_SMS").Value)
                    SMS(i).Mt_SMS.Mt_Abs_Sub = Val(SMSQuary.Fields("Mt_Abs_Sub").Value)
                    SMS(i).Mt_SMS.Mt_Mem_Full = Val(SMSQuary.Fields("Mt_Mem_Full").Value)
                    SMS(i).Mt_SMS.Mt_misc_Error_SMS = Val(SMSQuary.Fields("Mt_misc_Error_SMS").Value)
                    SMS(i).Mt_SMS.Mt_TimeOut_SMS = Val(SMSQuary.Fields("Mt_TimeOut_SMS").Value)
                    SMSQuary.MoveNext
                Wend
                SMSQuary.Close
            End Select
    Next i
    
    
End Sub
Public Function GetData_MDB(statsType As Statistic_Types, Optional DataDate As Date) As Op_SMS_Type
    'Storing Data
    Dim Conn As New ADODB.Connection
    Dim TableName, DataDate_S, Statment, Statment_Insert, Statment_Update, SET_, Fields, SetValues, Values As String
    
    TableName = StatsName(statsType)
    
    If Year(DataDate) = 1899 Then
        DataDate_S = Format(Date, "dd/mm/yy")
    Else
        DataDate_S = Format(DataDate, "dd/mm/yy")
    End If
    
    Call DB_Open(Conn, DailySMS_MDB)
    
    Dim SMS_Record As ADODB.Recordset
    Set SMS_Record = New ADODB.Recordset

    On Error GoTo Handler
    SMS_Record.Open "SELECT * FROM " + TableName + " WHERE Date_='" + DataDate_S + "'", Conn
    
    While Not SMS_Record.EOF
        GetData_MDB.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("MO_Success_SMS").Value)
        GetData_MDB.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("MO_Decimated_SMS").Value)
        GetData_MDB.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("MO_misc_Error_SMS").Value)
        GetData_MDB.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("MO_TimeOut_SMS").Value)
        GetData_MDB.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("MO_Unknown_SMS").Value)
        GetData_MDB.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("Mt_Success_SMS").Value)
        GetData_MDB.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("Mt_Abs_Sub").Value)
        GetData_MDB.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("Mt_Mem_Full").Value)
        GetData_MDB.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("Mt_misc_Error_SMS").Value)
        GetData_MDB.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("Mt_TimeOut_SMS").Value)
        SMS_Record.MoveNext
    Wend

Handler:
    If Err.Number = -2147467259 Then     'table exist

        Result.Open Statment_Update, Conn

        Conn.Close
'        Call Add_Status("Data " + TableName + " from FTD is Successfully Updated ")
        
'        Call Main_Form_FTD.DimUndimAll(False)
        
    Else
        If Err.Number = 20 Or Err.Number = 0 Then
            Resume Next
        Else
            MsgBox Err.Description
'            Call Main_Form_FTD.DimUndimAll(False)
        End If
    End If
End Function
Public Function GetMySQLStats_Switches(SwitchType As SwitchType, Optional DataDate As Date) As Switch_type()
    Dim Conn As New ADODB.Connection
    Dim SMS_Record, SMS_Record_Temp As New ADODB.Recordset
    Dim TempValue As Switch_type
    Dim MOC_Stats() As Switch_type
    ReDim MOC_Stats(0)
    Call DB_Open(Conn, TextStat)
    
    If Year(DataDate) = 1899 Then DataDate = Date
    
    ' Execute the statement.
    Set SMS_Record = New ADODB.Recordset
    
    On Error GoTo LocalHandler
    If SwitchType = MOC Then _
        SMS_Record.Open "select * from daily_samples where name LIKE 'MOC MSC%' " + _
                                                                    "And (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DataDate, Date)) + " DAY)))", _
                                                                    Conn
    If SwitchType = MTC Then _
        SMS_Record.Open "select * from daily_samples where name LIKE 'MTC MSC%' " + _
                                                                    "And (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DataDate, Date)) + " DAY)))", _
                                                                    Conn
    TempValue.SwitchName = ""
    TempValue.SwitchDataMO.MO_Success_SMS = 0
    TempValue.SwitchDataMO.MO_Decimated_SMS = 0
    TempValue.SwitchDataMO.MO_misc_Error_SMS = 0
    TempValue.SwitchDataMO.MO_TimeOut_SMS = 0
    TempValue.SwitchDataMO.MO_Unknown_SMS = 0
    TempValue.SwitchDataMT.Mt_Abs_Sub = 0
    TempValue.SwitchDataMT.Mt_Mem_Full = 0
    TempValue.SwitchDataMT.Mt_misc_Error_SMS = 0
    TempValue.SwitchDataMT.Mt_Success_SMS = 0
    TempValue.SwitchDataMT.Mt_TimeOut_SMS = 0
    
    While Not SMS_Record.EOF
        TempValue.SwitchName = SMS_Record.Fields("name").Value
        TempValue.SwitchDataMO.MO_Success_SMS = Val(SMS_Record.Fields("mo_ok").Value) ' + TempValue.MO_Success_SMS
        TempValue.SwitchDataMO.MO_Decimated_SMS = Val(SMS_Record.Fields("mo_dropped").Value) '+ TempValue.MO_Decimated_SMS
        TempValue.SwitchDataMO.MO_misc_Error_SMS = Val(SMS_Record.Fields("mo_err_mc").Value) '+ TempValue.MO_misc_Error_SMS
        TempValue.SwitchDataMO.MO_TimeOut_SMS = Val(SMS_Record.Fields("mo_err_to").Value) '+ TempValue.MO_TimeOut_SMS
        TempValue.SwitchDataMO.MO_Unknown_SMS = Val(SMS_Record.Fields("mo_unknown").Value) '+ TempValue.MO_Unknown_SMS
        
        TempValue.SwitchDataMT.Mt_Abs_Sub = Val(SMS_Record.Fields("mt_err_as").Value)
        TempValue.SwitchDataMT.Mt_Mem_Full = Val(SMS_Record.Fields("mt_err_mf").Value)
        TempValue.SwitchDataMT.Mt_misc_Error_SMS = Val(SMS_Record.Fields("mt_err_mc").Value)
        TempValue.SwitchDataMT.Mt_Success_SMS = Val(SMS_Record.Fields("mt_ok").Value)
        TempValue.SwitchDataMT.Mt_TimeOut_SMS = Val(SMS_Record.Fields("mt_err_to").Value)
        Call AddSwitchData(MOC_Stats, TempValue)
        SMS_Record.MoveNext
    Wend
    
    If SMS_Record.State = 1 Then SMS_Record.Close
   ' Next i
    Conn.Close
    GetMySQLStats_Switches = MOC_Stats
LocalHandler:
    If Err.Number = -2147217865 Then        'Table Not Found
        SMS_Record.Open "select * from current_samples where name = 'SMS " & NewI & "' ", Conn
        TableNotFound = True
        Resume Next
    
    Else
        If Err.Number = 0 Or Err.Number = 20 Then
            Resume Next
        Else
            MsgBox Err.Description
        End If
    End If
End Function
Private Sub AddSwitchData(ByRef WholeSMSData() As Switch_type, ByRef newSMSData As Switch_type)
    Dim Found As Boolean
    Dim FoundID As Integer
'    On Error GoTo LocalHandler
    For i = 0 To UBound(WholeSMSData)
        If WholeSMSData(i).SwitchName = newSMSData.SwitchName And WholeSMSData(i).SwitchName <> "" Then
            Found = True
            FoundID = i
        End If
    Next i
    If Found Then
        WholeSMSData(FoundID).SwitchDataMO.MO_Decimated_SMS = newSMSData.SwitchDataMO.MO_Decimated_SMS + WholeSMSData(FoundID).SwitchDataMO.MO_Decimated_SMS
        WholeSMSData(FoundID).SwitchDataMO.MO_misc_Error_SMS = newSMSData.SwitchDataMO.MO_misc_Error_SMS + WholeSMSData(FoundID).SwitchDataMO.MO_misc_Error_SMS
        WholeSMSData(FoundID).SwitchDataMO.MO_Success_SMS = newSMSData.SwitchDataMO.MO_Success_SMS + WholeSMSData(FoundID).SwitchDataMO.MO_Success_SMS
        WholeSMSData(FoundID).SwitchDataMO.MO_TimeOut_SMS = newSMSData.SwitchDataMO.MO_TimeOut_SMS + WholeSMSData(FoundID).SwitchDataMO.MO_TimeOut_SMS
        WholeSMSData(FoundID).SwitchDataMO.MO_Unknown_SMS = newSMSData.SwitchDataMO.MO_Unknown_SMS + WholeSMSData(FoundID).SwitchDataMO.MO_Unknown_SMS
        
        WholeSMSData(FoundID).SwitchDataMT.Mt_Abs_Sub = newSMSData.SwitchDataMT.Mt_Abs_Sub + WholeSMSData(FoundID).SwitchDataMT.Mt_Abs_Sub
        WholeSMSData(FoundID).SwitchDataMT.Mt_Mem_Full = newSMSData.SwitchDataMT.Mt_Mem_Full + WholeSMSData(FoundID).SwitchDataMT.Mt_Mem_Full
        WholeSMSData(FoundID).SwitchDataMT.Mt_misc_Error_SMS = newSMSData.SwitchDataMT.Mt_misc_Error_SMS + WholeSMSData(FoundID).SwitchDataMT.Mt_misc_Error_SMS
        WholeSMSData(FoundID).SwitchDataMT.Mt_Success_SMS = newSMSData.SwitchDataMT.Mt_Success_SMS + WholeSMSData(FoundID).SwitchDataMT.Mt_Success_SMS
        WholeSMSData(FoundID).SwitchDataMT.Mt_TimeOut_SMS = newSMSData.SwitchDataMT.Mt_TimeOut_SMS + WholeSMSData(FoundID).SwitchDataMT.Mt_TimeOut_SMS
    Else
        ReDim Preserve WholeSMSData(UBound(WholeSMSData) + 1)
        WholeSMSData(UBound(WholeSMSData)) = newSMSData
    End If
    
LocalHandler:
    If Err.Number = 9 Then
        ReDim Preserve WholeSMSData(0)
        WholeSMSData(0) = newSMSData
        i = i + 1
        Resume Next
    End If
End Sub
Public Function GetMySQLStats_Hourly(Optional DataDate As Date) As Mo_SMS_Type()
    Dim Conn As New ADODB.Connection
    Dim SMS_Record, SMS_Record_Temp As New ADODB.Recordset
    Dim TempValue As Mo_SMS_Type
    Dim HourlyStats() As Mo_SMS_Type
    'Getting Data
    Call DB_Open(Conn, TextStat)
    
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Set SMS_Record_Temp = New ADODB.Recordset
    ' Execute the statement.
    Dim NewI As String
    Dim proceed, TableNotFound As Boolean
    Set SMS_Record = New ADODB.Recordset
    proceed = True
    For i = 0 To 23
        'TableNotFound = False
        'If i > Hour(Time) Then
        proceed = False
        NewI = Trim(Str(i))
        If i < 10 Then NewI = "0" & Trim(Str(i))
        On Error GoTo LocalHandler
        If i <= Hour(Time) Then
            X = 5
        End If
        If (DataDate = Date And i <= Hour(Time)) Or (DataDate <> Date) Then
            If Hour(Time) - 4 <= i And DataDate = Date Then
                SMS_Record.Open "select * from current_samples where name = 'SMS " & NewI & "' ", Conn
                proceed = True
            Else
                SMS_Record.Open "select * from daily_samples where name = 'SMS " & NewI & "' " + _
                                                                            "And (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DataDate, Date)) + " DAY)))", _
                                                                            Conn
                proceed = True
            End If
        End If
        'If i > Hour(Time) Then Proceed = False
        
        TempValue.MO_Success_SMS = 0
        TempValue.MO_Decimated_SMS = 0
        TempValue.MO_misc_Error_SMS = 0
        TempValue.MO_TimeOut_SMS = 0
        TempValue.MO_Unknown_SMS = 0
        If proceed Then
            While Not SMS_Record.EOF
                TempValue.MO_Success_SMS = Val(SMS_Record.Fields("mo_ok").Value) + TempValue.MO_Success_SMS
                TempValue.MO_Decimated_SMS = Val(SMS_Record.Fields("mo_dropped").Value) + TempValue.MO_Decimated_SMS
                TempValue.MO_misc_Error_SMS = Val(SMS_Record.Fields("mo_err_mc").Value) + TempValue.MO_misc_Error_SMS
                TempValue.MO_TimeOut_SMS = Val(SMS_Record.Fields("mo_err_to").Value) + TempValue.MO_TimeOut_SMS
                TempValue.MO_Unknown_SMS = Val(SMS_Record.Fields("mo_unknown").Value) + TempValue.MO_Unknown_SMS
                SMS_Record.MoveNext
            Wend
        End If
        ReDim Preserve HourlyStats(i)
        HourlyStats(i).MO_Success_SMS = TempValue.MO_Success_SMS
        HourlyStats(i).MO_Decimated_SMS = TempValue.MO_Decimated_SMS
        HourlyStats(i).MO_misc_Error_SMS = TempValue.MO_misc_Error_SMS
        HourlyStats(i).MO_TimeOut_SMS = TempValue.MO_TimeOut_SMS
        HourlyStats(i).MO_Unknown_SMS = TempValue.MO_Unknown_SMS
        If SMS_Record.State = 1 Then SMS_Record.Close
    Next i
    Conn.Close
        GetMySQLStats_Hourly = HourlyStats
LocalHandler:
    If Err.Number = -2147217865 Then        'Table Not Found
        SMS_Record.Open "select * from current_samples where name = 'SMS " & NewI & "' ", Conn
        TableNotFound = True
        Resume Next
    
    Else
        If Err.Number = 0 Or Err.Number = 20 Then
            Resume Next
        Else
            MsgBox Err.Description
        End If
    End If
End Function




