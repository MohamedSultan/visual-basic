VERSION 5.00
Begin VB.Form Yesterday_Starter 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "Yesterday_Starter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    If Not IsProcessRunning("BackGroundStatsCollector_Yesterday.exe") Then
        If Not FileExists(App.Path + "\Temp\BackGroundStatsCollector_Yesterday.exe") Then _
            Call FileOperation(FO_COPY, App.Path + "\BackGroundStatsCollector_Yesterday.exe", App.Path + "\Temp\BackGroundStatsCollector_Yesterday.exe")
        'Call Add_Comment("Getting FTD Data")
        Dim O_FileName As Double
        O_FileName = FreeFile
        
        Open App.Path + "\Yesterdays_log.txt" For Append As O_FileName
            Print #O_FileName, Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + "  Getting Yesterday Data"
        Close O_FileName
        
        
        Call Shell(App.Path + "\Temp\BackGroundStatsCollector_Yesterday.exe")
        
        Open App.Path + "\Yesterdays_log.txt" For Append As O_FileName
            Print #O_FileName, Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + "  Finished Yesterday Data"
        Close O_FileName
        End
    End If
End Sub
Private Sub FileOperation(FileFunction As File_Functions_Enum, FromFile As String, ToFile As String, Optional WhichFiles As ConcatinationStatus_Enum)
    
    
    On Error GoTo LocalHandler
    If FileFunction = FO_DELETE Then
        Select Case WhichFiles
            Case File1
                Kill FromFile
            Case file2
                Kill ToFile
            Case both
                Kill ToFile
                Kill FromFile
        End Select
    Else
        
        Dim lResult As Long, SHF As SHFILEOPSTRUCT
        SHF.hwnd = hwnd
        SHF.wFunc = FileFunction
        SHF.pFrom = FromFile
        SHF.pTo = ToFile
        SHF.fFlags = FOF_FILESONLY
        lResult = SHFileOperation(SHF)
        'If lResult Then
        '    MsgBox "Error occurred!", vbInformation, "SHCOPY"
        'End If
    End If
LocalHandler:
    'Debug.Print Err.Number
    If Err.Number = 53 Then
        If ToFile <> "" And FileFunction = FO_DELETE And CurrentFile = FromFile Then Resume Next
        Exit Sub
    End If
End Sub

