select dsc.sdp_name,to_char(dsc.start_date_time,'yyyy-mm-dd-hh24') When,sum(dsc.org_atmp) BHCA,tbl.REJECTED_CALLS,tbl.REJECTION_EVENTS
            from DC_SDP_CALLS dsc , (select node,to_char(date_time,'yyyy-mm-dd hh24') dte,REJECTED_CALLS,REJECTION_EVENTS
                From DC_SDP_LOADREGULATION
                where to_char(date_time,'yyyy-mm-dd') = to_char(sysdate-2,'yyyy-mm-dd') and rejected_calls<>0) tbl 
            where to_char(dsc.start_date_time,'yyyy-mm-dd hh24')= tbl.dte  AND dsc.sdp_name=tbl.node
            group by dsc.sdp_name,dsc.start_date_time,tbl.REJECTED_CALLS,tbl.REJECTION_EVENTS 
            order by dsc.sdp_name,dsc.start_date_time desc;
            
            
select dsc.sdp_name,sum(dsc.org_atmp) BHCA
From DC_SDP_CALLS dsc
            where to_char(start_date_time,'yyyy-mm-dd') = to_char(sysdate-2,'yyyy-mm-dd')
            group by dsc.sdp_name
            order by dsc.sdp_name desc;

--SDP Calls Per SDP            
select SDP_NAME,sum(org_atmp)
from DC_SDP_CALLS
where to_char(start_date_time,'yyyy-mm-dd') = to_char(sysdate-2,'yyyy-mm-dd')
group by SDP_Name;


--SDP Calls Per Hour          
select to_char(start_date_time,'yyyy-mm-dd-HH24') TIME,sum(org_atmp)
from DC_SDP_CALLS
where to_char(start_date_time,'yyyy-mm-dd') = to_char(sysdate-2,'yyyy-mm-dd')
group by to_char(start_date_time,'yyyy-mm-dd-HH24')