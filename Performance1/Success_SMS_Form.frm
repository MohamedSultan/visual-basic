VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form Success_SMS_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Success_Messages"
   ClientHeight    =   4845
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   12945
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Success_SMS_Form.frx":0000
   ScaleHeight     =   4845
   ScaleWidth      =   12945
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Height          =   615
      Left            =   600
      TabIndex        =   3
      Top             =   4200
      Width           =   8775
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   27
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   1
         Left            =   480
         TabIndex        =   26
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   2
         Left            =   840
         TabIndex        =   25
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   3
         Left            =   1200
         TabIndex        =   24
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   4
         Left            =   1560
         TabIndex        =   23
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   5
         Left            =   1920
         TabIndex        =   22
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   6
         Left            =   2280
         TabIndex        =   21
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   7
         Left            =   2640
         TabIndex        =   20
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   8
         Left            =   3000
         TabIndex        =   19
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   9
         Left            =   3360
         TabIndex        =   18
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   10
         Left            =   3720
         TabIndex        =   17
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   11
         Left            =   4080
         TabIndex        =   16
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   12
         Left            =   4440
         TabIndex        =   15
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   13
         Left            =   4800
         TabIndex        =   14
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   14
         Left            =   5160
         TabIndex        =   13
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   15
         Left            =   5520
         TabIndex        =   12
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   16
         Left            =   5880
         TabIndex        =   11
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   17
         Left            =   6240
         TabIndex        =   10
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   18
         Left            =   6600
         TabIndex        =   9
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   19
         Left            =   6960
         TabIndex        =   8
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   20
         Left            =   7320
         TabIndex        =   7
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   21
         Left            =   7680
         TabIndex        =   6
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Height          =   375
         Index           =   22
         Left            =   8040
         TabIndex        =   5
         Top             =   160
         Width           =   255
      End
      Begin VB.TextBox Arrow 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   375
         Index           =   23
         Left            =   8400
         TabIndex        =   4
         Top             =   160
         Width           =   255
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   10200
      TabIndex        =   1
      Top             =   3480
      Width           =   2655
      Begin VB.Label Total_Label 
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   240
         Width           =   2055
      End
   End
   Begin MSChart20Lib.MSChart Success_Messages_Chart 
      Height          =   4215
      Left            =   0
      OleObjectBlob   =   "Success_SMS_Form.frx":0892
      TabIndex        =   0
      Top             =   0
      Width           =   12855
   End
End
Attribute VB_Name = "Success_SMS_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
