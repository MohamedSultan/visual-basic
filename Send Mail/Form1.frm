VERSION 5.00
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form Form1 
   Caption         =   "Mailto"
   ClientHeight    =   5370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   ScaleHeight     =   5370
   ScaleWidth      =   5655
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSend 
      Caption         =   "Send"
      Height          =   375
      Left            =   4560
      TabIndex        =   6
      Top             =   4800
      Width           =   975
   End
   Begin VB.TextBox txtSubject 
      Height          =   285
      Left            =   960
      TabIndex        =   4
      Text            =   "Test"
      Top             =   600
      Width           =   4575
   End
   Begin VB.TextBox txtSendTo 
      Height          =   285
      Left            =   960
      TabIndex        =   2
      Text            =   "mohamed.sultan@vodafone.com"
      Top             =   120
      Width           =   4575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Message"
      Height          =   3615
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   5415
      Begin VB.TextBox txtMessage 
         Height          =   3255
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   5
         Text            =   "Form1.frx":0000
         Top             =   240
         Width           =   5175
      End
   End
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   0
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   600
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.Label Label2 
      Caption         =   "Subject:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Send To:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSend_Click()
    With MAPIMessages1
        .MsgIndex = -1
        .RecipDisplayName = txtSendTo.Text
        .MsgSubject = txtSubject.Text
        .MsgNoteText = txtMessage.Text
        .SessionID = MAPISession1.SessionID
        .Send
    End With
    'MsgBox "Message sent successfully!", vbOKOnly, "Mail Sent"
End Sub

Private Sub Form_Load()
    MAPISession1.SignOn
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MAPISession1.SignOff
End Sub

