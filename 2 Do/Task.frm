VERSION 5.00
Begin VB.Form Task 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   3630
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   3630
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox On_Top 
      Caption         =   "Check1"
      Height          =   375
      Left            =   960
      TabIndex        =   1
      Top             =   1440
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox Tasks 
      Height          =   3045
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   0
      Width           =   3615
   End
   Begin VB.Menu Always_on_Top 
      Caption         =   "Always on Top                                              "
   End
End
Attribute VB_Name = "Task"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Always_on_Top_Click()
    Dim My_ID As Integer
    My_ID = ID_vs_Name_Function(Me.Caption)
    Call AlwaysOnTop(Task_Form(My_ID), True)
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    X = 1
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then  ' right click
        Menu.Top = Y
        Menu.Left = X
        Menu.Show
    End If
End Sub

Private Sub Tasks_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then  ' right click
        Menu.Top = Y
        Menu.Left = X
        Menu.Show
        Call AlwaysOnTop(Menu)
    End If
End Sub
