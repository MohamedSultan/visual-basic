VERSION 5.00
Begin VB.Form Tasks_Manager 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tasks"
   ClientHeight    =   4095
   ClientLeft      =   3495
   ClientTop       =   1935
   ClientWidth     =   11640
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   11640
   Begin VB.TextBox Else_ 
      Height          =   3045
      Left            =   8760
      MultiLine       =   -1  'True
      TabIndex        =   8
      Top             =   340
      Width           =   2775
   End
   Begin VB.TextBox Postponed 
      Height          =   3045
      Left            =   5880
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   340
      Width           =   2775
   End
   Begin VB.TextBox Pending 
      Height          =   3045
      Left            =   3000
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   340
      Width           =   2775
   End
   Begin VB.TextBox Started 
      Height          =   3045
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   360
      Width           =   2775
   End
   Begin VB.CommandButton RollOut 
      Caption         =   "RollOut"
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton reload 
      Caption         =   "Reload"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   3600
      Width           =   1095
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Else"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8760
      TabIndex        =   9
      Top             =   0
      Width           =   2775
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Postponed"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   7
      Top             =   0
      Width           =   2775
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Pending"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3000
      TabIndex        =   5
      Top             =   0
      Width           =   2775
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Started"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   15
      Width           =   2775
   End
End
Attribute VB_Name = "Tasks_Manager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim Form_ID As Integer

Private Sub Form_Load()
    Me.Left = Screen.Width - Me.Width
    Me.Top = (Screen.Height - Me.Height) / 2
End Sub



Private Sub reload_Click()
    Dim I_Tasks As Double
    I_Tasks = FreeFile
    Dim Step As Integer
    Dim AllTasks() As String
    Open "D:\Tasks.txt" For Input As I_Tasks
        Do Until EOF(I_Tasks)
            ReDim Preserve AllTasks(Step)
            Input #I_Tasks, AllTasks(Step)
            Step = Step + 1
        Loop
    Close I_Tasks
    
    'parsing Tasks
    Dim All_Tasks() As String
    ReDim Preserve All_Tasks((Step / 3) - 1, 2)
    Step = 0
    For i = 0 To UBound(AllTasks) Step 3
'        ReDim Preserve All_Tasks(Step, 2)
        All_Tasks(Step, 0) = AllTasks(i)
        All_Tasks(Step, 1) = AllTasks(i + 1)
        All_Tasks(Step, 2) = AllTasks(i + 2)
        Step = Step + 1
    Next i
    ReDim AllTasks(0)
    
    
    Started = ""
    Postponed = ""
    Else_ = ""
    Pending = ""
    For i = 0 To UBound(All_Tasks)
        Select Case UCase(All_Tasks(i, 1))
            Case "STARTED"
                Started = Started & vbCrLf & ". " & All_Tasks(i, 0)
            Case "POSTPONED"
                Postponed = Postponed & vbCrLf & ". " & All_Tasks(i, 0)
            Case "PENDING"
                Pending = Pending & vbCrLf & ". " & All_Tasks(i, 0)
            Case Else
                Else_ = Else_ & vbCrLf & ". " & All_Tasks(i, 0)
        End Select
    Next i
End Sub
Private Sub RollOut_Task(Task_Type As String)
    Dim ID As Integer
    ReDim Preserve Task_Form(Form_ID)
    Set Task_Form(Form_ID) = New Task
    ReDim Preserve ID_vs_Name(Form_ID)
    Select Case UCase(Task_Type)
        Case "STARTED"
            'ID = ID_vs_Name_Function("Started")
            'If ID = 0 Then ID = Form_ID
            ID_vs_Name(Form_ID) = "Started"
            Task_Form(Form_ID).Caption = Task_Type & " Tasks"
            Task_Form(Form_ID).Tasks = Tasks_Manager.Started
            Task_Form(Form_ID).Visible = True
        
            Task_Form(Form_ID).Top = Screen.Height * 0.05
            Call AlwaysOnTop(Task_Form(Form_ID))
            Task_Form(Form_ID).On_Top.Value = 1
            Task_Form(Form_ID).Left = Screen.Width - Task_Form(Form_ID).Width
        Case "POSTPONED"
            ID = ID_vs_Name_Function("Started")
            ID_vs_Name(Form_ID) = "POSTPONED"
            Task_Form(Form_ID).Caption = Task_Type & " Tasks"
            Task_Form(Form_ID).Tasks = Tasks_Manager.Postponed
            Task_Form(Form_ID).Visible = True
            
            Task_Form(Form_ID).Top = Screen.Height * 0.05 + Task_Form(Form_ID).Height * 2
            Task_Form(Form_ID).Left = Screen.Width - Task_Form(Form_ID).Width
        Case "PENDING"
            ID_vs_Name(Form_ID) = "PENDING"
            Task_Form(Form_ID).Caption = Task_Type & " Tasks"
            Task_Form(Form_ID).Tasks = Tasks_Manager.Pending
            Task_Form(Form_ID).Visible = True
            Task_Form(Form_ID).Top = Screen.Height * 0.05 + Task_Form(Form_ID).Height
            Task_Form(Form_ID).Left = Screen.Width - Task_Form(Form_ID).Width
            ID_vs_Name(Form_ID) = "Started"
        Case Else
            ID_vs_Name(Form_ID) = "Else"
            Task_Form(Form_ID).Caption = Task_Type & " Tasks"
            Task_Form(Form_ID).Tasks = Tasks_Manager.Else_
            Task_Form(Form_ID).Visible = True
            Task_Form(Form_ID).Left = Screen.Width - Task_Form(Form_ID).Width * 2
            Task_Form(Form_ID).Top = Screen.Height * 0.05 + Task_Form(Form_ID).Height * 2
            ID_vs_Name(Form_ID) = "Started"
    End Select
    Form_ID = Form_ID + 1
   ' For i = 0 To Form_ID - 1
   '     If InStr(1, UCase(Task_Form(i).Caption), "TARTED") <> 0 Then
   '         Call AlwaysOnTop(Task_Form(i))
   '     End If
   ' Next i
End Sub
Private Sub RollOut_Click()
    Call RollOut_Task("Started")
    Call RollOut_Task("Postponed")
    Call RollOut_Task("Pending")
    Call RollOut_Task("Else")
    Call Slide(Me, False)
End Sub

