VERSION 5.00
Begin VB.Form Small_Menu 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   1725
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   1725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   240
      Top             =   1680
   End
   Begin VB.CommandButton Task_manager_Command 
      Caption         =   "Task Manger"
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "Small_Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Deactivate()
'    Call Form_LostFocus
End Sub

Private Sub Form_LostFocus()
    Call Slide(Me, False)
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    While Me.Left > Screen.Width - Me.Width
        Me.Left = Me.Left - 10
        Timer1.Enabled = False
        Timer1.Enabled = True
    Wend
    Me.Left = Screen.Width - Me.Width
    If Timer1.Enabled = True Then
        Timer1.Enabled = False
        Timer1.Enabled = True
    End If
End Sub

Private Sub Task_manager_Command_Click()
    Call Slide(Tasks_Manager, True)
    Tasks_Manager.Show
    Call Slide(Me, False)
End Sub

Private Sub Timer1_Timer()
    Call Form_LostFocus
    Timer1.Enabled = False
End Sub
