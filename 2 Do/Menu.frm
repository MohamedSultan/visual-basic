VERSION 5.00
Begin VB.Form Menu 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   285
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1260
   LinkTopic       =   "Form1"
   ScaleHeight     =   285
   ScaleWidth      =   1260
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Label Label1 
      Caption         =   "Always On Top"
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1215
   End
End
Attribute VB_Name = "Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Label1_Click()
    Call AlwaysOnTop(Task_Form(Form_ID), False)
End Sub
