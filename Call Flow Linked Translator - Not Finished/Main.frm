VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Main_Form 
   Caption         =   "Call Flow Prompt Changer"
   ClientHeight    =   7920
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   6930
   LinkTopic       =   "Form1"
   ScaleHeight     =   7920
   ScaleWidth      =   6930
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox OutPut 
      Height          =   285
      Left            =   1680
      TabIndex        =   6
      Top             =   840
      Width           =   5055
   End
   Begin VB.TextBox Prefix 
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      Top             =   480
      Width           =   5055
   End
   Begin MSComDlg.CommonDialog SetPath 
      Left            =   6240
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Open_File_Command 
      Caption         =   "open File"
      Height          =   315
      Left            =   5640
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
   Begin VB.ComboBox Path_Combo 
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   5415
   End
   Begin VB.ListBox Check_Display 
      ForeColor       =   &H00000000&
      Height          =   6105
      ItemData        =   "Main.frx":0000
      Left            =   120
      List            =   "Main.frx":0007
      TabIndex        =   1
      Top             =   1560
      Width           =   6615
   End
   Begin VB.CommandButton Change_Prompts 
      Caption         =   "Start Changing Prompts"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   6615
   End
   Begin VB.Label Label2 
      Caption         =   "Output File Name"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Prefix To Add"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   1095
   End
   Begin VB.Menu Test 
      Caption         =   "Test"
      Index           =   1
   End
   Begin VB.Menu About 
      Caption         =   "About"
      Index           =   0
   End
End
Attribute VB_Name = "Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Display(ByVal Data As String)
    Check_Display.AddItem Data
    Check_Display.ListIndex = (Check_Display.ListCount - 1)
End Sub

Private Function Open_File(FileName As String) As Boolean
    Dim pprFileName, Step As Double
    Dim Temp As String
    On Error GoTo Local_ErrorHandler
    pprFileName = FreeFile
    Open_File = True
    Open FileName For Input As pprFileName
        Do Until EOF(pprFileName)
            Step = Step + 1
            ReDim Preserve CallFlow(Step)
            Line Input #pprFileName, CallFlow(Step)
        Loop
    Close pprFileName
    'Dim Parser_ret As Parser_Return
    'Parser_ret = Parser(Temp, vbTab)
Local_ErrorHandler:
    If Err.Number = 62 Then
        Dim Format_File As Integer
        Format_File = MsgBox("PLZ reformat the File In DOS Format", vbYesNoCancel, "File Format Error")
        Select Case Format_File
            Case vbYes
                Call Convert_Format(Path_Combo.Text)
                Call Open_File(App.Path + "\Temp.txt")
            Case vbNo
                Open_File = False
            Case vbCancel
                Open_File = False
        End Select
    Else
        Open_File = False
        If Err.Number <> 0 Then
            Call MsgBox("Error: " + Err.Description, vbCritical, "Error:" + Str(Err.Number))
        Else
            Open_File = True
        End If
       ' Call Err.Raise(Err.Number, Err.Source, Err.Description)
    End If
End Function
Private Sub Get_Paths()
    Dim SS7_Paths As Double
    SS7_Paths = FreeFile
    Dim Paths() As String
    Step = 0
    Path_Combo.Clear
    On Error GoTo Local_Handler
    Open App.Path + "\Paths.txt" For Input As SS7_Paths
        Do Until EOF(SS7_Paths)
            ReDim Preserve Paths(Step)
            Line Input #SS7_Paths, Paths(Step)
            Step = Step + 1
        Loop
    Close SS7_Paths
    
    For i = 0 To UBound(Paths)
        Path_Combo.AddItem Paths(i)
    Next i
    Path_Combo.ListIndex = 0
    
Local_Handler:
    If Err.Number = 53 Then
        SS7_Paths = FreeFile
        Open App.Path + "\Paths.txt" For Output As SS7_Paths
        Close SS7_Paths
    End If
End Sub

Private Sub About_Click(Index As Integer)
    MsgBox "This Program is Developed By Mohamed Sultan" + _
            vbCrLf + "Senior IN Charging" + _
            vbCrLf + "VAS, Network Development" + _
            vbCrLf + "25 Aug 2008", , "About SS7 Checker"
End Sub
Private Function Shift_Lines_(ByRef Array_ As Variant, ByRef Lines As Variant, ByVal StartLine As Integer) As Variant
    Dim Step, NumberOfAddedLines As Integer
    Dim Shift_Lines() As String
    
    NumberOfAddedLines = UBound(Lines)
    For i = 0 To UBound(Array_) + NumberOfAddedLines
        ReDim Preserve Shift_Lines(i)
        If i < StartLine Then
            Shift_Lines(i) = Array_(i)
        Else
            If i = StartLine Then
                For j = 0 To NumberOfAddedLines
                    Shift_Lines(i) = Lines(j)
                Next j
            Else
                Shift_Lines(i) = Array_(i - j)
            End If
        End If
    Next i
    Shift_Lines_ = Shift_Lines
End Function
Private Sub Change_Prompts_Click()
    Dim Correct_Format As Boolean
    Dim Comment(0) As String
    Dim Line, PromptName, NewPromptName As String
    Check_Display = ""
    Call Check_FileName(Path_Combo.Text)
    Correct_Format = Open_File(Path_Combo.Text)
    If Correct_Format Then
        For i = 2 To UBound(CallFlow)
            Line = ""
            PromptName = ""
            
            Comment(0) = "comment: 1"
            'Commenting the SS7 Block
            If CallFlow(i) = "name: SS7" And Left(CallFlow(i + 1), 2) = "X:" And Left(CallFlow(i + 2), 2) = "Y:" Then
                CallFlow = Shift_Lines_(CallFlow, Comment, i)
                i = i + UBound(Comment) - 1
            End If

        Next i
    End If
    
    Call SaveFileName(OutPut)
    MsgBox "Done", , "Finished"
End Sub
Private Function Add_Prefix(ByVal Line As String, ByVal Prompt As String) As String
    If Left(Prompt, Len(Prefix)) <> Prefix Then
        If Asc(Left(Prompt, 1)) = 34 And Asc(Right(Prompt, 1)) = 34 Then
            Add_Prefix = Line + Trim(Chr(34)) + Trim(Prefix) + Mid(Prompt, 2, Len(Prompt) - 2) + Trim(Chr(34))
        Else
            Add_Prefix = Line + Trim(Prefix) + Trim(Prompt)
        End If
        Call Display(Add_Prefix + "Updated")
    Else
        Add_Prefix = Line + Trim(Prompt)
    End If
End Function
Private Sub SaveFileName(FileName As String)
    Dim pprFileName As Double
    pprFileName = FreeFile
    Open FileName For Output As pprFileName
        For i = 1 To UBound(CallFlow)
            Print #pprFileName, CallFlow(i)
        Next i
    Close pprFileName
End Sub
Private Sub Check_FileName(ByVal FileName As String)
'This is used to check whether the the file name exists in the combo list or not and add in case of not exist
    found = False
    Dim Paths() As String
    ReDim Preserve Paths(0)
    For i = 0 To Path_Combo.ListCount - 1
        If FileName = Path_Combo.List(i) Then
            found = True
            Paths(0) = FileName
        Else
            ReDim Preserve Paths(i + 1)
            Paths(i + 1) = Path_Combo.List(i)
        End If
    Next i
    If Not found Then
        Paths(0) = FileName
    Else
        
    End If
        Call Save_Paths(Paths)
        Call Get_Paths
End Sub
Private Sub Save_Paths(ByVal Paths As Variant)
    Dim SS7_Paths As Double
    SS7_Paths = FreeFile
    Open App.Path + "\Paths.txt" For Output As SS7_Paths
        For i = 0 To UBound(Paths)
            If Paths(i) <> "" Then Print #SS7_Paths, Paths(i)
        Next i
    Close SS7_Paths
End Sub

Private Sub Form_Load()
    Call Get_Paths
End Sub
Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Open_File_Command_Click()
    Dim Directory As String
    Directory = Get_Dir(Path_Combo.Text)
    SetPath.InitDir = Directory
    SetPath.CancelError = False
    SetPath.Filter = "All Files (*.*)|*.*|Configuration Files(*.ppr)|*.ppr|Text Files(*.txt)|*.txt"
    SetPath.FilterIndex = 2
    SetPath.ShowOpen
    If SetPath.FileName <> "" Then Path_Combo.Text = SetPath.FileName
End Sub

Private Sub Path_Combo_KeyPress(KeyAscii As Integer)
    'On Error Resume Next
    If KeyAscii = 13 Then Call Change_Prompts_Click
End Sub

