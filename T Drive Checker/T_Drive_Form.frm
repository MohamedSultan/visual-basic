VERSION 5.00
Begin VB.Form T_Drive_Form 
   Caption         =   "T Drive Checker"
   ClientHeight    =   5265
   ClientLeft      =   7620
   ClientTop       =   4830
   ClientWidth     =   6075
   LinkTopic       =   "Form1"
   ScaleHeight     =   5265
   ScaleWidth      =   6075
   Begin VB.ListBox List1 
      Height          =   3180
      Left            =   120
      TabIndex        =   7
      Top             =   2040
      Width           =   5895
   End
   Begin VB.TextBox Dir_Path 
      Height          =   375
      Left            =   1920
      TabIndex        =   5
      Text            =   "T:\T-drive"
      Top             =   960
      Width           =   2415
   End
   Begin VB.CommandButton Mimizing 
      Caption         =   "_"
      Height          =   255
      Left            =   5760
      TabIndex        =   4
      Top             =   0
      Width           =   255
   End
   Begin VB.Frame Frame5 
      Height          =   855
      Left            =   2160
      TabIndex        =   1
      Top             =   0
      Width           =   2055
      Begin VB.Timer minutes 
         Interval        =   60000
         Left            =   -120
         Top             =   600
      End
      Begin VB.Timer Seconds 
         Interval        =   1000
         Left            =   1800
         Top             =   600
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Start"
      Height          =   495
      Left            =   2280
      TabIndex        =   0
      Top             =   1440
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Path"
      Height          =   255
      Left            =   1200
      TabIndex        =   6
      Top             =   960
      Width           =   855
   End
End
Attribute VB_Name = "T_Drive_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer

Private Sub Form_Load()
    CurrentSeconds = 60
    ReDim Checker(0)
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Me.Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Mimizing_Click()
            Me.WindowState = 1
            Minimized = True

            Dim i As Integer
            Dim s As String
            Dim nid As NOTIFYICONDATA

            s = "T Drive Checker"
            nid = setNOTIFYICONDATA(Me.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Me.Icon, s)
            i = Shell_NotifyIconA(NIM_ADD, nid)

            WindowState = vbMinimized
            Visible = False
End Sub

Private Sub Minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    If GetDataInterval = 20 Then
        Days = ""
        Call Command1_Click
        GetDataInterval = 0
    End If
End Sub


Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub

Private Sub Command1_Click()
    Dim oFso As Scripting.FileSystemObject
    Set oFso = New Scripting.FileSystemObject
    
    List1.Clear
    Check_Form.List1.Clear
    'X = oFso.GetFile("D:\Temp\ShortCuts")
    
    Dim sFiles() As File_Type
    Dim FinalFiles() As File_Type
    Dim SubFiles() As File_Type
    ReDim SubFiles(0)
    SubFiles(0).FilePath = Dir_Path
    ReDim FinalFiles(0)
    'Dim Dfiles As Collection
    'Set DFiles = New Collection
    'sFiles = AllFiles("T:\T-drive\")
    sFiles = GetAllFiles2(SubFiles(0).FilePath)
    Dim Step As Integer
    
    Do While Step <= UBound(SubFiles)
        sFiles = GetAllFiles2(SubFiles(Step).FilePath)
        For i = 1 To UBound(sFiles)
            If Right(sFiles(i).FilePath, 2) <> "\." And Right(sFiles(i).FilePath, 3) <> "\.." Then
                If sFiles(i).FileType = 16 Then  'Directory
                    ReDim Preserve SubFiles(UBound(SubFiles) + 1)
                    SubFiles(UBound(SubFiles)) = sFiles(i)
                Else    'Files
                    'StringFile
                    Call ToBeCopiedFile(sFiles(i).FilePath)
                    ReDim Preserve FinalFiles(UBound(FinalFiles) + 1)
                    FinalFiles(UBound(FinalFiles)) = sFiles(i)
                End If
            End If
        Next i
        ReDim sFiles(0)
        Step = Step + 1
    Loop
    
    Call AddChecker(List1)
    

  '  For i = 1 To UBound(FinalFiles)
  '      Kill FinalFiles(i).FilePath
  '  Next i
  '  For i = 1 To UBound(SubFiles)
  '      Kill SubFiles(i).FilePath
  '  Next i
    
    'Call GetAllFiles("D:\Work\My Tools\ShortCuts", "*.*", False)
    'sFiles = APIAllFiles("T:\t-drive\")
End Sub
Private Sub AddChecker(List As ListBox)
    ReDim Preserve Checker(UBound(Checker) + 1)
    Set Checker(UBound(Checker)) = New Check_Form
    For i = 0 To List.ListCount - 1
        Checker(UBound(Checker)).List1.AddItem List.List(i)
    Next i
    Checker(UBound(Checker)).Show
End Sub
Private Sub ToBeCopiedFile(FilePath As String)
    Dim FileLength As Long
    'Dim ToBeCopiedFiles() As String
    'ReDim ToBeCopiedFiles(0)
    'For i = 1 To UBound(FinalFiles)
        FileLength = FileLen(FilePath)
        If FileLength / 1024 / 1024 >= 698 Or Right(FilePath, 5) = ".rmvb" Or Right(FilePath, 4) = ".avi" Then
            List1.AddItem FilePath
    '        ReDim Preserve ToBeCopiedFiles(UBound(ToBeCopiedFiles) + 1)
    '        ToBeCopiedFiles(UBound(ToBeCopiedFiles)) = FinalFiles(i).FilePath
        End If
    'Next i
   '     If UBound(ToBeCopiedFiles) >= 1 Then
   '     Dim Message As String
   '     For i = 0 To UBound(ToBeCopiedFiles)
   '         Message = Message + vbCrLf + ToBeCopiedFiles(i)
   '     Next i
   ' End If
    
   ' MsgBox "Movie Files are:" + vbCrLf + Message
End Sub
