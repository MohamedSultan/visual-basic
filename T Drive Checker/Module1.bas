Attribute VB_Name = "Module1"
' Returns a collection with the names of all the files
' that match a file specification
'
' The file specification can include wildcards; multiple
' specifications can be provided, using a semicolon-delimited
' list, as in "*.tmp;*.bat"
' If RECURSEDIR is True the search is extended to all subdirectories
'
' It raises no error if path is invalid
'

Public Function GetAllFiles(ByVal path As String, ByVal filespec As String, _
    Optional RecurseDirs As Boolean) As Collection
    Dim spec As Variant
    Dim file As Variant
    Dim subdir As Variant
    Dim subdirs As New Collection
    Dim specs() As String
    
    ' initialize the result
    Set GetAllFiles = New Collection
    
    ' ensure that path has a trailing backslash
    If Right$(path, 1) <> "\" Then path = path & "\"
    
    ' get the list of provided file specifications
    specs() = Split(filespec, ";")
    
    ' this is necessary to ignore duplicates in result
    ' caused by overlapping file specifications
    On Error Resume Next
                
    ' at each iteration search for a different filespec
    For Each spec In specs
        ' start the search
        file = Dir$(path & spec)
        Do While Len(file)
            ' we've found a new file
            file = path & file
            GetAllFiles.Add file, file
            ' get ready for the next iteration
            file = Dir$
        Loop
    Next
    
    ' first, build the list of subdirectories to be searched
    If RecurseDirs Then
        ' get the collection of subdirectories
        ' start the search
        file = Dir$(path & "*.*", vbDirectory)
        Do While Len(file)
            ' we've found a new directory
            If file = "." Or file = ".." Then
                ' exclude the "." and ".." entries
            ElseIf (GetAttr(path & file) And vbDirectory) = 0 Then
                ' ignore regular files
            Else
                ' this is a directory, include the path in the collection
                file = path & file
                subdirs.Add file, file
            End If
            ' get next directory
            file = Dir$
        Loop
        
        ' parse each subdirectory
        For Each subdir In subdirs
            ' use GetAllFiles recursively
            For Each file In GetAllFiles(subdir, filespec, True)
                GetAllFiles.Add file, file
            Next
        Next
    End If
    
End Function


