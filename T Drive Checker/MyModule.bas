Attribute VB_Name = "MyModule"
Enum FileType
    Directory = vbDirectory
    file = 0
End Enum
Type File_Type
    FilePath As String
    FileType As Integer
End Type
Public Checker() As Check_Form
Dim NumberOfCheckers As Integer

Private Sub AddString2(ByRef AllFiles() As File_Type, NewString As String, FileType As Integer)
    ReDim Preserve AllFiles(UBound(AllFiles) + 1)
    AllFiles(UBound(AllFiles)).FilePath = NewString
    AllFiles(UBound(AllFiles)).FileType = FileType
End Sub
Public Function GetAllFiles2(ByVal path As String) As File_Type()
    Dim spec As Variant
    Dim file As Variant
    Dim subdir As Variant
    Dim subdirs As New Collection
    Dim specs() As String
    Dim AllFiles() As String
    ReDim Preserve GetAllFiles2(0)
    Dim StringFile As String
    
    ' initialize the result
    'Set GetAllFiles = New Collection
    
    ' ensure that path has a trailing backslash
    If Right$(path, 1) <> "\" Then path = path & "\"
    
    ' get the list of provided file specifications
    'specs() = Split(filespec, ";")
    
    ' this is necessary to ignore duplicates in result
    ' caused by overlapping file specifications
    On Error Resume Next
                
    ' at each iteration search for a different filespec
    'For Each spec In specs
        ' start the search
        file = Dir(path, vbDirectory)
        Do While Len(file)
            ' we've found a new file
            file = path & file
            'GetAllFiles.Add file, file
            StringFile = file
            Call AddString2(GetAllFiles2, StringFile, GetAttr(StringFile))
            ' get ready for the next iteration
            file = Dir$
        Loop
    'Next
    
End Function

