VERSION 5.00
Begin VB.Form CleanUp_Form 
   Caption         =   "Clean Up "
   ClientHeight    =   3240
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7755
   LinkTopic       =   "Form1"
   ScaleHeight     =   3240
   ScaleWidth      =   7755
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "CleanUp_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim All_Data(), Directory As String
Dim Step As Integer

Private Sub Form_Load()
    Directory = "T:\"
    
    Call Get_All_Contents(Directory)
    
    On Error Resume Next
    'Deleting all files Files
    For i = 0 To UBound(All_Data)
        Kill Left(All_Data(i), Len(All_Data(i)) - 1)
    Next i
    
    Call Get_All_Contents(Directory)
    
    For i = UBound(All_Data) To 0 Step -1
        If All_Data(i) <> Directory & ".\" Then _
            RmDir Left(All_Data(i), Len(All_Data(i)) - 1)
    Next i
    
    End
End Sub
Private Sub Get_All_Contents(Folder_Path As String)
    'Getting All Files
    ReDim All_Data(0)
    Step = 0
    Dim FileName As String
    Dim Pointer As Integer
    Call Get_All_Folder_Content(Directory)
    On Error Resume Next
    While Pointer <= UBound(All_Data)
        If InStr(1, All_Data(Pointer), "\.\") <> Len(All_Data(Pointer)) - 2 And _
        InStr(1, All_Data(Pointer), "\..\") <> Len(All_Data(Pointer)) - 3 Then _
            FileName = Dir(All_Data(Pointer), 16)
        If FileName = "." Then Get_All_Folder_Content (All_Data(Pointer))
        Pointer = Pointer + 1
        FileName = ""
    Wend
End Sub

Private Sub Get_All_Folder_Content(Folder_Path As String)
    Dim FileName As String
    
    FileName = Dir(Folder_Path, 16)
    While FileName <> ""
        ReDim Preserve All_Data(Step)
        All_Data(Step) = Folder_Path & FileName & "\"
        Step = Step + 1
        FileName = Dir
    Wend
End Sub

