VERSION 5.00
Begin VB.Form MinWind 
   Caption         =   "MinWind"
   ClientHeight    =   1860
   ClientLeft      =   3510
   ClientTop       =   3765
   ClientWidth     =   3600
   LinkTopic       =   "Form1"
   ScaleHeight     =   1860
   ScaleWidth      =   3600
   Begin VB.OptionButton optPlacement 
      Caption         =   "Restore"
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   5
      Top             =   1440
      Width           =   1695
   End
   Begin VB.OptionButton optPlacement 
      Caption         =   "Maximize"
      Height          =   255
      Index           =   1
      Left            =   360
      TabIndex        =   4
      Top             =   1080
      Width           =   1695
   End
   Begin VB.OptionButton optPlacement 
      Caption         =   "Minimize"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   3
      Top             =   720
      Value           =   -1  'True
      Width           =   1695
   End
   Begin VB.CommandButton cmdGo 
      Caption         =   "Go"
      Default         =   -1  'True
      Height          =   375
      Left            =   2760
      TabIndex        =   2
      Top             =   840
      Width           =   735
   End
   Begin VB.TextBox txtTargetName 
      Height          =   285
      Left            =   720
      TabIndex        =   1
      Text            =   "Document - WordPad"
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Task"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   495
   End
End
Attribute VB_Name = "MinWind"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private Type POINTAPI
    X As Long
    Y As Long
End Type
Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
Private Type WINDOWPLACEMENT
    length As Long
    flags As Long
    showCmd As Long
    ptMinPosition As POINTAPI
    ptMaxPosition As POINTAPI
    rcNormalPosition As RECT
End Type
Private Declare Function GetWindowPlacement Lib "user32" (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Private Declare Function SetWindowPlacement Lib "user32" (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Private Const SW_SHOWMINIMIZED = 2
Private Const SW_SHOWMAXIMIZED = 3
Private Const SW_SHOWNORMAL = 1
' Find the target window and minimize, maximize,
' or restore it.
Private Sub cmdGo_Click()
Dim app_hwnd As Long
Dim wp As WINDOWPLACEMENT

    ' Find the target.
    app_hwnd = FindWindow(vbNullString, txtTargetName.Text)

    ' Get the window's current placement information.
    wp.length = Len(wp)
    GetWindowPlacement app_hwnd, wp

    ' Set the appropriate action.
    If optPlacement(0).Value Then
        ' Minimize.
        wp.showCmd = SW_SHOWMINIMIZED
    ElseIf optPlacement(1).Value Then
        ' Maximize.
        wp.showCmd = SW_SHOWMAXIMIZED
    Else
        ' Restore.
        wp.showCmd = SW_SHOWNORMAL
    End If

    ' Perform the action.
    SetWindowPlacement app_hwnd, wp
End Sub


