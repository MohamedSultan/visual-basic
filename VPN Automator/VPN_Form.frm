VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form VPN_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "VPN Automator"
   ClientHeight    =   2280
   ClientLeft      =   3105
   ClientTop       =   4020
   ClientWidth     =   3690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   3690
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   1440
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Timer All_Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton VPN_Conf 
      Caption         =   ">"
      Height          =   1095
      Left            =   2880
      TabIndex        =   1
      Top             =   480
      Width           =   255
   End
   Begin VB.CommandButton VPN_Connect 
      Caption         =   "VPN"
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   2655
   End
   Begin VB.Menu Temp 
      Caption         =   "                                                             "
      Index           =   0
      NegotiatePosition=   3  'Right
      Visible         =   0   'False
   End
   Begin VB.Menu About 
      Caption         =   "About"
      Index           =   0
   End
End
Attribute VB_Name = "VPN_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub About_Click(Index As Integer)
    Call Slide(About_Form, Me.Left + Me.Width, About_Form.Width, True)
End Sub

Private Sub All_Timer_Timer()
    All_Timer.Enabled = False
    
    On Error GoTo ErrorHandler
    Applications_Handler
    
ErrorHandler:
    All_Timer.Enabled = True
End Sub


Private Sub VPN_Config_Click()
   
End Sub



Private Sub Form_Load()
    Call load_VPN_Configurations
End Sub
Sub load_VPN_Configurations()
    VPN_Configurations = FreeFile
    Dim VPN_Version, VPN_Profile As String
    Dim I_Data() As String
    Dim Step As Integer
    
    On Error GoTo LocalHandler
    
    Open App.Path + "\VPN.Conf" For Input As VPN_Configurations
        Do Until EOF(VPN_Configurations)
            ReDim Preserve I_Data(Step)
            Line Input #VPN_Configurations, I_Data(Step)
            Step = Step + 1
        Loop
    Close VPN_Configurations
    
    With VPN_Conf_Form
        .VPN_PIN = Decrypt(I_Data(0))
        .VPN_IP = I_Data(1)
        .VPN_Path = I_Data(2)
        .RSA_Path = I_Data(3)
        .VPN_Wait = I_Data(4)
        
         VPN_Version = I_Data(5)
        
        For I = 0 To .VPN_Version.UBound
            If Val(VPN_Version) = I Then .VPN_Version(I).Value = True
        Next I
        
        VPN_Profile = I_Data(6)
        
        If .GSM.Caption = VPN_Profile Then .GSM.Value = True
        If .GSM_Remote.Caption = VPN_Profile Then .GSM_Remote.Value = True
        If .My_Work.Caption = VPN_Profile Then .My_Work.Value = True
    End With

LocalHandler:
    If Err.Number = 53 Then
        MsgBox "Default Configurations Are applied" + vbCrLf + vbCrLf + "Note: PLZ Update Your VPN Profile Names", , "Configuration File Does Not Exist"
        
        With VPN_Conf_Form
            .VPN_PIN = "0000"
            .VPN_IP = "XX.XXX.XX.XX"
            .VPN_Path = "C:\Program Files\Cisco Systems\VPN Client"
            .RSA_Path = "C:\Program Files\RSA Security\RSA SecurID Software Token"
            .VPN_Wait = "3"
        
             VPN_Version = "1"
            
            For I = 0 To .VPN_Version.UBound
                If Val(VPN_Version) = I Then .VPN_Version(I).Value = True
            Next I
            
            VPN_Profile = "GSM Local"
            
            If .GSM.Caption = VPN_Profile Then .GSM.Value = True
            If .GSM_Remote.Caption = VPN_Profile Then .GSM_Remote.Value = True
            If .My_Work.Caption = VPN_Profile Then .My_Work.Value = True
            
            .Save_Click
        End With
    
    End If
End Sub


Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub VPN_Connect_Click()
    Dim RSA, VPNdialer As Double
    Dim Proceed As Boolean
    Proceed = False
    VPN_Conf_Form.Timer1.Enabled = True
    VPN_Conf_Form.Visible = False
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If (Not IsProcessRunning("ipsecdialer.exe") And VPN_Form.Winsock1.LocalIP <> VPN_Conf_Form.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
        
            If IsProcessRunning("vpngui.exe") Then KillProcess ("vpngui.exe")
            
            If IsProcessRunning("SecurID.exe") Then KillProcess ("SecurID.exe")
            
            Application_Wait = "RSA"
            Applications_Handler
            
            Debug.Print "RSA Finished"
            
            Wait 500
            
            Debug.Print "VPNdialer Started"
            
            Application_Wait = "VPNdialer"
            Applications_Handler
            
            Debug.Print "VPN Dialer Finished"
            Wait 500
            Debug.Print "VPN Dialer Authenticationk Started"
            'MsgBox "Done"
            
            Application_Wait = "VPN Dialer Authentication"
            Applications_Handler
            'SendKeys "^V"
            'SendKeys "{ENTER}"
            
        Else
            MsgBox "Sorry Can't, Since You Are Already Connected"
        End If
    End If
End Sub

Private Sub VPN_Conf_Click()
    VPN_Conf_Form.Top = Me.Top
    VPN_Conf_Form.Timer1.Enabled = True
    Call Slide(VPN_Conf_Form, Me.Left + VPN_Conf.Left + VPN_Conf.Width, VPN_Conf_Form.Width, True, 1)
End Sub
