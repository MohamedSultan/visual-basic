VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form VPN_Conf_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VPN Configurations"
   ClientHeight    =   2355
   ClientLeft      =   10125
   ClientTop       =   2685
   ClientWidth     =   5910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   5910
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox VPN_Wait 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   3240
      TabIndex        =   18
      Top             =   1320
      Width           =   2535
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save"
      Height          =   375
      Left            =   3240
      TabIndex        =   17
      Top             =   1800
      Width           =   2535
   End
   Begin VB.TextBox RSA_Path 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   3240
      TabIndex        =   15
      Top             =   960
      Width           =   2535
   End
   Begin VB.TextBox VPN_Path 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   3240
      TabIndex        =   13
      Top             =   720
      Width           =   2535
   End
   Begin VB.TextBox VPN_IP 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   3240
      TabIndex        =   11
      Top             =   480
      Width           =   1335
   End
   Begin VB.TextBox VPN_PIN 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   3240
      PasswordChar    =   "*"
      TabIndex        =   10
      Top             =   240
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Connection Entry:"
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   1695
      Begin VB.OptionButton GSM 
         BackColor       =   &H00E0E0E0&
         Caption         =   "GSM Local"
         Height          =   195
         Left            =   120
         MaskColor       =   &H00E0E0E0&
         TabIndex        =   3
         ToolTipText     =   "GSM Local"
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton GSM_Remote 
         BackColor       =   &H00E0E0E0&
         Caption         =   "GSM Remote"
         Height          =   195
         Left            =   120
         MaskColor       =   &H00E0E0E0&
         TabIndex        =   2
         ToolTipText     =   "GSM Remote"
         Top             =   600
         Width           =   1275
      End
      Begin VB.OptionButton My_Work 
         BackColor       =   &H00E0E0E0&
         Caption         =   "My Work"
         Height          =   195
         Left            =   120
         MaskColor       =   &H00E0E0E0&
         TabIndex        =   1
         ToolTipText     =   "My Work"
         Top             =   960
         Width           =   1275
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "VPN Version"
      Height          =   855
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   1695
      Begin VB.OptionButton VPN_Version 
         Caption         =   "V 4"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.OptionButton VPN_Version 
         Caption         =   "V 3"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   1800
      Top             =   0
   End
   Begin VB.Label Label4 
      Caption         =   "VPN Wait (Sec)"
      Height          =   495
      Left            =   2400
      TabIndex        =   19
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "RSA Path"
      Height          =   375
      Left            =   2400
      TabIndex        =   16
      Top             =   960
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "VPN Path"
      Height          =   375
      Left            =   2400
      TabIndex        =   14
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "VPN IP"
      Height          =   375
      Left            =   2400
      TabIndex        =   12
      Top             =   480
      Width           =   735
   End
   Begin MSForms.ToggleButton Star_VPN 
      Height          =   375
      Left            =   4560
      TabIndex        =   9
      Top             =   120
      Width           =   975
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "1720;661"
      Value           =   "0"
      Caption         =   "Show PIN"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin VB.Label VPN_Label 
      Caption         =   "VPN PIN"
      Height          =   375
      Left            =   2400
      TabIndex        =   8
      Top             =   240
      Width           =   735
   End
   Begin MSForms.ToggleButton Extend 
      Height          =   1935
      Left            =   1800
      TabIndex        =   7
      Top             =   240
      Width           =   255
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "450;3413"
      Value           =   "0"
      Caption         =   ">>>>>>>"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "VPN_Conf_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Extend_Click()
    If Extend.Value = True Then
        VPN_Conf_Form.Width = VPN_Conf_Form.Width * 2
        VPN_Conf_Form.Timer1.Enabled = False
    Else
        VPN_Conf_Form.Width = VPN_Conf_Form.Width / 2
        VPN_Conf_Form.Timer1.Enabled = True
    End If
End Sub

Private Sub Form_GotFocus()
    Timer1.Enabled = False
    Timer1.Enabled = True
End Sub

Public Sub Form_Load()
    Dim Current_Value As Integer
End Sub

Private Sub Form_LostFocus()
    'VPN_Conf_Form.Timer1.Enabled = True
    Call Timer1_Timer
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Form_GotFocus
End Sub

Private Sub GSM_Click()
    VPN_Form.VPN_Connect.Enabled = True
    VPN_Form.VPN_Connect.Caption = "VPN (Local GSM)"
End Sub
Private Sub GSM_Remote_Click()
    VPN_Form.VPN_Connect.Enabled = True
    VPN_Form.VPN_Connect.Caption = "VPN(RemoteGSM)"
End Sub

Private Sub My_Work_Click()
    VPN_Form.VPN_Connect.Enabled = True
    VPN_Form.VPN_Connect.Caption = "VPN (My Work)"
End Sub

Public Sub Save_Click()
    VPN_File = FreeFile
    Open App.Path + "\VPN.Conf" For Output As VPN_File
        With VPN_Conf_Form
            Print #VPN_File, Encrypt(.VPN_PIN)
            Print #VPN_File, .VPN_IP
            Print #VPN_File, .VPN_Path
            Print #VPN_File, .RSA_Path
            Print #VPN_File, .VPN_Wait
            
            For I = 0 To .VPN_Version.UBound
                If .VPN_Version(I).Value = True Then Print #VPN_File, Str(I)
            Next I
            
            If .GSM.Value Then Print #VPN_File, .GSM.Caption
            If .GSM_Remote.Value Then Print #VPN_File, .GSM_Remote.Caption
            If .My_Work.Value Then Print #VPN_File, .My_Work.Caption
        End With
    Close VPN_File
End Sub

Private Sub Star_VPN_Click()
    With Star_VPN
        If .Value = True Then
            VPN_PIN.PasswordChar = ""
            Else
            VPN_PIN.PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Timer1_Timer()
    Call Slide(Me, Me.Left, Me.Width, False)
    Timer1.Enabled = False
End Sub
