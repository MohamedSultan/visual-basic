Attribute VB_Name = "Handler"
Public Sub Applications_Handler() '(AppName As String)

    Dim Retry As Boolean
    
    On Error GoTo LocalHandler
    
    Select Case Application_Wait
        Case "VPN Dialer Authentication"
            If VPN_Conf_Form.VPN_Version(0) Then        'V3
                If VPN_Conf_Form.GSM_Remote.Value = True Then AppActivate ("User Authentication for Remote GSM")
                If VPN_Conf_Form.GSM.Value = True Then AppActivate ("User Authentication for GSM")
                If VPN_Conf_Form.My_Work.Value = True Then AppActivate ("User Authentication for My Work")
            End If
            If VPN_Conf_Form.VPN_Version(1) Then        'V4
                If VPN_Conf_Form.GSM_Remote.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""Remote GSM""")
                'If VPN_Conf_Form.GSM.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""GSM""")
                If VPN_Conf_Form.GSM.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""GSM Local""")
                If VPN_Conf_Form.My_Work.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""My Work""")
                Wait 500
                SendKeys "^V"
                SendKeys "{ENTER}"
            End If
        Case "VPNdialer"
            VPNdialer = Shell(VPN_Conf_Form.VPN_Path + "\ipsecdialer.exe", vbNormalFocus)
            'Applications_Handler
            Wait 500
            AppActivate (VPNdialer)
            
            Wait 500
            If VPN_Conf_Form.VPN_Version(0).Value Then      'V3.0
                AppActivate ("Cisco Systems VPN Client")
                SendKeys "{TAB}"
                'SendKeys "{TAB}"
                'SendKeys "{DOWN}"
            End If
            Wait 500
            If VPN_Conf_Form.VPN_Version(1).Value Then      'V4.0
                AppActivate ("VPN Client - Version 4.0 (Rel)")
            End If
            
            Wait 500
            If VPN_Conf_Form.GSM_Remote.Value = True Then SendKeys "R"
            If VPN_Conf_Form.GSM.Value = True Then SendKeys "G"
            If VPN_Conf_Form.My_Work.Value = True Then SendKeys "M"
            
            Wait 500
            SendKeys "{ENTER}"
            'Wait Wait_VPN
            
            Exit Sub

        Case "RSA"
            RSA = Shell(VPN_Conf_Form.RSA_Path + "\SecurID.exe", vbNormalFocus)
            AppActivate (RSA)
            SendKeys VPN_Conf_Form.VPN_PIN
            'Wait 500
            SendKeys "{ENTER}"
            'Wait 500
            SendKeys "{TAB}"
            SendKeys "{TAB}"
            SendKeys "{TAB}"
            Wait 500
            SendKeys "^C"
            Wait 500
            SendKeys "%{F4}"
            'Wait 500
        End Select
        
Application_Wait = ""
Main.All_Timer.Enabled = False
            
LocalHandler:
            VPN_Form.All_Timer.Enabled = True
            
'Exceed_Handler:
'            If Err.Number = 5 Then
'            x = 1
End Sub
