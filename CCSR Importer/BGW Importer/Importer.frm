VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    
    On Error GoTo LocalHandler
    
    Call Importer(Recharges, "Recharge_Import.bat", "D:\FTP\BGW", "BGW Importer.exe", "Temp_Recharges.txt")
    Call Importer(Payments, "Payments_Import.bat", "D:\FTP\BGW", "BGW Importer.exe", "Temp_Payments.txt")
    Call Importer(Adjustments, "Adjustments_Import.bat", "D:\FTP\BGW", "BGW Importer.exe", "Temp_Adjustments.txt")
    
    End
   
LocalHandler:
    Select Case Err.Number
        Case 53
            Wait 30000
            Resume
        Case 0, 20
            Resume Next
        Case Else
            Resume Next
    End Select
    
'    On Error GoTo LocalHandler
'    Open "D:\FTP\BGW\Raw_Data" For Input As BGW_Import_File
'        Shell App.Path + "\Insert_BGW.bat"
'        End
'    Close BGW_Import_File
    
'LocalHandler:
'    If Err.Number = 53 Then
'        Wait 30000
'        Resume
'    End If
End Sub
Private Sub Importer(Stats_Type As Stats_Types, ImportAppName As String, ImportDirPath As String, MyAppName As String, ImportFileName As String)
      
    BGW_Import_File = FreeFile
    
    If Count_ProcessRunning(MyAppName) > 1 Then End
    
    Dim FileName As String
    
    Select Case Stats_Type
        Case Recharges
            FileName = Dir(ImportDirPath + "\PPAS-PCR_*")
        Case Adjustments
            FileName = Dir(ImportDirPath + "\PPAS-ADJ_*")
        Case Payments
            FileName = Dir(ImportDirPath + "\PPAS-PAYMENT_*")
    End Select
    While FileName <> ""
        
        If FileExists(ImportDirPath + "\" + ImportFileName) Then Kill ImportDirPath + "\" + ImportFileName
        
        
        If Right(FileName, Len(ImportFileName)) <> ImportFileName Then
            'FileCopy FileName, ImportFileName
            RenameFile ImportDirPath + "\" + FileName, ImportDirPath + "\" + ImportFileName
            'Call FileOperation(FO_MOVE, FileName, ImportFileName)
            Shell App.Path + "\" + ImportAppName
            Wait 5000
        End If
        FileName = Dir
    Wend
End Sub
Public Sub RenameFile(iFileName As String, oFileName As String)
    FileCopy iFileName, oFileName
    Kill iFileName
End Sub

Public Sub FileOperation(FileFunction As File_Functions_Enum, FromFile As String, ToFile As String, Optional WhichFiles As ConcatinationStatus_Enum)
    
    
    On Error GoTo LocalHandler
    If FileFunction = FO_DELETE Then
        Select Case WhichFiles
            Case File1
                Kill FromFile
            Case file2
                Kill ToFile
            Case both
                Kill ToFile
                Kill FromFile
        End Select
    Else
        
        Dim lResult As Long, SHF As SHFILEOPSTRUCT
        SHF.hwnd = hwnd
        SHF.wFunc = FileFunction
        SHF.pFrom = FromFile
        SHF.pTo = ToFile
        SHF.fFlags = FOF_FILESONLY
        lResult = SHFileOperation(SHF)
        'If lResult Then
        '    MsgBox "Error occurred!", vbInformation, "SHCOPY"
        'End If
    End If
LocalHandler:
    'Debug.Print Err.Number
    If Err.Number = 53 Then
        If ToFile <> "" And FileFunction = FO_DELETE And CurrentFile = FromFile Then Resume Next
        Exit Sub
    End If
End Sub

