Attribute VB_Name = "Parameters"
Type Stats_Importer_Stats
    ServiceName As String
    ImporterPath As String
    StatsStartsWith As String
    StatsDirectory As String
    TempFileName As String
End Type

Public Function GetConfigurations(Configuration_Path As String) As Stats_Importer_Stats
    
    Import_File = FreeFile
    Dim application_properties() As String
    'Load application properties
    Step = 0

    Open Configuration_Path For Input As Import_File
      '  Shell App.Path + "\Insert_CCSR.bat"
      '  End
        Do Until EOF(Import_File)
            ReDim Preserve application_properties(Step)
            Line Input #Import_File, application_properties(Step)
            Step = Step + 1
        Loop
    Close CCSR_Import_File
    
    Dim Checker As String
    For I = 0 To UBound(application_properties)
        Checker = Mid(application_properties(I), Len("<entry key=") + 2, InStr(1, application_properties(I), ">") - Len("<entry key=") - 3)
        Select Case Checker
            Case "Service Name"
                GetConfigurations.ServiceName = Mid(application_properties(I), Len("<entry key=Service Name") + 4, Len(application_properties(I)) - Len("</entry>") - Len("<entry key=Service Name>") - 2)
            Case "Importer Path"
                GetConfigurations.ImporterPath = Mid(application_properties(I), Len("<entry key=Importer Path") + 4, Len(application_properties(I)) - Len("</entry>") - Len("<entry key=Importer Path>") - 2)
            Case "Stats Starts With"
                GetConfigurations.StatsStartsWith = Mid(application_properties(I), Len("<entry key=Stats Starts With") + 4, Len(application_properties(I)) - Len("</entry>") - Len("<entry key=Stats Starts With>") - 2)
             Case "Stats Directory"
                GetConfigurations.StatsDirectory = Mid(application_properties(I), Len("<entry key=Stats Directory") + 4, Len(application_properties(I)) - Len("</entry>") - Len("<entry key=Stats Directory>") - 2)
             Case "Temp File Name"
                GetConfigurations.TempFileName = Mid(application_properties(I), Len("<entry key=Temp File Name") + 4, Len(application_properties(I)) - Len("</entry>") - Len("<entry key=Temp File Name>") - 2)
             
        End Select
    Next I
End Function
