VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Dim Conf As Stats_Importer_Stats
    
    Conf = GetConfigurations(App.Path + "\properties.txt")
    
    'Call RenameFile("7akawy MSISDNs.TXT", "SSS.ttt", "D:\Work\Exchange")
    'If Count_ProcessRunning(Conf.ServiceName) > 1 Then End
    
    On Error GoTo LocalHandler
        
    
    Dim FileName As String
    FileName = Dir(Conf.StatsDirectory + "\" + Conf.StatsStartsWith + "*")
    
    While FileName <> ""
        If FileExists(Conf.StatsDirectory + "\" + Conf.TempFileName) Then Kill Conf.StatsDirectory + "\" + Conf.TempFileName
        If Right(FileName, Len(Conf.TempFileName)) <> Conf.TempFileName Then
            
            RenameFile FileName, Conf.TempFileName, Conf.StatsDirectory
            
            If Mid(Conf.ImporterPath, 2, 1) <> ":" Then Conf.ImporterPath = App.Path + "\" + Conf.ImporterPath
            If FileExists(Conf.ImporterPath) Then Shell Conf.ImporterPath
           ' Shell Conf.ImporterPath
            
            Wait 5000
        End If
        FileName = Dir
    Wend
        


    End
    
LocalHandler:
    If Err.Number = 53 Then
        Wait 30000
        Resume
    End If
End Sub

Private Sub RenameFile(OldFileName As String, NewFileName As String, Directory As String)
    Renamer = FreeFile
    Dim Drive As String
    Drive = Left(Directory, InStr(1, Directory, ":"))
    Open App.Path + "\Renamer.bat" For Output As Renamer
        Print #Renamer, Drive
        Print #Renamer, "cd " + Directory
        Print #Renamer, "ren " + Chr(34) + OldFileName + Chr(34) + " " + Chr(34) + NewFileName + Chr(34)
    Close Renamer
    Shell App.Path + "\Renamer.bat"
    'Kill App.Path + "\Renamer.bat"
End Sub
Function FileExists(File As String) As Boolean
    FilePath = FreeFile
    FileExists = True
    On Error GoTo Error
    Open File For Input As FilePath
    Close FilePath
Error:
    'Return False if an error occurs
    If Err.Number = 0 Or Err.Number = 20 Then Exit Function
    FileExists = False
End Function
Function DoesExist(File As String) As Boolean
    On Error GoTo Error
    'get the file attributes, and make sure what
    'is being passed isnt a directory
    DoesExist = (GetAttr(File) And vbDirectory) = 0
Error:
    'Return False if an error occurs
    DoesExist = False
End Function
