Attribute VB_Name = "Used_Functions"
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Const CB_SHOWDROPDOWN = &H14F
Public PSTR As String
Public Declare Function GetTickCount Lib "kernel32" () As Long


Public Sub Load_List(ByRef Combo_Name As ComboBox, File_Name As String)

    I_File_Name = FreeFile
    Dim File_List() As String
    Step = 0
    On Error GoTo Error_Handler
    
    Open App.Path + "\" + File_Name + ".txt" For Input As I_File_Name

        Do Until EOF(I_File_Name)
            ReDim Preserve File_List(Step)
            Line Input #I_File_Name, File_List(Step)
            If File_List(Step) <> "" Then
                Step = Step + 1
            End If
        Loop
    Close I_File_Name
    
    For I = 0 To UBound(File_List)
        If File_List(I) <> "" Then Combo_Name.AddItem Mid(File_List(I), 1, Len(File_List(I)) - 4)
    Next I
        
    Combo_Name.Text = Combo_Name.List(0)

Error_Handler:
    If Err.Number = 53 Then
        Dim O_File_Name As Long
        O_File_Name = FreeFile
        Open App.Path + "\" + File_Name + ".txt" For Output As O_File_Name
        Close O_File_Name
    End If
End Sub
Public Sub Save_List(Combo_Name As ComboBox, File_Name As String)


On Error GoTo Save_List_Error_Handler
   
   
    Dim found_in_list As Boolean
    Dim Index_S As String
    found_in_list = False
    
    For I = 0 To Combo_Name.ListCount
        If Combo_Name.Text = Combo_Name.List(I) Then found_in_list = True
        Next I

    If found_in_list = False Then
        Combo_Name.AddItem Combo_Name.Text, 0
        Combo_Name.Text = Combo_Name.List(0)
    End If
    
    Dim O_File_Name As Long
    Dim Index As Integer
    O_File_Name = FreeFile
                Open File_Name + ".txt" For Output As O_File_Name
                
                    Print #O_File_Name, Combo_Name.Text + "," + "000"
                    I = 0
                    Index = 1
                    While I <= Combo_Name.ListCount
                        
                        If Index <= 9 Then Index_S = Trim_All("00" + Str(Index))
                        If Index >= 10 And I <= 99 Then Index_S = Trim_All("0" + Str(Index))
                        If Index >= 100 Then Index_S = Trim_All(Str(Index))
                    
                        If Not (Combo_Name.List(I) = Combo_Name.Text Or Combo_Name.List(I) = "") Then
                            Print #O_File_Name, Combo_Name.List(I) + "," + Index_S
                            Index = Index + 1
                        End If
                        I = I + 1
                    Wend
                        
                Close O_File_Name


Save_List_Error_Handler:
    If Err.Number = 53 Then
            O_Favorites = FreeFile
            Open Config.Conf_File_Loc + "\Favorites.txt" For Output As O_Favorites
            Close O_Favorites
    End If

End Sub
Public Function IsProcessRunning(ByVal Process_Name As String) As Boolean
    
    Dim Process, strObject
    IsProcessRunning = False
    strObject = "winmgmts://"
    
    For Each Process In GetObject(strObject).InstancesOf("win32_process")
    If UCase(Process.Name) = UCase(Process_Name) Then
            IsProcessRunning = True
            Exit Function
        End If
    Next

End Function
Public Function Trim_All(ByVal To_Be_Trimmed As String, Optional Not_Trim_Enter As Boolean)
    Dim Result, Char_ As String
    For I = 1 To Len(To_Be_Trimmed)
        Char_ = Mid(To_Be_Trimmed, I, 1)
        If Not ((Asc(Char_) = 13 And Not Not_Trim_Enter) Or Asc(Char_) = 32) Then Result = Result + Char_
    Next I
    Trim_All = Result
End Function
Public Sub Load_AIR_IPs(ComboBox_ As ComboBox)
    On Error GoTo IP_Loads
    Dim ErrNumber As Integer
    Call Load_List(ComboBox_, "AIR_IPs")
    
IP_Loads:
    If ErrNumber = 53 Then
        Dim Continue As Integer
        Continue = MsgBox("Error Reading AIR_IPs File, Let the Application Selects the IP?", vbYesNo, "IP Missing")
        If Continue = 6 Then
            ComboBox_.AddItem "10.231.14.21", 0
            ComboBox_.Text = ComboBox_.List(0)
        End If
        Call Save_List(ComboBox_, "AIR_IPs")
    End If
End Sub
Public Sub Wait(ByVal dblMilliseconds As Double)
    Dim dblStart As Double
    Dim dblEnd As Double
    Dim dblTickCount As Double
    
    dblTickCount = GetTickCount()
    dblStart = GetTickCount()
    dblEnd = GetTickCount + dblMilliseconds
    
    Do
    DoEvents
    dblTickCount = GetTickCount()
    Loop Until dblTickCount > dblEnd Or dblTickCount < dblStart
       
    
End Sub
