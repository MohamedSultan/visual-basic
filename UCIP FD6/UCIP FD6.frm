VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form UCIP_FD6 
   Caption         =   "UCIP Command Creation"
   ClientHeight    =   9240
   ClientLeft      =   1230
   ClientTop       =   1785
   ClientWidth     =   10665
   Icon            =   "UCIP FD6.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9240
   ScaleWidth      =   10665
   Begin VB.Frame Frame 
      Caption         =   "UpdateBalanceAndDate"
      Height          =   7215
      Index           =   8
      Left            =   5160
      TabIndex        =   26
      Top             =   2040
      Width           =   5055
      Begin VB.TextBox dedicatedAccountUpdateInformation 
         Height          =   285
         Left            =   2520
         TabIndex        =   156
         Top             =   5280
         Width           =   2295
      End
      Begin VB.TextBox externalData1 
         Height          =   285
         Index           =   8
         Left            =   2520
         TabIndex        =   153
         Top             =   4560
         Width           =   2295
      End
      Begin VB.TextBox externalData2 
         Height          =   285
         Index           =   8
         Left            =   2520
         TabIndex        =   152
         Top             =   4920
         Width           =   2295
      End
      Begin VB.TextBox transactionType 
         Height          =   285
         Index           =   8
         Left            =   2520
         TabIndex        =   149
         Top             =   3840
         Width           =   2295
      End
      Begin VB.TextBox transactionCode 
         Height          =   285
         Index           =   8
         Left            =   2520
         TabIndex        =   148
         Top             =   4200
         Width           =   2295
      End
      Begin VB.TextBox supervisionExpiryDateRelative 
         Height          =   285
         Left            =   2520
         TabIndex        =   140
         Top             =   1680
         Width           =   2295
      End
      Begin VB.TextBox supervisionExpiryDate 
         Height          =   285
         Left            =   2520
         TabIndex        =   139
         Top             =   2040
         Width           =   2295
      End
      Begin VB.TextBox serviceFeeExpiryDateRelative 
         Height          =   285
         Left            =   2520
         TabIndex        =   138
         Top             =   2400
         Width           =   2295
      End
      Begin VB.TextBox serviceFeeExpiryDate 
         Height          =   285
         Left            =   2520
         TabIndex        =   137
         Top             =   2760
         Width           =   2295
      End
      Begin VB.TextBox creditClearancePeriod 
         Height          =   285
         Left            =   2520
         TabIndex        =   136
         Top             =   3120
         Width           =   2295
      End
      Begin VB.TextBox serviceRemovalPeriod 
         Height          =   285
         Left            =   2520
         TabIndex        =   135
         Top             =   3480
         Width           =   2295
      End
      Begin VB.TextBox adjustmentAmountRelative 
         Height          =   285
         Left            =   2520
         TabIndex        =   134
         Top             =   1320
         Width           =   2295
      End
      Begin VB.TextBox originOperatorID 
         Height          =   285
         Index           =   8
         Left            =   2520
         TabIndex        =   132
         Top             =   960
         Width           =   2295
      End
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   6
         Left            =   0
         TabIndex        =   128
         Top             =   5640
         Width           =   5055
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   2
            Left            =   240
            TabIndex        =   131
            Top             =   360
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   2
            Left            =   240
            TabIndex        =   130
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   2
            Left            =   240
            TabIndex        =   129
            Top             =   1080
            Width           =   3735
         End
      End
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   8
         Left            =   2520
         TabIndex        =   126
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label30 
         Caption         =   "dedicatedAccountUpdateInfo"
         Height          =   255
         Left            =   120
         TabIndex        =   157
         Top             =   5280
         Width           =   2415
      End
      Begin VB.Label Label14 
         Caption         =   "externalData1"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   155
         Top             =   4560
         Width           =   1935
      End
      Begin VB.Label Label15 
         Caption         =   "externalData2"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   154
         Top             =   4920
         Width           =   1935
      End
      Begin VB.Label Label16 
         Caption         =   "transactionType"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   151
         Top             =   3840
         Width           =   1935
      End
      Begin VB.Label Label17 
         Caption         =   "transactionCode"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   150
         Top             =   4200
         Width           =   1935
      End
      Begin VB.Label Label37 
         Caption         =   "adjustmentAmountRelative"
         Height          =   255
         Left            =   120
         TabIndex        =   147
         Top             =   1320
         Width           =   1935
      End
      Begin VB.Label Label36 
         Caption         =   "supervisionExpiryDateRelative"
         Height          =   255
         Left            =   120
         TabIndex        =   146
         Top             =   1680
         Width           =   2295
      End
      Begin VB.Label Label35 
         Caption         =   "supervisionExpiryDate"
         Height          =   255
         Left            =   120
         TabIndex        =   145
         Top             =   2040
         Width           =   1935
      End
      Begin VB.Label Label34 
         Caption         =   "serviceFeeExpiryDateRelative"
         Height          =   255
         Left            =   120
         TabIndex        =   144
         Top             =   2400
         Width           =   2295
      End
      Begin VB.Label Label33 
         Caption         =   "serviceFeeExpiryDate"
         Height          =   255
         Left            =   120
         TabIndex        =   143
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label32 
         Caption         =   "creditClearancePeriod"
         Height          =   255
         Left            =   120
         TabIndex        =   142
         Top             =   3120
         Width           =   1935
      End
      Begin VB.Label Label31 
         Caption         =   "serviceRemovalPeriod"
         Height          =   255
         Left            =   120
         TabIndex        =   141
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label13 
         Caption         =   "originOperatorID"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   133
         Top             =   960
         Width           =   1935
      End
      Begin VB.Label Label28 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   127
         Top             =   480
         Width           =   2175
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Refill"
      Height          =   7095
      Index           =   6
      Left            =   5160
      TabIndex        =   24
      Top             =   2040
      Width           =   5055
      Begin VB.CheckBox requestRefillAccountAfterFlag 
         Caption         =   "requestRefillAccountAfterFlag"
         Height          =   255
         Left            =   240
         TabIndex        =   103
         Top             =   4440
         Width           =   2775
      End
      Begin VB.CheckBox requestRefillDetailsFlag 
         Caption         =   "requestRefillDetailsFlag"
         Height          =   255
         Left            =   240
         TabIndex        =   102
         Top             =   4800
         Width           =   2295
      End
      Begin VB.CheckBox requestRefillAccountBeforeFlag 
         Caption         =   "requestRefillAccountBeforeFlag"
         Height          =   255
         Left            =   240
         TabIndex        =   101
         Top             =   4080
         Width           =   2655
      End
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   4
         Left            =   0
         TabIndex        =   97
         Top             =   5520
         Width           =   5055
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   6
            Left            =   240
            TabIndex        =   100
            Top             =   1080
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   6
            Left            =   240
            TabIndex        =   99
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   6
            Left            =   240
            TabIndex        =   98
            Top             =   360
            Width           =   3735
         End
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   2640
         TabIndex        =   95
         Top             =   5160
         Width           =   2295
      End
      Begin VB.TextBox selectedOption 
         Height          =   285
         Left            =   2640
         TabIndex        =   93
         Top             =   3720
         Width           =   2295
      End
      Begin VB.TextBox voucherActivationCode 
         Height          =   285
         Index           =   6
         Left            =   2640
         TabIndex        =   91
         Top             =   3360
         Width           =   2295
      End
      Begin VB.TextBox refillProfileID 
         Height          =   285
         Left            =   2640
         TabIndex        =   89
         Top             =   3000
         Width           =   2295
      End
      Begin VB.TextBox transactionAmount 
         Height          =   285
         Left            =   2640
         TabIndex        =   87
         Top             =   2640
         Width           =   2295
      End
      Begin VB.TextBox transactionCode 
         Height          =   285
         Index           =   6
         Left            =   2640
         TabIndex        =   85
         Top             =   2280
         Width           =   2295
      End
      Begin VB.TextBox transactionType 
         Height          =   285
         Index           =   6
         Left            =   2640
         TabIndex        =   83
         Top             =   1920
         Width           =   2295
      End
      Begin VB.TextBox externalData2 
         Height          =   285
         Index           =   6
         Left            =   2640
         TabIndex        =   81
         Top             =   1560
         Width           =   2295
      End
      Begin VB.TextBox externalData1 
         Height          =   285
         Index           =   6
         Left            =   2640
         TabIndex        =   79
         Top             =   1200
         Width           =   2295
      End
      Begin VB.TextBox originOperatorID 
         Height          =   285
         Index           =   6
         Left            =   2640
         TabIndex        =   78
         Top             =   840
         Width           =   2295
      End
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   6
         Left            =   2640
         TabIndex        =   75
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label27 
         Caption         =   "originOperatorID"
         Height          =   255
         Left            =   240
         TabIndex        =   96
         Top             =   5160
         Width           =   1935
      End
      Begin VB.Label Label22 
         Caption         =   "selectedOption"
         Height          =   255
         Left            =   240
         TabIndex        =   94
         Top             =   3720
         Width           =   1935
      End
      Begin VB.Label Label21 
         Caption         =   "voucherActivationCode"
         Height          =   255
         Left            =   240
         TabIndex        =   92
         Top             =   3360
         Width           =   1935
      End
      Begin VB.Label Label20 
         Caption         =   "refillProfileID"
         Height          =   255
         Left            =   240
         TabIndex        =   90
         Top             =   3000
         Width           =   1935
      End
      Begin VB.Label Label19 
         Caption         =   "transactionAmount"
         Height          =   255
         Left            =   240
         TabIndex        =   88
         Top             =   2640
         Width           =   1935
      End
      Begin VB.Label Label17 
         Caption         =   "transactionCode"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   86
         Top             =   2280
         Width           =   1935
      End
      Begin VB.Label Label16 
         Caption         =   "transactionType"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   84
         Top             =   1920
         Width           =   1935
      End
      Begin VB.Label Label15 
         Caption         =   "externalData2"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   82
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label14 
         Caption         =   "externalData1"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   80
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label13 
         Caption         =   "originOperatorID"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   77
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label Label12 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   240
         TabIndex        =   76
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "UpdateAccountDetails"
      Height          =   5535
      Index           =   7
      Left            =   5160
      TabIndex        =   25
      Top             =   2160
      Width           =   5055
      Begin VB.CheckBox pinCodeValidationFlag 
         Caption         =   "pinCodeValidationFlag"
         Height          =   255
         Left            =   120
         TabIndex        =   125
         Top             =   2880
         Width           =   2655
      End
      Begin VB.CheckBox firstIVRCallDoneFlag 
         Caption         =   "firstIVRCallDoneFlag"
         Height          =   255
         Left            =   120
         TabIndex        =   124
         Top             =   3600
         Width           =   2655
      End
      Begin VB.TextBox pinCode 
         Height          =   285
         Left            =   2520
         TabIndex        =   122
         Top             =   3240
         Width           =   2295
      End
      Begin VB.TextBox pinCodeOriginal 
         Height          =   285
         Left            =   2520
         TabIndex        =   120
         Top             =   2520
         Width           =   2295
      End
      Begin VB.TextBox accountHomeRegion 
         Height          =   285
         Left            =   2520
         TabIndex        =   118
         Top             =   2160
         Width           =   2295
      End
      Begin VB.TextBox languageIDNew 
         Height          =   285
         Left            =   2520
         TabIndex        =   116
         Top             =   1800
         Width           =   2295
      End
      Begin VB.TextBox externalData1 
         Height          =   285
         Index           =   7
         Left            =   2520
         TabIndex        =   113
         Top             =   1080
         Width           =   2295
      End
      Begin VB.TextBox externalData2 
         Height          =   285
         Index           =   7
         Left            =   2520
         TabIndex        =   112
         Top             =   1440
         Width           =   2295
      End
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   5
         Left            =   0
         TabIndex        =   108
         Top             =   3960
         Width           =   5055
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   7
            Left            =   240
            TabIndex        =   111
            Top             =   360
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   7
            Left            =   240
            TabIndex        =   110
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   7
            Left            =   240
            TabIndex        =   109
            Top             =   1080
            Width           =   3735
         End
      End
      Begin VB.TextBox originOperatorID 
         Height          =   285
         Index           =   7
         Left            =   2520
         TabIndex        =   106
         Top             =   720
         Width           =   2295
      End
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   7
         Left            =   2520
         TabIndex        =   104
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label29 
         Caption         =   "pinCode"
         Height          =   255
         Left            =   120
         TabIndex        =   123
         Top             =   3240
         Width           =   1935
      End
      Begin VB.Label Label26 
         Caption         =   "pinCodeOriginal"
         Height          =   255
         Left            =   120
         TabIndex        =   121
         Top             =   2520
         Width           =   1935
      End
      Begin VB.Label Label25 
         Caption         =   "accountHomeRegion"
         Height          =   255
         Left            =   120
         TabIndex        =   119
         Top             =   2160
         Width           =   1935
      End
      Begin VB.Label Label23 
         Caption         =   "languageIDNew"
         Height          =   255
         Left            =   120
         TabIndex        =   117
         Top             =   1800
         Width           =   1935
      End
      Begin VB.Label Label14 
         Caption         =   "externalData1"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   115
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Label Label15 
         Caption         =   "externalData2"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   114
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label Label13 
         Caption         =   "originOperatorID"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   107
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label Label18 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   105
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "GetRefillOptions"
      Height          =   3255
      Index           =   5
      Left            =   5160
      TabIndex        =   23
      Top             =   2040
      Width           =   5055
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   3
         Left            =   0
         TabIndex        =   71
         Top             =   1680
         Width           =   5055
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   5
            Left            =   240
            TabIndex        =   74
            Top             =   360
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   5
            Left            =   240
            TabIndex        =   73
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   5
            Left            =   240
            TabIndex        =   72
            Top             =   1080
            Width           =   3735
         End
      End
      Begin VB.TextBox voucherActivationCode 
         Height          =   285
         Index           =   0
         Left            =   2520
         TabIndex        =   69
         Top             =   1320
         Width           =   2295
      End
      Begin VB.TextBox serviceClassCurrent 
         Height          =   285
         Left            =   2520
         TabIndex        =   67
         Top             =   840
         Width           =   2295
      End
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   5
         Left            =   2520
         TabIndex        =   65
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label11 
         Caption         =   "voucherActivationCode"
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label Label10 
         Caption         =   "serviceClassCurrent"
         Height          =   255
         Left            =   120
         TabIndex        =   68
         Top             =   840
         Width           =   2175
      End
      Begin VB.Label Label9 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Get Account Details"
      Height          =   3255
      Index           =   0
      Left            =   5160
      TabIndex        =   19
      Top             =   2160
      Width           =   5055
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   0
         Left            =   2400
         TabIndex        =   62
         Top             =   240
         Width           =   2295
      End
      Begin VB.Frame frame2 
         Caption         =   "requestedInformationFlags"
         Height          =   1095
         Index           =   0
         Left            =   0
         TabIndex        =   37
         Top             =   2160
         Width           =   5055
         Begin VB.CheckBox allowedServiceClassChangeDateFlag_Check 
            Caption         =   "allowedServiceClassChangeDateFlag"
            Height          =   375
            Left            =   240
            TabIndex        =   39
            Top             =   600
            Width           =   3735
         End
         Begin VB.CheckBox requestMasterAccountBalanceFlag_Check 
            Caption         =   "requestMasterAccountBalanceFlag"
            Height          =   375
            Left            =   240
            TabIndex        =   38
            Top             =   240
            Width           =   3735
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   0
         Left            =   0
         TabIndex        =   30
         Top             =   720
         Width           =   5055
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   0
            Left            =   240
            TabIndex        =   33
            Top             =   1080
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   0
            Left            =   240
            TabIndex        =   32
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   0
            Left            =   240
            TabIndex        =   31
            Top             =   360
            Width           =   3735
         End
      End
      Begin VB.Label Label1 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "GetFaFList"
      Height          =   1575
      Index           =   4
      Left            =   5160
      TabIndex        =   21
      Top             =   2160
      Width           =   5055
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   4
         Left            =   2520
         TabIndex        =   64
         Top             =   360
         Width           =   2295
      End
      Begin VB.ComboBox requestedOwner_Combo 
         Height          =   315
         Left            =   2520
         TabIndex        =   60
         Top             =   840
         Width           =   2295
      End
      Begin VB.Label Label6 
         Caption         =   "requestedOwner"
         Height          =   255
         Left            =   240
         TabIndex        =   59
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame 
      Height          =   4095
      Index           =   11
      Left            =   0
      TabIndex        =   57
      Top             =   1560
      Width           =   5055
   End
   Begin VB.Frame Frame 
      Caption         =   "GetBalanceAndDate"
      Height          =   3735
      Index           =   3
      Left            =   5160
      TabIndex        =   22
      Top             =   2160
      Width           =   5055
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   3
         Left            =   2520
         TabIndex        =   61
         Top             =   360
         Width           =   2295
      End
      Begin VB.Frame frame2 
         Caption         =   "chargingRequestInformation"
         Height          =   1455
         Index           =   2
         Left            =   0
         TabIndex        =   49
         Top             =   2280
         Width           =   5055
         Begin VB.CheckBox chargingIndicator_check 
            Caption         =   "chargingIndicator"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   52
            Top             =   600
            Width           =   3735
         End
         Begin VB.CheckBox chargingType_Check 
            Caption         =   "chargingType"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   51
            Top             =   240
            Width           =   3735
         End
         Begin VB.CheckBox reservationCorrelationID_Check 
            Caption         =   "reservationCorrelationID"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   50
            Top             =   960
            Width           =   3735
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   2
         Left            =   0
         TabIndex        =   53
         Top             =   840
         Width           =   5055
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   56
            Top             =   1080
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   55
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   54
            Top             =   360
            Width           =   3735
         End
      End
      Begin VB.Label Label3 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.ComboBox Combo_MSISDNs 
      Height          =   315
      Left            =   8040
      TabIndex        =   29
      Text            =   "Combo1"
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Frame Frame 
      Height          =   6375
      Index           =   10
      Left            =   -240
      TabIndex        =   28
      Top             =   4800
      Width           =   5055
   End
   Begin VB.Frame Frame 
      Height          =   6375
      Index           =   9
      Left            =   -120
      TabIndex        =   27
      Top             =   4560
      Width           =   5055
   End
   Begin VB.Frame Frame 
      Height          =   6375
      Index           =   2
      Left            =   3240
      TabIndex        =   20
      Top             =   3000
      Width           =   5055
   End
   Begin VB.TextBox UCIP_Command 
      Height          =   7695
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   18
      Text            =   "UCIP FD6.frx":0442
      Top             =   120
      Width           =   4935
   End
   Begin VB.Frame Frame6 
      Caption         =   "Configurations"
      Height          =   735
      Left            =   5160
      TabIndex        =   14
      Top             =   0
      Width           =   4215
      Begin VB.ComboBox AIR_IP 
         Height          =   315
         Left            =   960
         TabIndex        =   16
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label24 
         Caption         =   "AIR IP"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.CommandButton Extend_Command 
      Caption         =   ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      Height          =   8895
      Left            =   10320
      TabIndex        =   13
      Top             =   120
      Width           =   255
   End
   Begin VB.Frame Frame3 
      Caption         =   "UCIP Rersponse"
      Height          =   9135
      Left            =   10680
      TabIndex        =   5
      Top             =   0
      Width           =   6135
      Begin VB.TextBox txtUCIPResponse 
         Height          =   5175
         Left            =   240
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   6
         Top             =   3000
         Width           =   5775
      End
      Begin VB.Label Label5 
         Caption         =   "Interval"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblUCIPRespInterval 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Left            =   1440
         TabIndex        =   11
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label lblResponseCode 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   10
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "Response Code"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label lblResponseText 
         BorderStyle     =   1  'Fixed Single
         Height          =   1455
         Left            =   240
         TabIndex        =   8
         Top             =   1320
         Width           =   5655
      End
      Begin VB.Label Label8 
         Caption         =   "Response Text"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1080
         Width           =   1215
      End
   End
   Begin VB.CommandButton Send_Command 
      Caption         =   "Send Command"
      Height          =   375
      Left            =   8640
      TabIndex        =   4
      Top             =   1200
      Width           =   1575
   End
   Begin VB.CommandButton Clear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   7320
      TabIndex        =   3
      Top             =   1200
      Width           =   1095
   End
   Begin VB.CommandButton Copy 
      Caption         =   "Copy"
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Top             =   7920
      Width           =   1215
   End
   Begin VB.ComboBox UCIP_Combo 
      Height          =   315
      Left            =   5160
      TabIndex        =   1
      Text            =   "Combo1"
      Top             =   840
      Width           =   4095
   End
   Begin VB.CommandButton Generate 
      Caption         =   "Load"
      Height          =   375
      Left            =   5160
      TabIndex        =   0
      Top             =   1200
      Width           =   1935
   End
   Begin VB.Frame Frame 
      Caption         =   "GetAccumulators"
      Height          =   3615
      Index           =   1
      Left            =   5160
      TabIndex        =   35
      Top             =   2160
      Width           =   5055
      Begin VB.ComboBox subscriberNumberNAI_Combo 
         Height          =   315
         Index           =   1
         Left            =   2520
         TabIndex        =   63
         Top             =   240
         Width           =   2295
      End
      Begin VB.Frame frame2 
         Caption         =   "chargingRequestInformation"
         Height          =   1455
         Index           =   1
         Left            =   0
         TabIndex        =   40
         Top             =   2160
         Width           =   5055
         Begin VB.CheckBox reservationCorrelationID_Check 
            Caption         =   "reservationCorrelationID"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   43
            Top             =   960
            Width           =   3735
         End
         Begin VB.CheckBox chargingType_Check 
            Caption         =   "chargingType"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   42
            Top             =   240
            Width           =   3735
         End
         Begin VB.CheckBox chargingIndicator_check 
            Caption         =   "chargingIndicator"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   41
            Top             =   600
            Width           =   3735
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "messageCapabilityFlag"
         Height          =   1575
         Index           =   1
         Left            =   0
         TabIndex        =   44
         Top             =   720
         Width           =   5055
         Begin VB.CheckBox promotionNotificationFlag_Check 
            Caption         =   "promotionNotificationFlag"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   47
            Top             =   360
            Width           =   3735
         End
         Begin VB.CheckBox firstIVRCallSetFlag_Check 
            Caption         =   "firstIVRCallSetFlag"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   46
            Top             =   720
            Width           =   3735
         End
         Begin VB.CheckBox accountActivationFlag_Check 
            Caption         =   "accountActivationFlag"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   45
            Top             =   1080
            Width           =   3735
         End
      End
      Begin VB.Label Label2 
         Caption         =   "subscriberNumberNAI"
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Label Label38 
      Caption         =   "subscriberNumber"
      Height          =   255
      Left            =   5280
      TabIndex        =   158
      Top             =   1680
      Width           =   2295
   End
   Begin MSForms.CommandButton CommandButton1 
      Height          =   495
      Left            =   9480
      TabIndex        =   17
      Top             =   360
      Width           =   615
      Size            =   "1085;873"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "UCIP_FD6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Header As String
Dim Extended As Boolean

Private Sub AIR_IP_Change()
    'SulSAT.AIR_IP = Me.AIR_IP
End Sub

Private Sub AIR_IP_LostFocus()
    Call Save_List(Me.AIR_IP, "AIR_IPs")
    Call AIR_IP_Change
End Sub



Private Sub Clear_Click()
    For I = 0 To Number_Of_Mandatory_Options
        Text_Mandatory(I) = ""
    Next I
    For I = 0 To Number_Of_Optional_Options
        Text_Optional(I) = ""
        Check_Optional(I).Value = 0
    Next I
    
    Combo_MSISDNs.Text = ""
End Sub

Private Sub CommandButton1_Click()
    Dim messageCapabilityFlag As messageCapabilityFlag_Type
    messageCapabilityFlag.promotionNotificationFlag = True
    UCIP_Command = GetAccumulators("1234567", messageCapabilityFlag)
End Sub

Private Sub Extend_Command_Click()
    Select Case Extended
        Case False
            Extend_Command.Caption = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            Me.Width = 17070
            Me.Left = 1170
        Case True
            Extend_Command.Caption = "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
            Me.Width = 10785
            Me.Left = 1170 + (17070 - 10785) / 2
    End Select
    Extended = Not Extended
End Sub

Private Sub Form_Terminate()
   End
End Sub
'
Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Public Sub Generate_Click()
    
    UCIP_Command = ""
    UCIP_Command = Generate_UCIP(UCIP_Combo.Text)
    
    If UCIP_Command = "" Then
        UCIP_Command = "This command is under construction...." + vbCrLf + vbCrLf + vbCrLf + _
            "PLZ check later........."
        Else
        
        Dim Header As String
        Header = "POST /Air" + vbCrLf + _
                "User-Agent: UGw Server/3.1/1.0" + vbCrLf + _
                "Host: VASP" + vbCrLf + _
                "Content-Type: text/xml" + vbCrLf + _
                "Content-length:" + Str(Len(UCIP_Command)) + vbCrLf
                    
            UCIP_Command = Header + vbCrLf + UCIP_Command
    End If

End Sub

Private Sub Load_Combo()
    Dim Commands(12) As String

    Commands(0) = "GetAccountDetails"
    Commands(1) = "GetAccumulators"
    Commands(2) = "GetAllowedServiceClasses"
    Commands(3) = "GetBalanceAndDate"
    Commands(4) = "GetFaFList"
    Commands(5) = "GetRefillOptions"
    Commands(6) = "Refill"
    Commands(7) = "UpdateAccountDetails"
    Commands(8) = "UpdateBalanceAndDate"
    Commands(9) = "UpdateCommunityList"
    Commands(10) = "UpdateFaFList"
    Commands(11) = "UpdateServiceClass"
    Commands(12) = "UpdateSubscriberSegmentation"

    For I = 0 To UBound(Commands)
        UCIP_Combo.AddItem Commands(I)
    Next I

    UCIP_Combo.Text = UCIP_Combo.List(0)
End Sub

Private Sub Copy_Click()
    Clipboard.SetText UCIP_Command.Text
End Sub

Public Sub Form_Load()
    Call Load_List(Combo_MSISDNs, "MSISDNs")
    Number_Of_Mandatory_Options = 5
    Number_Of_Optional_Options = 8
    Load_Combo
    
    requestedOwner_Combo.AddItem "1 Subscriber"
    requestedOwner_Combo.AddItem "2 Account (reserved for future use)"
    requestedOwner_Combo.AddItem "3 Subscriber and account (reserved for future use)"
    requestedOwner_Combo.ListIndex = 0
    
    On Error GoTo Error_Handler
    For I = 0 To subscriberNumberNAI_Combo.UBound
        subscriberNumberNAI_Combo(I).AddItem "0 Unknown"
        subscriberNumberNAI_Combo(I).AddItem "1 International Number"
        subscriberNumberNAI_Combo(I).AddItem "2 National significant number"
        subscriberNumberNAI_Combo(I).AddItem "3 Network specific number"
        subscriberNumberNAI_Combo(I).AddItem "4 Subscriber Number"
        subscriberNumberNAI_Combo(I).AddItem "5 Reserved"
        subscriberNumberNAI_Combo(I).AddItem "6 Abbreviated Number"
        subscriberNumberNAI_Combo(I).AddItem "7 Reserved for extension"
        'subscriberNumberNAI_Combo(I).ListIndex = 0
    Next I
    
    Hide_all
    Frame(0).Visible = True
    Call Load_AIR_IPs(Me.AIR_IP)
Error_Handler:
    If Err.Number = 340 Then Resume Next
End Sub

Private Sub LE_Command_Click()
    In_LE.Visible = True
    If In_LE.LE.Value = True Then
        Select Case UCIP_Combo.Text
            Case "RefillT"
                In_LE.Top = UCIP.Top + Mandatory.Top + 300 + Text_Mandatory(1).Top
            Case "AdjustmentT"
                In_LE.Top = UCIP.Top + Mandatory.Top + 400 + Text_Mandatory(1).Top + Text_Mandatory(1).Height
        End Select
    End If
    In_LE.Left = Mandatory.Left + Mandatory.Width + UCIP.Left + 100
End Sub

Public Sub Send_Command_Click()

    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If Mid(AIR_IP, 1, 3) = "172" Then
            If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then     'Connected
                Call UCIP_Connect
            Else
                Dim Connect_ As Integer
                Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                    If Connect_ = 1 Then
                        Call Main.VPN_Connect_Click
                        Wait 1000
                        Call Me.Send_Command_Click
                    End If
            End If
        Else
            If AIR_IP <> "" Then _
                Call UCIP_Connect
        End If
    End If
         
End Sub
Private Sub UCIP_Connect()
            Dim airReq As ServerXMLHTTP30 'XMLHTTP
            Dim ucipCommand As New DOMDocument
            Dim UCIP_Response As New DOMDocument
            Dim bret As Boolean
            'Dim lret As Long
            Dim url As String
            Dim headerStr As String
            Dim headerPairs() As String
            Dim headerPairItems() As String
            Dim cntr As Integer
            Dim sTime As Long
            Dim eTime As Long
        
            'AIR Request
            If Not (airReq Is Nothing) Then
                Set airReq = Nothing
            End If
            Set airReq = New ServerXMLHTTP30
        
        
            'url = "http://172.30.5.31:10010/Air"
            url = "http://" + Trim(AIR_IP) + ":10010/Air"
        
            'Header
            headerStr = Header      'txtUCIPRequestHeader.Text
            headerPairs = Split(headerStr, vbCrLf)
        
            Call airReq.open("POST", url, False)
            For cntr = 1 To UBound(headerPairs)
                If (headerPairs(cntr) <> "") Then
                    headerPairItems = Split(headerPairs(cntr), ":")
                    Call airReq.setRequestHeader(headerPairItems(0), headerPairItems(1))
                End If
            Next
            
            Dim Start_Body As Integer
            Dim UCIP_Command_Trimmed As String
            
            Start_Body = InStr(1, UCIP_Command, "<?xml version=")
            UCIP_Command_Trimmed = Mid(UCIP_Command, Start_Body, Len(UCIP_Command) - Start_Body)
            
            'Body
            bret = ucipCommand.loadXML(UCIP_Command_Trimmed)
        
            sTime = GetTickCount()
            Call airReq.setTimeouts(100, 1000, 1000, 5000)
            On Error GoTo SulSat_Handler
            Call airReq.send(ucipCommand.xml & vbCrLf & vbCrLf)
            eTime = GetTickCount()
        
            'Display Response
            lblUCIPRespInterval.Caption = CStr(eTime - sTime)
            
            If airReq.readyState = 4 Then
                lblResponseCode.Caption = CStr(airReq.Status)
                lblResponseText.Caption = CStr(airReq.responseText)
                txtUCIPResponse.Text = airReq.responseXML.xml
                UCIP_Response.loadXML (airReq.responseXML.xml)
                'UCIP_response.
                'X = UCIP_response.Text
                Last_UCIP_Response = UCIP_Response.Text
                Call Parsing(UCIP_Response.Text)
            End If
        
SulSat_Handler:
        If Err.Number = -2147012894 Then MsgBox "Connection Error"
End Sub


Private Sub Text_Optional_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call Generate_Click
End Sub

Public Sub UCIP_Combo_Change()
    Hide_all
    If UCIP_Combo.ListIndex >= 0 Then UCIP_FD6.Frame(UCIP_Combo.ListIndex).Visible = True
    If UCIP_Combo.ListIndex = -1 Then UCIP_Combo.ListIndex = 0
End Sub

Public Sub Hide_all()
    For I = 0 To Frame.UBound
        Frame(I).Visible = False
    Next I
    In_LE.Visible = False
    Extention.Visible = False
End Sub


Private Sub UCIP_Combo_Click()
    Call UCIP_Combo_Change
End Sub


Private Sub combo_msisdns_GotFocus()
    SendMessage Combo_MSISDNs.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub combo_msisdns_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = Combo_MSISDNs.Text
        For I = 0 To Combo_MSISDNs.ListCount - 1
            If StrComp(PSTR, (Left(Combo_MSISDNs.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                Combo_MSISDNs.ListIndex = I
            Exit For
            End If
        Next I
        Combo_MSISDNs.SelStart = Len(PSTR)
        Combo_MSISDNs.SelLength = Len(Combo_MSISDNs.Text) - Len(PSTR)
    End If
End Sub

Private Sub combo_msisdns_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(Combo_MSISDNs.Text) = 0) Then
        SendMessage Combo_MSISDNs.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage Combo_MSISDNs.hwnd, CB_SHOWDROPDOWN, 0, 1
    End If
    If KeyAscii = 32 And Len(Combo_MSISDNs.Text) = 0 Then KeyAscii = 0
    
End Sub

Private Sub combo_msisdns_LostFocus()
'    Text_Mandatory(0) = Combo_MSISDNs.Text
    Call Save_List(Combo_MSISDNs, "MSISDNS")
    'Call Roll_Out_Msisdns(Combo_MSISDNs)
End Sub
Private Function Generate_UCIP(CommandName As String) As String
    Dim messageCapabilityFlag As messageCapabilityFlag_Type
    Dim chargingRequestInformation As chargingRequestInformation_Type
    Dim requestedInformationFlags As requestedInformationFlags_Type
    
    Select Case CommandName
        ' Frame 0
        Case "GetAccountDetails"
            With messageCapabilityFlag
                .accountActivationFlag = accountActivationFlag_Check(UCIP_Combo.ListIndex).Value
                .firstIVRCallSetFlag = firstIVRCallSetFlag_Check(UCIP_Combo.ListIndex).Value
                .promotionNotificationFlag = promotionNotificationFlag_Check(UCIP_Combo.ListIndex).Value
            End With
            
            With requestedInformationFlags
                .allowedServiceClassChangeDateFlag = allowedServiceClassChangeDateFlag_Check.Value
                .requestMasterAccountBalanceFlag = requestMasterAccountBalanceFlag_Check.Value
            End With
            
            Generate_UCIP = GetAccountDetails(Combo_MSISDNs.Text, messageCapabilityFlag, requestedInformationFlags, Left(subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), 1))
        
        ' Frame 1
        Case "GetAccumulators"
            With messageCapabilityFlag
                .accountActivationFlag = accountActivationFlag_Check(UCIP_Combo.ListIndex).Value
                .firstIVRCallSetFlag = firstIVRCallSetFlag_Check(UCIP_Combo.ListIndex).Value
                .promotionNotificationFlag = promotionNotificationFlag_Check(UCIP_Combo.ListIndex).Value
            End With
           
            With chargingRequestInformation
                .chargingIndicator = chargingIndicator_check(UCIP_Combo.ListIndex).Value
                .chargingType = chargingType_Check(UCIP_Combo.ListIndex).Value
                .reservationCorrelationID = reservationCorrelationID_Check(UCIP_Combo.ListIndex).Value
            End With
            
            Generate_UCIP = GetAccumulators(Combo_MSISDNs.Text, messageCapabilityFlag, chargingRequestInformation, Left(subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), 1))
        
        ' Frame 2
        Case "GetAllowedServiceClasses"
        
        ' Frame 3
        Case "GetBalanceAndDate"
            With messageCapabilityFlag
                .accountActivationFlag = accountActivationFlag_Check(UCIP_Combo.ListIndex).Value
                .firstIVRCallSetFlag = firstIVRCallSetFlag_Check(UCIP_Combo.ListIndex).Value
                .promotionNotificationFlag = promotionNotificationFlag_Check(UCIP_Combo.ListIndex).Value
            End With
            
            
            With chargingRequestInformation
                .chargingIndicator = chargingIndicator_check(UCIP_Combo.ListIndex).Value
                .chargingType = chargingType_Check(UCIP_Combo.ListIndex).Value
                .reservationCorrelationID = reservationCorrelationID_Check(UCIP_Combo.ListIndex).Value
            End With
            
            Generate_UCIP = GetBalanceAndDate(Combo_MSISDNs.Text, messageCapabilityFlag, chargingRequestInformation, Left(subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), 1))
        
        ' Frame 4
        Case "GetFaFList"
            Generate_UCIP = GetFaFList(Combo_MSISDNs.Text, Left(requestedOwner_Combo.Text, 1), Left(subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), 1))
        
        ' Frame 5
        Case "GetRefillOptions"
            With messageCapabilityFlag
                .accountActivationFlag = accountActivationFlag_Check(UCIP_Combo.ListIndex).Value
                .firstIVRCallSetFlag = firstIVRCallSetFlag_Check(UCIP_Combo.ListIndex).Value
                .promotionNotificationFlag = promotionNotificationFlag_Check(UCIP_Combo.ListIndex).Value
            End With
            
            Generate_UCIP = GetRefillOptions(Combo_MSISDNs.Text, messageCapabilityFlag, voucherActivationCode(5), serviceClassCurrent, Left(subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), 1))
        ' Frame 6
        Case "Refill"
            With messageCapabilityFlag
                .accountActivationFlag = accountActivationFlag_Check(UCIP_Combo.ListIndex).Value
                .firstIVRCallSetFlag = firstIVRCallSetFlag_Check(UCIP_Combo.ListIndex).Value
                .promotionNotificationFlag = promotionNotificationFlag_Check(UCIP_Combo.ListIndex).Value
            End With
            Dim requestRefillAccountBeforeFlag_B, requestRefillAccountAfterFlag_B, requestRefillDetailsFlag_B As Boolean
            
            If requestRefillAccountBeforeFlag Then requestRefillAccountBeforeFlag_B = True
            If requestRefillAccountAfterFlag Then requestRefillAccountAfterFlag_B = True
            If requestRefillDetailsFlag Then requestRefillDetailsFlag_B = True
                                    
            Generate_UCIP = Refill(Combo_MSISDNs.Text, transactionAmount, "EGP", refillProfileID, voucherActivationCode(UCIP_Combo.ListIndex), _
                                    subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), messageCapabilityFlag, originOperatorID(UCIP_Combo.ListIndex), externalData1(UCIP_Combo.ListIndex), externalData2(UCIP_Combo.ListIndex), transactionType(UCIP_Combo.ListIndex), _
                                    transactionCode(UCIP_Combo.ListIndex), requestRefillAccountBeforeFlag_B, requestRefillAccountAfterFlag_B, _
                                    requestRefillDetailsFlag_B, selectedOption)
        ' Frame 7
        Case "UpdateAccountDetails"
            Dim firstIVRCallDoneFlag_B, pinCodeValidationFlag_B As Boolean
            If firstIVRCallDoneFlag Then firstIVRCallDoneFlag_B = True
            If pinCodeValidationFlag Then pinCodeValidationFlag_B = True
            
            With messageCapabilityFlag
                .accountActivationFlag = accountActivationFlag_Check(UCIP_Combo.ListIndex).Value
                .firstIVRCallSetFlag = firstIVRCallSetFlag_Check(UCIP_Combo.ListIndex).Value
                .promotionNotificationFlag = promotionNotificationFlag_Check(UCIP_Combo.ListIndex).Value
            End With

            Generate_UCIP = UpdateAccountDetails(Combo_MSISDNs.Text, messageCapabilityFlag, subscriberNumberNAI_Combo(UCIP_Combo.ListIndex), _
                                                originOperatorID(UCIP_Combo.ListIndex), languageIDNew, _
                                                firstIVRCallDoneFlag_B, externalData1(UCIP_Combo.ListIndex), externalData2(UCIP_Combo.ListIndex), _
                                                accountHomeRegion, pinCodeOriginal, pinCodeValidationFlag_B, pinCode)
        ' Frame 8
        Case "UpdateBalanceAndDate"
        
        ' Frame 9
        Case "UpdateCommunityList"
        
        ' Frame 10
        Case "UpdateFaFList"
        
        ' Frame 11
        Case "UpdateServiceClass"
        
        ' Frame 12
        Case "UpdateSubscriberSegmentation"
    
    End Select
End Function
