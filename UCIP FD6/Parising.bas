Attribute VB_Name = "Parising"
Public UCIP_Response_Data  As UCIP_Response_Type
Public Type DedicatedAccounts
    DedicatedAccountValue As String
    DedicateAccountExpiryDate As String
End Type
Public Type CCPAI_Parser_Return
    Ret As Variant
    found As Boolean
End Type
Public Type UCIP_Response_Type
    accountActivatedFlag As String
    accountFlags As String
    accountValue1 As String
    accountValue2 As String
    accountValueAfter1 As String
    accountValueAfter2 As String
    accumulatorInformation As String
    allowedOptions As String
    chargingResultInformation As String
    charginResultInformation As String
    creditClearanceDate As String
    creditClearancePeriod As String
    currency1 As String
    currency2 As String
    currentLanguageID As String
    dedicatedAccountInformation As String
    fafChangeUnbarDate As String
    fafInformation As String
    firstIVRCallFlag As String
    masterSubscriberFlag As String
    maxAllowedNumbers As String
    notAllowedReason As String
    pinCode As String
    promotionAnnouncementCode As String
    rechargeAmount1MainTotal As String
    rechargeAmount1PromoTotal As String
    rechargeAmount2MainTotal As String
    rechargeAmount2PromoTotal As String
    refillBarredFlag As String
    refillFraudCount As String
    removalPeriod As String
    ResponseCode As String
    serviceClassChangeUnbarDate As String
    serviceClassCurrent As String
    serviceFeeDate As String
    serviceFeeDateAfter As String
    serviceFeeDaysPromoExt As String
    serviceFeeDaysTotalExt As String
    serviceRemovalDate As String
    supervisionDate As String
    supervisionDateAfter As String
    supervisionDaysPromoExt As String
    supervisionDaysTotalExt As String
    temporaryBlockedFlag As String
    transactionAmount1Refill As String
    transactionAmount2Refill As String
    transactionServiceClass As String
    transactionVVPeriodExt As String
    valueVoucherDate As String
    valueVoucherDateAfter As String
    serviceFeePeriod As String
    supervisionPeriod As String
    DedicatedAccount(5) As DedicatedAccounts
End Type

Public Function Parser(ByVal Whole_String As String, ByVal Delimiter As String) As CCPAI_Parser_Return
    Dim Vars() As String
    Dim Locations(), Step, Loc As Integer
    
    Whole_String = Replace(Whole_String, Delimiter, ";")
    Loc = 1
    'Finding Number Of Vars
        While Loc <> 0
            Loc = InStr(Loc + 1, Whole_String, ";")
            ReDim Preserve Locations(Step)
            Locations(Step) = Loc
            Step = Step + 1
        Wend
    
    Locations(UBound(Locations)) = Len(Whole_String)
    ReDim Preserve Vars(Step - 1)
    
    If Step > 0 Then
        Parser.found = True
        Vars(0) = Mid(Whole_String, 1, Locations(0) - 1)
        For I = 1 To UBound(Vars) - 1
            If Locations(I - 1) <> Locations(I) Then Vars(I) = Mid(Whole_String, Locations(I - 1) + Len(";"), Locations(I) - Locations(I - 1) - 1)
        Next I
    End If
    Parser.Ret = Vars
    
End Function


Public Sub Parsing(ByRef UCIP_Response As String)
        UCIP_Response_Data.accountActivatedFlag = Values(UCIP_Response, "accountActivatedFlag")
        UCIP_Response_Data.accountFlags = Values(UCIP_Response, "accountFlags")
        UCIP_Response_Data.accountValue1 = Values(UCIP_Response, "accountValue1")
        UCIP_Response_Data.accountValue2 = Values(UCIP_Response, "accountValue2")
        UCIP_Response_Data.accountValueAfter1 = Values(UCIP_Response, "accountValueAfter1")
        UCIP_Response_Data.accountValueAfter2 = Values(UCIP_Response, "accountValueAfter2")
        UCIP_Response_Data.accumulatorInformation = Values(UCIP_Response, "accumulatorInformation")
        UCIP_Response_Data.allowedOptions = Values(UCIP_Response, "allowedOptions")
        UCIP_Response_Data.chargingResultInformation = Values(UCIP_Response, "chargingResultInformation")
        UCIP_Response_Data.charginResultInformation = Values(UCIP_Response, "charginResultInformation")
        UCIP_Response_Data.creditClearanceDate = Values(UCIP_Response, "creditClearanceDate")
        UCIP_Response_Data.creditClearancePeriod = Values(UCIP_Response, "creditClearancePeriod")
        UCIP_Response_Data.currency1 = Values(UCIP_Response, "currency1")
        UCIP_Response_Data.currency2 = Values(UCIP_Response, "currency2")
        UCIP_Response_Data.currentLanguageID = Values(UCIP_Response, "currentLanguageID")
        UCIP_Response_Data.dedicatedAccountInformation = Values(UCIP_Response, "dedicatedAccountInformation")
        UCIP_Response_Data.fafChangeUnbarDate = Values(UCIP_Response, "fafChangeUnbarDate")
        UCIP_Response_Data.fafInformation = Values(UCIP_Response, "fafInformation")
        UCIP_Response_Data.firstIVRCallFlag = Values(UCIP_Response, "firstIVRCallFlag")
        UCIP_Response_Data.masterSubscriberFlag = Values(UCIP_Response, "masterSubscriberFlag")
        UCIP_Response_Data.maxAllowedNumbers = Values(UCIP_Response, "maxAllowedNumbers")
        UCIP_Response_Data.notAllowedReason = Values(UCIP_Response, "notAllowedReason")
        UCIP_Response_Data.pinCode = Values(UCIP_Response, "pinCode")
        UCIP_Response_Data.promotionAnnouncementCode = Values(UCIP_Response, "promotionAnnouncementCode")
        UCIP_Response_Data.rechargeAmount1MainTotal = Values(UCIP_Response, "rechargeAmount1MainTotal")
        UCIP_Response_Data.rechargeAmount1PromoTotal = Values(UCIP_Response, "rechargeAmount1PromoTotal")
        UCIP_Response_Data.rechargeAmount2MainTotal = Values(UCIP_Response, "rechargeAmount2MainTotal")
        UCIP_Response_Data.rechargeAmount2PromoTotal = Values(UCIP_Response, "rechargeAmount2PromoTotal")
        UCIP_Response_Data.refillBarredFlag = Values(UCIP_Response, "refillBarredFlag")
        UCIP_Response_Data.refillFraudCount = Values(UCIP_Response, "refillFraudCount")
        UCIP_Response_Data.removalPeriod = Values(UCIP_Response, "removalPeriod")
        UCIP_Response_Data.ResponseCode = Values(UCIP_Response, "responseCode")
        UCIP_Response_Data.serviceClassChangeUnbarDate = Values(UCIP_Response, "serviceClassChangeUnbarDate")
        UCIP_Response_Data.serviceClassCurrent = Values(UCIP_Response, "serviceClassCurrent")
        UCIP_Response_Data.serviceFeeDate = Values(UCIP_Response, "serviceFeeDate")
        UCIP_Response_Data.serviceFeeDateAfter = Values(UCIP_Response, "serviceFeeDateAfter")
        UCIP_Response_Data.serviceFeeDaysPromoExt = Values(UCIP_Response, "serviceFeeDaysPromoExt")
        UCIP_Response_Data.serviceFeeDaysTotalExt = Values(UCIP_Response, "serviceFeeDaysTotalExt")
        UCIP_Response_Data.serviceRemovalDate = Values(UCIP_Response, "serviceRemovalDate")
        UCIP_Response_Data.supervisionDate = Values(UCIP_Response, "supervisionDate")
        UCIP_Response_Data.supervisionDateAfter = Values(UCIP_Response, "supervisionDateAfter")
        UCIP_Response_Data.supervisionDaysPromoExt = Values(UCIP_Response, "supervisionDaysPromoExt")
        UCIP_Response_Data.supervisionDaysTotalExt = Values(UCIP_Response, "supervisionDaysTotalExt")
        UCIP_Response_Data.temporaryBlockedFlag = Values(UCIP_Response, "temporaryBlockedFlag")
        UCIP_Response_Data.transactionAmount1Refill = Values(UCIP_Response, "transactionAmount1Refill")
        UCIP_Response_Data.transactionAmount2Refill = Values(UCIP_Response, "transactionAmount2Refill")
        UCIP_Response_Data.transactionServiceClass = Values(UCIP_Response, "transactionServiceClass")
        UCIP_Response_Data.transactionVVPeriodExt = Values(UCIP_Response, "transactionVVPeriodExt")
        UCIP_Response_Data.valueVoucherDate = Values(UCIP_Response, "valueVoucherDate")
        UCIP_Response_Data.valueVoucherDateAfter = Values(UCIP_Response, "valueVoucherDateAfter")
        UCIP_Response_Data.creditClearancePeriod = Values(UCIP_Response, "creditClearancePeriod")
        UCIP_Response_Data.removalPeriod = Values(UCIP_Response, "removalPeriod")
        UCIP_Response_Data.serviceFeePeriod = Values(UCIP_Response, "serviceFeePeriod")
        UCIP_Response_Data.supervisionPeriod = Values(UCIP_Response, "supervisionPeriod")
        
        For I = 1 To NumberOfDedicatedAccounts
            UCIP_Response_Data.DedicatedAccount(I).DedicatedAccountValue = Values(UCIP_Response, "dedicatedAccountID " + Trim(Str(I)) + " dedicatedAccountValue1")
            UCIP_Response_Data.DedicatedAccount(I).DedicateAccountExpiryDate = Values(UCIP_Response, "dedicatedAccountID " + Trim(Str(I)) + " dedicatedAccountValue1 " + UCIP_Response_Data.DedicatedAccount(I).DedicatedAccountValue + " expiryDate")
        Next I
End Sub
Private Function Values(ByRef Long_Text As String, ByRef Value As String)
        Dim Param_Start, Space_Loc As Integer
        
        Param_Start = InStr(1, Long_Text, Value)
        Space_Loc = InStr(Param_Start + Len(Value) + 1, Long_Text, " ")
        If Param_Start = 0 Then
            Values = ""
        Else
            If Space_Loc < (Param_Start + Len(Value) + 1) Then
                Values = Mid(Long_Text, Param_Start + Len(Value) + 1, Len(Long_Text) - Len(Value) - 1)
                Else
                Values = Mid(Long_Text, Param_Start + Len(Value) + 1, Space_Loc - (Param_Start + Len(Value) + 1))
            End If
        End If
End Function
