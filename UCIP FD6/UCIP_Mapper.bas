Attribute VB_Name = "UCIP_Mapper"
Public Function AccumulatorEnquiryT_Command(subscriberNumber As String, _
                                        Optional subscriberNumberNAI As String, _
                                        Optional messageCapabilityFlag As String)
    With UCIP
        .UCIP_Combo.Text = "AccumulatorEnquiryT"
        Call .UCIP_Combo_Change
        .Combo_MSISDNs = subscriberNumber
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If messageCapabilityFlag <> "" Then
            .Check_Optional(1).Value = 1
            .Text_Optional(1) = messageCapabilityFlag
        End If
        .Generate_Click
        .Send_Command_Click
    End With
    AccumulatorEnquiryT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Function AdjustmentT_Command(subscriberNumber As String, _
                                        adjustmentAmount As String, _
                                        Optional Adj_Amount_In_LE As Boolean, _
                                        Optional relativeDateAdjustmentSupervision As String, _
                                        Optional relativeDateAdjustmentServiceFee As String, _
                                        Optional subscriberNumberNAI As String, _
                                        Optional dedicatedAccountInformation As String, _
                                        Optional externalData1 As String, _
                                        Optional externalData2 As String)
'    UCIP.Show
    With UCIP
        .UCIP_Combo.Text = "AdjustmentT"
        Call .UCIP_Combo_Change
        .Combo_MSISDNs = subscriberNumber
        .Text_Mandatory(1) = "EGP"
        .Text_Mandatory(2) = adjustmentAmount
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        'Hard Coding messageCapabilityFlag=0 since it failed when checked
        .Text_Optional(1) = messageCapabilityFlag
        .Check_Optional(1).Value = 0
        'If messageCapabilityFlag <> "" Then
        '    .Check_Optional(1).Value = 1
        '    .Text_Optional(1) = messageCapabilityFlag
        'End If
        If dedicatedAccountInformation <> "" Then
            .Check_Optional(2).Value = 1
            .Text_Optional(2) = dedicatedAccountInformation
        End If
        If externalData1 <> "" Then
            .Check_Optional(3).Value = 1
            .Text_Optional(3) = externalData1
        End If
        If externalData2 <> "" Then
            .Check_Optional(4).Value = 1
            .Text_Optional(4) = externalData2
        End If
        If relativeDateAdjustmentSupervision <> "" And relativeDateAdjustmentSupervision <> "0" Then
            .Check_Optional(5).Value = 1
            .Text_Optional(5) = relativeDateAdjustmentSupervision
            '.Check_Optional(6).Value = 1
            '.Text_Optional(6) = "0"
        End If
        If relativeDateAdjustmentServiceFee <> "" And relativeDateAdjustmentServiceFee <> "0" Then
            .Check_Optional(6).Value = 1
            .Text_Optional(6) = relativeDateAdjustmentServiceFee
            '.Check_Optional(5).Value = 1
            'If .Text_Optional(5) = "" Then .Text_Optional(5) = "0"
        End If
        If Adj_Amount_In_LE Then
            In_LE.LE.Value = True
            Else
            In_LE.LE.Value = False
        End If
        .Generate_Click
        .Send_Command_Click
    End With
    AdjustmentT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Function BalanceEnquiryT_Command(subscriberNumber As String, _
                                        Optional subscriberNumberNAI As String, _
                                        Optional messageCapabilityFlag As String, _
                                        Optional chargingRequestInformation As String)
    With UCIP
        .UCIP_Combo.Text = "BalanceEnquiryT"
        .Combo_MSISDNs = subscriberNumber
        
        Call .UCIP_Combo_Change

        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If messageCapabilityFlag <> "" Then
            .Check_Optional(1).Value = 1
            .Text_Optional(1) = messageCapabilityFlag
        End If
        If requestedInformation <> "" Then
            .Check_Optional(2).Value = 1
            .Text_Optional(2) = chargingRequestInformation
        End If
        
        .Generate_Click
        .Send_Command_Click
    End With
    BalanceEnquiryT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Function GetAccountDetailsT_Command(subscriberNumber As String, _
                                        Optional subscriberNumberNAI As String, _
                                        Optional messageCapabilityFlag As String, _
                                        Optional requestedInformation As String)
    With UCIP
        .UCIP_Combo.Text = "GetAccountDetailsT"
        .Combo_MSISDNs = subscriberNumber
        
        Call .UCIP_Combo_Change
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If messageCapabilityFlag <> "" Then
            .Check_Optional(1).Value = 1
            .Text_Optional(1) = messageCapabilityFlag
        End If
        If requestedInformation <> "" Then
            .Check_Optional(2).Value = 1
            .Text_Optional(2) = requestedInformation
        End If
        
        .Generate_Click
        .Send_Command_Click
    End With
    GetAccountDetailsT_Command = UCIP_Response_Data.ResponseCode
End Function

Public Sub GetFaFListT_Command()
End Sub
Public Sub GetVoucherRefillOptionsT_Command()
End Sub
Public Function RefillT_Command(subscriberNumber As String, _
                                TransactionAmountRefill As String, _
                                paymentProfileID As String, _
                                Optional Adj_Amount_In_LE As Boolean, _
                                Optional subscriberNumberNAI As String, _
                                Optional externalData1 As String, _
                                Optional externalData2 As String)
        With UCIP
        .UCIP_Combo.Text = "RefillT"
        .Combo_MSISDNs = subscriberNumber
        Call .UCIP_Combo_Change
        .Text_Mandatory(1) = TransactionAmountRefill
        .Text_Mandatory(2) = "EGP"
        .Text_Mandatory(3) = paymentProfileID
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If externalData1 <> "" Then
            .Check_Optional(1).Value = 1
            .Text_Optional(1) = externalData1
        End If
        If externalData2 <> "" Then
            .Check_Optional(2).Value = 1
            .Text_Optional(2) = externalData2
        End If
        If Adj_Amount_In_LE Then
            In_LE.LE.Value = True
            Else
            In_LE.LE.Value = False
        End If
        .Generate_Click
        .Send_Command_Click
    End With
    'SulSAT.Load
    RefillT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Function StandardVoucherRefillT_Command(subscriberNumber As String, _
                                                activationNumber As String, _
                                                Optional subscriberNumberNAI As String, _
                                                Optional messageCapabilityFlag As String, _
                                                Optional externalData1 As String, _
                                                Optional externalData2 As String, _
                                                Optional locationNumber As String, _
                                                Optional locationNumberNAI As String, _
                                                Optional selectedOption As String)
        With UCIP
        .UCIP_Combo.Text = "StandardVoucherRefillT"
        .Combo_MSISDNs = subscriberNumber
        Call .UCIP_Combo_Change
        .Text_Mandatory(1) = activationNumber
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If messageCapabilityFlag <> "" Then
            .Check_Optional(1).Value = 1
            .Text_Optional(1) = messageCapabilityFlag
        End If
        If externalData1 <> "" Then
            .Check_Optional(2).Value = 1
            .Text_Optional(2) = externalData1
        End If
        If externalData2 <> "" Then
            .Check_Optional(3).Value = 1
            .Text_Optional(3) = externalData2
        End If
        If locationNumber <> "" Then
            .Check_Optional(4).Value = 1
            .Text_Optional(4) = locationNumber
        End If
        If locationNumberNAI <> "" Then
            .Check_Optional(5).Value = 1
            .Text_Optional(5) = locationNumberNAI
        End If
        If selectedOption <> "" Then
            .Check_Optional(6).Value = 1
            .Text_Optional(6) = selectedOption
        End If
        .Generate_Click
        .Send_Command_Click
    End With
    StandardVoucherRefillT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Function UpdateAccountDetailsT_Command(subscriberNumber As String, _
                                            currentLanguageID As String, _
                                            newLanguageID As String, _
                                            Optional subscriberNumberNAI As String, _
                                            Optional firstIVRCallDone As String, _
                                            Optional externalData1 As String, _
                                            Optional externalData2 As String, _
                                            Optional pinCode As String)
    With UCIP
        .UCIP_Combo.Text = "UpdateAccountDetailsT"
        Call .UCIP_Combo_Change
        .Combo_MSISDNs = subscriberNumber
        .Text_Optional(1) = currentLanguageID
        .Check_Optional(1).Value = 1
        .Text_Optional(2) = newLanguageID
        .Check_Optional(2).Value = 1
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If firstIVRCallDone <> "" Then
            .Check_Optional(3).Value = 1
            .Text_Optional(3) = firstIVRCallDone
        End If
        If externalData1 <> "" Then
            .Check_Optional(4).Value = 1
            .Text_Optional(4) = externalData1
        End If
        If externalData2 <> "" Then
            .Check_Optional(5).Value = 1
            .Text_Optional(5) = externalData2
        End If
        If pinCode <> "" Then
            .Check_Optional(6).Value = 1
            .Text_Optional(6) = pinCode
        End If
        .Generate_Click
        .Send_Command_Click
    End With
    UpdateAccountDetailsT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Sub UpdateFaFListT_Command()
End Sub
Public Function UpdateServiceClassT_Command(subscriberNumber As String, _
                                        serviceClassCurrent As String, _
                                        serviceClassNew As String, _
                                        Optional subscriberNumberNAI As String, _
                                        Optional chargingRequestInformation As String, _
                                        Optional performValidation As String)
    With UCIP
        .UCIP_Combo.Text = "UpdateServiceClassT"
        Call .UCIP_Combo_Change
        .Combo_MSISDNs = subscriberNumber
        .Text_Mandatory(1) = serviceClassCurrent
        .Text_Mandatory(2) = serviceClassNew
        If subscriberNumberNAI <> "" Then
            .Check_Optional(0).Value = 1
            .Text_Optional(0) = subscriberNumberNAI
        End If
        If chargingRequestInformation <> "" Then
            .Check_Optional(1).Value = 1
            .Text_Optional(1) = chargingRequestInformation
        End If
        If performValidation <> "" Then
            .Check_Optional(2).Value = 1
            .Text_Optional(2) = performValidation
        End If
        .Generate_Click
        .Send_Command_Click
    End With
    UpdateServiceClassT_Command = UCIP_Response_Data.ResponseCode
End Function
Public Sub ValueVoucherEnquiryT_Command()

End Sub

'Public Sub ValueVoucherRefillT_Command()
'            Select Case Sultan

'
'
'
'        Case "GetFaFListT"
'            Call Show_Checks("Optional", 1)
'            Check_Optional(0).Caption = "subscriberNumberNAI"
'            Text_Optional(0) = "2"
'            Check_Optional(0).Value = 0
'            Call Show_Checks("mandatory", 2)
'            Check_Mandatory(1).Caption = "requestedOwner"
'            Text_Mandatory(1) = "1"
'
'        Case "GetVoucherRefillOptionsT"
'
'
'

'
'
'
'        Case "UpdateFaFListT"
'            Call Show_Checks("Optional", 3)
'            Check_Optional(0).Caption = "subscriberNumberNAI"
'            Text_Optional(0) = "2"
'            Check_Optional(0).Value = 0
'            Check_Optional(1).Caption = "chargingRequestInformation"
'            Check_Optional(2).Caption = "selectedOption"
'            With Extention
'                .Visible = True
'                .Check_Optional(0).Caption = "fafNumber"
'                .Text_Optional_EXT(0) = "XXXXXXXXX"
'                .Check_Optional(1).Caption = "fafIndicator"
'                .Text_Optional_EXT(1) = "400"
'                .Check_Optional(2).Caption = "owner"
'                .Text_Optional_EXT(2) = "1"
'                .Check_Optional(3).Visible = False
'                .Text_Optional_EXT(3).Visible = False
'            End With
'            Call Show_Checks("Mandatory", 4)
'            Check_Mandatory(1).Caption = "fafAction"
'            Check_Mandatory(2).Caption = "fafInformation"
'            Check_Mandatory(3).Caption = "requestedOwner"
'            Text_Mandatory(3) = "1"
'
'
'
'        Case "ValueVoucherEnquiryT"
'        Case "ValueVoucherRefillT"
'    End Select
        
'    Call Show_Checks("mandatory", 1)
'    Check_Mandatory(0).Caption = "subscriberNumber"
'End Sub
'
