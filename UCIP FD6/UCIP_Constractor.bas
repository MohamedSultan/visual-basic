Attribute VB_Name = "UCIP_Constractor"
Public Type messageCapabilityFlag_Type
    promotionNotificationFlag As Boolean
    firstIVRCallSetFlag As Boolean
    accountActivationFlag As Boolean
End Type
Type requestedInformationFlags_Type
    requestMasterAccountBalanceFlag As Boolean
    allowedServiceClassChangeDateFlag As Boolean
End Type
Type chargingRequestInformation_Type
    chargingType As Boolean
    chargingIndicator As Boolean
    reservationCorrelationID As Boolean
End Type
Type dedicatedAccountUpdateInformation_Type
    dedicatedAccountID As Integer
    adjustmentAmountRelative As String
    dedicatedAccountValueNew As String
    adjustmentDateRelative As Integer
    expiryDate As String
End Type
Private Function UCIP_Close()
    UCIP_Close = "</struct>" + vbCrLf + _
                        "</value>" + vbCrLf + _
                    "</param>" + vbCrLf + _
                "</params>" + vbCrLf + _
            "</methodCall>" + vbCrLf
End Function
Public Function GetAccountDetails(subscriberNumber As String, _
                                messageCapabilityFlag As messageCapabilityFlag_Type, _
                                requestedInformationFlags As requestedInformationFlags_Type, _
                                Optional subscriberNumberNAI As String)
    
    subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    Dim messageCapabilityFlag_S, requestedInformationFlags_S As String
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    requestedInformationFlags_S = requestedInformationFlags_Builder(requestedInformationFlags)
    
    GetAccountDetails = Trim_All(Member_Builder("GetAccountDetails") + _
                                    subscriberNumber + _
                                    subscriberNumberNAI + _
                                    messageCapabilityFlag_S + _
                                    requestedInformationFlags_S + _
                                    UCIP_Close, True)
    
End Function
Public Function GetAccumulators(subscriberNumber As String, _
                                messageCapabilityFlag As messageCapabilityFlag_Type, _
                                chargingRequestInformation As chargingRequestInformation_Type, _
                                Optional subscriberNumberNAI As String)

                                 
    subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    
    Dim messageCapabilityFlag_S, chargingRequestInformation_S As String
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    chargingRequestInformation_S = chargingRequestInformation_Builder(chargingRequestInformation)
    
    GetAccumulators = Trim_All(Member_Builder("GetAccumulators") + _
                                        subscriberNumber + _
                                        subscriberNumberNAI + _
                                        messageCapabilityFlag_S + _
                                        chargingRequestInformation_S + _
                                        UCIP_Close, True)

End Function
Public Function GetAllowedServiceClasses()
Public Function GetBalanceAndDate(subscriberNumber As String, _
                                messageCapabilityFlag As messageCapabilityFlag_Type, _
                                chargingRequestInformation As chargingRequestInformation_Type, _
                                Optional subscriberNumberNAI As String)

                                 
    subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    
    Dim messageCapabilityFlag_S, chargingRequestInformation_S As String
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    chargingRequestInformation_S = chargingRequestInformation_Builder(chargingRequestInformation)
    
    GetBalanceAndDate = Trim_All(Member_Builder("GetBalanceAndDate") + _
                                        subscriberNumber + _
                                        subscriberNumberNAI + _
                                        messageCapabilityFlag_S + _
                                        chargingRequestInformation_S + _
                                        UCIP_Close, True)

End Function

Public Function GetFaFList(subscriberNumber As String, _
                            requestedOwner As Integer, _
                            Optional subscriberNumberNAI As String)
    subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    Dim requestedOwner_S As String
    requestedOwner_S = Member_Builder("requestedOwner", "int", Str(requestedOwner))
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    
    GetFaFList = Trim_All(Member_Builder("GetFaFList") + _
                                        subscriberNumber + _
                                        subscriberNumberNAI + _
                                        requestedOwner_S + _
                                        UCIP_Close, True)
End Function
Private Function chargingRequestInformation_Builder(chargingRequestInformation As chargingRequestInformation_Type) As String
        With chargingRequestInformation
        If .chargingIndicator Or _
            .chargingType Or _
            .reservationCorrelationID Then
            
            chargingRequestInformation_Builder = "<struct>" + vbCrLf + _
                                            Member_Builder("chargingIndicator", "Boolean", Boolean2String(.chargingIndicator)) + _
                                            Member_Builder("chargingType", "Boolean", Boolean2String(.chargingType)) + _
                                            Member_Builder("reservationCorrelationID", "Boolean", Boolean2String(.reservationCorrelationID)) + _
                                            "</struct>" + vbCrLf
        End If
    End With
End Function
Private Function requestedInformationFlags_Builder(requestedInformationFlags As requestedInformationFlags_Type) As String
        With requestedInformationFlags
        If .allowedServiceClassChangeDateFlag Or _
            .requestMasterAccountBalanceFlag Then
            
            requestedInformationFlags_Builder = "<struct>" + vbCrLf + _
                                            Member_Builder("allowedServiceClassChangeDateFlag", "Boolean", Boolean2String(.allowedServiceClassChangeDateFlag)) + _
                                            Member_Builder("requestMasterAccountBalanceFlag", "Boolean", Boolean2String(.requestMasterAccountBalanceFlag)) + _
                                            "</struct>" + vbCrLf
        End If
    End With
End Function
Private Function messageCapabilityFlag_Builder(messageCapabilityFlag As messageCapabilityFlag_Type) As String
        With messageCapabilityFlag
        If .promotionNotificationFlag Or _
            .firstIVRCallSetFlag Or _
            .accountActivationFlag Then
            
            messageCapabilityFlag_Builder = "<struct>" + vbCrLf + _
                                            Member_Builder("promotionNotificationFlag", "Boolean", Boolean2String(.promotionNotificationFlag)) + _
                                            Member_Builder("firstIVRCallSetFlag", "Boolean", Boolean2String(.firstIVRCallSetFlag)) + _
                                            Member_Builder("accountActivationFlag", "Boolean", Boolean2String(.accountActivationFlag)) + _
                                            "</struct>" + vbCrLf
        End If
    End With
End Function
Private Function dedicatedAccountUpdateInformation_Builder(dedicatedAccountUpdateInformation As dedicatedAccountUpdateInformation_Type) As String
        With dedicatedAccountUpdateInformation
        If .adjustmentAmountRelative <> 0 Or .adjustmentDateRelative <> 0 Or _
            .dedicatedAccountValueNew <> "" Or .expiryDate <> "" Then
            
            dedicatedAccountUpdateInformation_Builder = "<struct>" + vbCrLf + _
                                            Member_Builder("dedicatedAccountID", "int", .dedicatedAccountID) + _
                                            Member_Builder("adjustmentAmountRelative", "int", .adjustmentAmountRelative) + _
                                            Member_Builder("dedicatedAccountValueNew", "string", .dedicatedAccountValueNew) + _
                                            Member_Builder("adjustmentDateRelative", "int", .adjustmentDateRelative) + _
                                            "</struct>" + vbCrLf
        End If
    End With
End Function
Public Function GetRefillOptions(subscriberNumber As String, _
                                messageCapabilityFlag As messageCapabilityFlag_Type, _
                                voucherActivationCode As String, _
                                Optional serviceClassCurrent As String, _
                                Optional subscriberNumberNAI As String)

                                 
    subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    voucherActivationCode = Member_Builder("voucherActivationCode", "String", voucherActivationCode)
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    If serviceClassCurrent <> 0 Then serviceClassCurrent = Member_Builder("serviceClassCurrent", "int", serviceClassCurrent)
    
    
    Dim messageCapabilityFlag_S, chargingRequestInformation_S As String
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    
    GetRefillOptions = Trim_All(Member_Builder("GetRefillOptions") + _
                                        subscriberNumber + _
                                        subscriberNumberNAI + _
                                        messageCapabilityFlag_S + _
                                        chargingRequestInformation_S + _
                                        UCIP_Close, True)

End Function
Public Function Refill(subscriberNumber As String, _
                            transactionAmount As String, _
                            transactionCurrency As String, _
                            refillProfileID As String, _
                            voucherActivationCode As String, _
                            subscriberNumberNAI As String, _
                            messageCapabilityFlag As messageCapabilityFlag_Type, _
                            Optional originOperatorID As String, _
                            Optional externalData1 As String, _
                            Optional externalData2 As String, _
                            Optional transactionType As String, _
                            Optional transactionCode As String, _
                            Optional ByVal requestRefillAccountBeforeFlag As Boolean, _
                            Optional ByVal requestRefillAccountAfterFlag As Boolean, _
                            Optional ByVal requestRefillDetailsFlag As Boolean, _
                            Optional selectedOption As String, _
                            Optional locationNumber As String, _
                            Optional locationNumberNAI As String)

    Dim messageCapabilityFlag_S As String
            
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    If subscriberNumber <> "" Then subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    If transactionAmount <> "" Then transactionAmount = Member_Builder("transactionAmount", "String", transactionAmount)
    If transactionCurrency <> "" Then transactionCurrency = Member_Builder("transactionCurrency", "String", transactionCurrency)
    If refillProfileID <> "" Then refillProfileID = Member_Builder("refillProfileID", "String", refillProfileID)
    If voucherActivationCode <> "" Then voucherActivationCode = Member_Builder("voucherActivationCode", "String", voucherActivationCode)
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    If originOperatorID <> "" Then originOperatorID = Member_Builder("originOperatorID", "String", originOperatorID)
    If externalData1 <> "" Then externalData1 = Member_Builder("externalData1", "String", externalData1)
    If externalData2 <> "" Then externalData2 = Member_Builder("externalData2", "String", externalData2)
    If transactionType <> "" Then transactionType = Member_Builder("transactionType", "String", transactionType)
    If transactionCode <> "" Then transactionCode = Member_Builder("transactionCode", "String", transactionCode)
    If requestRefillAccountBeforeFlag Then requestRefillAccountBeforeFlag_S = Member_Builder("requestRefillAccountBeforeFlag", "Boolean", Boolean2String(requestRefillAccountBeforeFlag))
    If requestRefillAccountAfterFlag Then requestRefillAccountAfterFlag_S = Member_Builder("requestRefillAccountAfterFlag", "Boolean", Boolean2String(requestRefillAccountAfterFlag))
    If requestRefillDetailsFlag Then requestRefillDetailsFlag_S = Member_Builder("requestRefillDetailsFlag", "Boolean", Boolean2String(requestRefillDetailsFlag))
    If selectedOption <> "" Then selectedOption = Member_Builder("selectedOption", "int", selectedOption)
    If locationNumber <> "" Then locationNumber = Member_Builder("locationNumber", "String", locationNumber)
    If locationNumberNAI <> "" Then locationNumberNAI = Member_Builder("locationNumberNAI", "String", locationNumberNAI)

    Refill = Trim_All(Member_Builder("Refill") + _
                        messageCapabilityFlag_S + _
                        subscriberNumber + _
                        transactionAmount + _
                        transactionCurrency + _
                        refillProfileID + _
                        voucherActivationCode + _
                        subscriberNumberNAI + _
                        originOperatorID + _
                        externalData1 + _
                        externalData2 + _
                        transactionType + _
                        transactionCode + _
                        requestRefillAccountBeforeFlag_S + _
                        requestRefillAccountAfterFlag_S + _
                        requestRefillDetailsFlag_S + _
                        selectedOption + _
                        locationNumber + _
                        locationNumberNAI + _
                        UCIP_Close, True)

End Function
Public Function UpdateAccountDetails(subscriberNumber As String, _
                                        messageCapabilityFlag As messageCapabilityFlag_Type, _
                                        Optional subscriberNumberNAI As String, _
                                        Optional originOperatorID As String, _
                                        Optional languageIDNew As String, _
                                        Optional ByVal firstIVRCallDoneFlag As Boolean, _
                                        Optional externalData1 As String, _
                                        Optional externalData2 As String, _
                                        Optional accountHomeRegion As String, _
                                        Optional pinCodeOriginal As String, _
                                        Optional ByVal pinCodeValidationFlag As Boolean, _
                                        Optional pinCode As String)
    Dim messageCapabilityFlag_S, firstIVRCallDoneFlag_S, pinCodeValidationFlag_S As String
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    If originOperatorID <> "" Then originOperatorID = Member_Builder("originOperatorID", "String", originOperatorID)
    If languageIDNew <> "" Then languageIDNew = Member_Builder("languageIDNew", "int", languageIDNew)
    If firstIVRCallDoneFlag Then firstIVRCallDoneFlag = Member_Builder("firstIVRCallDoneFlag", "boolean", Boolean2String(firstIVRCallDoneFlag))
    If externalData1 <> "" Then externalData1 = Member_Builder("externalData1", "String", externalData1)
    If externalData2 <> "" Then externalData2 = Member_Builder("externalData2", "String", externalData2)
    If accountHomeRegion <> "" Then accountHomeRegion = Member_Builder("accountHomeRegion", "int", accountHomeRegion)
    If pinCodeOriginal <> "" Then pinCodeOriginal = Member_Builder("pinCodeOriginal", "String", pinCodeOriginal)
    If pinCodeValidationFlag Then pinCodeValidationFlag_S = Member_Builder("pinCodeValidationFlag", "boolean", Boolean2String(pinCodeValidationFlag))
    If pinCode <> "" Then pinCode = Member_Builder("pinCode", "String", pinCode)

UpdateAccountDetails = Trim_All(Member_Builder("Refill") + _
                        subscriberNumberNAI + _
                        subscriberNumber + _
                        originOperatorID + _
                        messageCapabilityFlag_S + _
                        languageIDNew + _
                        firstIVRCallDoneFlag_S + _
                        externalData1 + _
                        externalData2 + _
                        accountHomeRegion + _
                        pinCodeOriginal + _
                        pinCodeValidationFlag_S + _
                        pinCode + _
                        UCIP_Close, True)

End Function
Public Function UpdateBalanceAndDate(subscriberNumber As String, _
                            adjustmentAmountRelative As String, _
                            transactionCurrency As String, _
                            messageCapabilityFlag As messageCapabilityFlag_Type, _
                            dedicatedAccountInformation As dedicatedAccountUpdateInformation_Type, _
                            Optional subscriberNumberNAI As String, _
                            Optional originOperatorID As String, _
                            Optional externalData1 As String, _
                            Optional externalData2 As String, _
                            Optional transactionType As String, _
                            Optional transactionCode As String, _
                            Optional adjustmentAmountRelative As String, _
                            Optional supervisionExpiryDateRelative As Integer, _
                            Optional supervisionExpiryDate As String, _
                            Optional serviceFeeExpiryDateRelative As Integer, _
                            Optional serviceFeeExpiryDate As String, _
                            Optional creditClearancePeriod As Integer, _
                            Optional serviceRemovalPeriod As Integer)

    Dim messageCapabilityFlag_S, dedicatedAccountInformation_S As String
            
    messageCapabilityFlag_S = messageCapabilityFlag_Builder(messageCapabilityFlag)
    dedicatedAccountInformation_S = dedicatedAccountUpdateInformation_Builder(dedicatedAccountInformation)
    
    If subscriberNumber <> "" Then subscriberNumber = Member_Builder("subscriberNumber", "String", subscriberNumber)
    If adjustmentAmountRelative <> "" Then transactionAmount = Member_Builder("adjustmentAmountRelative", "String", transactionAmount)
    If transactionCurrency <> "" Then transactionCurrency = Member_Builder("transactionCurrency", "String", transactionCurrency)
    If subscriberNumberNAI <> "" Then subscriberNumberNAI = Member_Builder("subscriberNumberNAI", "String", subscriberNumberNAI)
    If originOperatorID <> "" Then originOperatorID = Member_Builder("originOperatorID", "String", originOperatorID)
    If externalData1 <> "" Then externalData1 = Member_Builder("externalData1", "String", externalData1)
    If externalData2 <> "" Then externalData2 = Member_Builder("externalData2", "String", externalData2)
    If transactionType <> "" Then transactionType = Member_Builder("transactionType", "String", transactionType)
    If transactionCode <> "" Then transactionCode = Member_Builder("transactionCode", "String", transactionCode)
    If adjustmentAmountRelative <> "" Then transactionType = Member_Builder("transactionType", "String", transactionType)
    If supervisionExpiryDateRelative Then supervisionExpiryDateRelative = Member_Builder("supervisionExpiryDateRelative", "int", supervisionExpiryDateRelative)
    If supervisionExpiryDate <> "" Then supervisionExpiryDate = Member_Builder("supervisionExpiryDate", "String", supervisionExpiryDate)
    If serviceFeeExpiryDateRelative Then serviceFeeExpiryDateRelative = Member_Builder("serviceFeeExpiryDateRelative", "int", serviceFeeExpiryDateRelative)
    If serviceFeeExpiryDate <> "" Then serviceFeeExpiryDate = Member_Builder("serviceFeeExpiryDate", "string", serviceFeeExpiryDate)
    If creditClearancePeriod Then creditClearancePeriod = Member_Builder("creditClearancePeriod", "int", creditClearancePeriod)
    If serviceRemovalPeriod Then serviceRemovalPeriod = Member_Builder("serviceRemovalPeriod", "int", serviceRemovalPeriod)
   
    UpdateBalanceAndDate = Trim_All(Member_Builder("UpdateBalanceAndDate") + _
                        subscriberNumber + _
                        transactionAmount + _
                        transactionCurrency + _
                        subscriberNumberNAI + _
                        originOperatorID + _
                        messageCapabilityFlag_S + _
                        externalData1 + _
                        externalData2 + _
                        transactionType + _
                        transactionCode + _
                        transactionType + _
                        supervisionExpiryDate + _
                        serviceFeeExpiryDate + _
                        dedicatedAccountInformation_S + _
                        supervisionExpiryDateRelative + _
                        serviceFeeExpiryDateRelative + _
                        creditClearancePeriod + _
                        serviceRemovalPeriod + _
                        UCIP_Close, True)

End Function

Public Function UpdateCommunityList()
Public Function UpdateFaFList()
Public Function UpdateServiceClass()
Public Function UpdateSubscriberSegmentation()



Public Function Member_Builder(Member_Name As String, _
                                Optional Member_Type As String, _
                                Optional Member_Value As String, _
                                Optional Struct As Boolean)
    
    Dim IsMethod As Boolean
    For I = 0 To UCIP_FD6.UCIP_Combo.ListCount - 1
        If Member_Name = UCIP_FD6.UCIP_Combo.List(I) Then IsMethod = True
    Next I
    
    If IsMethod Then
        Member_Builder = Trim_All("<?xml version=""1.0""?> " + vbCrLf + _
                            "<methodCall>" + vbCrLf + _
                                "<methodName>" + Member_Name + "</methodName>" + _
                                    "<params>" + vbCrLf + _
                                        "<param>" + vbCrLf + _
                                            "<value>" + vbCrLf + _
                                                 "<struct>" + vbCrLf + _
                                                     Member_Builder("originNodeType", "String", "EXT") + _
                                                     Member_Builder("originHostName", "String", "TestClient") + _
                                                     Member_Builder("originTransactionID", "String", "1136807058750") + _
                                                     Member_Builder("originTimeStamp", "dateTime.iso8601", TimeStamp), True)
    Else
        Member_Type = LCase(Member_Type)
        Member_Builder = Trim_All("<member>" + vbCrLf + _
                                    "<name>" + Member_Name + "</name>" + vbCrLf + _
                                        "<value>" + vbCrLf + _
                                            "<" + Member_Type + ">" + Member_Value + "</" + Member_Type + ">" + vbCrLf + _
                                        "</value>" + vbCrLf + _
                                "</member>" + vbCrLf, True)
    End If
End Function
Private Function Boolean2String(Var As Boolean) As String
    Boolean2String = "false"
    If Var Then Boolean2String = "true"
End Function
