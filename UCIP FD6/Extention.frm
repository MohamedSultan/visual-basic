VERSION 5.00
Begin VB.Form Extention 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   2160
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   5145
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2160
   ScaleWidth      =   5145
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Previous 
      Caption         =   "Previous"
      Height          =   375
      Left            =   600
      TabIndex        =   10
      Top             =   1680
      Width           =   975
   End
   Begin VB.CommandButton Add_New 
      Caption         =   "Add More"
      Height          =   375
      Left            =   3120
      TabIndex        =   9
      Top             =   1680
      Width           =   1215
   End
   Begin VB.CommandButton Apply 
      Caption         =   "Ok"
      Height          =   375
      Left            =   1560
      TabIndex        =   8
      Top             =   1680
      Width           =   1575
   End
   Begin VB.CheckBox Check_Optional 
      Caption         =   "Check1"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   2895
   End
   Begin VB.CheckBox Check_Optional 
      Caption         =   "Check1"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   6
      Top             =   840
      Width           =   2895
   End
   Begin VB.CheckBox Check_Optional 
      Caption         =   "Check1"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   2895
   End
   Begin VB.CheckBox Check_Optional 
      Caption         =   "Check1"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   2895
   End
   Begin VB.TextBox Text_Optional_EXT 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "d/M/yy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   3
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   3120
      TabIndex        =   3
      Text            =   "Text1"
      Top             =   1200
      Width           =   1935
   End
   Begin VB.TextBox Text_Optional_EXT 
      Height          =   285
      Index           =   2
      Left            =   3120
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   840
      Width           =   1935
   End
   Begin VB.TextBox Text_Optional_EXT 
      Height          =   285
      Index           =   1
      Left            =   3120
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   480
      Width           =   1935
   End
   Begin VB.TextBox Text_Optional_EXT 
      Height          =   285
      Index           =   0
      Left            =   3120
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "Extention"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim DedicatedAccountInfo As String
Dim Pointer As Integer
Dim Dedicateds1(4, 3) As String 'dedicateds(number of dedicated accounts(5),number of fields(4))
Dim Values(4) As String

Private Sub Add_New_Click()
    
    Dim Proceed As Boolean
    Proceed = True
    If Pointer = 6 Then
        Add_New.Enabled = False
    Else
        'Pointer = Pointer + 1
        
        For i = 0 To 1
            If Text_Optional_EXT(i) = "" Then
                MsgBox "You must fill the field number " + Str(i + 1), , "Missing Madatory Field"
                Proceed = False
            End If
        Next i
        For i = 0 To 3
            If Check_Optional(i).Value = 1 Then
                Values(Pointer) = Values(Pointer) + Filling(Check_Optional(i).Caption, Text_Optional_EXT(i))
            End If
        Next i
        
        'If Proceed = True Then
        '    For Fields = 0 To 3
        '        Dedicateds(Pointer, Fields) = Dedicateds(Pointer, Fields) + Text_Optional_EXT(Fields)
        '    Next Fields
            Pointer = Pointer + 1
            If Pointer = 5 Then Add_New.Visible = False
        'End If
    For i = 0 To 3
        Text_Optional_EXT(i) = ""
        Check_Optional(i).Value = 0
    Next i
    End If
End Sub

Private Sub Apply1_Click()
    Call Add_New_Click

    'For i = 0 To Pointer - 1
    '    Dedicateds(Pointer, i) = Text_Optional_EXT(i)
    'Next i

    Dim All_Values As String
    
    Dim Proceed As Boolean
    Proceed = True
    'For i = 0 To 1
    '    If Text_Optional_EXT(i) = "" Then
    '        MsgBox "You must fill the field number " + Str(i + 1), , "Missing Madatory Field"
    '        Proceed = False
    '    End If
    'Next i

 
    
    'For i = 0 To 3
    '    If Check_Optional(i).Value = 1 Then
    '        Select Case UCIP.UCIP_Combo.Text
    '            Case "AdjustmentT"
    '                Values(Pointer) = Values(Pointer) + Filling(Check_Optional(i).Caption, Text_Optional_EXT(i))
    '            Case "UpdateFaFListT"
    '                Values(Pointer) = Values(Pointer) + Filling(Check_Optional(i).Caption, Text_Optional_EXT(i))
    '        End Select
    '    End If
    'Next i
    
    If Proceed = True Then
        For i = 0 To Pointer - 1        'Prepairing the command
            All_Values = All_Values + Values(i)
            Next i
        'Next i
        'Value = Value1
            For i = 0 To Number_Of_Optional_Options
                If UCIP.Check_Optional(i).Caption = "dedicatedAccountInformation" Then UCIP.Text_Optional(i) = _
                                                                                All_Values + _
                                                                                "</struct>" + vbCrLf + _
                                                                            "</value>" + vbCrLf + _
                                                                        "</data>" + vbCrLf + _
                                                                    "</array>" + vbCrLf + _
                                                                "</value>" + vbCrLf + _
                                                            "</member>"
            Next i
            For i = 0 To Number_Of_Mandatory_Options
                If UCIP.Check_Mandatory(i).Caption = "fafInformation" Then UCIP.Text_Mandatory(i) = Values(Pointer) + _
                                                                                "</struct>" + vbCrLf + _
                                                                            "</value>" + vbCrLf + _
                                                                        "</data>" + vbCrLf + _
                                                                    "</array>" + vbCrLf + _
                                                                "</value>" + vbCrLf + _
                                                            "</member>" + vbCrLf
            Next i
        Me.Visible = False
    End If
    'Dedicateds = ""
End Sub
Public Function Dedicated_Constractor(dedicatedAccountID_ As String, _
                                        Optional adjustmentAmount_ As Double, _
                                        Optional newExpiryDate_ As String, _
                                        Optional relativeDateAdjustment_ As String)
    Dim Dedicated_Account_XML As String
    
    Dedicated_Account_XML = "<value><struct>" + vbCrLf + _
                            Filling("dedicatedAccountID", dedicatedAccountID_) & _
                            Filling("adjustmentAmount", adjustmentAmount_)
    If newExpiryDate_ <> "" Then
        Dedicated_Account_XML = Dedicated_Account_XML & _
                                    Filling("newExpiryDate", SulSAT.Date_Formated(newExpiryDate_))
        ElseIf relativeDateAdjustment_ <> "" Then Dedicated_Account_XML = Dedicated_Account_XML & _
                                                            Filling("relativeDateAdjustment", relativeDateAdjustment_)
    End If
    Dedicated_Account_XML = Dedicated_Account_XML & "</struct></value>"
    Dedicated_Constractor = Dedicated_Account_XML
End Function
Private Sub Apply_Click()
    Dim Check_Optional_Value, Ded_Info As String
    For i = 0 To Check_Optional.UBound
        If Me.Check_Optional(i).Value = 1 Then
            Check_Optional_Value = Check_Optional_Value & "1"
            Else
            Check_Optional_Value = Check_Optional_Value & "0"
        End If
    Next i
    
    Select Case Right(Check_Optional_Value, 3)
        Case "100"      'Adjustment only
            Ded_Info = Dedicated_Constractor(Text_Optional_EXT(0), Text_Optional_EXT(1))
        Case "110"      'Adjustment with New Expiry Dates
            Ded_Info = Dedicated_Constractor(Text_Optional_EXT(0), Text_Optional_EXT(1), , Text_Optional_EXT(2))
        Case "101"      'Adjustent With relative Dates
            Ded_Info = Dedicated_Constractor(Text_Optional_EXT(0), Text_Optional_EXT(1), TimeStamp(SulSAT.Date_Formated(Text_Optional_EXT(3))))
        Case Else
            Ded_Info = ""
    End Select
            
    For i = 0 To UCIP.Check_Optional.UBound
        If Ded_Info <> "" And UCIP.Check_Optional(i).Caption = "dedicatedAccountInformation" Then _
            UCIP.Text_Optional(i) = Ded_Info
    Next i
    Me.Visible = False
End Sub

Private Sub Text_Optional_EXT_Change(Index As Integer)
    Check_Optional(Index).Value = 1
End Sub
