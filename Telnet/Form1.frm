VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      Height          =   615
      Left            =   600
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   2160
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Send"
      Height          =   615
      Left            =   2880
      TabIndex        =   1
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   855
      Left            =   3360
      TabIndex        =   0
      Top             =   1320
      Width           =   1215
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   1200
      Top             =   1320
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    Dim RequestID As Long
    Print Winsock1.LocalIP

    Winsock1.Close
         
    Winsock1.RemoteHost = "10.231.14.21"
    Winsock1.RemotePort = 23
    
    Winsock1.Connect
    
    Wait 2000
    If Winsock1.State = sckConnected Then
        Print "Connected"
        Winsock1.SendData "NETSTAT"
        Else
            Print "Not Connected"
         Winsock1.Close
    End If
    


End Sub

Private Sub Command2_Click()
    Dim Data As String
    'Winsock1.Accept RequestID
    Winsock1.SendData "ping 172.28.6.15"
    Winsock1.GetData Data
    Print Data
End Sub

Private Sub Form_Unload(Cancel As Integer)
Winsock1.Close
End Sub

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
Dim Data As String
x = 1
If Winsock1.State = sckConnected Then
    Winsock1.GetData Data
    Text1.Visible = True
    Text1 = Data
    Else
        MsgBox "Not Connected"
End If
End Sub

Public Sub Wait(ByVal dblMilliseconds As Double)
    Dim dblStart As Double
    Dim dblEnd As Double
    Dim dblTickCount As Double
    
    dblTickCount = GetTickCount()
    dblStart = GetTickCount()
    dblEnd = GetTickCount + dblMilliseconds
    
    Do
    DoEvents
    dblTickCount = GetTickCount()
    Loop Until dblTickCount > dblEnd Or dblTickCount < dblStart
End Sub

'  value    name    description
'  0    sckClosed    connection closed
'  1    sckOpen    open
'  2    sckListening    listening for incoming connections
'  3    sckConnectionPending    connection pending
'  4    sckResolvingHost    resolving remote host name
'  5    sckHostResolved    remote host name successfully resolved
'  6    sckConnecting    connecting to remote host
'  7    sckConnected    connected to remote host
'  8      sckClosing connection Is closing
'  9    sckError    error occured

