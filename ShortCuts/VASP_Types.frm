VERSION 5.00
Begin VB.Form VASP_Types 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VASP Tests"
   ClientHeight    =   3075
   ClientLeft      =   11850
   ClientTop       =   5115
   ClientWidth     =   3540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   3540
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   2055
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   3255
      Begin VB.OptionButton Balance_Transfer 
         Caption         =   "Balance Transfer"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1680
         Width           =   2895
      End
      Begin VB.OptionButton Get_BTBarring 
         Caption         =   "Get BTBarring"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1440
         Width           =   2655
      End
      Begin VB.OptionButton Adjust_Change_SC 
         Caption         =   "Adjust Change SC"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1200
         Width           =   2415
      End
      Begin VB.OptionButton Adjust_ChangeSC_Payment 
         Caption         =   "Adjust ChangeSC  Payment"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   2895
      End
      Begin VB.OptionButton Get_Wave_Info 
         Caption         =   "Get Wave Info"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1575
      End
      Begin VB.OptionButton Adjust_Pay 
         Caption         =   "Adjust and Pay"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   1455
      End
      Begin VB.OptionButton Reactivate 
         Caption         =   "Reactivate"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   375
      Left            =   960
      TabIndex        =   1
      Top             =   2640
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "PLZ Select what type of VASP U want to test"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3255
   End
End
Attribute VB_Name = "VASP_Types"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Terminate()
    Display_Form.Visible = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub OK_Click()
    Display_Form.Display.Width = Display_Form.Width - 300
    Call Slide(Display_Form, 1485, Display_Form.Width, True)
    If Reactivate.Value = True Then
        Display_Form.Display = "GET /VASProv/reactivate.asp?MSISDN=20" + Main.MSISDNS.Text + "&REVIVAL_FLAG=False&ADJUST_AMOUNT=000000000000&SYSTEM_ID=IVR&USER_NT_ACCOUNT=IVR HTTP/1.0"
    ElseIf Get_Wave_Info.Value = True Then Display_Form.Display = "GET /VASProv/getWaiveInfo.asp?MSISDN=20" + Main.MSISDNS.Text
    ElseIf Adjust_ChangeSC_Payment = True Then Display_Form.Display = "Get /VASProv/adjust_changeSC_payment.asp?MSISDN=20" + Main.MSISDNS.Text + "&SERVICE_ID=INCOMER&ADJUST_AMOUNT=000000000300&PAYMENT_AMOUNT=000000000000&CARD_GROUP=V1&OLD_SC=0001&NEW_SC=0002"
    ElseIf Adjust_Pay = True Then Display_Form.Display = "GET /VASProv/adjust_payment.asp?MSISDN=20" + Main.MSISDNS.Text + "&SERVICE_ID=INCOMER&ADJUST_AMOUNT=000000000000&PAYMENT_AMOUNT=000000000000&CARD_GROUP=V1"
    ElseIf Adjust_Change_SC = True Then Display_Form.Display = "GET /VASProv/adjust_changeSC.asp?MSISDN=20" + Main.MSISDNS.Text + "&SERVICE_ID=INCOMER&ADJUST_AMOUNT=000000000100&OLD_SC=0000&NEW_SC=0000"
    ElseIf Get_BTBarring = True Then Display_Form.Display = "GET /VASProv/getBTBarring.asp?MSISDN=20" + Main.MSISDNS.Text
    ElseIf Balance_Transfer = True Then Display_Form.Display = "GET /VASProv/FastBalanceTransfer.asp?MSISDN=20" + Main.MSISDNS.Text + "&DEST_MSISDN=20102825817&SERVICE_ID=BALANCE_TRANSFER&DEDUCT_AMOUNT=000000000100&TRANSFER_AMOUNT=000000000100"
    Else: Display_Form.Visible = False
    End If
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
