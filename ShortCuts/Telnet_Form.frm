VERSION 5.00
Begin VB.Form Telnet_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Telnet Form"
   ClientHeight    =   7890
   ClientLeft      =   2745
   ClientTop       =   1245
   ClientWidth     =   4125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7890
   ScaleWidth      =   4125
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Telnet_Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   0
      Top             =   0
   End
   Begin VB.TextBox Display 
      Height          =   7575
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   3855
   End
   Begin ShortCut.Telnet Telnet1 
      Left            =   360
      Top             =   0
      _ExtentX        =   900
      _ExtentY        =   900
   End
End
Attribute VB_Name = "Telnet_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
    Private Type NOTIFYICONDATA
        cbSize As Long
        hwnd As Long
        uID As Long
        uFlags As Long
        uCallbackMessage As Long
        hIcon As Long
        szTip As String * 64
    End Type
       
    Const NIM_ADD = 0
    Const NIM_MODIFY = 1
    Const NIM_DELETE = 2
    Const NIF_MESSAGE = 1
    Const NIF_ICON = 2
    Const NIF_TIP = 4
    
    Const WM_MOUSEMOVE = &H200
    Const WM_LBUTTONDOWN = &H201
    Const WM_LBUTTONUP = &H202
    Const WM_LBUTTONDBLCLK = &H203
    Const WM_RBUTTONDOWN = &H204
    Const WM_RBUTTONUP = &H205
    Const WM_RBUTTONDBLCLK = &H206
    Const WM_MBUTTONDOWN = &H207
    Const WM_MBUTTONUP = &H208
    Const WM_MBUTTONDBLCLK = &H209
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Private Declare Function Shell_NotifyIconA Lib "SHELL32" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Integer
Private Function setNOTIFYICONDATA(hwnd As Long, ID As Long, Flags As Long, CallbackMessage As Long, Icon As Long, Tip As String) As NOTIFYICONDATA
    Dim nidTemp As NOTIFYICONDATA

    nidTemp.cbSize = Len(nidTemp)
    nidTemp.hwnd = hwnd
    nidTemp.uID = ID
    nidTemp.uFlags = Flags
    nidTemp.uCallbackMessage = CallbackMessage
    nidTemp.hIcon = Icon
    nidTemp.szTip = Tip & Chr$(0)

    setNOTIFYICONDATA = nidTemp
End Function

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                
                'Visible = True
                'WindowState = vbNormal
                'Dim hProcess As Long
                'GetWindowThreadProcessId hwnd, hProcess
                'AppActivate hProcess
                
                Call Main.Remain_Click
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Telnet_Timer_Timer()
    If InStr(1, Telnet_Form.Display, "login:") <> 0 And InStr(1, Telnet_Form.Display, "Last login:") = 0 Then
        Telnet_Form.Telnet1.SendData Config.TESTMPS_User, True
        Telnet_Form.Display = ""
        Telnet_Form.Telnet_Timer.Enabled = False
    End If
    If InStr(1, Telnet_Form.Display, "Password:") <> 0 Then
        Telnet_Form.Telnet1.SendData Config.TESTMPS_PIN, True
        Telnet_Form.Display = ""
        Telnet_Form.Telnet_Timer.Enabled = False
        Wait 1000
        Telnet_Form.Telnet1.SendData "NETSTAT", True
        'Call Slide(Telnet_Form, Telnet_Form.Left + Telnet_Form.Width, Telnet_Form.Width, False)
        
        Dim I As Integer
        Dim s As String
        Dim nid As NOTIFYICONDATA
        
        s = "Telnet is in System Tray"
        nid = setNOTIFYICONDATA(Me.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Me.Icon, s)
        I = Shell_NotifyIconA(NIM_ADD, nid)
        
        Me.WindowState = vbMinimized
        Visible = False
        
        'Main.Remain.Enabled = False
        Main.TelnetActivated = "True"
        
        'Telnet_Form.Visible = False
    End If
End Sub
Private Sub Telnet1_DataRecieved(ByVal Data As String)
    Telnet_Form.Telnet_Timer.Enabled = True
    Telnet_Form.Display = Telnet_Form.Display & Data
    Telnet_Form.Display.SelStart = Len(Telnet_Form.Display)
End Sub
