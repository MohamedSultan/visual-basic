VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form Set_Wait_Time_Detailed 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " Set Wait Time Detailed"
   ClientHeight    =   5235
   ClientLeft      =   10200
   ClientTop       =   2580
   ClientWidth     =   7830
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5235
   ScaleWidth      =   7830
   Begin VB.CommandButton Save 
      Caption         =   "Save And Close"
      Height          =   255
      Left            =   1440
      TabIndex        =   23
      Top             =   4680
      Width           =   1695
   End
   Begin VB.TextBox Minsat_Wait_Time 
      Height          =   285
      Index           =   0
      Left            =   2280
      TabIndex        =   22
      Text            =   "Text1"
      Top             =   120
      Width           =   1815
   End
   Begin VB.Frame MINSAT_Test_Frame 
      Caption         =   "MINSAT Test"
      Height          =   1935
      Index           =   1
      Left            =   480
      TabIndex        =   12
      Top             =   2520
      Width           =   3855
      Begin VB.TextBox Minsat_User_Name 
         Height          =   285
         Index           =   1
         Left            =   1800
         TabIndex        =   30
         Text            =   "Text1"
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox Wait_MSISDN 
         Height          =   285
         Index           =   1
         Left            =   1800
         TabIndex        =   20
         Text            =   "Text1"
         Top             =   1080
         Width           =   1815
      End
      Begin VB.TextBox MINSAT_Live_Password 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Index           =   1
         Left            =   1800
         PasswordChar    =   "*"
         TabIndex        =   18
         Text            =   "Text1"
         Top             =   720
         Width           =   855
      End
      Begin VB.CheckBox MSISDN 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   1800
         TabIndex        =   13
         Top             =   1560
         Width           =   255
      End
      Begin VB.Label Label4 
         Caption         =   "User Name"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   29
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "MINSAT Password"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   17
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Wait Time Before Setting MSISDN"
         Height          =   495
         Index           =   5
         Left            =   120
         TabIndex        =   16
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Set MSISDN"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   15
         Top             =   1560
         Width           =   1455
      End
      Begin MSForms.ToggleButton ToggleButton1 
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   14
         Top             =   600
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontName        =   "Times New Roman"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
   End
   Begin VB.Frame MINSAT_Live_Frame 
      Caption         =   "MINSAT Live"
      Height          =   1935
      Index           =   0
      Left            =   480
      TabIndex        =   5
      Top             =   480
      Width           =   3855
      Begin VB.TextBox Minsat_User_Name 
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   28
         Text            =   "Text1"
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox Wait_MSISDN 
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   19
         Text            =   "Text1"
         Top             =   1080
         Width           =   1815
      End
      Begin VB.CheckBox MSISDN 
         Caption         =   "Check1"
         Height          =   255
         Index           =   0
         Left            =   1800
         TabIndex        =   9
         Top             =   1560
         Width           =   255
      End
      Begin VB.TextBox MINSAT_Live_Password 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Index           =   0
         Left            =   1800
         PasswordChar    =   "*"
         TabIndex        =   8
         Text            =   "Text1"
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "User Name"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   27
         Top             =   360
         Width           =   1455
      End
      Begin MSForms.ToggleButton ToggleButton1 
         Height          =   375
         Index           =   0
         Left            =   2640
         TabIndex        =   11
         Top             =   600
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontName        =   "Times New Roman"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Label3 
         Caption         =   "Set MSISDN"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   10
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Wait Time Before Setting MSISDN"
         Height          =   495
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "MINSAT Password"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   1455
      End
   End
   Begin VB.Frame VPN_Frame 
      Height          =   1575
      Left            =   480
      TabIndex        =   0
      Top             =   120
      Width           =   3855
      Begin VB.TextBox RSA_Wait_Time 
         Height          =   285
         Left            =   1800
         TabIndex        =   26
         Text            =   "Text1"
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox VPN_Wait_Time 
         Height          =   285
         Left            =   1800
         TabIndex        =   25
         Text            =   "Text1"
         Top             =   720
         Width           =   1215
      End
      Begin VB.CommandButton Save_ 
         Caption         =   "Save And Close"
         Height          =   255
         Left            =   1200
         TabIndex        =   24
         Top             =   1200
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Sec"
         Height          =   375
         Index           =   1
         Left            =   3120
         TabIndex        =   4
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label2 
         Caption         =   "Sec"
         Height          =   375
         Index           =   0
         Left            =   3120
         TabIndex        =   3
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "VPN Dialer Wait Time"
         Height          =   495
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "RSA Wait Time"
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Label Label3 
      Caption         =   "Wait Time"
      Height          =   255
      Index           =   1
      Left            =   480
      TabIndex        =   21
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "Set_Wait_Time_Detailed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub Save__Click()
    Save_Click
End Sub

Private Sub Save_Click()
    Call Store_Minsat_conf
    Call Save_Conf
    Call Load_Conf
    Form_Unload (0)
End Sub

Private Sub ToggleButton1_Click(Index As Integer)
    With ToggleButton1(0)
        If .Value = True Then
            MINSAT_Live_Password(0).PasswordChar = ""
            Else
            MINSAT_Live_Password(0).PasswordChar = "*"
        End If
    End With
        
    With ToggleButton1(1)
        If .Value = True Then
            MINSAT_Live_Password(1).PasswordChar = ""
            Else
            MINSAT_Live_Password(1).PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Store_Minsat_conf()
    With Set_Wait_Time_Detailed
        Config.MINSAT_L_PIN = .MINSAT_Live_Password(0)
        Config.MINSAT_PIN = .MINSAT_Live_Password(1)
        Wait_Minsat = .Minsat_Wait_Time(0) * 1000
        Wait_Minsat_Live = .Wait_MSISDN(0) * 1000
        Wait_MINSAT_Test = .Wait_MSISDN(1) * 1000
        Wait_RSA = .RSA_Wait_Time * 1000
        Wait_VPN = .VPN_Wait_Time * 1000
    End With
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
