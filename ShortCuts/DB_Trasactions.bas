Attribute VB_Name = "DB_Trasactions"
Type DB_Details
    Connection As ADODB.Connection
    Result As ADODB.Recordset
    Statement As String
End Type
Public Sub Open_DB_Access(ByRef Conn As DB_Details, FileName As String)
    Dim CurrentFilePath As String

    ' Get the data.
    CurrentFilePath = App.Path
    If Right$(FileName, 4) <> ".mdb" Then FileName = FileName & ".mdb"
    If Right$(CurrentFilePath, 1) <> "\" Then CurrentFilePath = CurrentFilePath & "\"
    CurrentFilePath = CurrentFilePath & FileName

    ' Open a connection.
    Set Conn.Connection = New ADODB.Connection
    Conn.Connection.ConnectionString = _
        "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & CurrentFilePath & ";" '& _
        '"Persist Security Info=False"
    ''Conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & Transaction_File_Name & ";Persist Security Info=False"
    ''Conn.ConnectionString = "Provider=MSDAORA.1 ; Password=myPassword; User ID=myUser ; Data Source = ORCL; Persist Security Info=True"
    ''Conn = "DRIVER={ORACLE ODBC DRIVER};SERVER=Service name;UID=id;PWD=password;DBQ=Service name;DBA=W;APA=T;FEN=T;QTO=T;FRC=10;FDL=10;LOB=T;RST=T;FRL=F;MTS=F;CSR=F;PFC=10;TLO=O;"
    'Conn.ConnectionString = "Provider=MSDAORA.1 ; Data Source=VNPP; Password=Sulsat_DB; User ID=Sulsat_DB ; Persist Security Info=True"
    Conn.Connection.open
    Set Conn.Result = CreateObject("ADODB.Recordset")
End Sub

