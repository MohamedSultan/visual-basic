VERSION 5.00
Begin VB.Form Set_Wait_Time 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Set Wait Time"
   ClientHeight    =   1590
   ClientLeft      =   10365
   ClientTop       =   4860
   ClientWidth     =   3810
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1590
   ScaleWidth      =   3810
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox Times 
      Height          =   285
      Left            =   1320
      TabIndex        =   6
      Text            =   "1"
      Top             =   840
      Width           =   255
   End
   Begin VB.ComboBox Path 
      Height          =   315
      Left            =   1320
      TabIndex        =   5
      Text            =   "Combo1"
      Top             =   480
      Width           =   2415
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save And Close"
      Height          =   255
      Left            =   600
      TabIndex        =   3
      Top             =   1200
      Width           =   1695
   End
   Begin VB.TextBox Wait_Time 
      Height          =   285
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "Time(s)"
      Height          =   255
      Left            =   1680
      TabIndex        =   8
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Times To Load"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Start up Path"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Sec"
      Height          =   375
      Left            =   2160
      TabIndex        =   2
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Wait Time"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "Set_Wait_Time"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    'Main.Load_Peri_Conf
End Sub

Public Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub Save_Click()
    With Set_Wait_Time
        Select Case Set_Time_Object
            Case "Minsat Test"
                Wait_MINSAT_Test = Val(Wait_Time) * 1000
            Case "MINSAT Live"
                Wait_Minsat_Live = Val(Wait_Time) * 1000
            Case "Telnet"
                Wait_Telnet = Val(Wait_Time) * 1000
            Case "HLR"
                Wait_HLR = Val(Wait_Time) * 1000
            Case "VPN"
                Wait_VPN = Val(Wait_Time) * 1000
            Case "OMEGA"
                Wait_OMEGA = Val(Wait_Time) * 1000
            Case "TEL_REDA"
                Wait_Reda = Val(Wait_Time) * 1000
                Reda_Telnet_Path = Path
            Case "Tel_MPS"
                Wait_MPS = Val(Wait_Time) * 1000
                MPS_Telnet_Path = Path
            Case "FTP_Reda"
                Wait_Reda_FTP = Val(Wait_Time) * 1000
                Reda_FTP_Path = Path
            Case "FTP_MPS"
                Wait_MPS_FTP = Val(Wait_Time) * 1000
                MPS_FTP_Path = Path
        End Select
    End With
    
    Save_Conf
    Save_Last_Paths
    Load_Last_Paths
    
    Call Save_List(Peri_Form.Paths_List, "Paths")

    Set_Wait_Time.Form_Unload (0)
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub

Private Sub Times_Change()
Dim Times_Int As Integer
Times_Int = Int(Val(Times))
    If Times_Type_Error = False Then
        If Times_Int < 0 Then
            MsgBox "You Must Enter A Positive Integer Number"
            Times = "1"
            End If
        Else
        Times_Type_Error = False
        Times = Times_Old
    End If
End Sub


Private Sub Times_KeyPress(KeyAscii As Integer)
Times_Type_Error = False
Times_Old = Times
    If (KeyAscii >= 65 And KeyAscii <= 90) Or (KeyAscii >= 97 And KeyAscii <= 122) Then
        MsgBox "You Must Enter An Integer Number"
        Times_Type_Error = True
    End If
End Sub

Private Sub Times_LostFocus()
    Dim Times_Int As Integer
    Times_Int = Int(Val(Times))
        If Times_Type_Error = False Then
            If Times_Int < 1 Then
                MsgBox "You Must Enter A Positive Integer Number"
                Times = "1"
                End If
            Else
            Times_Type_Error = False
            Times = Times_Old
        End If
End Sub
