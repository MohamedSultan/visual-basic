VERSION 5.00
Begin VB.Form Favorite_File 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Archive"
   ClientHeight    =   7440
   ClientLeft      =   2085
   ClientTop       =   1875
   ClientWidth     =   6075
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   6075
   Begin VB.CommandButton Save_Close 
      Caption         =   "Save + Close"
      Height          =   375
      Left            =   2880
      TabIndex        =   2
      Top             =   6960
      Width           =   1335
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save"
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   6960
      Width           =   1335
   End
   Begin VB.TextBox Display 
      Height          =   6495
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   240
      Width           =   5535
   End
End
Attribute VB_Name = "Favorite_File"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Display_Change()
    Save.Enabled = True
End Sub


Private Sub Form_Terminate()
    Call Slide(Favorite_File, Favorite_File.Left, Favorite_File.Width, False)
    Call Move_Form(Main, "Right", 1000)
    Call Move_Form(Organized, "Right", 1000 + Main.Width)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Save_Click()
    Save.Enabled = False
    Dim O_Favorites As Long
    O_Favorites = FreeFile
        Open Config.Conf_File_Loc + "\Favorites.txt" For Output As O_Favorites
            Print #O_Favorites, Display
        Close O_Favorites
    
End Sub

Private Sub Save_Close_Click()
    Call Save_Click
    Call Slide(Favorite_File, Favorite_File.Left, Favorite_File.Width, False)
    Call Move_Form(Main, "Right", 1000)
    Call Move_Form(Organized, "Right", 1000 + Main.Width)
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
