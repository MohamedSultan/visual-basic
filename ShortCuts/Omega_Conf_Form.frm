VERSION 5.00
Begin VB.Form Omega_Conf_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Omega Configuration"
   ClientHeight    =   1140
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   1725
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1140
   ScaleWidth      =   1725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   1080
      Top             =   0
   End
   Begin VB.OptionButton No 
      Caption         =   "Press No"
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   720
      Width           =   1695
   End
   Begin VB.OptionButton Yes 
      Caption         =   "Press Yes"
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   480
      Width           =   1695
   End
   Begin VB.CheckBox Do_Nothing 
      Caption         =   "Do Nothing"
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Value           =   1  'Checked
      Width           =   3255
   End
End
Attribute VB_Name = "Omega_Conf_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Do_Nothing_Click()
    Call Form_Load
End Sub

Private Sub Form_Load()
    If Do_Nothing.Value = 1 Then
        Yes.Enabled = False
        No.Enabled = False
    Else
        Yes.Enabled = True
        No.Enabled = True
    End If
End Sub
Private Sub Timer1_Timer()
    Call Slide(Me, Me.Left, Me.Width, False)
    Timer1.Enabled = False
End Sub
Private Sub Form_LostFocus()
    'VPN_Conf_Form.Timer1.Enabled = True
    Call Timer1_Timer
End Sub
