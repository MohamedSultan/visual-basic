VERSION 5.00
Begin VB.Form Organized 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Organized"
   ClientHeight    =   8745
   ClientLeft      =   2445
   ClientTop       =   840
   ClientWidth     =   7890
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleWidth      =   7890
   Begin VB.TextBox Organized_Password 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   6360
      TabIndex        =   52
      Text            =   "Password"
      Top             =   120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   51
      Text            =   "Password"
      Top             =   120
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton SavePassword 
      Caption         =   "SavePassword"
      Height          =   255
      Left            =   6360
      TabIndex        =   50
      Top             =   360
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton Pages 
      Caption         =   "Pages"
      Height          =   375
      Index           =   1
      Left            =   5400
      TabIndex        =   49
      Top             =   8280
      Width           =   615
   End
   Begin VB.CommandButton Pages 
      Caption         =   "Pages"
      Height          =   375
      Index           =   0
      Left            =   1800
      TabIndex        =   48
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox Data 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   1800
      TabIndex        =   47
      Top             =   120
      Width           =   4455
   End
   Begin VB.CommandButton PreviousPage 
      Caption         =   "Previous Page"
      Height          =   375
      Left            =   120
      TabIndex        =   46
      Top             =   8280
      Width           =   1575
   End
   Begin VB.CommandButton Next_Page 
      Caption         =   "Next Page"
      Height          =   375
      Left            =   6120
      TabIndex        =   45
      Top             =   8280
      Width           =   1575
   End
   Begin VB.CommandButton Extend 
      Caption         =   ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      Height          =   7335
      Left            =   7560
      TabIndex        =   44
      ToolTipText     =   "Extend           "
      Top             =   720
      Width           =   255
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   20
      Left            =   120
      MaxLength       =   24
      TabIndex        =   43
      Top             =   7440
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   19
      Left            =   120
      MaxLength       =   24
      TabIndex        =   42
      Top             =   7440
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   18
      Left            =   120
      MaxLength       =   24
      TabIndex        =   41
      Top             =   7080
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   17
      Left            =   120
      MaxLength       =   24
      TabIndex        =   40
      Top             =   6720
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   16
      Left            =   120
      MaxLength       =   24
      TabIndex        =   39
      Top             =   6360
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   15
      Left            =   120
      MaxLength       =   24
      TabIndex        =   38
      Top             =   6000
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   14
      Left            =   120
      MaxLength       =   24
      TabIndex        =   37
      Top             =   5640
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   13
      Left            =   120
      MaxLength       =   24
      TabIndex        =   36
      Top             =   5280
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   12
      Left            =   120
      MaxLength       =   24
      TabIndex        =   35
      Top             =   4920
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   11
      Left            =   120
      MaxLength       =   24
      TabIndex        =   34
      Top             =   4560
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   120
      MaxLength       =   24
      TabIndex        =   33
      Top             =   4200
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   120
      MaxLength       =   24
      TabIndex        =   32
      Top             =   3840
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   120
      MaxLength       =   24
      TabIndex        =   31
      Top             =   3480
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   120
      MaxLength       =   24
      TabIndex        =   30
      Top             =   3120
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   120
      MaxLength       =   24
      TabIndex        =   29
      Top             =   2760
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   120
      MaxLength       =   24
      TabIndex        =   28
      Top             =   2400
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   120
      MaxLength       =   24
      TabIndex        =   27
      Top             =   2040
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   120
      MaxLength       =   24
      TabIndex        =   26
      Top             =   1680
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   120
      MaxLength       =   24
      TabIndex        =   25
      Top             =   1320
      Width           =   2295
   End
   Begin VB.TextBox Title 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   120
      MaxLength       =   24
      TabIndex        =   24
      Top             =   960
      Width           =   2295
   End
   Begin VB.Frame Frame2 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7455
      Left            =   0
      TabIndex        =   23
      Top             =   600
      Width           =   2655
   End
   Begin VB.Frame Frame1 
      Caption         =   "Data"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7455
      Left            =   2640
      TabIndex        =   2
      Top             =   600
      Width           =   4935
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   20
         Left            =   240
         TabIndex        =   22
         Top             =   7560
         Visible         =   0   'False
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   19
         Left            =   240
         TabIndex        =   21
         Top             =   6840
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   18
         Left            =   240
         TabIndex        =   20
         Top             =   6480
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   17
         Left            =   240
         TabIndex        =   19
         Top             =   6120
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   16
         Left            =   240
         TabIndex        =   18
         Top             =   5760
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   15
         Left            =   240
         TabIndex        =   17
         Top             =   5400
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   14
         Left            =   240
         TabIndex        =   16
         Top             =   5040
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   13
         Left            =   240
         TabIndex        =   15
         Top             =   4680
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   12
         Left            =   240
         TabIndex        =   14
         Top             =   4320
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   11
         Left            =   240
         TabIndex        =   13
         Top             =   3960
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   10
         Left            =   240
         TabIndex        =   12
         Top             =   3600
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   9
         Left            =   240
         TabIndex        =   11
         Top             =   3240
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   8
         Left            =   240
         TabIndex        =   10
         Top             =   2880
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   7
         Left            =   240
         TabIndex        =   9
         Top             =   2520
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   6
         Left            =   240
         TabIndex        =   8
         Top             =   2160
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   5
         Left            =   240
         TabIndex        =   7
         Top             =   1800
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   4
         Left            =   240
         TabIndex        =   6
         Top             =   1440
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   3
         Left            =   240
         TabIndex        =   5
         Top             =   1080
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   2
         Left            =   240
         TabIndex        =   4
         Top             =   720
         Width           =   4455
      End
      Begin VB.TextBox Data 
         Height          =   285
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   4455
      End
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save"
      Height          =   375
      Left            =   2520
      TabIndex        =   1
      Top             =   8280
      Width           =   1335
   End
   Begin VB.CommandButton Save_Close 
      Caption         =   "Save + Close"
      Height          =   375
      Left            =   3960
      TabIndex        =   0
      Top             =   8280
      Width           =   1335
   End
End
Attribute VB_Name = "Organized"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub Extend_Click()
    Dim I_Favorites As Double
    I_Favorites = FreeFile
    Dim Step As Integer

    On Error GoTo error_Handler_Favorites
    
        Open Config.Conf_File_Loc + "\Favorites.txt" For Input As I_Favorites
            Do Until EOF(I_Favorites)
                ReDim Preserve Favorites_Txt(Step)
                Line Input #I_Favorites, Favorites_Txt(Step)
                Step = Step + 1
                'If Favorites_Txt(Step) <> "" Then Step = Step + 1
            Loop
    Close I_Favorites
            
    Favorite_File.Display = Favorites_Txt(0)
    For I = 1 To UBound(Favorites_Txt)
        Favorite_File.Display = Favorite_File.Display + vbCrLf + Favorites_Txt(I)   'To Get New Line
    Next I
    
    'Close_All
    Call Move_Form(Main, "Left", -700)
    Call Move_Form(Me, "Left", Main.Width - 700)
    Call Slide(Favorite_File, Me.Left + Me.Width, 6165, True)

error_Handler_Favorites:
    If Err.Number = 53 Then
            O_Favorites = FreeFile
            Open Config.Conf_File_Loc + "\Favorites.txt" For Output As O_Favorites
            Close O_Favorites
    End If
End Sub

Private Sub Form_Load()
    'Number_Of_Organized_Pages = 3
End Sub

Private Sub Form_Terminate()
    Call Close_Me(Favorite_File)
    Call Close_Me(Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Organized_Password_Click()
    Organized_Form(Current_PageNumber).Organized_Password = ""
    Organized_Form(Current_PageNumber).Organized_Password.PasswordChar = "*"
End Sub

Private Sub Organized_Password_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call Password_Passed
    End If
End Sub
Private Function Password_Passed() As Boolean
    If Organized_Password = Decrypt(Trim(Organized_Form(Current_PageNumber).Title(0))) Or _
        Trim(Organized_Form(Current_PageNumber).Title(0)) = "" Then
        
        For I = 0 To 20
            Organized_Form(Current_PageNumber).Title(I).Enabled = True
            Organized_Form(Current_PageNumber).Data(I).Enabled = True
            Organized_Form(Current_PageNumber).Title(I).PasswordChar = ""
            Organized_Form(Current_PageNumber).Data(I).PasswordChar = ""
        Next I
        Organized_Form(Current_PageNumber).Title(0).Visible = False
        SavePassword.Visible = True
        Password_Passed = True
    End If
End Function
Private Sub Pages_Click(Index As Integer)
    'Call Load_Organized(Current_PageNumber)
    Load_Label_Pages (Current_PageNumber)
    Call Pages_Form.Adjust_Height
    Call Move_Form(Organized_Form(Current_PageNumber), "Left", Main.Left)
    Call Slide(Pages_Form, Organized_Form(Current_PageNumber).Left + Organized_Form(Current_PageNumber).Width, Pages_Form.Width, True)
End Sub

Private Sub PreviousPage_Click()
    Current_PageNumber = Current_PageNumber - 1
    'If Current_PageNumber = 1 Then Current_PageNumber = 0
    Call Load_Organized(Current_PageNumber)
End Sub
Private Sub Next_Page_Click()
    Current_PageNumber = Current_PageNumber + 1
    'If Current_PageNumber = 1 Then Current_PageNumber = 2
    Call Load_Organized(Current_PageNumber)
End Sub
Public Sub Loading_Organized_Data()
    Dim Organized_Txt() As Variant
    I_Organized = FreeFile
    Dim Step As Integer
    Dim Encrypted As Boolean
    
    On Error GoTo error_handler_organized
    ReDim Organized_Command(0)
        Open Config.Conf_File_Loc + "\Organized.txt" For Input As I_Organized
            Do Until EOF(I_Organized)
                ReDim Preserve Organized_Txt(Step)
                Line Input #I_Organized, Organized_Txt(Step)
                ReDim Preserve Organized_Command(Step)
                
                'If Step Mod 20 = 0 Then             'the password part
                '    If Trim(Mid(Organized_Txt(Step), 1, 24)) <> "" Then
                '        Encrypted = True
                '    Else
                '        Encrypted = False
                '    End If
                'End If
                
                'If Encrypted Then
                '    Organized_Command(Step).Title = Decrypt(Trim(Mid(Organized_Txt(Step), 1, 24)))
                '    If Step Mod 20 = 0 Then
                '        Organized_Command(Step).Data = Trim(Mid(Organized_Txt(Step), 25, Len(Organized_Txt(Step))))
                ''    Else
                 '       Organized_Command(Step).Data = Decrypt(Trim(Mid(Organized_Txt(Step), 25, Len(Organized_Txt(Step)))))
                 '   End If
                 '   Step = Step + 1
                'Else
                    Organized_Command(Step).Title = Mid(Organized_Txt(Step), 1, 24)
                    Organized_Command(Step).Data = Mid(Organized_Txt(Step), 25, Len(Organized_Txt(Step)))
                    Step = Step + 1
                'End If
            Loop
    Close I_Organized
    'Setting Forms
    ReDim Preserve Organized_Form((Int(UBound(Organized_Command) / 20)) + 1)
    
error_handler_organized:
    If Err.Number = 53 Then
        O_Organized = FreeFile
        Open Config.Conf_File_Loc + "\Organized.txt" For Output As O_Organized
        Close O_Organized
    
        For I = 0 To 20
            Organized.Title(I) = ""
            Organized.Data(I) = ""
        Next I
    
        ReDim Preserve Organized_Form((Int(UBound(Organized_Command) / 20)) + 1)

    End If
End Sub
Public Sub Load_Organized(Page_Number As Integer)
        
    For I = 0 To (Int(UBound(Organized_Command) / 20) + 1)
        If I = (Int(UBound(Organized_Command) / 20) + 1) Then ReDim Preserve Organized_Form((Int(UBound(Organized_Command) / 20)) + 1)
        Set Organized_Form(I) = New Organized
        
        For labels = 0 To Pages_Form.Command_Page.UBound
            'Organized_Form(I).Page_Label(labels).MousePointer = 0  ' Default
            'Organized_Form(I).Page_Label(labels).Caption = ""
            Pages_Form.Command_Page(labels).MousePointer = 0  ' Default
            Pages_Form.Command_Page(labels).Caption = ""
        Next labels
    
    Next I
    '--------------------------------------------------------------------------------
    'Empty Page
    For I = 0 To (Int(UBound(Organized_Command) / 20) + 1)
        For j = 0 To 20
            Organized_Form(I).Title(j) = ""
            Organized_Form(I).Data(j) = ""
        Next j
    Next I
    
    If Page_Number >= (Int(UBound(Organized_Command) / 20) + 1) Then
       Dim Step As Integer
       Step = UBound(Organized_Command) + 1
       For I = Step To 20 * Page_Number
            ReDim Preserve Organized_Command(Step)
            Step = Step + 1
        Next I
        ReDim Preserve Organized_Form(Page_Number)
        Set Organized_Form(Page_Number) = New Organized
    End If
    
    Close_All
    Call Move_Form(Main, "Left", 1000)
    Call Slide(Organized_Form(Page_Number), Main.Left + Main.Width, 8010, True, 3)
    
    If Trim(Organized_Command(Page_Number * 20).Title) <> "" Then   'With Password
        For I = 0 To 20
            Organized_Form(Page_Number).Title(I).PasswordChar = "*"
            Organized_Form(Page_Number).Title(I).Enabled = False
            Organized_Form(Page_Number).Data(I).PasswordChar = "*"
            Organized_Form(Page_Number).Data(I).Enabled = False
        Next I
        Organized_Form(Page_Number).Organized_Password.Visible = True
        Organized_Form(Page_Number).SavePassword.Visible = False
    Else
        For I = 0 To 20
            Organized_Form(Page_Number).Title(I).PasswordChar = ""
            Organized_Form(Page_Number).Title(I).Enabled = True
            Organized_Form(Page_Number).Data(I).PasswordChar = ""
            Organized_Form(Page_Number).Data(I).Enabled = True
        Next I
        Organized_Form(Page_Number).Title(0).Visible = False
        Organized_Form(Page_Number).Organized_Password.Visible = True
        Organized_Form(Page_Number).SavePassword.Visible = True
    End If

    'Loading data
    'For forms_ = 0 To UBound(Organized_Form)
        For I = 0 To UBound(Organized_Command)
            Organized_Form(Int(I / 20)).Title(I Mod 20) = Organized_Command(I).Title
            Organized_Form(Int(I / 20)).Data(I Mod 20) = Organized_Command(I).Data
        Next I
    'Next forms_
    
    
    
    Organized_Form(Page_Number).Caption = "Organized  - Page" + Str(Page_Number + 1)
    'ReDim Organized_Txt(UBound(Organized_Txt) + 1)
    Organized_Form(0).PreviousPage.Enabled = False

    Call Load_Label_Pages(Page_Number)
End Sub
Public Sub Load_Label_Pages(Current_PageNumber As Integer)
    For labels = 0 To UBound(Organized_Form)
        If labels * 20 <= UBound(Organized_Command) Then
            Pages_Form.Command_Page(labels).Caption = Trim(Organized_Command(labels * 20).Data) + " - Page" + Str(labels + 1)
            Pages_Form.Command_Page(labels).MousePointer = 2  ' Cross
            Pages_Form.Command_Page(labels).Visible = True
        Else
            Pages_Form.Command_Page(labels).MousePointer = 0   'Default
            Pages_Form.Command_Page(labels).Visible = False
        End If
    Next labels
End Sub

Private Sub Update_Organized_Commands()
    Dim Step As Integer
    For forms_ = 0 To UBound(Organized_Form)
        For titles = 0 To 20
            If Step > UBound(Organized_Command) Then
                ReDim Preserve Organized_Command(Step)
            End If
            Organized_Command(Step).Title = Organized_Form(forms_).Title(titles)
            Organized_Command(Step).Data = Organized_Form(forms_).Data(titles)
            Step = Step + 1
        Next titles
    Next forms_
End Sub

Private Sub Save_Click()
    
    Call Update_Organized_Commands
    Save.Enabled = False
    Dim O_Organized As Long
    O_Organized = FreeFile
        Open Config.Conf_File_Loc + "\Organized.txt" For Output As O_Organized
            'checking the if Data Exists
            Dim Last_Position_Of_Data As Integer
            For I = 0 To UBound(Organized_Command)
                If Trim(Organized_Command(I).Title) <> "" Or Trim(Organized_Command(I).Data) <> "" Then
                    Last_Position_Of_Data = I
                End If
            Next I
            
            'saving all forms data
            For I = 0 To Last_Position_Of_Data
                If Organized_Form(Int(I / 20)).Title(I Mod 20) = "" And Organized_Form(Int(I / 20)).Data(I Mod 20) = "" Then
                    Print #O_Organized, ""
                    Else
                        Print #O_Organized, Organized_Form(Int(I / 20)).Title(I Mod 20) + _
                                            Extend_Char(24 - Len(Organized_Form(Int(I / 20)).Title(I Mod 20))) + _
                                            Organized_Form(Int(I / 20)).Data(I Mod 20)
                End If
            Next I
        Close O_Organized
    Call Loading_Organized_Data
End Sub

Private Function Extend_Char(Length As Integer)
    Dim Temp As String
    Temp = ""
    If Length > 0 Then
        For I = 1 To Length
            Temp = Temp + " "
        Next I
    End If
    Extend_Char = Temp
End Function
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub

Private Sub Save_Close_Click()
    Call Save_Click
    Call Close_Me(Me)
End Sub

Private Sub SavePassword_Click()
    If Password_Passed() Or Organized_Form(1).Enabled Then
        Organized_Form(Current_PageNumber).Title(0) = Encrypt(Organized_Password)
        For I = 1 To Organized_Form(Current_PageNumber).Title.UBound
            Organized_Form(Current_PageNumber).Title(I) = Encrypt(Trim(Organized_Form(Current_PageNumber).Title(I)))
            Organized_Form(Current_PageNumber).Data(I) = Encrypt(Trim(Organized_Form(Current_PageNumber).Data(I)))
        Next I
        Call Save_Click
        Call Load_Organized(Current_PageNumber)
    End If
End Sub

Private Sub Title_Change(Index As Integer)
    Save.Enabled = True
End Sub
Private Sub Data_Change(Index As Integer)
    Save.Enabled = True
End Sub


