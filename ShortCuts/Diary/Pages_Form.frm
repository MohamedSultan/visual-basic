VERSION 5.00
Begin VB.Form Pages_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Pages"
   ClientHeight    =   7785
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3570
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7785
   ScaleWidth      =   3570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   31
      Left            =   120
      TabIndex        =   31
      Top             =   7440
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   30
      Left            =   120
      TabIndex        =   30
      Top             =   7200
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   29
      Left            =   120
      TabIndex        =   29
      Top             =   6960
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   28
      Left            =   120
      TabIndex        =   28
      Top             =   6720
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   27
      Left            =   120
      TabIndex        =   27
      Top             =   6480
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   26
      Left            =   120
      TabIndex        =   26
      Top             =   6240
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   25
      Left            =   120
      TabIndex        =   25
      Top             =   6000
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   24
      Left            =   120
      TabIndex        =   24
      Top             =   5760
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   23
      Left            =   120
      TabIndex        =   23
      Top             =   5520
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   22
      Left            =   120
      TabIndex        =   22
      Top             =   5280
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   21
      Left            =   120
      TabIndex        =   21
      Top             =   5040
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   20
      Left            =   120
      TabIndex        =   20
      Top             =   4800
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   19
      Left            =   120
      TabIndex        =   19
      Top             =   4560
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   18
      Left            =   120
      TabIndex        =   18
      Top             =   4320
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   17
      Left            =   120
      TabIndex        =   17
      Top             =   4080
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   16
      Top             =   3840
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   15
      Left            =   120
      TabIndex        =   15
      Top             =   3600
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   14
      Left            =   120
      TabIndex        =   14
      Top             =   3360
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   13
      Left            =   120
      TabIndex        =   13
      Top             =   3120
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   12
      Left            =   120
      TabIndex        =   12
      Top             =   2880
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   11
      Left            =   120
      TabIndex        =   11
      Top             =   2640
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   10
      Top             =   2400
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   9
      Top             =   2160
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   8
      Top             =   1920
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   6
      Top             =   1440
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.CommandButton Command_Page 
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   3375
   End
End
Attribute VB_Name = "Pages_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub Adjust_Height()
    Dim Number_Of_Commands As Integer
    For I = 0 To Command_Page.UBound
        If Command_Page(I).Caption <> "" Then Number_Of_Commands = Number_Of_Commands + 1
    Next I
    
    If Number_Of_Commands > 32 Then
        Me.Width = 4050
        Me.Height = 8295
    Else
        Me.Height = 510 + Number_Of_Commands * Command_Page(0).Height
        'ToggleButton1.Height = Me.Height - 510 - 2 * ToggleButton1.Top
        Me.Width = 3690
    End If
End Sub
Private Sub Command_Page_Click(Index As Integer)
    If Index < UBound(Organized_Form) Then
        Call Organized.Load_Organized(Index)
        Current_PageNumber = Index
    End If
End Sub

Private Sub Form_Terminate()
    'Call Organized.Load_Organized(Current_PageNumber)
    Call Move_Form(Organized_Form(Current_PageNumber), "right", Main.Left + Main.Width)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub ToggleButton1_Click()
    If ToggleButton1.Value = True Then
        ToggleButton1.Caption = "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        Pages_Form.Width = 7470
    Else
        ToggleButton1.Caption = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        Pages_Form.Width = 4050
    End If
End Sub
