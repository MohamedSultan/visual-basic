VERSION 5.00
Begin VB.Form Password_Form 
   BorderStyle     =   0  'None
   ClientHeight    =   1320
   ClientLeft      =   7800
   ClientTop       =   5055
   ClientWidth     =   2370
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1320
   ScaleWidth      =   2370
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Close1 
      Caption         =   "x"
      Height          =   195
      Left            =   4320
      TabIndex        =   12
      Top             =   40
      Width           =   195
   End
   Begin VB.CommandButton close2 
      Caption         =   "x"
      Height          =   195
      Left            =   2160
      TabIndex        =   11
      Top             =   0
      Width           =   195
   End
   Begin VB.CommandButton Cancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2280
      TabIndex        =   10
      Top             =   1560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1080
      TabIndex        =   9
      Top             =   1560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox OldPassword 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "*"
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox NewPassword1 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   600
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox NewPassword2 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   1080
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton Change_Password 
      Caption         =   "Change Password"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Width           =   1875
   End
   Begin VB.TextBox Password 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      IMEMode         =   3  'DISABLE
      Left            =   120
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   480
      Width           =   2175
   End
   Begin VB.Label Label4 
      Caption         =   "New Password"
      Height          =   255
      Left            =   360
      TabIndex        =   8
      Top             =   720
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Confirm New Password"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Old Password"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "PLZ Enter Your Password"
      Height          =   255
      Left            =   100
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
End
Attribute VB_Name = "Password_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Encrypted_Password As String

Private Sub Cancel_Click()
    Change_Password.Visible = True
    Label1.Visible = True
    Password.Visible = True
    close2.Visible = True
    
    Label2.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    
    OldPassword.Visible = False
    NewPassword1.Visible = False
    NewPassword2.Visible = False
    OK.Visible = False
    Close1.Visible = False
    Cancel.Visible = False
    
    Me.Top = 5055
    Me.Left = 7800
    Me.Width = 2370
    Me.Height = 1320
End Sub

Private Sub Change_Password_Click()
    Change_Password.Visible = False
    Label1.Visible = False
    Password.Visible = False
    close2.Visible = False
    
    Label2.Visible = True
    Label3.Visible = True
    Label4.Visible = True
    
    OldPassword.Visible = True
    NewPassword1.Visible = True
    NewPassword2.Visible = True
    OK.Visible = True
    Cancel.Visible = True
    Close1.Visible = True
    
    Me.Top = Me.Top - 900
    Me.Left = Me.Left - 900
    Me.Width = 4545
    Me.Height = 2070
End Sub

Private Sub Close1_Click()
    End
End Sub

Private Sub close2_Click()
    End
End Sub

Private Sub Form_Load()
    Call Cancel_Click
    On Error GoTo Password_Handler
    I_Password = FreeFile
    Open Config.Conf_File_Loc + "\Password.txt" For Input As I_Password
        Line Input #I_Password, Encrypted_Password
    Close I_Password

Password_Handler:
    If Err.Number = 53 Then         'No File
        Call Change_Password_Click
        OldPassword.Visible = False
        OldPassword = "Sultan"
        'NewPassword1 = ""
    End If
End Sub

Private Sub NewPassword2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call OK_Click
End Sub

Private Sub NewPassword1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 9 Then NewPassword2.SetFocus
End Sub

Private Sub OK_Click()
    Dim Continue As Integer
    If (Decrypt(Encrypted_Password) = OldPassword Or OldPassword = "Sultan") And OldPassword <> "" Then
        If NewPassword1 = NewPassword2 And NewPassword1.Visible = True And NewPassword2.Visible = True And NewPassword1 <> "" Then
            O_Password = FreeFile
            Open Config.Conf_File_Loc + "\Password.txt" For Output As O_Password
                Print #O_Password, Encrypt(NewPassword1)
            Close O_Password
            Call Form_Load
            MsgBox "Your Password Has Been Changed Successfully", vbInformation, "Password Change"
            Call Password_KeyPress(13)
        Else
            Continue = MsgBox("The Passwords you Typed do not Match, PLZ Type The Right Passwords In Both Fields", vbOKCancel, "Passwords Error")
            If Continue = 2 Then Call Cancel_Click
        End If
        Else
        Continue = MsgBox("Sorry your Old Password Is Incorrect, Please Type The Right Password", vbOKCancel, "Password Error")
        If Continue = 2 Then Call Cancel_Click
    End If
End Sub

Private Sub Password_KeyPress(KeyAscii As Integer)
    Dim Continue As Integer
    If KeyAscii = 13 Then
        If (Decrypt(Encrypted_Password) = Password Or OldPassword = "Sultan") Then
            'Call Slide(Main, Main.Left, Main.Width, True)
            Me.Hide
            Dim progress_Wait As Integer
            progress_Wait = 3000
            Show_ProgressBar (progress_Wait)
            Wait progress_Wait
            Main.Show
        Else
            Continue = MsgBox("Sorry The Password You Typed Is Incorrect, Please Type The Right Password", vbOKCancel, "Password Error")
            If Continue = 2 Then End
        End If
    End If
End Sub
