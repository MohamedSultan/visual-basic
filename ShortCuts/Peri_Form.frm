VERSION 5.00
Begin VB.Form Peri_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PeriPro"
   ClientHeight    =   1740
   ClientLeft      =   7980
   ClientTop       =   3840
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   4680
   Begin VB.ComboBox Paths_List 
      Height          =   315
      Left            =   1560
      TabIndex        =   4
      Text            =   "Paths List"
      Top             =   240
      Width           =   3015
   End
   Begin VB.TextBox Exceed_Time_Command 
      Height          =   285
      Left            =   1560
      TabIndex        =   2
      Text            =   "0"
      Top             =   720
      Width           =   1575
   End
   Begin VB.CommandButton Apply 
      Caption         =   "Apply"
      Height          =   375
      Left            =   1440
      TabIndex        =   1
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Exceed Wait Time"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Path_Label 
      Caption         =   "Path"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "Peri_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Apply_Click()
    Wait_Exceed = Val(Exceed_Time_Command) * 1000
    Save_Conf
    Peri_Form.Visible = False
    Call Save_List(Peri_Form.Paths_List, "Paths")
    Save_Last_Paths
    Load_Last_Paths
    Call Slide(Peri_Form, Peri_Form.Left, Peri_Form.Width, False)
    Call Move_Form(Main, "Right", 4635)
    Close_All
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub Show_PIN_Click()
    If Show_PIN.Value = False Then
        Password.PasswordChar = "*"
        Else
        Password.PasswordChar = ""
    End If
End Sub



Private Sub Paths_List_LostFocus()
    Dim MSISDN As Integer
    Dim found_in_list As Boolean
    found_in_list = False
    
    For i = 0 To Paths_List.ListCount
        If Paths_List.Text = Paths_List.List(i) Then found_in_list = True
        Next i
    
    'MSISDN = Val(Paths_List.Text)
    If found_in_list = False Then
        Paths_List.AddItem Paths_List.Text, 0
        Paths_List.Text = Paths_List.List(0)
    End If
    
    If LCase(Mid(Peri_Form.Caption, 5, 8)) = "pro" Then PeriPro_Last_Path = Paths_List
    If LCase(Mid(Peri_Form.Caption, 5, 11)) = "studio" Then PeriStudio_Path = Paths_List

End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
