VERSION 5.00
Begin VB.Form Vmm_form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "User"
   ClientHeight    =   2295
   ClientLeft      =   8445
   ClientTop       =   3720
   ClientWidth     =   5850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2295
   ScaleWidth      =   5850
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   ".cfg"
      Height          =   375
      Left            =   4320
      TabIndex        =   7
      Top             =   600
      Width           =   1300
   End
   Begin VB.TextBox Display 
      Height          =   285
      Index           =   1
      Left            =   360
      TabIndex        =   6
      Top             =   1680
      Width           =   5175
   End
   Begin VB.TextBox Display 
      Height          =   285
      Index           =   0
      Left            =   360
      TabIndex        =   5
      Top             =   1320
      Width           =   1455
   End
   Begin VB.CommandButton Status 
      Caption         =   "Status"
      Height          =   375
      Left            =   3000
      TabIndex        =   4
      Top             =   600
      Width           =   1300
   End
   Begin VB.CommandButton Unload 
      Caption         =   "Unload"
      Height          =   375
      Left            =   1680
      TabIndex        =   3
      Top             =   600
      Width           =   1300
   End
   Begin VB.ComboBox MMF_files 
      Height          =   315
      Left            =   1680
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   120
      Width           =   2655
   End
   Begin VB.CommandButton Load 
      Caption         =   "Load"
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   600
      Width           =   1300
   End
   Begin VB.Label Label1 
      Caption         =   "mmf File Name"
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   180
      Width           =   1455
   End
End
Attribute VB_Name = "Vmm_form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Last_MMF As String


Private Sub command1_Click()
    Display(0) = ""
    Display(1) = "vi /opt/vps/mps72/etc/vmm-mmf.cfg"
End Sub

Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub

Private Sub Form_Terminate()
    Call Slide(Vmm_form, Vmm_form.Left, Vmm_form.Width, False)
    Call Close_Me(Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Load_Click()
    Display(0) = "vsh"
    Display(1) = "vmm mmfload " + MMF_files.Text
End Sub

Private Sub MMF_files_GotFocus()
    SendMessage MMF_files.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub MMF_files_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = MMF_files.Text
        For I = 0 To MMF_files.ListCount - 1
            If StrComp(PSTR, (Left(MMF_files.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                MMF_files.ListIndex = I
            Exit For
            End If
        Next I
        MMF_files.SelStart = Len(PSTR)
        MMF_files.SelLength = Len(MMF_files.Text) - Len(PSTR)
    End If
End Sub

Private Sub MMF_files_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(MMF_files.Text) = 0) Then
        SendMessage MMF_files.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage MMF_files.hwnd, CB_SHOWDROPDOWN, 0, 1
    End If
    If KeyAscii = 32 And Len(MMF_files.Text) = 0 Then KeyAscii = 0
    
End Sub

Private Sub MMF_files_LostFocus()
    Call Save_List(MMF_files, "MMFs")
End Sub
Private Sub Form_Load()
    Call Load_List(MMF_files, "MMFs")
End Sub

Private Sub Status_Click()
    Display(0) = "vsh"
    Display(1) = "vmm mmfstatus"
End Sub

Private Sub Unload_Click()
    Display(0) = "vsh"
    Display(1) = "vmm mmfunload " + MMF_files.Text
End Sub
