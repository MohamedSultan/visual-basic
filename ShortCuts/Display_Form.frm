VERSION 5.00
Begin VB.Form Display_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Display"
   ClientHeight    =   1020
   ClientLeft      =   45
   ClientTop       =   2040
   ClientWidth     =   16110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1020
   ScaleWidth      =   16110
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Text            =   "Telnet VASPDEV1 80"
      Top             =   120
      Width           =   1815
   End
   Begin VB.TextBox Display 
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   480
      Width           =   15855
   End
End
Attribute VB_Name = "Display_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Me.Top = Main.Top - Me.Height
    Me.Left = Main.Left
End Sub

Private Sub Form_Terminate()
    VASP_Types.Visible = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
