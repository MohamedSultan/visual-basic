Attribute VB_Name = "Functions"
Public Sub ping(IP As String, Optional UserName As String, Optional ByVal Password As String)
    'Dim My_IP As String
    'My_IP = Get_IP_Address
    Dim Ping_TelNet_Form_ID As Integer
    X = Main.Create_Telnet_Form("172.28.6.15", UserName, Password, "", , "ping " + Config.VPN_IP)
    Active_Telnet_Form(X).Caption = "Sultan"
End Sub
Public Sub FTP(IP As String, Optional UserName As String, Optional ByVal Password As String, _
                    Optional ByVal StartUpPath As String, Optional ByVal LocalPath As String, _
                    Optional ByVal Command1ToBeSent As String, _
                    Optional ByVal Command2ToBeSent As String, _
                    Optional ByVal Command3ToBeSent As String, _
                    Optional ByVal Command4ToBeSent As String, _
                    Optional ByVal Command5ToBeSent As String)
                    
    Dim O_File_Name As Long
    O_File_Name = FreeFile
    Dim Drive As String
    If LocalPath = "" Then LocalPath = "D:\Work\Exchange"
    Drive = Left(LocalPath, InStr(1, LocalPath, "\") - 1)
    
    Open App.Path + "\Temp.bat" For Output As O_File_Name
        Print #O_File_Name, Drive
        Print #O_File_Name, "cd " + LocalPath
        Print #O_File_Name, "c:\windows\system32\ftp " + IP
    Close O_File_Name
    
    FTP_Shell = Shell(App.Path + "\Temp.bat", vbNormalFocus)
    Wait 400
    AppActivate (FTP_Shell)
    If UserName <> "" Then
        SendKeys (UserName)
        SendKeys "{ENTER}"
        Wait 100
    End If
    If Password <> "" Then
        SendKeys (Password)
        SendKeys "{ENTER}"
        Wait 100
    End If
    If StartUpPath <> "" Then
        SendKeys ("cd " + StartUpPath)
        SendKeys "{ENTER}"
        Wait 100
    End If
    
    Dim CommandsToBeSent(4) As String
    CommandsToBeSent(0) = Command1ToBeSent
    CommandsToBeSent(1) = Command2ToBeSent
    CommandsToBeSent(2) = Command3ToBeSent
    CommandsToBeSent(3) = Command4ToBeSent
    CommandsToBeSent(4) = Command5ToBeSent
    
    For I = 0 To UBound(CommandsToBeSent)
        If CommandsToBeSent(I) <> "" Then
            SendKeys (CommandsToBeSent(I))
            SendKeys "{ENTER}"
            Wait 100
        End If
    Next I
    
    Kill App.Path + "\Temp.bat"
End Sub
Public Sub Hide_All_Frames(frm As Form, Except As Frame)
    For Each Control In frm.Controls
        If TypeOf Control Is Frame Then
            If Control.Name <> Except.Name Then
                Control.Visible = False
            Else
                Control.Visible = True
            End If
        End If
    Next Control
End Sub
Public Sub LockAllTextBoxes(frm As Form, Lock_ As Boolean)
    For Each Control In frm.Controls
        If TypeOf Control Is TextBox Then
            If (Control.Name = "serviceRemovalDate" Or _
                Control.Name = "Response_TExt" Or _
                Control.Name = "Success" Or _
                Control.Name = "creditClearanceDate" Or _
                Control.Name = "Activation_Date" Or _
                Control.Name = "creditClearancePeriod" Or _
                Control.Name = "removalPeriod") And _
                frm.Name = "SulSAT2" Then
                'Nothing to Do
            Else
                Control.Enabled = Not Lock_  'Lock all text
            End If
        End If
    Next Control
End Sub
Public Sub ClearAllTextBoxes(frm As Form)
    If frm.Name = "SulSAT2" Then
        frm.Account_Flags = ""
        frm.Sub_Status = ""
        frm.Languages_Desc.Text = ""
        frm.Languages_ID.Text = ""
        frm.Service_Classes_Desc.Text = ""
        frm.Service_Classes_ID.Text = ""
    End If
    For Each Control In frm.Controls
        If TypeOf Control Is TextBox Then
            If (Control.Name = "Response_TExt" Or Control.Name = "Success") And frm.Name = "SulSAT2" Then
            Else
                Control.Text = ""     'Clear all text
            End If
        End If
    Next Control
End Sub
Public Function Sort(ByVal Array_ As Variant) As Variant
    Dim Sorted_Array(), Max As String
    Dim Max_Pos As Integer
    
    For I = 1 To UBound(Array_)
        'Getting max value and positioin
        For j = 1 To UBound(Array_)
            If Val(Max) <= Val(Array_(j)) Then
                Max = Val(Array_(j))
                Max_Pos = j
            End If
        Next j
        
        ReDim Preserve Sorted_Array(I)
        Sorted_Array(I) = Max
        Max = "0"
        Array_(Max_Pos) = 0
    Next I
    Sort = Sorted_Array
End Function

Public Sub Load_AIR_IPs(ComboBox_ As ComboBox)
    On Error GoTo IP_Loads
    Dim ErrNumber As Integer
    ErrNumber = Load_List(ComboBox_, "AIR_IPs")
    
IP_Loads:
    If ErrNumber = 53 Then
        Dim Continue As Integer
        Continue = MsgBox("Error Reading AIR_IPs File, Let the Application Selects the IP?", vbYesNo, "IP Missing")
        If Continue = 6 Then
            ComboBox_.AddItem "10.231.14.21", 0
            ComboBox_.Text = ComboBox_.List(0)
        End If
        Call Save_List(ComboBox_, "AIR_IPs")
    End If
End Sub

Public Sub Roll_Out_Msisdns(ByRef MSISDN As String)
    Main.MSISDNS = MSISDN
    UCIP.Combo_MSISDNs = MSISDN
    SulSAT.MSISDNs_List = MSISDN
End Sub
Public Function Numbers_Only(ByRef Number_String As String) As String
    For I = 1 To Len(Number_String)
        If Mid(Number_String, I, 1) = 0 Or _
                Mid(Number_String, I, 1) = 1 Or _
                Mid(Number_String, I, 1) = 2 Or _
                Mid(Number_String, I, 1) = 3 Or _
                Mid(Number_String, I, 1) = 4 Or _
                Mid(Number_String, I, 1) = 5 Or _
                Mid(Number_String, I, 1) = 6 Or _
                Mid(Number_String, I, 1) = 7 Or _
                Mid(Number_String, I, 1) = 8 Or _
                Mid(Number_String, I, 1) = 9 Then
            Numbers_Only = Numbers_Only + Mid(Number_String, I, 1)
        End If
    Next I
End Function

Public Sub Wait(ByVal dblMilliseconds As Double)
    Dim dblStart As Double
    Dim dblEnd As Double
    Dim dblTickCount As Double
    
    dblTickCount = GetTickCount()
    dblStart = GetTickCount()
    dblEnd = GetTickCount + dblMilliseconds
    
    Do
    DoEvents
    dblTickCount = GetTickCount()
    Loop Until dblTickCount > dblEnd Or dblTickCount < dblStart
       
    
End Sub


Public Sub Close_All()
    Call Move_Form(Main, "Right", 4635)
    Call Set_Time_Obj
    'Commands.Visible = False
    'VASP.Visible = False
    'Config.Visible = False
    'Get_Len_Form.Visible = False
    'Peri_Form.Visible = False
    'Favorite_File.Visible = False
    Call Extend("Up")
    Set_Wait_Time.Caption = " Set Wait Time"
    Set_Wait_Time.Visible = False
    Set_Wait_Time_Detailed.Caption = " Set Wait Time Detailed"
    Set_Wait_Time_Detailed.Visible = False
    'VASP_Types.Visible = False
    'Display_Form.Visible = False
    'Favorite_File.Visible = False
    'Organized.Visible = False
    'Users.Visible = False
    'Vmm_form.Visible = False
    'Verify_SDPs_Form.Visible = False
    'Vmm_form.Visible = False
    For I = 0 To Forms.Count - 1
        If Forms(I).Name <> "Main" And InStr(1, Forms(I).Caption, "Telnet Form") = 0 Then Forms(I).Visible = False
    Next I
    
    'Call Main.Extend_Button_Click
    
    'Main.Extend_Button.Value = False
    'UCIP.Visible = False
    'Extention.Visible = False
End Sub

Public Sub Set_Time_Obj()
    With Set_Wait_Time
        Select Case Set_Time_Object
            Case "TelNet"
                .Wait_Time = Wait_Telnet / 1000
            Case "VPN"
                Set_Wait_Time_Detailed.VPN_Wait_Time = Wait_VPN / 1000  'VPN
                Set_Wait_Time_Detailed.RSA_Wait_Time = Wait_RSA / 1000    'RSA
            Case "TEL_REDA"
                .Wait_Time = Wait_Reda / 1000
                .Path = Reda_Telnet_Path
                'MPS_Telnet_Path, , MPS_FTP_Path, Reda_FTP_Path
            Case "Tel_MPS"
                .Wait_Time = Wait_MPS / 1000
                .Path = MPS_Telnet_Path
            Case "FTP_Reda"
                .Wait_Time = Wait_Reda_FTP / 1000
                .Path = Reda_FTP_Path
            Case "FTP_MPS"
                .Wait_Time = Wait_MPS_FTP / 1000
                .Path = MPS_FTP_Path
            Case "MINSAT_L_Detailed"
                .Wait_Time = Wait_Minsat_Live / 1000
            Case "MINSAT_T_Detailed"
                .Wait_Time = Wait_MINSAT_Test / 1000
            Case "MINSAT"
                Set_Wait_Time_Detailed.Minsat_Wait_Time(0) = Wait_Minsat / 1000
        End Select
    End With
    Peri_Form.Exceed_Time_Command = Wait_Exceed / 1000
    Config.OMEGA_Wait_Time = Wait_OMEGA / 1000
    Config.VPN_Wait_Time = Wait_VPN / 1000
    Config.HLR_Wait_Time = Wait_HLR / 1000
    Config.Telnet_Wait_Time = Wait_Telnet / 1000
    Config.Minsat_t_Wait_Time = Wait_MINSAT_Test / 1000
    Config.Minsat_L_Wait_Time = Wait_Minsat_Live / 1000
    Config.Exceed_Wait_Time = Wait_Exceed / 1000
End Sub

Public Sub Close_Me(ME_ As Form)
    If Main.WindowState <> vbMinimized Then
        Call Slide(ME_, ME_.Left, ME_.Width, False)
        Call Move_Form(Main, "Right", 4635)
        Close_All
        Else
            'Main.WindowState = vbNormal
            'Call Close_Me(ME_)
    End If
End Sub
Public Sub Extend(Move_Direction As String)
    With Set_Wait_Time
        If Move_Direction = "Up" Then
            .Save.Top = 480
            .Height = 1275
            .Label3.Visible = False
            .Label4.Visible = False
            .Label5.Visible = False
            .Times.Visible = False
            .Path.Visible = False
        Else
            .Save.Top = 1200
            .Height = 2070
            .Label3.Visible = True
            .Label4.Visible = True
            .Label5.Visible = True
            .Times.Visible = True
            .Path.Visible = True
        End If
    End With
End Sub

Public Sub Cope_All(ByVal Case_ As String)
    Select Case Case_
        Case "Minimized"
            Commands.WindowState = 1
            Config.WindowState = 1
            Display_Form.WindowState = 1
            Favorite_File.WindowState = 1
            Favorite_File.WindowState = 1
            Get_Len_Form.WindowState = 1
            Organized.WindowState = 1
            Peri_Form.WindowState = 1
            Set_Wait_Time.WindowState = 1
            Set_Wait_Time_Detailed.WindowState = 1
            Start_Path.WindowState = 1
            VASP.WindowState = 1
            VASP_Types.WindowState = 1
        Case "Normal"
            Commands.WindowState = 0
            Config.WindowState = 0
            Display_Form.WindowState = 0
            Favorite_File.WindowState = 0
            Favorite_File.WindowState = 0
            Get_Len_Form.WindowState = 0
            Organized.WindowState = 0
            Peri_Form.WindowState = 0
            Set_Wait_Time.WindowState = 0
            Set_Wait_Time_Detailed.WindowState = 0
            Start_Path.WindowState = 0
            VASP.WindowState = 0
            VASP_Types.WindowState = 0
    End Select
End Sub

Public Sub Cope_Main(Form_ As Form)
    If Form_.WindowState = 0 Then Main.WindowState = 0
    If Form_.WindowState = 1 Then Main.WindowState = 1
End Sub

Public Function SortArray(ByRef TheArray As Variant)
    Dim Sorted As Boolean
    Sorted = False
    
    Do While Not Sorted
        Sorted = True
        For X = 0 To UBound(TheArray) - 1
            If Not (TheArray(X) = "" Or TheArray(X + 1) = "") Then
                If Mid(TheArray(X), Len(TheArray(X)) - 2, Len(TheArray(X))) > Mid(TheArray(X + 1), Len(TheArray(X + 1)) - 2, Len(TheArray(X + 1))) Then
                    Temp = TheArray(X + 1)
                    TheArray(X + 1) = TheArray(X)
                    TheArray(X) = Temp
                    Sorted = False
                End If
            End If
        Next X
    Loop
End Function

Public Function Trim_All(ByVal To_Be_Trimmed As String)
    Dim Result, Char_ As String
    
    For I = 1 To Len(To_Be_Trimmed)
        Char_ = Mid(To_Be_Trimmed, I, 1)
        If Not (Asc(Char_) = 13 Or Asc(Char_) = 32) Then Result = Result + Char_
    Next I
    Trim_All = Result
End Function
