VERSION 5.00
Begin VB.Form Commands 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Commands"
   ClientHeight    =   3195
   ClientLeft      =   6735
   ClientTop       =   3840
   ClientWidth     =   6030
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton FCD 
      Caption         =   "FCD"
      Height          =   255
      Left            =   840
      TabIndex        =   14
      Top             =   1440
      Width           =   615
   End
   Begin VB.OptionButton MPS 
      Caption         =   "MPS"
      Height          =   255
      Left            =   840
      TabIndex        =   13
      Top             =   1200
      Value           =   -1  'True
      Width           =   735
   End
   Begin VB.CommandButton On 
      Caption         =   "On"
      Height          =   495
      Left            =   1320
      TabIndex        =   12
      Top             =   1800
      Width           =   615
   End
   Begin VB.CommandButton Assign 
      Caption         =   "assign"
      Height          =   495
      Left            =   480
      TabIndex        =   11
      Top             =   1800
      Width           =   855
   End
   Begin VB.CommandButton Unassign 
      Caption         =   "Unassign"
      Height          =   495
      Left            =   4800
      TabIndex        =   10
      Top             =   1800
      Width           =   855
   End
   Begin VB.CommandButton Off 
      Caption         =   "Off"
      Height          =   495
      Left            =   4200
      TabIndex        =   9
      Top             =   1800
      Width           =   615
   End
   Begin VB.ComboBox App_Text 
      Height          =   315
      Left            =   2280
      TabIndex        =   8
      Text            =   "Combo1"
      Top             =   240
      Width           =   2655
   End
   Begin VB.CommandButton Status 
      Caption         =   "Status"
      Height          =   495
      Left            =   5160
      TabIndex        =   7
      Top             =   840
      Width           =   615
   End
   Begin VB.TextBox Command 
      Height          =   495
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   2520
      Width           =   5775
   End
   Begin VB.CommandButton OFF_UnAssign 
      Caption         =   "OFF and Unassign"
      Height          =   495
      Left            =   3120
      TabIndex        =   5
      Top             =   1800
      Width           =   1095
   End
   Begin VB.CommandButton ON_Assign 
      Caption         =   "ON And Assign"
      Height          =   495
      Left            =   1920
      TabIndex        =   4
      Top             =   1800
      Width           =   975
   End
   Begin VB.TextBox MPS_Text 
      Height          =   375
      Left            =   2280
      TabIndex        =   3
      Text            =   "MPS#"
      Top             =   1200
      Width           =   2655
   End
   Begin VB.TextBox Phones_Text 
      Height          =   375
      Left            =   2280
      TabIndex        =   2
      Text            =   "Lines"
      Top             =   720
      Width           =   2655
   End
   Begin VB.Label MPS_Label 
      Caption         =   "MPS #"
      Height          =   375
      Left            =   840
      TabIndex        =   15
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Phones_Label 
      Caption         =   "Phone Lines"
      Height          =   615
      Left            =   840
      TabIndex        =   1
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label App_Label 
      Caption         =   "Application Name"
      Height          =   495
      Left            =   840
      TabIndex        =   0
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "Commands"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Function Component_Name() As String
    If Me.MPS Then Component_Name = "mps"
    If Me.FCD Then Component_Name = "fcd"
End Function

Private Sub Assign_Click()
    'Command = "tappman -c #mps." + MPS_Text + " -p " + Phones_Text + " -a " + App_Text + " assign"
    Command = "tappman -c #" + Trim(Component_Name) + "." + MPS_Text + " -p " + Phones_Text + " -a " + App_Text + " assign"
End Sub

Private Sub FCD_Click()
    If Me.MPS Then Me.MPS_Text = "101"
    If Me.FCD Then Me.MPS_Text = "102"
End Sub

Private Sub Form_Load()
    Call Load_List(App_Text, "Apps")
End Sub

Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub

Private Sub Form_Terminate()
    Form_Unload (0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub MPS_Click()
    If Me.MPS Then Me.MPS_Text = "101"
    If Me.FCD Then Me.MPS_Text = "102"
End Sub

Private Sub MPS_Text_Click()
    MPS_Text = ""
End Sub

Private Sub Off_Click()
    Command = "tappman -c #" + Trim(Component_Name) + "." + MPS_Text + " -p " + Phones_Text + " off"
End Sub

Private Sub OFF_UnAssign_Click()
    If Commands.App_Label.Visible = True Then
        Command = "tappman -c #" + Trim(Component_Name) + "." + MPS_Text + " -p " + Phones_Text + " off unassign"
        Else
        Command = "block cic " + MPS_Text + " " + Phones_Text
        End If
End Sub

Private Sub ON_Assign_Click()
    If Commands.App_Label.Visible = True Then
        Command = "tappman -c #" + Trim(Component_Name) + "." + MPS_Text + " -p " + Phones_Text + " -a " + App_Text + " assign on"
        Else
        Command = "unblock cic " + MPS_Text + " " + Phones_Text
    End If
End Sub

Private Sub On_Click()
    Command = "tappman -c #" + Trim(Component_Name) + "." + MPS_Text + " -p " + Phones_Text + " -a " + App_Text + " on"
End Sub

Private Sub Phones_Text_Click()
    Phones_Text = ""
End Sub

Private Sub Status_Click()
    If Commands.App_Label.Visible = True Then
        Command = "srp -status"
        Else
        Command = "status cic " + MPS_Text + " " + Phones_Text + " " + "state"
        End If
End Sub


Private Sub App_Text_GotFocus()
    SendMessage App_Text.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub App_Text_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = App_Text.Text
        For I = 0 To App_Text.ListCount - 1
            If StrComp(PSTR, (Left(App_Text.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                App_Text.ListIndex = I
            Exit For
            End If
        Next I
        App_Text.SelStart = Len(PSTR)
        App_Text.SelLength = Len(App_Text.Text) - Len(PSTR)
    End If
End Sub

Private Sub App_Text_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(App_Text.Text) = 0) Then
        SendMessage App_Text.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage App_Text.hwnd, CB_SHOWDROPDOWN, 0, 1
    End If
    If KeyAscii = 32 And Len(App_Text.Text) = 0 Then KeyAscii = 0
    
End Sub

Private Sub App_Text_LostFocus()
    Call Save_List(App_Text, "Apps")
End Sub

Private Sub Unassign_Click()
    Command = "tappman -c #" + Trim(Component_Name) + "." + MPS_Text + " -p " + Phones_Text + " -a " + App_Text + " unassign"
End Sub
