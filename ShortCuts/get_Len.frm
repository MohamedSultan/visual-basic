VERSION 5.00
Begin VB.Form Get_Len_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Get Length"
   ClientHeight    =   8355
   ClientLeft      =   2640
   ClientTop       =   1185
   ClientWidth     =   7875
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8355
   ScaleWidth      =   7875
   Begin VB.Frame Length 
      Height          =   855
      Left            =   3120
      TabIndex        =   1
      Top             =   7440
      Visible         =   0   'False
      Width           =   1455
      Begin VB.Label Len_ 
         Height          =   375
         Left            =   840
         TabIndex        =   3
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "Length ="
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.TextBox Len_Text 
      Height          =   7215
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   240
      Width           =   7695
   End
End
Attribute VB_Name = "Get_Len_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub Len_Text_Change()
    Length.Visible = True
    Len_.Caption = Len(Len_Text)
End Sub

Private Sub Len_Text_Click()
    Len_Text = ""
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
