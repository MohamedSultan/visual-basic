VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form Config 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Config"
   ClientHeight    =   5955
   ClientLeft      =   3525
   ClientTop       =   2640
   ClientWidth     =   12480
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5955
   ScaleWidth      =   12480
   ShowInTaskbar   =   0   'False
   Begin VB.Frame PIN 
      Caption         =   "PIN"
      Height          =   4215
      Left            =   120
      TabIndex        =   45
      Top             =   120
      Width           =   4215
      Begin VB.TextBox AIR1_User 
         Height          =   285
         Left            =   2400
         TabIndex        =   70
         Text            =   "Text1"
         Top             =   1800
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox VPN_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   54
         Text            =   "1234"
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox OMEGA_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   53
         Text            =   "Sultan666"
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox MINSAT_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   52
         Text            =   "minsat"
         Top             =   1080
         Width           =   1335
      End
      Begin VB.TextBox MINSAT_L_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   51
         Text            =   "Test"
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox AIR1_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   50
         Text            =   "Text1"
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox TESTMPS_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   49
         Text            =   "Text1"
         Top             =   2160
         Width           =   1335
      End
      Begin VB.TextBox Reda_PIN 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1680
         PasswordChar    =   "*"
         TabIndex        =   48
         Text            =   "Text1"
         Top             =   2520
         Width           =   1335
      End
      Begin VB.TextBox TESTMPS_User 
         Height          =   285
         Left            =   2400
         TabIndex        =   47
         Text            =   "Text1"
         Top             =   2160
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox Reda_User 
         Height          =   285
         Left            =   2400
         TabIndex        =   46
         Text            =   "Text1"
         Top             =   2520
         Visible         =   0   'False
         Width           =   1335
      End
      Begin MSForms.ToggleButton AIR1_User_Window 
         Height          =   375
         Left            =   240
         TabIndex        =   69
         ToolTipText     =   "Click This Botton To Set The User Name"
         Top             =   1730
         Width           =   1335
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "2355;661"
         Value           =   "0"
         Caption         =   "AIR1"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
      End
      Begin MSForms.ToggleButton TestMPS_Window 
         Height          =   375
         Left            =   240
         TabIndex        =   68
         ToolTipText     =   "Click This Botton To Set The User Name"
         Top             =   2100
         Width           =   1335
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "2355;661"
         Value           =   "0"
         Caption         =   "Test MPS"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
      End
      Begin VB.Label MINSAT_T_Label 
         Caption         =   "MINSAT Test"
         Height          =   375
         Left            =   240
         TabIndex        =   65
         Top             =   1080
         Width           =   1095
      End
      Begin MSForms.ToggleButton Reda_Window 
         Height          =   375
         Left            =   240
         TabIndex        =   55
         ToolTipText     =   "Click This Botton To Set The User Name"
         Top             =   2475
         Width           =   1335
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "2355;661"
         Value           =   "0"
         Caption         =   "Reda's "
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
      End
      Begin VB.Label VPN_Label 
         Caption         =   "VPN"
         Height          =   375
         Left            =   240
         TabIndex        =   67
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label OMEGA_Label 
         Caption         =   "OMEGA"
         Height          =   495
         Index           =   0
         Left            =   240
         TabIndex        =   66
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label MINSAT_L_Label 
         Caption         =   "MINSAT Live"
         Height          =   375
         Left            =   240
         TabIndex        =   64
         Top             =   1440
         Width           =   1095
      End
      Begin MSForms.ToggleButton Star_OMEGA 
         Height          =   375
         Left            =   3120
         TabIndex        =   63
         Top             =   680
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton Star_MINSAT_T 
         Height          =   375
         Left            =   3120
         TabIndex        =   62
         Top             =   1040
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton Star_MINSAT_L 
         Height          =   375
         Left            =   3120
         TabIndex        =   61
         Top             =   1400
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton Star_VPN 
         Height          =   375
         Left            =   3120
         TabIndex        =   60
         Top             =   320
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label AIR1_Label 
         Caption         =   "AIR1"
         Height          =   375
         Left            =   240
         TabIndex        =   59
         Top             =   1800
         Width           =   1215
      End
      Begin MSForms.ToggleButton Star_AIR1 
         Height          =   375
         Left            =   3120
         TabIndex        =   58
         Top             =   1760
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton Star_TestMPS 
         Height          =   375
         Left            =   3120
         TabIndex        =   57
         Top             =   2120
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin MSForms.ToggleButton Star_Reda 
         Height          =   375
         Left            =   3120
         TabIndex        =   56
         Top             =   2480
         Width           =   975
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   6
         Size            =   "1720;661"
         Value           =   "0"
         Caption         =   "Show PIN"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
   End
   Begin VB.TextBox MINSAT_Path 
      Height          =   285
      Left            =   6240
      TabIndex        =   38
      Text            =   "D:\program files\minsat"
      Top             =   2520
      Width           =   2175
   End
   Begin VB.CommandButton Extend 
      Caption         =   ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      Height          =   5775
      Left            =   8760
      TabIndex        =   36
      ToolTipText     =   "Extend           "
      Top             =   120
      Width           =   255
   End
   Begin VB.Frame Frame1 
      Caption         =   "Wait Time"
      Height          =   2775
      Left            =   9120
      TabIndex        =   21
      Top             =   0
      Width           =   2895
      Begin VB.TextBox VPN_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   35
         Text            =   "Text1"
         Top             =   2400
         Width           =   1335
      End
      Begin VB.TextBox Exceed_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   32
         Text            =   "Text6"
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Minsat_L_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   31
         Text            =   "Text5"
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox Minsat_t_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   30
         Text            =   "Text4"
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox Telnet_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   29
         Text            =   "Text3"
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox HLR_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   28
         Text            =   "Text2"
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox OMEGA_Wait_Time 
         Height          =   285
         Left            =   1320
         TabIndex        =   27
         Text            =   "Text1"
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "VPN"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "Exceed"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "MINSAT Live"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "MINSAT Test"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Telnet"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "HLR"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "OMEGA"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Other 
      Caption         =   "Other"
      Height          =   735
      Left            =   9120
      TabIndex        =   12
      Top             =   2880
      Width           =   2895
      Begin VB.TextBox VPN_IP 
         Height          =   285
         Left            =   1320
         TabIndex        =   14
         Text            =   "10.240.22.29"
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label VPN_IP_Label 
         Caption         =   "VPN IP"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.TextBox Conf_File_Loc 
      Height          =   405
      Left            =   240
      TabIndex        =   19
      Text            =   "D:\Work\My Tools\ShortCuts"
      Top             =   4920
      Width           =   2775
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save+Close"
      Height          =   495
      Left            =   3600
      TabIndex        =   18
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton Apply 
      Caption         =   "Apply"
      Height          =   495
      Left            =   4680
      TabIndex        =   7
      Top             =   4920
      Width           =   1215
   End
   Begin VB.Frame Path 
      Caption         =   "Path"
      Height          =   4215
      Left            =   4440
      TabIndex        =   0
      Top             =   120
      Width           =   4215
      Begin VB.TextBox Exceed_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   44
         Text            =   "D:\program files\minsat"
         Top             =   2760
         Width           =   2175
      End
      Begin VB.TextBox RSA_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   43
         Text            =   "D:\program files\minsat"
         Top             =   3120
         Width           =   2175
      End
      Begin VB.TextBox VPN_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   42
         Text            =   "D:\program files\minsat"
         Top             =   3480
         Width           =   2175
      End
      Begin VB.TextBox Telnet_Reda_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   16
         Text            =   "D:\Work\My Tools"
         Top             =   1680
         Width           =   2175
      End
      Begin VB.TextBox FTP_MPS_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   11
         Text            =   "D:\Work\My Tools"
         Top             =   1320
         Width           =   2175
      End
      Begin VB.TextBox MPS_Connect_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   10
         Text            =   "D:\Work\My Tools"
         Top             =   960
         Width           =   2175
      End
      Begin VB.TextBox HLR_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   6
         Text            =   "D:\Work\HLR Tools"
         Top             =   600
         Width           =   2175
      End
      Begin VB.TextBox FTP_Reda_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   5
         Text            =   "D:\Work\My Tools"
         Top             =   2040
         Width           =   2175
      End
      Begin VB.TextBox OMEGA_Path 
         Height          =   285
         Left            =   1800
         TabIndex        =   4
         Text            =   "C:\Omega"
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Minsat_Label 
         Caption         =   "RSA Secure ID"
         Height          =   375
         Index           =   3
         Left            =   240
         TabIndex        =   41
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label Minsat_Label 
         Caption         =   "Cisco VPN"
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   40
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label Minsat_Label 
         Caption         =   "Exceed"
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   39
         Top             =   2760
         Width           =   1215
      End
      Begin VB.Label Minsat_Label 
         Caption         =   "MINSAT"
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   37
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label Telnet_Reda_Label 
         Caption         =   "Telnet Reda"
         Height          =   375
         Left            =   240
         TabIndex        =   15
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label FTP_MPS_Label 
         Caption         =   "FTP MPS"
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label MPS_Label 
         Caption         =   "MPS Connect"
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label FTP_Reda_Label 
         Caption         =   "FTP Reda"
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label HLR_Label 
         Caption         =   "HLR Guide Path"
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label OMEGA_Label 
         Caption         =   "OMEGA Path"
         Height          =   495
         Index           =   1
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Label Conf_File_Loc_Label 
      Caption         =   "Configuration File Location"
      Height          =   495
      Left            =   240
      TabIndex        =   20
      Top             =   4440
      Width           =   1215
   End
   Begin VB.Label Conf_Loc 
      Height          =   495
      Left            =   360
      TabIndex        =   17
      Top             =   5280
      Width           =   3615
   End
End
Attribute VB_Name = "Config"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub AIR1_User_Window_Click()
    Select Case AIR1_User_Window.Value
        Case True
            Reda_Window.Value = False
            TestMPS_Window.Value = False
            Users.Show
            Users.Caption = "Test AIR1 User Name"
            Users.User = Config.AIR1_User
        Case False
            Users.Hide
    End Select
End Sub

Public Sub Apply_Click()

    Wait_MINSAT_Test = Val(Minsat_t_Wait_Time) * 1000
    Wait_Minsat_Live = Val(Minsat_L_Wait_Time) * 1000
    Wait_Telnet = Val(Telnet_Wait_Time) * 1000
    Wait_HLR = Val(HLR_Wait_Time) * 1000
    Wait_VPN = Val(VPN_Wait_Time) * 1000
    Wait_OMEGA = Val(OMEGA_Wait_Time) * 1000
    Wait_Exceed = Val(Exceed_Wait_Time) * 1000

    Save_Conf
    Call Load_Conf
    
    Apply.Enabled = False
    Conf_Loc.Caption = "Saved in: " + Conf_File_Loc + "\Configuration.txt  ..."
    

End Sub





Private Sub Extend_Click()
    'Close_All
    If Right(Extend.Caption, 1) = ">" Then
        Call Move_Form(Main, "Left", 500)
        Call Move_Form(Config, "Left", Main.Left + Main.Width)
        Call Slide(Me, Config.Left, 12570, True)
        Extend.Left = 12120
        Extend.Caption = "<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    Else
        Call Slide(Me, Config.Left, Config.Width, False)
        Me.Visible = True
        Me.Width = 9180
        Extend.Left = 8760
        Extend.Caption = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        Call Move_Form(Main, "Right", 1400)
        Call Slide(Config, Main.Left + Main.Width, 9170, True)
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 35 Then
        Conf_File_Loc_Label.Visible = True
        Conf_File_Loc.Visible = True
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub Path_Click()
    Conf_Loc.Visible = False
End Sub

Private Sub Reda_Window_Click()
    Select Case Reda_Window.Value
        Case True
            TestMPS_Window.Value = False
            AIR1_User_Window.Value = False
            Users.Show
            Users.Caption = "Reda's Machine User Name"
            Users.User = Config.Reda_User
        Case False
            Users.Hide
    End Select
End Sub

Private Sub Save_Click()
    Apply_Click
    Form_Unload (0)
End Sub

Private Sub Star_AIR1_Click()
    With AIR1_PIN
        If Star_AIR1.Value = True Then
            .PasswordChar = ""
            Else
            .PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Star_MINSAT_L_Click()
    With Star_MINSAT_L
        If .Value = True Then
            MINSAT_L_PIN.PasswordChar = ""
            Else
            MINSAT_L_PIN.PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Star_MINSAT_T_Click()
    With Star_MINSAT_T
        If .Value = True Then
            MINSAT_PIN.PasswordChar = ""
            Else
            MINSAT_PIN.PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Star_OMEGA_Click()
    With Star_OMEGA
        If .Value = True Then
            OMEGA_PIN.PasswordChar = ""
            Else
            OMEGA_PIN.PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Star_Reda_Click()
    With Reda_PIN
        If Star_Reda.Value = True Then
            .PasswordChar = ""
            Else
            .PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Star_TestMPS_Click()
    With TESTMPS_PIN
        If Star_TestMPS.Value = True Then
            .PasswordChar = ""
            Else
            .PasswordChar = "*"
        End If
    End With
End Sub

Private Sub Star_VPN_Click()
    With Star_VPN
        If .Value = True Then
            VPN_PIN.PasswordChar = ""
            Else
            VPN_PIN.PasswordChar = "*"
        End If
    End With
End Sub



Private Sub TestMPS_Window_Click()
    Select Case TestMPS_Window.Value
        Case True
            Reda_Window.Value = False
            AIR1_User_Window.Value = False
            Users.Show
            Users.Caption = "TestMPS User Name"
            Users.User = Config.TESTMPS_User
        Case False
            Users.Hide
    End Select
End Sub


Private Sub ToggleButton1_Click()

End Sub
