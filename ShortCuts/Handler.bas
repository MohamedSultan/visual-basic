Attribute VB_Name = "Handler"
Public Sub Applications_Handler()

    Dim Retry As Boolean
    
    On Error GoTo LocalHandler
    
    Select Case Application_Wait
        Case "VPN Dialer Authentication"
            If VPN_Conf_Form.VPN_Version(0) Then        'V3
                If VPN_Conf_Form.GSM_Remote.Value = True Then AppActivate ("User Authentication for Remote GSM")
                If VPN_Conf_Form.GSM.Value = True Then AppActivate ("User Authentication for GSM")
                If VPN_Conf_Form.My_Work.Value = True Then AppActivate ("User Authentication for My Work")
            End If
            If VPN_Conf_Form.VPN_Version(1) Then        'V4
                If VPN_Conf_Form.GSM_Remote.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""Remote GSM""")
                'If VPN_Conf_Form.GSM.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""GSM""")
                If VPN_Conf_Form.GSM.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""GSM Local""")
                If VPN_Conf_Form.My_Work.Value = True Then AppActivate ("VPN Client  |  User Authentication for ""My Work""")
            End If
            Wait 500
            SendKeys "^V"
            SendKeys "{ENTER}"
        Case "VPNdialer"
            AppActivate ("Cisco Systems VPN Client")
            Wait 1000
            SendKeys "{ENTER}"
            'Wait 1000
            'SendKeys "^V"
            'Wait 1000
            'SendKeys "{ENTER}"
            'Wait 1000
        Case "Exceed"
                
'                On Error GoTo Exceed_Handler
'            x = IsProcessRunning("XDMCP Display ")
'            Do Until Not IsProcessRunning("XDMCP")
                AppActivate ("XDMCP")
                
                Wait Wait_Exceed
                'SendKeys "m"
                'SendKeys "m"
                SendKeys "{TAB}"
                SendKeys "{TAB}"
                SendKeys "{TAB}"
                SendKeys "{ENTER}"
'            Loop
            
            
        Case "HLR"
            'Wait  Wait_HLR
            Wait 300    'Separation
            
            SendKeys "{ENTER}"
            AppActivate ("HLR Guide")
            SendKeys Main.MSISDNS.Text
            SendKeys "{ENTER}"
            Wait 500

        Case "MINSAT Log In"
            AppActivate ("Log In")
 
            Progress_Form.ProgressBar1.Value = Progress_Form.ProgressBar1.Max
 
            AppActivate ("Log In")
            AppActivate ("Log In")
            
            SendKeys MINSAT_UserName
            SendKeys "{TAB}"
            Wait 500
            SendKeys MINSAT_Password
            SendKeys "{ENTER}"
            Wait 500
            
            Progressing (Int(Minsat_Wait_Time / 1000))
                
            If MINSAT_Check = 1 Then
                Application_Wait = "MINSAT 5.1"
                Applications_Handler
            End If
            
        Case "MINSAT 5.1"
                AppActivate ("MINSAT 5.1")
                AppActivate ("MINSAT 5.1")
                AppActivate ("MINSAT 5.1")
                SendKeys "{ENTER}"
                Wait 500
                SendKeys "20"
                SendKeys Main.MSISDNS.Text
                SendKeys "{ENTER}"
                Main.All_Timer.Enabled = False
                
        Case "Omega"
            If Omega_Conf_Form.Do_Nothing.Value = 0 Then
                If Omega_Conf_Form.Yes.Value = 1 Then    'to Elluminate the press OK prompt
                    SendKeys "{ENTER}"
                Else
                    SendKeys "{TAB}"
                    SendKeys "{ENTER}"
                End If
            End If
        
            AppActivate ("Central User Authentication")
            'SendKeys "{TAB}"
            'SendKeys "{TAB}"
            SendKeys Config.OMEGA_PIN
            SendKeys "{ENTER}"
            Wait 500
            Progressing (Int(Wait_OMEGA / 1000))
        End Select
            
Application_Wait = ""
Main.All_Timer.Enabled = False
            
LocalHandler:
            Main.All_Timer.Enabled = True
            
'Exceed_Handler:
'            If Err.Number = 5 Then
'            x = 1
End Sub

Public Sub Progressing(ByVal Time_ As Integer)
            If Time_ > 0 Then
                Progress_Form.ProgressBar1.Max = Time_
                Progress_Form.Time_Elapsed = 0
                'Progress_Form.Visible = True
                Call Slide(Progress_Form, Progress_Form.Left, 7815, True)
                Progress_Form.Timer1.Enabled = True
            End If
End Sub
