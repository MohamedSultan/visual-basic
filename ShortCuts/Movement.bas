Attribute VB_Name = "Movement"
Dim Wait_ As Double
Public Sub Show_ProgressBar(ByVal ProgressBar_Max As Integer)
    Progress_Form.ProgressBar1.Max = ProgressBar_Max / 1000
    Progress_Form.Time_Elapsed = 0
    'Progress_Form.Visible = True
    'Call Slide(Progress_Form, Progress_Form.Left, 7815, True)
    Progress_Form.Timer1.Enabled = True
End Sub

Public Sub Move_Form(Form_ As Form, Direction As String, End_ As Integer, Optional Speed As Double)
    
    If Speed = 0 Then Speed = 1
    Dim Direction_ As String
    Direction_ = LCase(Direction)
    Wait_ = 0
    Select Case Direction_
    Case "left"
        Do While Not Form_.Left <= End_
            Form_.Left = Form_.Left - 75 * Speed
'            Wait Wait_
             '4350
        Loop
    Case "right"
        Do While Not Form_.Left >= End_
            Form_.Left = Form_.Left + 75 * Speed
'            Wait Wait_
        Loop
    End Select
End Sub

Public Sub Slide(ByRef Form_ As Form, Start_ As Integer, ByVal Width_ As Integer, Open_ As Boolean, Optional Speed As Double)
    If Speed = 0 Then Speed = 1
    Wait_ = 0
    Dim Proceed As Integer
    Proceed = 1
    
    'SulSat_Dimensions.Sliding = True
    
    If Form_.WindowState <> vbNormal Then
        Form_.Visible = True
        Form_.WindowState = vbNormal
    End If
    If Open_ = True Then
        Form_.Left = Start_
        Form_.Width = 0
        Form_.Visible = True
        Do While Form_.Width <= Width_
            Form_.Width = Form_.Width + 75 * Speed
'            Wait Wait_
        Loop
        Else
            Form_.Left = Start_
            Form_.Width = Width_
            Do While Form_.Width > 1685 And Proceed = 1
                If Form_.Width <= 1685 Then
                    Proceed = 0
                    Else
                        Form_.Width = Form_.Width - 75 * Speed
                End If
                
                If Form_.Width = 1845 Then Proceed = 0
 '               Wait Wait_
            Loop
            Form_.Width = 0
            Form_.Hide
            Form_.Width = Width_
    End If
    
    'SulSat_Dimensions.Sliding = False
End Sub
