Attribute VB_Name = "IP_Address"
Private Declare Function GetIpAddrTable_API Lib "IpHlpApi" Alias "GetIpAddrTable" (pIPAddrTable As Any, pdwSize As Long, ByVal bOrder As Long) As Long

' Returns an array with the local IP addresses (as strings).
' Author: Christian d'Heureuse, www.source-code.biz
Private Function GetIpAddrTable()
   Dim Buf(0 To 511) As Byte
   Dim BufSize As Long: BufSize = UBound(Buf) + 1
   Dim rc As Long
   rc = GetIpAddrTable_API(Buf(0), BufSize, 1)
   If rc <> 0 Then Err.Raise vbObjectError, , "GetIpAddrTable failed with return value " & rc
   Dim NrOfEntries As Integer: NrOfEntries = Buf(1) * 256 + Buf(0)
   If NrOfEntries = 0 Then GetIpAddrTable = Array(): Exit Function
   ReDim IpAddrs(0 To NrOfEntries - 1) As String
   Dim I As Integer
   For I = 0 To NrOfEntries - 1
      Dim j As Integer, s As String: s = ""
      For j = 0 To 3: s = s & IIf(j > 0, ".", "") & Buf(4 + I * 24 + j): Next
      IpAddrs(I) = s
      Next
   GetIpAddrTable = IpAddrs
   End Function

Public Function Get_IP_Address() As String
        Dim IpAddrs
        Dim Local_IP As String
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        IpAddrs = GetIpAddrTable
        'Debug.Print "Nr of IP addresses: " & UBound(IpAddrs) - LBound(IpAddrs) + 1
        Dim I As Integer
        
            'For I = LBound(IpAddrs) To UBound(IpAddrs)
            ''Debug.Print IpAddrs(i)
            '    If Mid(IpAddrs(I), 1, 2) = "10" And IpAddrs(I) <> Config.VPN_IP Then Local_IP = IpAddrs(I)
            'Next I
        
        Get_IP_Address = Local_IP
        End If
End Function

Public Function Check_Connectivity() As Integer     'returns 1 if connected & 0 of not connected
    Dim Local_IP As String
    Local_IP = SulSAT.Winsock1.LocalIP                'this is an another way of getting Local IP address
    If Mid(Local_IP, 1, 3) = "127" Then             'No connection
        Check_Connectivity = 0
        Else
        Check_Connectivity = 1
    End If
End Function
