VERSION 5.00
Begin VB.Form Stored_Procedure_types1 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Stored Procedure Types"
   ClientHeight    =   4335
   ClientLeft      =   11850
   ClientTop       =   5115
   ClientWidth     =   3975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   3975
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Height          =   15
      Left            =   120
      TabIndex        =   13
      Top             =   2330
      Width           =   3495
   End
   Begin VB.Frame Frame2 
      Height          =   15
      Left            =   120
      TabIndex        =   12
      Top             =   1250
      Width           =   3495
   End
   Begin VB.Frame Frame1 
      Height          =   3135
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   3495
      Begin VB.OptionButton Option1 
         Caption         =   "FAF"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   11
         Top             =   1800
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Update Usage"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   10
         Top             =   2040
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   9
         Top             =   2280
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Get STATUS"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Update MSISDN"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   7
         Top             =   960
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Insert MSISDN"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   6
         Top             =   1200
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Truncate Table"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   5
         Top             =   1440
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Get Usage"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Usage"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   2775
      End
   End
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1200
      TabIndex        =   1
      Top             =   3840
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "PLZ Select the Stored Procedure U want to test"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
   End
End
Attribute VB_Name = "Stored_Procedure_types1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Terminate()
    Display_Form.Visible = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub OK_Click()
    Stored_Procedure.Body = "Sorry There is no Stored Procedure for this request"

    For I = 0 To Option1.UBound
        If Option1(I).Value = True Then Fill_Stored_Procedure (Option1(I).Caption)
    Next I
    
    Main.SetFocus
    Me.SetFocus
    
    If Option1(2).Value = False Then
        Stored_Procedure.SetFocus
    Else
        FAF.SetFocus
    End If
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub

Private Sub Fill_Stored_Procedure(Stored_Procedure_Name As String)
    Close_All
    Stored_Procedure_types1.Visible = True
    Main2.Visible = True
    
    If Stored_Procedure_Name = "FAF" Then
        Call Slide(FAF, Main.Left, FAF.Width, True)
        Else
        Call Slide(Stored_Procedure, Main.Left + Main.Width, Stored_Procedure.Width, True)
    End If
        
    Select Case Stored_Procedure_Name
        Case "Truncate Table"
            Stored_Procedure.Headder = "sqlplus base/base@Base_Promo"
            Stored_Procedure.Body = "truncate table postpromo drop storage;"
        Case "Reset Usage"
            Stored_Procedure.Headder = "sqlplus vpfee/vpfee@VPFEE"
            Stored_Procedure.Body = "set serveroutput on;" + vbCrLf + _
                                    "declare MSISDN varchar2(10);Result varchar(2);" + vbCrLf + _
                                    "begin" + vbCrLf + _
                                    "MSISDN:='" + Main.MSISDNS.Text + "';" + vbCrLf + _
                                    "RESETSUBSCRIBERUSAGE(MSISDN,Result);" + vbCrLf + _
                                    "dbms_output.put_line(result);" + vbCrLf + _
                                    "end;" + vbCrLf + _
                                    "/" + vbCrLf
        Case "Update Usage"
            Stored_Procedure.Headder = "sqlplus vpfee/vpfee@VPFEE"
            Stored_Procedure.Body = "update subscriber_usage set SERVICE01_USAGE=15 where msisdn='" + Main.MSISDNS.Text + "';" + vbCrLf + _
                                    "commit;" + vbCrLf + _
                                    "/" + vbCrLf
                                    
        Case "Get Usage"
            Stored_Procedure.Headder = "sqlplus vpfee/vpfee@VPFEE"
            Stored_Procedure.Body = "set serveroutput on;" + vbCrLf + _
                                    "declare  MSISDN Varchar2(20); SERVICE_USAGE VARCHAR2(20);FEE VARCHAR2(20) ;LAST_SUPERVISION VARCHAR2(20); STATUS VARCHAR(20);" + vbCrLf + _
                                    "begin" + vbCrLf + _
                                    "MSISDN := '" + Main.MSISDNS.Text + "';" + vbCrLf + _
                                    "VPFEE.GETSUBSCRIBERUSAGE (MSISDN ,SERVICE_USAGE, FEE ,LAST_SUPERVISION, STATUS);" + vbCrLf + _
                                    "dbms_output.put_line('MSISDN '|| MSISDN);" + vbCrLf + _
                                    "dbms_output.put_line('SERVICE_USAGE '|| SERVICE_USAGE);" + vbCrLf + _
                                    "dbms_output.put_line('FEE '|| FEE);" + vbCrLf + _
                                    "dbms_output.put_line('LAST_SUPERVISION '|| LAST_SUPERVISION);" + vbCrLf + _
                                    "dbms_output.put_line('STATUS '|| STATUS);" + vbCrLf + _
                                    "end;" + vbCrLf + _
                                    "/" + vbCrLf
        Case "Get STATUS"
            Stored_Procedure.Headder = "sqlplus base/base@Base_Promo"
            Stored_Procedure.Body = "set serveroutput on;" + vbCrLf + _
                                    "declare MSISDN varchar2(12);BTYPE varchar2(1);BSTATUS varchar2(1);Result varchar2(4);" + vbCrLf + _
                                    "begin" + vbCrLf + _
                                    "MSISDN:='20" + Main.MSISDNS.Text + "';" + vbCrLf + _
                                    "BTYPE:='1';" + vbCrLf + _
                                    "GETSTATUS(MSISDN,BTYPE,BSTATUS,Result);" + vbCrLf + _
                                    "dbms_output.put_line(Result);" + vbCrLf + _
                                    "dbms_output.put_line(BSTATUS);" + vbCrLf + _
                                    "end;" + vbCrLf + _
                                    "/" + vbCrLf
        Case "Update MSISDN"
            Stored_Procedure.Headder = "sqlplus base/base@Base_Promo"
            Stored_Procedure.Body = "set serveroutput on;" + vbCrLf + _
                                    "declare MSISDN varchar2(11);BTYPE varchar2(1);BSTATUS varchar2(1);Result varchar2(4);" + vbCrLf + _
                                    "begin" + vbCrLf + _
                                    "MSISDN:='20" + Main.MSISDNS.Text + "';" + vbCrLf + _
                                    "BTYPE:='1';" + vbCrLf + _
                                    "BSTATUS:='F';" + vbCrLf + _
                                    "UPDATESTATUS (MSISDN,BTYPE,BSTATUS,Result);" + vbCrLf + _
                                    "dbms_output.put_line(Result);" + vbCrLf + _
                                    "end;" + vbCrLf + _
                                    "/" + vbCrLf

        Case "Insert MSISDN"
            Stored_Procedure.Headder = "sqlplus base/base@Base_Promo"
            Stored_Procedure.Body = "set serveroutput on;" + vbCrLf + _
                                    "declare MSISDN varchar2(11);BTYPE varchar2(1);BSTATUS varchar2(1);Result varchar2(4);" + vbCrLf + _
                                    "begin" + vbCrLf + _
                                    "MSISDN:='20" + Main.MSISDNS.Text + "';" + vbCrLf + _
                                    "BTYPE:='1';" + vbCrLf + _
                                    "BSTATUS:='N';" + vbCrLf + _
                                    "INSERTMSISDN(MSISDN,BTYPE,BSTATUS,Result);" + vbCrLf + _
                                    "dbms_output.put_line(Result);" + vbCrLf + _
                                    "end;" + vbCrLf + _
                                    "/" + vbCrLf

    End Select
End Sub
