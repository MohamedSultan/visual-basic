Attribute VB_Name = "Combos"
Public Sub Save_List(Combo_Name As ComboBox, File_Name As String, Optional NumbersOnly As Boolean)
    
    If NumbersOnly Then Combo_Name.Text = Numbers_Only(Combo_Name.Text)
    Dim found_in_list As Boolean
    Dim Index_S As String
    found_in_list = False
    
    For i = 0 To Combo_Name.ListCount
        If Combo_Name.Text = Combo_Name.List(i) Then found_in_list = True
        Next i

    If found_in_list = False Then
        Combo_Name.AddItem Combo_Name.Text, 0
        Combo_Name.Text = Combo_Name.List(0)
    End If
    
    Dim O_File_Name As Long
    Dim Index As Integer
    O_File_Name = FreeFile
                Open App.Path + "\" + File_Name + ".txt" For Output As O_File_Name
                
                    Print #O_File_Name, Combo_Name.Text + "," + "000"
                    i = 0
                    Index = 1
                    While i <= Combo_Name.ListCount
                        
                        If Index <= 9 Then Index_S = Trim_All("00" + Str(Index))
                        If Index >= 10 And i <= 99 Then Index_S = Trim_All("0" + Str(Index))
                        If Index >= 100 Then Index_S = Trim_All(Str(Index))
                    
                        If Not (Combo_Name.List(i) = Combo_Name.Text Or Combo_Name.List(i) = "") Then
                            Print #O_File_Name, Combo_Name.List(i) + "," + Index_S
                            Index = Index + 1
                        End If
                        i = i + 1
                    Wend
                        
                Close O_File_Name

End Sub
                        
Public Function Load_List(ByRef Combo_Name As ComboBox, File_Name As String) As Integer

    I_File_Name = FreeFile
    Dim File_List() As String
    Step = 0
    Combo_Name.Clear
    
    On Error GoTo Error_Handler_File_Name

    Open App.Path + "\" + File_Name + ".txt" For Input As I_File_Name

        Do Until EOF(I_File_Name)
            ReDim Preserve File_List(Step)
            Line Input #I_File_Name, File_List(Step)
            If File_List(Step) <> "" Then
                Step = Step + 1
            End If
        Loop
    Close I_File_Name
    
    ReDim Combo_List(UBound(File_List))
    For i = 0 To UBound(File_List)
        If File_List(i) <> "" Then Combo_Name.AddItem Mid(File_List(i), 1, Len(File_List(i)) - 4)
    Next i
            
    Combo_Name.Text = Combo_Name.List(0)
    Load_List = Err.Number

Error_Handler_File_Name:
    If Err.Number = 53 Then
        O_File_Name = FreeFile
        Open App.Path + "\" + File_Name + ".txt" For Output As O_File_Name
        Close O_File_Name
        Load_List = Err.Number
    End If
End Function
