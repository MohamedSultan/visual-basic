Attribute VB_Name = "Variables"
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Declare Function GetWindowPlacement Lib "user32" (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Public Declare Function SetWindowPlacement Lib "user32" (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Public Declare Function GetTickCount Lib "kernel32" () As Long

Public Const CB_SHOWDROPDOWN = &H14F
Public PSTR As String
Public Type Organized_Txt       ' Used in the Oragnized Form to get and store data
    Title As String
    Data As String
End Type
Public Type POINTAPI
    X As Long
    Y As Long
End Type
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
Public Type WINDOWPLACEMENT
    Length As Long
    Flags As Long
    showCmd As Long
    ptMinPosition As POINTAPI
    ptMaxPosition As POINTAPI
    rcNormalPosition As RECT
End Type
Enum VNPP_Index
    CallCollect = 0
    USSD = 1
    SDP = 2
    OMN = 3
End Enum

Public Set_Time_Object As String
Public Wait_OMEGA, Wait_RSA, Wait_VPN, Wait_HLR As Integer
Public Wait_Minsat, Wait_Minsat_Live, Wait_MINSAT_Test As String
Public Wait_Reda, Wait_MPS, Wait_Reda_FTP, Wait_MPS_FTP, Wait_Exceed, Wait_Telnet As Integer
Public MPS_Telnet_Path, Reda_Telnet_Path, MPS_FTP_Path, Reda_FTP_Path As String
Public PeriPro_Last_Path As String
Public MINSAT_UserName, MINSAT_Password As String
Public Minsat_Wait_Time, MINSAT_Check As Integer
Public PeriStudio_Path As String
Public Minsat_Test_UserName, Minsat_Live_UserName As String
Public IP_Address As String
Public Favorites_Txt() As String
'Public VASP_Types.Width, Display_Form.Width, Vmm_form.Width, Stored_Procedure.width, _
        Verify_SDPs_Form.Width, Prefix_Form.Width, Stored_Procedure_types1.Width, _
        UCIP.Width As Integer
Public Application_Wait As String  ' Used as a label to identify which application has 2 wait
Public Current_PageNumber As Integer
Public NumberOfDedicatedAccounts As Integer
Public Number_Of_Active_Telnet_Connections As Integer
Public Active_Telnet_Form(), Organized_Form() As Form
Public Organized_Command() As Organized_Txt
