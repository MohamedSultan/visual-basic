VERSION 5.00
Begin VB.Form VASP 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Commands"
   ClientHeight    =   4320
   ClientLeft      =   9960
   ClientTop       =   3210
   ClientWidth     =   5880
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4320
   ScaleWidth      =   5880
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Verify_SDPs 
      Caption         =   "Verify SDPs"
      Height          =   495
      Left            =   3000
      TabIndex        =   14
      ToolTipText     =   "It Gives Analysis on SC5 (How many Creadit clearence =90, how many=30, how many Else and how many not SC5)"
      Top             =   3480
      Width           =   615
   End
   Begin VB.CommandButton MMFs_Command 
      Caption         =   "MMFs"
      Height          =   495
      Left            =   3600
      TabIndex        =   13
      Top             =   3480
      Width           =   855
   End
   Begin VB.CommandButton VASP_Tests 
      Caption         =   "VASP Tests"
      Height          =   495
      Left            =   4440
      TabIndex        =   12
      Top             =   3480
      Width           =   975
   End
   Begin VB.CommandButton Snoop 
      Caption         =   "snoop"
      Height          =   495
      Left            =   4440
      TabIndex        =   11
      Top             =   3000
      Width           =   975
   End
   Begin VB.CommandButton Minsat_Batch 
      Caption         =   "Minsat Batch"
      Height          =   495
      Left            =   3600
      TabIndex        =   10
      Top             =   3000
      Width           =   855
   End
   Begin VB.CommandButton JAVA 
      Caption         =   "JAVA"
      Height          =   495
      Left            =   3000
      TabIndex        =   9
      Top             =   3000
      Width           =   615
   End
   Begin VB.CommandButton Gen_Command 
      Caption         =   "gen.cfg Change"
      Height          =   495
      Left            =   2280
      TabIndex        =   8
      Top             =   3000
      Width           =   735
   End
   Begin VB.TextBox Command 
      Height          =   375
      Index           =   3
      Left            =   120
      TabIndex        =   5
      Top             =   2400
      Width           =   5655
   End
   Begin VB.TextBox Command 
      Height          =   375
      Index           =   2
      Left            =   120
      TabIndex        =   4
      Top             =   1920
      Width           =   5655
   End
   Begin VB.TextBox Command 
      Height          =   375
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   5655
   End
   Begin VB.TextBox Command 
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   360
      Width           =   5655
   End
   Begin VB.CommandButton AIR_Change_Command 
      Caption         =   "LiveAIR/TestAIR  Switch"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   3000
      Width           =   1455
   End
   Begin VB.CommandButton VASP_Chng_Command 
      Caption         =   "  VASP Change"
      Height          =   495
      Left            =   1560
      TabIndex        =   0
      Top             =   3000
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Detailed"
      Height          =   255
      Left            =   2520
      TabIndex        =   7
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "All"
      Height          =   255
      Left            =   2640
      TabIndex        =   6
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "VASP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub AIR_Change_Command_Click()
    Call Close_Child_Forms
    Command(0) = "vi /opt/vps/PERIxmlrpc/etc/xmlrpc.config"
    Command(1) = "cd /opt/vps/PERIxmlrpc/etc/"
    Command(2) = "vi  xmlrpc.config"
    Command(3) = "/opt/vps/PERIxmlrpc/etc/Vengine"
    Show_Commands (3)
End Sub

Private Sub Form_Load()
VASP_Types.Width = VASP_Types.Width

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Close_Me(Me)
End Sub

Private Sub Gen_Command_Click()
    Call Close_Child_Forms
    Command(0) = "vi /opt/vps/common/etc/gen.cfg"
    Command(1) = "cd /opt/vps/common/etc"
    Command(2) = "vi gen.cfg"
    Show_Commands (2)
End Sub

Private Sub JAVA_Click()
    Call Close_Child_Forms
    Command(0) = "vi /opt/vps/PERIxmlrpc/startjsb.csh*"
    Command(1) = "cd /opt/vps/PERIjsb"
    Command(2) = "/opt/jdk1.5.0_07/bin/java"
    Command(3) = "/opt/vps/PERIxmlrpc/kpalkilljsb.sh"
    Show_Commands (3)
End Sub

Private Sub Minsat_Batch_Click()
    Call Close_Child_Forms
    Command(0) = "setenv DISPLAY " + Config.VPN_IP + ":0" + ";/opt/minsatcs/java/java -jar sagui.jar"
    Command(1) = "cd /opt/minsatcs/java"
    Command(3) = "java -jar sagui.jar&"
    Command(2) = "setenv DISPLAY " + Config.VPN_IP + ":0"
    Show_Commands (3)
End Sub

Private Sub MMFs_Command_Click()
    Call Close_Child_Forms
    Call Show_Commands(-1)
    Call Slide(Vmm_form, Main.Left + Main.Width, Vmm_form.Width, True)
End Sub

Private Sub Snoop_Click()
    Call Close_Child_Forms
    Command(0) = "snoop �d hme0 �xV �t a TAIR1 and TAIR2 port 10010"
    Show_Commands (0)
End Sub

Private Sub VASP_Chng_Command_Click()
    Call Close_Child_Forms
    Command(0) = "vi /opt/vps/common/etc/VASP_Server"
    Command(1) = "cd /opt/vps/common/etc"
    Command(2) = "vi VASP_Server"
    Show_Commands (2)
End Sub

Private Sub VASP_Tests_Click()
    Call Close_Child_Forms
    Call Show_Commands(-1)
    Call Slide(VASP_Types, _
                Main.Left + Main.Width + VASP_Tests.Left + VASP_Tests.Width, _
                VASP_Types.Width, True)
End Sub

Private Sub Hide_all()
    For I = 0 To 3
        Command(I).Visible = False
    Next I
End Sub

Public Sub Show_Commands(Num_of_Commands_To_Display As Integer)
    Call Hide_all
    
    If Num_of_Commands_To_Display = 0 Then
        Label2.Visible = False
        Label1.Visible = True
        Else
            Label2.Visible = True
            Label1.Visible = True
    End If
    For I = 0 To Num_of_Commands_To_Display
        Command(I).Visible = True
    Next I
    If Num_of_Commands_To_Display = -1 Then
        Command(0).Visible = False
        Label1.Visible = False
        Label2.Visible = False
    End If
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
Private Sub Close_Child_Forms()
    If VASP_Types.Visible = True Then Call Slide(VASP_Types, VASP_Types.Left, VASP_Types.Width, False)
    If Vmm_form.Visible = True Then Call Slide(Vmm_form, Vmm_form.Left, Vmm_form.Width, False)
    If Display_Form.Visible = True Then Call Slide(Display_Form, Display_Form.Left, Display_Form.Width, False)
End Sub

Private Sub Verify_SDPs_Click()
    Close_All
    Call Slide(Verify_SDPs_Form, Main.Left + Main.Width, Verify_SDPs_Form.Width, True)
End Sub
