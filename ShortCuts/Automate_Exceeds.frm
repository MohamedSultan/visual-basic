VERSION 5.00
Begin VB.Form Automate_Exceeds 
   Caption         =   "Automate Exceeds"
   ClientHeight    =   2535
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8955
   LinkTopic       =   "Form1"
   ScaleHeight     =   2535
   ScaleWidth      =   8955
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox IP_Address 
      Height          =   315
      Left            =   3360
      Style           =   1  'Simple Combo
      TabIndex        =   13
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Save 
      Caption         =   "Add"
      Height          =   375
      Left            =   1680
      TabIndex        =   12
      Top             =   2040
      Width           =   1335
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   375
      Left            =   4320
      TabIndex        =   11
      Top             =   2040
      Width           =   1335
   End
   Begin VB.ComboBox Command3 
      Height          =   315
      Left            =   5040
      Style           =   1  'Simple Combo
      TabIndex        =   10
      Top             =   1200
      Width           =   3735
   End
   Begin VB.ComboBox Command1 
      Enabled         =   0   'False
      Height          =   315
      Left            =   5040
      Style           =   1  'Simple Combo
      TabIndex        =   9
      Top             =   480
      Width           =   3735
   End
   Begin VB.ComboBox Command2 
      Height          =   315
      Left            =   5040
      Style           =   1  'Simple Combo
      TabIndex        =   8
      Top             =   840
      Width           =   3735
   End
   Begin VB.ComboBox Command4 
      Height          =   315
      Left            =   5040
      Style           =   1  'Simple Combo
      TabIndex        =   7
      Top             =   1560
      Width           =   3735
   End
   Begin VB.ComboBox Password 
      Height          =   315
      Left            =   3360
      Style           =   1  'Simple Combo
      TabIndex        =   6
      Top             =   1320
      Width           =   1455
   End
   Begin VB.ComboBox User_Name 
      Height          =   315
      Left            =   3360
      Style           =   1  'Simple Combo
      TabIndex        =   5
      Top             =   960
      Width           =   1455
   End
   Begin VB.ComboBox Command 
      Height          =   315
      Left            =   240
      TabIndex        =   4
      Top             =   960
      Width           =   1815
   End
   Begin VB.Label Label5 
      Caption         =   "IP Address"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2280
      TabIndex        =   14
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Command"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   720
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Command(s)"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5400
      TabIndex        =   2
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2280
      TabIndex        =   1
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "User Name"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2280
      TabIndex        =   0
      Top             =   960
      Width           =   975
   End
End
Attribute VB_Name = "Automate_Exceeds"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command_click()
    Call Change_Combos(Command)
    If Left(IP_Address.Text, 3) = "172" Then
        Me.Command1 = "setenv DISPLAY " + Config.VPN_IP + ":0.0"
    Else
        X = Get_IP_Address
        Me.Command1 = "setenv DISPLAY " + X + ":0.0"
    End If
End Sub
Private Sub Change_Combos(Except As ComboBox)
    If Except.ListIndex <= Command.ListCount - 1 Then
        If Except.Name <> "Command" Then _
            Me.Command.ListIndex = Except.ListIndex
        If Except.Name <> "User_Name" Then _
            Me.User_Name.ListIndex = Except.ListIndex
        If Except.Name <> "Password" Then _
            Me.Password.ListIndex = Except.ListIndex
        If Except.Name <> "IP_Address" Then _
            Me.IP_Address.ListIndex = Except.ListIndex
        If Except.Name <> "Command2" Then _
            Me.Command2.ListIndex = Except.ListIndex
        If Except.Name <> "Command3" Then _
            Me.Command3.ListIndex = Except.ListIndex
        If Except.Name <> "Command4" Then _
            Me.Command4.ListIndex = Except.ListIndex
    End If
End Sub
Private Sub Form_Load()
    Dim I_Commands As Double
    I_Commands = FreeFile
    Dim Command_Input As String
    Dim Command_Input_Parser As CCPAI_Parser_Return
    On Error GoTo error_Handler_Configuration
    Open Config.Conf_File_Loc + "\Commands.txt" For Input As I_Commands
        Do Until EOF(I_Commands)
            Line Input #I_Commands, Command_Input
            Command_Input_Parser = Parser(Command_Input, ",")
            If Command_Input_Parser.found Then
                Me.IP_Address.AddItem Command_Input_Parser.Ret(0)
                Me.Command.AddItem Command_Input_Parser.Ret(1)
                Me.User_Name.AddItem Command_Input_Parser.Ret(2)
                Me.Password.AddItem Command_Input_Parser.Ret(3)
                'Me.Command1.AddItem Command_Input_Parser.Ret(4)
                Me.Command2.AddItem Command_Input_Parser.Ret(4)
                Me.Command3.AddItem Command_Input_Parser.Ret(5)
                Me.Command4.AddItem Command_Input_Parser.Ret(6)
            End If
        Loop
    Close I_Commands

error_Handler_Configuration:
    If Err.Number = 53 Then
        O_Commands = FreeFile
        Open Config.Conf_File_Loc + "\Commands.txt" For Output As O_Commands
        Close O_Commands
    End If
End Sub

Private Sub Save_Click()
    Dim O_Commands As Double
    O_Commands = FreeFile
    Dim found As Boolean
    For I = 0 To Me.Command.ListCount - 1
        If Me.Command.Text = Me.Command.List(I) Then found = True
    Next I
    
    If Not found Then
        If Command2.Text = "" Then Command2.Text = " "
        If Command3.Text = "" Then Command3.Text = " "
        If Command4.Text = "" Then Command4.Text = " "
        
        Me.Command.AddItem Me.Command.Text
        Me.IP_Address.AddItem Me.IP_Address.Text
        Me.User_Name.AddItem Me.User_Name.Text
        Me.Password.AddItem Me.Password.Text
        Me.Command2.AddItem Me.Command2.Text
        Me.Command3.AddItem Me.Command3.Text
        Me.Command4.AddItem Me.Command4.Text
        
        Open Config.Conf_File_Loc + "\Commands.txt" For Output As O_Commands
            For I = 0 To Me.Command.ListCount - 1
                Print #O_Commands, Me.IP_Address.List(I) & _
                                    "," & Me.Command.List(I) & _
                                    "," & User_Name.List(I) & _
                                    "," & Password.List(I) & _
                                    "," & Command2.List(I) & _
                                    "," & Command3.List(I) & _
                                    "," & Command4.List(I) & ","
            Next I
        Close O_Commands
    End If
End Sub

Private Sub Start_Click()
    Main.Call_Exceed
    Call Main.Create_Telnet_Form(Me.IP_Address.Text, Me.User_Name.Text, Me.Password.Text, "", , _
                                                            "setenv DISPLAY " + Config.VPN_IP + ":0.0", _
                                                            Me.Command2.Text, _
                                                            Me.Command3.Text, _
                                                            Me.Command4.Text)
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Close_ = "Terminate"
    Call Slide(Me, Me.Left, Me.Width, False)
End Sub
