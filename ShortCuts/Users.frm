VERSION 5.00
Begin VB.Form Users 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "User"
   ClientHeight    =   1245
   ClientLeft      =   8850
   ClientTop       =   4515
   ClientWidth     =   3540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1245
   ScaleWidth      =   3540
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox User 
      Height          =   285
      Left            =   1320
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   240
      Width           =   1695
   End
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   375
      Left            =   960
      TabIndex        =   1
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "UserName"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "Users"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Terminate()
    Config.TestMPS_Window.Value = False
    Config.Reda_Window.Value = False
    Config.AIR1_User_Window.Value = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub OK_Click()
    If LCase(Mid(Me.Caption, 1, 5)) = "testm" Then Config.testmps_user = User
    If LCase(Mid(Me.Caption, 1, 4)) = "reda" Then Config.reda_user = User
    If LCase(Mid(Me.Caption, 1, 5)) = "test " Then Config.air1_user = User
    Call Form_Terminate
End Sub
Private Sub Form_Resize()
    If Me.WindowState = 0 Then Call Cope_Main(Me)
End Sub
