Attribute VB_Name = "Batches"
Dim FTP_Reda_Path_Conf, OMEGA_Path_Conf, HLR_Path_Conf, MPS_Connect_Path_Conf, FTP_MPS_Path_Conf, Telnet_Reda_Path_Conf As String
Dim VPN_PIN, OMEGA_PIN, MINSAT_PIN, MINSAT_Live_PIN, AIR1_PIN, TESTMPS_PIN, Reda_PIN As String
Dim VPN_IP  As String
Dim I_MSISDNS, I_Configuration As Long
Dim Wait_OMEGA_S, Wait_RSA_S, Wait_VPN_S, Wait_Telnet_S, Wait_Exceed_S As String
Dim Wait_Minsat_S, Wait_Minsat_Live_S, Wait_MINSAT_Test_S As String
Dim Wait_Reda_S, Wait_MPS_S, Wait_Reda_FTP_S, Wait_MPS_FTP_S As String
Dim Minsat_Live_Load_MSISDN, Minsat_Test_Load_MSISDN As String
Dim Temp As String

Public Sub Load_Conf()
I_Configuration = FreeFile
Dim Minsat_L_UserName, Minsat_T_UserName As String
Dim minsat_path_, exceed_path_, rsa_path_, vpn_path_ As String
Dim testmps_user_, reda_user_, air1_user_ As String
Dim Wait_HLR_S As String

'On Error GoTo error_Handler_Configuration

Dim Conf_File_Loc As String
    'Load Configurations
    Open Config.Conf_File_Loc + "\Configuration.txt" For Input As I_Configuration
        Do Until EOF(I_Configuration)
                
                Line Input #I_Configuration, Temp   'Configuration Path
                Line Input #I_Configuration, Temp   '------------------
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, Conf_File_Loc
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, Temp   'Applications Path
                Line Input #I_Configuration, Temp   '-----------------
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, FTP_Reda_Path_Conf
                Line Input #I_Configuration, OMEGA_Path_Conf
                Line Input #I_Configuration, HLR_Path_Conf
                Line Input #I_Configuration, MPS_Connect_Path_Conf
                Line Input #I_Configuration, FTP_MPS_Path_Conf
                Line Input #I_Configuration, Telnet_Reda_Path_Conf
                Line Input #I_Configuration, minsat_path_
                Line Input #I_Configuration, exceed_path_
                Line Input #I_Configuration, rsa_path_
                Line Input #I_Configuration, vpn_path_
                
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp   'Applications PIN
                Line Input #I_Configuration, Temp   '----------------
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, VPN_PIN
                Line Input #I_Configuration, OMEGA_PIN
                Line Input #I_Configuration, MINSAT_PIN
                Line Input #I_Configuration, MINSAT_Live_PIN
                Line Input #I_Configuration, air1_user_
                Line Input #I_Configuration, AIR1_PIN
                Line Input #I_Configuration, testmps_user_
                Line Input #I_Configuration, TESTMPS_PIN
                Line Input #I_Configuration, reda_user_
                Line Input #I_Configuration, Reda_PIN
                
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp   'VPN IP
                Line Input #I_Configuration, Temp   '------
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, VPN_IP
                
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp   'Minsat Users
                Line Input #I_Configuration, Temp   '------------
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, Minsat_L_UserName
                Line Input #I_Configuration, Minsat_T_UserName
                
                
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, Wait_OMEGA_S
                Line Input #I_Configuration, Wait_VPN_S
                Line Input #I_Configuration, Wait_HLR_S
                Line Input #I_Configuration, Wait_Minsat_S
                Line Input #I_Configuration, Wait_Minsat_Live_S
                Line Input #I_Configuration, Minsat_Live_Load_MSISDN
                Line Input #I_Configuration, Wait_MINSAT_Test_S
                Line Input #I_Configuration, Minsat_Test_Load_MSISDN
                Line Input #I_Configuration, Wait_Telnet_S
                Line Input #I_Configuration, Wait_Exceed_S
                Line Input #I_Configuration, Wait_RSA_S
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, Wait_Reda_S
                Line Input #I_Configuration, Wait_MPS_S
                Line Input #I_Configuration, Wait_Reda_FTP_S
                Line Input #I_Configuration, Wait_MPS_FTP_S
                
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp
                Line Input #I_Configuration, Temp
                
                Line Input #I_Configuration, MPS_Telnet_Path
                Line Input #I_Configuration, Reda_Telnet_Path
                Line Input #I_Configuration, MPS_FTP_Path
                Line Input #I_Configuration, Reda_FTP_Path
        Loop
    Close I_Configuration
        
    With Config
        .Conf_File_Loc = Conf_File_Loc
        .FTP_Reda_Path = FTP_Reda_Path_Conf
        .OMEGA_Path = OMEGA_Path_Conf
        .HLR_Path = HLR_Path_Conf
        .MPS_Connect_Path = MPS_Connect_Path_Conf
        .FTP_MPS_Path = FTP_MPS_Path_Conf
        .Telnet_Reda_Path = Telnet_Reda_Path_Conf
        .MINSAT_L_PIN = MINSAT_Live_PIN
        .MINSAT_Path = minsat_path_
        .Exceed_Path = exceed_path_
        .RSA_Path = rsa_path_
        .VPN_Path = vpn_path_
    
        .VPN_PIN = Decrypt(VPN_PIN)
        .OMEGA_PIN = Decrypt(OMEGA_PIN)
        .MINSAT_PIN = Decrypt(MINSAT_PIN)
        .MINSAT_L_PIN = Decrypt(MINSAT_Live_PIN)
        .AIR1_User = Decrypt(air1_user_)
        .AIR1_PIN = Decrypt(AIR1_PIN)
        .TESTMPS_User = Decrypt(testmps_user_)
        .TESTMPS_PIN = Decrypt(TESTMPS_PIN)
        .Reda_User = Decrypt(reda_user_)
        .Reda_PIN = Decrypt(Reda_PIN)
          
        .VPN_IP = VPN_IP
    End With
    
    Wait_OMEGA = Val(Mid(Wait_OMEGA_S, 23, Len(Wait_OMEGA_S))) * 1000
    Wait_VPN = Val(Mid(Wait_VPN_S, 23, Len(Wait_VPN_S))) * 1000
    Wait_HLR = Val(Mid(Wait_HLR_S, 23, Len(Wait_HLR_S))) * 1000
    Wait_Minsat = Val(Mid(Wait_Minsat_S, 23, Len(Wait_Minsat_S))) * 1000
    Wait_Minsat_Live = Val(Mid(Wait_Minsat_Live_S, 23, Len(Wait_Minsat_Live_S))) * 1000
    Wait_MINSAT_Test = Val(Mid(Wait_MINSAT_Test_S, 23, Len(Wait_MINSAT_Test_S))) * 1000
    Wait_Telnet = Val(Mid(Wait_Telnet_S, 23, Len(Wait_Telnet_S))) * 1000
    Wait_Exceed = Val(Mid(Wait_Exceed_S, 23, Len(Wait_Exceed_S))) * 1000
    Wait_RSA = Val(Mid(Wait_RSA_S, 23, Len(Wait_RSA_S))) * 1000
    
    Wait_Reda = Val(Mid(Wait_Reda_S, 23, Len(Wait_Reda_S))) * 1000
    Wait_MPS = Val(Mid(Wait_MPS_S, 23, Len(Wait_MPS_S))) * 1000
    Wait_Reda_FTP = Val(Mid(Wait_Reda_FTP_S, 23, Len(Wait_Reda_FTP_S))) * 1000
    Wait_MPS_FTP = Val(Mid(Wait_MPS_FTP_S, 23, Len(Wait_MPS_FTP_S))) * 1000
    
    Set_Wait_Time_Detailed.MSISDN(0).Value = Val(Mid(Minsat_Live_Load_MSISDN, 23, Len(Minsat_Live_Load_MSISDN)))
    Set_Wait_Time_Detailed.MSISDN(1).Value = Val(Mid(Minsat_Test_Load_MSISDN, 23, Len(Minsat_Test_Load_MSISDN)))
    
    Peri_Form.Exceed_Time_Command = Wait_Exceed / 1000
    
    MPS_Telnet_Path = Mid(MPS_Telnet_Path, 18, Len(MPS_Telnet_Path))
    Reda_Telnet_Path = Mid(Reda_Telnet_Path, 18, Len(Reda_Telnet_Path))
    MPS_FTP_Path = Mid(MPS_FTP_Path, 18, Len(MPS_FTP_Path))
    Reda_FTP_Path = Mid(Reda_FTP_Path, 18, Len(Reda_FTP_Path))
    
    With Set_Wait_Time_Detailed
        .MINSAT_Live_Password(0) = Config.MINSAT_L_PIN
        .MINSAT_Live_Password(1) = Config.MINSAT_PIN
        .Wait_MSISDN(0) = Wait_Minsat_Live / 1000
        .Wait_MSISDN(1) = Wait_MINSAT_Test / 1000
        .RSA_Wait_Time = Wait_RSA / 1000
        .VPN_Wait_Time = Wait_VPN / 1000
        .Minsat_User_Name(0) = Mid(Minsat_L_UserName, 14, Len(Minsat_L_UserName))
        .Minsat_User_Name(1) = Mid(Minsat_T_UserName, 14, Len(Minsat_T_UserName))
    End With
    
error_Handler_Configuration:
    If Err.Number = 53 Then
        O_configuration = FreeFile
        Open Config.Conf_File_Loc + "\Configuration.txt" For Output As O_configuration
        Close O_configuration
    End If
End Sub
Public Sub Save_All()
    Call Save_Conf
    Call Save_List(Peri_Form.Paths_List, "Paths")
End Sub

Public Sub Save_Conf()
    Dim O_Configurations As Long
    O_Configurations = FreeFile
    
                Open Config.Conf_File_Loc + "\Configuration.txt" For Output As O_Configurations
                    
                     Print #O_Configurations, "Configuration Path"
                     Print #O_Configurations, "------------------"
                     Print #O_Configurations, ""
                     Print #O_Configurations, Config.Conf_File_Loc
                     Print #O_Configurations, ""
                
                     Print #O_Configurations, "Applications Path"
                     Print #O_Configurations, "-----------------"
                     Print #O_Configurations, ""
                     
                     Print #O_Configurations, Config.FTP_Reda_Path
                     Print #O_Configurations, Config.OMEGA_Path
                     Print #O_Configurations, Config.HLR_Path
                     Print #O_Configurations, Config.MPS_Connect_Path
                     Print #O_Configurations, Config.FTP_MPS_Path
                     Print #O_Configurations, Config.Telnet_Reda_Path
                     Print #O_Configurations, Config.MINSAT_Path
                     Print #O_Configurations, Config.Exceed_Path
                     Print #O_Configurations, Config.RSA_Path
                     Print #O_Configurations, Config.VPN_Path
                     
                     Print #O_Configurations, ""
                     Print #O_Configurations, "Applications PIN"
                     Print #O_Configurations, "----------------"
                     Print #O_Configurations, ""
                     
                     Print #O_Configurations, Encrypt(Config.VPN_PIN)
                     Print #O_Configurations, Encrypt(Config.OMEGA_PIN)
                     Print #O_Configurations, Encrypt(Config.MINSAT_PIN)
                     Print #O_Configurations, Encrypt(Config.MINSAT_L_PIN)
                     Print #O_Configurations, Encrypt(Config.AIR1_User)
                     Print #O_Configurations, Encrypt(Config.AIR1_PIN)
                     Print #O_Configurations, Encrypt(Config.TESTMPS_User)
                     Print #O_Configurations, Encrypt(Config.TESTMPS_PIN)
                     Print #O_Configurations, Encrypt(Config.Reda_User)
                     Print #O_Configurations, Encrypt(Config.Reda_PIN)
                     
                     Print #O_Configurations, ""
                     Print #O_Configurations, "VPN IP"
                     Print #O_Configurations, "------"
                     Print #O_Configurations, ""
                     
                     Print #O_Configurations, Config.VPN_IP
                     
                     Print #O_Configurations, ""
                     Print #O_Configurations, "Minsat Users"
                     Print #O_Configurations, "------------"
                     Print #O_Configurations, ""
                     Print #O_Configurations, "Minsat Live: " + Set_Wait_Time_Detailed.Minsat_User_Name(0)
                     Print #O_Configurations, "Minsat Test: " + Set_Wait_Time_Detailed.Minsat_User_Name(1)
                     
                     
                     Print #O_Configurations, ""
                     Print #O_Configurations, "Applications Wait Time"
                     Print #O_Configurations, "-----------------------"
                     Print #O_Configurations, ""
                     
                     Print #O_Configurations, "OMEGA Wait Time      =" + Str(Wait_OMEGA / 1000)
                     Print #O_Configurations, "VPN Wait Time        =" + Str(Wait_VPN / 1000)
                     Print #O_Configurations, "HLR Wait Time        =" + Str(Wait_HLR / 1000)
                     Print #O_Configurations, "MINSAT Wait Time     =" + Str(Wait_Minsat / 1000)
                     Print #O_Configurations, "MINSAT Live Wait Time=" + Str(Wait_Minsat_Live / 1000)
                     Print #O_Configurations, "MINSAT L Load MSISDN =" + Str(Set_Wait_Time_Detailed.MSISDN(0).Value)
                     Print #O_Configurations, "MINSAT Test Wait Time=" + Str(Wait_MINSAT_Test / 1000)
                     Print #O_Configurations, "MINSAT T Load MSISDN =" + Str(Set_Wait_Time_Detailed.MSISDN(1).Value)
                     Print #O_Configurations, "Telnet Wait Time     =" + Str(Wait_Telnet / 1000)
                     Print #O_Configurations, "Exceed Wait Time     =" + Str(Wait_Exceed / 1000)
                     Print #O_Configurations, "RSA Wait Time        =" + Str(Wait_RSA / 1000)
                     Print #O_Configurations, ""
                     Print #O_Configurations, "Telnet REDA Wait Time=" + Str(Wait_Reda / 1000)
                     Print #O_Configurations, "Telnet MPS Wait Time =" + Str(Wait_MPS / 1000)
                     Print #O_Configurations, "FTP Reda Wait Time   =" + Str(Wait_Reda_FTP / 1000)
                     Print #O_Configurations, "FTP MPS Wait Time    =" + Str(Wait_MPS_FTP / 1000)
                     
                     Print #O_Configurations, ""
                     Print #O_Configurations, "Paths"
                     Print #O_Configurations, "-----"
                     Print #O_Configurations, ""
                     
                     Print #O_Configurations, "Telnet MPS Path :" + MPS_Telnet_Path
                     Print #O_Configurations, "Telnet Reda Path:" + Reda_Telnet_Path
                     Print #O_Configurations, "FTP MPS Path    :" + MPS_FTP_Path
                     Print #O_Configurations, "FTP Reda Path   :" + Reda_FTP_Path
                Close O_Configurations
End Sub

Public Sub Save_Last_Paths()
    Dim O_Paths As Long
    O_Paths = FreeFile
    
                Open Config.Conf_File_Loc + "\Last_Paths.txt" For Output As O_Paths
                    
                     Print #O_Paths, "PeriPro Path"
                     Print #O_Paths, "------------"
                     Print #O_Paths, PeriPro_Last_Path
                     Print #O_Paths, ""
                     
                     Print #O_Paths, "Peri Studio Path"
                     Print #O_Paths, "----------------"
                     Print #O_Paths, PeriStudio_Path
                     Print #O_Paths, ""
                     
                     Print #O_Paths, "TestMPS Telnet Path"
                     Print #O_Paths, "-------------------"
                     Print #O_Paths, MPS_Telnet_Path
                     Print #O_Paths, ""
                                          
                     Print #O_Paths, "TestMPS FTP Path"
                     Print #O_Paths, "----------------"
                     Print #O_Paths, MPS_FTP_Path
                     Print #O_Paths, ""
                     
                     Print #O_Paths, "Reda's Telnet Path"
                     Print #O_Paths, "----------------"
                     Print #O_Paths, Reda_Telnet_Path
                     Print #O_Paths, ""
                     
                     Print #O_Paths, "Reda's FTP Path"
                     Print #O_Paths, "---------------"
                     Print #O_Paths, Reda_FTP_Path
                     Print #O_Paths, ""
                     
                     Print #O_Paths, "SDP Verify File Path"
                     Print #O_Paths, "--------------------"
                     Print #O_Paths, Verify_SDPs_Form.File_Loc
                     Print #O_Paths, ""
                     
                     Print #O_Paths, "SDP Verify Folder Path"
                     Print #O_Paths, "----------------------"
                     Print #O_Paths, Verify_SDPs_Form.Folder
                     Print #O_Paths, ""
                Close O_Paths
                     
End Sub

Public Sub Load_Last_Paths()
    Dim I_Paths As Long
    Dim Temp, Folder_S, File_S As String
    I_Paths = FreeFile
    
    On Error GoTo Error_Handler_Path
    
    Open Config.Conf_File_Loc + "\Last_Paths.txt" For Input As I_Paths
        Do Until EOF(I_Paths)
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, PeriPro_Last_Path
            Line Input #I_Paths, Temp
            
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, PeriStudio_Path
            Line Input #I_Paths, Temp

            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, MPS_Telnet_Path
            Line Input #I_Paths, Temp

            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, MPS_FTP_Path
            Line Input #I_Paths, Temp

            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Reda_Telnet_Path
            Line Input #I_Paths, Temp
            
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Reda_FTP_Path
            Line Input #I_Paths, Temp
            
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, File_S
            Line Input #I_Paths, Temp
            
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Temp
            Line Input #I_Paths, Folder_S
            Line Input #I_Paths, Temp
            
        Loop
    Close I_Paths
    Verify_SDPs_Form.File_Loc = File_S
    Verify_SDPs_Form.Folder = Folder_S

Error_Handler_Path:
    If Err.Number = 53 Then
        Dim O_Paths As Long
        O_Paths = FreeFile
        Open Config.Conf_File_Loc + "\Last_Paths.txt" For Output As O_Paths
        Close O_Paths
    End If
End Sub
