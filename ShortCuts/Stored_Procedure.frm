VERSION 5.00
Begin VB.Form Stored_Procedure 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Stored Procedure"
   ClientHeight    =   4950
   ClientLeft      =   2265
   ClientTop       =   705
   ClientWidth     =   6000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   6000
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Copy 
      Caption         =   "Copy"
      Height          =   375
      Left            =   2400
      TabIndex        =   2
      Top             =   4440
      Width           =   1095
   End
   Begin VB.TextBox Headder 
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Text            =   "----------------------------------------------------------------------------------"
      Top             =   360
      Width           =   5535
   End
   Begin VB.TextBox Body 
      Height          =   3255
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   960
      Width           =   5535
   End
End
Attribute VB_Name = "Stored_Procedure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Copy_Click()
    Clipboard.SetText Me.Body
End Sub
