Attribute VB_Name = "Account_History"
Type History_Type
    TransactionDate As String
    TransactionType As String
    OriginNodeID As String
    originTransactionID As String
    NewLanguageCode As String
    originNodeType As String
    Status As String
    SupervisionPeriodExpiryDate As String
    supervisionPeriod As String
    ServiceFeePeriodExpiryDate As String
    serviceFeePeriod As String
    CreditClearencePeriod As String
    ServiceRemovalPeriod As String
    GMT As String
    Balance As String
    TransactionAmountC1 As String
    AccountBalanceC1 As String
    System As String
    User As String
    CardProfile As String
    Ded5AccountBalanceC1 As String
    Ded5TransactionAmountC1 As String
    externalData1 As String
    externalData2 As String
    serviceClass As String
End Type
Public Function History_Type_details_Fill() As History_Type
    With History_Type_details_Fill
        .TransactionDate = "Transaction Date"
        .TransactionType = "Transaction Type"
        .OriginNodeID = "Origin Node ID"
        .originTransactionID = "origin Transaction ID"
        .NewLanguageCode = "New Language Code"
        .originNodeType = "originNode Type"
        .Status = "Status"
        .SupervisionPeriodExpiryDate = "Supervision Period Expiry Date"
        .supervisionPeriod = "Supervision Period"
        .ServiceFeePeriodExpiryDate = "Service Fee Period Expiry Date"
        .serviceFeePeriod = "Service Fee Period"
        .CreditClearencePeriod = "Credit Clearence Period"
        .ServiceRemovalPeriod = "Service Removal Period"
        .GMT = "GMT"
        .Balance = "Balance"
        .TransactionAmountC1 = "Transaction Amount"
        .AccountBalanceC1 = "Account Balance"
        .System = "System"
        .User = "User"
        .CardProfile = "Card Profile"
        .Ded5AccountBalanceC1 = "Ded5 Account Balance"
        .Ded5TransactionAmountC1 = "Ded5 Transaction Amount"
        .externalData1 = "External Data 1"
        .externalData2 = "External Data 2"
        .serviceClass = "Service Class"
    End With
End Function
Private Sub Fill_Date_Tranasaction(ByVal Row_ID As Integer, ByRef Data As History_Type)
    With SulSAT2
        .History_Time_List.AddItem Data.TransactionDate
        .History_Transaction_Type_List.AddItem Data.TransactionType
    End With
End Sub
Public Sub History_Display(History_Records As Variant)
    'Naming Fileds
    Dim History As History_Type
    On Error GoTo Local_Handler
    For records = 1 To UBound(History_Records)
        With History
            .TransactionDate = Date_Time(History_Records(records)(0))
            .TransactionType = History_Records(records)(1)
            .OriginNodeID = History_Records(records)(10)
            .originTransactionID = History_Records(records)(11)
            Select Case History_Records(records)(1)
                Case "Language Change"
                    .NewLanguageCode = History_Records(records)(12)
                    .originNodeType = History_Records(records)(13)
                    .GMT = History_Records(records)(14)
                Case "Life Cycle Change"
                    .Status = History_Records(records)(7)
                    .SupervisionPeriodExpiryDate = History_Records(records)(12)
                    .ServiceFeePeriodExpiryDate = History_Records(records)(14)
                    .GMT = History_Records(records)(36)
                Case "Adjustment"
                    .AccountBalanceC1 = History_Records(records)(5)
                    .TransactionAmountC1 = History_Records(records)(3)
                    .System = History_Records(records)(47)
                Case "Refill"
                    .TransactionAmountC1 = History_Records(records)(3)
                    .AccountBalanceC1 = History_Records(records)(5)
                    .CardProfile = History_Records(records)(16)
                    .originNodeType = History_Records(records)(12)
                    .User = History_Records(records)(13)
                    .serviceClass = History_Records(records)(41)
                    .originNodeType = History_Records(records)(12)
                    .originNodeType = History_Records(records)(12)
                    .Ded5TransactionAmountC1 = History_Records(records)(48)
                    .Ded5AccountBalanceC1 = History_Records(records)(44)
                    .CreditClearencePeriod = History_Records(records)(60)
                    .ServiceRemovalPeriod = History_Records(records)(61)
                    .SupervisionPeriodExpiryDate = History_Records(records)(62)
                    .ServiceFeePeriodExpiryDate = History_Records(records)(65)
                    .supervisionPeriod = History_Records(records)(63)
                    .serviceFeePeriod = History_Records(records)(66)
                    .externalData1 = History_Records(records)(68)
                    .externalData2 = History_Records(records)(69)
                    .GMT = History_Records(records)(70)
                Case Else
                    X = 1
            End Select
        End With
        Call Fill_History_Table(records - 1, History)
    Next records
Local_Handler:
    If Err.Number Then Resume Next
End Sub
Private Sub Fill_History_Table(ByVal Row_ID As Integer, ByRef Data As History_Type)
    'Displaying Data
    Dim History_Type_details As History_Type
    History_Type_details = History_Type_details_Fill()
    With SulSAT2
        .History_VScroll.Max = Row_ID
        Call Fill_Date_Tranasaction(Row_ID, Data)
        Select Case Data.TransactionType
            Case "Adjustment"
                .History_Field1_list.AddItem Val(Data.TransactionAmountC1)
                .History_Field2_list.AddItem Val(Data.AccountBalanceC1)
                .History_Field3_list.AddItem Data.System
                .History_Field4_list.AddItem ""
                .History_more_details_Values.AddItem ""
                .History_more_details_Desc.AddItem ""
            Case "Language Change"
                .History_Field1_list.AddItem Data.OriginNodeID
                .History_Field2_list.AddItem Data.NewLanguageCode
                .History_Field3_list.AddItem Data.originNodeType
                .History_Field4_list.AddItem ""
                .History_more_details_Values.AddItem ""
                .History_more_details_Desc.AddItem ""
            Case "Life Cycle Change"
                .History_Field1_list.AddItem Data.Status
                .History_Field2_list.AddItem Data.OriginNodeID
                .History_Field3_list.AddItem Data.SupervisionPeriodExpiryDate
                .History_Field4_list.AddItem Data.ServiceFeePeriodExpiryDate
                .History_more_details_Values.AddItem ""
                .History_more_details_Desc.AddItem ""
            Case "Refill"
                .History_Field1_list.AddItem Val(Data.TransactionAmountC1)
                .History_Field2_list.AddItem Val(Data.AccountBalanceC1)
                .History_Field3_list.AddItem Data.CardProfile
                .History_Field4_list.AddItem Data.User
                
                Dim more_details_history_Values, more_details_history_Desc As String
                more_details_history_Values = Data.TransactionAmountC1 & "," & _
                                        Data.AccountBalanceC1 & "," & _
                                        Data.CardProfile & "," & _
                                        Data.originNodeType & "," & _
                                        Data.User & "," & _
                                        Data.serviceClass & "," & _
                                        Data.originNodeType & "," & _
                                        Data.originNodeType & "," & _
                                        Data.Ded5TransactionAmountC1 & "," & _
                                        Data.Ded5AccountBalanceC1 & "," & _
                                        Data.CreditClearencePeriod & "," & _
                                        Data.ServiceRemovalPeriod & "," & _
                                        Data.SupervisionPeriodExpiryDate & "," & _
                                        Data.ServiceFeePeriodExpiryDate & "," & _
                                        Data.supervisionPeriod & "," & _
                                        Data.serviceFeePeriod & "," & _
                                        Data.externalData1 & "," & _
                                        Data.externalData2 & "," & _
                                        Data.GMT & ","
                more_details_history_Desc = History_Type_details.TransactionAmountC1 & "," & _
                                        History_Type_details.AccountBalanceC1 & "," & _
                                        History_Type_details.CardProfile & "," & _
                                        History_Type_details.originNodeType & "," & _
                                        History_Type_details.User & "," & _
                                        History_Type_details.serviceClass & "," & _
                                        History_Type_details.originNodeType & "," & _
                                        History_Type_details.originNodeType & "," & _
                                        History_Type_details.Ded5TransactionAmountC1 & "," & _
                                        History_Type_details.Ded5AccountBalanceC1 & "," & _
                                        History_Type_details.CreditClearencePeriod & "," & _
                                        History_Type_details.ServiceRemovalPeriod & "," & _
                                        History_Type_details.SupervisionPeriodExpiryDate & "," & _
                                        History_Type_details.ServiceFeePeriodExpiryDate & "," & _
                                        History_Type_details.supervisionPeriod & "," & _
                                        History_Type_details.serviceFeePeriod & "," & _
                                        History_Type_details.externalData1 & "," & _
                                        History_Type_details.externalData2 & "," & _
                                        History_Type_details.GMT & ","
               .History_more_details_Values.AddItem more_details_history_Values
               .History_more_details_Desc.AddItem more_details_history_Desc
            Case Else
                .History_Field1_list.AddItem "Missing Configurations"
                .History_Field1_list.AddItem ""
                .History_Field2_list.AddItem ""
                .History_Field3_list.AddItem ""
                .History_Field4_list.AddItem ""
                .History_more_details_Values.AddItem ""
                .History_more_details_Desc.AddItem ""
        End Select
    End With
End Sub



Private Function Date_Time(ByVal DateTimeYYYYMMDDhhmm As String) As String
    If DateTimeYYYYMMDDhhmm = "" Then
        Date_Time = ""
    Else
        Dim YYYY, MOnths, DD, HH, Min As String
        YYYY = Mid(DateTimeYYYYMMDDhhmm, 1, 4)
        MOnths = Mid(DateTimeYYYYMMDDhhmm, 5, 2)
        DD = Mid(DateTimeYYYYMMDDhhmm, 7, 2)
        HH = Mid(DateTimeYYYYMMDDhhmm, 9, 2)
        Min = Mid(DateTimeYYYYMMDDhhmm, 11, 2)
        Date_Time = DD + "\" + MOnths + "\" + YYYY + " " + HH + ":" + Min
    End If
End Function

