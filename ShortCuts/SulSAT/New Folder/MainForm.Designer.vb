<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mainMenuStrip = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExampleOneToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ReadMeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GetDotNetCodecomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mainMenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'mainMenuStrip
        '
        Me.mainMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.mainMenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.mainMenuStrip.Name = "mainMenuStrip"
        Me.mainMenuStrip.Size = New System.Drawing.Size(389, 24)
        Me.mainMenuStrip.TabIndex = 0
        Me.mainMenuStrip.Text = "Main Menu Strip"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(103, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExampleOneToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.ToolsToolStripMenuItem.Text = "&Examples"
        '
        'ExampleOneToolStripMenuItem
        '
        Me.ExampleOneToolStripMenuItem.Name = "ExampleOneToolStripMenuItem"
        Me.ExampleOneToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExampleOneToolStripMenuItem.Text = "Example One"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem, Me.ReadMeToolStripMenuItem, Me.GetDotNetCodecomToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(40, 20)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'ReadMeToolStripMenuItem
        '
        Me.ReadMeToolStripMenuItem.Name = "ReadMeToolStripMenuItem"
        Me.ReadMeToolStripMenuItem.Size = New System.Drawing.Size(297, 22)
        Me.ReadMeToolStripMenuItem.Text = "More Free Examples From Get Dot Net Code"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(297, 22)
        Me.AboutToolStripMenuItem.Text = "&About..."
        '
        'GetDotNetCodecomToolStripMenuItem
        '
        Me.GetDotNetCodecomToolStripMenuItem.Name = "GetDotNetCodecomToolStripMenuItem"
        Me.GetDotNetCodecomToolStripMenuItem.Size = New System.Drawing.Size(297, 22)
        Me.GetDotNetCodecomToolStripMenuItem.Text = "GetDotNetCode.com"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(389, 266)
        Me.Controls.Add(Me.mainMenuStrip)
        Me.IsMdiContainer = True
        Me.Name = "MainForm"
        Me.Text = "MainForm"
        Me.mainMenuStrip.ResumeLayout(False)
        Me.mainMenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mainMenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReadMeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExampleOneToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GetDotNetCodecomToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
