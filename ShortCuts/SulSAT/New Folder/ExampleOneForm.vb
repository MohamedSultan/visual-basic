﻿' GetDotNetCode http://www.getdotnetcode.com

Imports Microsoft.VisualBasic
Imports Microsoft.VisualBasic.FileIO
Imports System.Collections.Generic

Public Class ExampleOneForm

    Private Sub DemoOneForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Make this form a MDI child of MainForm.
        Me.MdiParent = My.Forms.MainForm

    End Sub

    Private Sub loadFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles loadFileButton.Click
        ' Call this form's LoadTabDelimetedTextFileIntoListBox method
        ' to parse a tab delimited file named DemoFile.txt.
        ' DemoFile.txt is located in this project's Bin folder.
        Me.LoadTabDelimetedTextFileIntoListBox("DemoFile.txt")
    End Sub

    Private Sub LoadTabDelimetedTextFileIntoListBox(ByVal filePath As String)

        ' Declare a variable named theTextFieldParser of type TextFieldParser.
        Dim theTextFieldParser As TextFieldParser

        ' Call the My feature's OpenTextFieldParser method passing in a file path.
        ' Assign the resulting TxtFileParser object to theTextFieldParser variable.
        theTextFieldParser = My.Computer.FileSystem.OpenTextFieldParser(filePath)

        ' Set TextFieldParser object's TextFieldType property to Delimited.
        theTextFieldParser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited

        ' Configure delimiters to handle a tab delimited text file:
        ' Set TextFieldParser object's Delimiter's property to a string array
        ' containing one element with the value ",".
        theTextFieldParser.SetDelimiters(vbTab)

        ' Declare a variable named currentRow of type string array.
        Dim currentRow() As String

        Try

            ' Use the Generic List(Of T) class to create a list to
            '   hold log entries from the .txt file.
            Dim newListOfLogEntry As New List(Of LogEntry)

            ' While the end of file has not been reached....
            While Not theTextFieldParser.EndOfData

                ' Read the fields on the current line
                ' and assign them to the currentRow array variable.
                currentRow = theTextFieldParser.ReadFields()

                ' Create a new LogEntry object from the current row of the text file.
                Dim newLogEntry As New LogEntry(currentRow.GetValue(0).ToString, currentRow.GetValue(2).ToString, currentRow.GetValue(1).ToString)

                ' Add the new LogEnry object to the newListOfLogEntry list.
                newListOfLogEntry.Add(newLogEntry)

            End While

            Me.DataGridView1.DataSource = newListOfLogEntry

        Catch malFormLineEx As Microsoft.VisualBasic.FileIO.MalformedLineException
            MessageBox.Show("Line " & malFormLineEx.Message & "is not valid and will be skipped.", "Malformed Line Exception")
        Catch ex As Exception
            MessageBox.Show(ex.Message & " exception has occurred.", "Exception")
        Finally
            ' If successful or if an exception is thrown,
            ' close the TextFieldParser.
            theTextFieldParser.Close()
        End Try


    End Sub
End Class

Public Class LogEntry
        ' EntryDateTime Property
        Private m_EntryDateTime As DateTime
        Public Property EntryDateTime() As DateTime
            Get
                Return m_EntryDateTime
            End Get
            Set(ByVal value As DateTime)
                m_EntryDateTime = value
            End Set
    End Property

        ' Device Property
        Private m_Device As String
        Public Property Device() As String
            Get
                Return m_Device
            End Get
            Set(ByVal value As String)
                m_Device = value
            End Set
        End Property
    ' Status Property
    Private m_Status As String
    Public Property Status() As String
        Get
            Return m_Status
        End Get
        Set(ByVal value As String)
            m_Status = value
        End Set
    End Property

    Public Sub New(ByVal logEntryDateTime As String, ByVal device As String, ByVal status As String)
        Me.EntryDateTime = DateTime.Parse(logEntryDateTime)
        Me.Device = device
        Me.Status = status
    End Sub

    Public Sub SetLogEntryDateTime(ByVal entryDateTime As String)
        Me.EntryDateTime = DateTime.Parse(entryDateTime)
    End Sub

End Class