﻿' GetDotNetCode MyAppliction Extensions
Namespace My
    Partial Class MyApplication
        Public ReadOnly Property GetDotNetCodeDataDirectory() As String
            Get
                ' Directory path returns path to directory where .exe or .dll is stored.
                Return Application.Info.DirectoryPath & "\GetDotNetCode Data"
            End Get
        End Property
    End Class

End Namespace

