﻿Partial Public Class AboutForm
    Inherits System.Windows.Forms.Form

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Friend WithEvents TableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents AssemblyTitleLabel As System.Windows.Forms.Label
    Friend WithEvents VersionLabel As System.Windows.Forms.Label
    Friend WithEvents CompanyNameLabel As System.Windows.Forms.Label
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents CopyrightLabel As System.Windows.Forms.Label

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel = New System.Windows.Forms.TableLayoutPanel
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox
        Me.AssemblyTitleLabel = New System.Windows.Forms.Label
        Me.VersionLabel = New System.Windows.Forms.Label
        Me.CopyrightLabel = New System.Windows.Forms.Label
        Me.CompanyNameLabel = New System.Windows.Forms.Label
        Me.OKButton = New System.Windows.Forms.Button
        Me.CompanyWebSiteLinkLabel = New System.Windows.Forms.LinkLabel
        Me.TableLayoutPanel.SuspendLayout()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel
        '
        Me.TableLayoutPanel.ColumnCount = 2
        Me.TableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133.0!))
        Me.TableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.0!))
        Me.TableLayoutPanel.Controls.Add(Me.LogoPictureBox, 0, 0)
        Me.TableLayoutPanel.Controls.Add(Me.AssemblyTitleLabel, 1, 0)
        Me.TableLayoutPanel.Controls.Add(Me.VersionLabel, 1, 1)
        Me.TableLayoutPanel.Controls.Add(Me.CopyrightLabel, 1, 2)
        Me.TableLayoutPanel.Controls.Add(Me.CompanyNameLabel, 1, 3)
        Me.TableLayoutPanel.Controls.Add(Me.OKButton, 1, 5)
        Me.TableLayoutPanel.Controls.Add(Me.CompanyWebSiteLinkLabel, 1, 4)
        Me.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel.Name = "TableLayoutPanel"
        Me.TableLayoutPanel.RowCount = 6
        Me.TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125.0!))
        Me.TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17.0!))
        Me.TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0.0!))
        Me.TableLayoutPanel.Size = New System.Drawing.Size(468, 359)
        Me.TableLayoutPanel.TabIndex = 0
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LogoPictureBox.InitialImage = Nothing
        Me.LogoPictureBox.Location = New System.Drawing.Point(3, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.TableLayoutPanel.SetRowSpan(Me.LogoPictureBox, 6)
        Me.LogoPictureBox.Size = New System.Drawing.Size(127, 353)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 0
        Me.LogoPictureBox.TabStop = False
        '
        'AssemblyTitleLabel
        '
        Me.AssemblyTitleLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AssemblyTitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AssemblyTitleLabel.Location = New System.Drawing.Point(136, 0)
        Me.AssemblyTitleLabel.Name = "AssemblyTitleLabel"
        Me.AssemblyTitleLabel.Padding = New System.Windows.Forms.Padding(5)
        Me.AssemblyTitleLabel.Size = New System.Drawing.Size(329, 125)
        Me.AssemblyTitleLabel.TabIndex = 0
        Me.AssemblyTitleLabel.Text = "Product Title"
        Me.AssemblyTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'VersionLabel
        '
        Me.VersionLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.VersionLabel.Location = New System.Drawing.Point(136, 125)
        Me.VersionLabel.Name = "VersionLabel"
        Me.VersionLabel.Size = New System.Drawing.Size(329, 74)
        Me.VersionLabel.TabIndex = 0
        Me.VersionLabel.Text = "Version"
        Me.VersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CopyrightLabel
        '
        Me.CopyrightLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CopyrightLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CopyrightLabel.Location = New System.Drawing.Point(136, 199)
        Me.CopyrightLabel.Name = "CopyrightLabel"
        Me.CopyrightLabel.Size = New System.Drawing.Size(329, 74)
        Me.CopyrightLabel.TabIndex = 0
        Me.CopyrightLabel.Text = "Copyright"
        Me.CopyrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CompanyNameLabel
        '
        Me.CompanyNameLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CompanyNameLabel.Location = New System.Drawing.Point(136, 273)
        Me.CompanyNameLabel.Name = "CompanyNameLabel"
        Me.CompanyNameLabel.Size = New System.Drawing.Size(329, 17)
        Me.CompanyNameLabel.TabIndex = 0
        Me.CompanyNameLabel.Text = "Company Name"
        Me.CompanyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'OKButton
        '
        Me.OKButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OKButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.OKButton.Location = New System.Drawing.Point(390, 362)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 1)
        Me.OKButton.TabIndex = 0
        Me.OKButton.Text = "&OK"
        '
        'CompanyWebSiteLinkLabel
        '
        Me.CompanyWebSiteLinkLabel.AutoSize = True
        Me.CompanyWebSiteLinkLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CompanyWebSiteLinkLabel.LinkArea = New System.Windows.Forms.LinkArea(0, 21)
        Me.CompanyWebSiteLinkLabel.Location = New System.Drawing.Point(136, 290)
        Me.CompanyWebSiteLinkLabel.Name = "CompanyWebSiteLinkLabel"
        Me.CompanyWebSiteLinkLabel.Size = New System.Drawing.Size(329, 69)
        Me.CompanyWebSiteLinkLabel.TabIndex = 2
        Me.CompanyWebSiteLinkLabel.TabStop = True
        Me.CompanyWebSiteLinkLabel.Text = "www.getdotnetcode.com"
        '
        'AboutForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.OKButton
        Me.ClientSize = New System.Drawing.Size(468, 359)
        Me.Controls.Add(Me.TableLayoutPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AboutForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "AboutForm"
        Me.TableLayoutPanel.ResumeLayout(False)
        Me.TableLayoutPanel.PerformLayout()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CompanyWebSiteLinkLabel As System.Windows.Forms.LinkLabel

End Class
