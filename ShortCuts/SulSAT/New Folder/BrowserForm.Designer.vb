<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrowserForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AboutBrowser = New System.Windows.Forms.WebBrowser
        Me.SuspendLayout()
        '
        'AboutBrowser
        '
        Me.AboutBrowser.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AboutBrowser.Location = New System.Drawing.Point(12, 25)
        Me.AboutBrowser.MinimumSize = New System.Drawing.Size(20, 20)
        Me.AboutBrowser.Name = "AboutBrowser"
        Me.AboutBrowser.Size = New System.Drawing.Size(699, 411)
        Me.AboutBrowser.TabIndex = 0
        '
        'BrowserForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 448)
        Me.Controls.Add(Me.AboutBrowser)
        Me.Name = "BrowserForm"
        Me.Text = "BrowserForm"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents AboutBrowser As System.Windows.Forms.WebBrowser
End Class
