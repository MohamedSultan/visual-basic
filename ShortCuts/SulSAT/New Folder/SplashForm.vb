﻿Public Class SplashForm

    Private Sub SplashForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' MM Set here due to Beta 1 issue with resources.
        Me.GdncSkyScraperPictureBox.Image = My.Resources.GdncSkyscraper

        'Application title
        If My.Application.Info.Title <> "" Then
            ApplicationTitle.Text = My.Application.Info.Title
        Else
            'If the application title is missing, use the application name, without the extension
            ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.DirectoryPath)
        End If

        Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        'Copyright
        Copyright.Text = My.Application.Info.Copyright
    End Sub
End Class
