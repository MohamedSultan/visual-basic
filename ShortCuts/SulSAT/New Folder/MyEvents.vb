Namespace My
    Partial Friend Class MyApplication

        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As ApplicationServices.StartupEventArgs) Handles Me.Startup
            ' MM SplashForm has opened.
            ' Sleep for three seconds before showing MainForm.
            System.Threading.Thread.Sleep(3000)

            ' Main MainForm a MDI container.
            My.Forms.MainForm.IsMdiContainer = True
            ' Open MainForm in its maximized windows state.
            My.Forms.MainForm.WindowState = FormWindowState.Maximized

        End Sub
    End Class
End Namespace
