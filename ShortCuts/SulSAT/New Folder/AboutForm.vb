﻿Public Class AboutForm

    Private Sub AboutForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Set LogoPictureBox image to GetDotNetCode Skyscraper logo image.
        Me.LogoPictureBox.Image = My.Resources.GdncSkyscraper

        ' Set the title of the form.
        Dim ApplicationTitle As String

        If My.Application.Info.Title <> "" Then
            ApplicationTitle = My.Application.Info.Title
        Else
            ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.DirectoryPath)
        End If
        Me.Text = String.Format("About {0}", ApplicationTitle)

        ' Initialize all of the text displayed on the About form.
        Me.AssemblyTitleLabel.Text = My.Application.Info.ProductName & Environment.NewLine & Environment.NewLine & ApplicationTitle
        Me.VersionLabel.Text = String.Format("Version {0}", My.Application.Info.Version.ToString)
        Me.CopyrightLabel.Text = My.Application.Info.Copyright
        Me.CompanyNameLabel.Text = My.Application.Info.CompanyName

    End Sub

    Private Sub CompanyWebSiteLinkLabel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CompanyWebSiteLinkLabel.Click
        System.Diagnostics.Process.Start("http://www.getdotnetcode.com")
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub


End Class
