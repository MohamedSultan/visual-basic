Public Class MainForm

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set Text of MainForm to application title.
        Me.Text = My.Application.Info.Title
    End Sub
    Private Sub ExampleOneToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExampleOneToolStripMenuItem.Click
        My.Forms.ExampleOneForm.Show()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        My.Forms.AboutForm.ShowDialog()
    End Sub

    Private Sub ReadMeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReadMeToolStripMenuItem.Click
        My.Forms.BrowserForm.Show()
        If My.Computer.Network.IsAvailable Then
            My.Forms.BrowserForm.AboutBrowser.Navigate("http://www.getdotnetcode.com/GdncStore/Free.aspx")
        Else
            My.Forms.BrowserForm.AboutBrowser.Navigate(My.Application.GetDotNetCodeDataDirectory & "\ReadMe\ReadMe.htm")
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub GetDotNetCodecomToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetDotNetCodecomToolStripMenuItem.Click
        My.Forms.BrowserForm.Show()
        If My.Computer.Network.IsAvailable Then
            My.Forms.BrowserForm.AboutBrowser.Navigate("http://www.getdotnetcode.com")
        Else
            My.Forms.BrowserForm.AboutBrowser.Navigate(My.Application.GetDotNetCodeDataDirectory & "\ReadMe\ReadMe.htm")
        End If
    End Sub
End Class