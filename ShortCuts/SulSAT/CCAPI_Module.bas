Attribute VB_Name = "CCAPI_Module"
Type Promotion_Plan_Type
    ID As String
    Desc As String
    Start_Date As String
    End_date As String
End Type
Type CCAPI_Response_Type
    Languages_ID  As String
    Languages_Desc As String
    Sub_Status As String
    Service_Classes_ID  As String
    Service_Classes_Desc  As String
    accountValue1  As String
    Accumulator1  As String
    Accumulator2  As String
    Accumulator3  As String
    Accumulator4  As String
    Accumulator5  As String
    AccumulatorResetDate1  As String
    AccumulatorResetDate2  As String
    AccumulatorResetDate3  As String
    AccumulatorResetDate4  As String
    AccumulatorResetDate5  As String
    AccumulatorStartDate1  As String
    AccumulatorStartDate2  As String
    AccumulatorStartDate3  As String
    AccumulatorStartDate4  As String
    AccumulatorStartDate5  As String
    AccumulatorDesc1  As String
    AccumulatorDesc2  As String
    AccumulatorDesc3  As String
    AccumulatorDesc4  As String
    AccumulatorDesc5  As String
    MaxServiceFeePeriod  As String
    MaxSupervisionPeriod  As String
    serviceFeeDate_  As String
    removalPeriod  As String
    supervisionDate_  As String
    creditClearancePeriod  As String
    DedicateAccount(5)  As String
    DedicatedAccountDesc(5)  As String
    DedicatedAccountExpiryDate(5) As String
    Activation_Date  As String
    Account_Flags  As String
    creditClearanceDate  As String
    supervisionDate  As String
    supervisionPeriod As String
    serviceFeeDate  As String
    serviceFeePeriod As String
    PromotionPlanID As String
    PromotionPlanStartDate As String
    PromotionPlanEndDate As String
End Type
Public CCAPI_Current_Data As CCAPI_Response_Type

Public Sub Fill_From_CCAPI(ByRef Response As String)
    With SulSAT2
        .Languages_Desc = CCAPI_Parsing(Response, "Language")
        
        'mapping Language ID and Language desc
        For i = 0 To .Languages_Desc.ListCount
            If .Languages_Desc = .Languages_Desc.List(i) Then .Languages_ID.Text = .Languages_ID.List(i)
        Next i
        .RefillBarred = CCAPI_Parsing(Response, "TempBlockingStatusDescription")
        .Sub_Status = CCAPI_Parsing(Response, "Status")
        .Service_Classes_ID = CCAPI_Parsing(Response, "ServiceClass")
        .Service_Classes_Desc = CCAPI_Parsing(Response, "ServiceClassDescription")
        .accountValue1 = CCAPI_Parsing(Response, "AccountBalance")
        .Accumulator(1) = CCAPI_Parsing(Response, "Accumulator1Value")
        .Accumulator(2) = CCAPI_Parsing(Response, "Accumulator2Value")
        .Accumulator(3) = CCAPI_Parsing(Response, "Accumulator3Value")
        .Accumulator(4) = CCAPI_Parsing(Response, "Accumulator4Value")
        .Accumulator(5) = CCAPI_Parsing(Response, "Accumulator5Value")
        '.Promotion_Plans_ID = CCAPI_Parsing(Response, "PromotionPlan")
        '.Promotion_Plans_End_Date = CCAPI_Parsing(Response, "Promotion_Plan_End_Date")
        '.Promotion_Plans_Start_Date = CCAPI_Parsing(Response, "PromotionPlanStartDate")
        '.AccumulatedProgressCounter = CCAPI_Parsing(Response, "PromotionProgressCount")
        '.PromotionProgressValue = CCAPI_Parsing(Response, "PromotionProgressValue")
        '.Promotion_Plans_ID = CCAPI_Parsing(Response, "PromotionPlan")
        
        .AccumulatorResetDate(1) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator1ResetDate"))
        .AccumulatorResetDate(2) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator2ResetDate"))
        .AccumulatorResetDate(3) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator3ResetDate"))
        .AccumulatorResetDate(4) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator4ResetDate"))
        .AccumulatorResetDate(5) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator5ResetDate"))
        .AccumulatorStartDate(1) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator1StartDate"))
        .AccumulatorStartDate(2) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator2StartDate"))
        .AccumulatorStartDate(3) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator3StartDate"))
        .AccumulatorStartDate(4) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator4StartDate"))
        .AccumulatorStartDate(5) = SulSAT.Date_Only(CCAPI_Parsing(Response, "Accumulator5StartDate"))
        .AccumulatorDesc(1) = CCAPI_Parsing(Response, "Accumulator1Description")
        .AccumulatorDesc(2) = CCAPI_Parsing(Response, "Accumulator2Description")
        .AccumulatorDesc(3) = CCAPI_Parsing(Response, "Accumulator3Description")
        .AccumulatorDesc(4) = CCAPI_Parsing(Response, "Accumulator4Description")
        .AccumulatorDesc(5) = CCAPI_Parsing(Response, "Accumulator5Description")
        .MaxServiceFeePeriod = CCAPI_Parsing(Response, "MaxServiceFeePeriod")
        .MaxSupervisionPeriod = CCAPI_Parsing(Response, "MaxSupervisionPeriod")
        For i = 1 To .DedicateAccount.UBound
            .DedicateAccount(i) = CCAPI_Parsing(Response, "DedicatedAccount" & Trim(Str(i)) & "Balance")
            .DedicatedAccountDesc(i) = CCAPI_Parsing(Response, "DedicatedAccount" & Trim(Str(i)) & "Description")
            .DedicatedAccountExpiryDate(i) = SulSAT.Date_Only(CCAPI_Parsing(Response, "DedicatedAccount" & Trim(Str(i)) & "Date"))
        Next i
        .serviceFeeDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "ServiceFeePeriodExpiryDate"))
        .removalPeriod = CCAPI_Parsing(Response, "ServiceRemovalGracePeriod")
        .supervisionDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "SupervisionPeriodExpiryDate"))
        .creditClearancePeriod = CCAPI_Parsing(Response, "CreditClearanceGracePeriod")
        .Activation_Date = SulSAT.Date_Only(CCAPI_Parsing(Response, "ActivationDate"))
        .Account_Flags = CCAPI_Parsing(Response, "AccountFlagsAfter")
        .creditClearanceDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "Count1ClearingDate"))
        
        
        If .supervisionDate <> "\\" Then .supervisionPeriod = DateDiff("d", Format(Now, "DD mmm YYYY"), Trim(SulSAT.ToDate(.supervisionDate)))
        If .serviceFeeDate <> "\\" Then .serviceFeePeriod = DateDiff("d", Format(Now, "DD mmm YYYY"), Trim(SulSAT.ToDate(.serviceFeeDate)))
        
        'Call SulSAT2.Languages_Desc_Click
        If .Sub_Status = "Installed" Then
            Call LockAllTextBoxes(SulSAT2, True)
        Else
            .serviceRemovalDate = Replace(Format(DateAdd("d", .removalPeriod, Replace(.serviceFeeDate, "\", " ")), "DD mm YYYY"), " ", "\")
            Call LockAllTextBoxes(SulSAT2, False)
        End If
        Call Fill_Current_Data
    End With
End Sub
Public Sub Fill_Current_Data()
    With SulSAT2
        '----------------------------------------------------------------------------------------------
        If .RefillBarred = "Refill Barred" Then
            .Standard_Voucher_Refill(0).Enabled = False
            .Standard_Voucher_Refill(1).Enabled = False
            .Refill_Cmd.Enabled = False
            For i = 0 To .Unbar.UBound
                .Unbar(i).Visible = True
            Next i
        Else
            .Standard_Voucher_Refill(0).Enabled = True
            .Standard_Voucher_Refill(1).Enabled = True
            .Refill_Cmd.Enabled = True
            For i = 0 To .Unbar.UBound
                .Unbar(i).Visible = False
            Next i
        End If
        CCAPI_Current_Data.Languages_ID = .Languages_ID
        CCAPI_Current_Data.Languages_Desc = .Languages_Desc
        CCAPI_Current_Data.Sub_Status = .Sub_Status
        CCAPI_Current_Data.Service_Classes_ID = .Service_Classes_ID
        CCAPI_Current_Data.Service_Classes_Desc = .Service_Classes_Desc
        CCAPI_Current_Data.accountValue1 = .accountValue1
        CCAPI_Current_Data.Accumulator1 = .Accumulator(1)
        CCAPI_Current_Data.Accumulator2 = .Accumulator(2)
        CCAPI_Current_Data.Accumulator3 = .Accumulator(3)
        CCAPI_Current_Data.Accumulator4 = .Accumulator(4)
        CCAPI_Current_Data.Accumulator5 = .Accumulator(5)
        CCAPI_Current_Data.AccumulatorResetDate1 = .AccumulatorResetDate(1)
        CCAPI_Current_Data.AccumulatorResetDate2 = .AccumulatorResetDate(2)
        CCAPI_Current_Data.AccumulatorResetDate3 = .AccumulatorResetDate(3)
        CCAPI_Current_Data.AccumulatorResetDate4 = .AccumulatorResetDate(4)
        CCAPI_Current_Data.AccumulatorResetDate5 = .AccumulatorResetDate(5)
        CCAPI_Current_Data.AccumulatorStartDate1 = .AccumulatorStartDate(1)
        CCAPI_Current_Data.AccumulatorStartDate2 = .AccumulatorStartDate(2)
        CCAPI_Current_Data.AccumulatorStartDate3 = .AccumulatorStartDate(3)
        CCAPI_Current_Data.AccumulatorStartDate4 = .AccumulatorStartDate(4)
        CCAPI_Current_Data.AccumulatorStartDate5 = .AccumulatorStartDate(5)
        CCAPI_Current_Data.AccumulatorDesc1 = .AccumulatorDesc(1)
        CCAPI_Current_Data.AccumulatorDesc2 = .AccumulatorDesc(2)
        CCAPI_Current_Data.AccumulatorDesc3 = .AccumulatorDesc(3)
        CCAPI_Current_Data.AccumulatorDesc4 = .AccumulatorDesc(4)
        CCAPI_Current_Data.AccumulatorDesc5 = .AccumulatorDesc(5)
        CCAPI_Current_Data.MaxServiceFeePeriod = .MaxServiceFeePeriod
        CCAPI_Current_Data.MaxSupervisionPeriod = .MaxSupervisionPeriod
        For i = 1 To .DedicateAccount.UBound
            CCAPI_Current_Data.DedicateAccount(i) = .DedicateAccount(i)
            CCAPI_Current_Data.DedicatedAccountExpiryDate(i) = .DedicatedAccountExpiryDate(i)
            CCAPI_Current_Data.DedicatedAccountDesc(i) = .DedicatedAccountDesc(i)
        Next i
        CCAPI_Current_Data.serviceFeeDate = .serviceFeeDate
        CCAPI_Current_Data.removalPeriod = .removalPeriod
        CCAPI_Current_Data.supervisionDate = .supervisionDate
        CCAPI_Current_Data.creditClearancePeriod = .creditClearancePeriod
        CCAPI_Current_Data.Activation_Date = .Activation_Date
        CCAPI_Current_Data.Account_Flags = .Account_Flags
        CCAPI_Current_Data.creditClearanceDate = .creditClearanceDate
        CCAPI_Current_Data.supervisionDate_ = .supervisionDate
        CCAPI_Current_Data.serviceFeeDate_ = .serviceFeeDate
        CCAPI_Current_Data.supervisionPeriod = .supervisionPeriod
        CCAPI_Current_Data.serviceFeePeriod = .serviceFeePeriod
        
    End With
    'Call SulSAT2.Languages_Desc_Click
End Sub

Public Function CCAPI_Parsing(ByVal Whole_String As String, _
                                ByRef Variable_Name As String, _
                                Optional ByVal Delimiter As String) As String
    If Delimiter = "" Then Delimiter = ":"
    Dim Requested_Variable_Location, Next_Column As Integer
    Requested_Variable_Location = InStr(1, Whole_String, Variable_Name)
    If Requested_Variable_Location <> 0 Then
        Next_Column = InStr(Requested_Variable_Location, Whole_String, Delimiter)
        If Next_Column = 0 Then Next_Column = InStr(Requested_Variable_Location, Whole_String, ";")
        CCAPI_Parsing = Mid(Whole_String, Requested_Variable_Location + Len(Variable_Name) + 1, Next_Column - (Requested_Variable_Location + Len(Variable_Name)) - 1)
    Else
        CCAPI_Parsing = ""
    End If
End Function

Public Function CCAPI_Command(Command_Name As String, _
                                Optional Data1 As String, _
                                Optional Data2 As String, _
                                Optional Data3 As String, _
                                Optional Data4 As String, _
                                Optional Data5 As String) As String
    If Command_Name <> "" Then
        Select Case Command_Name
            Case "Insert Subscriber"
                CCAPI_Command = "CREATE:SUBSCRIBER:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                            ":IMSI,20" & SulSAT2.MSISDNs_List.Text & ":SIM,20" & SulSAT2.MSISDNs_List.Text & _
                            ":BusinessPlan," & Data1 & ":TempBlockingStatus,CLEAR:OrganizationID,1;"
            Case "Get Account Information"  'Get Account Details
                CCAPI_Command = "GET:ACCOUNTINFORMATION:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & Trim(";")
            Case "Get Account History"
                CCAPI_Command = "GET:ACCOUNTHISTORY:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & ":StartRecord,0:NumberOfRecords,50;" ':SectionName,<Name of section>;"
Case "Update a Subscriber�s Usage Accumulators"
Case "Update Dedicated Accounts for a Subscriber"       'UCIP command is used
Case "Update Account Expiry Dates"
    CCAPI_Command = "SET:ACCOUNTEXPIRYDATE:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                    ":OldServiceFeePeriodExpiryDate," & Data1 & _
                    ":NewServiceFeePeriodExpiryDate," & Data2 & _
                    ":OldSupervisionPeriodExpiryDate," & Data3 & _
                    ":NewSupervisionPeriodExpiryDate," & Data4 & ";"
Case "Set Service Class for a Subscriber"
    CCAPI_Command = "SET:ACCOUNTSERVICECLASS:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                            ":OldServiceClass," & Data1 & ":NewServiceClass," & Data2 & ";"
            Case "Delete Subscriber"
                CCAPI_Command = "DELETE:SUBSCRIBER:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & ";"
            Case "Reset Subscriber Refill Barrings"
                CCAPI_Command = "SET:RESETREFILLBARRING:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & ";"
            Case "Set Subscriber Language"
                CCAPI_Command = "SET:LANGUAGE:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                    ":OldLanguageCode," & Data1 & ":NewLanguageCode," & Data2 & ";"
            Case "Allocate a Promotion Plan to a Subscriber"
                If Data2 = "" Then Data2 = Format(Now, "YYYYMMDD")
                CCAPI_Command = "CREATE:PROMOTIONPLANALLOCATION:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":PromotionPlan," & Data1 & ":PromotionPlanStartDate," & Data2 & _
                                ":PromotionPlanEndDate," & Data3 & ";"
            Case "Get Promotion Plans Allocated to a Subscriber"
                CCAPI_Command = "GET:PROMOTIONPLANALLOCATION:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & ";"
            Case "Delete a Promotion Plan Allocated to an Account"
                CCAPI_Command = "DELETE:PROMOTIONPLANALLOCATION:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                    ":PromotionPlan," & Data1 & _
                                    ":PromotionPlanStartDate," & Data2 & _
                                    ":PromotionPlanEndDate," & Data3 & ";"
            Case "Update a Promotion Plan Allocated to an Account"
                CCAPI_Command = "SET:PROMOTIONPLANALLOCATION:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":PromotionPlan," & Data1 & _
                                ":PromotionPlanStartDateOld," & Data2 & _
                                ":PromotionPlanStartDateNew," & Data3 & _
                                ":PromotionPlanEndDateOld," & Data4 & _
                                ":PromotionPlanEndDateNew," & Data5 & ";"
            Case "Get Payment Profile List"
                CCAPI_Command = "GET:PAYMENTPROFILELIST;"
            Case "Get Voucher Details"
                CCAPI_Command = "GET:VOUCHERDETAIL:VoucherSerialNumber," & Data1 & ";"
            Case "Standard Voucher Refill"
                CCAPI_Command = "SET:VOUCHERREFILL:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                            ":VoucherActivationCode," & Data1 & _
                            ":ExternalData1," & Data2 & _
                            ":ExternalData2," & Data3 & ";"
            Case "Payment / Refill"
                CCAPI_Command = "SET:REFILL:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":TransactionAmount," & Data1 & _
                                ":Currency,0" & _
                                ":PaymentProfile," & Data2 & _
                                ":ExternalData1," & Data3 & _
                                ":ExternalData2," & Data4 & ";"
            Case "Account Adjustment"
                CCAPI_Command = "SET:ACCOUNTADJUSTMENT:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":Currency,EGP:Action," & Data1 & _
                                ":AdjustmentAmount," & Data2 & _
                                ":TransactionType,TST:TransactionCode,TEST;"
                                
                                'Data1=<ADD |SUBTRACT>
                        
            Case "Get a Subscriber FaF Number List"
                CCAPI_Command = "GET:SUBSCRIBERFAFNUMBER:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & ";"
            Case "Add a FaF Number to a Subscriber"
                CCAPI_Command = "CREATE:SUBSCRIBERFAFNUMBER:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":FAFNumber," & Data1 & _
                                ":FAFIndicator," & Data2 & ";"
            Case "Modify a Subscriber FaF Number"
                CCAPI_Command = "SET:SUBSCRIBERFAFNUMBER:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":FAFNumber," & Data1 & _
                                ":FAFIndicator," & Data2 & ";"
            Case "Delete a Subscriber FaF Number"
                CCAPI_Command = "DELETE:SUBSCRIBERFAFNUMBER:SubscriberNumber,20" & SulSAT2.MSISDNs_List.Text & _
                                ":FAFNumber," & Data1 & _
                                ":FAFIndicator," & Data2 & ";"
    
    Case "Get an Account FaF Number List"
    Case "Add a FaF Number to an Account"
    Case "Modify an Account FaF Number"
    Case "Delete an Account FaF Number"
    
    Case "Get FaF Black List"
        CCAPI_Command = "GET:FAFBLACKLIST;"
            Case "Get FaF Plans"
                CCAPI_Command = "GET:FAFPLANS;"
            Case "Get FaF Indicators"
                CCAPI_Command = "GET:FAFINDICATORS;"
            Case "Get Languages"
                CCAPI_Command = "GET:LANGUAGES;"
            Case "Get Promotion Plans"
                CCAPI_Command = "GET:PROMOTIONPLANS;"
            Case "Get Service Classes"
                CCAPI_Command = "GET:SERVICECLASSES;"
            Case "Get Business Plans"
                CCAPI_Command = "GET:BUSINESSPLANS:OrganizationID,1;"
    Case "Get Business Plan Info"
        CCAPI_Command = "GET:BUSINESSPLANINFO:BusinessPlan," & Data1 & ":OrganizationID,1;"
            
            
            Case "Value Voucher Refill"
            Case "Get Subordinate Subscribers"
            Case "Get Currency Codes"
            Case "Get USSD End Of Call Notification Codes"
            Case "Update Subscriber USSD End Of Call Notification Code"
            Case "Get Account Groups"
            Case "Update Subscriber Account Group"
            Case "Get Service Offering Descriptions"
            Case "Update Subscriber Service Offering"
            Case "Get Service Offering Plans"
            Case "Get Community Charging"
            Case "Get Community Charging Groups"
            Case "Get Community Charging Group"
            Case "Update Subscriber Community Data"
            Case "Change Subscriber SIM"
            Case "Link Subscriber"
            Case "Get Subscriber Information"
            Case "Get Subscriber Personal Details"
            Case "Set Subscriber Personal Details"
        End Select
    Else
        CCAPI_Command = ""
    End If
End Function


