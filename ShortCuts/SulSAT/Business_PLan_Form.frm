VERSION 5.00
Begin VB.Form Business_PLan_Form 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   1545
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4485
   LinkTopic       =   "Form1"
   ScaleHeight     =   1545
   ScaleWidth      =   4485
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   -80
      Width           =   4455
      Begin VB.CommandButton OK 
         Caption         =   "OK"
         Height          =   375
         Left            =   1320
         TabIndex        =   5
         Top             =   1080
         Width           =   1575
      End
      Begin VB.ComboBox Business_Plan_Desc 
         Height          =   315
         Left            =   1560
         TabIndex        =   4
         Top             =   600
         Width           =   2415
      End
      Begin VB.ComboBox Business_Plan_ID 
         Height          =   315
         Left            =   360
         TabIndex        =   3
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Business Plan Description"
         Height          =   255
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "ID"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "Business_PLan_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Business_Plan_Desc_Click()
    Business_Plan_ID.Text = Business_Plan_ID.List(Business_Plan_Desc.ListIndex)
End Sub

Private Sub Business_Plan_ID_Click()
    Business_Plan_Desc.Text = Business_Plan_Desc.List(Business_Plan_ID.ListIndex)
End Sub

Private Sub OK_Click()
    Dim Response As String
    Response = SulSAT2.Send_CCAPI("Insert Subscriber", 1, Business_Plan_ID)
    Me.Hide
    If InStr(1, Response, "ESP:0") <> 0 Then Call SulSAT2.LoadAll_Click
End Sub
