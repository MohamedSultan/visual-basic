VERSION 5.00
Begin VB.Form CCAPI 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "CCAPI"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   6855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Log 
      Height          =   2535
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   240
      Width           =   6615
   End
End
Attribute VB_Name = "CCAPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim SentCommand As String
Dim All_Data_received As String

Private Sub CCAPI_Connection_Connect()
    'MsgBox "WS-Connected"
    SentCommand = "LOGIN:minsat:min_cs;" + vbCrLf
    Call Logging(">" + SentCommand)
    CCAPI_Connection.SendData SentCommand
End Sub

Private Sub CCAPI_Connection_DataArrival(ByVal bytesTotal As Long)
    Dim Data_received As String
    Call CCAPI_Connection.GetData(Data_received)
    All_Data_received = All_Data_received + Trim(Data_received)
    If InStr(1, All_Data_received, ";") <> 0 Then   'Data Finished
        If InStr(1, SentCommand, "LOGIN:") <> 0 Then MsgBox "Logged In"
        Call Logging(">" + All_Data_received)
        All_Data_received = ""
    End If
End Sub

Private Sub CCAPI_Connection_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox "Connection Error(" & CStr(Number) & "): " & Description
    Me.CCAPI_Connection.Close
End Sub

Private Sub Logging(ByRef Data As String)
    Log = Log + Main.MSISDNS.Text & vbTab & Format(Now, "YYYY-MM-DD HH:NN:SS") & vbTab & Data + vbCrLf
    Log.SelStart = Len(Log)
End Sub

Public Function CCAPI_Command(Command_Name As String) As String
    If Command_Name <> "" Then
        Select Case Command_Name
            Case "Insert Subscriber"
                SentCommand = "CREATE:SUBSCRIBER:SubscriberNumber,20" + Trim(Main.MSISDNS) + _
                            Trim(":IMSI,20") + Trim(Main.MSISDNS) + Trim(":SIM,20") + Trim(Main.MSISDNS) + _
                            Trim(":BusinessPlan,1:SDP,SDP4:,Language,1:TempBlockingStatus,CLEAR;OrganizationID,1;")
            Case "Get Account Information"  'Get Account Details
                SentCommand = "GET:ACCOUNTINFORMATION:SubscriberNumber,20" + Trim(Main.MSISDNS) + Trim(";")
            Case "Get Account History"
                SentCommand = "GET:ACCOUNTHISTORY:SubscriberNumber,20" + Main.MSISDNS + ":StartRecord,<Start Record>:NumberOfRecords,<Subscriber Number>:SectionName,<Name of section>;"
            Case "Update a Subscriber�s Usage Accumulators"
            Case "Update Dedicated Accounts for a Subscriber"
            Case "Update Account Expiry Dates"
            Case "Set Service Class for a Subscriber"
            Case "Delete Subscriber"
                SentCommand = "DELETE:SUBSCRIBER:SubscriberNumber,20" + Main.MSISDNS + Trim(";")
            Case "Reset Subscriber Refill Barrings"
            Case "Set Subscriber Language"
            Case "Allocate a Promotion Plan to a Subscriber"
            Case "Get Promotion Plans Allocated to a Subscriber"
            Case "Delete a Promotion Plan Allocated to an Account"
            Case "Update a Promotion Plan Allocated to an Account"
            Case "Get Payment Profile List"
            Case "Get Voucher Details"
                SentCommand = "GET:VOUCHERDETAIL:VoucherSerialNumber,<Voucher serialnumber>;"
            Case "Standard Voucher Refill"
            Case "Value Voucher Refill"
            Case "Payment / Refill"
            Case "Account Adjustment"
            Case "Get a Subscriber FaF Number List"
            Case "Add a FaF Number to a Subscriber"
            Case "Modify a Subscriber FaF Number"
            Case "Delete a Subscriber FaF Number"
            Case "Get an Account FaF Number List"
            Case "Add a FaF Number to an Account"
            Case "Modify an Account FaF Number"
            Case "Delete an Account FaF Number"
            Case "Get FaF Black List"
            Case "Get FaF Plans"
            Case "Get FaF Indicators"
            Case "Get Languages"
            Case "Get Promotion Plans"
            Case "Get Service Classes"
            Case "Get Business Plans"
            Case "Get Business Plan Info"
            
            
            
            Case "Get Subordinate Subscribers"
            Case "Get Currency Codes"
            Case "Get USSD End Of Call Notification Codes"
            Case "Update Subscriber USSD End Of Call Notification Code"
            Case "Get Account Groups"
            Case "Update Subscriber Account Group"
            Case "Get Service Offering Descriptions"
            Case "Update Subscriber Service Offering"
            Case "Get Service Offering Plans"
            Case "Get Community Charging"
            Case "Get Community Charging Groups"
            Case "Get Community Charging Group"
            Case "Update Subscriber Community Data"
            Case "Change Subscriber SIM"
            Case "Link Subscriber"
            Case "Get Subscriber Information"
            Case "Get Subscriber Personal Details"
            Case "Set Subscriber Personal Details"
        End Select
        CCAPI_Connection.SendData SentCommand + vbCrLf
    Else
        CCAPI_Command = ""
    End If
End Function

