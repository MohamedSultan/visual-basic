VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Begin VB.Form SulSAT2 
   Caption         =   "SUL SAT 2"
   ClientHeight    =   7515
   ClientLeft      =   5565
   ClientTop       =   1455
   ClientWidth     =   11655
   LinkTopic       =   "Form1"
   ScaleHeight     =   7515
   ScaleWidth      =   11655
   Begin VB.TextBox Text1 
      Height          =   2535
      Left            =   1080
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   185
      Text            =   "SulSAT2.frx":0000
      Top             =   4440
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.Frame History_Frame 
      Caption         =   "Account History"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5655
      Left            =   240
      TabIndex        =   236
      Top             =   3480
      Visible         =   0   'False
      Width           =   9495
      Begin VB.ListBox History_more_details_Desc 
         Height          =   2400
         ItemData        =   "SulSAT2.frx":0006
         Left            =   7320
         List            =   "SulSAT2.frx":000D
         TabIndex        =   251
         Top             =   2160
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.ListBox History_more_details_Values 
         Height          =   2400
         ItemData        =   "SulSAT2.frx":002E
         Left            =   5520
         List            =   "SulSAT2.frx":0035
         TabIndex        =   250
         Top             =   2160
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.ListBox History_Field4_list 
         Height          =   4740
         ItemData        =   "SulSAT2.frx":0056
         Left            =   7680
         List            =   "SulSAT2.frx":0058
         TabIndex        =   242
         Top             =   720
         Width           =   1575
      End
      Begin VB.ListBox History_Field3_list 
         Height          =   4740
         ItemData        =   "SulSAT2.frx":005A
         Left            =   6240
         List            =   "SulSAT2.frx":005C
         TabIndex        =   239
         Top             =   720
         Width           =   1695
      End
      Begin VB.ListBox History_Field2_list 
         Height          =   4740
         ItemData        =   "SulSAT2.frx":005E
         Left            =   4680
         List            =   "SulSAT2.frx":0060
         TabIndex        =   240
         Top             =   720
         Width           =   1815
      End
      Begin VB.ListBox History_Field1_list 
         Height          =   4740
         ItemData        =   "SulSAT2.frx":0062
         Left            =   3240
         List            =   "SulSAT2.frx":0064
         TabIndex        =   241
         Top             =   720
         Width           =   1695
      End
      Begin VB.ListBox History_Transaction_Type_List 
         Height          =   4740
         ItemData        =   "SulSAT2.frx":0066
         Left            =   1680
         List            =   "SulSAT2.frx":0068
         TabIndex        =   238
         Top             =   720
         Width           =   1815
      End
      Begin VB.ListBox History_Time_List 
         Height          =   4740
         ItemData        =   "SulSAT2.frx":006A
         Left            =   120
         List            =   "SulSAT2.frx":0071
         TabIndex        =   249
         Top             =   720
         Width           =   1815
      End
      Begin VB.VScrollBar History_VScroll 
         Height          =   4750
         Left            =   9240
         TabIndex        =   237
         Top             =   720
         Width           =   255
      End
      Begin VB.Label History_Label 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Index           =   5
         Left            =   7680
         TabIndex        =   248
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label History_Label 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Index           =   4
         Left            =   6240
         TabIndex        =   247
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label History_Label 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Index           =   3
         Left            =   4680
         TabIndex        =   246
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label History_Label 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Index           =   2
         Left            =   3240
         TabIndex        =   245
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label History_Label 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Index           =   1
         Left            =   1680
         TabIndex        =   244
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label History_Label 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   243
         Top             =   360
         Width           =   1575
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Accumulators"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   4
      Left            =   240
      TabIndex        =   45
      Top             =   3240
      Visible         =   0   'False
      Width           =   9375
      Begin VB.TextBox AccumulatorDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   4680
         TabIndex        =   81
         Top             =   2040
         Width           =   2055
      End
      Begin VB.TextBox AccumulatorDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   4680
         TabIndex        =   80
         Top             =   1680
         Width           =   2055
      End
      Begin VB.TextBox AccumulatorDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   4680
         TabIndex        =   79
         Top             =   1320
         Width           =   2055
      End
      Begin VB.TextBox AccumulatorDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   4680
         TabIndex        =   78
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox AccumulatorDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   4680
         TabIndex        =   77
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox AccumulatorResetDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   2280
         TabIndex        =   67
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox AccumulatorResetDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   2280
         TabIndex        =   66
         Top             =   1680
         Width           =   1095
      End
      Begin VB.TextBox AccumulatorResetDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   2280
         TabIndex        =   65
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox AccumulatorResetDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   2280
         TabIndex        =   64
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox AccumulatorResetDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   2280
         TabIndex        =   63
         Top             =   600
         Width           =   1095
      End
      Begin VB.TextBox Accumulator 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   1560
         TabIndex        =   62
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox AccumulatorStartDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   3480
         TabIndex        =   61
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox Accumulator 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   1560
         TabIndex        =   60
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox AccumulatorStartDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   3480
         TabIndex        =   59
         Top             =   1680
         Width           =   1095
      End
      Begin VB.TextBox Accumulator 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1560
         TabIndex        =   58
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox AccumulatorStartDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   3480
         TabIndex        =   57
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox Accumulator 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   1560
         TabIndex        =   56
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox AccumulatorStartDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   3480
         TabIndex        =   55
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox Accumulator 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   1560
         TabIndex        =   54
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox AccumulatorStartDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   3480
         TabIndex        =   53
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "End Date"
         Height          =   255
         Index           =   8
         Left            =   3480
         TabIndex        =   230
         Top             =   285
         Width           =   1215
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "ID"
         Height          =   255
         Index           =   7
         Left            =   1440
         TabIndex        =   229
         Top             =   280
         Width           =   855
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Description"
         Height          =   255
         Index           =   6
         Left            =   4920
         TabIndex        =   228
         Top             =   285
         Width           =   1095
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Reset Date"
         Height          =   255
         Index           =   5
         Left            =   2280
         TabIndex        =   227
         Top             =   285
         Width           =   1215
      End
      Begin VB.Label Label26 
         Caption         =   "Accumulator 4"
         Height          =   255
         Left            =   240
         TabIndex        =   50
         Top             =   1740
         Width           =   1695
      End
      Begin VB.Label Label8 
         Caption         =   "Accumulator 3"
         Height          =   255
         Left            =   240
         TabIndex        =   49
         Top             =   1380
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Accumulator 5"
         Height          =   255
         Left            =   240
         TabIndex        =   48
         Top             =   2100
         Width           =   1695
      End
      Begin VB.Label Label6 
         Caption         =   "Accumulator 2"
         Height          =   255
         Left            =   240
         TabIndex        =   47
         Top             =   1020
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Accumulator 1"
         Height          =   255
         Left            =   240
         TabIndex        =   46
         Top             =   660
         Width           =   1695
      End
   End
   Begin VB.Frame MSISDN_Frame 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   9255
      Begin VB.CommandButton LoadAll 
         Caption         =   "Load"
         Height          =   320
         Left            =   3960
         TabIndex        =   34
         Top             =   240
         Width           =   975
      End
      Begin VB.ComboBox MSISDNs_List 
         Height          =   315
         Left            =   1320
         TabIndex        =   22
         Text            =   "Combo1"
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label RefillBarred 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   375
         Left            =   5160
         TabIndex        =   222
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label1 
         Caption         =   "MSISDN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   285
         Width           =   975
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Balances"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   1
      Left            =   240
      TabIndex        =   19
      Top             =   3000
      Visible         =   0   'False
      Width           =   9375
      Begin VB.Frame Frame5 
         BorderStyle     =   0  'None
         Height          =   855
         Left            =   5760
         TabIndex        =   231
         Top             =   240
         Width           =   3135
         Begin VB.TextBox Adj_Code 
            Height          =   285
            Left            =   1680
            TabIndex        =   233
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox Adj_Type 
            Height          =   285
            Left            =   240
            TabIndex        =   232
            Top             =   480
            Width           =   1335
         End
         Begin VB.Label Label18 
            Alignment       =   2  'Center
            Caption         =   "Adj Code"
            Height          =   255
            Index           =   9
            Left            =   1680
            TabIndex        =   235
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label Label18 
            Alignment       =   2  'Center
            Caption         =   "Adj Type"
            Height          =   255
            Index           =   10
            Left            =   480
            TabIndex        =   234
            Top             =   120
            Width           =   855
         End
      End
      Begin VB.TextBox DedicatedAccountDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   4320
         TabIndex        =   72
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox DedicatedAccountDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   4320
         TabIndex        =   71
         Top             =   1680
         Width           =   1095
      End
      Begin VB.TextBox DedicatedAccountDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   4320
         TabIndex        =   70
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox DedicatedAccountDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   4320
         TabIndex        =   69
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox DedicatedAccountDesc 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   4320
         TabIndex        =   68
         Top             =   600
         Width           =   1095
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   2880
         TabIndex        =   39
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   2880
         TabIndex        =   38
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   2880
         TabIndex        =   37
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   2880
         TabIndex        =   36
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   2880
         TabIndex        =   35
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   1920
         TabIndex        =   33
         Top             =   2040
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   1920
         TabIndex        =   32
         Top             =   1680
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1920
         TabIndex        =   31
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   1920
         TabIndex        =   30
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   1920
         TabIndex        =   29
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Expiry Date"
         Height          =   255
         Index           =   2
         Left            =   2880
         TabIndex        =   184
         Top             =   280
         Width           =   1215
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Description"
         Height          =   255
         Index           =   1
         Left            =   4320
         TabIndex        =   183
         Top             =   280
         Width           =   1095
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "ID"
         Height          =   255
         Index           =   0
         Left            =   1920
         TabIndex        =   182
         Top             =   280
         Width           =   855
      End
      Begin VB.Label Label22 
         Caption         =   "Dedicated Account 1"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   660
         Width           =   1695
      End
      Begin VB.Label Label21 
         Caption         =   "Dedicated Account 2"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   1020
         Width           =   1695
      End
      Begin VB.Label Label20 
         Caption         =   "Dedicated Account 5"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   2100
         Width           =   1695
      End
      Begin VB.Label Label19 
         Caption         =   "Dedicated Account 3"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1380
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Dedicated Account 4"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   1740
         Width           =   1695
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Vouchers"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   2
      Left            =   240
      TabIndex        =   40
      Top             =   2760
      Visible         =   0   'False
      Width           =   8295
      Begin VB.CommandButton Unbar 
         Caption         =   "Unbar Refill"
         Height          =   255
         Index           =   2
         Left            =   5880
         TabIndex        =   188
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton Standard_Voucher_Refill 
         Caption         =   "Refill"
         Height          =   255
         Index           =   0
         Left            =   4800
         TabIndex        =   119
         Top             =   2400
         Width           =   1095
      End
      Begin VB.TextBox Activation_Number 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   2040
         TabIndex        =   117
         Top             =   2400
         Width           =   2535
      End
      Begin VB.Frame Voucher_Details 
         Height          =   1695
         Left            =   120
         TabIndex        =   90
         Top             =   600
         Visible         =   0   'False
         Width           =   7455
         Begin VB.Frame Frame4 
            Height          =   1695
            Left            =   2280
            TabIndex        =   97
            Top             =   0
            Width           =   2295
            Begin VB.Label Voucher_Subscriber_Number 
               Height          =   255
               Left            =   840
               TabIndex        =   104
               Top             =   1320
               Width           =   1335
            End
            Begin VB.Label Voucher_Time_Stamp 
               Height          =   255
               Left            =   120
               TabIndex        =   103
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label Label29 
               Caption         =   "Subscriber Number"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   99
               Top             =   1080
               Width           =   1575
            End
            Begin VB.Label Label29 
               Caption         =   "Time Stamp"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   98
               Top             =   360
               Width           =   1215
            End
         End
         Begin VB.Frame Frame2 
            Height          =   1695
            Left            =   0
            TabIndex        =   95
            Top             =   0
            Width           =   2295
            Begin VB.Label Label31 
               Caption         =   "Operator_ID"
               Height          =   255
               Left            =   120
               TabIndex        =   110
               Top             =   1200
               Width           =   975
            End
            Begin VB.Label Label30 
               Caption         =   "Agent"
               Height          =   255
               Left            =   120
               TabIndex        =   109
               Top             =   240
               Width           =   1215
            End
            Begin VB.Label Voucher_Operator_ID 
               Height          =   255
               Left            =   1080
               TabIndex        =   102
               Top             =   1200
               Width           =   1095
            End
            Begin VB.Label Voucher_BatchID 
               Height          =   255
               Left            =   1080
               TabIndex        =   101
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label Voucher_Agent 
               Height          =   255
               Left            =   1080
               TabIndex        =   100
               Top             =   240
               Width           =   1095
            End
            Begin VB.Label Label29 
               Caption         =   "Batch ID"
               Height          =   255
               Index           =   5
               Left            =   120
               TabIndex        =   96
               Top             =   720
               Width           =   1215
            End
         End
         Begin VB.Label Voucher_VoucherGroup 
            Height          =   255
            Left            =   6360
            TabIndex        =   108
            Top             =   600
            Width           =   975
         End
         Begin VB.Label Voucher_VoucherStatus 
            Height          =   255
            Left            =   6360
            TabIndex        =   107
            Top             =   960
            Width           =   975
         End
         Begin VB.Label Voucher_VoucherValue 
            Height          =   255
            Left            =   6360
            TabIndex        =   106
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label Voucher_VoucherExpiryDate 
            Height          =   255
            Left            =   6360
            TabIndex        =   105
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label29 
            Caption         =   "Voucher Value"
            Height          =   255
            Index           =   9
            Left            =   4680
            TabIndex        =   94
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label Label29 
            Caption         =   "Voucher Expiry Date"
            Height          =   255
            Index           =   8
            Left            =   4680
            TabIndex        =   93
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label Label29 
            Caption         =   "Voucher Group"
            Height          =   255
            Index           =   7
            Left            =   4680
            TabIndex        =   92
            Top             =   600
            Width           =   1215
         End
         Begin VB.Label Label29 
            Caption         =   "Voucher Status "
            Height          =   255
            Index           =   6
            Left            =   4680
            TabIndex        =   91
            Top             =   960
            Width           =   1215
         End
      End
      Begin VB.CommandButton Get_Voucher 
         Caption         =   "Get Details"
         Height          =   255
         Left            =   4800
         TabIndex        =   89
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox Voucher_serial 
         Height          =   285
         Left            =   2040
         TabIndex        =   87
         Top             =   240
         Width           =   2535
      End
      Begin VB.Frame Refills 
         BorderStyle     =   0  'None
         Height          =   855
         Left            =   120
         TabIndex        =   208
         Top             =   2640
         Visible         =   0   'False
         Width           =   8055
         Begin VB.Label Label32 
            Caption         =   "VoucherGroup"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   218
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label34 
            Caption         =   "VoucherSerialNumber"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   217
            Top             =   600
            Width           =   1815
         End
         Begin VB.Label VoucherGroup 
            Height          =   255
            Index           =   0
            Left            =   1800
            TabIndex        =   216
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label VoucherSerialNumber 
            Height          =   255
            Index           =   0
            Left            =   1800
            TabIndex        =   215
            Top             =   600
            Width           =   1455
         End
         Begin VB.Label RechargeAmountMarket 
            Alignment       =   2  'Center
            Height          =   255
            Index           =   0
            Left            =   6960
            TabIndex        =   214
            Top             =   120
            Width           =   1095
         End
         Begin VB.Label Label35 
            Caption         =   "RechargeAmountMarket"
            Height          =   255
            Index           =   0
            Left            =   4920
            TabIndex        =   213
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label RechargeAmountConverted 
            Alignment       =   2  'Center
            Height          =   255
            Index           =   0
            Left            =   6960
            TabIndex        =   212
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label Label37 
            Caption         =   "RechargeAmountConverted"
            Height          =   255
            Index           =   0
            Left            =   4920
            TabIndex        =   211
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label TransactionAmountRefill 
            Alignment       =   2  'Center
            Height          =   255
            Index           =   0
            Left            =   6960
            TabIndex        =   210
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label Label39 
            Caption         =   "TransactionAmountRefill"
            Height          =   255
            Index           =   0
            Left            =   4920
            TabIndex        =   209
            Top             =   600
            Width           =   1815
         End
      End
      Begin VB.Label Label23 
         Caption         =   "Activation Number"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   118
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label Label28 
         Caption         =   "Serial"
         Height          =   255
         Left            =   240
         TabIndex        =   88
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Promotion Plan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   5
      Left            =   240
      TabIndex        =   82
      Top             =   2520
      Visible         =   0   'False
      Width           =   9495
      Begin VB.ComboBox Promotion_Plans_Desc 
         Height          =   315
         Left            =   1200
         TabIndex        =   226
         Top             =   600
         Width           =   2775
      End
      Begin VB.ComboBox Promotion_Plans_ID 
         Height          =   315
         Left            =   240
         TabIndex        =   225
         Top             =   600
         Width           =   855
      End
      Begin VB.ComboBox Promotion_Plans_Start_Date 
         Height          =   315
         Left            =   4080
         TabIndex        =   224
         Top             =   600
         Width           =   1455
      End
      Begin VB.ComboBox Promotion_Plans_End_Date 
         Height          =   315
         Left            =   5640
         TabIndex        =   223
         Top             =   600
         Width           =   1455
      End
      Begin VB.CommandButton Update_Promotion_Plan 
         Caption         =   "Update"
         Height          =   255
         Left            =   7440
         TabIndex        =   181
         Top             =   1320
         Width           =   1455
      End
      Begin VB.CommandButton Remove_Promo 
         Caption         =   "Remove"
         Height          =   255
         Left            =   7440
         TabIndex        =   180
         Top             =   960
         Width           =   1455
      End
      Begin VB.CommandButton Allocate_Promotion_Plan 
         Caption         =   "Allocate"
         Height          =   255
         Left            =   7440
         TabIndex        =   179
         Top             =   600
         Width           =   1455
      End
      Begin VB.CommandButton PromotionPLans 
         Caption         =   "P.Plans"
         Height          =   255
         Left            =   7440
         TabIndex        =   178
         Top             =   1680
         Width           =   1455
      End
      Begin VB.ListBox Promotion_Plans_End_Date_lst 
         Height          =   1815
         ItemData        =   "SulSAT2.frx":0088
         Left            =   5640
         List            =   "SulSAT2.frx":008A
         TabIndex        =   177
         Top             =   1080
         Width           =   1455
      End
      Begin VB.ListBox Promotion_Plans_Start_Date_lst 
         Height          =   1815
         ItemData        =   "SulSAT2.frx":008C
         Left            =   4080
         List            =   "SulSAT2.frx":008E
         TabIndex        =   176
         Top             =   1080
         Width           =   1455
      End
      Begin VB.ListBox Promotion_Plans_Desc_lst 
         Height          =   1815
         ItemData        =   "SulSAT2.frx":0090
         Left            =   1200
         List            =   "SulSAT2.frx":0092
         TabIndex        =   175
         Top             =   1080
         Width           =   2775
      End
      Begin VB.ListBox Promotion_Plans_ID_lst 
         Height          =   1815
         ItemData        =   "SulSAT2.frx":0094
         Left            =   240
         List            =   "SulSAT2.frx":0096
         TabIndex        =   174
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label25 
         Caption         =   "Description"
         Height          =   375
         Index           =   5
         Left            =   2160
         TabIndex        =   86
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label25 
         Caption         =   "ID"
         Height          =   375
         Index           =   4
         Left            =   480
         TabIndex        =   85
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label25 
         Caption         =   "End Date"
         Height          =   375
         Index           =   2
         Left            =   5880
         TabIndex        =   84
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label25 
         Caption         =   "Start Date"
         Height          =   375
         Index           =   3
         Left            =   4320
         TabIndex        =   83
         Top             =   300
         Width           =   975
      End
   End
   Begin VB.Frame Colors 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   9720
      TabIndex        =   189
      Top             =   120
      Width           =   1695
      Begin VB.Label Green 
         BackColor       =   &H0000C000&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   0
         TabIndex        =   195
         Top             =   120
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label Red_Label 
         Caption         =   "Disconnected"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   194
         Top             =   135
         Width           =   1455
      End
      Begin VB.Label Yellow 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   0
         TabIndex        =   193
         Top             =   120
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label Yellow_Label 
         Caption         =   "Connecting..."
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   255
         Left            =   480
         TabIndex        =   192
         Top             =   135
         Width           =   1455
      End
      Begin VB.Label Red 
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   0
         TabIndex        =   191
         Top             =   120
         Width           =   375
      End
      Begin VB.Label Green_Label 
         Caption         =   "Connected"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   255
         Left            =   480
         TabIndex        =   190
         Top             =   135
         Width           =   1095
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Refill"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   3
      Left            =   240
      TabIndex        =   134
      Top             =   2280
      Visible         =   0   'False
      Width           =   8295
      Begin VB.CommandButton Unbar 
         Caption         =   "Unbar Refill"
         Height          =   375
         Index           =   1
         Left            =   3360
         TabIndex        =   187
         Top             =   1560
         Width           =   975
      End
      Begin VB.Frame Frame12 
         Caption         =   "Promotion Plans"
         Height          =   1935
         Left            =   4680
         TabIndex        =   161
         Top             =   240
         Width           =   3615
         Begin VB.Label PromotionProgressValue 
            Height          =   255
            Left            =   2280
            TabIndex        =   173
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label Label42 
            Caption         =   "PromotionProgressValue"
            Height          =   255
            Left            =   120
            TabIndex        =   172
            Top             =   1440
            Width           =   2055
         End
         Begin VB.Label PromotionPlanAllocStartDate 
            Height          =   255
            Left            =   2280
            TabIndex        =   171
            Top             =   1200
            Width           =   735
         End
         Begin VB.Label Label41 
            Caption         =   "PromotionPlanAllocStartDate"
            Height          =   255
            Left            =   120
            TabIndex        =   170
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label RechargeAmountPromotion 
            Height          =   255
            Left            =   2280
            TabIndex        =   169
            Top             =   960
            Width           =   735
         End
         Begin VB.Label Label40 
            Caption         =   "RechargeAmountPromotion"
            Height          =   255
            Left            =   120
            TabIndex        =   168
            Top             =   960
            Width           =   2055
         End
         Begin VB.Label Label38 
            Height          =   255
            Left            =   2280
            TabIndex        =   167
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label36 
            Caption         =   "AccumulatedRefillCounter"
            Height          =   255
            Left            =   120
            TabIndex        =   166
            Top             =   720
            Width           =   2175
         End
         Begin VB.Label PromotionPlanBefore 
            Height          =   255
            Left            =   2160
            TabIndex        =   165
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label33 
            Caption         =   "PromotionPlanBefore"
            Height          =   255
            Left            =   120
            TabIndex        =   164
            Top             =   480
            Width           =   2055
         End
         Begin VB.Label AccumulatedProgressCounter 
            Height          =   255
            Left            =   2280
            TabIndex        =   163
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label24 
            Caption         =   "AccumulatedProgressCounter"
            Height          =   255
            Left            =   120
            TabIndex        =   162
            Top             =   240
            Width           =   2175
         End
      End
      Begin VB.Frame Frame8 
         Height          =   1215
         Left            =   0
         TabIndex        =   149
         Top             =   225
         Width           =   855
         Begin VB.TextBox Refill_Amount_LE 
            Height          =   285
            Left            =   120
            TabIndex        =   152
            Top             =   720
            Width           =   615
         End
         Begin VB.Frame Frame10 
            Height          =   495
            Left            =   0
            TabIndex        =   150
            Top             =   0
            Width           =   855
            Begin VB.Label Label12 
               Caption         =   "Amount"
               Height          =   255
               Left            =   120
               TabIndex        =   151
               Top             =   120
               Width           =   615
            End
         End
         Begin VB.Label Label17 
            Caption         =   "LE"
            Height          =   255
            Left            =   360
            TabIndex        =   153
            Top             =   480
            Width           =   375
         End
      End
      Begin VB.Frame Frame9 
         Height          =   1215
         Left            =   840
         TabIndex        =   154
         Top             =   225
         Width           =   3855
         Begin VB.ComboBox Payment_Profiles_ID 
            Height          =   315
            Left            =   120
            TabIndex        =   159
            Top             =   720
            Width           =   855
         End
         Begin VB.ComboBox Payment_Profiles_Desc 
            Height          =   315
            Left            =   1080
            TabIndex        =   157
            Top             =   720
            Width           =   2655
         End
         Begin VB.Frame Frame11 
            Height          =   495
            Left            =   0
            TabIndex        =   155
            Top             =   0
            Width           =   3855
            Begin VB.Label Label16 
               Caption         =   "Refill Profile"
               Height          =   255
               Left            =   120
               TabIndex        =   156
               Top             =   120
               Width           =   855
            End
         End
      End
      Begin VB.Frame Frame3 
         Height          =   1575
         Left            =   0
         TabIndex        =   135
         Top             =   2040
         Width           =   8295
         Begin VB.CommandButton Unbar 
            Caption         =   "Unbar Refill"
            Height          =   255
            Index           =   0
            Left            =   5760
            TabIndex        =   186
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox Activation_Number 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   1
            Left            =   1920
            TabIndex        =   137
            Top             =   240
            Width           =   2535
         End
         Begin VB.CommandButton Standard_Voucher_Refill 
            Caption         =   "Refill"
            Height          =   255
            Index           =   1
            Left            =   4680
            TabIndex        =   136
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label23 
            Caption         =   "Activation Number"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   148
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label Label32 
            Caption         =   "VoucherGroup"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   147
            Top             =   720
            Width           =   1335
         End
         Begin VB.Label Label34 
            Caption         =   "VoucherSerialNumber"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   146
            Top             =   1080
            Width           =   1815
         End
         Begin VB.Label VoucherGroup 
            Height          =   255
            Index           =   1
            Left            =   1800
            TabIndex        =   145
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label VoucherSerialNumber 
            Height          =   255
            Index           =   1
            Left            =   1800
            TabIndex        =   144
            Top             =   1080
            Width           =   1455
         End
         Begin VB.Label RechargeAmountMarket 
            Alignment       =   2  'Center
            Height          =   255
            Index           =   1
            Left            =   7080
            TabIndex        =   143
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label Label35 
            Caption         =   "RechargeAmountMarket"
            Height          =   255
            Index           =   1
            Left            =   4920
            TabIndex        =   142
            Top             =   600
            Width           =   1815
         End
         Begin VB.Label RechargeAmountConverted 
            Alignment       =   2  'Center
            Height          =   255
            Index           =   1
            Left            =   7080
            TabIndex        =   141
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label Label37 
            Caption         =   "RechargeAmountConverted"
            Height          =   255
            Index           =   1
            Left            =   4920
            TabIndex        =   140
            Top             =   840
            Width           =   2175
         End
         Begin VB.Label TransactionAmountRefill 
            Alignment       =   2  'Center
            Height          =   255
            Index           =   1
            Left            =   7080
            TabIndex        =   139
            Top             =   1080
            Width           =   1095
         End
         Begin VB.Label Label39 
            Caption         =   "TransactionAmountRefill"
            Height          =   255
            Index           =   1
            Left            =   4920
            TabIndex        =   138
            Top             =   1080
            Width           =   1815
         End
      End
      Begin VB.CommandButton Refill_Cmd 
         Caption         =   "Refill"
         Height          =   375
         Left            =   1800
         TabIndex        =   158
         Top             =   1560
         Width           =   1575
      End
      Begin VB.Label AccumulatedProgressValue 
         Height          =   255
         Left            =   2400
         TabIndex        =   160
         Top             =   1800
         Width           =   1935
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Family And Friends"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   0
      Left            =   240
      TabIndex        =   121
      Top             =   2040
      Visible         =   0   'False
      Width           =   8295
      Begin VB.Frame Frame7 
         Height          =   2535
         Left            =   4080
         TabIndex        =   126
         Top             =   240
         Width           =   4215
         Begin VB.ListBox FAF_MSISDN_Lst 
            Height          =   1425
            ItemData        =   "SulSAT2.frx":0098
            Left            =   360
            List            =   "SulSAT2.frx":009A
            TabIndex        =   128
            Top             =   720
            Width           =   2295
         End
         Begin VB.ListBox FAF_Indicator_Lst 
            Enabled         =   0   'False
            Height          =   1425
            ItemData        =   "SulSAT2.frx":009C
            Left            =   2640
            List            =   "SulSAT2.frx":009E
            TabIndex        =   127
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label Label25 
            Caption         =   "FAF Indicator"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   178
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   6
            Left            =   2760
            TabIndex        =   130
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label25 
            Caption         =   "MSISDN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   178
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   7
            Left            =   1080
            TabIndex        =   129
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame6 
         Height          =   2535
         Left            =   0
         TabIndex        =   122
         Top             =   240
         Width           =   4095
         Begin VB.CommandButton FAF_Delete 
            Caption         =   "Delete"
            Height          =   375
            Left            =   2400
            TabIndex        =   133
            Top             =   1800
            Width           =   1095
         End
         Begin VB.CommandButton FAF_Update 
            Caption         =   "Update"
            Height          =   375
            Left            =   1320
            TabIndex        =   132
            Top             =   1800
            Width           =   1095
         End
         Begin VB.CommandButton FAF_Create 
            Caption         =   "Add"
            Height          =   375
            Left            =   240
            TabIndex        =   131
            Top             =   1800
            Width           =   1095
         End
         Begin VB.ComboBox FAF_Indicators_Desc 
            Height          =   315
            Left            =   2040
            TabIndex        =   125
            Top             =   960
            Width           =   1455
         End
         Begin VB.ComboBox FAF_Indicators_ID 
            Height          =   315
            Left            =   240
            TabIndex        =   124
            Top             =   960
            Width           =   1455
         End
         Begin VB.ComboBox FAF_MSISDN 
            Height          =   315
            Left            =   240
            TabIndex        =   123
            Top             =   360
            Width           =   3255
         End
      End
   End
   Begin VB.Frame Main_Frame 
      Height          =   2175
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   5175
      Begin VB.TextBox Sub_Status 
         Height          =   285
         Left            =   2160
         TabIndex        =   221
         Top             =   1320
         Width           =   1815
      End
      Begin VB.ComboBox Languages_ID 
         Height          =   315
         Left            =   1320
         TabIndex        =   116
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton Get_Languages 
         Caption         =   "Langs"
         Height          =   255
         Left            =   4200
         TabIndex        =   115
         Top             =   600
         Width           =   615
      End
      Begin VB.CommandButton Get_SCs 
         Caption         =   "SCs"
         Height          =   255
         Left            =   4200
         TabIndex        =   114
         Top             =   960
         Width           =   615
      End
      Begin VB.ComboBox Service_Classes_Desc 
         Height          =   315
         Left            =   2160
         TabIndex        =   113
         Top             =   960
         Width           =   1815
      End
      Begin VB.ComboBox Service_Classes_ID 
         Height          =   315
         Left            =   1320
         TabIndex        =   112
         Top             =   960
         Width           =   855
      End
      Begin VB.ComboBox Languages_Desc 
         Height          =   315
         Left            =   2160
         TabIndex        =   111
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox accountValue1 
         Height          =   285
         Left            =   2160
         TabIndex        =   41
         Top             =   1680
         Width           =   1815
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Description"
         Height          =   255
         Index           =   4
         Left            =   2400
         TabIndex        =   220
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "ID"
         Height          =   255
         Index           =   3
         Left            =   1320
         TabIndex        =   219
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Account_Flags 
         Height          =   255
         Left            =   4200
         TabIndex        =   74
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Main Balance"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   1740
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Language"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   660
         Width           =   975
      End
      Begin VB.Label Label9 
         Caption         =   "Sub Status"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1380
         Width           =   1815
      End
      Begin VB.Label Label10 
         Caption         =   "Service Class"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1020
         Width           =   1815
      End
   End
   Begin VB.CommandButton SulSAT_SW 
      Caption         =   "UCIP SulSat"
      Height          =   375
      Left            =   9720
      TabIndex        =   120
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save"
      Height          =   375
      Left            =   9720
      TabIndex        =   23
      Top             =   5520
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   "Response"
      Height          =   615
      Left            =   120
      TabIndex        =   51
      Top             =   6480
      Width           =   11415
      Begin VB.TextBox Response_TExt 
         Height          =   285
         Left            =   840
         TabIndex        =   52
         Top             =   200
         Width           =   9735
      End
   End
   Begin VB.TextBox Success 
      Height          =   1965
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   20
      Top             =   7560
      Width           =   11415
   End
   Begin VB.Frame Dates_Frame 
      Height          =   2175
      Left            =   5280
      TabIndex        =   2
      Top             =   600
      Width           =   4095
      Begin VB.TextBox Activation_Date 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   207
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox MaxSupervisionPeriod 
         Height          =   285
         Left            =   2880
         TabIndex        =   76
         Text            =   "Text1"
         Top             =   1680
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox MaxServiceFeePeriod 
         Height          =   285
         Left            =   2880
         TabIndex        =   75
         Text            =   "Text1"
         Top             =   1320
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox supervisionPeriod 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1680
         TabIndex        =   14
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox serviceFeePeriod 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1680
         TabIndex        =   13
         Top             =   1080
         Width           =   495
      End
      Begin VB.TextBox removalPeriod 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   1680
         TabIndex        =   12
         Top             =   1800
         Width           =   495
      End
      Begin VB.TextBox creditClearancePeriod 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   1680
         TabIndex        =   11
         Top             =   1440
         Width           =   495
      End
      Begin VB.TextBox creditClearanceDate 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   10
         Top             =   1440
         Width           =   1455
      End
      Begin VB.TextBox serviceRemovalDate 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   9
         Top             =   1800
         Width           =   1455
      End
      Begin VB.TextBox serviceFeeDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2280
         TabIndex        =   8
         Top             =   1080
         Width           =   1455
      End
      Begin VB.TextBox supervisionDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2280
         TabIndex        =   7
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label27 
         Caption         =   "Activation Date"
         Height          =   255
         Left            =   240
         TabIndex        =   73
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label25 
         Caption         =   "Date"
         Height          =   375
         Index           =   1
         Left            =   2400
         TabIndex        =   44
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label25 
         Caption         =   "Days"
         Height          =   375
         Index           =   0
         Left            =   1680
         TabIndex        =   43
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label11 
         Caption         =   "Credit Clearance"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label Label13 
         Caption         =   "Supervision"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label14 
         Caption         =   "Service Fee"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Label15 
         Caption         =   "Service Removal"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   1800
         Width           =   1815
      End
   End
   Begin MSWinsockLib.Winsock CCAPI_Connection 
      Left            =   11160
      Top             =   6000
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Frame Frame13 
      Height          =   4815
      Left            =   9600
      TabIndex        =   196
      Top             =   600
      Width           =   1935
      Begin VB.CommandButton Vouchers 
         Caption         =   "Vouchers"
         Height          =   255
         Left            =   120
         TabIndex        =   206
         Top             =   2760
         Width           =   1695
      End
      Begin VB.CommandButton Balances 
         Caption         =   "Balances"
         Height          =   255
         Left            =   120
         TabIndex        =   205
         Top             =   1320
         Width           =   1695
      End
      Begin VB.CommandButton PromoPLan_Command 
         Caption         =   "Promotion Plans"
         Height          =   255
         Left            =   120
         TabIndex        =   204
         Top             =   2040
         Width           =   1695
      End
      Begin VB.CommandButton Accumulator_Command 
         Caption         =   "Accumulators"
         Height          =   255
         Left            =   120
         TabIndex        =   203
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CommandButton Disconnect_Command 
         Caption         =   "Disconnect"
         Height          =   255
         Left            =   120
         TabIndex        =   202
         Top             =   840
         Width           =   1695
      End
      Begin VB.CommandButton Get_History_Command 
         Caption         =   "History"
         Height          =   255
         Left            =   120
         TabIndex        =   201
         Top             =   3480
         Width           =   1695
      End
      Begin VB.CommandButton FAF_Cmd 
         Caption         =   "Family and Friends"
         Height          =   255
         Left            =   120
         TabIndex        =   200
         Top             =   2280
         Width           =   1695
      End
      Begin VB.CommandButton Refill 
         Caption         =   "Refill"
         Height          =   255
         Left            =   120
         TabIndex        =   199
         Top             =   3000
         Width           =   1695
      End
      Begin VB.CommandButton Insert_MSISDN_Command 
         Caption         =   "Insert MSISDN"
         Height          =   255
         Left            =   120
         TabIndex        =   198
         Top             =   360
         Width           =   1695
      End
      Begin VB.CommandButton Activate 
         Caption         =   "Activate"
         Height          =   255
         Left            =   120
         TabIndex        =   197
         Top             =   600
         Width           =   1695
      End
   End
   Begin MSForms.ToggleButton ShowResponses_botton 
      Height          =   375
      Left            =   120
      TabIndex        =   21
      Top             =   7080
      Width           =   11415
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "20135;661"
      Value           =   "0"
      Caption         =   "Show Response Details"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "SulSAT2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim All_Data_received, Last_CCAPI_Response, SentCommand As String
Dim CCAPI_Data_Received As Boolean
Private Sub Disable_All(Optional Enable As Boolean)
    With SulSAT2
        If Enable Then
            .Balances.Enabled = True
            .Vouchers.Enabled = True
            .Accumulator_Command.Enabled = True
            .PromoPLan_Command.Enabled = True
            .Disconnect_Command.Enabled = True
            .Get_History_Command.Enabled = True
            .Insert_MSISDN_Command.Enabled = True
            .LoadAll.Enabled = True
            .Activate.Enabled = True
            .Remove_Promo.Enabled = True
            .Get_Voucher.Enabled = True
            .Save.Enabled = True
            .supervisionPeriod.Enabled = True
            .serviceFeePeriod.Enabled = True
            .FAF_Cmd.Enabled = True
            .Refill.Enabled = True
        Else
            .Refill.Enabled = False
            .FAF_Cmd.Enabled = False
            .serviceFeePeriod.Enabled = False
            .supervisionPeriod.Enabled = False
            .Balances.Enabled = False
            .Vouchers.Enabled = False
            .Accumulator_Command.Enabled = False
            .PromoPLan_Command.Enabled = False
            .Disconnect_Command.Enabled = False
            .Get_History_Command.Enabled = False
            .Insert_MSISDN_Command.Enabled = False
            .LoadAll.Enabled = False
            .Activate.Enabled = False
            .Remove_Promo.Enabled = False
            .Get_Voucher.Enabled = False
            .Save.Enabled = False
        End If
    End With
End Sub

Private Sub Get_All_FAF_Indicators()
    Dim Response As String
    Response = Send_CCAPI("Get FaF Indicators")
    'Parsing All FAF Indicators
    If InStr(1, Response, "ESP:0") <> 0 Then
        Dim FAF_Indicators_Details As CCPAI_Parser_Return
        FAF_Indicators_Details = Parser(Last_CCAPI_Response, Chr(10))
        
        Dim FAF_Indicators() As String
        FAF_Indicators = FAF_Indicators_Details.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, FAF_Indicators(0), "ESP:0:")
        If Temp_Loc <> 0 Then FAF_Indicators(0) = Mid(FAF_Indicators(0), Temp_Loc + Len("ESP:0:"), Len(FAF_Indicators(0)))
        
        FAF_Indicators_ID.Clear
        FAF_Indicators_Desc.Clear
        For I = 0 To UBound(FAF_Indicators)
            FAF_Indicators_Details.Found = False
            ReDim FAF_Indicators_Details.Ret(0)
            If FAF_Indicators(I) <> "" Then
                FAF_Indicators_Details = Parser(FAF_Indicators(I) & ";", Chr(9))
                If FAF_Indicators_Details.Found Then
                    FAF_Indicators_ID.AddItem FAF_Indicators_Details.Ret(0), I
                    FAF_Indicators_Desc.AddItem FAF_Indicators_Details.Ret(1), I
                End If
            End If
        Next I
           FAF_Indicators_ID.Text = FAF_Indicators_ID.List(0)
           FAF_Indicators_Desc.Text = FAF_Indicators_Desc.List(0)
    End If
End Sub

Private Sub Get_All_FAF_Plans()
    'Dim Response As String
    'Response = Send_CCAPI("Get FaF Plans")
End Sub

Private Sub Get_All_Payment_Profile_List()
    Dim Response As String
    Response = Send_CCAPI("Get Payment Profile List")
    'Parsing All FAF Indicators
    If InStr(1, Response, "ESP:0") <> 0 Then
        Dim Payment_Profiles_Details As CCPAI_Parser_Return
        Payment_Profiles_Details = Parser(Last_CCAPI_Response, Chr(10))
        
        Dim Payment_Profiles() As String
        Payment_Profiles = Payment_Profiles_Details.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, Payment_Profiles(0), "ESP:0:")
        If Temp_Loc <> 0 Then Payment_Profiles(0) = Mid(Payment_Profiles(0), Temp_Loc + Len("ESP:0:"), Len(Payment_Profiles(0)))
        
        Payment_Profiles_ID.Clear
        Payment_Profiles_Desc.Clear
        For I = 0 To UBound(Payment_Profiles)
            Payment_Profiles_Details.Found = False
            ReDim Payment_Profiles_Details.Ret(0)
            If Payment_Profiles(I) <> "" Then
                Payment_Profiles_Details = Parser(Payment_Profiles(I) & ";", Chr(9))
                If Payment_Profiles_Details.Found Then
                    Payment_Profiles_ID.AddItem Payment_Profiles_Details.Ret(0), I
                    Payment_Profiles_Desc.AddItem Payment_Profiles_Details.Ret(1), I
                End If
            End If
        Next I
           Payment_Profiles_ID.Text = Payment_Profiles_ID.List(0)
           Payment_Profiles_Desc.Text = Payment_Profiles_Desc.List(0)
    End If
End Sub

Private Sub Accumulator_Command_Click()
    Call Slide_Frame(4)
End Sub

Private Sub Activate_Click()
    Dim Data_Received As String
    Data_Received = Response_Code(GetAccountDetailsT_Command(MSISDNs_List, , "01100000"))
    If InStr(1, Data_Received, "successful") <> 0 Then
        Call Logging("<" + "successful")
        Call LoadAll_Click
    Else
        Call Logging("<" + Data_Received)
    End If
End Sub



Private Sub Activation_Number_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call Standard_Voucher_Refill_Click(Index)
End Sub

Private Sub Allocate_Promotion_Plan_Click()
    Call Send_CCAPI("Allocate a Promotion Plan to a Subscriber", 3, _
                        Promotion_Plans_ID.Text, _
                        SulSAT.Date_Inverted(Promotion_Plans_Start_Date), _
                        SulSAT.Date_Inverted(Promotion_Plans_End_Date))
    'Parsing Subsriber Promotion Plans
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Call Get_Current_Promotion_Plan
    End If
End Sub

Private Sub CCAPI_Connection_Connect()
    'MsgBox "WS-Connected"
    Wait 500
    'SentCommand = "LOGIN:" & Set_Wait_Time_Detailed.Minsat_User_Name(0) & ":" & Set_Wait_Time_Detailed.MINSAT_Live_Password(0) & ";" + vbCrLf
    'SentCommand = "LOGIN:minsat:min_cs;" + vbCrLf
    SentCommand = "LOGIN:vasadm:vasadm;" + vbCrLf
    'SentCommand = "LOGIN:minsat:minsat;" + vbCrLf
    Red.Visible = False
    Red_Label.Visible = False
    Yellow.Visible = True
    Yellow_Label.Visible = True
    Green.Visible = False
    Green_Label.Visible = False
    'Call Logging(">" + Mid(SentCommand, 1, Len(SentCommand) - 1))
    CCAPI_Connection.SendData SentCommand
End Sub

Private Sub CCAPI_Connection_DataArrival(ByVal bytesTotal As Long)
    Dim Data_Received As String
    Call CCAPI_Connection.GetData(Data_Received)
    All_Data_received = All_Data_received + Trim(Data_Received)
    If InStr(1, All_Data_received, ";") <> 0 Then   'Data Finished
        Red.Visible = False
        Red_Label.Visible = False
        Yellow.Visible = False
        Yellow_Label.Visible = False
        Green.Visible = True
        Green_Label.Visible = True
        Call Logging("<" + Mid(All_Data_received, 1, Len(All_Data_received) - 1))
        Last_CCAPI_Response = All_Data_received
        CCAPI_Data_Received = True
        All_Data_received = ""
        Call Disable_All(True)
    End If
End Sub

Private Sub CCAPI_Connection_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox "Connection Error: " & Description, , "Error"
    'MsgBox "Connection Error(" & CStr(Number) & "): " & Description, , "Error"
    Me.CCAPI_Connection.Close
End Sub

Public Sub Logging(ByRef Data As String)
    'Response_TExt = Main.MSISDNS.Text & vbTab & Format(Now, "YYYY-MM-DD HH:NN:SS") & vbTab & Data + vbCrLf
    If InStr(1, Data, "ESP:0") <> 0 And InStr(1, Data, "<") <> 0 Then
        Response_TExt = "Success"
    ElseIf InStr(1, Data, "successful") <> 0 And InStr(1, Data, "<") <> 0 Then
        Response_TExt = "UCIP Success"
    ElseIf InStr(1, Data, ">") <> 0 Then
        Response_TExt = Data
    Else
        Response_TExt = "Failed" + Data
    End If
    Success = Success + Data + vbCrLf
    Success.SelStart = Len(Success)
End Sub

Private Sub accountValue1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub
Public Sub Balances_Click()
    Call Slide_Frame(1)
End Sub
Private Sub Get_Current_FAF()
    Response = Send_CCAPI("Get a Subscriber FaF Number List")
    If InStr(1, Response, "ESP:0") <> 0 Then
        Dim FAF_Records As CCPAI_Parser_Return
        FAF_Records = Parser(Response, "FAF_N")
        
        Dim FAFs() As String
        FAFs = FAF_Records.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, FAFs(0), "ESP:0:")
        If Temp_Loc <> 0 Then FAFs(0) = Mid(FAFs(0), Temp_Loc + Len("ESP:0:"), Len(FAFs(0)))
        
        FAF_MSISDN.Clear
        'FAF_Indicators_ID.Clear
        FAF_MSISDN_Lst.Clear
        FAF_Indicator_Lst.Clear
        For I = 2 To UBound(FAFs)
            FAF_Records.Found = False
            ReDim FAF_Records.Ret(0)
            If FAFs(I) <> "" Then
                FAF_Records = Parser(FAFs(I) & ";", "K")
                If FAF_Records.Found Then
                    FAF_MSISDN.AddItem Trim(FAF_Records.Ret(0)), I - 2
                    FAF_MSISDN_Lst.AddItem Trim(FAF_Records.Ret(0)), I - 2
                    'FAF_Indicators_ID.AddItem FAF_Records.Ret(1), I - 2
                    FAF_Indicator_Lst.AddItem Trim(FAF_Records.Ret(1)), I - 2
                End If
            End If
        Next I
           FAF_MSISDN.Text = FAF_MSISDN.List(0)
           'FAF_Indicators_ID.Text = FAF_Indicators_ID.List(0)
    End If
End Sub



Private Sub DedicateAccount_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub DedicatedAccountExpiryDate_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub FAF_Cmd_Click()
    Call Slide_Frame(0)
    Call Get_Current_FAF
    Call Get_All_FAF_Indicators
    'Call Get_All_FAF_Plans
End Sub
Private Function Get_FAF_List_ID(FAF_Indicator_ As String) As Integer
    For I = 0 To FAF_Indicators_ID.ListCount
        If Trim(FAF_Indicators_Desc.List(I)) = Trim(FAF_Indicator_) Then Get_FAF_List_ID = I
    Next I
End Function
Private Sub FAF_Indicator_Click()
    FAF_MSISDN.Text = FAF_MSISDN.List(FAF_Indicator.ListIndex)
End Sub

Private Sub FAF_Create_Click()
    Dim Response As String
    Response = Send_CCAPI("Add a FaF Number to a Subscriber", 2, FAF_MSISDN, FAF_Indicators_Desc)
    If InStr(1, Response, "ESP:0") <> 0 Then Call Get_Current_FAF
End Sub

Private Sub FAF_Delete_Click()
    Dim Response As String
    Response = Send_CCAPI("Delete a Subscriber FaF Number", 2, FAF_MSISDN, FAF_Indicators_Desc)
    If InStr(1, Response, "ESP:0") <> 0 Then Call Get_Current_FAF
End Sub

Private Sub FAF_Indicator_Lst_Click()
    FAF_MSISDN.Text = FAF_MSISDN.List(FAF_Indicator_Lst.ListIndex)
    FAF_Indicator.Text = FAF_Indicator.List(FAF_Indicator_Lst.ListIndex)
    FAF_MSISDN_Lst.Text = FAF_MSISDN_Lst.List(FAF_Indicator_Lst.ListIndex)
End Sub


Private Sub FAF_Indicators_Desc_Click()
    FAF_Indicators_ID.Text = FAF_Indicators_ID.List(FAF_Indicators_Desc.ListIndex)
End Sub

Private Sub FAF_Indicators_ID_Click()
    FAF_Indicators_Desc.Text = FAF_Indicators_Desc.List(FAF_Indicators_ID.ListIndex)
End Sub

Private Sub FAF_MSISDN_click()
    Dim Current_FAF_Indicator_ID As Integer
    Current_FAF_Indicator_ID = Get_FAF_List_ID(FAF_Indicator_Lst.List(FAF_MSISDN.ListIndex))
    FAF_Indicators_ID.Text = FAF_Indicators_ID.List(Current_FAF_Indicator_ID)
End Sub

Private Sub FAF_MSISDN_Lst_Click()
    FAF_MSISDN.Text = FAF_MSISDN.List(FAF_MSISDN_Lst.ListIndex)
    Dim Current_FAF_Indicator_ID As Integer
    Current_FAF_Indicator_ID = Get_FAF_List_ID(FAF_Indicator_Lst.List(FAF_MSISDN_Lst.ListIndex))
    If FAF_Indicator_Lst.List(FAF_MSISDN_Lst.ListIndex) = FAF_Indicators_Desc.List(Current_FAF_Indicator_ID) Then
        FAF_Indicators_ID.Text = FAF_Indicators_ID.List(Current_FAF_Indicator_ID)
        FAF_Indicators_Desc.Text = FAF_Indicators_Desc.List(Current_FAF_Indicator_ID)
    Else
        FAF_Indicators_ID = ""
        FAF_Indicators_Desc = FAF_Indicator_Lst.List(FAF_MSISDN_Lst.ListIndex)
    End If
    'FAF_Indicator_Lst.Text = FAF_Indicator_Lst.List(FAF_MSISDN_Lst.ListIndex)
End Sub
Private Function Get_FAF_Ind_ID(ByVal FAF_Indicator As String) As Integer
End Function

Private Sub FAF_Update_Click()
    Dim Response As String
    Response = Send_CCAPI("Modify a Subscriber FaF Number", 2, FAF_MSISDN, FAF_Indicators_Desc)
    If InStr(1, Response, "ESP:0") <> 0 Then Call Get_Current_FAF
End Sub



Private Sub History_Date_Click()
    Dim Record() As String
    Dim Step As Integer
    
End Sub
Private Sub History_list_Click(Except As ListBox)
    If Except.ListIndex <= History_Time_List.ListCount - 1 Then
        If Except.Name <> "History_Time_List" Then _
            History_Time_List.ListIndex = Except.ListIndex
        If Except.Name <> "History_Transaction_Type_List" Then _
            History_Transaction_Type_List.ListIndex = Except.ListIndex
        If Except.Name <> "History_Field1_list" Then _
            History_Field1_list.ListIndex = Except.ListIndex
        If Except.Name <> "History_Field2_list" Then _
            History_Field2_list.ListIndex = Except.ListIndex
        If Except.Name <> "History_Field3_list" Then _
            History_Field3_list.ListIndex = Except.ListIndex
        If Except.Name <> "History_Field4_list" Then _
            History_Field4_list.ListIndex = Except.ListIndex
        History_more_details_Values.ListIndex = Except.ListIndex
        History_more_details_Desc.ListIndex = Except.ListIndex
    End If
End Sub
Private Sub Follow_Change_All_Lists(ByRef Current_List_Box As ListBox)
    Call History_list_Click(Current_List_Box)
    Call Details_History_Labels(Current_List_Box.ListIndex)
End Sub

Private Sub History_Field1_list_KeyUp(KeyCode As Integer, Shift As Integer)
    Call Follow_Change_All_Lists(SulSAT2.History_Field1_list)
End Sub

Private Sub History_Field1_list_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Follow_Change_All_Lists(SulSAT2.History_Field1_list)
End Sub

Private Sub History_Field2_list_KeyUp(KeyCode As Integer, Shift As Integer)
    Call Follow_Change_All_Lists(SulSAT2.History_Field2_list)
End Sub

Private Sub History_Field2_list_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Follow_Change_All_Lists(SulSAT2.History_Field2_list)
End Sub

Private Sub History_Field3_list_KeyUp(KeyCode As Integer, Shift As Integer)
    Call Follow_Change_All_Lists(SulSAT2.History_Field3_list)
End Sub

Private Sub History_Field3_list_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Follow_Change_All_Lists(SulSAT2.History_Field3_list)
End Sub

Private Sub History_Field4_list_KeyUp(KeyCode As Integer, Shift As Integer)
    Call Follow_Change_All_Lists(SulSAT2.History_Field3_list)
End Sub

Private Sub History_Field4_list_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Follow_Change_All_Lists(SulSAT2.History_Field4_list)
End Sub

Private Sub History_Time_List_KeyUp(KeyCode As Integer, Shift As Integer)
    Call Follow_Change_All_Lists(SulSAT2.History_Time_List)
End Sub

Private Sub History_Time_List_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Follow_Change_All_Lists(SulSAT2.History_Time_List)
End Sub

Private Sub History_Transaction_Type_List_KeyUp(KeyCode As Integer, Shift As Integer)
    Call Follow_Change_All_Lists(SulSAT2.History_Transaction_Type_List)
End Sub

Private Sub History_Transaction_Type_List_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Follow_Change_All_Lists(SulSAT2.History_Transaction_Type_List)
End Sub
Private Sub Details_History_Labels(Row_Index As Integer)
    History_Label(0) = "Transaction Date"
    History_Label(1) = "Transaction Type"
    Dim History_Type_details As History_Type
    History_Type_details = History_Type_details_Fill()
    With SulSAT2
        Select Case History_Transaction_Type_List.List(Row_Index)
                Case "Adjustment"
                    History_Label(2) = History_Type_details.TransactionAmountC1
                    History_Label(3) = History_Type_details.AccountBalanceC1
                    History_Label(4) = History_Type_details.System
                    History_Label(5) = ""
                Case "Language Change"
                    History_Label(2) = History_Type_details.OriginNodeID
                    History_Label(3) = History_Type_details.NewLanguageCode
                    History_Label(4) = History_Type_details.originNodeType
                    History_Label(5) = ""
                Case "Life Cycle Change"
                    History_Label(2) = History_Type_details.Status
                    History_Label(3) = History_Type_details.OriginNodeID
                    History_Label(4) = History_Type_details.SupervisionPeriodExpiryDate
                    History_Label(5) = History_Type_details.ServiceFeePeriodExpiryDate
                Case "Refill"
                    History_Label(2) = History_Type_details.TransactionAmountC1
                    History_Label(3) = History_Type_details.AccountBalanceC1
                    History_Label(4) = History_Type_details.CardProfile
                    History_Label(5) = History_Type_details.User
                Case Else
                    History_Label(2) = ""
                    History_Label(3) = ""
                    History_Label(4) = ""
                    History_Label(5) = ""
        End Select
    End With
    Call SHow_More_Details
End Sub
Private Sub SHow_More_Details()
    History_More_details_Form.Visible = False
    If History_more_details_Values.List(History_Time_List.ListIndex) <> "" Then
        History_More_details_Form.Left = SulSAT2.Left - History_More_details_Form.Width
        History_More_details_Form.History_Title.Clear
        History_More_details_Form.History_Value.Clear
        History_More_details_Form.Visible = True
        Dim more_Details_Values As CCPAI_Parser_Return
        Dim more_Details_Desc      As CCPAI_Parser_Return
        more_Details_Values = Parser(History_more_details_Values.List(History_Time_List.ListIndex), ",")
        more_Details_Desc = Parser(History_more_details_Desc.List(History_Time_List.ListIndex), ",")
        If more_Details_Values.Found Then
            For I = 0 To UBound(more_Details_Desc.Ret)
                History_More_details_Form.History_Title.AddItem more_Details_Desc.Ret(I)
                History_More_details_Form.History_Value.AddItem more_Details_Values.Ret(I)
            Next I
        End If
    End If
End Sub
Private Function Extend_Text(ByVal Number_of_Characters, ByVal String_To_B_Extended As String) As String
    For I = 1 To Number_of_Characters
        If Len(String_To_B_Extended) >= I Then
            Extend_Text = Extend_Text & Mid(String_To_B_Extended, I, 1)
        Else
            Extend_Text = Extend_Text & " "
        End If
    Next I
End Function
Private Sub History_VScroll_Change()
    If History_VScroll.Value <= History_Time_List.ListCount Then
        History_Time_List.ListIndex = History_VScroll.Value
        History_Transaction_Type_List.ListIndex = History_VScroll.Value
        History_Field1_list.ListIndex = History_VScroll.Value
        History_Field2_list.ListIndex = History_VScroll.Value
        History_Field3_list.ListIndex = History_VScroll.Value
        History_Field4_list.ListIndex = History_VScroll.Value
    End If
End Sub

Private Sub Languages_ID_Click()
    Languages_Desc.Text = Languages_Desc.List(Languages_ID.ListIndex)
End Sub

Private Sub Languages_ID_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Disconnect_Command_Click()
    Dim Disc As Integer
    Dim Response As String
    Disc = MsgBox("Do You Want To Disconnect " & Main.MSISDNS.Text, vbYesNo, "Warning")
    If Disc = 6 Then
        Response = Send_CCAPI("Delete Subscriber")
        If InStr(1, Response, "ESP:0") <> 0 Then
            Call ClearAllTextBoxes(Me)
            Me.Account_Flags = ""
            Me.Sub_Status = ""
            Me.Languages_Desc.Text = ""
            Me.Languages_ID.Text = ""
            Me.Service_Classes_Desc.Text = ""
            Me.Service_Classes_ID.Text = ""
        End If
    End If
End Sub

Private Sub Form_Load()
    Call Disable_All
    Call Load_List(SulSAT2.MSISDNs_List, "MSISDNS")
    
    SulSAT2.Languages_Desc.AddItem "Arabic"
    SulSAT2.Languages_Desc.AddItem "English"
    SulSAT2.Languages_Desc.AddItem "French"
    SulSAT2.Languages_Desc.AddItem "Italian"
    
    SulSAT2.Languages_ID.AddItem "1"
    SulSAT2.Languages_ID.AddItem "2"
    SulSAT2.Languages_ID.AddItem "3"
    SulSAT2.Languages_ID.AddItem "4"
End Sub

Private Sub Form_Terminate()
    CCAPI_Connection.Close
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub
Private Sub Show_History_Frame()
   'Hiding all Frames
    For I = 0 To SulSAT2.Frame.UBound
        If Frame(I).Visible = True Then
            SulSAT2.Frame(I).Top = 2760
            SulSAT2.Frame(I).Left = 120
            While SulSAT2.Frame(I).Width >= 75 And I <> Frame_ID
                SulSAT2.Frame(I).Width = SulSAT2.Frame(I).Width - 75
            Wend
            SulSAT2.Frame(I).Visible = False
            SulSAT2.Frame(I).Width = 8295
        End If
    Next I
    'Hiding Main Frame
    Dim Current_Main_Frame_Width, Current_Dates_Frame_Width As Integer
    Current_Main_Frame_Width = SulSAT2.Main_Frame.Width
    Current_Dates_Frame_Width = Dates_Frame.Width
    While SulSAT2.Main_Frame.Width >= 75 And SulSAT2.Dates_Frame.Width >= 75
        SulSAT2.Dates_Frame.Width = SulSAT2.Dates_Frame.Width - 75
        SulSAT2.Main_Frame.Width = SulSAT2.Main_Frame.Width - 75
    Wend
    SulSAT2.Dates_Frame.Visible = False
    SulSAT2.Main_Frame.Visible = False
    
    SulSAT2.Dates_Frame.Width = Current_Dates_Frame_Width
    SulSAT2.Main_Frame.Width = Current_Main_Frame_Width
    
    'Showing History Frame

    SulSAT2.History_Frame.Top = 720
    SulSAT2.History_Frame.Left = 120
    
    SulSAT2.History_Frame.Width = 75
    SulSAT2.History_Frame.Visible = True
    While SulSAT2.History_Frame.Width < 7815
        SulSAT2.History_Frame.Width = SulSAT2.History_Frame.Width + 75
        'Wait 1
    Wend
    SulSAT2.History_Frame.Width = Dates_Frame.Left + Dates_Frame.Width + 100
End Sub
Private Sub CleanUp_History()
    History_Time_List.Clear
    History_Transaction_Type_List.Clear
    History_Field1_list.Clear
    History_Field2_list.Clear
    History_Field3_list.Clear
    History_Field4_list.Clear
    History_more_details_Values.Clear
    History_more_details_Desc.Clear
    History_VScroll.Max = 0
End Sub
Private Sub Get_History_Command_Click()
    Dim History_Record As Variant
    Dim History_Details As CCPAI_Parser_Return
    
    'History_Frame.Visible = True
    Call CleanUp_History
    History = Send_CCAPI("Get Account History")
        
    If InStr(1, History, "ESP:0") <> 0 And InStr(1, History, "RESP:0;") <> 1 Then
        Call Show_History_Frame
        'Parsing Data into records
        History_Details = Parser(History, "20" & SulSAT2.MSISDNs_List)
        History_Record = History_Details.Ret
        
        Dim Date_Time(1) As String
        Dim Record_Details() As Variant
        For records = 1 To UBound(History_Record) - 1
            'Parsing Record into Fileds
            History_Details.Found = False
            ReDim History_Details.Ret(0)
            History_Details = Parser(History_Record(records), Chr(9))
            
            ReDim Preserve Record_Details(records)
            Record_Details(records) = History_Details.Ret
            If Mid(Record_Details(records)(0), 2, 3) = "200" Then
                    Record_Details(records)(0) = Format( _
                                                    Mid(Record_Details(records)(0), 2, 10) & _
                                                    " " & _
                                                    Mid(Replace(Record_Details(records)(0), ".", ":"), 12, Len(Record_Details(records)(0)) - 10) _
                                                    , "YYYYMMDDhhmm")
            Else
                X = 1
            End If
        Next records
        
        'Sorting Records
        Record_Details = Sort_Complex_Array(Record_Details)
        Call History_Display(Record_Details)
    End If
End Sub

Public Function Sort_Complex_Array(ByVal Complex_Array As Variant) As Variant
    Dim Sorted_Array(), Max As String
    Dim Max_Pos As Integer
    
    For I = 1 To UBound(Complex_Array)
        Success = Success + Str(I) + vbTab & Complex_Array(I)(0) + vbCrLf
    Next I
    
    
    For I = 1 To UBound(Complex_Array)
        'Getting max value and positioin
        For j = 1 To UBound(Complex_Array)
            If Val(Max) <= Val(Complex_Array(j)(0)) Then
                Max = Val(Complex_Array(j)(0))
                Max_Pos = j
            End If
        Next j
        
        ReDim Preserve Sorted_Array(I)
        Sorted_Array(I) = Complex_Array(Max_Pos)
        Max = "0"
        Complex_Array(Max_Pos)(0) = "0"
    Next I
    Sort_Complex_Array = Sorted_Array
End Function

Private Sub Get_Languages_Click()
   Call Send_CCAPI("Get Languages")
       'Parsing All Service Classes
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Dim Languages_Details As CCPAI_Parser_Return
        Languages_Details = Parser(Last_CCAPI_Response, Chr(10))
        
        Dim Languages() As String
        Languages = Languages_Details.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, Languages(0), "ESP:0:")
        If Temp_Loc <> 0 Then Languages(0) = Mid(Languages(0), Temp_Loc + Len("ESP:0:"), Len(Languages(0)))
        
        Languages_ID.Clear
        Languages_Desc.Clear
        For I = 0 To UBound(Languages)
            Languages_Details.Found = False
            ReDim Languages_Details.Ret(0)
            If Languages(I) <> "" Then
                Languages_Details = Parser(Languages(I) & ";", Chr(9))
                If Languages_Details.Found Then
                    Languages_ID.AddItem Languages_Details.Ret(1), I
                    Languages_Desc.AddItem Languages_Details.Ret(0), I
                End If
            End If
        Next I
           Languages_ID.Text = Languages_ID.List(0)
           Languages_Desc.Text = Languages_Desc.List(0)
           Get_Languages.Enabled = False
    End If
    Languages_ID.Text = CCAPI_Current_Data.Languages_ID
    Languages_Desc.Text = CCAPI_Current_Data.Languages_Desc
End Sub

Private Sub Get_SCs_Click()
    Call CCAPI_Combos_Parser("Get Service Classes", Me.Service_Classes_ID, Me.Service_Classes_Desc)
    Service_Classes_ID.Text = CCAPI_Current_Data.Service_Classes_ID
    Get_SCs.Enabled = False
End Sub
Private Sub CCAPI_Combos_Parser(CCAPI_Command_Name As String, Combo_ID As ComboBox, Combo_Desc As ComboBox)
    Call Send_CCAPI(CCAPI_Command_Name)
    'Parsing All Service Classes
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Dim CCAPI_Combos_Parser As CCPAI_Parser_Return
        CCAPI_Combos_Parser = Parser(Last_CCAPI_Response, Chr(10))
        
        Dim CCAPI_Combos_Parsers() As String
        CCAPI_Combos_Parsers = CCAPI_Combos_Parser.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, CCAPI_Combos_Parsers(0), "ESP:0:")
        If Temp_Loc <> 0 Then CCAPI_Combos_Parsers(0) = Mid(CCAPI_Combos_Parsers(0), Temp_Loc + Len("ESP:0:"), Len(CCAPI_Combos_Parsers(0)))
        
        Combo_ID.Clear
        Combo_Desc.Clear
        For I = 0 To UBound(CCAPI_Combos_Parsers)
            CCAPI_Combos_Parser.Found = False
            ReDim CCAPI_Combos_Parser.Ret(0)
            If CCAPI_Combos_Parsers(I) <> "" Then
                CCAPI_Combos_Parser = Parser(CCAPI_Combos_Parsers(I) & ";", Chr(9))
                If CCAPI_Combos_Parser.Found Then
                    Text1 = Text1 + CCAPI_Combos_Parser.Ret(0) + vbTab + CCAPI_Combos_Parser.Ret(1) + vbCrLf
                    Combo_ID.AddItem CCAPI_Combos_Parser.Ret(0), I
                    Combo_Desc.AddItem CCAPI_Combos_Parser.Ret(1), I
                End If
            End If
        Next I
        Combo_ID.Text = Combo_ID.List(0)
        Combo_Desc.Text = Combo_Desc.List(0)
    End If
    'Combo_ID.Text = CCAPI_Current_Data.Service_Classes_ID
    'Combo_Desc.Text = CCAPI_Current_Data.Combo_Desc
End Sub


Private Sub Get_SCs1_Click()
    Call Send_CCAPI("Get Service Classes")
    'Parsing All Service Classes
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Dim Service_Classes_Details As CCPAI_Parser_Return
        Service_Classes_Details = Parser(Last_CCAPI_Response, Chr(10))
        
        Dim Service_Classes() As String
        Service_Classes = Service_Classes_Details.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, Service_Classes(0), "ESP:0:")
        If Temp_Loc <> 0 Then Service_Classes(0) = Mid(Service_Classes(0), Temp_Loc + Len("ESP:0:"), Len(Service_Classes(0)))
        
        Service_Classes_ID.Clear
        Service_Classes_Desc.Clear
        For I = 0 To UBound(Service_Classes)
            Service_Classes_Details.Found = False
            ReDim Service_Classes_Details.Ret(0)
            If Service_Classes(I) <> "" Then
                Service_Classes_Details = Parser(Service_Classes(I) & ";", Chr(9))
                If Service_Classes_Details.Found Then
                    Text1 = Text1 + Service_Classes_Details.Ret(0) + vbTab + Service_Classes_Details.Ret(1) + vbCrLf
                    Service_Classes_ID.AddItem Service_Classes_Details.Ret(0), I
                    Service_Classes_Desc.AddItem Service_Classes_Details.Ret(1), I
                End If
            End If
        Next I
        Service_Classes_ID.Text = Service_Classes_ID.List(0)
        Service_Classes_Desc.Text = Service_Classes_Desc.List(0)
        Get_SCs.Enabled = False
    End If
    Service_Classes_ID.Text = CCAPI_Current_Data.Service_Classes_ID
    Service_Classes_Desc.Text = CCAPI_Current_Data.Service_Classes_Desc
End Sub

Private Sub Get_Voucher_Click()
    CCAPI_Data_Received = False
    Last_CCAPI_Response = ""
    Voucher_Details.Visible = False
    Call Send_CCAPI("Get Voucher Details", 1, Voucher_serial)
    While Not CCAPI_Data_Received
        Wait 500
    Wend
    'Parsing Voucher Data
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Voucher_Details.Visible = True
        Voucher_Agent = CCAPI_Parsing(Last_CCAPI_Response, "Agent")
        Voucher_BatchID = CCAPI_Parsing(Last_CCAPI_Response, "BatchID")
        Voucher_Operator_ID = CCAPI_Parsing(Last_CCAPI_Response, "OperatorID")
        Voucher_Subscriber_Number = CCAPI_Parsing(Last_CCAPI_Response, "SubscriberNumber")
        Voucher_Time_Stamp = CCAPI_Parsing(Last_CCAPI_Response, "TimeStamp")
        Voucher_VoucherExpiryDate = Mid(CCAPI_Parsing(Last_CCAPI_Response, "VoucherExpiryDate"), 1, 8)
        Voucher_VoucherGroup = CCAPI_Parsing(Last_CCAPI_Response, "VoucherGroup")
        Voucher_VoucherValue = CCAPI_Parsing(Last_CCAPI_Response, "VoucherValue")
        Voucher_VoucherStatus = CCAPI_Parsing(Last_CCAPI_Response, "VoucherStatus")
    End If
End Sub

Private Sub Insert_MSISDN_Command_Click()
    Business_PLan_Form.Top = Insert_MSISDN_Command.Top + SulSAT2.Top '- (Business_PLan_Form.Width / 2)
    Call Slide(Business_PLan_Form, Insert_MSISDN_Command.Left + Insert_MSISDN_Command.Width + SulSAT2.Left, Business_PLan_Form.Width, True)
    Call CCAPI_Combos_Parser("Get Business Plans", Business_PLan_Form.Business_Plan_ID, Business_PLan_Form.Business_Plan_Desc)
End Sub
Public Function Send_CCAPI(ByRef CCAPI_Command_Name As String, _
                        Optional Number_Of_Additional_Data As Integer, _
                        Optional ByRef Data1 As String, _
                        Optional ByRef Data2 As String, _
                        Optional ByRef Data3 As String, _
                        Optional ByRef Data4 As String, _
                        Optional ByRef Data5 As String) As String
    If Green.Visible Then
        Select Case Number_Of_Additional_Data
            Case 0
                SentCommand = CCAPI_Command(CCAPI_Command_Name)
            Case 1
                SentCommand = CCAPI_Command(CCAPI_Command_Name, Data1)
            Case 2
                SentCommand = CCAPI_Command(CCAPI_Command_Name, Data1, Data2)
            Case 3
                SentCommand = CCAPI_Command(CCAPI_Command_Name, Data1, Data2, Data3)
            Case 4
                SentCommand = CCAPI_Command(CCAPI_Command_Name, Data1, Data2, Data3, Data4)
            Case 5
                SentCommand = CCAPI_Command(CCAPI_Command_Name, Data1, Data2, Data3, Data4, Data5)
            Case Else
                SentCommand = CCAPI_Command(CCAPI_Command_Name)
        End Select
        Call Disable_All
        Call Logging(">" + SentCommand)
        CCAPI_Data_Received = False
        Last_CCAPI_Response = ""
        CCAPI_Connection.SendData SentCommand & vbCrLf
        While Not CCAPI_Data_Received
            Wait 500
        Wend
        Send_CCAPI = Last_CCAPI_Response
    Else
        MsgBox "Sorry, PLZ Connect To Server", vbCritical, "Error"
    End If
End Function

Public Sub Languages_Desc_Click()
    Languages_ID.Text = Languages_ID.List(Languages_Desc.ListIndex)
End Sub

Public Sub LoadAll_Click()
    Call ClearAllTextBoxes(Me)
    Call Balances_Click
    CCAPI_Data_Received = False
    Call Send_CCAPI("Get Account Information")
    While Not CCAPI_Data_Received
        Wait 500
    Wend
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then Fill_From_CCAPI (Last_CCAPI_Response)
End Sub

Private Sub Payment_Profiles_Desc_Click()
    Payment_Profiles_ID.Text = Payment_Profiles_ID.List(Payment_Profiles_Desc.ListIndex)
End Sub

Private Sub Payment_Profiles_ID_Click()
    Payment_Profiles_Desc.Text = Payment_Profiles_Desc.List(Payment_Profiles_ID.ListIndex)
End Sub

Private Sub Promotion_Plans_Desc_GotFocus()
    SendMessage Promotion_Plans_Desc.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub Promotion_Plans_Desc_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(Promotion_Plans_Desc.Text) = 0) Then
        SendMessage Promotion_Plans_Desc.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage Promotion_Plans_Desc.hwnd, CB_SHOWDROPDOWN, 0, 1
        Call LoadAll_Click
    End If
    If KeyAscii = 32 And Len(Promotion_Plans_Desc.Text) = 0 Then KeyAscii = 0
End Sub


Private Sub Promotion_Plans_Desc_lst_Click()
    Promotion_Plans_ID.Text = Promotion_Plans_ID.List(Promotion_Plans_Desc_lst.ListIndex)
    Promotion_Plans_Desc.Text = Promotion_Plans_Desc.List(Promotion_Plans_Desc_lst.ListIndex)
    Promotion_Plans_Start_Date.Text = Promotion_Plans_Start_Date.List(Promotion_Plans_Desc_lst.ListIndex)
    Promotion_Plans_End_Date.Text = Promotion_Plans_End_Date.List(Promotion_Plans_Desc_lst.ListIndex)
End Sub

Private Sub Promotion_Plans_ID_GotFocus()
    SendMessage Promotion_Plans_ID.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub Promotion_Plans_ID_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(Promotion_Plans_ID.Text) = 0) Then
        SendMessage Promotion_Plans_ID.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage Promotion_Plans_ID.hwnd, CB_SHOWDROPDOWN, 0, 1
        Call LoadAll_Click
    End If
    If KeyAscii = 32 And Len(Promotion_Plans_ID.Text) = 0 Then KeyAscii = 0
End Sub

Private Sub Promotion_Plans_ID_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = Promotion_Plans_ID.Text
        For I = 0 To Promotion_Plans_ID.ListCount - 1
            If StrComp(PSTR, (Left(Promotion_Plans_ID.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                Promotion_Plans_ID.ListIndex = I
            Exit For
            End If
        Next I
        Promotion_Plans_ID.SelStart = Len(PSTR)
        Promotion_Plans_ID.SelLength = Len(Promotion_Plans_ID.Text) - Len(PSTR)
    End If
End Sub
Private Sub MSISDNs_List_GotFocus()
    SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub MSISDNs_List_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(MSISDNs_List.Text) = 0) Then
        SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, 0, 1
        Call LoadAll_Click
    End If
    If KeyAscii = 32 And Len(MSISDNs_List.Text) = 0 Then KeyAscii = 0
End Sub

Private Sub MSISDNs_List_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = MSISDNs_List.Text
        For I = 0 To MSISDNs_List.ListCount - 1
            If StrComp(PSTR, (Left(MSISDNs_List.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                MSISDNs_List.ListIndex = I
            Exit For
            End If
        Next I
        MSISDNs_List.SelStart = Len(PSTR)
        MSISDNs_List.SelLength = Len(MSISDNs_List.Text) - Len(PSTR)
    End If
End Sub

Private Sub MSISDNs_List_LostFocus()
    Call Save_List(MSISDNs_List, "MSISDNS", True)
    Call Roll_Out_Msisdns(MSISDNs_List)
End Sub
Private Sub Get_Current_Promotion_Plan()
    Call Send_CCAPI("Get Promotion Plans Allocated to a Subscriber")
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Dim Promotion_Plan_Details As CCPAI_Parser_Return
        Promotion_Plan_Details = Parser(Last_CCAPI_Response, Chr(10))
        If Promotion_Plan_Details.Found Then
        
        Dim Current_Promotion_Plans() As String
        Current_Promotion_Plans = Promotion_Plan_Details.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, Current_Promotion_Plans(0), "ESP:0:PromotionPlans,")
        If Temp_Loc <> 0 Then Current_Promotion_Plans(0) = Mid(Current_Promotion_Plans(0), Temp_Loc + Len("ESP:0:PromotionPlans,"), Len(Current_Promotion_Plans(0)))
        
        Promotion_Plans_ID.Clear
        Promotion_Plans_Desc.Clear
        Promotion_Plans_Start_Date.Clear
        Promotion_Plans_End_Date.Clear
        Promotion_Plans_ID_lst.Clear
        Promotion_Plans_Desc_lst.Clear
        Promotion_Plans_Start_Date_lst.Clear
        Promotion_Plans_End_Date_lst.Clear
        For I = 0 To UBound(Current_Promotion_Plans)
            Promotion_Plan_Details.Found = False
            ReDim Promotion_Plan_Details.Ret(0)
            If Current_Promotion_Plans(I) <> "" Then
                Promotion_Plan_Details = Parser(Current_Promotion_Plans(I) & ";", Chr(9))
                If Promotion_Plan_Details.Found And Promotion_Plan_Details.Ret(1) <> "" Then
                    Promotion_Plans_ID.AddItem Promotion_Plan_Details.Ret(0), I
                    Promotion_Plans_Desc.AddItem Promotion_Plan_Details.Ret(1), I
                    Promotion_Plans_Start_Date.AddItem SulSAT.Date_Only(Trim(Promotion_Plan_Details.Ret(2))), I
                    Promotion_Plans_End_Date.AddItem SulSAT.Date_Only(Trim(Promotion_Plan_Details.Ret(3))), I
                    
                    Promotion_Plans_ID_lst.AddItem Promotion_Plan_Details.Ret(0), I
                    Promotion_Plans_Desc_lst.AddItem Promotion_Plan_Details.Ret(1), I
                    Promotion_Plans_Start_Date_lst.AddItem SulSAT.Date_Only(Trim(Promotion_Plan_Details.Ret(2))), I
                    Promotion_Plans_End_Date_lst.AddItem SulSAT.Date_Only(Trim(Promotion_Plan_Details.Ret(3))), I
                End If
            End If
        Next I
            Promotion_Plans_ID.Text = Promotion_Plans_ID.List(0)
            Promotion_Plans_Desc.Text = Promotion_Plans_Desc.List(0)
            Promotion_Plans_Start_Date.Text = Promotion_Plans_Start_Date.List(0)
            Promotion_Plans_End_Date.Text = Promotion_Plans_End_Date.List(0)
    End If
End If
End Sub
Private Sub Clear_Promo_Plans()
    With SulSAT2
        .Promotion_Plans_Desc.Clear
        .Promotion_Plans_Desc_lst.Clear
        .Promotion_Plans_End_Date.Clear
        .Promotion_Plans_End_Date_lst.Clear
        .Promotion_Plans_ID.Clear
        .Promotion_Plans_ID_lst.Clear
        .Promotion_Plans_Start_Date.Clear
        .Promotion_Plans_Start_Date_lst.Clear
    End With
End Sub
Private Sub PromoPLan_Command_Click()
    Call Clear_Promo_Plans
    Call Slide_Frame(5)
    Call Get_Current_Promotion_Plan
End Sub
Private Sub Get_All_Promotion_Plans()
    Call Send_CCAPI("Get Promotion Plans")
    'Parsing All Promotion Plans
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Dim Promotion_Plans_Details As CCPAI_Parser_Return
        Promotion_Plans_Details = Parser(Last_CCAPI_Response, Chr(10))
        
        Dim Promotion_Plans() As String
        Promotion_Plans = Promotion_Plans_Details.Ret
        Dim Temp_Loc As Integer
        Temp_Loc = InStr(1, Promotion_Plans(0), "ESP:0:")
        If Temp_Loc <> 0 Then Promotion_Plans(0) = Mid(Promotion_Plans(0), Temp_Loc + Len("ESP:0:"), Len(Promotion_Plans(0)))
        
        Promotion_Plans_ID.Clear
        Promotion_Plans_Desc.Clear
        For I = 0 To UBound(Promotion_Plans)
            Promotion_Plans_Details.Found = False
            ReDim Promotion_Plans_Details.Ret(0)
            If Promotion_Plans(I) <> "" Then
                Promotion_Plans_Details = Parser(Promotion_Plans(I) & ";", Chr(9))
                If Promotion_Plans_Details.Found Then
                    Promotion_Plans_ID.AddItem Promotion_Plans_Details.Ret(0), I
                    Promotion_Plans_Desc.AddItem Promotion_Plans_Details.Ret(1), I
                End If
            End If
        Next I
           Promotion_Plans_ID.Text = Promotion_Plans_ID.List(0)
           Promotion_Plans_Desc.Text = Promotion_Plans_Desc.List(0)
    End If
End Sub
Private Sub Promotion_Plans_Desc_Click()
    Promotion_Plans_ID.Text = Promotion_Plans_ID.List(Promotion_Plans_Desc.ListIndex)
    Promotion_Plans_Start_Date.Text = Promotion_Plans_Start_Date.List(Promotion_Plans_Desc.ListIndex)
    Promotion_Plans_End_Date.Text = Promotion_Plans_End_Date.List(Promotion_Plans_Desc.ListIndex)
End Sub
Private Sub Promotion_Plans_ID_Click()
    Promotion_Plans_Desc.Text = Promotion_Plans_Desc.List(Promotion_Plans_ID.ListIndex)
    Promotion_Plans_Start_Date.Text = Promotion_Plans_Start_Date.List(Promotion_Plans_ID.ListIndex)
    Promotion_Plans_End_Date.Text = Promotion_Plans_End_Date.List(Promotion_Plans_ID.ListIndex)
End Sub

Private Sub Promotion_Plans_ID_lst_Click()
    Promotion_Plans_Desc.Text = Promotion_Plans_Desc.List(Promotion_Plans_ID_lst.ListIndex)
    Promotion_Plans_ID.Text = Promotion_Plans_ID.List(Promotion_Plans_ID_lst.ListIndex)
    Promotion_Plans_Start_Date.Text = Promotion_Plans_Start_Date.List(Promotion_Plans_ID_lst.ListIndex)
    Promotion_Plans_End_Date.Text = Promotion_Plans_End_Date.List(Promotion_Plans_ID_lst.ListIndex)
End Sub

Private Sub PromotionPLans_Click()
        Call Get_All_Promotion_Plans
End Sub

Private Sub Refill_Amount_LE_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Refill_Cmd_Click
End Sub

Private Sub Refill_Click()
    Call Slide_Frame(3)
    Call Get_All_Payment_Profile_List
End Sub
Private Sub RefillProfileID_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Refill_Cmd_Click()
    Dim Response As String
    Response = Send_CCAPI("Payment / Refill", 4, _
                        Trim(Refill_Amount_LE), _
                        Payment_Profiles_ID.Text, _
                        "Sultan", "Sultan2")
                    
    If InStr(1, Response, "ESP:0") <> 0 Then
        VoucherGroup(1) = CCAPI_Parsing(Response, "VoucherGroup")
        accountValue1 = Val(CCAPI_Parsing(Response, "AccountValueAfter")) / 100
        Account_Flags = CCAPI_Parsing(Response, "AccountFlagsAfter")
        PromotionPlanBefore = CCAPI_Parsing(Response, "PromotionPlanBefore")
        serviceFeeDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "ServiceFeeDateAfter"))
        RechargeAmountMarket(1) = CCAPI_Parsing(Response, "RechargeAmountMarket")
        RechargeAmountConverted(1) = CCAPI_Parsing(Response, "RechargeAmountConverted")
        TransactionAmountRefill(1) = CCAPI_Parsing(Response, "TransactionAmountRefill")
        AccumulatedRefillCounter = CCAPI_Parsing(Response, "AccumulatedRefillCounter")
        RechargeAmountPromotion = CCAPI_Parsing(Response, "RechargeAmountPromotion")
        Service_Classes_ID = CCAPI_Parsing(Response, "ServiceClassCurrent")
        AccumulatedProgressCounter = CCAPI_Parsing(Response, "AccumulatedProgressCounter")
        PromotionPlanAllocStartDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "PromotionPlanAllocStartDate"))
        creditClearancePeriod = CCAPI_Parsing(Response, "CreditClearancePeriodAfter")
        removalPeriod = CCAPI_Parsing(Response, "ServiceRemovalGracePeriodAfter")
        supervisionDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "SupervisionPeriodExpiryDateAfter"))
        Sub_Status = CCAPI_Parsing(Response, "Status")
        If CCAPI_Current_Data.Service_Classes_ID = Service_Classes_ID Then
            Service_Classes_Desc = CCAPI_Parsing(Response, "ServiceClassDescription")
        Else
            Service_Classes_Desc = ""
        End If
    End If
End Sub

Private Sub Remove_Promo_Click()
    Dim Response_data As String
    If Promotion_Plans_ID <> "" And Promotion_Plans_Start_Date <> "" And Promotion_Plans_End_Date <> "" Then
        Response_data = Send_CCAPI("Delete a Promotion Plan Allocated to an Account", 3, Promotion_Plans_ID, SulSAT.Date_Inverted(Promotion_Plans_Start_Date), SulSAT.Date_Inverted((Promotion_Plans_End_Date)))
        'Parsing Subsriber Promotion Plans
        If InStr(1, Response_data, "ESP:0") <> 0 Then
            Call Get_Current_Promotion_Plan
        End If
    End If
End Sub



Private Sub Save_Click()
    Dim Data_Received As String
    'update Service Class
    Response_TExt = ""
    If Trim(CCAPI_Current_Data.Service_Classes_ID) <> Trim(SulSAT2.Service_Classes_ID) And _
            Trim(CCAPI_Current_Data.Service_Classes_ID) <> "" And _
            Trim(SulSAT2.Service_Classes_ID) <> "" Then
                'Data_Received = Send_CCAPI("Set Service Class for a Subscriber", 2,
                Data_Received = Response_Code(UpdateServiceClassT_Command(SulSAT2.MSISDNs_List, _
                                Trim(CCAPI_Current_Data.Service_Classes_ID), _
                                Trim(SulSAT2.Service_Classes_ID)))
                If InStr(1, Data_Received, "successful") <> 0 Then
                    Call Logging("<" + "successful")
                    SulSAT2.Service_Classes_Desc = ""
                Else
                    Call Logging("<" + Data_Received)
                End If
    End If
    'Update Language
    If Trim(CCAPI_Current_Data.Languages_ID) <> Trim(SulSAT2.Languages_ID) And _
            Trim(CCAPI_Current_Data.Languages_ID) <> "" And _
            Trim(SulSAT2.Languages_ID) <> "" Then
                Data_Received = Send_CCAPI("Set Subscriber Language", 2, _
                        Trim(CCAPI_Current_Data.Languages_ID), _
                        Trim(SulSAT2.Languages_ID))
        If InStr(1, Data_Received, "ESP:0") <> 0 Then Call Fill_Current_Data
    End If
    'Update Promotion Pplan
    If SulSAT.Date_Inverted(CCAPI_Current_Data.PromotionPlanEndDate) <> SulSAT.Date_Inverted(SulSAT2.Promotion_Plans_End_Date) Or _
        SulSAT.Date_Inverted(CCAPI_Current_Data.PromotionPlanStartDate) <> SulSAT.Date_Inverted(SulSAT2.Promotion_Plans_Start_Date) Then
            Data_Received = Send_CCAPI("Update a Promotion Plan Allocated to an Account", 5, _
                                        Trim(SulSAT2.Promotion_Plans_ID), _
                                        SulSAT.Date_Inverted(CCAPI_Current_Data.PromotionPlanStartDate), _
                                        SulSAT.Date_Inverted(SulSAT2.Promotion_Plans_Start_Date), _
                                        SulSAT.Date_Inverted(CCAPI_Current_Data.PromotionPlanEndDate), _
                                        SulSAT.Date_Inverted(SulSAT2.Promotion_Plans_End_Date))
        If InStr(1, Data_Received, "ESP:0") <> 0 Then Call Get_Current_Promotion_Plan
    End If
    'Account Adjustment ccapi
    'If Val(SulSAT2.accountValue1) > Val(CCAPI_Current_Data.accountValue1) Then _
    '    Data_Received = Send_CCAPI("Account Adjustment", 2, "ADD", _
    '                                Abs(Val(SulSAT2.accountValue1) - Val(CCAPI_Current_Data.accountValue1)))
    'If Val(SulSAT2.accountValue1) < Val(CCAPI_Current_Data.accountValue1) Then _
    '    Data_Received = Send_CCAPI("Account Adjustment", 2, "SUBTRACT", _
    '                                Abs(Val(SulSAT2.accountValue1) - Val(CCAPI_Current_Data.accountValue1)))
    
    
    'Dedicated Account Chnaged ??
    Dim Deds, All_Deds As String
    Dim Adjust_Ded As Boolean
    For I = 1 To Me.DedicateAccount.UBound
        If Val(Me.DedicateAccount(I)) <> Val(CCAPI_Current_Data.DedicateAccount(I)) Or _
            Me.DedicatedAccountExpiryDate(I) <> CCAPI_Current_Data.DedicatedAccountExpiryDate(I) Then
                'With Expiry Dates ??
                If Me.DedicatedAccountExpiryDate(I) <> CCAPI_Current_Data.DedicatedAccountExpiryDate(I) Then
                    Deds = Extention.Dedicated_Constractor(Trim(Str(I)), _
                                                            (DedicateAccount(I) - CCAPI_Current_Data.DedicateAccount(I)) * 100, _
                                                            DedicatedAccountExpiryDate(I))
                Else
                    Deds = Extention.Dedicated_Constractor(Trim(Str(I)), _
                                        (Val(DedicateAccount(I)) - Val(CCAPI_Current_Data.DedicateAccount(I))) * 100)
                    Adjust_Ded = True
                    All_Deds = All_Deds & Deds
                    'Text1 = All_Deds
                    'Text1.Visible = True
                End If
        End If
    Next I
    'Text1 = All_Deds
    'Account Adjustement UCIP
    If Val(Me.accountValue1) <> Val(CCAPI_Current_Data.accountValue1) Or _
        Me.serviceFeePeriod <> CCAPI_Current_Data.serviceFeePeriod Or _
        Me.supervisionPeriod <> CCAPI_Current_Data.supervisionPeriod Or _
        Adjust_Ded Then
            If Not Adjust_Ded Then  'Without Dedicated account ?
                Data_Received = Response_Code(AdjustmentT_Command(Me.MSISDNs_List, _
                                                                    (Val(Me.accountValue1) - Val(CCAPI_Current_Data.accountValue1)) * 100, _
                                                                    False, _
                                                                    Me.supervisionPeriod - CCAPI_Current_Data.supervisionPeriod, _
                                                                    Me.serviceFeePeriod - CCAPI_Current_Data.serviceFeePeriod))
            Else
                Data_Received = Response_Code(AdjustmentT_Command(Me.MSISDNs_List, _
                                                                    (Val(Me.accountValue1) - Val(CCAPI_Current_Data.accountValue1)) * 100, _
                                                                    False, _
                                                                    Me.supervisionPeriod - CCAPI_Current_Data.supervisionPeriod, _
                                                                    Me.serviceFeePeriod - CCAPI_Current_Data.serviceFeePeriod, _
                                                                    , _
                                                                    All_Deds, _
                                                                    Adj_Type, _
                                                                    Adj_Code))
            End If
            If InStr(1, Data_Received, "successful") <> 0 Then
                Call Logging("<" + "successful")
            Else
                Call Logging("<" + Data_Received)
            End If
            Call Fill_Current_Data
            
    End If
    

    
End Sub



Private Sub Service_Classes_Desc_Click()
    Service_Classes_ID.Text = Service_Classes_ID.List(Service_Classes_Desc.ListIndex)
End Sub



Private Sub Promotion_Plans_Desc_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = Promotion_Plans_Desc.Text
        For I = 0 To Promotion_Plans_Desc.ListCount - 1
            If StrComp(PSTR, (Left(Promotion_Plans_Desc.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                Promotion_Plans_Desc.ListIndex = I
            Exit For
            End If
        Next I
        Promotion_Plans_Desc.SelStart = Len(PSTR)
        Promotion_Plans_Desc.SelLength = Len(Promotion_Plans_Desc.Text) - Len(PSTR)
    End If
End Sub





Private Sub service_classes_id_GotFocus()
    SendMessage Service_Classes_ID.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub service_classes_id_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(Service_Classes_ID.Text) = 0) Then
        SendMessage Service_Classes_ID.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage Service_Classes_ID.hwnd, CB_SHOWDROPDOWN, 0, 1
        Call Save_Click
    End If
    If KeyAscii = 32 And Len(Service_Classes_ID.Text) = 0 Then KeyAscii = 0
End Sub

Private Sub service_classes_id_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = Service_Classes_ID.Text
        For I = 0 To Service_Classes_ID.ListCount - 1
            If StrComp(PSTR, (Left(Service_Classes_ID.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                Service_Classes_ID.ListIndex = I
            Exit For
            End If
        Next I
        Service_Classes_ID.SelStart = Len(PSTR)
        Service_Classes_ID.SelLength = Len(Service_Classes_ID.Text) - Len(PSTR)
    End If
End Sub
Private Sub Service_Classes_ID_Change()
'    If Get_SCs.Enabled = False Then
'        Call Service_Classes_ID_Click
'        For I = 0 To .Languages_Desc.ListCount
'            If .Languages_Desc = .Languages_Desc.List(I) Then .Languages_ID.Text = .Languages_ID.List(I)
'        Next I
'    End If
End Sub

Private Sub Service_Classes_ID_Click()
    Service_Classes_Desc.Text = Service_Classes_Desc.List(Service_Classes_ID.ListIndex)
End Sub



Private Sub serviceFeeDate_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub serviceFeeDate_LostFocus()
    If Len(SulSAT.Date_Inverted(serviceFeeDate)) = 8 Then _
            serviceFeePeriod = DateDiff("d", Format(Now, "DD MMM YYYY"), Format(Replace(serviceFeeDate, "\", " "), "DD MMM YYYY"))
End Sub

Private Sub serviceFeePeriod_LostFsocus()
    Me.serviceFeeDate = Replace(Format((DateAdd("d", Val(serviceFeePeriod), Format(Now, "DD MMM YYYY"))), "DD MM YYYY"), " ", "\")
End Sub

Private Sub serviceFeePeriod_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub ShowResponses_botton_Click()
        If Not ShowResponses_botton.Value Then
            Me.Height = Me.Height - 2300
            ShowResponses_botton.Value = 0
            ShowResponses_botton.Caption = "Show Response Details"
        Else
            Me.Height = Me.Height + 2300
            ShowResponses_botton.Value = 1
            ShowResponses_botton.Caption = "Hide Response Details"
    End If
End Sub

Private Sub Standard_Voucher_Refill_Click(Index As Integer)
    Refills.Visible = False
    Dim Response As String
    Response = Send_CCAPI("Standard Voucher Refill", 3, Activation_Number(Index), "Sultan1", "Sultan2")
    If InStr(1, Response, "ESP:0") <> 0 Then
        Refills.Visible = True
        VoucherGroup(Index) = CCAPI_Parsing(Response, "VoucherGroup")
        accountValue1 = CCAPI_Parsing(Response, "AccountValueAfter")
        Account_Flags = CCAPI_Parsing(Response, "AccountFlagsAfter")
        serviceFeeDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "ServiceFeeDateAfter"))
        Service_Classes_ID = CCAPI_Parsing(Response, "ServiceClassCurrent")
        VoucherSerialNumber(Index) = CCAPI_Parsing(Response, "VoucherSerialNumber")
        RechargeAmountMarket(Index) = CCAPI_Parsing(Response, "RechargeAmountMarket")
        RechargeAmountConverted(Index) = CCAPI_Parsing(Response, "RechargeAmountConverted")
        TransactionAmountRefill(Index) = CCAPI_Parsing(Response, "TransactionAmountRefill")
        creditClearancePeriod = CCAPI_Parsing(Response, "CreditClearancePeriodAfter")
        removalPeriod = CCAPI_Parsing(Response, "ServiceRemovalGracePeriodAfter")
        supervisionDate = SulSAT.Date_Only(CCAPI_Parsing(Response, "SupervisionPeriodExpiryDateAfter"))
        Sub_Status = CCAPI_Parsing(Response, "Status")
        If CCAPI_Current_Data.Service_Classes_ID = Service_Classes_ID Then
            Service_Classes_Desc = CCAPI_Parsing(Response, "ServiceClassDescription")
        Else
            Service_Classes_Desc = ""
        End If
        
        Call Fill_Current_Data
    End If
End Sub

Private Sub SulSAT_SW_Click()
    Call Main2.UCIP_Command_Click
End Sub

Private Sub supervisionDate_LostFocus()
    X = Format(Replace(supervisionDate, "\", " "), "DD MMM YYYY")
    If Len(SulSAT.Date_Inverted(supervisionDate)) = 8 Then _
        supervisionPeriod = DateDiff("d", Format(Now, "DD MMM YYYY"), Format(Replace(supervisionDate, "\", " "), "DD MMM YYYY"))
End Sub

Private Sub supervisionDate_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call supervisionDate_LostFocus
        Call Save_Click
    End If
End Sub

Private Sub supervisionPeriod_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call supervisionPeriod_LostFocus
        Call Save_Click
    End If
End Sub


Public Function ToDate(ByRef Date_String As String) As String
    Dim DD, MM, YYYY, Loc1, Loc2 As Integer
    
    Loc1 = InStr(1, Date_String, "\")
    DD = Val(Mid(Date_String, 1, Loc1 - 1))
    Loc2 = InStr(Loc1 + 1, Date_String, "\")
    MM = Val(Mid(Date_String, Loc1 + 1, Loc2 - Loc1 - 1))
    YYYY = Val(Mid(Date_String, Loc2 + 1, 4))
    
    
    Select Case MM
        Case 1
            ToDate = Str(DD) + " Jan " + Str(YYYY)
        Case 2
            ToDate = Str(DD) + " Feb " + Str(YYYY)
        Case 3
            ToDate = Str(DD) + " Mar " + Str(YYYY)
        Case 4
            ToDate = Str(DD) + " Apr " + Str(YYYY)
        Case 5
            ToDate = Str(DD) + " May " + Str(YYYY)
        Case 6
            ToDate = Str(DD) + " Jun " + Str(YYYY)
        Case 7
            ToDate = Str(DD) + " Jul " + Str(YYYY)
        Case 8
            ToDate = Str(DD) + " Aug " + Str(YYYY)
        Case 9
            ToDate = Str(DD) + " Sep " + Str(YYYY)
        Case 10
            ToDate = Str(DD) + " Oct " + Str(YYYY)
        Case 11
            ToDate = Str(DD) + " Nov " + Str(YYYY)
        Case 12
            ToDate = Str(DD) + " Dec " + Str(YYYY)
    End Select
End Function


Private Sub Slide_Frame(Frame_ID As Integer)
    'Showing Main Frame
    If SulSAT2.Main_Frame.Visible = False Or SulSAT2.Dates_Frame.Visible = False Then
        Dim Current_Main_Frame_Width, Current_Dates_Frame_Width As Integer
        Current_Main_Frame_Width = SulSAT2.Main_Frame.Width
        Current_Dates_Frame_Width = SulSAT2.Dates_Frame.Width
        
        'Showing Main and Dates Frames
        SulSAT2.Main_Frame.Width = 75
        SulSAT2.Dates_Frame.Width = 75
        SulSAT2.Main_Frame.Visible = True
        SulSAT2.Dates_Frame.Visible = True
        While SulSAT2.Main_Frame.Width < Current_Main_Frame_Width And SulSAT2.Dates_Frame.Width < Current_Dates_Frame_Width
            SulSAT2.Main_Frame.Width = SulSAT2.Main_Frame.Width + 75
            SulSAT2.Dates_Frame.Width = SulSAT2.Dates_Frame.Width + 75
            'Wait 1
        Wend
        SulSAT2.Main_Frame.Width = Current_Main_Frame_Width
        SulSAT2.Dates_Frame.Width = Current_Dates_Frame_Width
    End If
    'hiding all other frames
    For I = 0 To SulSAT2.Frame.UBound
        If SulSAT2.Frame(I).Visible = True Then
            While SulSAT2.Frame(I).Width >= 75 And I <> Frame_ID
                SulSAT2.Frame(I).Width = SulSAT2.Frame(I).Width - 75
            '    Wait 1
            Wend
            SulSAT2.Frame(I).Visible = False
            SulSAT2.Frame(I).Width = Dates_Frame.Left + Dates_Frame.Width - 100
        End If
    Next I
    'Hiding History Frame
    If SulSAT2.History_Frame.Visible = True Then
        While SulSAT2.History_Frame.Width >= 75
            SulSAT2.History_Frame.Width = SulSAT2.History_Frame.Width - 75
        Wend
        SulSAT2.History_Frame.Visible = False
        SulSAT2.History_Frame.Width = Dates_Frame.Left + Dates_Frame.Width - 100
    End If
    
    'showing Frame(id)
    SulSAT2.Frame(Frame_ID).Width = 75
    SulSAT2.Frame(Frame_ID).Visible = True
    SulSAT2.Frame(Frame_ID).Top = Main_Frame.Top + Main_Frame.Height + 100
    SulSAT2.Frame(Frame_ID).Left = 120
    While SulSAT2.Frame(Frame_ID).Width < Dates_Frame.Left + Dates_Frame.Width
        SulSAT2.Frame(Frame_ID).Width = SulSAT2.Frame(Frame_ID).Width + 75
        'Wait 1
    Wend
    SulSAT2.Frame(Frame_ID).Width = Dates_Frame.Left + Dates_Frame.Width - 100
End Sub

Private Sub supervisionPeriod_LostFocus()
    Me.supervisionDate = Replace(Format((DateAdd("d", Val(supervisionPeriod), Format(Now, "DD MMM YYYY"))), "DD MM YYYY"), " ", "\")
End Sub
Private Function Get_Promo_ID(Promotion_Plan_ID) As Promotion_Plan_Type
    For I = 0 To Promotion_Plans_ID.ListCount
        If Promotion_Plans_ID.List(I) = Promotion_Plan_ID Then
            Get_Promo_ID.Desc = Promotion_Plans_Desc.List(I)
            Get_Promo_ID.ID = Promotion_Plans_ID.List(I)
            Get_Promo_ID.Start_Date = Promotion_Plans_Start_Date.List(I)
            Get_Promo_ID.End_date = Promotion_Plans_End_Date.List(I)
        End If
    Next I
End Function


Private Sub test_Click()

End Sub

Private Sub Unbar_Click(Index As Integer)
    Dim Response As String
    Response = Send_CCAPI("Reset Subscriber Refill Barrings")
    If InStr(1, Response, "ESP:0") Then
            Standard_Voucher_Refill(0).Enabled = True
            Standard_Voucher_Refill(1).Enabled = True
            Refill_Cmd.Enabled = True
            For I = 0 To Unbar.UBound
                Unbar(I).Visible = False
            Next I
            RefillBarred = ""
    End If
End Sub

Private Sub Update_Promotion_Plan_Click()
    Dim Current_Promo_Plan As Promotion_Plan_Type
    Current_Promo_Plan = Get_Promo_ID(Promotion_Plans_ID)
    
    Call Send_CCAPI("Update a Promotion Plan Allocated to an Account", 5, _
                        Promotion_Plans_ID.Text, _
                        SulSAT.Date_Inverted(Current_Promo_Plan.Start_Date), _
                        SulSAT.Date_Inverted(Promotion_Plans_Start_Date), _
                        SulSAT.Date_Inverted(Current_Promo_Plan.End_date), _
                        SulSAT.Date_Inverted(Promotion_Plans_End_Date))
    'Parsing Subsriber Promotion Plans
    If InStr(1, Last_CCAPI_Response, "ESP:0") <> 0 Then
        Call Get_Current_Promotion_Plan
    End If
                                
End Sub

Private Sub Voucher_Serial_Copy_Click(Index As Integer)

End Sub

Private Sub Voucher_serial_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Get_Voucher_Click
End Sub

Private Sub Vouchers_Click()
    Call Slide_Frame(2)
End Sub

