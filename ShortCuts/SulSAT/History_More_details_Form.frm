VERSION 5.00
Begin VB.Form History_More_details_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "More Details"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   4110
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.VScrollBar History_VScroll 
      Height          =   2175
      Left            =   3840
      TabIndex        =   2
      Top             =   0
      Width           =   255
   End
   Begin VB.ListBox History_Value 
      Height          =   2205
      Left            =   2160
      TabIndex        =   1
      Top             =   0
      Width           =   1935
   End
   Begin VB.ListBox History_Title 
      Height          =   2205
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2535
   End
End
Attribute VB_Name = "History_More_details_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub History_Title_KeyUp(KeyCode As Integer, Shift As Integer)
    Call History_list_Click(Me.History_Title)
End Sub

Private Sub History_Title_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call History_list_Click(Me.History_Title)
End Sub

Private Sub History_list_Click(Except As ListBox)
    If Except.Name <> "History_Title" Then _
        History_Title.ListIndex = Except.ListIndex
    If Except.Name <> "History_Value" Then _
        History_Value.ListIndex = Except.ListIndex
End Sub

Private Sub History_Value_KeyUp(KeyCode As Integer, Shift As Integer)
    Call History_list_Click(Me.History_Value)
End Sub

Private Sub History_Value_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call History_list_Click(Me.History_Value)
End Sub

Private Sub History_VScroll_Change()
    If History_VScroll.Value < History_Title.ListCount Then
        History_Title.ListIndex = History_VScroll.Value
        History_Value.ListIndex = History_VScroll.Value
    End If
End Sub
