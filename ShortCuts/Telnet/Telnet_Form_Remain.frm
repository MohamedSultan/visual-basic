VERSION 5.00
Begin VB.Form Telnet_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Telnet Form"
   ClientHeight    =   8925
   ClientLeft      =   7245
   ClientTop       =   1245
   ClientWidth     =   4125
   Icon            =   "Telnet_Form_Remain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8925
   ScaleWidth      =   4125
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Connect 
      Caption         =   "Connect"
      Height          =   615
      Left            =   2520
      TabIndex        =   7
      Top             =   8040
      Width           =   855
   End
   Begin VB.TextBox Password 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Text            =   "peri"
      Top             =   8160
      Width           =   975
   End
   Begin VB.TextBox IP_Address 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Text            =   "172.28.6.15"
      Top             =   8520
      Width           =   975
   End
   Begin VB.TextBox UserName 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Text            =   "peri"
      Top             =   7800
      Width           =   975
   End
   Begin Project1.Telnet Telnet1 
      Left            =   1920
      Top             =   0
      _ExtentX        =   900
      _ExtentY        =   900
   End
   Begin VB.Timer Telnet_Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   0
      Top             =   0
   End
   Begin VB.TextBox Display 
      Height          =   7575
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   3855
   End
   Begin VB.Label Label3 
      Caption         =   "Password"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   8160
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "IP Address"
      Height          =   255
      Left            =   480
      TabIndex        =   5
      Top             =   8520
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "User Name"
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   7800
      Width           =   1215
   End
End
Attribute VB_Name = "Telnet_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

    Private Type NOTIFYICONDATA
        cbSize As Long
        hwnd As Long
        uID As Long
        uFlags As Long
        uCallbackMessage As Long
        hIcon As Long
        szTip As String * 64
    End Type
       
    Const NIM_ADD = 0
    Const NIM_MODIFY = 1
    Const NIM_DELETE = 2
    Const NIF_MESSAGE = 1
    Const NIF_ICON = 2
    Const NIF_TIP = 4
    
    Const WM_MOUSEMOVE = &H200
    Const WM_LBUTTONDOWN = &H201
    Const WM_LBUTTONUP = &H202
    Const WM_LBUTTONDBLCLK = &H203
    Const WM_RBUTTONDOWN = &H204
    Const WM_RBUTTONUP = &H205
    Const WM_RBUTTONDBLCLK = &H206
    Const WM_MBUTTONDOWN = &H207
    Const WM_MBUTTONUP = &H208
    Const WM_MBUTTONDBLCLK = &H209
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Private Declare Function Shell_NotifyIconA Lib "SHELL32" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Integer
Private Function setNOTIFYICONDATA(hwnd As Long, ID As Long, Flags As Long, CallbackMessage As Long, Icon As Long, Tip As String) As NOTIFYICONDATA
    Dim nidTemp As NOTIFYICONDATA

    nidTemp.cbSize = Len(nidTemp)
    nidTemp.hwnd = hwnd
    nidTemp.uID = ID
    nidTemp.uFlags = Flags
    nidTemp.uCallbackMessage = CallbackMessage
    nidTemp.hIcon = Icon
    nidTemp.szTip = Tip & Chr$(0)

    setNOTIFYICONDATA = nidTemp
End Function

Public Sub Connect_Click()
            Telnet_Form.Telnet1.RemotePort = 23
            Telnet_Form.Telnet1.Connect IP_Address
            Telnet_Form.Telnet_Timer.Enabled = True
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
                
                Wait 4000
                Dim I As Integer
                Dim s As String
                Dim nid As NOTIFYICONDATA
                
                s = "Telnet is in System Tray"
                nid = setNOTIFYICONDATA(hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Icon, s)
                I = Shell_NotifyIconA(NIM_ADD, nid)
                
                WindowState = vbMinimized
                Visible = False
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Telnet_Timer_Timer()
    If InStr(1, Telnet_Form.Display, "login:") <> 0 And InStr(1, Telnet_Form.Display, "Last login:") = 0 Then
        Telnet_Form.Telnet1.SendData UserName, True
        Telnet_Form.Display = ""
        Telnet_Form.Telnet_Timer.Enabled = False
    End If
    If InStr(1, Telnet_Form.Display, "Password:") <> 0 Then
        Telnet_Form.Telnet1.SendData Password, True
        Telnet_Form.Display = ""
        Telnet_Form.Telnet_Timer.Enabled = False
        'Wait 1000
        Telnet_Form.Telnet1.SendData "NETSTAT", True
        'Call Slide(Telnet_Form, Telnet_Form.Left + Telnet_Form.Width, Telnet_Form.Width, False)
        
        Dim I As Integer
        Dim s As String
        Dim nid As NOTIFYICONDATA
        
        s = "Telnet is in System Tray"
        nid = setNOTIFYICONDATA(hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Icon, s)
        I = Shell_NotifyIconA(NIM_ADD, nid)
        
        WindowState = vbMinimized
        Visible = False
        
        'Main.Remain.Enabled = False
        'Main.TelnetActivated = "True"
        
        'Telnet_Form.Visible = False
    End If
End Sub
Private Sub Telnet1_DataRecieved(ByVal Data As String)
    Telnet_Form.Telnet_Timer.Enabled = True
    Telnet_Form.Display = Telnet_Form.Display & Data
    Telnet_Form.Display.SelStart = Len(Telnet_Form.Display)
End Sub
