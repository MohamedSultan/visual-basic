VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.UserControl Telnet 
   ClientHeight    =   270
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   270
   InvisibleAtRuntime=   -1  'True
   Picture         =   "Telnet.ctx":0000
   ScaleHeight     =   270
   ScaleWidth      =   270
   ToolboxBitmap   =   "Telnet.ctx":1E72
   Begin MSWinsockLib.Winsock Winsock 
      Left            =   480
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
End
Attribute VB_Name = "Telnet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Rem ==================== Telnet Commands =============================
     Const TelnetCommand_SE = 240:       Rem    End of sub-negotiation parameters.
     Const TelnetCommand_NOP = 241:      Rem    No operation.
     Const TelnetCommand_DM = 242:       Rem    Data Mark; The data stream portion of a Synch. This should always be accompanied by a TCP Urgent notification.
     Const TelnetCommand_BRK = 243:      Rem    NVT character Break.
     Const TelnetCommand_IP = 244:       Rem    Interrupt Process.
     Const TelnetCommand_AO = 245:       Rem    Abort output.
     Const TelnetCommand_AYT = 246:      Rem    Are You There.
     Const TelnetCommand_EC = 247:       Rem    Erase character.
     Const TelnetCommand_EL = 248:       Rem    Erase Line.
     Const TelnetCommand_GA = 249:       Rem    The Go ahead signal.
     Const TelnetCommand_SB = 250:       Rem    Indicates that what follows is sub-negotiation of the indicated option.
     Const TelnetCommand_WILL = 251:     Rem    Indicates the desire to begin performing, or confirmation that you are now performing, the indicated option.
     Const TelnetCommand_WONT = 252:     Rem    Indicates the refusal to perform, or continue performing, the indicated option.
     Const TelnetCommand_DO = 253:       Rem    Indicates the request that the other party perform, or confirmation that you are expecting the other party to perform the indicated option.
     Const TelnetCommand_DONT = 254:     Rem    Indicates the demand that the other party stop performing, or confirmation that you are no longer expecting the other party to perform, the indicated option.
     Const Telnet_IAC = 255:             Rem    Interpret As Command.
    
    Rem ==================== Telnet Options =============================
     Const TelnetOption_BIN = 0:         Rem    Binary Transmission
     Const TelnetOption_ECHO = 1:        Rem    Echo
     Const TelnetOption_RECN = 2:        Rem    Reconnection
     Const TelnetOption_SUPP = 3:        Rem    Suppress Go Ahead
     Const TelnetOption_APRX = 4:        Rem    Approx Message Size Negotiation
     Const TelnetOption_STAT = 5:        Rem    Status
     Const TelnetOption_TIM = 6:         Rem    Timing Mark
     Const TelnetOption_REM = 7:         Rem    Remote Controlled Trans and Echo
     Const TelnetOption_OLW = 8:         Rem    Output Line Width
     Const TelnetOption_OPS = 9:         Rem    Output Page Size
     Const TelnetOption_OCRD = 10:       Rem    Output Carriage-Return Disposition
     Const TelnetOption_OHT = 11:        Rem    Output Horizontal Tabstops
     Const TelnetOption_OHTD = 12:       Rem    Output Horizontal Tab Disposition
     Const TelnetOption_OFD = 13:        Rem    Output Formfeed Disposition
     Const TelnetOption_OVT = 14:        Rem    Output Vertical Tabstops
     Const TelnetOption_OVTD = 15:       Rem    Output Vertical Tab Disposition
     Const TelnetOption_OLD = 16:        Rem    Output Linefeed Disposition
     Const TelnetOption_EXT = 17:        Rem    Extended ASCII
     Const TelnetOption_LOGO = 18:       Rem    Logout
     Const TelnetOption_BYTE = 19:       Rem    Byte Macro
     Const TelnetOption_DATA = 20:       Rem    Data Entry Terminal
     Const TelnetOption_SUP = 21:        Rem    SUPDUP
     Const TelnetOption_SUPO = 22:       Rem    SUPDUP Output
     Const TelnetOption_SNDL = 23:       Rem    Send Location
     Const TelnetOption_TERM = 24:       Rem    Terminal Type
     Const TelnetOption_EOR = 25:        Rem    End of Record
     Const TelnetOption_TACACS = 26:     Rem    TACACS User Identification
     Const TelnetOption_OM = 27:         Rem    Output Marking
     Const TelnetOption_TLN = 28:        Rem    Terminal Location Number
     Const TelnetOption_3270 = 29:       Rem    Telnet 3270 Regime
     Const TelnetOption_X_3 = 30:        Rem    X.3 PAD
     Const TelnetOption_NAWS = 31:       Rem    Negotiate About Window Size
     Const TelnetOption_TS = 32:         Rem    Terminal Speed
     Const TelnetOption_RFC = 33:        Rem    Remote Flow Control
     Const TelnetOption_LINE = 34:       Rem    Linemode
     Const TelnetOption_XDL = 35:        Rem    X Display Location
     Const TelnetOption_ENVIR1 = 36:     Rem    Telnet Environment Option
     Const TelnetOption_AUTH = 37:       Rem    Telnet Authentication Option
     Const TelnetOption_ENVIR2 = 39:     Rem    Telnet Environment Option
     Const TelnetOption_TN3270 = 40:     Rem    E TN3270 Enhancements
     Const TelnetOption_XAUTH = 41:      Rem    Telnet XAUTH
     Const TelnetOption_CHARSE = 42:     Rem    T Telnet CHARSET
     Const TelnetOption_RSP = 43:        Rem    Telnet Remote Serial Port
     Const TelnetOption_COMPOR = 44:     Rem    T Telnet Com Port Control
     Const TelnetOption_SLE = 45:        Rem    Telnet Suppress Local Echo
     Const TelnetOption_STARTT = 46:     Rem    LS Telnet Start TLS
     Const TelnetOption_KERMIT = 47:     Rem    Telnet KERMIT
     Const TelnetOption_SEND_URL = 48:   Rem    Send-URL
     Const TelnetOption_EXTOP = 255:     Rem    Extended-Options-List
    
    Rem =================================================================
    Rem =================================================================


'----------------------------------------------------------------------
'General Variables and Parameters
'----------------------------------------------------------------------
Dim TempData As String                      'Variable containing reurned Data
Dim WaitForWhat As String                   'String to wait for before returning data
'Variables to Hold Properties Values (For both Nodes)
Dim HostIP_V As String
Dim RemotePort_V As Integer
'----------------------------------------------------------------------
'Events raised by the Control
'----------------------------------------------------------------------
Public Event DataRecieved(ByVal Data As String)
Public Event ErrorOccured(ByVal ErrorDesc As String)
Public Event Connected()
Public Event Disconnected()
'----------------------------------------------------------------------
'Properties needed by the Control
'----------------------------------------------------------------------
Public Property Get HostIP() As String
Attribute HostIP.VB_Description = "The IP/Host Name used to establish a telnet session with the remote host"
    HostIP = HostIP_V
End Property
Public Property Let HostIP(ByVal vNewValue As String)
    HostIP_V = vNewValue
    PropertyChanged nodeName
End Property

Public Property Get RemotePort() As Integer
Attribute RemotePort.VB_Description = "Port used for telnet, default is 23"
    RemotePort = RemotePort_V
End Property
Public Property Let RemotePort(ByVal vNewValue As Integer)
    RemotePort_V = vNewValue
    PropertyChanged RemotePort
End Property

'----------------------------------------------------------------------
'Functions for the Control
'----------------------------------------------------------------------
Public Function Connect(Optional ByVal HostName As String = "")
    If HostName <> "" Then
        HostIP = HostName
        PropertyChanged HostIP
    ElseIf HostIP_V = "" Then
        RaiseEvent ErrorOccured("HostIP property is missing")
        Exit Function
    End If
    On Error Resume Next
    Winsock.Protocol = sckTCPProtocol
    Winsock.RemoteHost = HostIP_V
    Winsock.RemotePort = RemotePort_V
    Winsock.Connect
    TempData = ""
    If Err.Number = 40020 Then
        Winsock.Close
        Winsock.Protocol = sckTCPProtocol
        Winsock.RemoteHost = HostIP_V
        Winsock.RemotePort = RemotePort_V
        Winsock.Connect
        TempData = ""
    End If
End Function
Public Function Disconnect()
    Winsock.Close
    RaiseEvent Disconnected
End Function
Public Function SendData(ByVal Data As String, Optional SendNewLine As Boolean = False, Optional ByVal WaitFor As String = "")
    WaitForWhat = WaitFor
    TempData = ""
    On Error Resume Next
    If SendNewLine Then
        Winsock.SendData Data & Chr(13) & Chr(10)
    Else
        Winsock.SendData Data
    End If
    If Err.Number <> 0 Then
        RaiseEvent ErrorOccured(Err.Description)
    End If
End Function

Private Sub UserControl_Initialize()
    UserControl.Height = 510
    UserControl.Width = 510
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    RemotePort_V = 23
    PropertyChanged RemotePort_V
End Sub

Private Sub UserControl_Resize()
    UserControl.Height = 510
    UserControl.Width = 510
End Sub

Private Sub Winsock_DataArrival(ByVal bytesTotal As Long)
    Dim strData As String
    On Error Resume Next
    Winsock.GetData strData
    TempData = TempData & strData
    If Err.Number <> 0 Then
        Winsock.Close
        RaiseEvent ErrorOccured(Err.Description)
    End If
    If InStr(strData, Chr(Telnet_IAC)) > 0 Then
        Winsock.SendData FilterData(strData)
    End If
    If Trim(WaitForWhat) = "" Then
        RaiseEvent DataRecieved(CleanData(strData))
    ElseIf InStr(TempData, WaitForWhat) > 0 Then
        RaiseEvent DataRecieved(CleanData(TempData))
    Else
        Exit Sub
    End If
End Sub
Private Sub Winsock_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    RaiseEvent ErrorOccured(Description)
End Sub
Private Sub Winsock_Close()
    Winsock.Close
    RaiseEvent Disconnected
End Sub

Private Sub Winsock_Connect()
    RaiseEvent Connected
End Sub
Private Function FilterData(ByVal Data As String) As String
    If Len(Data) > 0 Then
        Dim I As Integer
        I = 1
        FilterData = ""
        Dim location As Integer
        Dim TelnetCommand As Integer
        Dim TelnetOption As Integer
        Do While I < Len(Data)
            location = InStr(I, Data, Chr(Telnet_IAC))
            If location = 0 Then Exit Do
            TelnetCommand = Asc(Mid(Data, location + 1, 1))
            TelnetOption = Asc(Mid(Data, location + 2, 1))
            If TelnetCommand = TelnetCommand_WILL Then
                FilterData = FilterData & Chr(Telnet_IAC)
                FilterData = FilterData & Chr(TelnetCommand_DONT)
                FilterData = FilterData & Chr(TelnetOption)
            ElseIf TelnetCommand = TelnetCommand_DO Then
                FilterData = FilterData & Chr(Telnet_IAC)
                FilterData = FilterData & Chr(TelnetCommand_WONT)
                FilterData = FilterData & Chr(TelnetOption)
            End If
            
            I = location + 1
        Loop
    End If
End Function
Private Function CleanData(ByVal Data As String) As String
    Dim ReturnedData As String
    ReturnedData = ""
    Dim OneChar As String
    If Len(Data) > 0 Then
        For I = 1 To Len(Data)
            OneChar = Mid(Data, I, 1)
            If Asc(OneChar) = 27 Then
                OneChar = " "
                I = I + 3
                If Mid(Data, I, 1) = "$" Then
                    I = I + 1
                    OneChar = Chr(0)
                ElseIf Mid(Data, I, 1) = "0" Then
                    I = I + 2
                    OneChar = Chr(0)
                ElseIf Mid(Data, I + 1, 1) = "0" Then
                    I = I + 3
                    OneChar = Chr(0)
                End If
            ElseIf Asc(OneChar) = 255 Then
                OneChar = Chr(0)
                I = I + 2
            End If
            If Asc(OneChar) >= 32 And Asc(OneChar) <= 255 Or Asc(OneChar) = 13 Or Asc(OneChar) = 10 Then
                ReturnedData = ReturnedData & OneChar
            End If
        Next
    End If
    CleanData = ReturnedData
End Function

