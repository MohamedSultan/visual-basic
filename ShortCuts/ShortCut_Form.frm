VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form ShortCut_Form 
   Caption         =   "ShortCut Commands"
   ClientHeight    =   3135
   ClientLeft      =   6390
   ClientTop       =   3825
   ClientWidth     =   6030
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   6030
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   11
      Left            =   3000
      TabIndex        =   44
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   11
         Left            =   840
         TabIndex        =   45
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   11
         Left            =   120
         TabIndex        =   47
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   46
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   10
      Left            =   4440
      TabIndex        =   40
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   10
         Left            =   840
         TabIndex        =   41
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   10
         Left            =   120
         TabIndex        =   43
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   42
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   9
      Left            =   120
      TabIndex        =   36
      Top             =   960
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   9
         Left            =   840
         TabIndex        =   37
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   9
         Left            =   120
         TabIndex        =   39
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   38
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   8
      Left            =   1560
      TabIndex        =   32
      Top             =   960
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   8
         Left            =   840
         TabIndex        =   33
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   8
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   34
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   7
      Left            =   3000
      TabIndex        =   28
      Top             =   960
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   7
         Left            =   840
         TabIndex        =   29
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   7
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   30
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   6
      Left            =   4440
      TabIndex        =   24
      Top             =   960
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   6
         Left            =   840
         TabIndex        =   25
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   6
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   26
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   5
      Left            =   120
      TabIndex        =   20
      Top             =   1920
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   5
         Left            =   840
         TabIndex        =   21
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   5
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   22
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   4
      Left            =   1560
      TabIndex        =   16
      Top             =   1920
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   4
         Left            =   840
         TabIndex        =   17
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   18
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   3
      Left            =   3000
      TabIndex        =   12
      Top             =   1920
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   3
         Left            =   840
         TabIndex        =   13
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   2
      Left            =   4440
      TabIndex        =   8
      Top             =   1920
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   2
         Left            =   840
         TabIndex        =   9
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   10
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   1
      Left            =   1560
      TabIndex        =   4
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   1
         Left            =   840
         TabIndex        =   5
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   1215
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton Settting_Command 
         Caption         =   "Set"
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   1
         Top             =   240
         Width           =   495
      End
      Begin MSForms.CommandButton ShortCut_Command 
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   735
         Size            =   "1296;661"
         FontHeight      =   165
         FontCharSet     =   178
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
      End
      Begin VB.Label Shortcut_Label 
         Alignment       =   2  'Center
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1215
      End
   End
   Begin MSComDlg.CommonDialog SetPath 
      Left            =   0
      Top             =   1800
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
End
Attribute VB_Name = "ShortCut_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ShortCut_Command_Path(), ShortCut_Icon_Path(), StartUpPath As String

Private Sub Form_Load()
    'X = ShortCut_Command(0).Picture
    For I = 0 To ShortCut_Command.UBound
        ReDim Preserve ShortCut_Command_Path(I)
        ReDim Preserve ShortCut_Icon_Path(I)
        ShortCut_Icon_Path(I) = "---"
    Next I
    Call Load_ShortCuts
End Sub
Private Sub Save_ShortCuts(Optional Index As Integer, Optional Icon_Path As String)
    O_Shortcuts = FreeFile
        Open Config.Conf_File_Loc + "\Shortcuts.txt" For Output As O_Shortcuts
            Print #O_Shortcuts, "This file Contains the shorcuts commands mapping"
            Print #O_Shortcuts, ""
            Print #O_Shortcuts, "Setting Path start up location: " + StartUpPath
            Print #O_Shortcuts, "Shortcut Command,Icon,Mapping"
            For I = 0 To ShortCut_Command.UBound
                Print #O_Shortcuts, "Command #" + Str(I) + "," + ShortCut_Icon_Path(I) + "," + ShortCut_Command_Path(I)
            Next I
        Close O_Shortcuts
End Sub
Private Function Get_Icon(Path As String) As String
    Dim Icon As String
    Get_Icon = "---"
    Icon = Dir(Path)
    While Icon <> ""
        If InStr(1, Icon, "ico") <> 0 Then
            Get_Icon = Icon
        End If
        Icon = Dir
    Wend
End Function
Private Sub Load_ShortCuts()
    On Error GoTo ShortCutCommand_Handler
    I_Shortcuts = FreeFile
    Open Config.Conf_File_Loc + "\Shortcuts.txt" For Input As I_Shortcuts
        Dim Shortcut_Confs, Temp As String
        Line Input #I_Shortcuts, Temp
        Line Input #I_Shortcuts, Temp
        Line Input #I_Shortcuts, StartUpPath
        StartUpPath = Mid(StartUpPath, 33, Len(StartUpPath) - 32)
        Line Input #I_Shortcuts, Temp
        For I = 0 To ShortCut_Command.UBound
            Dim Space1, Space2 As String
            Line Input #I_Shortcuts, Shortcut_Confs
            
            'Parsing
            Space1 = InStr(1, Shortcut_Confs, ",")
            Space2 = InStr(Space1 + 1, Shortcut_Confs, ",")
            ShortCut_Icon_Path(I) = Mid(Shortcut_Confs, Space1 + 1, Space2 - Space1 - 1)
            ShortCut_Command_Path(I) = Mid(Shortcut_Confs, Space2 + 1, Len(Shortcut_Confs) - Space2)
            
            'Mapping
            If ShortCut_Command_Path(I) <> "" Then
                Dim BackSlashPos, DotPos As String
                BackSlashPos = InStrRev(ShortCut_Command_Path(I), "\")
                DotPos = InStrRev(ShortCut_Command_Path(I), ".")
                ShortCut_Command(I).BackColor = &H80FF80
                Shortcut_Label(I) = Mid(ShortCut_Command_Path(I), BackSlashPos + 1, DotPos - BackSlashPos - 1)
                If ShortCut_Icon_Path(I) <> "---" Then ShortCut_Command(I).Picture = LoadPicture(ShortCut_Icon_Path(I))
            Else
                Shortcut_Label(I) = "---"
                ShortCut_Command(I).BackColor = &H8000000F
            End If
            
        Next I
    Close I_Shortcuts
     
ShortCutCommand_Handler:
        If Err.Number = 62 Then         'File Still Open
            Call Save_ShortCuts
        End If
        If Err.Number = 53 Then         'No File Exist
            O_Shortcuts = FreeFile
            Open Config.Conf_File_Loc + "\Shortcuts.txt" For Output As O_Shortcuts
        End If
        Close O_Shortcuts
End Sub

Private Sub Form_Terminate()
    Call Slide(Me, Me.Left, Me.Width, False)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Settting_Command_Click(Index As Integer)
    'On Error GoTo Sultan
    If ShortCut_Command_Path(Index) = "" Then
        SetPath.InitDir = StartUpPath
        Else
            Dim BackSlashPos As Integer
            BackSlashPos = InStrRev(ShortCut_Command_Path(Index), "\")
            SetPath.InitDir = Mid(ShortCut_Command_Path(Index), 1, BackSlashPos)
    End If
    SetPath.CancelError = False
    SetPath.Filter = "All Files (*.*)|*.*|Executable Files" & "(*.exe)|*.exe"
    SetPath.FilterIndex = 2
    SetPath.ShowOpen
    Dim Loc As Integer
    For I = 1 To Len(SetPath.FileName)
        If Mid(SetPath.FileName, I, 1) = "\" Then Loc = I
    Next I
    ShortCut_Icon_Path(Index) = Get_Icon(Mid(SetPath.FileName, 1, Loc))
    If ShortCut_Icon_Path(Index) <> "---" Then ShortCut_Icon_Path(Index) = Mid(SetPath.FileName, 1, InStrRev(SetPath.FileName, "\")) + "\" + ShortCut_Icon_Path(Index)
    'ShortCut_Command(Index).Picture = ShortCut_Icon_Path(Index)
    ShortCut_Command_Path(Index) = SetPath.FileName
    Call Save_ShortCuts
    Call Load_ShortCuts
Sultan:
End Sub

Private Sub ShortCut_Command_Click(Index As Integer)
    Dim Command
    If ShortCut_Command_Path(Index) <> "" Then _
        Command = Shell(ShortCut_Command_Path(Index))
    Call Form_Terminate
End Sub

Private Sub ShortCut_Command_LostFocus(Index As Integer)
    Call Save_ShortCuts
End Sub

