VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form Main 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ShortCuts"
   ClientHeight    =   5595
   ClientLeft      =   4680
   ClientTop       =   3225
   ClientWidth     =   11940
   Icon            =   "Main.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5595
   ScaleWidth      =   11940
   Begin VB.CommandButton VNPP_Commands 
      BackColor       =   &H00E0E0E0&
      Caption         =   "OMN"
      Height          =   375
      Index           =   3
      Left            =   360
      TabIndex        =   67
      Top             =   1440
      Width           =   1695
   End
   Begin VB.CommandButton VNPP_Commands_Conf 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   "..."
      Enabled         =   0   'False
      Height          =   375
      Index           =   3
      Left            =   2040
      TabIndex        =   66
      ToolTipText     =   "Set Wait Time and Startup Path"
      Top             =   1440
      Width           =   255
   End
   Begin VB.Frame Frame2 
      Caption         =   "VNPP"
      Height          =   1815
      Left            =   240
      TabIndex        =   59
      Top             =   120
      Width           =   2175
      Begin VB.CommandButton VNPP_Commands 
         BackColor       =   &H00E0E0E0&
         Caption         =   "SDP"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   65
         Top             =   960
         Width           =   1695
      End
      Begin VB.CommandButton VNPP_Commands_Conf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Index           =   2
         Left            =   1800
         TabIndex        =   64
         ToolTipText     =   "Set Wait Time and Startup Path"
         Top             =   960
         Width           =   255
      End
      Begin VB.CommandButton VNPP_Commands 
         BackColor       =   &H00E0E0E0&
         Caption         =   "USSD"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   63
         Top             =   600
         Width           =   1695
      End
      Begin VB.CommandButton VNPP_Commands_Conf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Index           =   1
         Left            =   1800
         TabIndex        =   62
         ToolTipText     =   "Set Wait Time and Startup Path"
         Top             =   600
         Width           =   255
      End
      Begin VB.CommandButton VNPP_Commands_Conf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Index           =   0
         Left            =   1800
         TabIndex        =   61
         ToolTipText     =   "Set Wait Time and Startup Path"
         Top             =   240
         Width           =   255
      End
      Begin VB.CommandButton VNPP_Commands 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Call Collect"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   60
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   255
      Left            =   2760
      TabIndex        =   58
      Top             =   4560
      Width           =   2055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   360
      TabIndex        =   57
      Top             =   4440
      Width           =   2055
   End
   Begin VB.ComboBox Applications 
      Height          =   315
      Left            =   2640
      TabIndex        =   54
      Text            =   "Other Applications"
      Top             =   2520
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame Frame_IN 
      Caption         =   "IN"
      Height          =   1575
      Left            =   9720
      TabIndex        =   43
      Top             =   3480
      Width           =   2175
      Begin VB.CommandButton IN 
         Caption         =   "AIRs"
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   46
         Top             =   240
         Width           =   1695
      End
      Begin VB.CommandButton IN 
         Caption         =   "SDPs"
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   45
         Top             =   600
         Width           =   1695
      End
      Begin VB.CommandButton IN 
         Caption         =   "Tariff"
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   44
         Top             =   960
         Width           =   1695
      End
   End
   Begin VB.Frame Frame_Apps 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Applications"
      Height          =   2535
      Left            =   2640
      TabIndex        =   41
      Top             =   120
      Width           =   2175
      Begin VB.CommandButton Extend_Button 
         Caption         =   "Other Applications"
         Height          =   375
         Left            =   240
         TabIndex        =   51
         Top             =   1880
         Width           =   1695
      End
      Begin VB.CommandButton Favorites 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Archive"
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   1505
         Width           =   1695
      End
      Begin VB.CommandButton Sulsat2_command 
         Caption         =   "SULSAT 2"
         Height          =   375
         Left            =   240
         TabIndex        =   50
         Top             =   1140
         Width           =   1695
      End
      Begin VB.CommandButton Omega_command 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Omega"
         Height          =   435
         Left            =   240
         MaskColor       =   &H00E0E0E0&
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   720
         Width           =   1455
      End
      Begin VB.CheckBox With_Remain 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         Height          =   135
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   49
         ToolTipText     =   "Check to press ENTER before starting Exceed"
         Top             =   360
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.CommandButton VPN_Connect 
         BackColor       =   &H00E0E0E0&
         Caption         =   "VPN"
         Height          =   375
         Left            =   240
         MaskColor       =   &H00E0E0E0&
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   360
         Width           =   1455
      End
      Begin VB.CommandButton Omega_Conf_Command 
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Height          =   435
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   55
         ToolTipText     =   "Set Wait Time"
         Top             =   720
         Width           =   255
      End
      Begin VB.CommandButton VPN_Conf 
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   360
         Width           =   255
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   735
      Left            =   9000
      TabIndex        =   35
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      Begin VB.CheckBox MPS_Check 
         BackColor       =   &H00E0E0E0&
         Height          =   135
         Left            =   600
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Check if you want to increase the wait time by 2 Sec"
         Top             =   240
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.TextBox TelnetActivated 
         Height          =   225
         Left            =   360
         TabIndex        =   37
         Text            =   "False"
         Top             =   330
         Visible         =   0   'False
         Width           =   150
      End
      Begin VB.CommandButton Remain 
         BackColor       =   &H00C000C0&
         Height          =   135
         Left            =   600
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Used To Keep The VPN Connected"
         Top             =   360
         Width           =   135
      End
   End
   Begin VB.Frame Frame_IVR 
      Caption         =   "IVR"
      Height          =   4215
      Left            =   6000
      TabIndex        =   20
      Top             =   720
      Width           =   2175
      Begin VB.CommandButton FTP_Command 
         Caption         =   "FTP"
         Enabled         =   0   'False
         Height          =   375
         Left            =   240
         TabIndex        =   40
         Top             =   3600
         Width           =   1455
      End
      Begin VB.CommandButton FTP_Conf 
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   1680
         TabIndex        =   39
         Top             =   3600
         Width           =   255
      End
      Begin VB.CommandButton Advanced_TelNet 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Advanced Telnet"
         Enabled         =   0   'False
         Height          =   375
         Left            =   240
         MaskColor       =   &H0000FF00&
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   3240
         Width           =   1455
      End
      Begin VB.CommandButton Quick_TelNet 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Quick Telnet"
         Enabled         =   0   'False
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   2880
         Width           =   1455
      End
      Begin VB.CommandButton MMFs 
         BackColor       =   &H00E0E0E0&
         Caption         =   "MMFs"
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   2520
         Width           =   1695
      End
      Begin VB.CommandButton Tappman 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Tappman"
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   2160
         Width           =   1695
      End
      Begin VB.CommandButton BLK_UnBLK 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Block and Unblock"
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   1800
         Width           =   1695
      End
      Begin VB.CommandButton PeriStudio 
         BackColor       =   &H00E0E0E0&
         Caption         =   "PeriStudio"
         Enabled         =   0   'False
         Height          =   375
         Left            =   240
         MaskColor       =   &H0080C0FF&
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   1440
         Width           =   1455
      End
      Begin VB.CheckBox Peri_Enter 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         Height          =   135
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Check to press ENTER before starting Exceed"
         Top             =   1080
         Width           =   135
      End
      Begin VB.CommandButton PeriPro 
         BackColor       =   &H00E0E0E0&
         Caption         =   "PeriPro"
         Enabled         =   0   'False
         Height          =   375
         Left            =   240
         MaskColor       =   &H0080C0FF&
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   1080
         Width           =   1455
      End
      Begin VB.OptionButton Peri_Reda 
         Caption         =   "Reda"
         Height          =   195
         Left            =   480
         TabIndex        =   26
         Top             =   480
         Width           =   735
      End
      Begin VB.OptionButton Peri_TestMPS 
         Caption         =   "MPS"
         Height          =   195
         Left            =   480
         TabIndex        =   25
         Top             =   720
         Width           =   735
      End
      Begin VB.CommandButton Peri_Pro_Conf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Options: Set UserName, Password, Start Directory and Exceed Wait Time"
         Top             =   1080
         Width           =   255
      End
      Begin VB.CommandButton Peri_Studio_Conf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Options: Set UserName, Password, Start Directory and Exceed Wait Time"
         Top             =   1440
         Width           =   255
      End
      Begin VB.CommandButton Quick_TelNet_Conf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   32
         ToolTipText     =   "Set Wait Time and Startup Path"
         Top             =   2880
         Width           =   255
      End
      Begin VB.CommandButton Advanced_Telnet_Time_Command 
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   1680
         TabIndex        =   34
         Top             =   3240
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Machine"
         Height          =   255
         Left            =   360
         TabIndex        =   42
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Other 
      Caption         =   "Other"
      Height          =   1455
      Left            =   9360
      TabIndex        =   5
      Top             =   1920
      Width           =   2535
      Begin VB.CommandButton Test_Cmd 
         Caption         =   "Test"
         Height          =   375
         Left            =   1560
         TabIndex        =   19
         Top             =   960
         Width           =   615
      End
      Begin VB.CommandButton ShortCuts 
         Caption         =   "ShortCuts"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton VASP_AIR 
         BackColor       =   &H00FF8080&
         Caption         =   "Useful Commands"
         Height          =   375
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   960
         Width           =   1455
      End
      Begin VB.CommandButton Telnet_AIR1 
         BackColor       =   &H0000FF00&
         Caption         =   "Telnet AIR1"
         Height          =   375
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame Frame3 
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   8160
      TabIndex        =   13
      Top             =   3480
      Width           =   1455
      Begin VB.TextBox IP_Add 
         Height          =   285
         Left            =   120
         TabIndex        =   15
         Text            =   "My IP Address"
         Top             =   480
         Width           =   1215
      End
      Begin VB.CommandButton IP 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Get My IP Add."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.TextBox ShortCut_Text 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      TabIndex        =   10
      Text            =   "ShortCut"
      Top             =   5160
      Width           =   975
   End
   Begin VB.CommandButton Minimizing 
      Caption         =   "_"
      Height          =   255
      Left            =   11640
      TabIndex        =   9
      Top             =   0
      Width           =   255
   End
   Begin VB.Timer All_Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   8520
      Top             =   360
   End
   Begin VB.Frame Minsat_Frame 
      Caption         =   "Minsat"
      Height          =   975
      Left            =   9360
      TabIndex        =   1
      Top             =   840
      Width           =   2535
      Begin VB.CommandButton MINSAT_Conf 
         BackColor       =   &H80000009&
         Height          =   495
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   240
         Width           =   75
      End
      Begin VB.CommandButton Minsat 
         BackColor       =   &H00FFFFFF&
         Caption         =   "MINSAT"
         Height          =   495
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton Test 
         Caption         =   "Test"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   480
         Width           =   615
      End
      Begin VB.OptionButton Live 
         Caption         =   "Live"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.CommandButton Config_command 
      Caption         =   "Configuration"
      Height          =   495
      Left            =   8280
      TabIndex        =   0
      Top             =   4440
      Width           =   1335
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   8520
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Frame Frame4 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   6000
      TabIndex        =   16
      Top             =   0
      Width           =   2655
      Begin VB.ComboBox MSISDNS 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   315
         ItemData        =   "Main.frx":0442
         Left            =   840
         List            =   "Main.frx":0444
         TabIndex        =   17
         Text            =   "MSISDNS"
         ToolTipText     =   "Type Here The Requested MSISDN"
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label MSISDN_Label 
         Caption         =   "MSISDN"
         Height          =   375
         Left            =   120
         TabIndex        =   18
         Top             =   285
         Width           =   855
      End
   End
   Begin VB.ComboBox Tool_Selection 
      Height          =   315
      Left            =   480
      TabIndex        =   48
      Top             =   5040
      Visible         =   0   'False
      Width           =   3135
   End
   Begin MSForms.ToggleButton Extend_Main 
      Height          =   5055
      Left            =   5640
      TabIndex        =   12
      Top             =   360
      Width           =   255
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "450;8916"
      Value           =   "0"
      Caption         =   ">>>>>>>>>>>>>>>>>>>>>>>>>"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin VB.Menu SulSAT2_Menu 
      Caption         =   "SulSAT2"
      NegotiatePosition=   3  'Right
   End
   Begin VB.Menu UCIP_Menu 
      Caption         =   "&UCIP"
   End
   Begin VB.Menu IVR_Menu 
      Caption         =   "&IVR"
      Begin VB.Menu PeriPro_Menu 
         Caption         =   "&Peripro"
         Begin VB.Menu Peripro_testmps_Menu 
            Caption         =   "&TestMPS"
         End
         Begin VB.Menu Peripro_Reda__Menu 
            Caption         =   "&Reda"
         End
      End
      Begin VB.Menu PeriStudio__Menu 
         Caption         =   "PeriStudio"
         Begin VB.Menu PeriStudio_TestMPS__Menu 
            Caption         =   "TestMPS"
         End
         Begin VB.Menu PeriStudio_Reda__Menu 
            Caption         =   "Reda"
         End
      End
      Begin VB.Menu Connections_Menu 
         Caption         =   "Connections"
         Begin VB.Menu Telnet_Menu 
            Caption         =   "Telnet"
            Begin VB.Menu Connection_telnet_testmps_Menu 
               Caption         =   "TestMPS"
            End
            Begin VB.Menu Connection_telnet_reda_Menu 
               Caption         =   "Reda"
            End
         End
         Begin VB.Menu Connection_FTP 
            Caption         =   "FTP"
            Begin VB.Menu FTP_TestMPS_Menu 
               Caption         =   "TestMPS"
            End
            Begin VB.Menu FTP_Reda_Menu 
               Caption         =   "Reda"
            End
         End
      End
      Begin VB.Menu Tappman__Menu 
         Caption         =   "tappman"
      End
      Begin VB.Menu MMF__Menu 
         Caption         =   "MMF"
      End
      Begin VB.Menu Block_Unblock_Menu 
         Caption         =   "Block/UnBlock Lines"
      End
   End
   Begin VB.Menu IN_Menu 
      Caption         =   "IN"
      Begin VB.Menu AIRs_Menu 
         Caption         =   "AIRs"
      End
      Begin VB.Menu SDPs_Menu 
         Caption         =   "SDPs"
      End
      Begin VB.Menu Tariff_Menu 
         Caption         =   "Tariff"
      End
   End
   Begin VB.Menu Apps_Menu 
      Caption         =   "Apps"
      Begin VB.Menu Omega_Menu 
         Caption         =   "Omega"
      End
      Begin VB.Menu VPN_Menu 
         Caption         =   "VPN"
         Begin VB.Menu GSM_Local_Menu 
            Caption         =   "GSM Local"
         End
         Begin VB.Menu Remote_GSM_Menu 
            Caption         =   "Remote GSM"
         End
         Begin VB.Menu My_Work_Menu 
            Caption         =   "My Work"
         End
      End
   End
   Begin VB.Menu minsat_Menu 
      Caption         =   "MINSAT"
      Begin VB.Menu minsat_Live_Menu 
         Caption         =   "Live"
      End
      Begin VB.Menu minsat_Test_Menu 
         Caption         =   "Test"
      End
   End
   Begin VB.Menu Others_menu 
      Caption         =   "Others"
      Begin VB.Menu ShortCuts_Menu 
         Caption         =   "ShortCuts"
      End
      Begin VB.Menu Archive_Menu 
         Caption         =   "Archive"
      End
   End
   Begin VB.Menu Configs 
      Caption         =   "Configuration"
   End
   Begin VB.Menu About_Menu 
      Caption         =   "About"
      NegotiatePosition=   3  'Right
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
    Private Type NOTIFYICONDATA
        cbSize As Long
        hwnd As Long
        uID As Long
        uFlags As Long
        uCallbackMessage As Long
        hIcon As Long
        szTip As String * 64
    End Type
       
    Const NIM_ADD = 0
    Const NIM_MODIFY = 1
    Const NIM_DELETE = 2
    Const NIF_MESSAGE = 1
    Const NIF_ICON = 2
    Const NIF_TIP = 4
    
    Const WM_MOUSEMOVE = &H200
    Const WM_LBUTTONDOWN = &H201
    Const WM_LBUTTONUP = &H202
    Const WM_LBUTTONDBLCLK = &H203
    Const WM_RBUTTONDOWN = &H204
    Const WM_RBUTTONUP = &H205
    Const WM_RBUTTONDBLCLK = &H206
    Const WM_MBUTTONDOWN = &H207
    Const WM_MBUTTONUP = &H208
    Const WM_MBUTTONDBLCLK = &H209
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Private Declare Function Shell_NotifyIconA Lib "SHELL32" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Integer

Dim Times_Old, TelNetFormWidth As Integer
Dim Minimized As Boolean

Dim Times_Type_Error As Boolean
Dim Current_Loaded_Oragnized_Forms As Integer


Private Sub About_menu_Click()
    Call Slide(About_Form, Main.Left + Main.Width, About_Form.Width, True)
End Sub



Private Sub AIRs_Menu_Click()
    Call IN_Click(0)
End Sub

Private Sub Applications_Click()
    With Main2
        Select Case Applications.Text
            Case "VASP Tests"
                Call .VASP_Tests_Click
            Case "SULSAT"
                Call .UCIP_Command_Click
            Case "Stored Procedure Tests"
                Call .Stored_Procedures_tests_Click
            Case "Get Length"
                Call .Get_Len_Click
            Case "Verify SDPs"
                Call .Verify_SDPs_Click
            Case "Prefix Operations"
                Call .Prefix_Command_Click
            Case "Shortcuts"
                Call .ShortCuts_Click
            Case "Automate Exceed"
                Call .Automate_Exceeds_Cmd_Click
            Case "Unix Times"
                Unix_Times_Form.Visible = True
        End Select
    End With
    Applications.Visible = False
End Sub

Private Sub Applications_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Applications_Click
End Sub

Private Sub Applications_LostFocus()
    Applications.Visible = False
End Sub

Private Sub Archive_Menu_Click()
    Call Favorites_Click
End Sub





Private Sub Block_Unblock_Menu_Click()
    Call BLK_UnBLK_Click
End Sub

Private Sub Command2_Click()
    Call ping("172.28.6.15", "peri", "peri")
End Sub



Private Sub Command1_Click()
    'Call Create_Telnet_Form("172.28.6.15", "peri", "peri", "/opt/home/peri", 21)
    Call FTP("172.28.7.29", "peri", "peri", "/opt/home/peri", , "hash", "prompt", "bin")
End Sub

Private Sub Command3_Click()

End Sub

Private Sub Command4_Click()

End Sub

Private Sub Configs_Click()
    Call Config_command_Click
End Sub

Private Sub Connection_telnet_reda_Menu_Click()
    Peri_Reda.Value = True
    Call Quick_TelNet_Click
End Sub

Private Sub Connection_telnet_testmps_Menu_Click()
    Peri_TestMPS.Value = True
    Call Quick_TelNet_Click
End Sub

Private Sub Extend_Main_Click()
    If Extend_Main.Value = True Then
        Main.Width = Main.Width * 2
    Else
        Main.Width = Main.Width / 2
    End If
End Sub



Private Sub FTP_Conf_Click()
    If Peri_TestMPS.Value = True Then
        Call Load_List(Set_Wait_Time.Path, "Paths")
        Set_Time_Object = "FTP_MPS"
        Close_All
        Call Extend("Down")
        Set_Wait_Time.Caption = "FTP MPS" + Set_Wait_Time.Caption
        Set_Wait_Time.Visible = True
        Call Move_Form(Main, "Left", 3000)
        Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3840, True)
    Else
        Call Load_List(Set_Wait_Time.Path, "Paths")
        Set_Time_Object = "FTP_Reda"
        Close_All
        Call Extend("Down")
        Set_Wait_Time.Caption = "FTP REDA" + Set_Wait_Time.Caption
        Set_Wait_Time.Visible = True
        Call Move_Form(Main, "Left", 3000)
        Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3840, True)
    End If
End Sub

Private Sub FTP_Reda_Menu_Click()
    Peri_Reda.Value = True
    Call FTP_Command_Click
End Sub

Private Sub FTP_TestMPS_Menu_Click()
    Peri_TestMPS.Value = True
    Call FTP_Command_Click
End Sub


Private Sub GSM_Local_Menu_Click()
    GSM.Value = True
    Call VPN_Connect_Click
End Sub

Private Sub IN_Click(Index As Integer)
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
            If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
                Call_IN_URL (Index)
            Else
                Dim Connect_ As Integer
                Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                If Connect_ = 1 Then
                    Call VPN_Connect_Click
                    Wait 1000
                    Call IN_Click(Index)
                End If
            End If
        End If
End Sub

Private Sub BLK_UnBLK_Click()
    Close_All
    
    With Commands
        .App_Label.Visible = False
        .MPS_Label.Caption = "E1 #"
        .MPS_Text = "1"
        .Caption = "Block / Unblock"
        .Status.Visible = True
        .App_Text.Visible = False
        .ON_Assign.Caption = "Unblock"
        .OFF_UnAssign.Caption = "Block"
        .On.Visible = False
        .Off.Visible = False
        .Assign.Visible = False
        .Unassign.Visible = False
        .MPS.Visible = False
        .FCD.Visible = False
    End With
    
    Call Move_Form(Main, "Left", 900)
    Call Slide(Commands, Main.Left + Main.Width, 6150, True)
    Commands.Show
End Sub
Public Function Create_Telnet_Form(ByRef IP_Address As String, _
                                ByRef Login As String, _
                                ByRef Password As String, _
                                ByRef Start_Up_Path As String, _
                                Optional Port As Integer, _
                                Optional ByRef SendCommand1 As String, _
                                Optional ByRef SendCommand2 As String, _
                                Optional ByRef SendCommand3 As String, _
                                Optional ByRef SendCommand4 As String, _
                                Optional ping As Boolean) As Integer
    Dim SendCommand(3) As String
        SendCommand(0) = SendCommand1
        SendCommand(1) = SendCommand2
        SendCommand(2) = SendCommand3
        SendCommand(3) = SendCommand4
    
    If Port = 0 Then Port = 23
    ReDim Preserve Active_Telnet_Form(Number_Of_Active_Telnet_Connections)
    Set Active_Telnet_Form(Number_Of_Active_Telnet_Connections) = New Telnet_Instance_Form
    
    Dim Form_Caption As String
    Form_Caption = "Telnet Form ("
    Select Case IP_Address
        Case "172.28.6.15"
            Form_Caption = "Test MPS " + Form_Caption
        Case "10.231.14.21"
           Form_Caption = "Reda's " + Form_Caption
    End Select
    
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Caption = Form_Caption + Trim(Str(Number_Of_Active_Telnet_Connections)) + ")"
    Dim Sultan As Form
    Set Sultan = Active_Telnet_Form(Number_Of_Active_Telnet_Connections)
    Call Slide(Sultan, Main.Left + Main.Width, Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Width, True)
    
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Login = Login
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Password = Password
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Path = Start_Up_Path
    
    For j = 0 To UBound(SendCommand)
        Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Additional_Commands(j) = SendCommand(j)
    Next j
    
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Telnet_Connect.RemotePort = Port
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Telnet_Connect.Connect IP_Address
    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Telnet_Timer.Enabled = True
    
    Dim Step_Color As Double
    Step_Color = 99000
    With Active_Telnet_Form(Number_Of_Active_Telnet_Connections)
        If Number_Of_Active_Telnet_Connections = 0 Then
            .Display.BackColor = select_Color(.Display.BackColor)
            .Command_To_Be_Sent.BackColor = select_Color(.Command_To_Be_Sent.BackColor)
        Else
            .Display.BackColor = select_Color(Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Display.BackColor)
            .Command_To_Be_Sent.BackColor = select_Color(Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Command_To_Be_Sent.BackColor)
        End If
    End With
    Create_Telnet_Form = Number_Of_Active_Telnet_Connections
    Number_Of_Active_Telnet_Connections = Number_Of_Active_Telnet_Connections + 1
End Function
Private Function select_Color(current_Color As Double) As Double
    Dim Colors(10) As Double
        Colors(5) = 12632319
        Colors(2) = 12640511
        Colors(3) = 16761087
        Colors(1) = 16761024
        Colors(4) = 12648384
        Colors(6) = 8454143
        Colors(7) = 715000
        Colors(8) = 781000
        Colors(9) = 385000
        Colors(10) = 55000
        Colors(0) = -2147483643
        
        Dim Found As Boolean
        For I = 0 To 9
            If Not Found Then
                If current_Color = Colors(I) Then
                    select_Color = Colors(I + 1)
                    Found = True
                End If
            End If
        Next I
        If Not Found Then select_Color = current_Color
End Function
Private Sub Call_IN_URL(Index As Integer)
    Select Case Index
        Case 0  ' AIR
            ShellExecute hwnd, "open", "http://172.30.201.11:10000/java/gui.jnlp", vbNullString, vbNullString, SW_NORMAL
        Case 1  'SDP
            ShellExecute hwnd, "open", "http://172.30.5.29:10000/java/sma.jnlp", vbNullString, vbNullString, SW_NORMAL
        Case 2  'Tariff
            ShellExecute hwnd, "open", "http://172.30.5.29:10000/java/tma.jnlp", vbNullString, vbNullString, SW_NORMAL
    End Select
End Sub





Private Sub minsat_Live_Menu_Click()
    Live.Value = True
    Call Minsat_Click
End Sub

Private Sub minsat_Test_Menu_Click()
    Test.Value = True
    Call Minsat_Click
End Sub

Private Sub MMF__Menu_Click()
    Call MMFs_Click
End Sub

Private Sub MPS_Conf_Click()
    Call Load_List(Set_Wait_Time.Path, "Paths")
    Close_All
        Set_Time_Object = "Tel_MPS"
        Set_Wait_Time.Caption = "Telnet MPS" + Set_Wait_Time.Caption
    Set_Wait_Time.Visible = True
    Call Extend("Down")
    Call Move_Form(Main, "Left", 3000)
    Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3840, True)
End Sub
Private Sub My_Work_Menu_Click()
    My_Work.Value = True
    Call VPN_Connect_Click
End Sub

Private Sub Omega_Conf_Command_Click()
    Close_All
'    Set_Wait_Time.Caption = "OMEGA" + Set_Wait_Time.Caption
'    Set_Wait_Time.Visible = True
'    Call Move_Form(Main, "Left", 3000)
'    Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3075, True)
'    Set_Time_Object = "OMEGA"
    'Call Set_Time_Obj
    Omega_Conf_Form.Top = Omega_Conf_Command.Top + Me.Top
    Omega_Conf_Form.Timer1.Enabled = True
    Call Slide(Omega_Conf_Form, Me.Left + Frame_Apps.Left + Omega_command.Left + Omega_command.Width + Omega_Conf_Command.Width, Omega_Conf_Form.Width, True)
End Sub

Private Sub Omega_Menu_Click()
    Call Omega_command_Click
End Sub

Private Sub Peri_Reda_Click()
   Call Machine_Enabeled
End Sub

Private Sub Peri_TestMPS_Click()
    Call Machine_Enabeled
End Sub
Private Sub Machine_Enabeled()
    
    PeriPro.Enabled = True
    Peri_Enter.Enabled = True
    Peri_Pro_Conf.Enabled = True
    
    PeriStudio.Enabled = True
    Peri_Studio_Conf.Enabled = True
    Quick_TelNet.Enabled = True
    Quick_TelNet_Conf.Enabled = True
      
    Advanced_TelNet.Enabled = True
    Advanced_Telnet_Time_Command.Enabled = True
    
    FTP_Command.Enabled = True
    FTP_Conf.Enabled = True
End Sub

Private Sub Peripro_Reda__Menu_Click()
    Peri_Reda.Value = True
    Call PeriPro_Click
End Sub

Private Sub Peripro_testmps_Menu_Click()
    Peri_TestMPS.Value = True
    Call PeriPro_Click
End Sub

Private Sub PeriStudio_Reda__Menu_Click()
    Peri_Reda.Value = True
    Call PeriStudio_Click
End Sub

Private Sub PeriStudio_TestMPS__Menu_Click()
    Peri_TestMPS.Value = True
    Call PeriStudio_Click
End Sub

Private Sub Quick_TelNet_Click()
    Dim Wait_Time As String
    
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
            Dim Path As String
            Path = Reda_Telnet_Path
            If Peri_Reda.Value = True Then
                Call Create_Telnet_Form("10.231.14.21", Config.Reda_User, Config.Reda_PIN, Path)
            Else        'MPS is true
                Path = MPS_Telnet_Path
                If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
                    Call Create_Telnet_Form("172.28.6.15", Config.TESTMPS_User, Config.TESTMPS_PIN, Path)
                    If Set_Wait_Time.Times > 1 Then
                        For I = 2 To Set_Wait_Time.Times
                            Path = MPS_Telnet_Path
                            Call Create_Telnet_Form("172.28.6.15", Config.TESTMPS_User, Config.TESTMPS_PIN, Path)
                        Next I
                    End If
                    Set_Wait_Time.Times = 1
                Else
                    Dim Connect_ As Integer
                    Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                    If Connect_ = 1 Then
                        Call VPN_Connect_Click
                        Wait 200
                        
                        Dim MPS_Check_Old_Value As Integer
                        MPS_Check_Old_Value = MPS_Check.Value
                        
                        MPS_Check.Value = 1
                        Call Tel_MPS_Click
                        MPS_Check.Value = MPS_Check_Old_Value
                    End If
                End If
            End If
    End If
End Sub

Private Sub Quick_TelNet_Conf_Click()
    If Peri_TestMPS.Value = True Then
        Set_Time_Object = "Tel_MPS"
        Set_Wait_Time.Caption = "Telnet MPS" + Set_Wait_Time.Caption
    Else
        Set_Time_Object = "TEL_REDA"
        Set_Wait_Time.Caption = "Telnet REDA" + Set_Wait_Time.Caption
    End If
    Close_All
    Call Extend("Down")
    Call Load_List(Set_Wait_Time.Path, "Paths")
    Call Move_Form(Main, "Left", 3000)
    Call Slide(Set_Wait_Time, Main.Left + Main.Width, Set_Wait_Time.Width, True)
End Sub

Private Sub Remote_GSM_Menu_Click()
    GSM_Remote.Value = True
    Call VPN_Connect_Click
End Sub

Private Sub S_Click()
    Call Minimizing_Click
End Sub

Private Sub SDPs_Menu_Click()
    Call IN_Click(1)
End Sub

Private Sub ShortCut_Text_Click()
    ShortCut_Text = ""
End Sub

Private Sub ShortCut_Text_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Select Case LCase(ShortCut_Text)
            Case "ucip"
                Call SulSAT.ShoW_UCIP_Click
            Case "shortcut"
                Call Main2.ShortCuts_Click
            Case "sulsat"
                Call Main2.UCIP_Command_Click
            Case "sulsat2"
                Call Sulsat2_command_Click
        End Select
    End If
End Sub

Public Sub Config_command_Click()
    Load_Conf
    Close_All
    Call Move_Form(Main, "Left", 1400)
    Call Slide(Config, Main.Left + Main.Width, 9170, True)
    Config.Extend.Left = 8760
End Sub

Public Sub Extend_Button_Click()
        Applications.Text = "Other Applications"
        Applications.Visible = True
'        Close_All
'        Call Move_Form(Main, "Left", 1500)
'        Call Slide(Main2, Main.Left + Main.Width, 4770, True)
End Sub



Private Sub Favorites_Click()
    Current_PageNumber = 0
    Call Organized.Loading_Organized_Data
    Call Organized.Load_Organized(Current_PageNumber)
End Sub

Private Sub Form_Load()
'    Call Slide(Hat, Me.Left - 500, Hat.Width, True)
'    Hat.Top = Me.Top - Hat.Height
'    Hat.Width = Me.Width + 1000
    Main.Width = 6050
    Load_Conf
    Load_Last_Paths
    Call Load_List(MSISDNS, "MSISDNS")
    Call Load_List(Peri_Form.Paths_List, "Paths")
    
    If VPN_Conf_Form.GSM.Value = True Then
        Main.VPN_Connect.Caption = "VPN (Local GSM)"
        ElseIf VPN_Conf_Form.GSM_Remote.Value = True Then
            Main.VPN_Connect.Caption = "VPN(Remote GSM)"
        ElseIf VPN_Conf_Form.My_Work.Value = True Then
             Main.VPN_Connect.Caption = "VPN (My Work)"
    End If
    
    Applications.AddItem "Shortcuts"
    Applications.AddItem "Unix Times"
    Applications.AddItem "VASP Tests"
    Applications.AddItem "SULSAT"
    Applications.AddItem "Stored Procedure Tests"
    Applications.AddItem "Get Length"
    Applications.AddItem "Verify SDPs"
    Applications.AddItem "Prefix Operations"
    Applications.AddItem "Automate Exceed"
    
    
    TelNetFormWidth = Telnet_Form.Width
    
    Test.Value = True
    
    Tool_Selection.AddItem "IVR"
    Tool_Selection.AddItem "Applications"
    Tool_Selection.AddItem "IN"
    Tool_Selection.AddItem "Minsat"

    Peri_Form.Paths_List.Text = PeriPro_Last_Path
    
    
End Sub


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Form_Resize()
    Select Case Main.WindowState
    Case 2                      'Maximized
        Main.WindowState = 0    'Make it normal
        Minimized = False
'    Case 1                      'Minimized
'        If Minimized = False Then
'            Call Cope_All("Minimized")
'            Minimized = True
'
'            Dim i As Integer
'            Dim s As String
'            Dim nid As NOTIFYICONDATA
'
'            s = "Shortcut program is in System Tray"
'            nid = setNOTIFYICONDATA(Main.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Main.Icon, s)
'            i = Shell_NotifyIconA(NIM_ADD, nid)
'
'            WindowState = vbMinimized
'            Visible = False
'
'        End If
    Case 0                      'Normal
        Call Cope_All("Normal")
        Minimized = False
    End Select
    
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub FTP_Command_Click()
    'FTP
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
            If Peri_TestMPS.Value = True Then
                If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
     
                    Call FTP("172.28.6.15", Config.TESTMPS_User, Config.TESTMPS_PIN, _
                                MPS_FTP_Path, , "hash", "prompt", "bin")
                Else
                        Dim Connect_ As Integer
                        Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                        If Connect_ = 1 Then
                            Call VPN_Connect_Click
                            Wait 1000
                            Call Tel_MPS_Click
                        End If
                End If
            Else
                 Call FTP("10.231.14.21", Config.Reda_User, Config.Reda_PIN, _
                                Reda_FTP_Path, , "hash", "prompt", "bin")
                                
                'Dim Reda
                'Reda = Shell(Config.FTP_Reda_Path + "\FTP Reda.bat", vbNormalFocus)
                'Wait Wait_Reda_FTP
                'SendKeys Config.reda_user
                'SendKeys "{ENTER}"
                'Wait 500
                'SendKeys Config.Reda_PIN
                'SendKeys "{ENTER}"
                'Wait 500
                'SendKeys "hash"
                'SendKeys "{ENTER}"
                'Wait 500
                'SendKeys "bin"
                'SendKeys "{ENTER}"
                'Wait 700
                'SendKeys "cd " + Reda_FTP_Path
                'SendKeys "{ENTER}"
            End If
    End If
End Sub

Private Sub FTP_REDA_Click()
    'FTP
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        Dim Reda
        Reda = Shell(Config.FTP_Reda_Path + "\FTP Reda.bat", vbNormalFocus)
        Wait Wait_Reda_FTP
        SendKeys Config.Reda_User
        SendKeys "{ENTER}"
        Wait 500
        SendKeys Config.Reda_PIN
        SendKeys "{ENTER}"
        Wait 500
        SendKeys "hash"
        SendKeys "{ENTER}"
        Wait 500
        SendKeys "bin"
        SendKeys "{ENTER}"
        Wait 700
        SendKeys "cd " + Reda_FTP_Path
        SendKeys "{ENTER}"
    End If
End Sub



'Private Sub HLR_Command_Click()
'    Dim HLR
'    If Check_Connectivity = 0 Then
'        MsgBox "Sorry you can't connect since there is no local connection"
'        Else
'            If (IsProcessRunning("ipsecdialer.exe")) Then
'            Wait 300
'            HLR = Shell(Config.HLR_Path + "\HLR Guide2.exe", vbNormalFocus)
'
'            If Main.HLR_Enter.Value = 1 Then
'                While Not IsProcessRunning("HLR Guide2.exe")
'                    Wait 500
'                Wend
'                SendKeys "{ENTER}"     'to Elluminate the press OK prompt
'            End If
'
'            Application_Wait = "HLR"
'
'            If Wait_HLR / 1000 > 0 Then
'                Progress_Form.ProgressBar1.Max = Wait_HLR / 1000
'                Progress_Form.Time_Elapsed = 0
'                'Progress_Form.Visible = True
''                Call Slide(Progress_Form, Progress_Form.Left, 7815, True)
'                Progress_Form.Timer1.Enabled = True
'            End If
'            Applications_Handler
'
''
'            Else
'            Dim Connect_ As Integer
'            Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
'            If Connect_ = 1 Then
'                Call VPN_Connect_Click
'                Wait 1000
'                Call HLR_Command_Click
'            End If
'        End If
'    End If
'End Sub

'Private Sub HLR_Time_Command_Click()
'    Set_Time_Object = "HLR"
'    Close_All
'    Set_Wait_Time.Caption = "HLR" + Set_Wait_Time.Caption
'    Set_Wait_Time.Visible = True
'    Call Move_Form(Main, "Left", 3000)
'    Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3075, True)
'End Sub

Private Sub IP_Click()
    If Get_IP_Address = "" Then
        IP_Add = "My IP Address"
    Else
'        IP.Caption = Get_IP_Address()
 '       If IP.Caption = "" Then IP.Caption = "Get My IP"
        IP_Add = Get_IP_Address
    End If
End Sub

Private Sub Minimizing_Click()
            Call Cope_All("Minimized")
            Minimized = True

            Dim I As Integer
            Dim s As String
            Dim nid As NOTIFYICONDATA

            s = "Shortcut program is in System Tray"
            nid = setNOTIFYICONDATA(Main.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Main.Icon, s)
            I = Shell_NotifyIconA(NIM_ADD, nid)

            WindowState = vbMinimized
            Visible = False
End Sub

Private Sub Minsat_Click()
    
    Dim I_application_properties, O_application_properties As Long
    Dim application_properties() As String
    Dim Step As Integer
    
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
            If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
    
                I_application_properties = FreeFile
            
                'Load application_properties
                Step = 0
                Open Config.MINSAT_Path + "\application.properties" For Input As I_application_properties
                    Do Until EOF(I_application_properties)
                        ReDim Preserve application_properties(Step)
                        Line Input #I_application_properties, application_properties(Step)
                        Step = Step + 1
                    Loop
                Close I_application_properties
                
                If Live.Value = True Then   'Live
                    application_properties(2) = "#SERVER=172.30.5.30"
                    application_properties(3) = "SERVER=Minsat"
                    MINSAT_Password = Config.MINSAT_L_PIN
                    MINSAT_Check = Set_Wait_Time_Detailed.MSISDN(0)
                    Minsat_Wait_Time = Wait_Minsat_Live
                    MINSAT_UserName = Set_Wait_Time_Detailed.Minsat_User_Name(0)
                    ElseIf Test.Value = True Then   'Test
                        application_properties(2) = "SERVER=172.30.5.30"
                        application_properties(3) = "#SERVER=Minsat"
                        MINSAT_Password = Config.MINSAT_PIN
                        MINSAT_Check = Set_Wait_Time_Detailed.MSISDN(1)
                        Minsat_Wait_Time = Wait_MINSAT_Test
                        MINSAT_UserName = Set_Wait_Time_Detailed.Minsat_User_Name(1)
                End If
                
                'save application_properties
                O_application_properties = FreeFile
                Open Config.MINSAT_Path + "\application.properties" For Output As O_application_properties
                    For I = 0 To UBound(application_properties)
                        Print #O_application_properties, application_properties(I)
                    Next I
                Close O_application_properties
                
                MINSAT_Call
                Else
                    Dim Connect_ As Integer
                    Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                    If Connect_ = 1 Then
                        Call VPN_Connect_Click
                        Wait 1000
                        Call Minsat_Click
                    End If
            End If
        End If
    
End Sub
Private Sub MINSAT_Call()
    Dim Minsat
    
    Minsat = Shell(Config.MINSAT_Path + "\minsat.exe", vbNormalFocus)
    Progressing (Minsat_Wait_Time / 1000)
    Application_Wait = "MINSAT Log In"
    Applications_Handler
End Sub

Private Sub MINSAT_Conf_Click()
        Set_Time_Object = "MINSAT"
        Close_All
        With Set_Wait_Time_Detailed
            .Label3(1).Visible = True
            .Minsat_Wait_Time(0).Visible = True
            .Top = 2600
            .Height = 5610
            .VPN_Frame.Visible = False
            .MINSAT_Live_Frame(0).Visible = True
            .MINSAT_Test_Frame(1).Visible = True
'        Set_Wait_Time_Detailed.Label1(2) = "MINSAT Startup"
 '       Set_Wait_Time_Detailed.Label1(1) = "MINSAT Wait before setting MSISDN"
            .Caption = "MINSAT Live Detailed" + Set_Wait_Time_Detailed.Caption
            .Visible = True
        End With
        Call Move_Form(Main, "Left", 3000)
        Call Slide(Set_Wait_Time_Detailed, Main.Left + Main.Width, 4800, True)
End Sub



Private Sub All_Timer_Timer()
    All_Timer.Enabled = False
    
    On Error GoTo ErrorHandler
    Applications_Handler
    
ErrorHandler:
    All_Timer.Enabled = True
End Sub

Private Sub MMFs_Click()
    Close_All
    Call Move_Form(Main, "Left", 900)
    Call Slide(Vmm_form, Main.Left + Main.Width, 6150, True)
    'Call VASP.MMFs_Command_Click
End Sub



Private Sub MPS_FTP_Conf_Click()
    Call Load_List(Set_Wait_Time.Path, "Paths")
    Set_Time_Object = "FTP_MPS"
    Close_All
    Call Extend("Down")
    Set_Wait_Time.Caption = "FTP MPS" + Set_Wait_Time.Caption
    Set_Wait_Time.Visible = True
    Call Move_Form(Main, "Left", 3000)
    Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3840, True)
End Sub

Private Sub MSISDNS_GotFocus()
    SendMessage MSISDNS.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub MSISDNS_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = MSISDNS.Text
        For I = 0 To MSISDNS.ListCount - 1
            If StrComp(PSTR, (Left(MSISDNS.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                MSISDNS.ListIndex = I
            Exit For
            End If
        Next I
        MSISDNS.SelStart = Len(PSTR)
        MSISDNS.SelLength = Len(MSISDNS.Text) - Len(PSTR)
    End If
End Sub

Private Sub MSISDNS_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(MSISDNS.Text) = 0) Then
        SendMessage MSISDNS.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage MSISDNS.hwnd, CB_SHOWDROPDOWN, 0, 1
    End If
    If KeyAscii = 32 And Len(MSISDNS.Text) = 0 Then KeyAscii = 0
    
End Sub

Private Sub MSISDNS_LostFocus()
    Call Save_List(MSISDNS, "MSISDNS", True)
    Call Roll_Out_Msisdns(MSISDNS)
End Sub

Private Sub Omega_command_Click()
    Dim OMEGA
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        Application_Wait = "Omega"
                
        OMEGA = Shell(Config.OMEGA_Path + "\Omega.exe", vbNormalFocus)
        
        Applications_Handler
    End If
'LocalHandler:
 '   Call Handler_(Err.Number)
End Sub

Private Sub Omega_Time_Command_Click()

    
End Sub

Private Sub Peri_Pro_Conf_Click()
    Peri_Form.Caption = "PeriPro"
    Call Load_Peri_Conf
    Peri_Form.Paths_List = PeriPro_Last_Path
End Sub
Public Sub Load_Peri_Conf()
    
    Close_All
    Peri_Form.Visible = True
    Call Move_Form(Main, "Left", 2000)
    Call Slide(Peri_Form, Main.Left + Main.Width, 4800, True)
    Exceed_Time_Command = Wait_Exceed
    
    'Call Load_List(Peri_Form.Paths_List, "Peripro")
    
End Sub

Private Sub Peri_Studio_Conf_Click()
    Peri_Form.Caption = "PeriStudio"
    Call Load_Peri_Conf
    Peri_Form.Paths_List = PeriStudio_Path
End Sub

Private Sub PeriPro_Click()
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If Peri_TestMPS.Value = True Then
            If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
                Call_Exceed
                Tel_MPS_Click
                Wait 700
                
                Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(0) = "cd " + PeriPro_Last_Path
                Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(1) = "setenv DISPLAY " + Config.VPN_IP + ":0.0"
                Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(2) = "peripro&"
                Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Close_ = "Terminate"
                'SendKeys "cd " + PeriPro_Last_Path + vbCrLf
                'SendKeys "setenv DISPLAY " + Config.VPN_IP + ":0.0" + vbCrLf
                'SendKeys "peripro&"
            Else
                Dim Connect_ As Integer
                Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                If Connect_ = 1 Then
                    Call VPN_Connect_Click
                    Wait 1000
                    Call PeriPro_Click
                Else
                    GoTo Sultan
                End If
            End If
        Else
            
            Call_Exceed
        
            Local_IP = Get_IP_Address()
                    
            Tel_Reda_Click
            Wait 700
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(0) = "cd " + PeriPro_Last_Path
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(1) = "setenv DISPLAY " + Local_IP + ":0.0"
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(2) = "peripro&"
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Close_ = "Terminate"
            
            'SendKeys "cd " + PeriPro_Last_Path + vbCrLf
            'SendKeys "setenv DISPLAY " + Local_IP + ":0.0" + vbCrLf
            'SendKeys "peripro&"
        End If
                
        SendKeys "{ENTER}"
        SendKeys "^D"
    End If

Sultan: Connect_ = 2    'anything

End Sub
Public Sub Call_Exceed()
    Dim Exceed      'Passive Exceed
    Exceed = Shell(Config.Exceed_Path + "\exceed.exe", vbNormalFocus)
    
    If Peri_Enter.Value = 1 Then    'to Elluminate the press OK prompt
    Wait Wait_Exceed
    SendKeys "{ENTER}"
    End If
    
    Progressing (Wait_Exceed / 1000)
    Application_Wait = "Exceed"
    Applications_Handler
    Wait 500
    'Wait Wait_Exceed
End Sub
Private Sub PeriStudio_Click()
    Dim Exceed      'Passive Exceed
    Exceed = Shell(Config.Exceed_Path + "\exceed.exe", vbNormalFocus)
    
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If Peri_Enter.Value = 1 Then    'to Elluminate the press OK prompt
            Wait Wait_Exceed
            SendKeys "{ENTER}"
            End If
        
        Wait Wait_Exceed
        SendKeys "m"
        SendKeys "m"
        SendKeys "{TAB}"
        SendKeys "{TAB}"
        SendKeys "{TAB}"
        SendKeys "{ENTER}"
           
        If Peri_TestMPS.Value = True Then
            Tel_MPS_Click
            Wait 500
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(0) = "cd " + PeriStudio_Path
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(1) = "setenv DISPLAY " + Config.VPN_IP + ":0.0"
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(2) = "peristudio&"
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Close_ = "Terminate"
            Else
            Local_IP = Get_IP_Address()
                    
            Tel_Reda_Click
            Wait 500
            
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(0) = "cd " + PeriStudio_Path
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(1) = "setenv DISPLAY " + Local_IP + ":0.0"
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(2) = "peristudio&"
            Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Close_ = "Terminate"
            
'            SendKeys "cd " + PeriStudio_Path + ";setenv DISPLAY " + Local_IP + ":0.0;peristudio&"
        End If
                
        'SendKeys "{ENTER}"
        'SendKeys "^D"
    End If
    
End Sub



Private Sub REDA_FTP_Conf_Click()
    Call Load_List(Set_Wait_Time.Path, "Paths")
    Set_Time_Object = "FTP_Reda"
    Close_All
    Call Extend("Down")
    Set_Wait_Time.Caption = "FTP REDA" + Set_Wait_Time.Caption
    Set_Wait_Time.Visible = True
    Call Move_Form(Main, "Left", 3000)
    Call Slide(Set_Wait_Time, Main.Left + Main.Width, 3840, True)
End Sub

Public Sub Remain_Click()
    If TelnetActivated = "False" Then
        '    Call Tel_MPS_Click
        '    SendKeys "NETSTAT"
        '    SendKeys "{ENTER}"
        Call Slide(Telnet_Form, Main.Left + Main.Width, TelNetFormWidth, True)
            Telnet_Form.Telnet1.RemotePort = 23
            Telnet_Form.Telnet1.Connect "172.28.6.15"
            Telnet_Form.Telnet_Timer.Enabled = True
            
            Wait 300
        'Option Explicit
    Else
        If Telnet_Form.WindowState = vbMinimized Then Telnet_Form.WindowState = vbNormal
        Call Slide(Telnet_Form, Main.Left + Main.Width, TelNetFormWidth, True)
        Wait 4000
        Call Slide(Telnet_Form, Telnet_Form.Left + TelNetFormWidth, TelNetFormWidth, False)
    End If

'Const SW_SHOWMINIMIZED = 2
'Const SW_SHOWMAXIMIZED = 3
'Const SW_SHOWNORMAL = 1
'' Find the target window and minimize, maximize,
'' or restore it.
'Dim app_hwnd As Long
'Dim wp As WINDOWPLACEMENT
'    ' Find the target.
'    app_hwnd = FindWindow(vbNullString, "Telnet 172.28.6.15")
'    ' Get the window's current placement information.
'    wp.Length = Len(wp)
'    GetWindowPlacement app_hwnd, wp
'    wp.showCmd = SW_SHOWMINIMIZED
'    ' Perform the action.
'    SetWindowPlacement app_hwnd, wp
    
End Sub

Private Sub ShortCuts_Click()
    Call Main2.ShortCuts_Click
End Sub
Private Sub ShortCuts_Menu_Click()
    Call Main2.ShortCuts_Click
End Sub

Public Sub Sulsat2_command_Click()
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
    Else
        If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP) Or IsProcessRunning("vpngui.exe") Then
            'Call Close_All_Main2
            Call Move_Form(Main, "left", 800)
            Call Slide(SulSAT2, Main.Left + Main.Width, SulSAT2.Width, True)
            
            'Initiating connction With MINSAT CCAPI
            SulSAT2.CCAPI_Connection.Close
            SulSAT2.CCAPI_Connection.RemoteHost = "172.28.6.15"
            'SulSAT2.CCAPI_Connection.RemoteHost = "172.30.5.30"
            SulSAT2.CCAPI_Connection.RemotePort = "7020"
            SulSAT2.CCAPI_Connection.Protocol = sckTCPProtocol
            SulSAT2.CCAPI_Connection.Connect
        Else
            Dim Connect_ As Integer
            Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
            If Connect_ = 1 Then
                Call VPN_Connect_Click
                Wait 200
                
                Call Sulsat2_command_Click
            End If
        End If
    End If
End Sub

Private Sub SulSAT2_Menu_Click()
    Call Sulsat2_command_Click
End Sub

Private Sub SulSAT2_Click()

End Sub

Private Sub Tappman__Menu_Click()
    Call Tappman_Click
End Sub

Private Sub Tappman_Click()
    Close_All
    
    With Commands
        .App_Label.Visible = True
        If .MPS Then .MPS_Text = "101"
        If .FCD Then .MPS_Text = "102"
        .MPS_Label.Caption = "MPS #"
        .Status.Visible = True
        .Caption = "Tappman Commands"
        .App_Text.Visible = True
        .ON_Assign.Caption = "Assign And On"
        .OFF_UnAssign.Caption = "Off And Unassign"
        .On.Visible = True
        .MPS.Visible = True
        .FCD.Visible = True
        .Off.Visible = True
        .Assign.Visible = True
        .Unassign.Visible = True
    End With
    
    Call Move_Form(Main, "Left", 900)
    Call Slide(Commands, Main.Left + Main.Width, 6150, True)
    Commands.Show
End Sub

Private Sub Tariff_Menu_Click()
    Call IN_Click(2)
End Sub

Public Sub Tel_MPS_Click()
    Dim Wait_Time As String
    
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
            Dim Path As String
            Path = MPS_Telnet_Path
            Call Create_Telnet_Form("172.28.6.15", Config.TESTMPS_User, Config.TESTMPS_PIN, Path)
    
            If Set_Wait_Time.Times > 1 Then
                For I = 2 To Set_Wait_Time.Times
                    Path = MPS_Telnet_Path
                    Call Create_Telnet_Form("172.28.6.15", Config.TESTMPS_User, Config.TESTMPS_PIN, Path)
                Next I
            End If
        Else
            Dim Connect_ As Integer
            Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
            If Connect_ = 1 Then
                Call VPN_Connect_Click
                Wait 200
                
                Dim MPS_Check_Old_Value As Integer
                MPS_Check_Old_Value = MPS_Check.Value
                
                MPS_Check.Value = 1
                Call Tel_MPS_Click
                MPS_Check.Value = MPS_Check_Old_Value
            End If
        End If
    End If
End Sub
Private Sub TelNet()
    Dim TelNet
    TelNet = Shell(Config.Exceed_Path + "\TNVT.EXE", vbNormalFocus)
    'Wait Wait_Telnet
    Wait 1000
End Sub
Private Sub Goto_TelNet(GotoMPS As Boolean)
    If GotoMPS Then
        For I = 1 To Set_Wait_Time.Times
            TelNet
            Wait 500
            SendKeys "T"
            'SendKeys "T"
            SendKeys "{ENTER}"
            Wait Wait_MPS
            SendKeys Config.TESTMPS_User
            SendKeys "{ENTER}"
            Wait 1000
            SendKeys Config.TESTMPS_PIN
            SendKeys "{ENTER}"
            Wait 700
            SendKeys "cd " + MPS_Telnet_Path
            SendKeys "{ENTER}"
        Next I
        Set_Wait_Time.Times = 1
    Else
        For I = 1 To Set_Wait_Time.Times
            TelNet
            SendKeys "E"
            SendKeys "{ENTER}"
            Wait Wait_Reda
            SendKeys Config.Reda_User
            SendKeys "{ENTER}"
            Wait 700
            SendKeys Config.Reda_PIN
            SendKeys "{ENTER}"
            Wait 700
            SendKeys "cd " + Reda_Telnet_Path
            SendKeys "{ENTER}"
            Wait 100
        Next I
        Set_Wait_Time.Times = 1
    End If
End Sub

Private Sub Advanced_TelNet_Click()

Dim Wait_Time As String
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If Peri_TestMPS.Value = True Then
            If (IsProcessRunning("ipsecdialer.exe") Or Get_IP_Address = Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
                
                If MPS_Check.Value = 1 Then
                    Wait_Time = Wait_MPS + 2000
                Else
                    Wait_Time = Wait_MPS
                End If
                
                If Set_Wait_Time.Times >= 1 Then
                    Goto_TelNet (True)
                End If
                Else
                    Dim Connect_ As Integer
                    Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
                    If Connect_ = 1 Then
                        Call VPN_Connect_Click
                        Wait 2000
                        Call Advanced_TelNet_Click
                    End If
            End If
        Else
            Goto_TelNet (False)
        End If
    End If
End Sub

Private Sub Tel_Reda_Click()
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        'Dim Reda As Double
        'Dim REDA_Wait_Time As String
            
        'If MPS_Check.Value = 1 Then
        '    REDA_Wait_Time = Wait_Reda + 2000
        'Else
        '    REDA_Wait_Time = Wait_Reda
        'End If
        Dim Path As String
        For I = 1 To Set_Wait_Time.Times
            'Reda = Shell(Config.MPS_Connect_Path + "\Telnet Reda.bat", vbNormalFocus)
            'Wait REDA_Wait_Time
            'SendKeys Config.Reda_User
            'SendKeys "{ENTER}"
            'Wait 500
            'SendKeys Config.Reda_PIN
            'SendKeys "{ENTER}"
            'Wait 100
            'SendKeys "cd " + Reda_Telnet_Path
            'SendKeys "{ENTER}"
            'Wait 100
            Path = Reda_Telnet_Path
            Call Create_Telnet_Form("10.231.14.21", Config.Reda_User, Config.Reda_PIN, Path)
        Next I
    End If
End Sub

Private Sub Tel_Reda2_Click()
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        Dim TelNet
            For I = 1 To Set_Wait_Time.Times
                TelNet = Shell(Config.Exceed_Path + "\TNVT.EXE", vbNormalFocus)
                Wait Wait_Telnet
                SendKeys "E"
                SendKeys "{ENTER}"
                Wait Wait_Reda
                SendKeys Config.Reda_User
                SendKeys "{ENTER}"
                Wait 700
                SendKeys Config.Reda_PIN
                SendKeys "{ENTER}"
                Wait 700
                SendKeys "cd " + Reda_Telnet_Path
                SendKeys "{ENTER}"
                Wait 100
            Next I
            Set_Wait_Time.Times = 1
    End If
End Sub

Private Sub Telnet_AIR1_Click()
    TelNet
    SendKeys "A"
    SendKeys "{ENTER}"
    Wait Wait_Telnet
    SendKeys "root"
    SendKeys "{ENTER}"
    Wait 500
    SendKeys Config.AIR1_PIN
    SendKeys "{ENTER}"
End Sub

Private Sub Advanced_Telnet_Time_Command_Click()
    Set_Time_Object = "TelNet"
    Close_All
    'Call Extend("Down")
    Set_Wait_Time.Caption = "Telnet" + Set_Wait_Time.Caption
    Set_Wait_Time.Visible = True
    Call Move_Form(Main, "Left", 3000)
    Call Slide(Set_Wait_Time, Main.Left + Main.Width, Set_Wait_Time.Width, True)
End Sub

Private Sub Test_Cmd_Click()
'    Call Main.Create_Telnet_Form("172.28.6.15", "peri", "peri", "", 21)
'    Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(0) = "setenv DISPLAY " + Config.VPN_IP + ":0.0"
'    Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(1) = "hash"
'    Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(2) = "bin"
'    Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Additional_Commands(3) = ""
'    Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Close_ = "Terminate"

    Test_Form.Show
End Sub





Private Sub Test1_Click()

End Sub

Private Sub Tool_Selection_Click()
    Select Case UCase(Tool_Selection.Text)
        Case "IVR"
            Call Hide_All_Frames(Me, Frame_IVR)
        Case "IN"
            Call Hide_All_Frames(Me, Frame_IN)
        Case "APPLICATIONS"
            Call Hide_All_Frames(Me, Frame_Apps)
    End Select
End Sub
Private Sub UCIP_Menu_Click()
    Call SulSAT.ShoW_UCIP_Click
End Sub

Private Sub VASP_AIR_Click()
    Close_All
    Call Move_Form(Main, "Left", 1500)
    Call Slide(VASP, Main.Left + Main.Width, 6000, True)
End Sub


Private Sub VNPP_Commands_Click(Index As Integer)
    Dim Wait_Time As String
    
    If Check_Connectivity = 0 Then
            MsgBox "Sorry you can't connect since there is no local connection"
        Else
            Dim Yesterdays_Date As String
            Yesterdays_Date = Format(Date - 1, "DD/MM/YYYY")
            
            Select Case Index
                Case CallCollect
                    Call Create_Telnet_Form("10.231.45.27", _
                                                "vas_sys", "vas_sys", _
                                                "DataCollection", , _
                                                "./Batchdc.sh call_collect_service_sys " + Yesterdays_Date + " " + Yesterdays_Date + " . &")
                Case USSD
                     Call Create_Telnet_Form("10.231.45.27", _
                                                "vas_sys", "vas_sys", _
                                                "DataCollection", , _
                                                "./Batchdc.sh USSD " + Yesterdays_Date + " " + Yesterdays_Date + " . &")
                Case SDP
                    Call Create_Telnet_Form("10.231.45.27", _
                                                                   "vas_sys", "vas_sys", _
                                                                   "DataCollection", , _
                                                                   "./Batchdc.sh SDP " + Yesterdays_Date + " " + Yesterdays_Date + " . &")
                Case OMN
                    Call Create_Telnet_Form("10.231.45.27", _
                                                                   "vas_sys", "vas_sys", _
                                                                   "DataCollection", , _
                                                                   "./Batchdc.sh OMN " + Yesterdays_Date + " " + Yesterdays_Date + " . &")
                End Select
    End If
End Sub

Private Sub VPN_Conf_Click()
    VPN_Conf_Form.Top = VPN_Conf.Top + Me.Top
    VPN_Conf_Form.Timer1.Enabled = True
    Call Slide(VPN_Conf_Form, VPN_Conf.Width + Me.Left + Frame_Apps.Left + VPN_Connect.Left + VPN_Connect.Width, VPN_Conf_Form.Width, True)
End Sub

Public Sub VPN_Connect_Click()
    Dim RSA, VPNdialer As Double
    Dim Proceed As Boolean
    Proceed = False
    VPN_Conf_Form.Timer1.Enabled = True
    VPN_Conf_Form.Visible = False
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        If (Not IsProcessRunning("ipsecdialer.exe") And Get_IP_Address <> Config.VPN_IP Or IsProcessRunning("vpngui.exe")) Then
        
            If IsProcessRunning("vpngui.exe") Then KillProcess ("vpngui.exe")
            Dim Remain_Connection As Integer
            If VPN_Conf_Form.GSM.Value Or VPN_Conf_Form.GSM_Remote Then
                'Remain_Connection = MsgBox("Keep the VPN Connected?", vbYesNoCancel, "VPN Connection")             'yes=6
                Remain_Connection = 7
            Else                                                                                    'No=7
                Remain_Connection = 7                                                               'Cancel=2
            End If
            Select Case Remain_Connection
            
                Case 6  'Yes
                    With_Remain.Value = 1
                    Proceed = True
                Case 7  'No
                    With_Remain.Value = 0
                    Proceed = True
                Case 2  'Cancel
                    Proceed = False
            End Select
            
            If Proceed = True Then
                
                If Not IsProcessRunning("SecurID.exe") Then
                    Application_Wait = "RSA"
                    Applications_Handler
                    RSA = Shell(Config.RSA_Path + "\SecurID.exe", vbNormalFocus)
                    Wait Wait_RSA
                    'On Error GoTo LocalHandler
                    If VPN_Conf_Form.RSA_Version(0).Value = True Then
                        AppActivate (RSA)
                        SendKeys Config.VPN_PIN
                        Wait 100
                        SendKeys "{ENTER}"
                        Wait 100
                        SendKeys "{TAB}"
                        Wait 100
                        SendKeys "{TAB}"
                        Wait 100
                        SendKeys "{TAB}"
                        Wait 100
                        SendKeys "^C"
                        Wait 200
                        SendKeys "%{F4}"
                        Wait 100
                    Else
                        AppActivate (RSA)
                        SendKeys Config.VPN_PIN
                        Wait 100
                        Wait 500
                        SendKeys "{ENTER}"
                        Wait 100
                        Wait 500
                        SendKeys "{TAB}"
                        Wait 100
                        Wait 500
                        SendKeys "{ENTER}"
                        Wait 100
                        Wait 500
                        SendKeys "%{F4}"
                        Wait 500
                    End If
                    'Application_Wait = "VPNdialer"
                    VPNdialer = Shell(Config.VPN_Path + "\ipsecdialer.exe", vbNormalFocus)
                    'Applications_Handler
                    Wait Wait_VPN
                    If VPN_Conf_Form.VPN_Version(0).Value Then      'V3.0
                        AppActivate ("Cisco Systems VPN Client")
                        SendKeys "{TAB}"
                        SendKeys "{TAB}"
                        SendKeys "{DOWN}"
                    End If
                    If VPN_Conf_Form.VPN_Version(1).Value Then      'V4.0
                        AppActivate ("VPN Client - Version 4.0 (Rel)")
                    End If
                    If VPN_Conf_Form.GSM_Remote.Value = True Then SendKeys "R"
                    If VPN_Conf_Form.GSM.Value = True Then SendKeys "G"
                    If VPN_Conf_Form.My_Work.Value = True Then SendKeys "M"
                    SendKeys "{ENTER}"
                    'Wait Wait_VPN
                    Wait 500
                    Application_Wait = "VPN Dialer Authentication"
                    Applications_Handler
                    'SendKeys "^V"
                    'SendKeys "{ENTER}"
                    
                    If With_Remain.Value = 1 Then
                        MPS_Check.Value = 1
                        Wait 3000
                        Call Remain_Click
                        MPS_Check.Value = 0
                    End If
                Else
                    KillProcess ("SecurID.exe")
                    VPN_Connect_Click
                End If
            End If
        Else
            MsgBox "Sorry Can't, Since You Are Already Connected"
        End If
    End If
End Sub

Private Sub VPN_Time_Command_Click()
    
End Sub

Private Function setNOTIFYICONDATA(hwnd As Long, ID As Long, Flags As Long, CallbackMessage As Long, Icon As Long, Tip As String) As NOTIFYICONDATA
    Dim nidTemp As NOTIFYICONDATA

    nidTemp.cbSize = Len(nidTemp)
    nidTemp.hwnd = hwnd
    nidTemp.uID = ID
    nidTemp.uFlags = Flags
    nidTemp.uCallbackMessage = CallbackMessage
    nidTemp.hIcon = Icon
    nidTemp.szTip = Tip & Chr$(0)

    setNOTIFYICONDATA = nidTemp
End Function


