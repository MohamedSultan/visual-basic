VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   2085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4020
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2085
   ScaleWidth      =   4020
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Default         =   -1  'True
      Height          =   375
      Left            =   2760
      TabIndex        =   2
      Top             =   1320
      Width           =   1095
   End
   Begin Project1.URLLink URLLink1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   1296
      ShowToolTip     =   -1  'True
   End
   Begin VB.Label Label1 
      Caption         =   $"URLLink.frx":0000
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   3615
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'URLLink - Simply URL control demo program
'Copyright (c) 1997 SoftCircuits Programming (R)
'Redistributed by Permission.
'
'This Visual Basic 5.0 example program demonstrates how to
'automatically load a user's web browser and point it to a particular
'web site. The URL can also be a data file that has an extension that
'has been registered by an application on your system. The appropriate
'application is automatically loaded with the specified data file or
'URL.
'
'The code is implemented as a simple control. The text (which can be
'different from the actual URL) is displayed with an underline and the
'mouse pointer changes to a hand when it is over the text. The URL
'property specifies the URL or data file that is loaded when the text
'is clicked. If the ShowToolTip property is true, a tooltip with the
'underlying URL is displayed when the mousepointer is parked over the
'text. The only event, GoToURL, gives you a chance to change the URL
'or cancel altogether after the text is clicked.
'
'This program may be distributed on the condition that it is
'distributed in full and unchanged, and that no fee is charged for
'such distribution with the exception of reasonable shipping and media
'charged. In addition, the code in this program may be incorporated
'into your own programs and the resulting programs may be distributed
'without payment of royalties.
'
'This example program was provided by:
' SoftCircuits Programming
' http://www.softcircuits.com
' P.O. Box 16262
' Irvine, CA 92623
Option Explicit

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub URLLink1_GoToURL(URL As String, Cancel As Boolean)
    'Here you can either modify the URL before it
    'is loaded (does not affect the regular URL
    'property) or cancel the operation altogether
End Sub
