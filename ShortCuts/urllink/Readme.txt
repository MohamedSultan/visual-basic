URLLink - Simply URL control demo program
Copyright (c) 1997 SoftCircuits Programming (R)
Redistributed by Permission.

This Visual Basic 5.0 example program demonstrates how to
automatically load a user's web browser and point it to a particular
web site. The URL can also be a data file that has an extension that
has been registered by an application on your system. The appropriate
application is automatically loaded with the specified data file or
URL.

The code is implemented as a simple control. The text (which can be
different from the actual URL) is displayed with an underline and the
mouse pointer changes to a hand when it is over the text. The URL
property specifies the URL or data file that is loaded when the text
is clicked. If the ShowToolTip property is true, a tooltip with the
underlying URL is displayed when the mousepointer is parked over the
text. The only event, GoToURL, gives you a chance to change the URL
or cancel altogether after the text is clicked.

This program may be distributed on the condition that it is
distributed in full and unchanged, and that no fee is charged for
such distribution with the exception of reasonable shipping and media
charged. In addition, the code in this program may be incorporated
into your own programs and the resulting programs may be distributed
without payment of royalties.

This example program was provided by:
 SoftCircuits Programming
 http://www.softcircuits.com
 P.O. Box 16262
 Irvine, CA 92623
