VERSION 5.00
Begin VB.Form VPN_Conf_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VPN Configurations"
   ClientHeight    =   3000
   ClientLeft      =   2145
   ClientTop       =   3390
   ClientWidth     =   2070
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   2070
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton RSA_Version 
      Caption         =   "V4.1"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   9
      Top             =   1320
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Connection Entry:"
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   1560
      Width           =   1695
      Begin VB.OptionButton GSM 
         BackColor       =   &H00E0E0E0&
         Caption         =   "GSM Local"
         Height          =   195
         Left            =   120
         MaskColor       =   &H00E0E0E0&
         TabIndex        =   3
         ToolTipText     =   "GSM Local"
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton GSM_Remote 
         BackColor       =   &H00E0E0E0&
         Caption         =   "GSM Remote"
         Height          =   195
         Left            =   120
         MaskColor       =   &H00E0E0E0&
         TabIndex        =   2
         ToolTipText     =   "GSM Remote"
         Top             =   600
         Width           =   1275
      End
      Begin VB.OptionButton My_Work 
         BackColor       =   &H00E0E0E0&
         Caption         =   "My Work"
         Height          =   195
         Left            =   120
         MaskColor       =   &H00E0E0E0&
         TabIndex        =   1
         ToolTipText     =   "My Work"
         Top             =   960
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "RSA Version"
      Height          =   855
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   1695
      Begin VB.OptionButton RSA_Version 
         Caption         =   "V3"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "VPN Version"
      Height          =   855
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   1695
      Begin VB.OptionButton VPN_Version 
         Caption         =   "V 4"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.OptionButton VPN_Version 
         Caption         =   "V 3"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   1800
      Top             =   240
   End
End
Attribute VB_Name = "VPN_Conf_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_GotFocus()
    Timer1.Enabled = False
    Timer1.Enabled = True
End Sub

Private Sub Form_Load()
    Dim Current_Value As Integer
End Sub

Private Sub Form_LostFocus()
    'VPN_Conf_Form.Timer1.Enabled = True
    Call Timer1_Timer
End Sub

Private Sub GSM_Click()
    Main.VPN_Connect.Enabled = True
    Main.VPN_Connect.Caption = "VPN (Local GSM)"
End Sub
Private Sub GSM_Remote_Click()
    Main.VPN_Connect.Enabled = True
    Main.VPN_Connect.Caption = "VPN(RemoteGSM)"
End Sub

Private Sub Label2_Click()

End Sub

Private Sub My_Work_Click()
    Main.VPN_Connect.Enabled = True
    Main.VPN_Connect.Caption = "VPN (My Work)"
End Sub

Private Sub Timer1_Timer()
    Call Slide(Me, Me.Left, Me.Width, False)
    Timer1.Enabled = False
End Sub
