VERSION 5.00
Begin VB.Form Main2 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "More Commands"
   ClientHeight    =   3090
   ClientLeft      =   2475
   ClientTop       =   5010
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Automate_Exceeds_Cmd 
      Caption         =   "Automate Exceeds"
      Height          =   375
      Left            =   1320
      TabIndex        =   7
      Top             =   1080
      Width           =   1455
   End
   Begin VB.CommandButton ShortCuts 
      Caption         =   "ShortCuts"
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Width           =   1095
   End
   Begin VB.CommandButton Prefix_Command 
      Caption         =   "Prefix Operations"
      Height          =   375
      Left            =   2400
      TabIndex        =   5
      Top             =   720
      Width           =   1695
   End
   Begin VB.CommandButton Stored_Procedures_tests 
      Caption         =   "Stored Procedures Tests"
      Height          =   495
      Left            =   2400
      TabIndex        =   4
      Top             =   240
      Width           =   1695
   End
   Begin VB.CommandButton Verify_SDPs 
      Caption         =   "Verify SDPs"
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      ToolTipText     =   "It Gives Analysis on SC5 (How many Creadit clearence =90, how many=30, how many Else and how many not SC5)"
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton Get_Len 
      BackColor       =   &H0080C0FF&
      Caption         =   "Get Length"
      Height          =   375
      Left            =   240
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton UCIP_Command 
      Caption         =   "SULSAT"
      Height          =   495
      Left            =   1320
      TabIndex        =   1
      Top             =   240
      Width           =   1095
   End
   Begin VB.CommandButton VASP_Tests 
      Caption         =   "VASP Tests"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "Main2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Public Sub Automate_Exceeds_Cmd_Click()
    Close_All_Main2
    Call Slide(Automate_Exceeds, Me.Left, Automate_Exceeds.Width, True)
End Sub

Private Sub Form_Terminate()
    Call Close_Me(Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Public Sub Get_Len_Click()
'    Close_All
    Call Close_All_Main2
    Call Move_Form(Main, "Left", 200)
    Call Slide(Get_Len_Form, Main.Left + Main.Width, 7995, True)
End Sub

Public Sub Prefix_Command_Click()
'    Close_All
    Call Close_All_Main2
    Call Move_Form(Main, "left", 800)
    Call Move_Form(Main2, "left", Main.Width + 800)
    Call Slide(Prefix_Form, Main.Left + Main.Width, Prefix_Form.Width, True)
End Sub

Public Sub ShortCuts_Click()
    Call Slide(ShortCut_Form, Main.Left + Main.Width, ShortCut_Form.Width, True)
End Sub

Public Sub Stored_Procedures_tests_Click()
    'Call Close_Child_Forms
    'Call Show_Commands(-1)
Call Close_All_Main2
    Call Slide(Stored_Procedure_types1, _
                Main.Left + Main.Width + Stored_Procedures_tests.Left + Stored_Procedures_tests.Width, _
                Stored_Procedure_types1.Width, True)
End Sub



Public Sub UCIP_Command_Click()
'    Close_All
    Call Close_All_Main2
    Call Move_Form(Main, "left", 800)
    Call Slide(SulSAT, Main.Left + Main.Width, SulSAT.Width, True)
End Sub

Public Sub VASP_Tests_Click()
    Call Close_All_Main2
    Call Slide(VASP_Types, _
                Main.Left + Main.Width + VASP_Tests.Left + VASP_Tests.Width, _
                VASP_Types.Width, True)
End Sub

Public Sub Verify_SDPs_Click()
Call Close_All_Main2
'    Close_All
    Call Move_Form(Main, "left", 800)
    Call Slide(Verify_SDPs_Form, Main.Left + Main.Width, Verify_SDPs_Form.Width, True)
End Sub
Private Sub Close_All_Main2()
    'Call Slide(FAF, FAF.Left, FAF.Width, False)
    Call Slide(Display_Form, Display_Form.Left, Display_Form.Width, False)
    Call Slide(VASP_Types, VASP_Types.Left, VASP_Types.Width, False)
    Call Slide(UCIP, UCIP.Left, UCIP.Width, False)
    Call Slide(Verify_SDPs_Form, Verify_SDPs_Form.Left, Verify_SDPs_Form.Width, False)
    Call Slide(Stored_Procedure_types1, Stored_Procedure_types1.Left, Stored_Procedure_types1.Width, False)
    Call Slide(Stored_Procedure, Stored_Procedure.Left, Stored_Procedure.Width, False)
    Call Slide(Get_Len_Form, Get_Len_Form.Left, Get_Len_Form.Width, False)
    Call Slide(Prefix_Form, Prefix_Form.Left, Prefix_Form.Width, False)
End Sub


