VERSION 5.00
Begin VB.Form Telnet_Instance_Form 
   Caption         =   "Telnet Form"
   ClientHeight    =   4740
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6840
   LinkTopic       =   "Form1"
   ScaleHeight     =   4740
   ScaleWidth      =   6840
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Close_ 
      Height          =   375
      Left            =   2040
      TabIndex        =   9
      Text            =   "Close"
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox Additional_Commands 
      Height          =   375
      Index           =   3
      Left            =   3840
      TabIndex        =   8
      Top             =   3120
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox Additional_Commands 
      Height          =   375
      Index           =   2
      Left            =   3840
      TabIndex        =   7
      Top             =   2760
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox Additional_Commands 
      Height          =   375
      Index           =   1
      Left            =   3840
      TabIndex        =   6
      Top             =   2400
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox Additional_Commands 
      Height          =   375
      Index           =   0
      Left            =   3840
      TabIndex        =   5
      Top             =   2040
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox Path 
      Height          =   375
      Left            =   3840
      TabIndex        =   4
      Text            =   "Path"
      Top             =   1560
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox Password 
      Height          =   375
      Left            =   3840
      TabIndex        =   3
      Text            =   "Password"
      Top             =   1080
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox Login 
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Text            =   "Login"
      Top             =   600
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Timer Telnet_Timer 
      Interval        =   1000
      Left            =   0
      Top             =   480
   End
   Begin VB.TextBox Command_To_Be_Sent 
      Enabled         =   0   'False
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   3960
      Width           =   6615
   End
   Begin VB.TextBox Display 
      Height          =   3735
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   6615
   End
   Begin ShortCut.Telnet Telnet_Connect 
      Left            =   0
      Top             =   0
      _ExtentX        =   900
      _ExtentY        =   900
   End
End
Attribute VB_Name = "Telnet_Instance_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Connected, ProcessFinished As Boolean
Dim Last_Sent_Command As String

Private Sub Command_To_Be_Sent_Click()
'    Command_To_Be_Sent = ""
End Sub

Private Sub Command_To_Be_Sent_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        'If InStr(1, Display, "Password:") <> 0 And Not ProcessFinished Then
        Display = Display + Command_To_Be_Sent + vbCrLf
        Display.SelStart = Len(Display)
        Telnet_Connect.SendData Command_To_Be_Sent, True
        If Command_To_Be_Sent = "clear" Then Display = ""
        If Command_To_Be_Sent = "!!" Then
            Command_To_Be_Sent = Last_Sent_Command
            Call Command_To_Be_Sent_KeyPress(13)
        End If
        Last_Sent_Command = Command_To_Be_Sent
        Command_To_Be_Sent = ""
    End If
End Sub


Private Sub Form_Resize()
    If Me.WindowState = vbNormal Then    'Normal
        Display.Width = Width - 330
        Command_To_Be_Sent.Width = Display.Width
        
        Display.Height = Me.Height - Command_To_Be_Sent.Height - 900 '3735
        Command_To_Be_Sent.Top = Display.Top + Display.Height + 105 '3960
    End If
End Sub

Private Sub Form_Terminate()
    Telnet_Connect.Disconnect
    If Me.WindowState <> vbNormal Then Me.WindowState = vbNormal
    Call Slide(Me, Me.Left, Me.Width, False)
End Sub

Public Sub Form_Unload(Cancel As Integer)
    'While Not Connected
    '    X = 1
    'Wend
    Call Form_Terminate
End Sub

Private Sub Telnet_Connect_Connected()
    Caption = Caption + " - Connected"
End Sub

Private Sub Telnet_Connect_DataRecieved(ByVal Data As String)
    Telnet_Timer.Enabled = True
    Display = Display & Data
    Display.SelStart = Len(Display)
End Sub

Private Sub Telnet_Connect_ErrorOccured(ByVal ErrorDesc As String)
    If ErrorDesc <> "Connection is aborted due to timeout or other failure" Then
        MsgBox ("Error Occure : " & ErrorDesc)
        Call Slide(Me, Me.Left, Me.Width, False)
    Else
        MsgBox ("Error Occure : " & ErrorDesc)
    End If
End Sub

Private Sub Telnet_Timer_Timer()
    If InStr(1, Display, "login:") <> 0 And InStr(1, Display, "Last login:") = 0 And Not ProcessFinished Then
        Telnet_Connect.SendData Login, True
        Display = ""
    End If
    If InStr(1, Display, "Password:") <> 0 And Not ProcessFinished Then
        Telnet_Connect.SendData Password, True
        Display = ""
        Wait 1000
        Telnet_Connect.SendData "cd " + Path, True
        For I = 0 To Additional_Commands.Count - 1
            If Additional_Commands(I) <> "" Then Telnet_Connect.SendData Additional_Commands(I), True
        Next I
        Connected = True
        Command_To_Be_Sent.Enabled = True
        If Close_ = "Terminate" Then Call Slide(Me, Me.Left, Me.Width, False)
        ProcessFinished = True
    End If
    Telnet_Timer.Enabled = False
End Sub
