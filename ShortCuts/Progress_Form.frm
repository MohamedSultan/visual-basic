VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Progress_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   675
   ClientLeft      =   3840
   ClientTop       =   8520
   ClientWidth     =   7725
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   675
   ScaleWidth      =   7725
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   0
      Top             =   120
   End
   Begin VB.TextBox Time_Elapsed 
      Height          =   375
      Left            =   480
      TabIndex        =   1
      Top             =   240
      Visible         =   0   'False
      Width           =   615
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
End
Attribute VB_Name = "Progress_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Timer1_Timer()
    If Int(Val(Time_Elapsed)) < ProgressBar1.Max Then
        Time_Elapsed = Int(Val(Time_Elapsed)) + 1
        ProgressBar1.Value = Time_Elapsed
    Else
        Call Slide(Progress_Form, Progress_Form.Left, Progress_Form.Width, False)
        Timer1.Enabled = False
    End If
End Sub
