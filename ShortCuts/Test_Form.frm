VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Begin VB.Form Test_Form 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12240
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   12240
   StartUpPosition =   3  'Windows Default
   Begin VB.VScrollBar VScroll1 
      Height          =   1215
      Left            =   5040
      Max             =   0
      TabIndex        =   6
      Top             =   120
      Width           =   375
   End
   Begin VB.ListBox List4 
      Height          =   1230
      Left            =   3840
      TabIndex        =   8
      Top             =   120
      Width           =   1455
   End
   Begin VB.ListBox List3 
      Height          =   1230
      Left            =   2640
      TabIndex        =   7
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      Height          =   735
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   5
      Text            =   "Test_Form.frx":0000
      Top             =   1440
      Width           =   3135
   End
   Begin MSACAL.Calendar Calendar1 
      Height          =   2295
      Left            =   6960
      TabIndex        =   4
      Top             =   120
      Width           =   4695
      _Version        =   524288
      _ExtentX        =   8281
      _ExtentY        =   4048
      _StockProps     =   1
      BackColor       =   -2147483633
      Year            =   2008
      Month           =   3
      Day             =   1
      DayLength       =   1
      MonthLength     =   1
      DayFontColor    =   0
      FirstDay        =   7
      GridCellEffect  =   1
      GridFontColor   =   10485760
      GridLinesColor  =   -2147483632
      ShowDateSelectors=   -1  'True
      ShowDays        =   -1  'True
      ShowHorizontalGrid=   -1  'True
      ShowTitle       =   -1  'True
      ShowVerticalGrid=   -1  'True
      TitleFontColor  =   10485760
      ValueIsNull     =   0   'False
      BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox List2 
      Height          =   1230
      Left            =   1440
      TabIndex        =   3
      Top             =   120
      Width           =   1455
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H0000FF00&
      Caption         =   "Command1"
      Height          =   375
      Left            =   1080
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2280
      Width           =   1935
   End
   Begin MSForms.ListBox ListBox1 
      Height          =   1215
      Left            =   3480
      TabIndex        =   2
      Top             =   1560
      Width           =   3135
      VariousPropertyBits=   746589211
      ScrollBars      =   3
      DisplayStyle    =   2
      Size            =   "5530;2465"
      cColumnInfo     =   1
      MatchEntry      =   0
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      Object.Width           =   "1058"
   End
End
Attribute VB_Name = "Test_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Calendar1_Click()
    X = Calendar1.Month
End Sub

Private Sub Command1_Click()
    ListBox1.AddItem "1" & vbTab & "1"
    ListBox1.AddItem "2" & vbTab & "2"
    ListBox1.AddItem "3" & vbTab & "3"
    ListBox1.AddItem "4" & vbTab & "4"
    
    List1.AddItem "1"
    List1.AddItem "2"
    List1.AddItem "3"
    List1.AddItem "4"
    
    List2.AddItem "1"
    List2.AddItem "2"
    List2.AddItem "3"
    List2.AddItem "4"
    
    List3.AddItem "1"
    List3.AddItem "2"
    List3.AddItem "3"
    List3.AddItem "4"
    
        List4.AddItem "1"
    List4.AddItem "2"
    List4.AddItem "3"
    List4.AddItem "4"
    
    VScroll1.Max = List1.ListCount - 1
    
    'MsgBox RGB(Command1.BackColor)
    
End Sub

Private Sub Form_Load()
'   Dim I   ' Declare variable.
'   List1.Move 50, 50, 2000, 1750   ' Arrange list boxes.
'   List2.Move 2500, 50, 3000, 1750
'   For I = 0 To Screen.FontCount - 1  ' Fill both boxes with
'      List1.AddItem Screen.Fonts(I)   ' names of screen fonts.
'      List2.AddItem Screen.Fonts(I)
'   Next I
End Sub
Private Sub Click(Except As ListBox)
X = List2.List(2)
    If Except.Name <> "List1" Then _
        List1.ListIndex = Except.ListIndex
    If Except.Name <> "List2" Then _
        List2.ListIndex = Except.ListIndex
    If Except.Name <> "List3" Then _
        List3.ListIndex = Except.ListIndex
    If Except.Name <> "List4" Then _
        List4.ListIndex = Except.ListIndex
    'If Except.Name <> "History_Field3_list" Then _
    '    History_Field3_list.Text = History_Field3_list.List(Except.ListIndex)
    'If Except.Name <> "History_Field4_list" Then _
    '    History_Field4_list.Text = History_Field4_list.List(Except.ListIndex)
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
Call Click(List1)
End Sub

Private Sub List1_KeyUp(KeyCode As Integer, Shift As Integer)
Call Click(List1)
End Sub

Private Sub List1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Call Click(List1)
End Sub


Private Sub List2_KeyPress(KeyAscii As Integer)
Call Click(List2)
End Sub

Private Sub List2_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Call Click(List2)
End Sub

Private Sub List3_KeyPress(KeyAscii As Integer)
Call Click(List3)
End Sub

Private Sub List3_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

Call Click(List3)
End Sub

Private Sub List4_KeyPress(KeyAscii As Integer)
Call Click(List4)
End Sub

Private Sub List4_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

Call Click(List4)
End Sub


Private Sub VScroll1_Change()
    If VScroll1.Value <= List1.ListCount Then
        List1.ListIndex = VScroll1.Value
        List2.ListIndex = VScroll1.Value
        List3.ListIndex = VScroll1.Value
        List4.ListIndex = VScroll1.Value
    End If
End Sub
