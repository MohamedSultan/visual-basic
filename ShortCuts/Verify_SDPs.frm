VERSION 5.00
Begin VB.Form Verify_SDPs_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Verify SDPs "
   ClientHeight    =   3390
   ClientLeft      =   6840
   ClientTop       =   4320
   ClientWidth     =   7425
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   7425
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame8 
      Caption         =   "Files To Be Proccessed - Status"
      Height          =   3135
      Left            =   7440
      TabIndex        =   23
      Top             =   120
      Width           =   5055
      Begin VB.TextBox Files 
         Height          =   2655
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   24
         Text            =   "Verify_SDPs.frx":0000
         Top             =   360
         Width           =   4815
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Case"
      Height          =   855
      Left            =   120
      TabIndex        =   19
      Top             =   960
      Width           =   1215
      Begin VB.OptionButton Option_File 
         Caption         =   "File"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton Option_Folder 
         Caption         =   "Folder"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   480
         Width           =   975
      End
   End
   Begin VB.TextBox Folder 
      Height          =   285
      Left            =   1320
      TabIndex        =   17
      Top             =   600
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CheckBox Store_Records 
      Caption         =   "Store Records"
      Height          =   255
      Left            =   1440
      TabIndex        =   16
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Frame Frame6 
      Caption         =   "SC5"
      Height          =   1575
      Left            =   3840
      TabIndex        =   9
      Top             =   960
      Width           =   3495
      Begin VB.Frame Frame3 
         Caption         =   "Else"
         Height          =   615
         Left            =   240
         TabIndex        =   13
         Top             =   840
         Width           =   3135
         Begin VB.Label Label_Other 
            Caption         =   "Tot. # of Subs having CC not 90 nor 30"
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Width           =   2895
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "CC 30"
         Height          =   615
         Left            =   1800
         TabIndex        =   11
         Top             =   240
         Width           =   1575
         Begin VB.Label Label30 
            Caption         =   "Having CC = 30"
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   240
            Width           =   1335
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "CC 90"
         Height          =   615
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   1575
         Begin VB.Label Label90 
            Caption         =   "Having CC = 90"
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   1335
         End
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Total"
      Height          =   615
      Left            =   240
      TabIndex        =   6
      Top             =   2640
      Width           =   3495
      Begin VB.Label Total 
         Caption         =   "Total Subscribers Records"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   2895
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Not SC 5"
      Height          =   615
      Left            =   3840
      TabIndex        =   4
      Top             =   2640
      Width           =   3495
      Begin VB.Label Nt_SC5 
         Caption         =   "Total Number of Subscribers not SC 5"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   2895
      End
   End
   Begin VB.TextBox File_Loc 
      Height          =   375
      Left            =   1320
      TabIndex        =   1
      Text            =   "D:\Work\Exchange\SDPs\SDP1.DUMP_subscriber_sorted.csv"
      Top             =   120
      Width           =   6015
   End
   Begin VB.CommandButton Command1 
      Caption         =   "RUN"
      Height          =   615
      Left            =   1440
      TabIndex        =   0
      Top             =   960
      Width           =   1215
   End
   Begin VB.Label Proccessing_File_Name 
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   2280
      Width           =   3615
   End
   Begin VB.Label Label_Folder 
      Caption         =   "Folder Location"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "Proccessing"
      Height          =   375
      Left            =   220
      TabIndex        =   8
      Top             =   1920
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Busy"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1200
      TabIndex        =   3
      Top             =   1850
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "File Location"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "Verify_SDPs_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Pos() As Integer

Private Function Trim_Back_Slash(ByVal Character As String)
    Dim Char As String
    Step = Len(Character)
    While Step >= 1
        Char = Mid(Character, Step, 1)
        If Char = "\" Then
            Trim_Back_Slash = Mid(Character, 1, Step)
            Step = 0
        End If
        Step = Step - 1
    Wend
End Function
Private Sub Command1_Click()
    
    Save_Last_Paths
    Label2.Visible = True
    Label5.Visible = True
    Dim File_Name, Folder_Name, File_Names(), Status() As String
    Dim Back_Slash, Step As Integer
    Step = 0
    
    If Folder <> "" And Option_Folder.Value = True Then
        If Right(Folder, 1) <> "\" Then Back_Slash = MsgBox("Do You Wish To Add a  \  After The Folder Name", vbYesNo, "BackSlash Append")
        If Back_Slash = 6 Then Folder = Folder + "\"
        Folder_Name = Trim_Back_Slash(Folder)
        File_Name = Dir(Folder)
        
        Call Save_Last_Paths
        
        Do While File_Name <> ""
            ReDim Preserve File_Names(Step)
            ReDim Preserve Status(Step)
            File_Names(Step) = File_Name
            Status(Step) = "Not Done"
            Step = Step + 1
            File_Name = Dir
        Loop
        
        Me.Width = 12675
        Call Move_Form(Main, "Left", Main.Left - 1200)
        Call Move_Form(Me, "Left", Me.Left - 1200)
        
        Files = ""
        'Done = ""
        
        For i = 0 To UBound(File_Names)
            Files = Files + vbCrLf + File_Names(i) + "    " + Status(i)
            'Done = Done + vbCrLf + Status(i)
        Next i
                 
        For i = 0 To UBound(File_Names)
            Proccessing_File_Name.Caption = File_Names(i)
            Call Verify(Folder_Name + File_Names(i))
            Status(i) = "Done"
            Files = ""
            For j = 0 To UBound(File_Names)
                'Done = Done + vbCrLf + Status(j)
                Files = Files + vbCrLf + File_Names(j) + "    " + Status(j)
            Next j
            'File_Name = Dir
        Next i
 '           O_File = FreeFile
 '           File_Name = "All_Summary.txt"
 '           Open File_Name For Output As O_File
 '               Tot_CC90 = Tot_CC90 + CC90
 '               Tot_CC30 = Tot_CC30 + CC30
 '               Tot_SC5 = Tot_SC5 + SC5
 '               Tot_Not_SC5 = Tot_Not_SC5 + Not_SC5
 '               Tot_Total = Tot_Total + Total
 '           Close O_File
    

        
        Else
            File_Name = File_Loc
            Me.Width = 7530
            Call Save_Last_Paths
            
            Call Verify(File_Name)
    End If
    


End Sub
Private Sub Verify(ByVal File_Name As String)

Dim Field(), SC As String
Dim Credit_Clearence As Double
Dim Conf_File_Loc, Record, Char As String
Dim I_File, O_File As Double
Dim Line, Step As Integer
Line = 0
Step = 0
I_File = FreeFile

On Error GoTo error_Handler_SDPs

    Open File_Name For Input As I_File
        Do Until EOF(I_File)
            'ReDim Preserve Record(Line)
            Line Input #I_File, Record
            Call Comma_Pos(Record)        'to update the fields
            
            ReDim Preserve Field(0)
            Field(0) = Mid(Record, 1, Pos(0) - 1)
            
            For Fields = 0 To UBound(Pos) - 1
                Step = Step + 1
                'x = Len(Record)
                ReDim Preserve Field(Step)
                Field(Step) = Mid(Record, Pos(Fields) + 1, Pos(Fields + 1) - Pos(Fields) - 1)
            Next Fields
                   
            Step = 1
            
            
            DoEvents
            
            For i = 0 To UBound(Field)
                Record_all = Record_all + Field(i) + ","
            Next i
            'x = Len(Record_all)
            
            
            SC = Field(18)
            If SC = "5" And Field(26) <> "" And Field(24) <> "" Then Credit_Clearence = Abs(DateDiff("d", CDate(Field(26)), CDate(Field(24))))
            
            
            
            If SC = "5" Then    'all SC5
                Select Case Credit_Clearence
                    Case 95
                        If Store_Records.Value = 1 Then Call Save_File(Mid(File_Name, 1, Len(File_Name) - 4) + "SC5_" + "90" + ".csv", Record_all)
                        CC90 = CC90 + 1
                    
                    Case 35
                        If Store_Records.Value = 1 Then Call Save_File(Mid(File_Name, 1, Len(File_Name) - 4) + "SC5_" + "30" + ".csv", Record_all)
                        CC30 = CC30 + 1
                    
                    Case Else
                        If Store_Records.Value = 1 Then Call Save_File(Mid(File_Name, 1, Len(File_Name) - 4) + "SC5_Else" + ".csv", Record_all)
                        SC5 = SC5 + 1
                    End Select
            
            Else    'not SC5
                If Store_Records.Value = 1 Then Call Save_File(Mid(File_Name, 1, Len(File_Name) - 4) + "Not_SC5" + ".csv", Record_all)
                Not_SC5 = Not_SC5 + 1
            End If

            Line = Line + 1
            ReDim Field(0)
            ReDim Pos(0)
            Record_all = ""
            Record = ""
            SC = ""
            Credit_Clearence = 0
            Line = 0
            Processed = Processed + 1
            Label2.Caption = Str(Processed)             'Proccessing
            Label_Other.Caption = Str(SC5)              'SC5 Else
            Label30.Caption = Str(CC30)                 'SC5 30
            Label90.Caption = Str(CC90)                 'SC5 90
            Total = Str(SC5 + CC30 + CC90 + Not_SC5)    '# of Records
            Nt_SC5 = Str(Not_SC5)                       'Not SC5
        
        Loop
    Close I_File

    O_File = FreeFile
    File_Name = Mid(File_Name, 1, Len(File_Name) - 4) + "_Summary.txt"
    Open File_Name For Output As O_File
        Print #O_File, "SC5"
        Print #O_File, "        Credit Clearence=90        " + Thousand(Str(CC90))
        Print #O_File, "        Credit Clearence=30        " + Thousand(Str(CC30))
        Print #O_File, "        Else                       " + Thousand(Str(SC5))
        Print #O_File, "Not SC5"
        Print #O_File, "                                   " + Thousand(Str(Not_SC5))
        Print #O_File, ""
        Print #O_File, "Total                              " + Thousand(Str(Total))
    Close O_File

error_Handler_SDPs:
    If Err.Number = 53 Then
        O_File_Name = FreeFile
        Open File_Name For Output As O_File
        Close O_File_Name
    End If
End Sub

Private Sub Save_File(ByVal File_Name As String, ByVal Record_Name As String)
    O_File = FreeFile
    Open File_Name For Append As O_File
        Print #O_File, Record_Name
    Close O_File
End Sub

Private Sub Form_Terminate()
    Call Slide(Me, Me.Left, Me.Width, False)
End Sub


Private Sub Comma_Pos(ByVal String_ As String)
    Dim Char As String
    Dim Field As Integer
    
    For i = 1 To Len(String_)
        Char = Mid(String_, i, 1)
        If Char = "," Then
            ReDim Preserve Pos(Field)
            Pos(Field) = i
            Field = Field + 1
        End If
    Next i

End Sub

Private Function Thousand(ByVal Number As String)
    Dim Char, Result, Reversed As String
    Dim Step As Integer
    Step = Len(Number)
    
    For i = 1 To Len(Number)
        
        Char = Mid(Number, Step, 1)
        Step = Step - 1
        If i = 4 Or i = 7 Or i = 10 Or i = 13 Or i = 16 Or i = 19 Or i = 22 Or i = 25 Or i = 28 Then Char = "," + Char
        Result = Result + Char
    Next i
    
    
    For i = Len(Result) To 1 Step -1
        
        Char = Mid(Result, i, 1)
        Reversed = Reversed + Char
    Next i
        
    Thousand = Reversed
    
End Function

Private Sub Form_Unload(Cancel As Integer)
    Form_Terminate
End Sub

Private Sub option_file_Click()

        If Option_File.Value = True Then
    
        File_Loc.Visible = True
        Label1.Visible = True
        Folder.Visible = False
        'Folder = ""
        Label_Folder.Visible = False
        
        Else
        File_Loc.Visible = False
        Label1.Visible = False
        Folder.Visible = True
        Label_Folder.Visible = True
    End If
End Sub

Private Sub option_folder_Click()
    If Option_Folder.Value = True Then
        
        Folder.Visible = True
        Label_Folder.Visible = True
        
        File_Loc.Visible = False
        Label1.Visible = False
        
        Else
        Folder.Visible = False
        'Folder = ""
        Label_Folder.Visible = False
        File_Loc.Visible = True
        Label1.Visible = True
    End If

End Sub
