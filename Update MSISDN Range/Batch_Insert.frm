VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Batch_Insert 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Insert MSISDN Batch"
   ClientHeight    =   4500
   ClientLeft      =   4860
   ClientTop       =   3495
   ClientWidth     =   12195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   12195
   Begin VB.ComboBox File_Path 
      Height          =   315
      Left            =   720
      TabIndex        =   4
      Top             =   120
      Width           =   10935
   End
   Begin VB.CommandButton Get_File_Path 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11640
      TabIndex        =   3
      Top             =   120
      Width           =   375
   End
   Begin MSComDlg.CommonDialog Batch_Insert_CommonDialog 
      Left            =   120
      Top             =   960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Display 
      Height          =   3375
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Top             =   960
      Width           =   11895
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   375
      Left            =   5160
      TabIndex        =   1
      Top             =   480
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Path"
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "Batch_Insert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub File_Path_Change()
    Call File_Path_LostFocus
End Sub

Private Sub File_Path_Click()
    Call File_Path_LostFocus
End Sub

Private Sub File_Path_LostFocus()
    Dim MSISDN As Integer
    Dim found_in_list As Boolean
    found_in_list = False
    
    If Trim(File_Path.Text) = "" Then Exit Sub
    
    For I = 0 To File_Path.ListCount
        If File_Path.Text = File_Path.List(I) Then found_in_list = True
        Next I
    
    If found_in_list = False Then
        File_Path.AddItem File_Path.Text, 0
        File_Path.Text = File_Path.List(0)
        Call Save_List(File_Path, "File_Names")
    End If
    
End Sub

Private Sub Form_Load()
    Call Load_List(File_Path, "File_Names")
End Sub

Private Sub Get_File_Path_Click()
    Dim Directory As String
    If File_Path = "" Then File_Path = "D:\"
    Directory = Get_Dir(File_Path.Text)
    Batch_Insert_CommonDialog.InitDir = Directory
    Batch_Insert_CommonDialog.CancelError = False
    Batch_Insert_CommonDialog.Filter = "All Files (*.*)|*.*|Text Files(*.xls)|*.xls|Text Files(*.xlsx)|*.xlsx"
    Batch_Insert_CommonDialog.FilterIndex = 3
    Batch_Insert_CommonDialog.ShowOpen
    If Batch_Insert_CommonDialog.FileName <> "" Then
        File_Path.Text = Batch_Insert_CommonDialog.FileName
    End If
End Sub
Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function
'private function ReadExcelConfiguations
Private Sub Start_Click()
    Dim AppExcel As Excel.Application
    Set AppExcel = New Excel.Application
    AppExcel.Workbooks.Open File_Path
    
    Dim Locs As Cell_Locations
    
    With Locs
        For Rows_ = 1 To 10
            For Col = 1 To 20
                Select Case AppExcel.Sheets("SDP").Cells(Rows_, Col)
                    Case "MSISDN Range From:"
                        .From.Row = Rows_
                        .From.Column = Col
                    Case "MSISDN Range to:"
                        .To.Row = Rows_
                        .To.Column = Col
                    Case "SDP"
                        .SDP.Row = Rows_
                        .SDP.Column = Col
                    Case "Service_key"
                        .ServiceKey.Row = Rows_
                        .ServiceKey.Column = Col
                    Case "Prepaid OICK/TICK"
                        If .OICK.Column = 0 Then
                            .OICK.Row = Rows_
                            .OICK.Column = Col
                        End If
                    End Select
            Next Col
        Next Rows_
    End With
    
    Dim Data_Date As String
    Dim Dot_Loc, Under_Score_Loc As Integer
    Dot_Loc = InStrRev(File_Path, ".xls")
    Under_Score_Loc = InStrRev(File_Path, "_")
    
    Data_Date = Mid(File_Path, Under_Score_Loc + 1, Dot_Loc - Under_Score_Loc - 1)
    Data_Date = Left(Data_Date, 4) + "-" + Mid(Data_Date, 5, 2) + "-" + Right(Data_Date, 2)
    
    Dim Step As Integer
    Dim MSISDN As String
    Dim ServiceClass As String
    Dim Language As String
    Dim ResponseCodeID As String
    
    
    Dim Statement As String
    Dim Conn As ADODB.Connection
    Dim Result As ADODB.Recordset
    
    Set Conn = New ADODB.Connection
    Set Result = CreateObject("ADODB.Recordset")
    
    Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=172.19.10.221;UID=root;PWD=Sultan(999;PORT=3306"
    Conn.Open
    
    Result.Open "DELETE FROM group_stats.msisdn_ranges WHERE Data_Date='" + Data_Date + "'", Conn
    
    Step = Locs.From.Row + 1
    
    With AppExcel.Sheets("SDP")
        While .Cells(Step, 1) <> ""
            Statement = BuildStatement(.Cells(Step, Locs.From.Column), _
                                        .Cells(Step, Locs.To.Column), _
                                        .Cells(Step, Locs.SDP.Column), _
                                        .Cells(Step, Locs.OICK.Column), _
                                        .Cells(Step, Locs.ServiceKey.Column), _
                                        Data_Date)
            Result.Open Statement, Conn
                        
            'If Display = "" Then Display = "Start,End,SDP_Name,OICK,Service_Key " + vbCrLf
            'Display = Display + .Cells(Step, Locs.From.Column) + "," + .Cells(Step, Locs.To.Column) + "," + .Cells(Step, Locs.SDP.Column) + "," + .Cells(Step, Locs.OICK.Column) + "," + .Cells(Step, Locs.ServiceKey.Column) + vbCrLf
            'Display.SelStart = Len(Display)
            
            Step = Step + 1
        Wend
    End With
    
    If Conn.State = 1 Then Conn.Close
    AppExcel.Workbooks.Close
    
    MsgBox "Done"
    
End Sub
Private Function BuildStatement(From As String, _
                                To_ As String, _
                                SDP As String, _
                                OICK As String, _
                                ServiceKey As String, _
                                Data_Date As String) As String
    BuildStatement = "INSERT INTO group_stats.msisdn_ranges(Start,End,SDP_Name,OICK,Data_Date,Service_Key) VALUES(" + _
                        "'" + From + "'," + _
                        "'" + To_ + "'," + _
                        "'" + SDP + "'," + _
                        "'" + OICK + "'," + _
                        "'" + Data_Date + "'," + _
                        "'" + ServiceKey + "')"
End Function
