Attribute VB_Name = "Combos"
Public Function Numbers_Only(ByRef Number_String As String) As String
    For I = 1 To Len(Number_String)
        If Mid(Number_String, I, 1) = 0 Or _
                Mid(Number_String, I, 1) = 1 Or _
                Mid(Number_String, I, 1) = 2 Or _
                Mid(Number_String, I, 1) = 3 Or _
                Mid(Number_String, I, 1) = 4 Or _
                Mid(Number_String, I, 1) = 5 Or _
                Mid(Number_String, I, 1) = 6 Or _
                Mid(Number_String, I, 1) = 7 Or _
                Mid(Number_String, I, 1) = 8 Or _
                Mid(Number_String, I, 1) = 9 Then
            Numbers_Only = Numbers_Only + Mid(Number_String, I, 1)
        End If
    Next I
End Function
Public Sub Save_List(Combo_Name As ComboBox, File_Name As String, Optional NumbersOnly As Boolean)
    
    If NumbersOnly Then Combo_Name.Text = Numbers_Only(Combo_Name.Text)
    Dim found_in_list As Boolean
    Dim Index_S As String
    found_in_list = False
    
    For I = 0 To Combo_Name.ListCount
        If Combo_Name.Text = Combo_Name.List(I) Then found_in_list = True
        Next I

    If found_in_list = False Then
        Combo_Name.AddItem Combo_Name.Text, 0
        Combo_Name.Text = Combo_Name.List(0)
    End If
    
    Dim O_File_Name As Long
    Dim Index As Integer
    O_File_Name = FreeFile
                Open App.Path + "\" + File_Name + ".txt" For Output As O_File_Name
                
                    Print #O_File_Name, Combo_Name.Text + "," + "000"
                    I = 0
                    Index = 1
                    While I <= Combo_Name.ListCount
                        
                        If Index <= 9 Then Index_S = Trim_All("00" + Str(Index))
                        If Index >= 10 And I <= 99 Then Index_S = Trim_All("0" + Str(Index))
                        If Index >= 100 Then Index_S = Trim_All(Str(Index))
                    
                        If Not (Combo_Name.List(I) = Combo_Name.Text Or Combo_Name.List(I) = "") Then
                            Print #O_File_Name, Combo_Name.List(I) + "," + Index_S
                            Index = Index + 1
                        End If
                        I = I + 1
                    Wend
                        
                Close O_File_Name

End Sub
Public Function Trim_All(ByVal To_Be_Trimmed As String)
    Dim Result, Char_ As String
    
    For I = 1 To Len(To_Be_Trimmed)
        Char_ = Mid(To_Be_Trimmed, I, 1)
        If Not (Asc(Char_) = 13 Or Asc(Char_) = 32) Then Result = Result + Char_
    Next I
    Trim_All = Result
End Function
Public Function Load_List(ByRef Combo_Name As ComboBox, File_Name As String) As Integer

    I_File_Name = FreeFile
    Dim File_List() As String
    Step = 0
    Combo_Name.Clear
    
    On Error GoTo Error_Handler_File_Name

    Open App.Path + "\" + File_Name + ".txt" For Input As I_File_Name

        Do Until EOF(I_File_Name)
            ReDim Preserve File_List(Step)
            Line Input #I_File_Name, File_List(Step)
            If File_List(Step) <> "" Then
                Step = Step + 1
            End If
        Loop
    Close I_File_Name
    
    ReDim Combo_List(UBound(File_List))
    For I = 0 To UBound(File_List)
        If File_List(I) <> "" Then Combo_Name.AddItem Mid(File_List(I), 1, Len(File_List(I)) - 4)
    Next I
            
    Combo_Name.Text = Combo_Name.List(0)
    Load_List = Err.Number

Error_Handler_File_Name:
    If Err.Number = 53 Then
        O_File_Name = FreeFile
        Open App.Path + "\" + File_Name + ".txt" For Output As O_File_Name
        Close O_File_Name
        Load_List = Err.Number
    End If
End Function
