Attribute VB_Name = "SendMail_Module"
Public Sub CDO_Mail_Small_Text(Subject As String, _
                            SendTo As String, _
                            CC As String, _
                            Body As String, _
                            Optional FilePath As String, _
                            Optional BCC As String)
    Dim iMsg As Object
    Dim iConf As Object
    Dim strbody As String
    '    Dim Flds As Variant

    Dim Conn As ADODB.Connection
    Dim RS As ADODB.Recordset
    Set Conn = New ADODB.Connection
    Set RS = New ADODB.Recordset

    Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=ActiveDB;DATABASE=group_stats;UID=Reader;PWD=Reader;PORT=3306"
    Conn.Open
    
    RS.Open "Select CNPVAS_Password from Scheduler_Mail_Conf", Conn
    
    Dim Password As String
    Password = RS.Fields("CNPVAS_Password").Value
    
    Set iMsg = CreateObject("CDO.Message")
    Set iConf = CreateObject("CDO.Configuration")

        iConf.Load -1    ' CDO Source Defaults
        Set Flds = iConf.Fields
        With Flds
            .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
            .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") _
                           = "SVLE3KA1.clickgsm.Misrfone.com"
            .Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
            .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
            .Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "CNPVAS"
            .Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Password
            .Update
        End With

 Body = "<HTML><HEAD><BODY><b>Dears; <br><br>" + Body + "</b>" + _
                    "<BR>**This an automatic mail for more informations find the attached file and don't hesitate to contact us" + _
                    "<BR>" + _
                    "To Stop Receiving This Automatic Mail, Reply With STOP" + "</BODY></HTML>"

    With iMsg
        Set .Configuration = iConf
        .To = SendTo
        .CC = CC
        .BCC = BCC
        .From = "cnp.VAS@vodafone.com"
        '.From = "CNP VAS"
        .Subject = Subject
        '.TextBody = strbody
        .HTMLBody = Body
        If FilePath <> "" Then .AddAttachment FilePath
        .Send
    End With
    
    'MsgBox "Message Sent"

End Sub




