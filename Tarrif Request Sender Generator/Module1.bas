Attribute VB_Name = "Functions"
Public CallFlow() As String
Type Locations
    FirstLocation As Integer
    LastLocation As Integer
End Type
Public Sub Convert_Format(FileName As String)
    Dim SS7FileName As Double
    Dim Temp As String
    Dim Sultan As Parser_Return
    SS7FileName = FreeFile
    Open FileName For Input As SS7FileName
    Do Until EOF(SS7FileName)
            Line Input #SS7FileName, Temp
            Sultan = Parser(Temp, Chr(10))
        Loop
    Close SS7FileName
    SS7_Paths = FreeFile
    Open App.Path + "\Temp.txt" For Output As SS7_Paths
    For i = 0 To UBound(Sultan.Ret)
        Print #SS7_Paths, Sultan.Ret(i)
    Next i
    Close SS7_Paths
    
End Sub

Public Function Sort(ByVal Array_ As Variant) As Variant
    Dim Sorted_Array(), Max As String
    Dim Max_Pos As Integer
    
    For i = 1 To UBound(Array_)
        'Getting max value and positioin
        For j = 1 To UBound(Array_)
            If Val(Max) <= Val(Array_(j)) Then
                Max = Val(Array_(j))
                Max_Pos = j
            End If
        Next j
        
        ReDim Preserve Sorted_Array(i)
        Sorted_Array(i) = Max
        Max = "0"
        Array_(Max_Pos) = 0
    Next i
    Sort = Sorted_Array
End Function
