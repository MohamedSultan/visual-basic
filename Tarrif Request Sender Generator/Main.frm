VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Main_Form 
   Caption         =   "Tarrif Request Sender Generator"
   ClientHeight    =   4245
   ClientLeft      =   5325
   ClientTop       =   3570
   ClientWidth     =   8730
   LinkTopic       =   "Form1"
   ScaleHeight     =   4245
   ScaleWidth      =   8730
   Begin VB.CommandButton Batch 
      Caption         =   "Batch Files"
      Height          =   495
      Left            =   240
      TabIndex        =   9
      Top             =   1560
      Width           =   3615
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   1680
      TabIndex        =   8
      Top             =   480
      Width           =   2295
   End
   Begin VB.DirListBox Dir1 
      Height          =   3240
      Left            =   3960
      TabIndex        =   7
      Top             =   480
      Width           =   2295
   End
   Begin VB.FileListBox File1 
      Height          =   3210
      Left            =   6360
      TabIndex        =   6
      Top             =   480
      Width           =   2295
   End
   Begin VB.TextBox OutPut 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Top             =   3840
      Width           =   7215
   End
   Begin MSComDlg.CommonDialog SetPath 
      Left            =   6240
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Open_File_Command 
      Caption         =   "Open File"
      Height          =   315
      Left            =   360
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.ComboBox Path_Combo 
      Height          =   315
      Left            =   1680
      TabIndex        =   1
      Top             =   120
      Width           =   6975
   End
   Begin VB.CommandButton Change_Prompts 
      Caption         =   "Generate File"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   960
      Width           =   3615
   End
   Begin VB.TextBox Check_Display 
      Height          =   5175
      Left            =   1440
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   5
      Top             =   2400
      Visible         =   0   'False
      Width           =   6615
   End
   Begin VB.Label Label2 
      Caption         =   "Output File Name"
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Input File Name"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   1335
   End
   Begin VB.Menu Test 
      Caption         =   "Test"
      Index           =   1
   End
   Begin VB.Menu About 
      Caption         =   "About"
      Index           =   0
   End
End
Attribute VB_Name = "Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Display(ByVal Data As String)
    Check_Display = Check_Display + vbCrLf + Data
    'Check_Display
    'Check_Display.AddItem Data
    'Check_Display.ListIndex = (Check_Display.ListCount - 1)
End Sub

Private Function Open_File(FileName As String, Optional Batch As Boolean) As Boolean
    Dim pprFileName, Step As Double
    Dim Temp As String
    'Call Convert_Format(Path_Combo.Text)
    
    'On Error GoTo Local_ErrorHandler
    'pprFileName = FreeFile
    Open_File = True
    'Open FileName For Input As pprFileName
    '    Do Until EOF(pprFileName)
    '        Step = Step + 1
    '        ReDim Preserve CallFlow(Step)
    '        Line Input #pprFileName, CallFlow(Step)
    '    Loop
    'Close pprFileName
    
    'Dim Request_S() As String
    'Open App.Path + "\Set_Tariff.xml" For Input As pprFileName
    '    Do Until EOF(pprFileName)
    '        Step = Step + 1
    '        ReDim Preserve Request_S(Step)
    '        Line Input #pprFileName, Request_S(Step)
    '    Loop
    'Close pprFileName
    
    Dim Tariff As New DOMDocument
    Dim SetTariff As New DOMDocument
    Dim RequestXML As New DOMDocument

    Dim Tariff_Child As IXMLDOMNode
    Dim RequestXML_Child As IXMLDOMNode
    
    bret = RequestXML.Load(App.Path + "\Set_Tariff.xml")
    bret = Tariff.Load(FileName)
    
    'setting the Tariff Child Node
    'For i = 0 To Tariff.childNodes.length - 1
    '    If Tariff.childNodes.Item(i).nodeName = "TariffStructure" Then _
    '        Set Tariff_Child = Tariff.childNodes.Item(i)
    'Next i
    Set Tariff_Child = Tariff.selectSingleNode("TariffStructure")
    
    'Setting the request main node
    'For i = 0 To RequestXML.childNodes.length - 1
    '    If RequestXML.childNodes.Item(i).nodeName = "Request" Then _
    '        Set RequestXML_Child = RequestXML.childNodes.Item(i)
    'Next i
    Set RequestXML_Child = RequestXML.selectSingleNode("Request")
    Call RequestXML_Child.appendChild(Tariff_Child)
    
    Call SetTariff.appendChild(RequestXML_Child)
                Dim NewFileName As String
            Dim ExtentionLocation As Integer
            
    If Not Batch Then
        If OutPut = "" Then
            NewFileName = File1.List(GetFileName(File1, FileName))
            ExtentionLocation = CharLocation(NewFileName, ".").LastLocation
            NewFileName = Left(NewFileName, ExtentionLocation - 1) + "-Prepared" + Right(NewFileName, Len(NewFileName) - ExtentionLocation + 1)
            OutPut = GetDir(FileName) + "\" + NewFileName
        End If
        SetTariff.save (OutPut)
        MsgBox "File is saved in " + OutPut, , "Finished"
    Else
        NewFileName = File1.List(GetFileName(File1, FileName))
        ExtentionLocation = CharLocation(NewFileName, ".").LastLocation
        NewFileName = Left(NewFileName, ExtentionLocation - 1) + "-Prepared" + Right(NewFileName, Len(NewFileName) - ExtentionLocation + 1)
        
        SetTariff.save (OutPut + NewFileName)
    End If
    
  '  pprFileName = FreeFile
  '  Open_File = True
  '  Open App.Path + "\Temp.xml" For Input As pprFileName
  '      Do Until EOF(pprFileName)
  '          Step = Step + 1
  '          ReDim Preserve CallFlow(Step)
  '         Line Input #pprFileName, CallFlow(Step)
  '      Loop
  '  Close pprFileName
    
  '  Dim SetTariffXML() As String
  '  ReDim Preserve SetTariffXML(Int(Step / 3300))
  '
  '  Step = 0
  '  For i = 0 To UBound(CallFlow)
  '      SetTariffXML(Int(i / 3300)) = SetTariffXML(Int(i / 3300)) + vbCrLf + CallFlow(i)
  '  Next i
  '
  '  For i = 0 To UBound(SetTariffXML)
  '      Main_Form.XML(i) = SetTariffXML(i)
  '  Next i
    

Local_ErrorHandler:
    If Err.Number = 62 Then
        Dim Format_File As Integer
        Format_File = MsgBox("PLZ reformat the File In DOS Format", vbYesNoCancel, "File Format Error")
        Select Case Format_File
            Case vbYes
                Call Convert_Format(Path_Combo.Text)
                Call Open_File(App.Path + "\Temp.txt")
            Case vbNo
                Open_File = False
            Case vbCancel
                Open_File = False
        End Select
    Else
        Open_File = False
        If Err.Number <> 0 Then
            Call MsgBox("Error: " + Err.Description, vbCritical, "Error:" + Str(Err.Number))
            End
        Else
            Open_File = True
        End If
       ' Call Err.Raise(Err.Number, Err.Source, Err.Description)
    End If
End Function
Private Sub Get_Paths()
    Dim SS7_Paths As Double
    SS7_Paths = FreeFile
    Dim Paths() As String
    Step = 0
    Path_Combo.Clear
    On Error GoTo Local_Handler
    Open App.Path + "\Paths.txt" For Input As SS7_Paths
        Do Until EOF(SS7_Paths)
            ReDim Preserve Paths(Step)
            Line Input #SS7_Paths, Paths(Step)
            Step = Step + 1
        Loop
    Close SS7_Paths
    
    For i = 0 To UBound(Paths)
        Path_Combo.AddItem Paths(i)
    Next i
    Path_Combo.ListIndex = 0
    
Local_Handler:
    If Err.Number = 53 Then
        SS7_Paths = FreeFile
        Open App.Path + "\Paths.txt" For Output As SS7_Paths
        Close SS7_Paths
    End If
End Sub

Private Sub About_Click(Index As Integer)
    MsgBox "This Program is Developed By Mohamed Sultan" + _
            vbCrLf + "Senior IN Charging" + _
            vbCrLf + "VAS, Network Development" + _
            vbCrLf + "27 Sep 2008", , "About SS7 Checker"
End Sub

Private Sub Batch_Click()
Dim Correct_Format As Boolean
    Dim Line, PromptName, NewPromptName As String
    Check_Display = ""
    Call Check_FileName(Path_Combo.Text)
    
    Dim FileNames() As String
    Dim Step As Integer
    
    ReDim Preserve FileNames(Step)
    If OutPut = "" Then OutPut = Dir1.Path
    If Right(OutPut, 1) <> "\" Then OutPut = OutPut + "\"
    FileNames(Step) = Dir(Dir1.Path + "\")
    While FileNames(Step) <> ""
        Step = Step + 1
        ReDim Preserve FileNames(Step)
        FileNames(Step) = Dir
    Wend
    For i = 0 To UBound(FileNames) - 1
        Correct_Format = Open_File(Dir1.Path + "\" + FileNames(i), True)
    Next i

    MsgBox "Files are successfully saved in " + OutPut, , "Finished"
End Sub

Private Sub Change_Prompts_Click()
    Dim Correct_Format As Boolean
    Dim Line, PromptName, NewPromptName As String
    Check_Display = ""
    Call Check_FileName(Path_Combo.Text)
    Correct_Format = Open_File(Path_Combo.Text)
End Sub
Private Function Add_Prefix(ByVal Line As String, ByVal Prompt As String) As String
    If Left(Prompt, Len(prefix)) <> prefix Then
        If Asc(Left(Prompt, 1)) = 34 And Asc(Right(Prompt, 1)) = 34 Then
            Add_Prefix = Line + Trim(Chr(34)) + Trim(prefix) + Mid(Prompt, 2, Len(Prompt) - 2) + Trim(Chr(34))
        Else
            Add_Prefix = Line + Trim(prefix) + Trim(Prompt)
        End If
        Call Display(Add_Prefix + " - Updated")
    Else
        Add_Prefix = Line + Trim(Prompt)
    End If
End Function
Private Sub SaveFileName(FileName As String)
    Dim pprFileName As Double
    pprFileName = FreeFile
    Open FileName For Output As pprFileName
        For i = 1 To UBound(CallFlow)
            Print #pprFileName, CallFlow(i)
        Next i
    Close pprFileName
End Sub
Private Sub Check_FileName(ByVal FileName As String)
'This is used to check whether the the file name exists in the combo list or not and add in case of not exist
    found = False
    Dim Paths() As String
    ReDim Preserve Paths(0)
    For i = 0 To Path_Combo.ListCount - 1
        If FileName = Path_Combo.List(i) Then
            found = True
            Paths(0) = FileName
        Else
            ReDim Preserve Paths(i + 1)
            Paths(i + 1) = Path_Combo.List(i)
        End If
    Next i
    If Not found Then
        Paths(0) = FileName
    Else
        
    End If
        Call Save_Paths(Paths)
        Call Get_Paths
End Sub
Private Sub Save_Paths(ByVal Paths As Variant)
    Dim SS7_Paths As Double
    SS7_Paths = FreeFile
    Open App.Path + "\Paths.txt" For Output As SS7_Paths
        For i = 0 To UBound(Paths)
            If Paths(i) <> "" Then Print #SS7_Paths, Paths(i)
        Next i
    Close SS7_Paths
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Dir1_Change()
File1.Path = Dir1.Path
End Sub

Private Sub Drive1_Change()
Dir1.Path = Drive1.Drive
End Sub

Private Sub File1_Click()
    Path_Combo.Text = Dir1.Path + "\" + File1.List(File1.ListIndex)
End Sub

Private Sub Form_Load()
    Call Get_Paths
End Sub
Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Open_File_Command_Click()
    Dim Directory As String
    Directory = Get_Dir(Path_Combo.Text)
    SetPath.InitDir = Directory
    SetPath.CancelError = False
    SetPath.Filter = "All Files (*.*)|*.*|Tariffs (*.xml)|*.xml|Text Files(*.txt)|*.txt"
    SetPath.FilterIndex = 2
    SetPath.ShowOpen
    If SetPath.FileName <> "" Then
        Path_Combo.Text = SetPath.FileName
        Call RollOutPath
    End If
End Sub
Private Sub RollOutPath()
    Drive1.Drive = GetDrive(Path_Combo.Text)
    Dir1.Path = GetDir(Path_Combo.Text)
    File1.Path = Dir1.Path
    File1.ListIndex = GetFileName(File1, Path_Combo.Text)
End Sub
Private Function CharLocation(String_ As String, Character As String) As Locations
    Dim BackSlash As Integer
    BackSlash = InStr(1, String_, Character)
    CharLocation.FirstLocation = BackSlash
    While BackSlash <> 0
        CharLocation.LastLocation = BackSlash
        BackSlash = InStr(BackSlash + 1, String_, "\")
    Wend
End Function
Private Function GetFileName(Files As FileListBox, Path As String)
    Dim FileName As String

    FileName = Mid(Path, CharLocation(Path, "\").LastLocation + 1, Len(Path))
    
    For i = 0 To Files.ListCount - 1
        If Files.List(i) = FileName Then GetFileName = i
    Next i
End Function
Private Function GetDrive(Path As String)
    GetDrive = Left(Path, InStr(1, Path, "\"))
End Function
Private Function GetDir(Path As String)
    Dim FirstBackSlash, BackSlash, LastBackSlash As Integer
    BackSlash = InStr(1, Path, "\")
    FirstBackSlash = BackSlash
    While BackSlash <> 0
        LastBackSlash = BackSlash
        BackSlash = InStr(BackSlash + 1, Path, "\")
    Wend
    GetDir = Mid(Path, 1, LastBackSlash)
End Function
Private Sub Path_Combo_Change()
    Call RollOutPath
End Sub

Private Sub Path_Combo_Click()
    Call RollOutPath
End Sub

Private Sub Path_Combo_KeyPress(KeyAscii As Integer)
    'On Error Resume Next
    If KeyAscii = 13 Then Call Change_Prompts_Click
End Sub

Private Sub Text1_Change()

End Sub
