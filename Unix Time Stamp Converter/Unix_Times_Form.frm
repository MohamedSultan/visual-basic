VERSION 5.00
Begin VB.Form Unix_Times_Form 
   Caption         =   "Unix Time Converter"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton ToUnixTime_Command 
      Caption         =   "To Unix Time"
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   1440
      Width           =   1455
   End
   Begin VB.TextBox DateTime 
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Text            =   "Date Time"
      Top             =   1440
      Width           =   2415
   End
   Begin VB.TextBox UnixTime 
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Text            =   "Unix Time"
      Top             =   720
      Width           =   2415
   End
   Begin VB.CommandButton ToDateTime_Command 
      Caption         =   "To Date Time"
      Height          =   375
      Left            =   3000
      TabIndex        =   0
      Top             =   720
      Width           =   1455
   End
End
Attribute VB_Name = "Unix_Times_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command2_Click()
    
End Sub

Private Sub ToDateTime_Command_Click()
    DateTime = FromUnixTime(UnixTime)
End Sub
Private Sub DateTime_Click()
    DateTime = ""
End Sub

Private Sub ToUnixTime_Command_Click()
    UnixTime = ToUnixTime(DateTime)
End Sub

Private Sub UnixTime_Click()
    UnixTime = ""
End Sub
