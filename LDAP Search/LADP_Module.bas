Attribute VB_Name = "LADP_Module"
Type Authentication_Return
    Authentication_String As String
    Authentication_Status As Boolean
End Type
Public All_Emplyees() As Employees
Private Sub GetEmpData()
    Dim Conn As ADODB.Connection
    Dim RS As ADODB.Recordset
    
    Set Conn = New ADODB.Connection
    Set RS = New ADODB.Recordset
    
    Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=172.19.10.221;DATABASE=group_stats;UID=Reader;PWD=Reader;PORT=3306"
    If RS.State = 1 Then RS.Close
    RS.Open "select Employee_ID from BCP.Employees", Conn
    Dim Step As Integer
    
    While Not RS.EOF
        ReDim Preserve All_Emplyees(Step)
        All_Emplyees(Step).StuffID = RS.Fields("Employee_ID")
        RS.MoveNext
        Step = Step + 1
    Wend
    
End Sub

Public Function Authenticate_UserName_Password(ByVal UserName As String, _
                                                ByVal Password As String, _
                                                Optional Test As Boolean) As Authentication_Return
                                                
Call GetEmpData
    'If Check_Connectivity <> 0 Then
        Dim adoCommand, adoConnection, strBase, strFilter, strAttributes
        
        Dim objRootDSE, strDNSDomain, strQuery, adoRecordset, strName, strCN, strLastLogin
        
        
        
        ' Setup ADO objects.
        
        Set adoCommand = CreateObject("ADODB.Command")
        Set adoConnection = CreateObject("ADODB.Connection")
        adoConnection.Provider = "ADsDSOObject"
        'adoConnection.open "Active Directory Provider"
        adoConnection.Open "Active Directory Provider" ', "msultan", "MySara111"
        'adoConnection.open "ADs Provider", "uid=msultan,ou=" & m_OrgUnit & ",o=VF-EG", m_Password
        adoCommand.ActiveConnection = adoConnection
        
        
    
        ' Search entire Active Directory domain.
        If Test Then
            Set objRootDSE = GetObject("LDAP://RootDSE")
        
            strDNSDomain = objRootDSE.Get("defaultNamingContext")
            strDNSDomain = "DC=miislab,DC=root,DC=miisroot,DC=com"
            strBase = "<LDAP://" & strDNSDomain & ">"
            'objRootDSE = Nothing
        Else    'Live
            On Error GoTo Connection_error
            Set objRootDSE = GetObject("LDAP://RootDSE")
            
            strDNSDomain = objRootDSE.Get("defaultNamingContext")
            'strDNSDomain = "miislab.root.miisroot.com"
            strBase = "<LDAP://" & strDNSDomain & ">"
            'objRootDSE = Nothing
        End If
        
        
        ' Filter on user objects.
        'KNOWN OBJECTS
        '=======================================
        'ObjectClass=user
        'ObjectCategory=person
        'CN
        
        'strFilter = "(&(objectCategory=person)(objectClass=user)(cn=Mohamed* ))"
        '
        'strFilter = "(&(cn=Mohamed*))"  'Success
        ''strFilter = "(&(Last=Sultan)(Alias=Msultan)(cn=*sultan*))"
        'strFilter = "(&(cn=Mohamed Sultan))"  'Success
        'strFilter = "(&(cn=*Sultan))"  'Success
        
        strFilter = "(&(sAMAccountName=" & UserName & "))" 'Success
        strFilter = "(&(extensionAttribute1=" & UserName & "))" 'Success
        
        'strFilter = "(&(sAMAccountName=" & UserName & "))" 'Success
        
        'EXAMPLES OF FILTER WHICH IS REALLY JUST A QUERY
        '================================================= ==========
        '"(&(objectCategory=person)(objectClass=user))"
        '"(&(objectCategory=person)(objectClass=user)(cn=Joe*) )"
        '"(objectCategory=computer)"
        '================================================= ==========
        
        
        ' Comma delimited list of attribute values to retrieve.
        
        'strAttributes = "AdsPath,badPasswordTime,badPwdCount,displayName,sAMAccountName,cn,distinguishedName, mail, lastlogon,objectCategory,objectClass"
        
        strAttributes = "AdsPath,displayName,cn, mail, Mobile, extensionAttribute1, sAMAccountName"
        'ATTRIBUTES
        '================================================= ==========
        'SAMAccountName CN DistinguishedName
        'mail company givenName sn
        'ADsPath name sAMAccountName telephoneNumber
        
        
        ' Construct the LDAP syntax query.
        strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"
        
        adoCommand.CommandText = strQuery
        adoCommand.Properties("Page Size") = 100
        adoCommand.Properties("Timeout") = 30
        adoCommand.Properties("Cache Results") = False
        
        ' Run the query.
        On Error GoTo Locked
        Set adoRecordset = adoCommand.Execute
        'Dim cn As iads
    
        
        If adoRecordset.EOF And adoRecordset.BOF Then      'NotFound
            'Debug.Print Now() & " - User Name " & strUsername & " not exists in the directory."
            'Call BadUserName_Password
            Authenticate_UserName_Password.Authentication_String = "Bad User Name/Password"
            Exit Function
        Else                                                                                'Found
            'Debug.Print Now() & " - User Name " & strUsername & " exists in the directory."
            strUserADSPath = adoRecordset.Fields("ADSPATH").Value
            blnUserExists = True
            Dim DisplayUserName As String
            DisplayUserName = adoRecordset.Fields("displayName").Value
        End If
        adoRecordset.Close
        Set adoRecordset = Nothing
        adoConnection.Close
        Set adoConnection = Nothing
    
    
    End
    
        If Not blnUserExists Then Exit Function
    
        Set oUser = GetObject(strUserADSPath)
        'Debug.Print "Account Disabled = " & oUser.AccountDisabled
        
        ' FYI
        If disabled = True Then Authenticate_UserName_Password.Authentication_String = "Bad User Name/Password" 'Call BadUserName_Password
        
        On Error GoTo Err_Hnd
        
        Set oDSObj = GetObject("LDAP:")
        Set oAuth = oDSObj.OpenDSObject("LDAP://" & strDNSDomain, UserName, Password, ADS_SECURE_AUTHENTICATION)
        
        If Not oAuth Is Nothing Then
            'MsgBox "Authentication Success", vbInformation
            Authenticate_UserName_Password.Authentication_String = DisplayUserName
            Authenticate_UserName_Password.Authentication_Status = True
            Set oAuth = Nothing
        End If
        
        Exit Function
 '   Else
        'MsgBox "Sorry, There Is No Active Connection", , "Connection Error"
 '       Authenticate_UserName_Password.Authentication_String = "Sorry, There Is No Active Connection"
 '       Exit Function
  '  End If
    
Connection_error:
        If Err.Number = -2147023541 Then
            'MsgBox "Sorry, Could Not Contact Active Directory", , "Connection Error"
            Authenticate_UserName_Password.Authentication_String = "Sorry, Could Not Contact Active Directory"
            Exit Function
        End If
    
Locked:
        If Err.Number = -2147217865 Then
            'MsgBox "Sorry, User Name Is Locked", , "Login"
            Authenticate_UserName_Password.Authentication_String = "Sorry, User Name Is Locked"
            Exit Function
        End If
    
    
Err_Hnd:
            If Err.Number = -2147023570 Then
                'Call BadUserName_Password
                Authenticate_UserName_Password.Authentication_String = "Bad User Name/Password"
                Exit Function
            Else
                MsgBox Err.Description, vbCritical, Err.Number
                Exit Function
            End If
            ' Clean up.
            If Not (adoRecordset Is Nothing) Then adoRecordset.Close
            If Not (adoConnection Is Nothing) Then adoRecordset.Close
        
End Function

Private Sub BadUserName_Password()
    'MsgBox "Bad User Name/Password"
    'Authenticate_UserName_Password.Authentication_String = "Bad User Name/Password"
End Sub


