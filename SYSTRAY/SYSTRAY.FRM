VERSION 4.00
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   Caption         =   "Systray sample from BlackBeltVB.com"
   ClientHeight    =   3195
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   5835
   Height          =   3600
   Left            =   1080
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   5835
   Top             =   1170
   Width           =   5955
   Begin VB.ListBox List1 
      Height          =   1815
      Left            =   180
      TabIndex        =   3
      Top             =   1320
      Width           =   5475
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Delete"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Add Icon to System Tray"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   $"systray.frx":0000
      Height          =   1155
      Left            =   2520
      TabIndex        =   2
      Top             =   60
      Width           =   3135
   End
End
Attribute VB_Name = "Form1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

' Systray sample from BlackBeltVB.com
' http://blackbeltvb.com
'
' Written by Matt Hart
' Copyright 1999 by Matt Hart
' 
' This software is FREEWARE. You may use it as you see fit for 
' your own projects but you may not re-sell the original or the 
' source code. Do not copy this sample to a collection, such as
' a CD-ROM archive. You may link directly to the original sample
' using "http://blackbeltvb.com/systray.htm"
' 
' No warranty express or implied, is given as to the use of this
' program. Use at your own risk.
'
' The uCallbackMessage parameter is usually a user-defined
' message.  However, we can also use a message that VB
' automatically handles as an Event - the MouseMove message.
' The X parameter of this event contains the action.
'
' This method came from a Tip in VBPJ. http://www.windx.com
'
' Updated to show how to forcefully activate a window.
' Win98 doesn't automatically set the focus to the new
' window, but has an annoying tendancy to "blink" the
' task bar caption and window. The hProcess returned by
' GetWindowThreadProcessID (not the thread ID) can be
' used with AppActivate. Note that you can also use the
' Caption of the form you want to activate, but if you
' have more than one window with the same caption, you
' cannot be guaranteed of activating the correct one.

Private Type NOTIFYICONDATA
    cbSize As Long
    hwnd As Long
    uID As Long
    uFlags As Long
    uCallbackMessage As Long
    hIcon As Long
    szTip As String * 64
End Type
   
Const NIM_ADD = 0
Const NIM_MODIFY = 1
Const NIM_DELETE = 2
Const NIF_MESSAGE = 1
Const NIF_ICON = 2
Const NIF_TIP = 4

Const WM_MOUSEMOVE = &H200
Const WM_LBUTTONDOWN = &H201
Const WM_LBUTTONUP = &H202
Const WM_LBUTTONDBLCLK = &H203
Const WM_RBUTTONDOWN = &H204
Const WM_RBUTTONUP = &H205
Const WM_RBUTTONDBLCLK = &H206
Const WM_MBUTTONDOWN = &H207
Const WM_MBUTTONUP = &H208
Const WM_MBUTTONDBLCLK = &H209
   
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Private Declare Function Shell_NotifyIconA Lib "SHELL32" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Integer

Private Function setNOTIFYICONDATA(hwnd As Long, ID As Long, Flags As Long, CallbackMessage As Long, Icon As Long, Tip As String) As NOTIFYICONDATA
    Dim nidTemp As NOTIFYICONDATA

    nidTemp.cbSize = Len(nidTemp)
    nidTemp.hwnd = hwnd
    nidTemp.uID = ID
    nidTemp.uFlags = Flags
    nidTemp.uCallbackMessage = CallbackMessage
    nidTemp.hIcon = Icon
    nidTemp.szTip = Tip & Chr$(0)

    setNOTIFYICONDATA = nidTemp
End Function

Private Sub Command1_Click()
    'Add an icon.  This procedure uses the icon specified in
    'the Icon property of Form1. This can be modified as desired.

    Dim i As Integer
    Dim s As String
    Dim nid As NOTIFYICONDATA

    s = "ToolTipText Here!"
    nid = setNOTIFYICONDATA(Form1.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Form1.Icon, s)
    i = Shell_NotifyIconA(NIM_ADD, nid)
    
    WindowState = vbMinimized
    Visible = False
End Sub
   
Private Sub Command3_Click()
    'Delete an existing icon.

    Dim i As Integer
    Dim nid As NOTIFYICONDATA

    nid = setNOTIFYICONDATA(Form1.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, vbNull, Form1.Icon, "")

    i = Shell_NotifyIconA(NIM_DELETE, nid)
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Not Visible Then
        Select Case x \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Command3_Click
End Sub
