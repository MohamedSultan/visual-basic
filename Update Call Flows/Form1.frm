VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "                                                           ---     Update SC 2   ---"
   ClientHeight    =   2835
   ClientLeft      =   5550
   ClientTop       =   4440
   ClientWidth     =   9675
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2835
   ScaleWidth      =   9675
   Begin VB.TextBox OutPut_TextBox 
      Height          =   375
      Left            =   1080
      TabIndex        =   4
      Text            =   "D:\Work\Exchange\Sultan.ppr"
      Top             =   1320
      Width           =   8415
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   1920
      Width           =   8985
      _ExtentX        =   15849
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.TextBox CHK 
      Height          =   375
      Left            =   1080
      TabIndex        =   2
      Text            =   "xml.xmlrpc.Ericxmlrpcv1_0.GetAccountDetailsTResponse.GetAccountDetailsTResponse.serviceClassCurrent == 00"
      Top             =   840
      Width           =   8415
   End
   Begin VB.TextBox Input_Text_Box 
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Text            =   "D:\Work\Exchange\868_V16_Doaa.ppr"
      Top             =   360
      Width           =   8415
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   0
      Top             =   2280
      Width           =   2175
   End
   Begin VB.Label Label4 
      Caption         =   "Chk Pattern"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Output File"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Input File"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

' This program is used to update the ppr file got from Nortel Call Flow
' It searches on the pattern in CHK textbox and and changes the whole variable to itselef & its value + 1000
'
' This program is developped by Mohamed Sultan, IN Planning Engineer, on 24th OCT 2007


Dim Start_, Length_, Line2_Part2_Loc As Integer
Dim At_End As Boolean

Private Sub Command1_Click()
    Dim Line, Line2, Line2_Part1, Line2_Part2 As String
    Dim Last_Loc As Integer
    
    Stats.Visible = True
    Command1.Enabled = False
    
    O_Sultan = FreeFile
    Open OutPut_TextBox For Output As O_Sultan
    Close O_Sultan
    
    I_Sultan = FreeFile
    Open Input_Text_Box For Input As I_Sultan
        Do Until EOF(I_Sultan)
        Step = Step + 1
        Line Input #I_Sultan, Line
        Loop
    Close I_Sultan
    
    Stats.Occurence = 0
    Stats.Lines = 0
    Stats.Total = 0
    
    ProgressBar1.Value = 0
    ProgressBar1.Max = Step
    
    I_Sultan = FreeFile
    Open Input_Text_Box For Input As I_Sultan
        Do Until EOF(I_Sultan)
            Line Input #I_Sultan, Line
            ProgressBar1.Value = ProgressBar1.Value + 1
            Command1.Caption = "----- " + Str(Int(ProgressBar1.Value / Step * 100)) + " %" + " -----"
            
            Line2 = Update_SC(Line, CHK)
            
            DoEvents: DoEvents: DoEvents: DoEvents
            
            Last_Loc = 0
 '           If Line <> Line2 Then
                While Not At_End
                    Line2_Part1 = Mid(Line2, 1, Line2_Part2_Loc + Last_Loc)
                    Last_Loc = Line2_Part2_Loc + Last_Loc
                    Line2_Part2 = Update_SC(Mid(Line2, Last_Loc + 1, Len(Line2) - Last_Loc), CHK)
                    If Line2_Part2 <> Mid(Line2, Line2_Part2_Loc + 1, Len(Line2) - Line2_Part2_Loc) Then _
                                                                            Stats.Occurence = Stats.Occurence + 1
                    DoEvents: DoEvents: DoEvents: DoEvents
                    Line2 = Line2_Part1 + Line2_Part2
                Wend
            

            
            
                O_Sultan = FreeFile
                Open OutPut_TextBox For Append As O_Sultan
'                    Print #O_Sultan, Line
                    Print #O_Sultan, Line2
'                    For j = 1 To Len(Line2)
'                        Sult = Sult + "-"
'                    Next j
'                    Print #O_Sultan, Sult
'                    Sult = ""
                Close O_Sultan
                DoEvents: DoEvents: DoEvents: DoEvents
 '           End If
                Stats.Total = Val(Stats.Occurence) + Val(Stats.Lines)
        Loop
    Close I_Sultan
    'MsgBox Line
    
    Command1.Enabled = True
    Command1.Caption = "Start"
    Stats.Visible = True
    
    Me.SetFocus
    Stats.SetFocus
    
    MsgBox "Done"
                
End Sub
Private Function Update_SC(ByVal Line As String, ByVal String_To_Compare As String) As String

    Dim Service_Class, Service_Class2, CHK_Pattern, New_CHK_Pattern As String
    Dim Equal_Loc As Integer
    
    At_End = True
    Line2_Part2_Loc = 0
    Start_ = InStr(Line, String_To_Compare)
    
            'If Equals_(String_To_Compare, Line, True) <> 0 Then
            If Start_ <> 0 Then
                
                Stats.Lines = Stats.Lines + 1

                Length_ = Len(String_To_Compare) + 5
                    
                If Right(Mid(Line, Start_, Length_), 1) = "{" Then        ' ends with (")
                    Line2_Part2_Loc = Len(Mid(Line, 1, Length_ + Start_))
                    At_End = False
                End If
                
                CHK_Pattern = Mid(Line, Start_, Length_)
'                Equal_Loc = SPACE_LOC(CHK_Pattern, "==")
                Equal_Loc = InStr(CHK_Pattern, "==")
                Service_Class = Mid(CHK_Pattern, Equal_Loc + 3, 5)
                'Service_Class2 = Mid(CHK_Pattern, Len(CHK_Pattern) - 6 + 1, 5)
                
                'If Service_Class = Service_Class2 Then
                Service_Class2 = Zero_Ext(Trim(Str(Val(Service_Class) + 1000)), 5)
                New_CHK_Pattern = Left(CHK_Pattern, Len(CHK_Pattern) - 1) + " {Or} " + Mid(CHK_Pattern, 1, Equal_Loc + 3 - 1) + Service_Class2 + Mid(CHK_Pattern, Equal_Loc - 1 + 6 + 3, Len(CHK_Pattern))
                
                'If At_End = False Then 'Not At End
                '    Update_SC = Mid(Line, 1, Start_ - 2) + New_CHK_Pattern + Mid(Line, Start_ + Length_, Len(Line))
                '    Else
                        Update_SC = Mid(Line, 1, Start_ - 1) + New_CHK_Pattern + Mid(Line, Start_ + Length_, Len(Line))
                'End If
            Else
                Update_SC = Line
            End If
            
            If InStr(Update_SC, "  {") <> 0 Then
                Update_SC = Replace(Update_SC, "  {", " {")
            End If
End Function

'Private Function Equals_(ByVal Small_String As String, ByVal Big_String, Update_Locations As Boolean) As Integer
'
'
'    Start_ = InStr(Big_String, Small_String)
'    Equals_ = 100
'    Length_ = Len(Small_String) + 5
'    Exit Function
'
'
'
'    Equals_ = 0
'
'    If Big_String Like "{" And Big_String Like "}" Then
'        X = 1
'        Exit Function
'    End If
'
'    For i = 1 To Len(Big_String)
'        If Mid(Big_String, i, Len(Small_String)) = Small_String Then
'            Equals_ = i
'            If Update_Locations Then
'                Start_ = i
'                Length_ = Len(Small_String) + 5
'            End If
'            'Length_ = SPACE_LOC(Mid(Big_String, i, Len(Big_String) - i), " ") + Start_
'            Exit Function
'        End If
'    Next i
'
'End Function

Private Function Zero_Ext(ByVal Character As String, ByVal Length As Integer)
    Dim Temp As String
    If Len(Character) < Length Then
        For i = Len(Character) To Length - 1
            Temp = Temp + "0"
        Next i
    End If
    Zero_Ext = Temp + Character
End Function

Private Sub Form_Terminate()
    Form_Unload (0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

