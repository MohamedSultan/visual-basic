VERSION 5.00
Begin VB.Form Stats 
   BorderStyle     =   0  'None
   ClientHeight    =   1260
   ClientLeft      =   5505
   ClientTop       =   2505
   ClientWidth     =   2910
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1260
   ScaleWidth      =   2910
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   0
      TabIndex        =   0
      Top             =   -90
      Width           =   2895
      Begin VB.Timer Timer1 
         Interval        =   500
         Left            =   720
         Top             =   600
      End
      Begin VB.Label Occurence 
         Height          =   255
         Left            =   2280
         TabIndex        =   6
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label5 
         Caption         =   "Number Of Lines: "
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label7 
         Caption         =   "Number Of SC Occurence: "
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Lines 
         Height          =   255
         Left            =   2280
         TabIndex        =   3
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Total 
         Height          =   255
         Left            =   2280
         TabIndex        =   2
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label8 
         Caption         =   "Total: "
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   960
         Width           =   1455
      End
   End
End
Attribute VB_Name = "Stats"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Stats.Top = Form1.Top - Stats.Height
    Timer1.Enabled = True
End Sub

Private Sub Timer1_Timer()
    Stats.Top = Form1.Top - Stats.Height
    Stats.Left = Form1.Left
End Sub
