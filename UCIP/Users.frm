VERSION 5.00
Begin VB.Form Users 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "SULSAT - Login"
   ClientHeight    =   2205
   ClientLeft      =   7425
   ClientTop       =   6180
   ClientWidth     =   6480
   BeginProperty Font 
      Name            =   "Georgia"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   6480
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox Level_Of_Security 
      Height          =   390
      Left            =   240
      TabIndex        =   8
      Text            =   "Level Of Security"
      Top             =   1680
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton GetPassword 
      Caption         =   "Login"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   4
      Top             =   1560
      Width           =   1695
   End
   Begin VB.TextBox Password 
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   1440
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   960
      Width           =   2055
   End
   Begin VB.TextBox UserName 
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Top             =   360
      Width           =   2055
   End
   Begin VB.Label Both_Comments 
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   3600
      TabIndex        =   7
      Top             =   600
      Width           =   2415
   End
   Begin VB.Label UserName_Comment 
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   975
      Left            =   3600
      TabIndex        =   6
      Top             =   360
      Width           =   2655
   End
   Begin VB.Label Password_comment 
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   975
      Left            =   3600
      TabIndex        =   5
      Top             =   360
      Width           =   2655
   End
   Begin VB.Label Label2 
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "User Name"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   1455
   End
End
Attribute VB_Name = "Users"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim LoggedOut As Boolean
Public Function Validate(UserName As TextBox, _
                            UserName_Comment As Label, _
                            Password_comment As Label, _
                            Both_Comments As Label, _
                            Optional New_ As Boolean) As Boolean
    Dim Pass_Size As Integer
    Pass_Size = 6
    
    Validate = True
    Both_Comments = ""
    Password_comment = ""
    UserName_Comment = ""
    
    'Dim Capital, Special_Chr, Small As Boolean
    'If New_ Then
    '    Dim i As Integer
    '    'checking password contains capital
    '    For i = 1 To Len(Password)
    '        If Asc(Mid(Password, i, 1)) >= 65 And Asc(Mid(Password, i, 1)) <= 90 Then Capital = True
    '        If Asc(Mid(Password, i, 1)) >= 97 And Asc(Mid(Password, i, 1)) <= 122 Then Small = True
    '        If Not (Asc(Mid(Password, i, 1)) >= 97 And Asc(Mid(Password, i, 1)) <= 122) And _
    '            Not (Asc(Mid(Password, i, 1)) >= 65 And Asc(Mid(Password, i, 1)) <= 90) Then Special_Chr = True
    '    Next i
    '    If Not (Capital And Special_Chr And Small) Then
    '        'Password_comment = "Poor Password, Please Enter Password That Contains " + vbCrLf + "Capital Characters, Small Characters and Special Characters"
    '        Both_Comments = "Invalid User Name/Password"
    '        'Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, True, True, False)
    '        Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
    '        Validate = False
    '    End If
    'End If
        
    'checking empty user name
    If Trim(UserName) = "" Then
        'UserName_Comment = "Please Enter User Name"
        Both_Comments = "Invalid User Name/Password"
        Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
        'Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, True, False, True)
        Validate = False
        Exit Function
    End If
    ''checking empty password
    'If Trim(Password) = "" Then
    '    'Password_comment = "Please Enter Password"
    '    'Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, True, True, False)
    '    Both_Comments = "Invalid User Name/Password"
    '    Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
    '    Validate = False
    'End If
    ''checking PAssword Length
    'If New_ And Len(Password) < Pass_Size Then
    '    'Password_comment = "Invalid Password, Please Enter at least " & Pass_Size & " characters"
    '    'UserName_Comment.Visible = False
    '    'Both_Comments.Visible = False
    '    Both_Comments = "Invalid User Name/Password"
    '    Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
    '    Validate = False
    'End If
    
    'User Name Validation
    If Not ((Asc(Mid(UserName, 1, 1)) >= 65 And Asc(Mid(UserName, 1, 1)) <= 90) Or _
        (Asc(Mid(UserName, 1, 1)) >= 97 And Asc(Mid(UserName, 1, 1)) <= 122)) Then
            'UserName_Comment = "Invalid User Name, Please Enter User Name that Starts With At Least One Character"
            'Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, True, False, True)
            'Password_comment.Visible = False
            'Both_Comments.Visible = False
            Both_Comments = "Invalid User Name/Password"
            Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
            Validate = False
            Exit Function
    End If
    
    If Len(UserName) < 2 Then
           ' UserName_Comment = "Short User Name, Please Enter User Name With Length At Least 2 Characters"
           ' Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, True, False, True)
           ' Password_comment.Visible = False
           ' Both_Comments.Visible = False
            Both_Comments = "Invalid User Name/Password"
            Call LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
            Validate = False
    End If

End Function
Public Sub LabelsHiding(Both_Comments As Label, _
                            UserName_Comment As Label, _
                            Password_comment As Label, _
                            Optional Hide_Both As Boolean, _
                            Optional Hide_UserName_Comment As Boolean, _
                            Optional Hide_Password_Comment As Boolean)
    If Hide_Both Then
        Both_Comments.Visible = False
    Else
        Both_Comments.Visible = True
    End If
    
    If Hide_UserName_Comment Then
        UserName_Comment.Visible = False
    Else
        UserName_Comment.Visible = True
    End If
    
    If Hide_Password_Comment Then
        Password_comment.Visible = False
    Else
        Password_comment.Visible = True
    End If
End Sub


Private Sub Form_Load()
    Me.Left = Screen.Width / 2 - Me.Width / 2
    Me.Top = Screen.Height / 2 - Me.Height / 2
    Set Conn = New ADODB.Connection
End Sub

Private Sub Form_Terminate()
    If Conn.State = 1 Then Conn.Close
    End
End Sub

Private Sub GetPassword_Click()
    Dim Test As Boolean
    'Test = True
    If Test Then
        Users.Level_Of_Security = "CS5"
        Users.UserName = "Test User"
    End If
    
    On Error GoTo SystemHandler
        Dim Login, Locked As Boolean
        If Not Test Then Login = DB_Users("login", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments, , , Me.Password.Text)
        Wait 500
        If Login Or Test Then
            Locked = DB_Users("Locked", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments, Users.Level_Of_Security)
            Wait 500
            If Locked Then
                Users.Visible = False
                Call Add_Transaction("Locked", "DB Locked")
                Conn.Close
                MsgBox "Sorry you can't proceed." + vbCrLf + "You are not Authorized To Use SulSat" + Chr(174), , "Blocked"   'FontSize
                End
            End If
        
            Call Add_Transaction("login", "Logged In")
            
            CurrentUser.SecurityLevel = Users.Level_Of_Security
            Call SulSAT.User_Security_Level
            
            If LoggedOut Then
                Call SulSAT.Form_Load
            Else
                LoggedOut = True
            End If

            'Call Slide(SulSAT, SulSAT.Left, SulSAT.Width, True, 2)
            SulSAT.Show
            
            Users.Visible = False
        End If
'    End If
SystemHandler:
    If Err.Number = 0 Or Err.Number = 20 Then
        Resume Next
    Else
        If Conn.State = 1 Then Conn.Close
        MsgBox Err.Description
        End
    End If
End Sub

Private Sub Password_Change()
    Call Password_Click
    Call UserName_Change
End Sub

Private Sub Password_Click()
    Password_comment = ""
    Both_Comments = ""
End Sub

Private Sub Password_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call GetPassword_Click
End Sub

Private Sub UserName_Change()
    UserName_Comment = ""
    Both_Comments = ""
End Sub

Private Sub UserName_Click()
    Call UserName_Change
    Call Password_Click
End Sub

Private Sub UserName_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call GetPassword_Click
End Sub
