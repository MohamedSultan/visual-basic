Attribute VB_Name = "Used_Functions"
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Const CB_SHOWDROPDOWN = &H14F
Public PSTR As String
Public Declare Function GetTickCount Lib "kernel32" () As Long
Public NumberOfDedicatedAccounts As Integer
Public NumberOfAccumulators As Integer

Type Form_Dimensions_Type
    Height As Integer
    Width As Integer
    Sliding As Boolean
End Type
Type Transactions_Type
    AccumulatorEnquiry As String
    Adjustment As String
    BalanceEnquiry As String
    GetAccountDetails As String
    UpdateServiceClass As String
    UpdateAccountDetails As String
End Type
Public SulSat_ConfigurationData As SulSatConf_Type
Public SulSat_Dimensions As Form_Dimensions_Type
Public Transactions As Transactions_Type

Public Sub Roll_Out_Msisdns(ByRef MSISDN As String)
    'Main.MSISDNS = MSISDN
    UCIP.Combo_MSISDNs = MSISDN
    SulSAT.MSISDNs_List = MSISDN
End Sub
Public Function Trim_All(ByVal To_Be_Trimmed As String)
    Dim Result, Char_ As String
    
    For i = 1 To Len(To_Be_Trimmed)
        Char_ = Mid(To_Be_Trimmed, i, 1)
        If Not (Asc(Char_) = 13 Or Asc(Char_) = 32) Then Result = Result + Char_
    Next i
    Trim_All = Result
End Function
Public Sub Wait(ByVal dblMilliseconds As Double)
    Dim dblStart As Double
    Dim dblEnd As Double
    Dim dblTickCount As Double
    
    dblTickCount = GetTickCount()
    dblStart = GetTickCount()
    dblEnd = GetTickCount + dblMilliseconds
    
    Do
    DoEvents
    dblTickCount = GetTickCount()
    Loop Until dblTickCount > dblEnd Or dblTickCount < dblStart
End Sub
Public Function Numbers_Only(ByRef Number_String As String) As String
    For i = 1 To Len(Number_String)
        If Mid(Number_String, i, 1) = 0 Or _
                Mid(Number_String, i, 1) = 1 Or _
                Mid(Number_String, i, 1) = 2 Or _
                Mid(Number_String, i, 1) = 3 Or _
                Mid(Number_String, i, 1) = 4 Or _
                Mid(Number_String, i, 1) = 5 Or _
                Mid(Number_String, i, 1) = 6 Or _
                Mid(Number_String, i, 1) = 7 Or _
                Mid(Number_String, i, 1) = 8 Or _
                Mid(Number_String, i, 1) = 9 Then
            Numbers_Only = Numbers_Only + Mid(Number_String, i, 1)
        End If
    Next i
End Function


