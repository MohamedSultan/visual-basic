Attribute VB_Name = "Temp"
Public Function GetCodes() As CodesType()
    Dim Step As Integer
    Dim Statement As String
    Dim Result As ADODB.Recordset
    'Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    Call Users_DB_Open '(Conn)
    
    Statement = "SELECT TX_Code.Description  TX_Code_Desc, TX_Code.Value  TX_Code_Value, TX_Type.Description  TX_Type_Desc, TX_Type.Value  TX_Type_Value " + _
                "FROM TransactionCode  TX_Code, TransactionType  TX_Type, TransactionLink  TX_Link " + _
                "Where TX_Type.TransactionTypeID = TX_Link.TransactionTypeID And TX_Code.TransactionCodeID = TX_Link.TransactionCodeID " + _
                "ORDER BY TX_Type.Description"
                
    ' Execute the statement.
    Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    'getting all Trasaction Links
    Dim AllCodes() As CodesType
    While Not Result.EOF
        ReDim Preserve AllCodes(Step)
        AllCodes(Step).Transaction_Type.Description = Result.fields("TX_Type_Desc").Value
        AllCodes(Step).Transaction_Type.Value = Result.fields("TX_Type_Value").Value
        AllCodes(Step).Transaction_Code.Description = Result.fields("TX_Code_Desc").Value
        AllCodes(Step).Transaction_Code.Value = Result.fields("TX_Code_Value").Value
        Result.MoveNext
        Step = Step + 1
    Wend
    Conn.Close
    
    GetCodes = AllCodes
    
End Function
Public Function Get_SecurityLevels() As SecurityLevel_Types()
    Dim Step As Integer
    Dim Statement As String
    Dim Result As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    Call Users_DB_Open '(Conn)
    
    Statement = "SELECT * from SECURITY"
                
    ' Execute the statement.
    Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    'getting all Trasaction Links
    Dim SecurityLevels_temp() As SecurityLevel_Types
    If Not (Result.EOF Or Result.BOF) Then
        For i = 0 To Result.fields.Count - 1
            If LCase(Result.fields(i).Name) <> "privilage" And UCase(Result.fields(i).Name) <> "SULSAT" Then
                ReDim Preserve SecurityLevels_temp(Step)
                SecurityLevels_temp(Step).Name = Result.fields(i).Name
                SecurityLevels_temp(Step).Locked = Result.fields(i).Value
                Step = Step + 1
            End If
        Next i
    End If

    
    Conn.Close
    
    Get_SecurityLevels = SecurityLevels_temp
    
End Function
Public Function Get_Languages() As Language_Type()
    Dim Step As Integer
    Dim Statement As String
    Dim Result As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    Call Users_DB_Open '(Conn)
    
    Statement = "SELECT * from languages"
                
    ' Execute the statement.
    Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    'getting all Trasaction Links
    Dim Languages_temp() As Language_Type
    While Not Result.EOF
        ReDim Preserve Languages_temp(Step)
        Languages_temp(Step).ID = Result.fields("LaguageID").Value
        Languages_temp(Step).Description = Result.fields("Laguage_Desc").Value
        Result.MoveNext
        Step = Step + 1
    Wend
    
    Conn.Close
    
    Get_Languages = Languages_temp
    
End Function
Public Function Get_SCs() As ServiceClassType()
    Dim Statement As String
    Dim Result As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    Call Users_DB_Open '(Conn)
    
    Statement = "SELECT * from Service_Classes"
                
    ' Execute the statement.
    Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    'getting all Trasaction Links
    Dim AllService_Classes() As ServiceClassType
    Dim Step As Integer
    While Not Result.EOF
        ReDim Preserve AllService_Classes(Step)
        AllService_Classes(Step).ID = Result.fields("SC_ID").Value
        AllService_Classes(Step).Desc = Result.fields("SC_Desc").Value
        Result.MoveNext
        Step = Step + 1
    Wend
    Get_SCs = AllService_Classes
    Conn.Close
End Function
Public Function Get_AIR_IPs() As AIR_IP_Type
    Dim Statement As String
    Dim Result As ADODB.Recordset
    Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    Call Users_DB_Open '(Conn)
    
    Statement = "SELECT * " + _
                "from AIRs where Local_Machine_IP = '" + SulSAT.Winsock1.LocalIP + "'  "
                
    ' Execute the statement.
    Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    'getting all Trasaction Links
    Dim AIRs As AIR_IP_Type
    Dim Step As Integer
    While Not Result.EOF
        ReDim Preserve AIRs.AIR(Step)
        AIRs.AIR(Step).AIR_Name = Result.fields("Connected_AIR_Name").Value
        AIRs.AIR(Step).AIR_IP = Result.fields("Connected_AIR_IP").Value
        AIRs.AIR(Step).AIR_Port = Result.fields("Connected_AIR_PORT").Value
        Result.MoveNext
        Step = Step + 1
        AIRs.Found = True
    Wend
    
    If AIRs.Found = False Then
        Result.Close
        Statement = "SELECT * " + _
                "from AIRs where Local_Machine_IP = '" + Left(SulSAT.Winsock1.LocalIP, InStrRev(SulSAT.Winsock1.LocalIP, ".")) + "X'  "
        Result.open Statement, Conn
        While Not Result.EOF
            ReDim Preserve AIRs.AIR(Step)
            AIRs.AIR(Step).AIR_Name = Result.fields("Connected_AIR_Name").Value
            AIRs.AIR(Step).AIR_IP = Result.fields("Connected_AIR_IP").Value
            AIRs.AIR(Step).AIR_Port = Result.fields("Connected_AIR_PORT").Value
            Result.MoveNext
            Step = Step + 1
            AIRs.Found = True
        Wend
    End If
    Get_AIR_IPs = AIRs
    Conn.Close
End Function


