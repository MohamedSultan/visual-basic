VERSION 5.00
Begin VB.Form Form_Codes 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Adjustement Code and Adjustement Type"
   ClientHeight    =   1920
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1920
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox ID 
      Height          =   315
      ItemData        =   "Form_Codes.frx":0000
      Left            =   3840
      List            =   "Form_Codes.frx":0002
      TabIndex        =   7
      Top             =   600
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   255
      Left            =   1320
      TabIndex        =   6
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Frame Frame_Codes 
      Height          =   975
      Left            =   480
      TabIndex        =   0
      Top             =   480
      Width           =   3255
      Begin VB.ComboBox AdjType 
         Height          =   315
         ItemData        =   "Form_Codes.frx":0004
         Left            =   1560
         List            =   "Form_Codes.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   120
         Width           =   1575
      End
      Begin VB.ComboBox AdjCode 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label Label37 
         Caption         =   "Adj. Type"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   195
         Width           =   855
      End
      Begin VB.Label Label36 
         Caption         =   "Adj. Code"
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   640
         Width           =   855
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Please Select Adjustement Code and Adjustement Type"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "Form_Codes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim AdjCodes() As CodesType

Private Sub AdjCode_Click()
    ID.Text = ID.List(AdjCode.ListIndex)
End Sub

Private Sub AdjCode_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call OK_Click
End Sub

Private Sub AdjType_Click()
    Dim LinkedCodes() As String
    LinkedCodes = Get_LinkedCodes(SulSat_ConfigurationData.Codes, AdjType.Text)
    
    AdjCode.Clear
    For i = 0 To UBound(LinkedCodes)
        AdjCode.AddItem LinkedCodes(i)
    Next i
    
    AdjCode.Text = AdjCode.List(0)
    ID.Text = ID.List(0)
End Sub

Private Sub AdjType_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call OK_Click
End Sub

Private Sub Form_Load()
    
    'AdjCodes = GetCodes
    
    
    Dim Distincts() As String
    'Distincts = Distinct_Array(suls)
    Distincts = Distinct_Array(SulSat_ConfigurationData.Codes)
    
    For i = 0 To UBound(Distincts)
        AdjType.AddItem Distincts(i)
    Next i
    
End Sub
Private Function Get_LinkedCodes(Array_() As CodesType, Selected_Adj_Type As String) As Variant
    Dim Step As Integer
    ID.Clear
    Dim LinkedCodes_Temp() As String
    
    For i = 0 To UBound(Array_)
        If Array_(i).Transaction_Type.Description = Selected_Adj_Type Then
            ReDim Preserve LinkedCodes_Temp(Step)
            LinkedCodes_Temp(Step) = Array_(i).Transaction_Code.Description
            ID.AddItem i
            Step = Step + 1
        End If
    Next i
    
    Get_LinkedCodes = LinkedCodes_Temp
    
End Function
Private Function Distinct_Array(Array_() As CodesType) As Variant
    Dim Step As Integer
    Dim Distincts() As String
    ReDim Preserve Distincts(Step)
    Distincts(Step) = Array_(0).Transaction_Type.Description
    
    For i = 1 To UBound(Array_)
        If Array_(i).Transaction_Type.Description <> Distincts(Step) Then
            Step = Step + 1
            ReDim Preserve Distincts(Step)
            Distincts(Step) = Array_(i).Transaction_Type.Description
        End If
    Next i
    
    Distinct_Array = Distincts
End Function
Private Sub Form_Terminate()
    Call SulSAT.Dim_Undim_All(True)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub OK_Click()
    If ID.Text = "" Or AdjCode = "" Or AdjType = "" Then
        MsgBox "PLZ select Adjustement Code and Adjustment Type", , "Error"
    Else
        Me.Hide
        SulSAT.SetFocus
        Call SulSAT.Dim_Undim_All(True)
        Call SulSAT.Save_All(SulSat_ConfigurationData.Codes(ID.Text).Transaction_Type.Value, SulSat_ConfigurationData.Codes(ID.Text).Transaction_Code.Value)
        Me.AdjType.Text = Me.AdjType.List(0)
        Me.ID = ""
    End If
End Sub
