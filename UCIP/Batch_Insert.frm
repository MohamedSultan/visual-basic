VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Batch_Insert 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Insert MSISDN Batch"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Get_File_Path 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5160
      TabIndex        =   4
      Top             =   120
      Width           =   375
   End
   Begin MSComDlg.CommonDialog Batch_Insert_CommonDialog 
      Left            =   120
      Top             =   960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Display 
      Height          =   3375
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   3
      Top             =   960
      Width           =   5415
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   375
      Left            =   2160
      TabIndex        =   2
      Top             =   480
      Width           =   1695
   End
   Begin VB.TextBox File_Path 
      Height          =   285
      Left            =   840
      TabIndex        =   0
      Text            =   "C:\Documents and Settings\msultan\Desktop\MSISDN_RANGES_PARAMETERS_20100506.xls"
      Top             =   120
      Width           =   4335
   End
   Begin VB.Label Label1 
      Caption         =   "Path"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "Batch_Insert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Terminate()
    Call SulSAT.Dim_Undim_All(True)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Get_File_Path_Click()
    Dim Directory As String
    If File_Path = "" Then File_Path = "D:\"
    Directory = Get_Dir(File_Path.Text)
    Batch_Insert_CommonDialog.InitDir = Directory
    Batch_Insert_CommonDialog.CancelError = False
    Batch_Insert_CommonDialog.Filter = "All Files (*.*)|*.*|Text Files(*.xls)|*.xls"
    Batch_Insert_CommonDialog.FilterIndex = 2
    Batch_Insert_CommonDialog.ShowOpen
    If Batch_Insert_CommonDialog.FileName <> "" Then
        File_Path.Text = Batch_Insert_CommonDialog.FileName
    End If
End Sub
Private Function Get_Dir(FileName As String) As String
    Dim Loc, Old_Loc As Integer
    Loc = 1
    Loc = InStr(1, FileName, "\")
    While Loc <> 0
        Old_Loc = Loc
        Loc = InStr(Loc + 1, FileName, "\")
    Wend
    Get_Dir = Left(FileName, Old_Loc)
End Function
'private function ReadExcelConfiguations
Private Sub Start_Click()
    Dim AppExcel As Excel.Application
    Set AppExcel = New Excel.Application
    AppExcel.Workbooks.Open File_Path
    
    Dim Locs As Cell_Locations
    
    With Locs
        For Rows_ = 1 To 10
            For Col = 1 To 20
                Select Case AppExcel.Sheets("SDP").Cells(Rows_, Col)
                    Case "MSISDN Range From:"
                        .From.Row = Rows_
                        .From.Column = Col
                    Case "MSISDN Range to:"
                        .To.Row = Rows_
                        .To.Column = Col
                    Case "SDP"
                        .SDP.Row = Rows_
                        .SDP.Column = Col
                    Case "Service_key"
                        .ServiceKey.Row = Rows_
                        .ServiceKey.Column = Col
                    Case "Prepaid OICK/TICK"
                        .OICK.Row = Rows_
                        .OICK.Column = Col
                    End Select
            Next Col
        Next Rows_
    End With
    
    Dim Step As Integer
    Dim MSISDN As String
    Dim ServiceClass As String
    Dim Language As String
    Dim ResponseCodeID As String
    
    
    Dim Statement As String
    Dim Conn As ADODB.Connection
    Dim Result As ADODB.Recordset
    
    Set Conn = New ADODB.Connection
    Set Result = CreateObject("ADODB.Recordset")
    
    Conn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=localhost;UID=root;PWD=root;PORT=3306"
    Conn.Open
    
    Result.Open "DELETE FROM Group_Stats.msisdn_ranges", Conn
    
    Step = Locs.From.Row + 1
    
    With AppExcel.Sheets("SDP")
        While .Cells(Step, 1) <> ""
            Statement = BuildStatement(.Cells(Step, Locs.From.Column), .Cells(Step, Locs.To.Column), .Cells(Step, Locs.SDP.Column), .Cells(Step, Locs.OICK.Column), .Cells(Step, Locs.ServiceKey.Column))
            Result.Open Statement, Conn
                        
            If Display = "" Then Display = "Start,End,SDP_Name,OICK,Service_Key " + vbCrLf
            Display = Display + .Cells(Step, Locs.From.Column) + "," + .Cells(Step, Locs.To.Column) + "," + .Cells(Step, Locs.SDP.Column) + "," + .Cells(Step, Locs.OICK.Column) + "," + .Cells(Step, Locs.ServiceKey.Column) + vbCrLf
            Display.SelStart = Len(Display)
            
            Step = Step + 1
        Wend
    End With
    
    If Conn.State = 1 Then Conn.Close
    
End Sub
Private Function BuildStatement(From As String, To_ As String, SDP As String, OICK As String, ServiceKey As String) As String
    BuildStatement = "INSERT INTO Group_Stats.msisdn_ranges(Start,End,SDP_Name,OICK,Service_Key) VALUES(" + _
                        "'" + From + "'," + _
                        "'" + To_ + "'," + _
                        "'" + SDP + "'," + _
                        "'" + OICK + "'," + _
                        "'" + ServiceKey + "')"
End Function
