'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalSCRIPT(TM)
'
' NAME: CCPULS.vbs
'
' AUTHOR:  , 
' DATE  : 01/07/2004
'
' COMMENT: Mohamed Fouad Taha
'
'==========================================================================
On Error Resume Next
const HKEY_LOCAL_MACHINE = &H80000002
strComputer = "."

Set oReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" &_
 strComputer & "\root\default:StdRegProv")
strKeyPath = "SOFTWARE\ORACLE"
strValueName = "ORACLE_HOME"
oReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

 Const ForReading = 1, ForWriting = 2, ForAppending = 8
 Set fso = CreateObject("Scripting.FileSystemObject")
 Set f = fso.OpenTextFile(strValue & "\network\admin\tnsnames.ora", ForAppending , False)
  f.Write vbCrLf & "############## Brio & CCPLUS  ######################" & vbCrLf _ 
  & "DMRTPRD1 ="& vbCrLf _ 
  & " (DESCRIPTION =" & vbCrLf _
  & "   (ADDRESS_LIST =" & vbCrLf _
  & "       (ADDRESS =" & vbCrLf _
  & "         (PROTOCOL = TCP)" & vbCrLf _
  & "         (Host = 10.234.133.105)" & vbCrLf _
  & "         (Port = 1530)" & vbCrLf _
  & "       )" & vbCrLf _
  & "   )" & vbCrLf _
  & "   (CONNECT_DATA =" & vbCrLf _
  & "     (SID = DMRTPRD1)" & vbCrLf _
  & "        (GLOBAL_NAME = DMRTPRD1.world)" & vbCrLf _
  & "   )" & vbCrLf _
  & " )" & vbCrLf
f.Close 

Set fsoH = CreateObject("Scripting.FileSystemObject")
 Set fH = fsoH.OpenTextFile("c:\WINNT\system32\drivers\etc\hosts", ForAppending , False)
  fH.Write vbCrLf & "############## Brio & CCPLUS  ######################" & vbCrLf _ 
  & "10.234.133.104	 dcctipb1"& vbCrLf _
  & "10.234.133.105  dcersPa1"& vbCrLf _
  & "10.234.133.106  dccimda1"& vbCrLf _
  & "10.234.133.103  dcctipa1"& vbCrLf _
  & "###########################################################"& vbCrLf 
  fH.Close 