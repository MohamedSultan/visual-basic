' ProcessKillLocal.vbs
' Sample VBScript to kill a program
' Author Guy Thomas http://computerperformance.co.uk/
' Version 2.7 - December 2005
' ------------------------ -------------------------------' 
Option Explicit
Dim objWMIService, objProcess, colProcess
Dim strComputer, strProcessKill 
strComputer = "."
strProcessKill = "'SulSat.exe'" 

Set objWMIService = GetObject("winmgmts:" _
& "{impersonationLevel=impersonate}!\\" _ 
& strComputer & "\root\cimv2") 

Set colProcess = objWMIService.ExecQuery _
("Select * from Win32_Process Where Name = " & strProcessKill )
For Each objProcess in colProcess
objProcess.Terminate()
Next 
'WSCript.Echo "Just killed process " & strProcessKill _
'& " on " & strComputer
dim filesys
Set filesys = CreateObject("Scripting.FileSystemObject") 
If filesys.FileExists("C:\SulSat\SulSat.exe") Then 
	filesys.DeleteFile "C:\SulSat\SulSat.exe"
End If

WScript.Quit 