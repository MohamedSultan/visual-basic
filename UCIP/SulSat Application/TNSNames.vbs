'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalSCRIPT(TM)
'
' NAME: CCPULS.vbs
'
' AUTHOR:  , 
' DATE  : 01/07/2004
'
' COMMENT: Mohamed Fouad Taha
'
'==========================================================================
On Error Resume Next
const HKEY_LOCAL_MACHINE = &H80000002
strComputer = "."

Set oReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" &_
 strComputer & "\root\default:StdRegProv")
strKeyPath = "SOFTWARE\ORACLE"
strValueName = "ORACLE_HOME"
oReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

 Const ForReading = 1, ForWriting = 2, ForAppending = 8
 Set fso = CreateObject("Scripting.FileSystemObject")
 Set f = fso.OpenTextFile(strValue & "\network\admin\tnsnames.ora", ForAppending , False)
  f.Write vbCrLf & "############## SulSat DB  ######################" & vbCrLf _ 
  & "SulSat ="& vbCrLf _ 
  & " (DESCRIPTION =" & vbCrLf _
  & "   (ADDRESS_LIST =" & vbCrLf _
  & "       (ADDRESS =" & vbCrLf _
  & "         (PROTOCOL = TCP)" & vbCrLf _
  & "         (Host = 172.28.201.21)" & vbCrLf _
  & "         (Port = 1521)" & vbCrLf _
  & "       )" & vbCrLf _
  & "   )" & vbCrLf _
  & "   (CONNECT_DATA =" & vbCrLf _
  & "     (SID = AIWADRD1)" & vbCrLf _
  & "   )" & vbCrLf _
  & " )" & vbCrLf
f.Close 
