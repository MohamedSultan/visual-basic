Attribute VB_Name = "DB_Transactions"
Type TransactionCode_Type
    Value As String
    Description As String
End Type
Type TransactionType_Type
    Value As String
    Description As String
End Type
Type CodesType
    Transaction_Type As TransactionType_Type
    Transaction_Code As TransactionCode_Type
End Type
Type ServiceClassType
    ID As Integer
    Desc As String
End Type
Type SecurityLevel_Types
    Name As String
    Locked As Boolean
End Type
Type Language_Type
    ID As Integer
    Description As String
End Type
Type AIR_IP_Sub_Type
    AIR_IP As String
    AIR_Name As String
    AIR_Port As String
End Type
Type AIR_IP_Type
    Found As Boolean
    AIR() As AIR_IP_Sub_Type
End Type
Type SulSatConf_Type
    ServiceClasses() As ServiceClassType
    SecurityLevels() As SecurityLevel_Types
    Language() As Language_Type
    AIR As AIR_IP_Type
    Codes() As CodesType
End Type
Public Conn As ADODB.Connection
Public Function SecurityLevelType_VS_ID(SecurityLevelType As String) As Integer
    SecurityLevelType_VS_ID = 100
    For i = 0 To UBound(AllSecurityLevels)
        If UCase(AllSecurityLevels(i).SecurityLevelName) = UCase(SecurityLevelType) Then SecurityLevelType_VS_ID = i
    Next i
End Function
Private Sub StoreSecurityLevels(PrivilageName As String, SecurityLevelType As String, SecurityLevelValue As Boolean)
    Select Case PrivilageName
        Case "StandardRefill"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.StandardRefill = SecurityLevelValue
        Case "Adjustment"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Adjustment = SecurityLevelValue
        Case "Activate"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Activate = SecurityLevelValue
        Case "Change_AIR"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Change_AIR = SecurityLevelValue
        Case "Lock_DB"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Lock_DB = SecurityLevelValue
        Case "Refill"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Refill = SecurityLevelValue
        Case "UCIP_Command"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.UCIP_Command = SecurityLevelValue
        Case "Update_ServiceClass"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Update_ServiceClass = SecurityLevelValue
        Case "Update_User"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Update_User = SecurityLevelValue
        Case "UpdateLanguage"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.UpdateLanguage = SecurityLevelValue
        Case "Billing_GW"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.BGW_History = SecurityLevelValue
        Case "BasicInformations"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.BasicInformations = SecurityLevelValue
        Case "Balances"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Balances = SecurityLevelValue
        Case "Accumulators"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Accumulators = SecurityLevelValue
        Case "LoadAll"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.LoadAll = SecurityLevelValue
        Case "InstallSubscribers"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.InstallSubscribers = SecurityLevelValue
        Case "Batch_Insert_MSISDN"
            AllSecurityLevels(SecurityLevelType_VS_ID(SecurityLevelType)).SecurityLevelPrivilages.Batch_Insert_MSISDN = SecurityLevelValue
    End Select
End Sub
Public Function GetAllSulSatConfigurations() As SulSatConf_Type
    Dim Step As Integer
    Dim Statement As String
    Dim Result As ADODB.Recordset
    'Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    On Error GoTo LocalHandler
    Set Result = CreateObject("ADODB.Recordset")
    Call Users_DB_Open '(Conn)
    '-------------------------------------------------------------------------------------------------------------------------------------------------------------

    'Getting Codes
    '-------------
    Statement = "SELECT TX_Code.Description  TX_Code_Desc, TX_Code.Value  TX_Code_Value, TX_Type.Description  TX_Type_Desc, TX_Type.Value  TX_Type_Value " + _
            "FROM TransactionCode  TX_Code, TransactionType  TX_Type, TransactionLink  TX_Link " + _
            "Where TX_Type.TransactionTypeID = TX_Link.TransactionTypeID And TX_Code.TransactionCodeID = TX_Link.TransactionCodeID " + _
            "ORDER BY TX_Type.Description"
                
    Result.open Statement, Conn
    While Not Result.EOF
        ReDim Preserve GetAllSulSatConfigurations.Codes(Step)
        GetAllSulSatConfigurations.Codes(Step).Transaction_Type.Description = Result.fields("TX_Type_Desc").Value
        GetAllSulSatConfigurations.Codes(Step).Transaction_Type.Value = Result.fields("TX_Type_Value").Value
        GetAllSulSatConfigurations.Codes(Step).Transaction_Code.Description = Result.fields("TX_Code_Desc").Value
        GetAllSulSatConfigurations.Codes(Step).Transaction_Code.Value = Result.fields("TX_Code_Value").Value
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    Step = 0
    '-------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    'Getting Security Levels
    '-----------------------
    Statement = "SELECT * from SECURITY"
                
    ' Execute the statement.
    Result.open Statement, Conn
    
    'getting all Transaction Links
    If Not (Result.EOF Or Result.BOF) Then
        For i = 0 To Result.fields.Count - 1
            If LCase(Result.fields(i).Name) <> "privilage" And Result.fields(i).Name <> "SulSat" Then
                ReDim Preserve GetAllSulSatConfigurations.SecurityLevels(Step)
                GetAllSulSatConfigurations.SecurityLevels(Step).Name = Result.fields(i).Name
                GetAllSulSatConfigurations.SecurityLevels(Step).Locked = Result.fields(i).Value
                Step = Step + 1
            End If
        Next i
    End If
    Result.Close
    Step = 0
    '--------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Getting All Languages
    '-----------------------
    Statement = "SELECT * from languages"
                
    ' Execute the statement.
    Result.open Statement, Conn
    
    'getting all Languages
    While Not Result.EOF
        ReDim Preserve GetAllSulSatConfigurations.Language(Step)
        GetAllSulSatConfigurations.Language(Step).ID = Result.fields("LaguageID").Value
        GetAllSulSatConfigurations.Language(Step).Description = Result.fields("Laguage_Desc").Value
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    Step = 0
    '--------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Getting All Service Class
    '--------------------------
   Statement = "SELECT * from Service_Classes"
                
    ' Execute the statement.
    Result.open Statement, Conn
    
    'getting all Service Class
    While Not Result.EOF
        ReDim Preserve GetAllSulSatConfigurations.ServiceClasses(Step)
        GetAllSulSatConfigurations.ServiceClasses(Step).ID = Result.fields("SC_ID").Value
        GetAllSulSatConfigurations.ServiceClasses(Step).Desc = Result.fields("SC_Desc").Value
        Result.MoveNext
        Step = Step + 1
    Wend
    Result.Close
    Step = 0
    '--------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Getting AIR IP
    '--------------
    Statement = "SELECT * " + _
                "from AIRs where Local_Machine_IP = '" + SulSAT.Winsock1.LocalIP + "'  "
                
    ' Execute the statement.
    'Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    'getting all AIR IPs
    While Not Result.EOF
        ReDim Preserve GetAllSulSatConfigurations.AIR.AIR(Step)
        GetAllSulSatConfigurations.AIR.AIR(Step).AIR_Name = Result.fields("Connected_AIR_Name").Value
        GetAllSulSatConfigurations.AIR.AIR(Step).AIR_IP = Result.fields("Connected_AIR_IP").Value
        GetAllSulSatConfigurations.AIR.AIR(Step).AIR_Port = Result.fields("Connected_AIR_PORT").Value
        Result.MoveNext
        Step = Step + 1
        GetAllSulSatConfigurations.AIR.Found = True
    Wend
    
    If GetAllSulSatConfigurations.AIR.Found = False Then
        Result.Close
        Statement = "SELECT * " + _
                "from AIRs where Local_Machine_IP = '" + Left(SulSAT.Winsock1.LocalIP, InStrRev(SulSAT.Winsock1.LocalIP, ".")) + "X'  "
        Result.open Statement, Conn
        While Not Result.EOF
            ReDim Preserve GetAllSulSatConfigurations.AIR.AIR(Step)
            GetAllSulSatConfigurations.AIR.AIR(Step).AIR_Name = Result.fields("Connected_AIR_Name").Value
            GetAllSulSatConfigurations.AIR.AIR(Step).AIR_IP = Result.fields("Connected_AIR_IP").Value
            GetAllSulSatConfigurations.AIR.AIR(Step).AIR_Port = Result.fields("Connected_AIR_PORT").Value
            Result.MoveNext
            Step = Step + 1
            GetAllSulSatConfigurations.AIR.Found = True
        Wend
    End If
    Result.Close
    Step = 0
    
    '---------------------------------------------------------------------------------------
    'Security Levels
    Statement = "select *  from SECURITY"
    Result.open Statement, Conn
    If Not Result.EOF Then
        ReDim Preserve AllSecurityLevels(Result.fields.Count - 2)  'Ignoring Privilage
        Step = 0
        For i = 0 To Result.fields.Count - 1
            If UCase(Result.fields(i).Name) <> "PRIVILAGE" Then
                AllSecurityLevels(Step).SecurityLevelName = Result.fields(i).Name
                Step = Step + 1
            End If
        Next i
    End If
    Step = 0
    While Not Result.EOF
        If UCase(Result.fields("PRIVILAGE").Value) <> "LOCKED" Then
            For i = 0 To Result.fields.Count - 1
                If UCase(Result.fields(i).Name) <> "PRIVILAGE" Then _
                    Call StoreSecurityLevels(Result.fields("PRIVILAGE").Value, Result.fields(i).Name, Val(Result.fields(Result.fields(i).Name).Value))
            Next i
        End If
        Result.MoveNext
    Wend
    Result.Close
    Step = 0
    
    '---------------------------------------------------------------------------------------
    'BGW Data
    Statement = "select *  from BILLING_GW"
    Result.open Statement, Conn
    If Not Result.EOF Then
        With BillingGW_Form
            'Dim CommandBeforeMSISDN, CommandAfterMSISDN As String
            .UserName = "aregaily"
            .Password = "aregaily123"
            .UserName = Result.fields("USERNAME").Value
            .Password = Result.fields("PASSWORD").Value
            .Command(0) = "cd " + Result.fields("PATH").Value
            .Command1_Part1 = Result.fields("COMMANDBEFOREMSISDN").Value
            .Command1_Part2 = Result.fields("COMMANDAFTERMSISDN").Value
            '.Command(1) = CommandBeforeMSISDN + SulSAT.MSISDNs_List.Text + CommandAfterMSISDN '160405626"
            .Command(2) = "echo Finished"
            .Port = Result.fields("PORT").Value
            .IP = Result.fields("IP").Value
        End With
    End If
    Result.Close
    Step = 0
    'Conn.Close
LocalHandler:
    If Err.Number = 94 Then Resume Next 'Invalid use of null
End Function


Private Function GetTransactionCodeID(TransactionCodes As Variant, Value As Double) As Integer
    For i = 0 To UBound(TransactionCodes)
        If Value = TransactionCodes(i) Then
            GetTransactionCodeID = TransactionCodes(i)
            Exit Function
        End If
    Next i
End Function
Public Function Add_Transaction(Transaction_Type As String, Transaction_Response_Desc As String, _
                                    Optional MainAdjusmentAmount As Double, Optional Codes As Boolean)
    Dim Statement As String
    Dim Result As ADODB.Recordset
    'Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    For Each Ctr In SulSAT
        If TypeOf Ctr Is CommandButton Or _
            TypeOf Ctr Is TextBox Or _
            TypeOf Ctr Is ComboBox Then Ctr.Enabled = False
    Next Ctr
    Call SulSAT.User_Security_Level
   
    Dim Details As String
    Select Case Transaction_Type
        Case "Get Account Details"
            Details = UCIP_Request_Data.subscriberNumberNAI + "," + UCIP_Request_Data.messageCapabilityFlag
        Case "Balance Enquiry"
            Details = UCIP_Request_Data.subscriberNumberNAI + "," + UCIP_Request_Data.messageCapabilityFlag
        Case "Accumulator enquiry"
            Details = UCIP_Request_Data.subscriberNumberNAI + "," + UCIP_Request_Data.messageCapabilityFlag
        Case "Update Service Class"
                   Details = UCIP_Request_Data.subscriberNumberNAI + "," + _
                   UCIP_Request_Data.serviceClassCurrent + "," + _
                   UCIP_Request_Data.serviceClassNew
        Case "Adjustment"
            Details = UCIP_Request_Data.subscriberNumberNAI + "," + _
                        UCIP_Request_Data.messageCapabilityFlag + "," + _
                        Str(Val(UCIP_Request_Data.adjustmentAmount) / 100) + "," + _
                        UCIP_Request_Data.externalData1 + "," + _
                        UCIP_Request_Data.externalData2 + "," + _
                        UCIP_Request_Data.relativeDateAdjustmentSupervision + "," + _
                        UCIP_Request_Data.relativeDateAdjustmentServiceFee
        Case "Refill"
               Details = Str(Val(UCIP_Request_Data.TransactionAmountRefill) / 100) + "," + _
                            UCIP_Request_Data.paymentProfileID + "," + _
                            UCIP_Request_Data.subscriberNumberNAI + "," + _
                            UCIP_Request_Data.externalData1 + "," + _
                            UCIP_Request_Data.externalData2
        Case "Standard Voucher Refill"
                Details = UCIP_Request_Data.activationNumber + "," + _
                            UCIP_Request_Data.messageCapabilityFlag + "," + _
                            UCIP_Request_Data.subscriberNumberNAI + "," + _
                            UCIP_Request_Data.externalData1 + "," + _
                            UCIP_Request_Data.externalData2
        Case "Install Subscriber"
                Details = UCIP_Request_Data.serviceClassNew + "," + _
                            UCIP_Request_Data.newLanguageID
    End Select
    
    Call Users_DB_Open '(Conn)
                   
    Statement = "INSERT INTO Transactions " & _
        "(  UserName, TransactionType, NodeName, NodeIP, Response, MSISDN,ResponseCodeDesc, Details )" & _
        " VALUES (" & _
        "'" & Users.UserName & "', " & _
        "'" & Transaction_Type & "', " & _
        "'" & SulSAT.Winsock1.LocalHostName & "', " & _
        "'" & SulSAT.Winsock1.LocalIP & "', " & _
        "'" & UCIP_Response_Data.ResponseCode & "', " & _
        "'" & SulSAT.MSISDNs_List & "', " & _
        "'" & Transaction_Response_Desc & "'," & _
        "'" & Details & "'" & _
        ")"
    
    ' Execute the statement.
    Set Result = CreateObject("ADODB.Recordset")
    Result.open Statement, Conn
    
    If Result.State = 1 Then Result.Close
    'Conn.Close
    
    For Each Ctr In SulSAT
        If TypeOf Ctr Is CommandButton Or _
            TypeOf Ctr Is TextBox Or _
            TypeOf Ctr Is ComboBox Then Ctr.Enabled = True
    Next Ctr
    Call SulSAT.User_Security_Level
End Function

Public Function DB_RecordValue(Record As ADODB.Recordset, FieldName As String) As String
    If Record.fields.Count > 0 Then
        For i = 0 To Record.fields.Count - 1
            If FieldName = Record.fields.Item(i).Name Then DB_RecordValue = Record.fields.Item(i).Value
        Next i
    End If
End Function


Public Function DB_Users(Transaction_Type As String, _
                            UserName As TextBox, _
                            UserName_Comment As Label, Password_comment As Label, Both_Comments As Label, _
                            Optional Security_Level As String, _
                            Optional Lock_DB As Boolean, _
                            Optional Password As String, _
                            Optional Test As Boolean) As Boolean
    Dim Statement As String
    Dim Result As ADODB.Recordset
    'Dim Conn As ADODB.Connection
    Dim Ctl As Control
    
    Call Users_DB_Open
    
        Dim Cont As Boolean
        Dim Locked_Users As String
        
        Select Case LCase(Transaction_Type)
'-----------------------------------------------------------------------------------------------------------
            Case "unlock db"
                ' Compose the Select statement.
                If Security_Level <> "" Then
                    Statement = "select " & Security_Level & " from Security where Privilage='Locked' "
                Else
                    Cont = True
                    Statement = "select SulSat from Security where Privilage='Sultan' "     'Always returns empty
                End If
                ' Execute the statement.
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                
                If Cont Or Not (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then   'This user name Exists
                
                    If Security_Level = "" Then     'Lock All
                        Statement = ""
                        For i = 0 To SulSAT.Security_Levels_Locked.ListCount - 1
                            Statement = " update Security " & _
                                       " set " & SulSAT.Security_Levels_Locked.List(i) & "=0 " & _
                                        "where Privilage='Locked'   "
                            
                            Conn.Execute Statement, , adCmdText
                            
                            Locked_Users = Locked_Users + SulSAT.Security_Levels_Locked.List(i) & ","
                            Call SulSAT.Update_Response("Unlock DB: For " & SulSAT.Security_Levels_Locked.List(i) & " Users")
                        Next i
                    Else
                        Statement = "update Security " & _
                                   " set " & Security_Level & "=0 " & _
                                    "where Privilage='Locked' "
                        
                        Conn.Execute Statement, , adCmdText
                        DB_Users = True
                        Call SulSAT.Update_Response("Unock DB: For " & Security_Level & " Users is Done")
                    End If
                
                
                'SulSAT.Security_Levels_Locked.Text = ""
                'DB_Status = "DB Is Locked For " & Left(Locked_Users, Len(Locked_Users) - 1) & " Users"
            Else
                MsgBox "Privilege Does Not Exist, PLZ Select A Valid One"
                Call SulSAT.Update_Response("Privilege Does Not Exist, PLZ Select A Valid One")
            End If
            
'-----------------------------------------------------------------------------------------------------------
            Case "lock db"
                'On Error GoTo Lock_DB_Handler
                ' Compose the Select statement.
                If Security_Level <> "" Then
                    Statement = "select " & Security_Level & " from Security where Privilage='Locked' "
                Else
                    Cont = True
                    Statement = "select SulSat from Security where Privilage='Sultan' "     'Always returns empty
                End If
                ' Execute the statement.
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                
                If Cont Or Not (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then   'This user name Exists
                
                    If Security_Level = "" Then     'Lock All
                        Statement = ""
                        For i = 0 To SulSAT.Security_Levels_Locked.ListCount - 1
                            Statement = " update Security " & _
                                       " set " & SulSAT.Security_Levels_Locked.List(i) & "=1 " & _
                                        "where Privilage='Locked'   "
                            
                            Conn.Execute Statement, , adCmdText
                            
                            Locked_Users = Locked_Users + SulSAT.Security_Levels_Locked.List(i) & ","
                            Call SulSAT.Update_Response("Lock DB: For " & SulSAT.Security_Levels_Locked.List(i) & " Users")
                        Next i
                    Else
                        Statement = "update Security " & _
                                   " set " & Security_Level & "=1 " & _
                                    "where Privilage='Locked' "
                        
                        Conn.Execute Statement, , adCmdText
                        DB_Users = True
                        Call SulSAT.Update_Response("Lock DB: For " & Security_Level & " Users is Done")
                    End If
                
                
                'SulSAT.Security_Levels_Locked.Text = ""
                'DB_Status = "DB Is Locked For " & Left(Locked_Users, Len(Locked_Users) - 1) & " Users"
            Else
                MsgBox "Privilege Does Not Exist, PLZ Select A Valid One"
                Call SulSAT.Update_Response("Privilege Does Not Exist, PLZ Select A Valid One")
            End If
            
'-----------------------------------------------------------------------------------------------------------
            Case "locked"
                ' Compose the Select statement.
                'Dim Temp_Security_Level As String
                'Temp_Security_Level = Security_Level.Text
                Statement = "select " & Security_Level & " from Security where Privilage='Locked' "
                ' Execute the statement.
                
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                
                Dim Locked As Boolean
                If (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then MsgBox "DB Error"
                
                If Result.fields.Item(0).Value = "1" Then DB_Users = True
'---------------------------------------------------------------------------------------------------------------
            Case "login"
                Statement = "select SulSat_UserName,SulSat_Level_Of_Security " + _
                            "from Users " + _
                            "where UPPER(SulSat_UserName)='" & UCase(UserName) & "' "
                
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                
                'Active directory Connection
                Dim DisplayName As String
                Dim Authenticate_UserName_Password_Return As Authentication_Return
                
                Authenticate_UserName_Password_Return = Authenticate_UserName_Password(UserName, Password)
                
                If Not Authenticate_UserName_Password_Return.Authentication_Status Then     'Failed
                    Both_Comments = Authenticate_UserName_Password_Return.Authentication_String
                    Call Users.LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
                    Call Add_Transaction(Transaction_Type, Both_Comments.Caption)
                    Result.Close
                    'Conn.Close
                    Exit Function
                End If
                
                If (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then
                    'Both_Comments = "Invalid User Name."
                    Both_Comments = "Invalid User Name/Password"
                    Call Users.LabelsHiding(Both_Comments, UserName_Comment, Password_comment, False, True, True)
                    Call Add_Transaction(Transaction_Type, "Not Exist In SulSat DB")
                Else        'Success
                    Users.Level_Of_Security = Result.fields("SulSat_Level_Of_Security").Value
                    CurrentUser.UserName = Result.fields("SulSat_UserName").Value

                    SulSAT.UserDetails_Label = Authenticate_UserName_Password_Return.Authentication_String & " - " & Users.Level_Of_Security
                    DB_Users = True
                End If
'-----------------------------------------------------------------------------------------------------------------------------------
            Case "update user"
                ' Compose the Select statement.
                    Statement = "select SulSat_UserName from Users where SulSat_UserName='" & UserName & "' "
                
                    ' Execute the statement.
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                'Result.MoveFirst
                    
                If Not Users.Validate(UserName, UserName_Comment, Password_comment, Both_Comments, True) Then
                    Result.Close
                    'Conn.Close
                    Exit Function
                End If
                
                If Not (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then   'This user name Exists
                
                    If Security_Level = "" Then
                        Statement = "update users " & _
                                   " set SulSat_Level_Of_Security='CS3'" & _
                                    "where SulSat_UserName='" & UserName & "' "
                    Else
                        Statement = "update users " & _
                                   " set SulSat_Level_Of_Security='" & Trim(Security_Level) & "'" & _
                                    "where SulSat_UserName='" & UserName & "' "
                    End If
                    
                 Conn.Execute Statement, , adCmdText
                
                Call SulSAT.Update_Response("Update User: User " & UserName & " Is Updated")
                UserName = ""
                Password = ""
                UserName_Comment = ""
                Password_comment = ""
                Both_Comments = ""
                SulSAT.SecurityLevels.Text = "CS3"
                
                '' Clear the TextBoxes.
                'For Each Ctl In SulSAT.Controls
                '    If TypeOf Ctl Is TextBox Then
                '        Ctl.Text = ""
                '    End If
                'Next Ctl
            Else
                MsgBox "User Does Not Exist, PLZ Select A Valid One"
                Call SulSAT.Update_Response("Update User: User Does Not Exist, PLZ Select A Valid One")
            End If
            'DB_Status = SulSAT.Response_TExt
                
'--------------------------------------------------------------------------------------------------------------
            Case "delete user"
                If Not Users.Validate(UserName, UserName_Comment, Password_comment, Both_Comments) Then
                    'Conn.Close
                    Exit Function
                End If
                ' Compose the Select statement.
                Statement = "select SulSat_UserName from Users where SulSat_UserName='" & UserName & "' "
                
                ' Execute the statement.
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                
                If Not (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then   'This user name Exists
                    ' Compose the Select statement.
                    Statement = "delete from Users where SulSat_UserName='" & UserName & "' "
                                
                    Conn.Execute Statement, , adCmdText
                    Call SulSAT.Update_Response("Delete User: User " + UserName + " Is Deleted")
                Else
                    MsgBox "User Does Not Exist, PLZ Select A Valid One"
                    Call SulSAT.Update_Response("Delete User: User Does Not Exist, PLZ Select A Valid One")
                End If
                UserName = ""
                Password = ""
                UserName_Comment = ""
                Password_comment = ""
                Both_Comments = ""
                'SulSAT.SecurityLevels.Text = ""
                'DB_Status = SulSAT.Response_TExt
'--------------------------------------------------------------------------------------------------------------
            Case "add user"
                ' Compose the Select statement.
                    Statement = "select SulSat_UserName from Users where SulSat_UserName='" & UserName & "' "
                
                    ' Execute the statement.
                Set Result = CreateObject("ADODB.Recordset")
                Result.open Statement, Conn
                'Result.MoveFirst
                    
                If Not Users.Validate(UserName, UserName_Comment, Password_comment, Both_Comments, True) Then
                    Result.Close
                    'Conn.Close
                    Exit Function
                End If
                                
                If (Result.EOF Or Result.BOF Or Result.fields.Count = 0) Then   'This user name is unique
                
                    If Security_Level = "" Then
                    
                        Statement = "INSERT INTO Users " & _
                            "(SulSat_UserName) " & _
                            " VALUES (" & _
                            "'" & UserName.Text & "' " & _
                            ") "
                    Else
                        Statement = "INSERT INTO Users " & _
                            "(SulSat_UserName, SulSat_Level_Of_Security) " & _
                            " VALUES (" & _
                            "'" & UserName.Text & "', " & _
                            "'" & Trim(Security_Level) & "'" & _
                            ") "
                    End If
                    
                 Conn.Execute Statement, , adCmdText
                
                Call SulSAT.Update_Response("Add User: User " & UserName & " Is Added")
                UserName = ""
                Password = ""
                UserName_Comment = ""
                Password_comment = ""
                Both_Comments = ""
                'Security_Level.Text = ""
            Else
                MsgBox "User Already Exists, PLZ Select Another"
                Call SulSAT.Update_Response("Add User: User Already Exists, PLZ Select Another")
            End If
            'DB_Status = SulSAT.Response_TExt
'-----------------------------------------------------------------------------------------------------------------------------------
        End Select
        If Result.State = 1 Then Result.Close
        'Conn.Close
        
        'Call Add_Transaction(Transaction_Type, DB_Status)
End Function
