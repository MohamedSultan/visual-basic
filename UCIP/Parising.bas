Attribute VB_Name = "Parising"
Public UCIP_Response_Data  As UCIP_Response_Type
Public UCIP_Request_Data  As UCIP_Inputs_Type

Public Type Node_Details
    NOdeName As String
    NodeValue As String
    'Sub_Node(5) As Sub_Node_Details
    Array_ As Boolean
    SubMember As IXMLDOMNode
    SubMembers() As String
End Type
Public Type Node_Members
    member() As Node_Details
End Type
Public Type DedicatedAccounts
    DedicatedAccountValue As String
    DedicateAccountExpiryDate As String
End Type
Public Type Accumulators
    'accumulatorID As String
    AccumulatorStartDate As String
    accumulatorEndDate As String
    accumulatorValue As String
End Type
Public Type CCPAI_Parser_Return
    Ret As Variant
    Found As Boolean
End Type
Public Type UCIP_Response_Type
    accountActivatedFlag As String
    accountFlags As String
    accountValue1 As String
    accountValue2 As String
    accountValueAfter1 As String
    accountValueAfter2 As String
    accumulatorInformation As String
    allowedOptions As String
    chargingResultInformation As String
    charginResultInformation As String
    creditClearanceDate As String
    creditClearancePeriod As String
    currency1 As String
    currency2 As String
    currentLanguageID As String
    dedicatedAccountInformation As String
    fafChangeUnbarDate As String
    fafInformation As String
    firstIVRCallFlag As String
    masterSubscriberFlag As String
    maxAllowedNumbers As String
    notAllowedReason As String
    pinCode As String
    promotionAnnouncementCode As String
    rechargeAmount1MainTotal As String
    rechargeAmount1PromoTotal As String
    rechargeAmount2MainTotal As String
    rechargeAmount2PromoTotal As String
    refillBarredFlag As String
    refillFraudCount As String
    removalPeriod As String
    ResponseCode As String
    serviceClassChangeUnbarDate As String
    serviceClassCurrent As String
    serviceFeeDate As String
    serviceFeeDateAfter As String
    serviceFeeDaysPromoExt As String
    serviceFeeDaysTotalExt As String
    serviceRemovalDate As String
    supervisionDate As String
    supervisionDateAfter As String
    supervisionDaysPromoExt As String
    supervisionDaysTotalExt As String
    temporaryBlockedFlag As String
    transactionAmount1Refill As String
    transactionAmount2Refill As String
    transactionServiceClass As String
    transactionVVPeriodExt As String
    valueVoucherDate As String
    valueVoucherDateAfter As String
    serviceFeePeriod As String
    supervisionPeriod As String
    DedicatedAccount(5) As DedicatedAccounts
    Accumulator(5) As Accumulators
End Type
Type UCIP_Inputs_Type
    subscriberNumberNAI As String
    subscriberNumber As String
    messageCapabilityFlag As String
    adjustmentAmount As String
    externalData1 As String
    externalData2 As String
    relativeDateAdjustmentSupervision As String
    relativeDateAdjustmentServiceFee As String
    requestedOwner As String
    TransactionAmountRefill As String
    paymentProfileID As String
    activationNumber As String
    currentLanguageID As String
    newLanguageID As String
    firstIVRCallDone As String
    serviceClassCurrent As String
    serviceClassNew As String
End Type
'Public Members() As Node_Details
Public Function Parser(ByVal Whole_String As String, ByVal Delimiter As String) As CCPAI_Parser_Return
    Dim Vars() As String
    Dim Locations(), Step, Loc As Integer
    
    Whole_String = Replace(Whole_String, Delimiter, ";")
    Loc = 1
    'Finding Number Of Vars
        While Loc <> 0
            Loc = InStr(Loc + 1, Whole_String, ";")
            ReDim Preserve Locations(Step)
            Locations(Step) = Loc
            Step = Step + 1
        Wend
    
    Locations(UBound(Locations)) = Len(Whole_String)
    ReDim Preserve Vars(Step - 1)
    
    If Step > 0 Then
        Parser.Found = True
        Vars(0) = Mid(Whole_String, 1, Locations(0) - 1)
        For i = 1 To UBound(Vars) - 1
            If Locations(i - 1) <> Locations(i) Then Vars(i) = Mid(Whole_String, Locations(i - 1) + Len(";"), Locations(i) - Locations(i - 1) - 1)
        Next i
    End If
    Parser.Ret = Vars
    
End Function
Private Function MemberValue(member As Variant, MemberName As String) As String
    For i = 0 To UBound(member)
        If member(i) = MemberName Then MemberValue = member(i + 1)
    Next i
End Function

Public Sub Parsing2(ByRef UCIP_Response As Node_Members)
        'checking for arrays
        'Dim SubMember() As CCPAI_Parser_Return
        Dim SubMember As CCPAI_Parser_Return
        Dim Step, Max_Member_Length As Integer
        Dim Temp As DOMDocument
        For i = 1 To UBound(UCIP_Response.member)
            If UCIP_Response.member(i).Array_ Then      'the member contains sub array
                For j = 0 To UCIP_Response.member(i).SubMember.childNodes.Length - 1
                    'ReDim Preserve SubMember(Step)
                    'SubMember(Step) = Parser(UCIP_Response.Member(I).SubMember.childNodes.Item(j).nodeTypedValue + " ", " ")
                    SubMember = Parser(UCIP_Response.member(i).SubMember.childNodes.Item(j).nodeTypedValue + " ", " ")
                    
                    If UBound(SubMember.Ret) > Max_Member_Length Then Max_Member_Length = UBound(SubMember.Ret)
                    ReDim Preserve UCIP_Response.member(i).SubMembers(NumberOfDedicatedAccounts - 1, Max_Member_Length - 1)
                    For k = 0 To UBound(SubMember.Ret) - 1
                            UCIP_Response.member(i).SubMembers(Step, k) = SubMember.Ret(k)
                    Next k
                    Step = Step + 1
               Next j
               
            End If
        Next i
        
        
        For i = 1 To UBound(UCIP_Response.member)
            Select Case UCIP_Response.member(i).NOdeName
                Case "accumulatorInformation"
                    Dim CurrentAccumulatorID As Integer
                    For j = 1 To NumberOfAccumulators
                        CurrentAccumulatorID = UCIP_Response.member(i).SubMembers(j - 1, 3)
                        'UCIP_Response_Data.Accumulator(CurrentAccumulatorID).accumulatorID = Val(UCIP_Response.member(I).SubMembers(j - 1, 3))
                        UCIP_Response_Data.Accumulator(CurrentAccumulatorID).AccumulatorStartDate = UCIP_Response.member(i).SubMembers(j - 1, 5)
                        UCIP_Response_Data.Accumulator(CurrentAccumulatorID).accumulatorEndDate = UCIP_Response.member(i).SubMembers(j - 1, 1)
                        UCIP_Response_Data.Accumulator(CurrentAccumulatorID).accumulatorValue = Val(UCIP_Response.member(i).SubMembers(j - 1, 7))
                    Next j
                Case "dedicatedAccountInformation"
                    Dim CurrentDedicatedAccountID As Integer
                    For j = 1 To NumberOfDedicatedAccounts
                        CurrentDedicatedAccountID = UCIP_Response.member(i).SubMembers(j - 1, 1)
                        UCIP_Response_Data.DedicatedAccount(CurrentDedicatedAccountID).DedicatedAccountValue = _
                                                                                    Val(UCIP_Response.member(i).SubMembers(j - 1, 3))
                        If Max_Member_Length - 1 > 3 Then UCIP_Response_Data.DedicatedAccount(CurrentDedicatedAccountID).DedicateAccountExpiryDate = UCIP_Response.member(i).SubMembers(j - 1, 5)
                    Next j
                Case "accountActivatedFlag"
                    UCIP_Response_Data.accountActivatedFlag = UCIP_Response.member(i).NodeValue
                Case "accountFlags"
                    UCIP_Response_Data.accountFlags = UCIP_Response.member(i).NodeValue
                Case "accountValue1"
                    UCIP_Response_Data.accountValue1 = UCIP_Response.member(i).NodeValue
                Case "accountValue2"
                    UCIP_Response_Data.accountValue2 = UCIP_Response.member(i).NodeValue
                Case "accountValueAfter1"
                    UCIP_Response_Data.accountValueAfter1 = UCIP_Response.member(i).NodeValue
                Case "accountValueAfter2"
                    UCIP_Response_Data.accountValueAfter2 = UCIP_Response.member(i).NodeValue
                Case "accumulatorInformation"
                    UCIP_Response_Data.accumulatorInformation = UCIP_Response.member(i).NodeValue
                Case "allowedOptions"
                    UCIP_Response_Data.allowedOptions = UCIP_Response.member(i).NodeValue
                Case "chargingResultInformation"
                    UCIP_Response_Data.chargingResultInformation = UCIP_Response.member(i).NodeValue
                Case "charginResultInformation"
                    UCIP_Response_Data.charginResultInformation = UCIP_Response.member(i).NodeValue
                Case "creditClearanceDate"
                    UCIP_Response_Data.creditClearanceDate = UCIP_Response.member(i).NodeValue
                Case "creditClearancePeriod"
                    UCIP_Response_Data.creditClearancePeriod = UCIP_Response.member(i).NodeValue
                Case "currency1"
                    UCIP_Response_Data.currency1 = UCIP_Response.member(i).NodeValue
                Case "currency2"
                    UCIP_Response_Data.currency2 = UCIP_Response.member(i).NodeValue
                Case "currentLanguageID"
                    UCIP_Response_Data.currentLanguageID = UCIP_Response.member(i).NodeValue
                Case "dedicatedAccountInformation"
                    UCIP_Response_Data.dedicatedAccountInformation = UCIP_Response.member(i).NodeValue
                Case "fafChangeUnbarDate"
                    UCIP_Response_Data.fafChangeUnbarDate = UCIP_Response.member(i).NodeValue
                Case "fafInformation"
                    UCIP_Response_Data.fafInformation = UCIP_Response.member(i).NodeValue
                Case "firstIVRCallFlag"
                    UCIP_Response_Data.firstIVRCallFlag = UCIP_Response.member(i).NodeValue
                Case "masterSubscriberFlag"
                    UCIP_Response_Data.masterSubscriberFlag = UCIP_Response.member(i).NodeValue
                Case "maxAllowedNumbers"
                    UCIP_Response_Data.maxAllowedNumbers = UCIP_Response.member(i).NodeValue
                Case "notAllowedReason"
                    UCIP_Response_Data.notAllowedReason = UCIP_Response.member(i).NodeValue
                Case "pinCode"
                    UCIP_Response_Data.pinCode = UCIP_Response.member(i).NodeValue
                Case "promotionAnnouncementCode"
                    UCIP_Response_Data.promotionAnnouncementCode = UCIP_Response.member(i).NodeValue
                Case "rechargeAmount1MainTotal"
                    UCIP_Response_Data.rechargeAmount1MainTotal = UCIP_Response.member(i).NodeValue
                Case "rechargeAmount1PromoTotal"
                    UCIP_Response_Data.rechargeAmount1PromoTotal = UCIP_Response.member(i).NodeValue
                Case "rechargeAmount2MainTotal"
                    UCIP_Response_Data.rechargeAmount2MainTotal = UCIP_Response.member(i).NodeValue
                Case "rechargeAmount2PromoTotal"
                    UCIP_Response_Data.rechargeAmount2PromoTotal = UCIP_Response.member(i).NodeValue
                Case "refillBarredFlag"
                    UCIP_Response_Data.refillBarredFlag = UCIP_Response.member(i).NodeValue
                Case "refillFraudCount"
                    UCIP_Response_Data.refillFraudCount = UCIP_Response.member(i).NodeValue
                Case "removalPeriod"
                    UCIP_Response_Data.removalPeriod = UCIP_Response.member(i).NodeValue
                Case "responseCode"
                    UCIP_Response_Data.ResponseCode = UCIP_Response.member(i).NodeValue
                Case "serviceClassChangeUnbarDate"
                    UCIP_Response_Data.serviceClassChangeUnbarDate = UCIP_Response.member(i).NodeValue
                Case "serviceClassCurrent"
                    UCIP_Response_Data.serviceClassCurrent = UCIP_Response.member(i).NodeValue
                Case "serviceFeeDate"
                    UCIP_Response_Data.serviceFeeDate = UCIP_Response.member(i).NodeValue
                Case "serviceFeeDateAfter"
                    UCIP_Response_Data.serviceFeeDateAfter = UCIP_Response.member(i).NodeValue
                Case "serviceFeeDaysPromoExt"
                    UCIP_Response_Data.serviceFeeDaysPromoExt = UCIP_Response.member(i).NodeValue
                Case "serviceFeeDaysTotalExt"
                    UCIP_Response_Data.serviceFeeDaysTotalExt = UCIP_Response.member(i).NodeValue
                Case "serviceRemovalDate"
                    UCIP_Response_Data.serviceRemovalDate = UCIP_Response.member(i).NodeValue
                Case "supervisionDate"
                    UCIP_Response_Data.supervisionDate = UCIP_Response.member(i).NodeValue
                Case "supervisionDateAfter"
                    UCIP_Response_Data.supervisionDateAfter = UCIP_Response.member(i).NodeValue
                Case "supervisionDaysPromoExt"
                    UCIP_Response_Data.supervisionDaysPromoExt = UCIP_Response.member(i).NodeValue
                Case "supervisionDaysTotalExt"
                    UCIP_Response_Data.supervisionDaysTotalExt = UCIP_Response.member(i).NodeValue
                Case "temporaryBlockedFlag"
                    UCIP_Response_Data.temporaryBlockedFlag = UCIP_Response.member(i).NodeValue
                Case "transactionAmount1Refill"
                    UCIP_Response_Data.transactionAmount1Refill = UCIP_Response.member(i).NodeValue
                Case "transactionAmount2Refill"
                    UCIP_Response_Data.transactionAmount2Refill = UCIP_Response.member(i).NodeValue
                Case "transactionServiceClass"
                    UCIP_Response_Data.transactionServiceClass = UCIP_Response.member(i).NodeValue
                Case "transactionVVPeriodExt"
                    UCIP_Response_Data.transactionVVPeriodExt = UCIP_Response.member(i).NodeValue
                Case "valueVoucherDate"
                    UCIP_Response_Data.valueVoucherDate = UCIP_Response.member(i).NodeValue
                Case "valueVoucherDateAfter"
                    UCIP_Response_Data.valueVoucherDateAfter = UCIP_Response.member(i).NodeValue
                Case "creditClearancePeriod"
                    UCIP_Response_Data.creditClearancePeriod = UCIP_Response.member(i).NodeValue
                Case "removalPeriod"
                    UCIP_Response_Data.removalPeriod = UCIP_Response.member(i).NodeValue
                Case "serviceFeePeriod"
                    UCIP_Response_Data.serviceFeePeriod = UCIP_Response.member(i).NodeValue
                Case "supervisionPeriod"
                    UCIP_Response_Data.supervisionPeriod = UCIP_Response.member(i).NodeValue
            End Select
        Next i
End Sub

Public Sub Parsing(ByRef UCIP_Response As String)
        UCIP_Response_Data.accountActivatedFlag = Values(UCIP_Response, "accountActivatedFlag")
        UCIP_Response_Data.accountFlags = Values(UCIP_Response, "accountFlags")
        UCIP_Response_Data.accountValue1 = Values(UCIP_Response, "accountValue1")
        UCIP_Response_Data.accountValue2 = Values(UCIP_Response, "accountValue2")
        UCIP_Response_Data.accountValueAfter1 = Values(UCIP_Response, "accountValueAfter1")
        UCIP_Response_Data.accountValueAfter2 = Values(UCIP_Response, "accountValueAfter2")
        UCIP_Response_Data.accumulatorInformation = Values(UCIP_Response, "accumulatorInformation")
        UCIP_Response_Data.allowedOptions = Values(UCIP_Response, "allowedOptions")
        UCIP_Response_Data.chargingResultInformation = Values(UCIP_Response, "chargingResultInformation")
        UCIP_Response_Data.charginResultInformation = Values(UCIP_Response, "charginResultInformation")
        UCIP_Response_Data.creditClearanceDate = Values(UCIP_Response, "creditClearanceDate")
        UCIP_Response_Data.creditClearancePeriod = Values(UCIP_Response, "creditClearancePeriod")
        UCIP_Response_Data.currency1 = Values(UCIP_Response, "currency1")
        UCIP_Response_Data.currency2 = Values(UCIP_Response, "currency2")
        UCIP_Response_Data.currentLanguageID = Values(UCIP_Response, "currentLanguageID")
        UCIP_Response_Data.dedicatedAccountInformation = Values(UCIP_Response, "dedicatedAccountInformation")
        UCIP_Response_Data.fafChangeUnbarDate = Values(UCIP_Response, "fafChangeUnbarDate")
        UCIP_Response_Data.fafInformation = Values(UCIP_Response, "fafInformation")
        UCIP_Response_Data.firstIVRCallFlag = Values(UCIP_Response, "firstIVRCallFlag")
        UCIP_Response_Data.masterSubscriberFlag = Values(UCIP_Response, "masterSubscriberFlag")
        UCIP_Response_Data.maxAllowedNumbers = Values(UCIP_Response, "maxAllowedNumbers")
        UCIP_Response_Data.notAllowedReason = Values(UCIP_Response, "notAllowedReason")
        UCIP_Response_Data.pinCode = Values(UCIP_Response, "pinCode")
        UCIP_Response_Data.promotionAnnouncementCode = Values(UCIP_Response, "promotionAnnouncementCode")
        UCIP_Response_Data.rechargeAmount1MainTotal = Values(UCIP_Response, "rechargeAmount1MainTotal")
        UCIP_Response_Data.rechargeAmount1PromoTotal = Values(UCIP_Response, "rechargeAmount1PromoTotal")
        UCIP_Response_Data.rechargeAmount2MainTotal = Values(UCIP_Response, "rechargeAmount2MainTotal")
        UCIP_Response_Data.rechargeAmount2PromoTotal = Values(UCIP_Response, "rechargeAmount2PromoTotal")
        UCIP_Response_Data.refillBarredFlag = Values(UCIP_Response, "refillBarredFlag")
        UCIP_Response_Data.refillFraudCount = Values(UCIP_Response, "refillFraudCount")
        UCIP_Response_Data.removalPeriod = Values(UCIP_Response, "removalPeriod")
        UCIP_Response_Data.ResponseCode = Values(UCIP_Response, "responseCode")
        UCIP_Response_Data.serviceClassChangeUnbarDate = Values(UCIP_Response, "serviceClassChangeUnbarDate")
        UCIP_Response_Data.serviceClassCurrent = Values(UCIP_Response, "serviceClassCurrent")
        UCIP_Response_Data.serviceFeeDate = Values(UCIP_Response, "serviceFeeDate")
        UCIP_Response_Data.serviceFeeDateAfter = Values(UCIP_Response, "serviceFeeDateAfter")
        UCIP_Response_Data.serviceFeeDaysPromoExt = Values(UCIP_Response, "serviceFeeDaysPromoExt")
        UCIP_Response_Data.serviceFeeDaysTotalExt = Values(UCIP_Response, "serviceFeeDaysTotalExt")
        UCIP_Response_Data.serviceRemovalDate = Values(UCIP_Response, "serviceRemovalDate")
        UCIP_Response_Data.supervisionDate = Values(UCIP_Response, "supervisionDate")
        UCIP_Response_Data.supervisionDateAfter = Values(UCIP_Response, "supervisionDateAfter")
        UCIP_Response_Data.supervisionDaysPromoExt = Values(UCIP_Response, "supervisionDaysPromoExt")
        UCIP_Response_Data.supervisionDaysTotalExt = Values(UCIP_Response, "supervisionDaysTotalExt")
        UCIP_Response_Data.temporaryBlockedFlag = Values(UCIP_Response, "temporaryBlockedFlag")
        UCIP_Response_Data.transactionAmount1Refill = Values(UCIP_Response, "transactionAmount1Refill")
        UCIP_Response_Data.transactionAmount2Refill = Values(UCIP_Response, "transactionAmount2Refill")
        UCIP_Response_Data.transactionServiceClass = Values(UCIP_Response, "transactionServiceClass")
        UCIP_Response_Data.transactionVVPeriodExt = Values(UCIP_Response, "transactionVVPeriodExt")
        UCIP_Response_Data.valueVoucherDate = Values(UCIP_Response, "valueVoucherDate")
        UCIP_Response_Data.valueVoucherDateAfter = Values(UCIP_Response, "valueVoucherDateAfter")
        UCIP_Response_Data.creditClearancePeriod = Values(UCIP_Response, "creditClearancePeriod")
        UCIP_Response_Data.removalPeriod = Values(UCIP_Response, "removalPeriod")
        UCIP_Response_Data.serviceFeePeriod = Values(UCIP_Response, "serviceFeePeriod")
        UCIP_Response_Data.supervisionPeriod = Values(UCIP_Response, "supervisionPeriod")
        
        Dim Accumulators_Temp(5) As String
        Dim Pos As Integer
        
        
        
        For i = 1 To NumberOfAccumulators
            UCIP_Response_Data.Accumulator(i).accumulatorValue = Values(UCIP_Response, "dedicatedAccountID " + Trim(Str(i)) + " dedicatedAccountValue1")
            UCIP_Response_Data.DedicatedAccount(i).DedicateAccountExpiryDate = Values(UCIP_Response, "dedicatedAccountID " + Trim(Str(i)) + " dedicatedAccountValue1 " + UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue + " expiryDate")
        Next i
        For i = 1 To NumberOfDedicatedAccounts
            UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue = Values(UCIP_Response, "dedicatedAccountID " + Trim(Str(i)) + " dedicatedAccountValue1")
            UCIP_Response_Data.DedicatedAccount(i).DedicateAccountExpiryDate = Values(UCIP_Response, "dedicatedAccountID " + Trim(Str(i)) + " dedicatedAccountValue1 " + UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue + " expiryDate")
        Next i
End Sub
Public Sub ClearUCIP_Response_Data()
        UCIP_Response_Data.accountActivatedFlag = ""
        UCIP_Response_Data.accountFlags = ""
        UCIP_Response_Data.accountValue1 = ""
        UCIP_Response_Data.accountValue2 = ""
        UCIP_Response_Data.accountValueAfter1 = ""
        UCIP_Response_Data.accountValueAfter2 = ""
        UCIP_Response_Data.accumulatorInformation = ""
        UCIP_Response_Data.allowedOptions = ""
        UCIP_Response_Data.chargingResultInformation = ""
        UCIP_Response_Data.charginResultInformation = ""
        UCIP_Response_Data.creditClearanceDate = ""
        UCIP_Response_Data.creditClearancePeriod = ""
        UCIP_Response_Data.currency1 = ""
        UCIP_Response_Data.currency2 = ""
        UCIP_Response_Data.currentLanguageID = ""
        UCIP_Response_Data.dedicatedAccountInformation = ""
        UCIP_Response_Data.fafChangeUnbarDate = ""
        UCIP_Response_Data.fafInformation = ""
        UCIP_Response_Data.firstIVRCallFlag = ""
        UCIP_Response_Data.masterSubscriberFlag = ""
        UCIP_Response_Data.maxAllowedNumbers = ""
        UCIP_Response_Data.notAllowedReason = ""
        UCIP_Response_Data.pinCode = ""
        UCIP_Response_Data.promotionAnnouncementCode = ""
        UCIP_Response_Data.rechargeAmount1MainTotal = ""
        UCIP_Response_Data.rechargeAmount1PromoTotal = ""
        UCIP_Response_Data.rechargeAmount2MainTotal = ""
        UCIP_Response_Data.rechargeAmount2PromoTotal = ""
        UCIP_Response_Data.refillBarredFlag = ""
        UCIP_Response_Data.refillFraudCount = ""
        UCIP_Response_Data.removalPeriod = ""
        UCIP_Response_Data.ResponseCode = ""
        UCIP_Response_Data.serviceClassChangeUnbarDate = ""
        UCIP_Response_Data.serviceClassCurrent = ""
        UCIP_Response_Data.serviceFeeDate = ""
        UCIP_Response_Data.serviceFeeDateAfter = ""
        UCIP_Response_Data.serviceFeeDaysPromoExt = ""
        UCIP_Response_Data.serviceFeeDaysTotalExt = ""
        UCIP_Response_Data.serviceRemovalDate = ""
        UCIP_Response_Data.supervisionDate = ""
        UCIP_Response_Data.supervisionDateAfter = ""
        UCIP_Response_Data.supervisionDaysPromoExt = ""
        UCIP_Response_Data.supervisionDaysTotalExt = ""
        UCIP_Response_Data.temporaryBlockedFlag = ""
        UCIP_Response_Data.transactionAmount1Refill = ""
        UCIP_Response_Data.transactionAmount2Refill = ""
        UCIP_Response_Data.transactionServiceClass = ""
        UCIP_Response_Data.transactionVVPeriodExt = ""
        UCIP_Response_Data.valueVoucherDate = ""
        UCIP_Response_Data.valueVoucherDateAfter = ""
        UCIP_Response_Data.creditClearancePeriod = ""
        UCIP_Response_Data.removalPeriod = ""
        UCIP_Response_Data.serviceFeePeriod = ""
        UCIP_Response_Data.supervisionPeriod = ""
        
        Dim Accumulators_Temp(5) As String
        Dim Pos As Integer
        
        
        
        For i = 1 To NumberOfAccumulators
            UCIP_Response_Data.Accumulator(i).accumulatorValue = ""
            UCIP_Response_Data.DedicatedAccount(i).DedicateAccountExpiryDate = ""
        Next i
        For i = 1 To NumberOfDedicatedAccounts
            UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue = ""
            UCIP_Response_Data.DedicatedAccount(i).DedicateAccountExpiryDate = ""
        Next i
End Sub
Public Function Parsing_Expert(AIR_Response_XML As IXMLDOMNode, Optional SubNode As Boolean, Optional ACIP As Boolean) As Node_Members
    'ReDim Members(0)
    Dim Number_Of_Members As Integer
    Dim AIR_Response_XML_Members, member As IXMLDOMNode
    Dim ChildNode As String
    'On Error GoTo display
    If Not SubNode Then
        Set AIR_Response_XML_Members = AIR_Response_XML.selectSingleNode("methodResponse")
    Else
        Set AIR_Response_XML_Members = AIR_Response_XML
    End If
    
    While AIR_Response_XML_Members.childNodes.Length < 2
        ChildNode = AIR_Response_XML_Members.childNodes.Item(0).baseName
        Set AIR_Response_XML_Members = AIR_Response_XML_Members.selectSingleNode(ChildNode)
    Wend
    
    Number_Of_Members = AIR_Response_XML_Members.childNodes.Length
    If Number_Of_Members > 2 Or ACIP Then
        ReDim Members(Number_Of_Members) As Node_Details
        
        For i = 0 To Number_Of_Members - 1
            Set member = AIR_Response_XML_Members.childNodes.Item(i)
            Members(i + 1) = Get_Node_Value(member)
        Next i
    Else
        If Number_Of_Members = 2 Then
               ReDim Members(Number_Of_Members - 1) As Node_Details
               Set member = AIR_Response_XML_Members
               Members(1) = Get_Node_Value(member)
        End If
    End If
    ReDim Parsing_Expert.member(Number_Of_Members)
    Parsing_Expert.member = Members
'display:
'    MsgBox Err.Description
End Function
Private Function Get_Node_Value(Node As IXMLDOMNode) As Node_Details
    Dim NodeValue, NOdeName As IXMLDOMNode
    
    If InStr(1, Node.nodeTypedValue, " ") <> 0 Then
        If Node.childNodes.Item(1).childNodes.Item(0).baseName <> "array" Then
            
            Set NOdeName = Node.selectSingleNode("name")
            Set NodeValue = Node.selectSingleNode("value")
            
            With Get_Node_Value
                    .NOdeName = NOdeName.childNodes.Item(0).nodeTypedValue
                    .NodeValue = NodeValue.childNodes.Item(0).nodeTypedValue
            End With
        ElseIf Node.childNodes.Item(1).childNodes.Item(0).baseName = "array" Then
            Get_Node_Value.Array_ = True
            Dim Pos As Integer
            Pos = InStr(1, Node.nodeTypedValue, " ")
            Get_Node_Value.NOdeName = Trim(Left(Node.nodeTypedValue, Pos))
            Set Get_Node_Value.SubMember = Node.childNodes.Item(1).childNodes.Item(0).childNodes.Item(0)
        End If
    End If
End Function
Private Function Values(ByRef Long_Text As String, ByRef Value As String)
        Dim Param_Start, Space_Loc As Integer
        
        Param_Start = InStr(1, Long_Text, Value)
        Space_Loc = InStr(Param_Start + Len(Value) + 1, Long_Text, " ")
        If Param_Start = 0 Then
            Values = ""
        Else
            If Space_Loc < (Param_Start + Len(Value) + 1) Then
                Values = Mid(Long_Text, Param_Start + Len(Value) + 1, Len(Long_Text) - Len(Value) - 1)
                Else
                Values = Mid(Long_Text, Param_Start + Len(Value) + 1, Space_Loc - (Param_Start + Len(Value) + 1))
            End If
        End If
End Function
