Attribute VB_Name = "ACIP_Builder"
Enum MemberType_Enum
    Integer_ = 0
    DateTime_ = 1
    i4 = 2
    boolean_ = 3
    String_ = 4
End Enum

Enum ACIP_MethodName_Enum
    InstallSubscriber = 0
End Enum
Enum BasicACIP_Headders_Enum
    ACIP_Begin1 = 0
    ACIP_Begin2 = 1
    ACIP_Close = 2
End Enum

Public Function ACIP_InstallSubscriber(subscriberNumber As String, ServiceClass As Integer, LanguageID As Integer) As String
    'UCIP.UCIP_Command = BasicACIP_Headders + _

    Dim InstallSubscriber_Command As String
    InstallSubscriber_Command = BasicACIP_Headders(ACIP_Begin1) + _
                                ACIP_MethodName(InstallSubscriber) + _
                                BasicACIP_Headders(ACIP_Begin2) + _
                                Member_Builder("languageIDNew", Integer_, Trim(Str(LanguageID))) + _
                                Member_Builder("serviceClassNew", Integer_, Trim(Str(ServiceClass))) + _
                                Member_Builder("subscriberNumber", String_, subscriberNumber) + _
                                BasicACIP_Headders(ACIP_Close)
                                
    UCIP.UCIP_Combo.Text = "ACIP"
    Call UCIP.BuildComplete_Command(True, InstallSubscriber_Command)
    Call UCIP.Send_Command_Click
    ACIP_InstallSubscriber = UCIP_Response_Data.ResponseCode
End Function
Private Function ACIP_MethodName(MethodName As ACIP_MethodName_Enum) As String
    Select Case MethodName
        Case InstallSubscriber
            ACIP_MethodName = "<methodName>InstallSubscriber</methodName>"
    End Select
End Function
Private Function BasicACIP_Headders(BasicACIP_Headder As BasicACIP_Headders_Enum) As String
    Dim ACIP_Begin1_S, ACIP_Begin2_S, ACIP_Close_S As String
    
    ACIP_Begin1_S = "<?xml version=""1.0""?> " + vbCrLf + _
        "<methodCall>" + vbCrLf
    ACIP_Begin2_S = "<params>" + vbCrLf + _
                        "<param>" + vbCrLf + _
                            "<value>" + vbCrLf + _
                                 "<struct>" + vbCrLf
    ACIP_Begin2_S = ACIP_Begin2_S + _
                    Member_Builder("originTransactionID", String_, "1136807058750") + _
                    Member_Builder("originTimeStamp", DateTime_, TimeStamp) + _
                    Member_Builder("originHostName", String_, SulSAT.Winsock1.LocalHostName) + _
                    Member_Builder("originNodeType", String_, "SDP")
    ACIP_Close_S = "</struct>" + vbCrLf + _
                        "</value>" + vbCrLf + _
                    "</param>" + vbCrLf + _
                "</params>" + vbCrLf + _
            "</methodCall>" + vbCrLf
            
    Select Case BasicACIP_Headder
        Case ACIP_Begin1
            BasicACIP_Headders = ACIP_Begin1_S
        Case ACIP_Begin2
            BasicACIP_Headders = ACIP_Begin2_S
        Case ACIP_Close
            BasicACIP_Headders = ACIP_Close_S
    End Select
End Function
Public Function Member_Builder(MemberName As String, MemberType As MemberType_Enum, MemberValue As String)
    Select Case MemberType
        Case Integer_, i4
            Member_Builder = "<member>" + vbCrLf + _
                                    "<name>" + MemberName + "</name>" + vbCrLf + _
                                        "<value>" + vbCrLf + _
                                            "<i4>" + MemberValue + "</i4>" + vbCrLf + _
                                        "</value>" + vbCrLf + _
                                "</member>" + vbCrLf
        Case boolean_
            Member_Builder = "<member>" + vbCrLf + _
                                    "<name>" + MemberName + "</name>" + vbCrLf + _
                                        "<value>" + vbCrLf + _
                                            "<boolean>" + MemberValue + "</boolean>" + vbCrLf + _
                                        "</value>" + vbCrLf + _
                                "</member>" + vbCrLf
        Case String_
            Member_Builder = "<member>" + vbCrLf + _
                                    "<name>" + MemberName + "</name>" + vbCrLf + _
                                        "<value>" + vbCrLf + _
                                            "<string>" + MemberValue + "</string>" + vbCrLf + _
                                        "</value>" + vbCrLf + _
                                "</member>" + vbCrLf
        Case DateTime_
            Member_Builder = "<member>" + vbCrLf + _
                                    "<name>" + MemberName + "</name>" + vbCrLf + _
                                        "<value>" + vbCrLf + _
                                            "<dateTime.iso8601>" + MemberValue + "</dateTime.iso8601>" + vbCrLf + _
                                        "</value>" + vbCrLf + _
                                "</member>" + vbCrLf
    End Select
End Function
