VERSION 5.00
Begin VB.Form UCIP 
   Caption         =   "UCIP Command Creation"
   ClientHeight    =   8340
   ClientLeft      =   1230
   ClientTop       =   1785
   ClientWidth     =   10665
   Icon            =   "UCIP.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8340
   ScaleWidth      =   10665
   Begin VB.Frame Frame6 
      Caption         =   "Configurations"
      Height          =   735
      Left            =   5160
      TabIndex        =   49
      Top             =   0
      Width           =   5055
      Begin VB.ComboBox AIR_IP 
         Height          =   315
         Left            =   420
         TabIndex        =   52
         Top             =   240
         Width           =   1575
      End
      Begin VB.ComboBox AIR_Port 
         Height          =   315
         Left            =   4080
         TabIndex        =   51
         Top             =   240
         Width           =   855
      End
      Begin VB.ComboBox AIR_Name 
         Height          =   315
         Left            =   2500
         TabIndex        =   50
         Top             =   240
         Width           =   1250
      End
      Begin VB.Label Label24 
         Caption         =   "AIR   IP"
         Height          =   375
         Left            =   120
         TabIndex        =   55
         Top             =   300
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "AIR  Port"
         Height          =   375
         Left            =   3760
         TabIndex        =   54
         Top             =   300
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "AIR Name"
         Height          =   375
         Left            =   2050
         TabIndex        =   53
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.CommandButton Extend_Command 
      Caption         =   ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      Height          =   8055
      Left            =   10320
      TabIndex        =   48
      Top             =   120
      Width           =   255
   End
   Begin VB.Frame Frame3 
      Caption         =   "UCIP Rersponse"
      Height          =   9135
      Left            =   10680
      TabIndex        =   39
      Top             =   0
      Width           =   6135
      Begin VB.TextBox txtUCIPResponse 
         Height          =   5175
         Left            =   240
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   40
         Top             =   3000
         Width           =   5775
      End
      Begin VB.Label Label5 
         Caption         =   "Interval"
         Height          =   255
         Left            =   240
         TabIndex        =   46
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblUCIPRespInterval 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Left            =   1440
         TabIndex        =   45
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label lblResponseCode 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   44
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "Response Code"
         Height          =   255
         Left            =   240
         TabIndex        =   43
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label lblResponseText 
         BorderStyle     =   1  'Fixed Single
         Height          =   1455
         Left            =   240
         TabIndex        =   42
         Top             =   1320
         Width           =   5655
      End
      Begin VB.Label Label8 
         Caption         =   "Response Text"
         Height          =   255
         Left            =   240
         TabIndex        =   41
         Top             =   1080
         Width           =   1215
      End
   End
   Begin VB.CommandButton Send_Command 
      Caption         =   "Send Command"
      Height          =   375
      Left            =   8640
      TabIndex        =   38
      Top             =   1200
      Width           =   1575
   End
   Begin VB.CommandButton Clear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   7320
      TabIndex        =   36
      Top             =   1200
      Width           =   1095
   End
   Begin VB.CommandButton Copy 
      Caption         =   "Copy"
      Height          =   375
      Left            =   3840
      TabIndex        =   35
      Top             =   7920
      Width           =   1215
   End
   Begin VB.Frame Optional 
      Caption         =   "Optional"
      Height          =   3855
      Left            =   5160
      TabIndex        =   4
      Top             =   4320
      Width           =   5055
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   8
         Left            =   2880
         TabIndex        =   28
         Top             =   3240
         Width           =   1815
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   495
         Index           =   8
         Left            =   120
         TabIndex        =   27
         Top             =   3240
         Width           =   2655
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   7
         Left            =   2880
         TabIndex        =   26
         Top             =   2760
         Width           =   1815
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   495
         Index           =   7
         Left            =   120
         TabIndex        =   25
         Top             =   2760
         Width           =   2655
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   24
         Top             =   600
         Width           =   2655
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   6
         Left            =   120
         TabIndex        =   23
         Top             =   2400
         Width           =   2655
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   5
         Left            =   120
         TabIndex        =   22
         Top             =   2040
         Width           =   2655
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   21
         Top             =   1680
         Width           =   2655
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   20
         Top             =   1320
         Width           =   2655
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   2655
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   6
         Left            =   2880
         TabIndex        =   18
         Top             =   2400
         Width           =   1815
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   5
         Left            =   2880
         TabIndex        =   17
         Top             =   2040
         Width           =   1815
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   4
         Left            =   2880
         MultiLine       =   -1  'True
         TabIndex        =   16
         Top             =   1680
         Width           =   1815
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   3
         Left            =   2880
         TabIndex        =   15
         Top             =   1320
         Width           =   1815
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   2
         Left            =   2880
         TabIndex        =   14
         Top             =   960
         Width           =   1815
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   1
         Left            =   2880
         TabIndex        =   13
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox Text_Optional 
         Height          =   285
         Index           =   0
         Left            =   2880
         TabIndex        =   12
         Top             =   240
         Width           =   1815
      End
      Begin VB.CheckBox Check_Optional 
         Caption         =   "subscriberNumberNAI"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   2655
      End
   End
   Begin VB.Frame Mandatory 
      Caption         =   "Mandatory"
      Height          =   2535
      Left            =   5160
      TabIndex        =   3
      Top             =   1680
      Width           =   5055
      Begin VB.CommandButton LE_Command 
         Appearance      =   0  'Flat
         Caption         =   ">"
         Height          =   255
         Left            =   4800
         TabIndex        =   47
         Top             =   620
         Width           =   215
      End
      Begin VB.ComboBox Combo_MSISDNs 
         Height          =   315
         Left            =   2760
         TabIndex        =   37
         Text            =   "Combo1"
         Top             =   240
         Width           =   2055
      End
      Begin VB.TextBox Text_Mandatory 
         Height          =   285
         Index           =   5
         Left            =   2760
         TabIndex        =   11
         Top             =   2040
         Width           =   2055
      End
      Begin VB.TextBox Text_Mandatory 
         Height          =   285
         Index           =   4
         Left            =   2760
         TabIndex        =   10
         Top             =   1680
         Width           =   2055
      End
      Begin VB.TextBox Text_Mandatory 
         Height          =   285
         Index           =   3
         Left            =   2760
         TabIndex        =   9
         Top             =   1320
         Width           =   2055
      End
      Begin VB.TextBox Text_Mandatory 
         Height          =   285
         Index           =   2
         Left            =   2760
         TabIndex        =   8
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox Text_Mandatory 
         Height          =   285
         Index           =   1
         Left            =   2760
         TabIndex        =   7
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox Text_Mandatory 
         Height          =   285
         Index           =   0
         Left            =   2760
         TabIndex        =   6
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Check_Mandatory 
         Caption         =   "Label1"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   34
         Top             =   2040
         Width           =   2415
      End
      Begin VB.Label Check_Mandatory 
         Caption         =   "Label1"
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   33
         Top             =   1680
         Width           =   2415
      End
      Begin VB.Label Check_Mandatory 
         Caption         =   "Label1"
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   32
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Label Check_Mandatory 
         Caption         =   "Label1"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   31
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label Check_Mandatory 
         Caption         =   "Label1"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   30
         Top             =   600
         Width           =   2415
      End
      Begin VB.Label Check_Mandatory 
         Caption         =   "Label1"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.ComboBox UCIP_Combo 
      Height          =   315
      Left            =   5160
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   840
      Width           =   4095
   End
   Begin VB.TextBox UCIP_Command 
      Height          =   7815
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   120
      Width           =   4935
   End
   Begin VB.CommandButton Generate 
      Caption         =   "Load"
      Height          =   375
      Left            =   5160
      TabIndex        =   0
      Top             =   1200
      Width           =   1935
   End
End
Attribute VB_Name = "UCIP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Header As String
Dim Extended As Boolean

Private Sub AIR_IP_Change()
    SulSAT.AIR_IP.ListIndex = Me.AIR_IP.ListIndex
    SulSAT.AIR_Port.ListIndex = Me.AIR_IP.ListIndex
    Me.AIR_Name.ListIndex = Me.AIR_IP.ListIndex
    Me.AIR_Port.ListIndex = Me.AIR_IP.ListIndex
End Sub
Private Sub AIR_IP_LostFocus()
    Call Save_List(Me.AIR_IP, "AIR_IPs")
    Call AIR_IP_Change
End Sub

Private Sub Check_Optional_Click(Index As Integer)
    If UCIP.Visible And Check_Optional(Index).Caption = "dedicatedAccountInformation" And UCIP_Combo.Text = "AdjustmentT" _
        And Check_Optional(Index).Value = 1 Then
        With Extention
            .Visible = True
            .Caption = "Dedicated Account Information"
            .Check_Optional(0).Caption = "dedicatedAccountID"
            .Text_Optional_EXT(0) = "1"
            .Check_Optional(0).Value = 0
            .Check_Optional(1).Caption = "adjustmentAmount"
             .Text_Optional_EXT(1) = ""
            .Check_Optional(1).Value = 0
            .Check_Optional(2).Caption = "relativeDateAdjustment"
             .Text_Optional_EXT(2) = "+/- XXXX"
            .Check_Optional(2).Value = 0
            .Check_Optional(3).Visible = True
            .Text_Optional_EXT(3).Visible = True
            .Check_Optional(3).Caption = "newExpiryDate"
             .Text_Optional_EXT(3) = "dd/mm/yyyy"
            .Check_Optional(3).Value = 0
        End With
    End If
    
    If Check_Optional(Index).Caption = "dedicatedAccountInformation" And UCIP_Combo.Text = "AdjustmentT" _
        And Check_Optional(Index).Value = 0 Then Extention.Visible = False
    
End Sub

Private Sub Clear_Click()
    For i = 0 To Number_Of_Mandatory_Options
        Text_Mandatory(i) = ""
    Next i
    For i = 0 To Number_Of_Optional_Options
        Text_Optional(i) = ""
        Check_Optional(i).Value = 0
    Next i
    
    Combo_MSISDNs.Text = ""
End Sub

Private Sub Extend_Command_Click()
    Select Case Extended
        Case False
            Extend_Command.Caption = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            Me.Width = 17070
            Me.Left = 1170
        Case True
            Extend_Command.Caption = "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
            Me.Width = 10785
            Me.Left = 1170 + (17070 - 10785) / 2
    End Select
    Extended = Not Extended
End Sub

Private Sub Form_Terminate()
   Call Slide(Me, Me.Left, Me.Width, False)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub
Public Sub BuildComplete_Command(Optional ACIP As Boolean, Optional ACIP_Command As String)
   
    If Not ACIP Then
        Text_Mandatory(0) = Combo_MSISDNs.Text
        UCIP_Command = ""
        Call Initiate
        UCIP_Begin2 = UCIP_Begin2 + _
            originNodeType + originHostName + originTransactionID + originTimeStamp
    
        Select Case UCIP_Combo.Text
                           
            Case "AccumulatorEnquiryT"
                UCIP_Command = UCIP_Begin1 + AccumulatorEnquiryTRequest + UCIP_Begin2
            Case "AdjustmentT"
                UCIP_Command = UCIP_Begin1 + AdjustmentTRequest + UCIP_Begin2
            Case "BalanceEnquiryT"
                UCIP_Command = UCIP_Begin1 + BalanceEnquiryTRequest + UCIP_Begin2
            Case "GetAccountDetailsT"
                UCIP_Command = UCIP_Begin1 + GetAccountDetailsTRequest + UCIP_Begin2
            Case "GetFaFListT"
                UCIP_Command = UCIP_Begin1 + GetFaFListTRequest + UCIP_Begin2
            Case "GetVoucherRefillOptionsT"
            Case "RefillT"
                UCIP_Command = UCIP_Begin1 + RefillTRequest + UCIP_Begin2
            Case "StandardVoucherRefillT"
                UCIP_Command = UCIP_Begin1 + StandardVoucherRefillTRequest + UCIP_Begin2
            Case "UpdateAccountDetailsT"
                UCIP_Command = UCIP_Begin1 + UpdateAccountDetailsT + UCIP_Begin2
            Case "UpdateFaFListT"
                UCIP_Command = UCIP_Begin1 + UpdateAccountDetailsT + UCIP_Begin2
            Case "UpdateServiceClassT"
                UCIP_Command = UCIP_Begin1 + UpdateServiceClassT + UCIP_Begin2
            Case "ValueVoucherEnquiryT"
                UCIP_Command = UCIP_Begin1 + ValueVoucherEnquiryT + UCIP_Begin2
            Case "ValueVoucherRefillT"
                UCIP_Command = UCIP_Begin1 + ValueVoucherRefillT + UCIP_Begin2
            Case Else
                UCIP_Command = "This command is under construction...." + vbCrLf + vbCrLf + vbCrLf + _
                            "PLZ check later........."
        End Select
        
        If UCIP_Command = "" Then
            UCIP_Command = "This command is under construction...." + vbCrLf + vbCrLf + vbCrLf + _
                "PLZ check later........."
        Else
            For i = 0 To Number_Of_Optional_Options
                If Check_Optional(i).Value = 1 Then  'and Check_Optional(i).Visible = True Then
                    UCIP_Command = UCIP_Command + Filling(Check_Optional(i).Caption, Text_Optional(i))
                End If
            Next i
            
            
            Dim Field_Missing As Boolean
            
            For i = 0 To Number_Of_Mandatory_Options
                If Check_Mandatory(i).Visible = True Then
                    If Text_Mandatory(i) <> "" Then
                        If In_LE.LE.Value = True Then
                            Select Case UCIP_Combo.Text
                                Case "RefillT"
                                    Text_Mandatory(1) = Val(Text_Mandatory(1)) * 100
                                    UCIP_Command = UCIP_Command + Filling(Check_Mandatory(i).Caption, Text_Mandatory(i))
                                    Text_Mandatory(1) = Val(Text_Mandatory(1)) / 100
                                Case "AdjustmentT"
                                    Text_Mandatory(2) = Val(Text_Mandatory(2)) * 100
                                    UCIP_Command = UCIP_Command + Filling(Check_Mandatory(i).Caption, Text_Mandatory(i))
                                    Text_Mandatory(2) = Val(Text_Mandatory(2)) / 100
                                Case Else
                                    UCIP_Command = UCIP_Command + Filling(Check_Mandatory(i).Caption, Text_Mandatory(i))
                            End Select
                            Else
                            UCIP_Command = UCIP_Command + Filling(Check_Mandatory(i).Caption, Text_Mandatory(i))
                        End If
                    Else
                        UCIP_Command = "Error..." + vbCrLf + vbCrLf + "Missing Field(s)...."
                        MsgBox "You must fill the field number " + Str(i + 1), , "Missing Madatory Field"
                        Field_Missing = True
                    End If
                End If
            Next i
        End If
        If Field_Missing = False Then
        
            UCIP_Command = UCIP_Command + UCIP_Close
            
            Header = "POST /Air" + vbCrLf + _
                    "User-Agent: UGw Server/2.3/1.0" + vbCrLf + _
                    "Host: SulSat" + vbCrLf + _
                    "Content-Type: text/xml" + vbCrLf + _
                    "Content-length:" + Str(Len(UCIP_Command)) + vbCrLf + _
                    "Authorization: Basic U3lzQWRtOlN5c0FkbQ==" + vbCrLf
                    
            UCIP_Command = Header + vbCrLf + UCIP_Command
        End If
    Else
        'UCIP_Command = ACIP_Command
        Header = "POST /Air" + vbCrLf + _
                    "User-Agent: UGw Server/3.1/1.0" + vbCrLf + _
                    "Host: SulSat" + vbCrLf + _
                    "Content-Type: text/xml" + vbCrLf + _
                    "Content-length:" + Str(Len(ACIP_Command)) + vbCrLf + _
                    "Authorization: Basic U3lzQWRtOlN5c0FkbQ==" + vbCrLf
                    
        UCIP_Command = Header + vbCrLf + ACIP_Command
    End If
End Sub
Public Sub Generate_Click()
    Call BuildComplete_Command
End Sub

Private Sub Load_Combo()
    Dim Commands(13) As String

    Commands(0) = "AccumulatorEnquiryT"
    Commands(1) = "AdjustmentT"
    Commands(2) = "BalanceEnquiryT"
    Commands(3) = "GetAccountDetailsT"
    Commands(4) = "GetFaFListT"
    Commands(5) = "GetVoucherRefillOptionsT"
    Commands(6) = "RefillT"
    Commands(7) = "StandardVoucherRefillT"
    Commands(8) = "UpdateAccountDetailsT"
    Commands(9) = "UpdateFaFListT"
    Commands(10) = "UpdateServiceClassT"
    Commands(11) = "ValueVoucherEnquiryT"
    Commands(12) = "ValueVoucherRefillT"
    Commands(13) = "ACIP"
    
    For i = 0 To UBound(Commands)
        UCIP_Combo.AddItem Commands(i)
    Next i

    UCIP_Combo.Text = UCIP_Combo.List(0)
End Sub

Private Sub Copy_Click()
    Clipboard.SetText UCIP_Command.Text
End Sub

Public Sub Form_Load()
    Call Load_List(Combo_MSISDNs, "MSISDNs")
    Number_Of_Mandatory_Options = 5
    Number_Of_Optional_Options = 8
    Load_Combo
   ' Call Load_AIR_IPs(Me.AIR_IP)
End Sub
Private Sub AIR_Next()
    If Me.AIR_IP.ListIndex + 1 > Me.AIR_IP.ListCount - 1 Then
        Me.AIR_IP.ListIndex = 0
    Else
        Me.AIR_IP.ListIndex = Me.AIR_IP.ListIndex + 1
    End If
End Sub
Private Sub AIR_Name_Click()
    Me.AIR_IP.ListIndex = (AIR_Name.ListIndex)
    Me.AIR_Port.ListIndex = (AIR_Name.ListIndex)
End Sub
Private Sub AIR_IP_Click()
    Call AIR_IP_Change
End Sub
Private Sub AIR_Port_Click()
    Me.AIR_IP.ListIndex = (AIR_Port.ListIndex)
    Me.AIR_Name.ListIndex = (AIR_Port.ListIndex)
End Sub
Private Sub LE_Command_Click()
    In_LE.Visible = True
    If In_LE.LE.Value = True Then
        Select Case UCIP_Combo.Text
            Case "RefillT"
                In_LE.Top = UCIP.Top + Mandatory.Top + 300 + Text_Mandatory(1).Top
            Case "AdjustmentT"
                In_LE.Top = UCIP.Top + Mandatory.Top + 400 + Text_Mandatory(1).Top + Text_Mandatory(1).Height
        End Select
    End If
    In_LE.Left = Mandatory.Left + Mandatory.Width + UCIP.Left + 100
End Sub

Public Sub Send_Command_Click()
    'UCIP_Response_Data = Nothing
    Dim Ctr As Control
    
    For Each Ctr In SulSAT
        If TypeOf Ctr Is CommandButton Or _
            TypeOf Ctr Is TextBox Or _
            TypeOf Ctr Is ComboBox Then Ctr.Enabled = False
    Next Ctr
    Call SulSAT.User_Security_Level
    
    Call ClearUCIP_Response_Data
    
    If Check_Connectivity = 0 Then
        MsgBox "Sorry you can't connect since there is no local connection"
        Else
        'If Mid(AIR_IP, 1, 3) = "172" Then
        '    If (IsProcessRunning("ipsecdialer.exe") Or IsProcessRunning("vpngui.exe")) Then      'Connected
        '        Call UCIP_Connect
        '    Else
        '        Dim Connect_ As Integer
        '        Connect_ = MsgBox("Sorry You Are Not Connected by VPN, Connect Now?", vbOKCancel, "VPN Error")
        '            If Connect_ = 1 Then
        '                Call Main.VPN_Connect_Click
        '                Wait 1000
        '                Call Me.Send_Command_Click
        '            End If
        '    End If
        'Else
            If AIR_IP <> "" Then
                If UCIP_Combo = "ACIP" Then
                    Call UCIP_Connect(True)
                Else
                    Call UCIP_Connect
                End If
            End If
        'End If
    End If
    
    For Each Ctr In SulSAT
        If TypeOf Ctr Is CommandButton Or _
            TypeOf Ctr Is TextBox Or _
            TypeOf Ctr Is ComboBox Then Ctr.Enabled = True
    Next Ctr
    
    Call SulSAT.User_Security_Level
End Sub
Private Sub UCIP_Connect(Optional ACIP As Boolean)
            'Clearing UCIP response data
            Call ClearUCIP_Response_Data
            
            Dim airReq As ServerXMLHTTP30 'XMLHTTP
            Dim ucipCommand As New DOMDocument
            Dim UCIP_Response As New DOMDocument
            Dim bret As Boolean
            'Dim lret As Long
            Dim url As String
            Dim headerStr As String
            Dim headerPairs() As String
            Dim headerPairItems() As String
            Dim cntr As Integer
            Dim sTime As Long
            Dim eTime As Long
        
            'AIR Request
            If Not (airReq Is Nothing) Then
                Set airReq = Nothing
            End If
            Set airReq = New ServerXMLHTTP30
        
        
            'url = "http://172.30.5.31:10010/Air"
            url = "http://" + Trim(AIR_IP) + ":" + Trim(AIR_Port) + "/Air"
            
            'Header
            headerStr = Header      'txtUCIPRequestHeader.Text
            headerPairs = Split(headerStr, vbCrLf)
        
            Call airReq.open("POST", url, False)
            For cntr = 1 To UBound(headerPairs)
                If (headerPairs(cntr) <> "") Then
                    headerPairItems = Split(headerPairs(cntr), ":")
                    Call airReq.setRequestHeader(headerPairItems(0), headerPairItems(1))
                End If
            Next
            
            Dim Start_Body As Integer
            Dim UCIP_Command_Trimmed As String
            
            Start_Body = InStr(1, UCIP_Command, "<?xml version=")
            UCIP_Command_Trimmed = Mid(UCIP_Command, Start_Body, Len(UCIP_Command) - Start_Body)
            
            'Body
            bret = ucipCommand.loadXML(UCIP_Command_Trimmed)
        
            sTime = GetTickCount()
            Call airReq.setTimeouts(100, 2000, 2000, 10000)
            On Error GoTo SulSat_Handler
            Call airReq.send(ucipCommand.xml & vbCrLf & vbCrLf)
            eTime = GetTickCount()
        
            'Display Response
            lblUCIPRespInterval.Caption = CStr(eTime - sTime)
            
            If airReq.readyState = 4 Then
                lblResponseCode.Caption = CStr(airReq.Status)
                lblResponseText.Caption = CStr(airReq.responseText)
                txtUCIPResponse.Text = airReq.responseXML.xml
                UCIP_Response.loadXML (airReq.responseXML.xml)
                'UCIP_response.
                'X = UCIP_response.Text
                Last_UCIP_Response = UCIP_Response.Text
                Dim Members As Node_Members
                Members = Parsing_Expert(UCIP_Response, , ACIP)
                Call Parsing2(Members)
                'Call Parsing(UCIP_Response.Text)
            End If
            
            Call AIR_Next
SulSat_Handler:
        If Err.Number = -2147012894 Then
            MsgBox "Connection Error"
        Else
            Resume Next
        End If
End Sub
Private Sub test_Click()
    SulSAT.Show
End Sub

Private Sub Text_Mandatory_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call Generate_Click
End Sub

Private Sub Text_Optional_Change(Index As Integer)
    Check_Optional(Index).Value = 1
End Sub

Private Sub Text_Optional_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call Generate_Click
End Sub

Public Sub UCIP_Combo_Change()
    Hide_all
    
    Select Case UCIP_Combo.Text
        Case "AccumulatorEnquiryT"
            Call Show_Checks("Optional", 2)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "messageCapabilityFlag"
            Text_Optional(1) = "01100000"
            Check_Optional(1).Value = 0
            
        Case "AdjustmentT"
            Call Show_Checks("Optional", 7)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "messageCapabilityFlag"
            Text_Optional(1) = "01100000"
            Check_Optional(1).Value = 0
            Check_Optional(2).Caption = "dedicatedAccountInformation"
            Text_Optional(2) = ""
            Check_Optional(2).Value = 0
            Extention.Visible = False
            Check_Optional(3).Caption = "externalData1"
            Check_Optional(4).Caption = "externalData2"
            Check_Optional(5).Caption = "relativeDateAdjustment Supervision"
            Check_Optional(6).Caption = "relativeDateAdjustment ServiceFee"
            Call Show_Checks("Mandatory", 3)
            Check_Mandatory(1).Caption = "transactionCurrency"
            Text_Mandatory(1) = "EGP"
            Check_Mandatory(2).Caption = "adjustmentAmount"
            Text_Mandatory(2) = "0"
            
            LE_Command.Top = 680 + Text_Mandatory(1).Height
            
            LE_Command.Visible = True
            
        Case "BalanceEnquiryT"
            Call Show_Checks("Optional", 3)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "messageCapabilityFlag"
            Text_Optional(1) = "01100000"
            Check_Optional(1).Value = 0
            Check_Optional(2).Caption = "chargingRequestInformation"
            Check_Optional(2).Enabled = False
            Text_Optional(2).Enabled = False
            
        Case "GetAccountDetailsT"
            Call Show_Checks("Optional", 3)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "messageCapabilityFlag"
            Text_Optional(1) = "01100000"
            Check_Optional(1).Value = 0
            Check_Optional(2).Caption = "requestedInformation"
            Check_Optional(2).Enabled = False
            Text_Optional(2).Enabled = False
            
        Case "GetFaFListT"
            Call Show_Checks("Optional", 1)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Call Show_Checks("mandatory", 2)
            Check_Mandatory(1).Caption = "requestedOwner"
            Text_Mandatory(1) = "1"
            
        Case "GetVoucherRefillOptionsT"
        
        Case "RefillT"
            Call Show_Checks("Optional", 3)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "externalData1"
            Check_Optional(2).Caption = "externalData2"
            LE_Command.Visible = True
            LE_Command.Top = 620
            
            Call Show_Checks("Mandatory", 4)
            Check_Mandatory(1).Caption = "transactionAmountRefill"
            Check_Mandatory(2).Caption = "transactionCurrency"
            Text_Mandatory(2) = "EGP"
            Check_Mandatory(3).Caption = "paymentProfileID"
            
        Case "StandardVoucherRefillT"
            Call Show_Checks("Optional", 7)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "messageCapabilityFlag"
            Text_Optional(1) = "01100000"
            Check_Optional(1).Value = 0
            Check_Optional(2).Caption = "externalData1"
            Check_Optional(3).Caption = "externalData2"
            Check_Optional(4).Caption = "locationNumber"
            Check_Optional(5).Caption = "locationNumberNAI"
            Check_Optional(6).Caption = "selectedOption"
            
            Call Show_Checks("Mandatory", 2)
            Check_Mandatory(1).Caption = "activationNumber"
            
        Case "UpdateAccountDetailsT"
            Call Show_Checks("Optional", 7)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "currentLanguageID"
            Text_Optional(1) = "1"
            Check_Optional(1).Value = 0
            Check_Optional(2).Caption = "newLanguageID"
            Text_Optional(2) = "2"
            Check_Optional(2).Value = 0
            Check_Optional(3).Caption = "firstIVRCallDone"
            Check_Optional(4).Caption = "externalData1"
            Check_Optional(5).Caption = "externalData2"
            Check_Optional(6).Caption = "pinCode"
            Check_Optional(6).Enabled = False
            Text_Optional(6).Enabled = False
            
        Case "UpdateFaFListT"
            Call Show_Checks("Optional", 3)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "chargingRequestInformation"
            Check_Optional(2).Caption = "selectedOption"
            With Extention
                .Visible = True
                .Check_Optional(0).Caption = "fafNumber"
                .Text_Optional_EXT(0) = "XXXXXXXXX"
                .Check_Optional(1).Caption = "fafIndicator"
                .Text_Optional_EXT(1) = "400"
                .Check_Optional(2).Caption = "owner"
                .Text_Optional_EXT(2) = "1"
                .Check_Optional(3).Visible = False
                .Text_Optional_EXT(3).Visible = False
            End With
            Call Show_Checks("Mandatory", 4)
            Check_Mandatory(1).Caption = "fafAction"
            Check_Mandatory(2).Caption = "fafInformation"
            Check_Mandatory(3).Caption = "requestedOwner"
            Text_Mandatory(3) = "1"
            
        Case "UpdateServiceClassT"
            Call Show_Checks("Optional", 3)
            Check_Optional(0).Caption = "subscriberNumberNAI"
            Text_Optional(0) = "2"
            Check_Optional(0).Value = 0
            Check_Optional(1).Caption = "chargingRequestInformation"
            Check_Optional(1).Enabled = False
            Text_Optional(1).Enabled = False
            Check_Optional(2).Caption = "performValidation"
            Check_Optional(2).Enabled = False
            Text_Optional(2).Enabled = False
            
            Call Show_Checks("Mandatory", 3)
            Check_Mandatory(1).Caption = "serviceClassCurrent"
            Check_Mandatory(2).Caption = "serviceClassNew"
            
        Case "ValueVoucherEnquiryT"
        Case "ValueVoucherRefillT"
    End Select
    
    Call Show_Checks("mandatory", 1)
    Check_Mandatory(0).Caption = "subscriberNumber"
End Sub

Public Sub Hide_all()
With UCIP
    For i = 0 To Number_Of_Mandatory_Options
        .Check_Mandatory(i).Visible = False
        .Check_Mandatory(i).Enabled = True
        .Text_Mandatory(i).Visible = False
        .Text_Mandatory(i).Enabled = True
        .Text_Mandatory(i) = ""
    Next i
    
    For i = 0 To Number_Of_Optional_Options
        .Check_Optional(i).Visible = False
        .Check_Optional(i).Enabled = True
        .Text_Optional(i).Enabled = True
        .Text_Optional(i) = ""
        .Check_Optional(i).Value = 0
        .Text_Optional(i).Visible = False
    Next i
End With
    In_LE.Visible = False
    LE_Command.Visible = False
    Extention.Visible = False
End Sub
Public Sub Show_Checks(ByVal Mandatory_Or_Optional As String, ByVal Number_Visible_Checks As Integer)
With UCIP
    Dim MandatoryOrOptional As String
    MandatoryOrOptional = LCase(Mandatory_Or_Optional)
    
    Select Case MandatoryOrOptional
        Case "mandatory"
            For i = 0 To Number_Visible_Checks - 1
                .Check_Mandatory(i).Visible = True
                .Text_Mandatory(i).Visible = True
            Next i
        Case "optional"
            For i = 0 To Number_Visible_Checks - 1
                .Check_Optional(i).Visible = True
                .Text_Optional(i).Visible = True
            Next i
        End Select
End With
End Sub

Private Sub UCIP_Combo_Click()
    Call UCIP_Combo_Change
End Sub


Private Sub combo_msisdns_GotFocus()
    SendMessage Combo_MSISDNs.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub combo_msisdns_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = Combo_MSISDNs.Text
        For i = 0 To Combo_MSISDNs.ListCount - 1
            If StrComp(PSTR, (Left(Combo_MSISDNs.List(i), Len(PSTR))), vbTextCompare) = 0 Then
                Combo_MSISDNs.ListIndex = i
            Exit For
            End If
        Next i
        Combo_MSISDNs.SelStart = Len(PSTR)
        Combo_MSISDNs.SelLength = Len(Combo_MSISDNs.Text) - Len(PSTR)
    End If
End Sub

Private Sub combo_msisdns_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(Combo_MSISDNs.Text) = 0) Then
        SendMessage Combo_MSISDNs.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage Combo_MSISDNs.hwnd, CB_SHOWDROPDOWN, 0, 1
    End If
    If KeyAscii = 32 And Len(Combo_MSISDNs.Text) = 0 Then KeyAscii = 0
    
End Sub

Private Sub combo_msisdns_LostFocus()
'    Text_Mandatory(0) = Combo_MSISDNs.Text
    Call Save_List(Combo_MSISDNs, "MSISDNS", True)
    Call Roll_Out_Msisdns(Combo_MSISDNs)
End Sub

