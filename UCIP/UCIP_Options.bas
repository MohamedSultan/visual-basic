Attribute VB_Name = "UCIP_Options"

Public originTransactionID, originTimeStamp, originHostName, _
        originNodeType, subscriberNumber, UCIP_Begin1, UCIP_Begin2, UCIP_Close, subscriberNumberNAI, _
            messageCapabilityFlag, transactionCurrency, adjustmentAmount, dedicatedAccountInformation, _
            externalData1, externalData2, currentLanguageID, newLanguageID, firstIVRCallDone, pinCode, _
            DateAdjustmentSupervision, relativeDateAdjustmentServiceFee, _
            chargingRequestInformation, selectedOption, fafAction, fafInformation, performValidation, _
            dedicatedAccountID, newExpiryDate, relativeDateAdjustment, requestedInformation, TransactionAmountRefill, _
            requestedOwner, paymentProfileID, fafNumber, fafIndicator, Owner, activationNumber As String
            
Public AccumulatorEnquiryTRequest, AdjustmentTRequest, BalanceEnquiryTRequest, GetAccountDetailsTRequest, _
        GetFaFListTRequest, RefillTRequest, StandardVoucherRefillTRequest, UpdateAccountDetailsT, _
        UpdateFaFTRequest, UpdateServiceClassT, ValueVoucherEnquiryT, ValueVoucherRefillT          As String
        
Public Number_Of_Optional_Options, Number_Of_Mandatory_Options As Integer


Public Sub Initiate()
    'each Variable finishes with an enter
    
    Call UCIP_Methods
    
    UCIP_Begin1 = "<?xml version=""1.0""?> " + vbCrLf + _
                    "<methodCall>" + vbCrLf
    
    UCIP_Begin2 = "<params>" + vbCrLf + _
                        "<param>" + vbCrLf + _
                            "<value>" + vbCrLf + _
                                 "<struct>" + vbCrLf
    
    
    'UCIP Memebers
    '-------------
    
    originTransactionID = "<member>" + vbCrLf + _
                                "<name>originTransactionID</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                        "<string>1136807058750</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
                        
    originTimeStamp = "<member>" + vbCrLf + _
                                "<name>originTimeStamp</name>" + vbCrLf + _
                                "<value>" + vbCrLf + _
                                "<dateTime.iso8601>" + TimeStamp + "</dateTime.iso8601>" + vbCrLf + _
                                "</value>" + vbCrLf + _
                        "</member>" + vbCrLf
    
    originHostName = "<member>" + vbCrLf + _
                                "<name>originHostName</name>" + vbCrLf + _
                                "<value>" + vbCrLf + _
                                "<string>" + SulSAT.Winsock1.LocalHostName + "</string>" + vbCrLf + _
                                "</value>" + vbCrLf + _
                        "</member>" + vbCrLf
    
    originNodeType = "<member>" + vbCrLf + _
                                "<name>originNodeType</name>" + vbCrLf + _
                                "<value>" + vbCrLf + _
                                "<string>SDP</string>" + vbCrLf + _
                                "</value>" + vbCrLf + _
                        "</member>" + vbCrLf
    
    UCIP_Close = "</struct>" + vbCrLf + _
                            "</value>" + vbCrLf + _
                        "</param>" + vbCrLf + _
                    "</params>" + vbCrLf + _
                "</methodCall>" + vbCrLf
End Sub
Public Function DateStamp()

End Function

Public Function TimeStamp(Optional DateTime As String)
    If DateTime = "" Then DateTime = Date
    TimeStamp = Zero_Ext(Trim(Str(Year(DateTime))), 4) + Zero_Ext(Trim(Str(Month(DateTime))), 2) + Zero_Ext(Trim(Str(Day(DateTime))), 2) + _
                "T" + Zero_Ext(Trim(Str(Hour(Time))), 2) + ":" + Zero_Ext(Trim(Str(Minute(Time))), 2) + ":" + Zero_Ext(Trim(Str(Second(Time))), 2) + _
                "+0200"
End Function

Private Function Zero_Ext(ByVal Character As String, ByVal Length As Integer)
    Dim Temp As String
    
    If Len(Character) < Length Then
        For i = Len(Character) To Length - 1
            Temp = Temp + "0"
        Next i
    End If
    Zero_Ext = Temp + Character
End Function

Public Sub UCIP_Methods()
    AccumulatorEnquiryTRequest = "<methodName>AccumulatorEnquiryTRequest</methodName> " + vbCrLf
    AdjustmentTRequest = "<methodName>AdjustmentTRequest</methodName> " + vbCrLf
    BalanceEnquiryTRequest = "<methodName>BalanceEnquiryTRequest</methodName> " + vbCrLf
    GetAccountDetailsTRequest = "<methodName>GetAccountDetailsTRequest</methodName>" + vbCrLf
    GetFaFListTRequest = "<methodName>GetFaFListTRequest</methodName>" + vbCrLf
    RefillTRequest = "<methodName>RefillTRequest</methodName>" + vbCrLf
    StandardVoucherRefillTRequest = "<methodName>StandardVoucherRefillTRequest</methodName>" + vbCrLf
    UpdateAccountDetailsT = "<methodName>UpdateAccountDetailsTRequest</methodName>" + vbCrLf
    UpdateFaFTRequest = "<methodName>UpdateFaFTRequest</methodName>" + vbCrLf
    UpdateServiceClassT = "<methodName>UpdateServiceClassTRequest</methodName>" + vbCrLf
    ValueVoucherEnquiryT = "<methodName>ValueVoucherEnquiryTRequest</methodName>" + vbCrLf
    ValueVoucherRefillT = "<methodName>ValueVoucherRefillTRequest</methodName>" + vbCrLf
End Sub

Public Function Filling(ByVal Option_Name As String, ByVal Value As String) As String

Dim Updated_Option As String

Select Case Option_Name
        
    Case "subscriberNumber"
        subscriberNumber = "<member>" + vbCrLf + _
                                    "<name>subscriberNumber</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
        Updated_Option = subscriberNumber
                            
    Case "subscriberNumberNAI"
        subscriberNumberNAI = "<member>" + vbCrLf + _
                                    "<name>subscriberNumberNAI</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<int>" + Value + "</int>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
        Updated_Option = subscriberNumberNAI
        
    Case "messageCapabilityFlag"
        messageCapabilityFlag = "<member>" + vbCrLf + _
                                    "<name>messageCapabilityFlag</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
        Updated_Option = messageCapabilityFlag

    Case "transactionCurrency"
        transactionCurrency = "<member>" + vbCrLf + _
                                    "<name>transactionCurrency</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
        Updated_Option = transactionCurrency

    Case "externalData1"
        externalData1 = "<member>" + vbCrLf + _
                                    "<name>externalData1</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
        Updated_Option = externalData1
                            
    Case "dedicatedAccountInformation"
        dedicatedAccountInformation = "<member>" + vbCrLf + _
                                        "<name>dedicatedAccountInformation</name>" + vbCrLf + _
                                        "<value>" + vbCrLf + _
                                            "<array>" + vbCrLf + _
                                                "<data>" + vbCrLf + _
                                                     Value + vbCrLf + _
                                                "</data>" + vbCrLf + _
                                              "</array>" + vbCrLf + _
                                            "</value>" + vbCrLf + _
                                        "</member>" + vbCrLf
         Updated_Option = dedicatedAccountInformation
                            
    Case "externalData2"
        externalData2 = "<member>" + vbCrLf + _
                                    "<name>externalData2</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
          Updated_Option = externalData2
                            
    Case "relativeDateAdjustment Supervision"
        relativeDateAdjustmentSupervision = "<member>" + vbCrLf + _
                                    "<name>relativeDateAdjustmentSupervision</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<i4>" + Value + "</i4>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
           Updated_Option = relativeDateAdjustmentSupervision
                   
    Case "relativeDateAdjustment ServiceFee"
        relativeDateAdjustmentServiceFee = "<member>" + vbCrLf + _
                                    "<name>relativeDateAdjustmentServiceFee</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<i4>" + Value + "</i4>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = relativeDateAdjustmentServiceFee
            
    Case "currentLanguageID"
        currentLanguageID = "<member>" + vbCrLf + _
                                    "<name>currentLanguageID</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<int>" + Value + "</int>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = currentLanguageID
                            
    Case "newLanguageID"
        newLanguageID = "<member>" + vbCrLf + _
                                    "<name>newLanguageID</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<int>" + Value + "</int>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = newLanguageID
                            
    Case "firstIVRCallDone"
        firstIVRCallDone = "<member>" + vbCrLf + _
                                    "<name>firstIVRCallDone</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = firstIVRCallDone
                            
    Case "pinCode"
        pinCode = "<member>" + vbCrLf + _
                                    "<name>pinCode</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = pinCode
                            
    Case "performValidation"
        performValidation = "<member>" + vbCrLf + _
                                    "<name>performValidation</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<value><boolean>" + Value + "</boolean></value>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = performValidation
                            
    Case "chargingRequestInformation"
        chargingRequestInformation = "<member>" + vbCrLf + _
                                    "<name>chargingRequestInformation</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = chargingRequestInformation
                            
    Case "selectedOption"
        selectedOption = "<member>" + vbCrLf + _
                                    "<name>selectedOption</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = selectedOption
                            
    Case "fafAction"
        fafAction = "<member>" + vbCrLf + _
                                    "<name>fafAction</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = fafAction
    
    Case "fafInformation"
        fafInformation = "<member>" + vbCrLf + _
                                    "<name>fafInformation</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<array>" + vbCrLf + _
                                    "<data>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<struct>" + vbCrLf + _
                                     Value
            Updated_Option = fafInformation
                            
    Case "serviceClassNew"
        serviceClassNew = "<member>" + vbCrLf + _
                                    "<name>serviceClassNew</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<i4>" + Value + "</i4>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = serviceClassNew
                            
    Case "serviceClassCurrent"
        serviceClassCurrent = "<member>" + vbCrLf + _
                                    "<name>serviceClassCurrent</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<i4>" + Value + "</i4>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = serviceClassCurrent
                                
    Case "dedicatedAccountID"
        dedicatedAccountID = "<member>" + vbCrLf + _
                                    "<name>dedicatedAccountID</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<i4>" + Value + "</i4>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = dedicatedAccountID
            
    Case "newExpiryDate"
        newExpiryDate = "<member>" + vbCrLf + _
                        "<name>newExpiryDate</name>" + vbCrLf + _
                        "<value><dateTime.iso8601>" + _
                        Zero_Ext(Trim(Str(Year(Value))), 4) + Zero_Ext(Trim(Str(Month(Value))), 2) + Zero_Ext(Trim(Str(Day(Value))), 2) + _
                        "T00:00:00+0700</dateTime.iso8601></value>" + vbCrLf + _
                        "</member>" + vbCrLf
            Updated_Option = newExpiryDate
            
    Case "adjustmentAmount"
        adjustmentAmount = "<member>" + vbCrLf + _
                                    "<name>adjustmentAmount</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = adjustmentAmount
            
            
    Case "relativeDateAdjustment"
        relativeDateAdjustment = "<member>" + vbCrLf + _
                                    "<name>relativeDateAdjustment</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<i4>" + Value + "</i4>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = relativeDateAdjustment
            
    Case "requestedInformation"
        requestedInformation = "<member>" + vbCrLf + _
                                    "<name>requestedInformation</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = requestedInformation
            
    Case "requestedOwner"
        requestedOwner = "<member>" + vbCrLf + _
                                    "<name>requestedOwner</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = requestedOwner
            
    Case "transactionAmountRefill"
        TransactionAmountRefill = "<member>" + vbCrLf + _
                                    "<name>transactionAmountRefill</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = TransactionAmountRefill
            
    Case "fafNumber"
        fafNumber = "<member>" + vbCrLf + _
                                    "<name>fafNumber</name>" + vbCrLf + _
                                    "<value>" + Value + "</value>" + vbCrLf + _
                                        "</member>" + vbCrLf
            Updated_Option = fafNumber
            
    Case "fafIndicator"
        fafIndicator = "<member>" + vbCrLf + _
                                    "<name>fafIndicator</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<int>" + Value + "</int>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = fafIndicator
            
    Case "owner"
        Owner = "<member>" + vbCrLf + _
                                    "<name>owner</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<int>" + Value + "</int>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = Owner
            
    Case "paymentProfileID"
        paymentProfileID = "<member>" + vbCrLf + _
               "<name>paymentProfileID</name>" + vbCrLf + _
               "<value><string>" + Value + "</string></value>" + vbCrLf + _
              "</member>" + vbCrLf
            Updated_Option = paymentProfileID
            
    Case "activationNumber"
        activationNumber = "<member>" + vbCrLf + _
                            "<name>activationNumber</name>" + vbCrLf + _
                            "<value>" + vbCrLf + _
                            "<string>" + Value + "</string>" + vbCrLf + _
                            "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = activationNumber
            
                Case "transactionAmountRefill"
        TransactionAmountRefill = "<member>" + vbCrLf + _
                                    "<name>transactionAmountRefill</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = TransactionAmountRefill
            
                Case "transactionAmountRefill"
        TransactionAmountRefill = "<member>" + vbCrLf + _
                                    "<name>transactionAmountRefill</name>" + vbCrLf + _
                                    "<value>" + vbCrLf + _
                                    "<string>" + Value + "</string>" + vbCrLf + _
                                    "</value>" + vbCrLf + _
                            "</member>" + vbCrLf
            Updated_Option = TransactionAmountRefill
            
            
        Case Else
            Updated_Option = "ERRRRRRRRRRRRRRRRRRRRRRRRRRRRRROR"
    End Select
    
    Filling = Updated_Option
End Function

