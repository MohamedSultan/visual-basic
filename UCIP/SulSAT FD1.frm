VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form SulSAT 
   Caption         =   "SULSAT V2.5"
   ClientHeight    =   7140
   ClientLeft      =   5565
   ClientTop       =   1755
   ClientWidth     =   10230
   Icon            =   "SulSAT FD1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7140
   ScaleWidth      =   10230
   Begin VB.CommandButton Batch_Insert_MSISDN_Command 
      Caption         =   "Batch Insert"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8280
      TabIndex        =   127
      Top             =   4320
      Width           =   1815
   End
   Begin VB.CommandButton Install_Subscribers_Command 
      Caption         =   "Install Subscribers"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   126
      Top             =   3960
      Width           =   1815
   End
   Begin VB.CommandButton BGW_History 
      Caption         =   "History"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   125
      Top             =   3480
      Width           =   1815
   End
   Begin VB.Frame Frame 
      Caption         =   "Balances"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   0
      Left            =   120
      TabIndex        =   17
      Top             =   3600
      Visible         =   0   'False
      Width           =   8055
      Begin VB.CommandButton Save 
         Caption         =   "Save"
         Height          =   495
         Left            =   5400
         TabIndex        =   36
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   3000
         TabIndex        =   99
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label DedicateAccount 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   2040
         TabIndex        =   98
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   3000
         TabIndex        =   97
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label DedicateAccount 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   2040
         TabIndex        =   96
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   3000
         TabIndex        =   95
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label DedicateAccount 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   2040
         TabIndex        =   94
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   3000
         TabIndex        =   93
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label DedicateAccount 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   2040
         TabIndex        =   92
         Top             =   960
         Width           =   855
      End
      Begin VB.Label DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   3000
         TabIndex        =   91
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label DedicateAccount 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   2040
         TabIndex        =   90
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         Caption         =   "Expiry Date"
         Height          =   255
         Left            =   3000
         TabIndex        =   47
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label25 
         Alignment       =   2  'Center
         Caption         =   "Balance"
         Height          =   255
         Left            =   2040
         TabIndex        =   46
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label21 
         Caption         =   "Dedicated Account 2"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label22 
         Caption         =   "Dedicated Account 1"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label20 
         Caption         =   "Dedicated Account 5"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   2085
         Width           =   1695
      End
      Begin VB.Label Label19 
         Caption         =   "Dedicated Account 3"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1365
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Dedicated Account 4"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1725
         Width           =   1695
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Accumulators"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   3
      Left            =   120
      TabIndex        =   48
      Top             =   3840
      Visible         =   0   'False
      Width           =   8055
      Begin VB.Label AccumulatorStartDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   3600
         TabIndex        =   114
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label AccumulatorResetDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   2400
         TabIndex        =   113
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Accumulator 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   1680
         TabIndex        =   112
         Top             =   2040
         Width           =   615
      End
      Begin VB.Label AccumulatorStartDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   3600
         TabIndex        =   111
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label AccumulatorResetDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   2400
         TabIndex        =   110
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Accumulator 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   1680
         TabIndex        =   109
         Top             =   1680
         Width           =   615
      End
      Begin VB.Label AccumulatorStartDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   3600
         TabIndex        =   108
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label AccumulatorResetDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   2400
         TabIndex        =   107
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Accumulator 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   1680
         TabIndex        =   106
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label AccumulatorStartDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   3600
         TabIndex        =   105
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label AccumulatorResetDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   2400
         TabIndex        =   104
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Accumulator 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   1680
         TabIndex        =   103
         Top             =   960
         Width           =   615
      End
      Begin VB.Label AccumulatorStartDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   3600
         TabIndex        =   102
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label AccumulatorResetDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   2400
         TabIndex        =   101
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Accumulator 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   1680
         TabIndex        =   100
         Top             =   600
         Width           =   615
      End
      Begin VB.Label Label33 
         Caption         =   "Accumulator 1"
         Height          =   255
         Left            =   240
         TabIndex        =   56
         Top             =   660
         Width           =   1695
      End
      Begin VB.Label Label32 
         Caption         =   "Accumulator 2"
         Height          =   255
         Left            =   240
         TabIndex        =   55
         Top             =   1020
         Width           =   1695
      End
      Begin VB.Label Label31 
         Caption         =   "Accumulator 5"
         Height          =   255
         Left            =   240
         TabIndex        =   54
         Top             =   2100
         Width           =   1695
      End
      Begin VB.Label Label30 
         Caption         =   "Accumulator 3"
         Height          =   255
         Left            =   240
         TabIndex        =   53
         Top             =   1380
         Width           =   1695
      End
      Begin VB.Label Label29 
         Caption         =   "Accumulator 4"
         Height          =   255
         Left            =   240
         TabIndex        =   52
         Top             =   1740
         Width           =   1695
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Reset Date"
         Height          =   255
         Index           =   5
         Left            =   2280
         TabIndex        =   51
         Top             =   285
         Width           =   1215
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "ID"
         Height          =   255
         Index           =   7
         Left            =   1440
         TabIndex        =   50
         Top             =   280
         Width           =   855
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Start Date"
         Height          =   255
         Index           =   8
         Left            =   3480
         TabIndex        =   49
         Top             =   285
         Width           =   1215
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Refills"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   3480
      Visible         =   0   'False
      Width           =   8055
      Begin VB.TextBox Activation_Number 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         TabIndex        =   85
         Top             =   360
         Width           =   1695
      End
      Begin VB.ComboBox RefillProfileID 
         Height          =   315
         Left            =   2040
         TabIndex        =   43
         Top             =   1440
         Width           =   975
      End
      Begin VB.TextBox Refill_Amount_Pt 
         Height          =   285
         Left            =   2520
         TabIndex        =   39
         Top             =   1080
         Width           =   495
      End
      Begin VB.TextBox Refill_Amount_LE 
         Height          =   285
         Left            =   2040
         TabIndex        =   38
         Top             =   1080
         Width           =   495
      End
      Begin VB.CommandButton Refill 
         Caption         =   "Refill"
         Height          =   495
         Left            =   5400
         TabIndex        =   34
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label refillFraudCount 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6000
         TabIndex        =   122
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label27 
         Caption         =   "refillFraudCount"
         Height          =   255
         Left            =   4560
         TabIndex        =   121
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label16 
         Caption         =   "Refill Profile"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   1520
         Width           =   855
      End
      Begin VB.Label Label17 
         Caption         =   "LE"
         Height          =   255
         Left            =   2160
         TabIndex        =   41
         Top             =   840
         Width           =   375
      End
      Begin VB.Label Label18 
         Caption         =   "Pt."
         Height          =   255
         Index           =   0
         Left            =   2625
         TabIndex        =   40
         Top             =   840
         Width           =   255
      End
      Begin VB.Label Label12 
         Caption         =   "Charging Amount"
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label23 
         Caption         =   "Activation Number"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Timer TimeOut 
      Interval        =   60000
      Left            =   8880
      Tag             =   "1 Minute"
      Top             =   120
   End
   Begin VB.Frame Dates_Frame 
      Caption         =   "Life Cycle"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   8055
      Begin VB.TextBox supervisionPeriod 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         TabIndex        =   8
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox serviceFeePeriod 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         TabIndex        =   7
         Top             =   600
         Width           =   1695
      End
      Begin VB.TextBox serviceFeeDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   6000
         TabIndex        =   6
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox supervisionDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   6000
         TabIndex        =   5
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label removalPeriod 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2040
         TabIndex        =   89
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label creditClearanceDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6000
         TabIndex        =   88
         Top             =   960
         Width           =   1815
      End
      Begin VB.Label serviceRemovalDate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6000
         TabIndex        =   87
         Top             =   1320
         Width           =   1815
      End
      Begin VB.Label creditClearancePeriod 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2040
         TabIndex        =   86
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label11 
         Caption         =   "Credit Clearance Period"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   1000
         Width           =   1815
      End
      Begin VB.Label Label13 
         Caption         =   "Supervision Period"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   280
         Width           =   1815
      End
      Begin VB.Label Label14 
         Caption         =   "Service Fee Period"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   640
         Width           =   1815
      End
      Begin VB.Label Label15 
         Caption         =   "Service Removal Period"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1360
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "Service Removal Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   12
         Top             =   1320
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Service Fee Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   11
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Supervision Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   10
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Credit Clearance Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   9
         Top             =   960
         Width           =   1815
      End
   End
   Begin VB.Frame MainDataFrame 
      Caption         =   "Main Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   77
      Top             =   720
      Width           =   8055
      Begin VB.CommandButton Language_ID_Switch 
         Caption         =   "Language ID "
         Height          =   255
         Left            =   4080
         TabIndex        =   120
         Top             =   300
         Width           =   1215
      End
      Begin VB.CommandButton Service_Class_Switch 
         Caption         =   "Service Class   "
         Height          =   255
         Left            =   120
         TabIndex        =   119
         Top             =   300
         Width           =   1215
      End
      Begin VB.ComboBox Language_Desc 
         Height          =   315
         Left            =   6000
         TabIndex        =   118
         Text            =   "Language_Desc"
         Top             =   240
         Width           =   1695
      End
      Begin VB.ComboBox currentLanguageID 
         Height          =   315
         Left            =   6000
         TabIndex        =   117
         Top             =   240
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.ComboBox serviceClassCurrent 
         Height          =   315
         Left            =   2040
         TabIndex        =   116
         Text            =   "serviceClassCurrent"
         Top             =   240
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.ComboBox ServiceClass_Desc 
         Height          =   315
         Left            =   2040
         TabIndex        =   115
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox accountValue1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         TabIndex        =   78
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Sub_Status 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6000
         TabIndex        =   84
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Label10 
         Caption         =   "Service Class"
         Height          =   255
         Left            =   120
         TabIndex        =   82
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Main Balance"
         Height          =   255
         Left            =   120
         TabIndex        =   81
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Language ID"
         Height          =   255
         Left            =   4080
         TabIndex        =   80
         Top             =   300
         Width           =   1815
      End
      Begin VB.Label Label9 
         Caption         =   "Subscriber Status"
         Height          =   255
         Left            =   4080
         TabIndex        =   79
         Top             =   720
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Response"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   6000
      Width           =   8055
      Begin VB.TextBox Response_TExt 
         Height          =   285
         Left            =   840
         TabIndex        =   19
         Top             =   240
         Width           =   7095
      End
   End
   Begin VB.Frame MSISDN_Frame 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   3375
      Begin VB.ComboBox MSISDNs_List 
         Height          =   315
         Left            =   960
         TabIndex        =   21
         Text            =   "Combo1"
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "MSISDN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   280
         Width           =   1815
      End
   End
   Begin VB.CommandButton Refills 
      Caption         =   "Refills"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   35
      Top             =   2520
      Width           =   1815
   End
   Begin VB.CommandButton Configurations 
      Caption         =   "Configurations"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   33
      Top             =   4920
      Width           =   1815
   End
   Begin VB.CommandButton Accumulators 
      Caption         =   "Accumulators"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   32
      Top             =   1440
      Width           =   1815
   End
   Begin VB.CommandButton GetBalances 
      Caption         =   "Balances"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   31
      Top             =   1080
      Width           =   1815
   End
   Begin VB.CommandButton Activate 
      Caption         =   "Activate"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   30
      Top             =   3000
      Width           =   1815
   End
   Begin VB.CommandButton Load 
      Caption         =   "Basic Information"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   29
      Top             =   720
      Width           =   1815
   End
   Begin VB.CommandButton LoadAll 
      Caption         =   "Load All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   28
      Top             =   1920
      Width           =   1815
   End
   Begin VB.CommandButton CCAPI_SW 
      Caption         =   "CCAPI Switch"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   27
      Top             =   6000
      Width           =   1815
   End
   Begin VB.CommandButton ShoW_UCIP 
      Caption         =   "Show UCIP Command"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8280
      TabIndex        =   22
      Top             =   5400
      Width           =   1815
   End
   Begin VB.TextBox Success 
      Height          =   1965
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   18
      Top             =   7200
      Width           =   8055
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   7560
      Top             =   6720
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Frame Frame 
      Caption         =   "Configurations"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Index           =   2
      Left            =   0
      TabIndex        =   57
      Top             =   960
      Visible         =   0   'False
      Width           =   8175
      Begin VB.Frame Lock_DB_Frame 
         Caption         =   "    Lock/Unlock SulSat Access"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   0
         TabIndex        =   72
         Top             =   3360
         Width           =   8055
         Begin VB.CommandButton Unlock_DB 
            Caption         =   "Unlock Data Base"
            Height          =   375
            Left            =   840
            TabIndex        =   76
            Top             =   840
            Width           =   2175
         End
         Begin VB.CommandButton Lock_DB 
            Caption         =   "Lock Data Base"
            Height          =   375
            Left            =   4320
            TabIndex        =   75
            Top             =   840
            Width           =   2175
         End
         Begin VB.ComboBox Security_Levels_Locked 
            Height          =   315
            Left            =   2280
            Style           =   2  'Dropdown List
            TabIndex        =   73
            Top             =   320
            Width           =   2055
         End
         Begin VB.Label Label35 
            Caption         =   "Security Level"
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            TabIndex        =   74
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.Frame Users_Frame 
         Caption         =   "    Users"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   0
         TabIndex        =   61
         Top             =   1320
         Width           =   8055
         Begin VB.ComboBox SecurityLevels 
            Height          =   315
            Left            =   2280
            Style           =   2  'Dropdown List
            TabIndex        =   71
            Top             =   840
            Width           =   2055
         End
         Begin VB.TextBox UserName 
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2280
            TabIndex        =   70
            Top             =   360
            Width           =   2055
         End
         Begin VB.CommandButton UpdateUser 
            Caption         =   "Update User"
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5640
            TabIndex        =   64
            Top             =   1560
            Width           =   2175
         End
         Begin VB.CommandButton AddUser 
            Caption         =   "Add User"
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   63
            Top             =   1560
            Width           =   2175
         End
         Begin VB.CommandButton Delete_User 
            Caption         =   "Delete User"
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3000
            TabIndex        =   62
            Top             =   1560
            Width           =   2175
         End
         Begin VB.Label Label34 
            Caption         =   "Security Level"
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   69
            Top             =   920
            Width           =   1455
         End
         Begin VB.Label Label28 
            Caption         =   "User Name"
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   68
            Top             =   400
            Width           =   1455
         End
         Begin VB.Label UserName_Comment 
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1455
            Left            =   4800
            TabIndex        =   66
            Top             =   480
            Width           =   2655
         End
         Begin VB.Label Both_Comments 
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   495
            Left            =   4800
            TabIndex        =   65
            Top             =   720
            Width           =   2415
         End
         Begin VB.Label Password_comment 
            BeginProperty Font 
               Name            =   "Georgia"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1575
            Left            =   4800
            TabIndex        =   67
            Top             =   480
            Width           =   2655
         End
      End
      Begin VB.Frame Connectivity_Frame 
         Caption         =   "    Connectivity"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   0
         TabIndex        =   58
         Top             =   360
         Width           =   8055
         Begin VB.ComboBox AIR_Port 
            Height          =   315
            Left            =   3720
            TabIndex        =   123
            Top             =   360
            Width           =   975
         End
         Begin VB.ComboBox AIR_IP 
            Height          =   315
            Left            =   1080
            Style           =   2  'Dropdown List
            TabIndex        =   59
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label Label36 
            Caption         =   "AIR Port"
            Height          =   375
            Left            =   2880
            TabIndex        =   124
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label24 
            Caption         =   "AIR IP"
            Height          =   255
            Left            =   120
            TabIndex        =   60
            Top             =   360
            Width           =   615
         End
      End
   End
   Begin VB.Label UserDetails_Label 
      Height          =   615
      Left            =   8280
      TabIndex        =   83
      Top             =   6480
      Width           =   1935
   End
   Begin MSForms.ToggleButton ShowResponses_botton 
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   6600
      Width           =   8055
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "14208;661"
      Value           =   "0"
      Caption         =   "Show Responses"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin VB.Menu LogOut 
      Caption         =   "LogOut"
   End
   Begin VB.Menu Help 
      Caption         =   "Help"
   End
   Begin VB.Menu About 
      Caption         =   "About"
   End
End
Attribute VB_Name = "SulSAT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Last_UCIP_Response As UCIP_Response_Type
'Dim All_SecurityLevels() As SecurityLevel_Types
Dim TimeOut_Counter As Integer
Private Sub Reset_TimeOut_Counter()
    TimeOut_Counter = 0
End Sub
Public Sub ClearUCIP_Request_Data()
    With UCIP_Request_Data
        .subscriberNumberNAI = ""
        .subscriberNumber = ""
        .messageCapabilityFlag = ""
        .adjustmentAmount = ""
        .externalData1 = ""
        .externalData2 = ""
        .relativeDateAdjustmentSupervision = ""
        .relativeDateAdjustmentServiceFee = ""
        .requestedOwner = ""
        .TransactionAmountRefill = ""
        .paymentProfileID = ""
        .activationNumber = ""
        .currentLanguageID = ""
        .newLanguageID = ""
        .firstIVRCallDone = ""
        .serviceClassCurrent = ""
        .serviceClassNew = ""
    End With
End Sub
Private Sub About_Click()
    About_Form.Top = SulSAT.Top
    About_Form.Label1 = "SulSat V2.4" + vbCrLf + _
                        vbCrLf + _
                        "For 32-bit Windows Operating Systems" + vbCrLf + _
                         vbCrLf + _
                         "Copyright" + Chr(169) + " For Sultan Development Tools Inc." + Chr(174) + vbCrLf + _
                         vbCrLf + _
                        "This Product is Developed For Customer Operations Teams as Consistancy Plan In Case of MINSAT Problems" + vbCrLf + _
                         vbCrLf + _
                        vbCrLf + _
                        vbCrLf
   Call About_Form.SulSat_Data

End Sub

Private Sub accountValue1_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Accumulators_Click()
    Call Reset_TimeOut_Counter
    If MSISDN_Selected Then
        Dim MainBalance As String
        MainBalance = Val(Last_UCIP_Response.accountValue1) / 100
        Call Slide_Frame(3)
        Call Clear_UnEnable_Accumulators
        Update_Response ("Accumulator enquiry: " + Response_Code(AccumulatorEnquiryT_Command(MSISDNs_List)))
        Call Fill
        SulSAT.accountValue1 = MainBalance
    End If
    Call LockDates(True)
End Sub
Private Sub LockDates(Lock_ As Boolean)
    With SulSAT
        If Lock_ Then
            .supervisionPeriod.Enabled = False
            .serviceFeePeriod.Enabled = False
            .supervisionDate.Enabled = False
            .serviceFeeDate.Enabled = False
        Else
            .supervisionPeriod.Enabled = True
            .serviceFeePeriod.Enabled = True
            .supervisionDate.Enabled = True
            .serviceFeeDate.Enabled = True
        End If
    End With
End Sub

Private Sub Activate_Click()
    Call Reset_TimeOut_Counter
    Call LockDates(False)
    If MSISDN_Selected Then
        Call Slide_Frame(0)
        UCIP_Request_Data.subscriberNumber = MSISDNs_List
        UCIP_Request_Data.messageCapabilityFlag = "01100000"
        Update_Response ("Get Account Details (Activate): " + Response_Code(GetAccountDetailsT_Command(UCIP_Request_Data.subscriberNumber, , UCIP_Request_Data.messageCapabilityFlag)))
        Call Fill
    End If
End Sub
Private Sub Activation_Number_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If Len(Activation_Number) > 13 And KeyAscii <> 8 And KeyAscii <> 24 And KeyAscii <> 3 Then
        MsgBox "Please Enter Activation Number in 14 Digits", , "Length Error"
        KeyAscii = 0
    End If
    
    If KeyAscii = 13 Then Call Refill_Click
End Sub
Private Sub Validicate_Numbers(KeyAscii As Integer)
    If Not ((KeyAscii >= 48 And KeyAscii <= 57) Or KeyAscii = 46 Or KeyAscii = 8 Or KeyAscii = 13 Or KeyAscii = 22 Or KeyAscii = 24 Or KeyAscii = 3) Then
        MsgBox "Numbers Are Only Accepted In This Field", , "Character Error"
        KeyAscii = 0
    End If
    'KeyAscii = 8 'Back Space
    'KeyAscii = 13 'Enter
    'KeyAscii = 22 'Paste
    'KeyAscii = 24 'Cut
    'KeyAscii = 3 'Copy
End Sub
Private Sub AddUser_Click()
    Call DB_Users("Add User", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments, Me.SecurityLevels)
End Sub

Private Sub AIR_IP_Change()
    UCIP.AIR_IP = Me.AIR_IP
End Sub

Private Sub AIR_IP_LostFocus()
    Call Save_List(Me.AIR_IP, "AIR_IPs")
    Call AIR_IP_Change
End Sub

Private Sub Batch_Insert_MSISDN_Command_Click()
    Dim_Undim_All (False)
    
    Call Slide(Batch_Insert, Int((SulSAT.Left + SulSAT.Width) / 2), Batch_Insert.Width, True, 2)
End Sub

Public Sub BGW_History_Click()
'    With BillingGW_Form
'        .UserName = "aregaily"
'        .Password = "aregaily123"
'        .UserName = "peri"
'        .Password = "peri"
'        'Path = "Sultan"
'        .Command(0) = "cd /var/opt/BGw/Server1/Vodafone/A1000vol1/CS30/genReports"
'        .Command(1) = "grep ,5000 PPAS-ADJ_* | grep 160405626 | head"
'        .Command(2) = "echo Finished"
'
'        .Port = "23"
'        'Telnet_BillingGW.RemotePort = "10023"
'        .IP = "172.28.6.15"
'        .Connect_Click
'        .Show
'    End With
    With BillingGW_Form
        For i = 0 To .History_List.UBound
            .History_List(i).Clear
        Next i
        .Display = ""
        .Sub_Display = ""
        .Status.Clear
        .Command(1) = .Command1_Part1 + SulSAT.MSISDNs_List.Text + .Command1_Part2
        Call .Connect_Click
        Call Slide(BillingGW_Form, .Left, .Width, True, 2)
        BillingGW_Form.SetFocus
        Call Add_Transaction("BGW History", "View History")
    End With
End Sub

Private Sub CCAPI_SW_Click()
    Call Reset_TimeOut_Counter
'    Call SulSAT.sulsat_command_Click
End Sub

Private Sub currentLanguageID_Change()
    Dim LanguageIndex As Integer
    LanguageIndex = LanguageID(currentLanguageID, currentLanguageID.Text)
    Language_Desc.Text = Language_Desc.List(LanguageIndex)
End Sub

Private Sub currentLanguageID_Click()
    Language_Desc.Text = Language_Desc.List(currentLanguageID.ListIndex)
End Sub

Private Sub d_Click()

End Sub

Private Sub Form_Resize()
    If Not SulSat_Dimensions.Sliding Then
        Select Case Me.WindowState
            Case vbMaximized
                Me.WindowState = vbNormal
            Case vbNormal
                Me.Height = SulSat_Dimensions.Height
                Me.Width = SulSat_Dimensions.Width
        End Select
    End If
End Sub

Private Sub Help_Click()
    Dim appWord As Word.Application
    Dim wrdDoc As Word.Document
    Set appWord = New Word.Application
    Set wrdDoc = appWord.Documents.open(App.Path + "\SulSat.doc")
    appWord.Visible = True
End Sub

Private Sub Install_Subscribers_Command_Click()
    ServiceClass_Form.ServiceClass_Desc.Clear
    ServiceClass_Form.serviceClassCurrent.Clear
    ServiceClass_Form.currentLanguageID.Clear
    ServiceClass_Form.Language_Desc.Clear
    
    For i = serviceClassCurrent.ListCount - 1 To 0 Step -1
        ServiceClass_Form.ServiceClass_Desc.AddItem ServiceClass_Desc.List(i)
        ServiceClass_Form.serviceClassCurrent.AddItem serviceClassCurrent.List(i)
    Next i
    For i = currentLanguageID.ListCount - 1 To 0 Step -1
        ServiceClass_Form.Language_Desc.AddItem Language_Desc.List(i)
        ServiceClass_Form.currentLanguageID.AddItem currentLanguageID.List(i)
    Next i
    
    Dim_Undim_All (False)
    
    Call Slide(ServiceClass_Form, Int((SulSAT.Left + SulSAT.Width) / 2), ServiceClass_Form.Width, True, 2)
    ServiceClass_Form.serviceClassCurrent = 1
    ServiceClass_Form.currentLanguageID = 3
End Sub

Private Sub Language_Desc_Click()
    currentLanguageID.Text = currentLanguageID.List(Language_Desc.ListIndex)
End Sub

Private Sub Language_Desc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Language_ID_Switch_Click()
    Language_Desc.Visible = Not Language_Desc.Visible
    currentLanguageID.Visible = Not currentLanguageID.Visible
End Sub

Private Sub Lock_DB_Click()
    If DB_Users("Lock DB", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments, Me.Security_Levels_Locked, True) Then
        Unlock_DB.Enabled = True
        Lock_DB.Enabled = False
        For i = 0 To UBound(SulSat_ConfigurationData.SecurityLevels)
            If SulSat_ConfigurationData.SecurityLevels(i).Name = Security_Levels_Locked.Text Then
                SulSat_ConfigurationData.SecurityLevels(i).Locked = True
            End If
        Next i
    End If
End Sub

Private Sub MSISDNs_List_Click()
    Dim TextBoxes As Control
    For Each TextBoxes In Me
        If TypeOf TextBoxes Is TextBox Then TextBoxes = ""
        If TypeOf TextBoxes Is Label Then
            If TextBoxes.BorderStyle = 1 Then TextBoxes = ""
        End If
    Next TextBoxes
End Sub

Private Sub Security_Levels_Locked_Click()
    For i = 0 To UBound(SulSat_ConfigurationData.SecurityLevels)
        If Security_Levels_Locked.Text = SulSat_ConfigurationData.SecurityLevels(i).Name Then
            Lock_DB.Enabled = Not SulSat_ConfigurationData.SecurityLevels(i).Locked
            Unlock_DB.Enabled = SulSat_ConfigurationData.SecurityLevels(i).Locked
        End If
    Next i
End Sub



Private Sub Service_Class_Switch_Click()
    serviceClassCurrent.Visible = Not serviceClassCurrent.Visible
    ServiceClass_Desc.Visible = Not ServiceClass_Desc.Visible
End Sub

Private Sub ServiceClass_Desc_Click()
    serviceClassCurrent.Text = serviceClassCurrent.List(ServiceClass_Desc.ListIndex)
End Sub

Private Sub ServiceClass_Desc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub serviceClassCurrent_Change()
    Dim serviceClassCurrentIndex As Integer
    serviceClassCurrentIndex = serviceClassID(serviceClassCurrent, serviceClassCurrent.Text)
    ServiceClass_Desc.Text = ServiceClass_Desc.List(serviceClassCurrentIndex)
End Sub
Private Function serviceClassID(ServiceClass As ComboBox, Text As String) As Integer
    For i = 0 To ServiceClass.ListCount - 1
        If ServiceClass.List(i) = Text Then serviceClassID = i
    Next i
End Function
Private Function LanguageID(Language As ComboBox, Text As String) As Integer
    For i = 0 To Language.ListCount - 1
        If Language.List(i) = Text Then LanguageID = i
    Next i
End Function
Private Sub serviceClassCurrent_Click()
    ServiceClass_Desc.Text = ServiceClass_Desc.List(serviceClassCurrent.ListIndex)
End Sub

Private Sub TimeOut_Timer()
    If TimeOut_Counter = 20 Then
        MsgBox "You Need To Login Again as SulSat Session Has Expired", , "Time Out"
        End
    Else
        TimeOut_Counter = TimeOut_Counter + 1
    End If
End Sub

Private Sub Unlock_DB_Click()
    If DB_Users("UnLock DB", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments, Me.Security_Levels_Locked, True) Then
        Unlock_DB.Enabled = False
        Lock_DB.Enabled = True
        For i = 0 To UBound(SulSat_ConfigurationData.SecurityLevels)
            If SulSat_ConfigurationData.SecurityLevels(i).Name = Security_Levels_Locked.Text Then
                SulSat_ConfigurationData.SecurityLevels(i).Locked = False
            End If
        Next i
    End If
End Sub

Private Sub UpdateUser_Click()
    Call DB_Users("Update User", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments, Me.SecurityLevels)
End Sub

Private Sub Command1_Click()
    Call Add_Transaction("TYPE", "Success")
End Sub
Private Sub Configurations_Click()
    Call Reset_TimeOut_Counter
    Security_Levels_Locked.Clear
    For i = 0 To SecurityLevels.ListCount - 1
        If SecurityLevels.List(i) <> "SulSat" Then Security_Levels_Locked.AddItem SecurityLevels.List(i)
    Next i
    Call Slide_Frame(2)
End Sub

Private Sub currentLanguageID_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Delete_User_Click()
    Call DB_Users("Delete User", Me.UserName, Me.UserName_Comment, Me.Password_comment, Me.Both_Comments)
End Sub
Private Function AddString(Times As Integer)
    For i = 0 To Times
        AddString = AddString + " "
    Next i
End Function
Public Sub Form_Load()
    If Hour(Now) = 7 Then Kill App.Path + "\MSISDNs.txt"
    'Clearing Everything
    SulSat_ConfigurationData = GetAllSulSatConfigurations
    
    If Not SulSat_ConfigurationData.AIR.Found Then
        MsgBox "Sorry There is No AIR IP Configured For Your IP Address", , "Connection Error"
        Conn.Close
        End
    Else
        SulSAT.AIR_IP.Clear
        UCIP.AIR_IP.Clear
        For i = 0 To UBound(SulSat_ConfigurationData.AIR.AIR)
            SulSAT.AIR_IP.AddItem SulSat_ConfigurationData.AIR.AIR(i).AIR_IP
            UCIP.AIR_IP.AddItem SulSat_ConfigurationData.AIR.AIR(i).AIR_IP
            
            SulSAT.AIR_Port.AddItem SulSat_ConfigurationData.AIR.AIR(i).AIR_Port
            UCIP.AIR_Port.AddItem SulSat_ConfigurationData.AIR.AIR(i).AIR_Port
            
            'SulSAT.AIR_IP.AddItem To_Be_Connected_AIRs.AIR(i).AIR_IP
            UCIP.AIR_Name.AddItem SulSat_ConfigurationData.AIR.AIR(i).AIR_Name
        Next i
        SulSAT.AIR_IP.ListIndex = 0
        UCIP.AIR_IP.ListIndex = 0
    End If
    
    SulSat_Dimensions.Height = Me.Height
    SulSat_Dimensions.Width = Me.Width
    'D.Caption = AddString(Int(Screen.Width / 121.348314606742))
    
    UserName.ToolTipText = "User Name can't start with special characters or numbers, with minimum length 2 characters."
    ServiceClass_Desc.Clear
    serviceClassCurrent.Clear
    ShowResponses_botton.Value = False
    
    Dim TextBoxes As Control
    For Each TextBoxes In SulSAT
        If TypeOf TextBoxes Is TextBox Then TextBoxes.Text = ""
        If TypeOf TextBoxes Is Label Then If TextBoxes.BorderStyle = 1 Then TextBoxes = ""
    Next TextBoxes
    
    Me.Left = Screen.Width / 2 - Me.Width / 2
    Me.Top = Screen.Height / 2 - Me.Height / 2
    Me.Height = Int(13500 / 1.69811320754717) 'ShowResponses_botton.Top + ShowResponses_botton.Height + 200 '7950
    Me.Width = 10320
    
    
    Call Load_List(SulSAT.RefillProfileID, "RefillProfiles")
    Call Load_List(SulSAT.MSISDNs_List, "MSISDNS")
    'Kill App.Path + "\MSISDNS.txt"
    'Call Save_List(MSISDNs_List, "MSISDNS", True)
    
    NumberOfAccumulators = 5
    NumberOfDedicatedAccounts = 5
    
    Call Slide_Frame(0)
    
    Call SetTransactionData
    
    
    'Dim All_Service_Classes() As ServiceClassType
    'All_Service_Classes = Get_SCs
    
    Me.ServiceClass_Desc.Clear
    Me.serviceClassCurrent.Clear
    
    'For i = 0 To UBound(All_Service_Classes)
    For i = 0 To UBound(SulSat_ConfigurationData.ServiceClasses)
        Me.ServiceClass_Desc.AddItem SulSat_ConfigurationData.ServiceClasses(i).Desc
        Me.serviceClassCurrent.AddItem SulSat_ConfigurationData.ServiceClasses(i).ID
    Next i
    
    'All_SecurityLevels = Get_SecurityLevels
    
    'Dim All_Languages() As Language_Type
    'All_Languages = Get_Languages
    Me.Language_Desc.Clear
    Me.currentLanguageID.Clear
    For i = 0 To UBound(SulSat_ConfigurationData.Language)
        Me.Language_Desc.AddItem SulSat_ConfigurationData.Language(i).Description
        Me.currentLanguageID.AddItem SulSat_ConfigurationData.Language(i).ID
    Next i
    
    RefillProfileID = ""
    
    For i = 1 To NumberOfDedicatedAccounts
        DedicatedAccountExpiryDate(i).Enabled = False
        DedicateAccount(i).Enabled = False
    Next i

    'Dim To_Be_Connected_AIRs As AIR_IP_Type
    'To_Be_Connected_AIRs = Get_AIR_IPs
    

    'Call Dim_Undim_All(False)
    'For Each Control In SulSAT
    '    If TypeOf Control Is CommandButton Then Control.Enabled = True
    'Next Control
End Sub
Private Sub Security_Levels_Selection(Optional StandardRefill As Boolean, _
                                        Optional Refill As Boolean, _
                                        Optional Adjustment As Boolean, _
                                        Optional Update_Users As Boolean, _
                                        Optional Activate As Boolean, _
                                        Optional UCIP_Command As Boolean, _
                                        Optional UpdateLanguage As Boolean, _
                                        Optional Update_ServiceClass As Boolean, _
                                        Optional Change_AIR_IP As Boolean, _
                                        Optional Lock_DB As Boolean, _
                                        Optional BGW_History As Boolean, _
                                        Optional BasicInformations As Boolean, _
                                        Optional Balances As Boolean, _
                                        Optional Accumulators As Boolean, _
                                        Optional LoadAll As Boolean, _
                                        Optional InstallSubscribers As Boolean, _
                                        Optional Batch_Insert_MSISDN As Boolean)
    With SulSAT
        
        If Batch_Insert_MSISDN Then
            Batch_Insert_MSISDN_Command.Enabled = True
        Else
            Batch_Insert_MSISDN_Command.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        If InstallSubscribers Then
            Install_Subscribers_Command.Enabled = True
        Else
            Install_Subscribers_Command.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        'Standard Refill
        If StandardRefill Then
            .Activation_Number.Enabled = True
        Else
            .Activation_Number.Enabled = False
        End If
        
        'Refill
        If Refill Then
            .RefillProfileID.Enabled = True
            .Refill_Amount_LE.Enabled = True
            .Refill_Amount_Pt.Enabled = True
        Else
            .RefillProfileID.Enabled = False
            .Refill_Amount_LE.Enabled = False
            .Refill_Amount_Pt.Enabled = False
        End If
        
        If Refill Or StandardRefill Then .Refill.Enabled = True
        If Refill = False And StandardRefill = False Then .Refills.Enabled = False
'-----------------------------------------------------------------------------------------------------------
       'Lock DB
        If Lock_DB Then
            .Lock_DB_Frame.Enabled = True
        Else
            .Lock_DB_Frame.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
       'Basic Information
        If BasicInformations Then
            Me.Load.Enabled = True
        Else
            Me.Load.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
       'Balances
        If Balances Then
            .GetBalances.Enabled = True
        Else
            .GetBalances.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
       'Accumulator
        If Accumulators Then
            .Accumulators.Enabled = True
        Else
            .Accumulators.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
       'Lock DB
        If LoadAll Then
            .LoadAll.Enabled = True
        Else
            .LoadAll.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        ' BGW_History
        If BGW_History Then
            .BGW_History.Enabled = True
        Else
            .BGW_History.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        ' Change AIR IP
        If Change_AIR_IP Then
            .Connectivity_Frame.Enabled = True
        Else
            .Connectivity_Frame.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        ' Update Users
        If Update_Users Then
            .Users_Frame.Enabled = True
        Else
            .Users_Frame.Enabled = False
        End If
        
        If Not (Update_Users Or Change_AIR_IP Or Lock_DB) Then
            Configurations.Enabled = False
        Else
            Configurations.Enabled = True
        End If
'-----------------------------------------------------------------------------------------------------------
        'Activate
        If Activate Then
            .Activate.Enabled = True
        Else
            .Activate.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        'UCIP Command
        If UCIP_Command Then
            .ShoW_UCIP.Enabled = True
        Else
            .ShoW_UCIP.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        'Adjustemnt
        If Adjustment Then
            For Each Ctrl In .Controls
                If TypeOf Ctrl Is TextBox Then Ctrl.Enabled = True
            Next Ctrl
            .Activation_Number.Enabled = True
            .Save.Enabled = True
        Else
            For Each Ctrl In .Controls
                If TypeOf Ctrl Is TextBox Then Ctrl.Enabled = False
            Next Ctrl
            .Activation_Number.Enabled = True
            .Save.Enabled = False
        End If
        
'-----------------------------------------------------------------------------------------------------------
        'Update Language
        If UpdateLanguage Then
            .currentLanguageID.Enabled = True
            .Language_Desc.Enabled = True
        Else
            .currentLanguageID.Enabled = False
            .Language_Desc.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
        'Update Service Class
        If Update_ServiceClass Then
            .serviceClassCurrent.Enabled = True
            .ServiceClass_Desc.Enabled = True
        Else
            .serviceClassCurrent.Enabled = False
            .ServiceClass_Desc.Enabled = False
        End If
'-----------------------------------------------------------------------------------------------------------
    If Adjustment = False And UpdateLanguage = False And Update_ServiceClass = False Then
        .Save.Enabled = False
    Else
        .Save.Enabled = True
    End If
'-----------------------------------------------------------------------------------------------------------
    End With
End Sub
Public Sub User_Security_Level()
    If Users.Level_Of_Security <> "Level Of Security" Then
        SecurityLevels.Clear
        For i = 0 To UBound(SulSat_ConfigurationData.SecurityLevels)
            If UCase(SulSat_ConfigurationData.SecurityLevels(i).Name) <> "SULSAT" Then SecurityLevels.AddItem SulSat_ConfigurationData.SecurityLevels(i).Name
        Next i
        If LCase(CurrentUser.SecurityLevel) = "sulsat" Then SecurityLevels.AddItem "SulSat"
        
        Dim Ctrl As Control
        
        Dim Security_LevelsID As Integer
        Security_LevelsID = SecurityLevelType_VS_ID(Users.Level_Of_Security)
        With AllSecurityLevels(Security_LevelsID).SecurityLevelPrivilages
            Call Security_Levels_Selection(.StandardRefill, .Refill, .Adjustment, .Update_User, _
                                            .Activate, .UCIP_Command, .UpdateLanguage, .Update_ServiceClass, .Change_AIR, _
                                            .Lock_DB, .BGW_History, .BasicInformations, .Balances, .Accumulators, .LoadAll, _
                                            .InstallSubscribers, .Batch_Insert_MSISDN)
        End With
    End If
    
    'Select Case UCase(Users.Level_Of_Security)
    '    Case "CS3"
    '        Call Security_Levels_Selection(True, False, False, False, False, False, True, False, False, False)
    '    Case "CS4"
    '        Call Security_Levels_Selection(True, False, True, False, False, False, True, True, False, False)
    '    Case "ADMIN"
    '        Call Security_Levels_Selection(True, True, True, True, True, True, True, True, False, False)
    '    Case "SULSAT"
    '        Call Security_Levels_Selection(True, True, True, True, True, True, True, True, True, True)
    '    Case Else
    '        Call Security_Levels_Selection(False, False, False, False, False, False, False, False, False, False)
    'End Select
End Sub
Private Sub SetTransactionData()
    Transactions.AccumulatorEnquiry = "Accumulator Enquiry"
    Transactions.Adjustment = "Adjustment"
    Transactions.BalanceEnquiry = "Balance Enquiry"
    Transactions.GetAccountDetails = "Get Account Details"
    Transactions.UpdateAccountDetails = "Update Account Details"
    Transactions.UpdateServiceClass = "Update Service Class"
End Sub

Private Sub Form_Terminate()
    If Conn.State = 1 Then Conn.Close
    End
End Sub

Public Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub GetBalances_Click()
    Call Reset_TimeOut_Counter
    Call LockDates(False)
    If MSISDN_Selected Then
        If Sub_Status = "" Then
            Call LoadAll_Click
        Else
            Call Slide_Frame(0)
            Call Clear_Balances
            Call Update_Response("Balance Enquiry: " + Response_Code(BalanceEnquiryT_Command(MSISDNs_List)))
            Call Fill(True)
        End If
    End If
End Sub
Private Sub Slide_Frame(Frame_ID As Integer)
    'Showing Main Frame
    If SulSAT.MainDataFrame.Visible = False Or SulSAT.Dates_Frame.Visible = False Then
        Dim Current_MainDataframe_Width, Current_Dates_Frame_Width As Integer
        Current_MainDataframe_Width = SulSAT.MainDataFrame.Width
        Current_Dates_Frame_Width = SulSAT.Dates_Frame.Width
    
        'Showing Main and Dates Frames
        SulSAT.MainDataFrame.Width = 75
        SulSAT.Dates_Frame.Width = 75
        SulSAT.MainDataFrame.Visible = True
        SulSAT.Dates_Frame.Visible = True
        While SulSAT.MainDataFrame.Width < Current_MainDataframe_Width And SulSAT.Dates_Frame.Width < Current_Dates_Frame_Width
            SulSAT.MainDataFrame.Width = SulSAT.MainDataFrame.Width + 75
            SulSAT.Dates_Frame.Width = SulSAT.Dates_Frame.Width + 75
            'Wait 1
        Wend
        SulSAT.MainDataFrame.Width = Current_MainDataframe_Width
        SulSAT.Dates_Frame.Width = Current_Dates_Frame_Width
    End If
    
    'hiding all other frames
    For i = 0 To SulSAT.Frame.UBound
        If SulSAT.Frame(i).Visible = True Then
            While SulSAT.Frame(i).Width >= 75 And i <> Frame_ID
                SulSAT.Frame(i).Width = SulSAT.Frame(i).Width - 75
            '    Wait 1
            Wend
            SulSAT.Frame(i).Visible = False
            SulSAT.Frame(i).Width = Dates_Frame.Left + Dates_Frame.Width - 100
        End If
    Next i
    
    'Call HideCodes(Frame_ID)
    
    'showing Frame(id)
    If Frame_ID <> 2 Then 'Frame ID 2 is the configurations frame
        Dates_Frame.Visible = True
        MainDataFrame.Visible = True
        SulSAT.Frame(Frame_ID).Top = Dates_Frame.Top + Dates_Frame.Height - 200 '+ Dates_Frame.Height - 270
    Else
        Dates_Frame.Visible = False
        MainDataFrame.Visible = False
        SulSAT.Frame(Frame_ID).Top = MainDataFrame.Top 'MSISDN_Frame.Top + MSISDN_Frame.Height 'MainDataFrame.Top + MainDataFrame.Height + Dates_Frame.Height - 270
        
    End If
    SulSAT.Frame(Frame_ID).Width = 75
    SulSAT.Frame(Frame_ID).Visible = True
    
    SulSAT.Frame(Frame_ID).Left = 120
    While SulSAT.Frame(Frame_ID).Width < Dates_Frame.Left + Dates_Frame.Width
        SulSAT.Frame(Frame_ID).Width = SulSAT.Frame(Frame_ID).Width + 75
        'Wait 1
    Wend
    SulSAT.Frame(Frame_ID).Width = Dates_Frame.Left + Dates_Frame.Width - 100
End Sub

Private Sub HideCodes(FrameID As Integer)
    With Frame_Codes
        Select Case FrameID
            Case 0  'Balances
                .Visible = True
            Case 1  'Refills
                .Visible = True
            Case 2  'Configurations
                .Visible = False
            Case 3  'Accumulators
                .Visible = True
        End Select
    End With
End Sub

Public Sub Load_Click()
'   Call Dim_Undim_All(False)
    Call Reset_TimeOut_Counter
    Call LockDates(False)
    If MSISDN_Selected Then
        Call Slide_Frame(0)
        Call Clear_Main_Data
        Call Clear_Balances
        Call ClearUCIP_Request_Data
        UCIP_Request_Data.subscriberNumber = MSISDNs_List
        Call Update_Response("Get Account Details: " + Response_Code(GetAccountDetailsT_Command(UCIP_Request_Data.subscriberNumber)))
        Call Fill
    End If
'    Call Dim_Undim_All(True)
End Sub
Private Function MSISDN_Selected() As Boolean
    If MSISDNs_List.Text <> "" Then
        MSISDN_Selected = True
    Else
        MSISDN_Selected = False
        MsgBox "Please Enter a MSISDN Before Invoking Any Of The Command Buttons", vbCritical, "MSISDN Error"
    End If
End Function
Private Sub Clear_Main_Data()
    With SulSAT
        .supervisionDate = ""
        .supervisionPeriod = ""
        .serviceFeeDate = ""
        .serviceFeePeriod = ""
        .serviceClassCurrent = ""
        .ServiceClass_Desc = ""
        .Language_Desc = ""
        .currentLanguageID = ""
        .Sub_Status = ""
        .refillFraudCount = ""
        .removalPeriod = ""
        .creditClearanceDate = ""
        .creditClearancePeriod = ""
        .serviceRemovalDate = ""
    End With
End Sub
Private Sub Clear_UnEnable_Accumulators()
    With SulSAT
        For i = 1 To 5
            .Accumulator(i) = ""
            .AccumulatorResetDate(i) = ""
            .AccumulatorStartDate(i) = ""
            .Accumulator(i).Enabled = False
            .AccumulatorResetDate(i).Enabled = False
            .AccumulatorStartDate(i).Enabled = False
        Next i
    End With
End Sub

Private Sub Clear_Balances()
    With SulSAT
        For i = 1 To 5
            .DedicateAccount(i) = ""
            .DedicatedAccountExpiryDate(i) = ""
        Next i
    End With
End Sub
Private Sub Fill(Optional WithRemainValues As Boolean)
        Last_UCIP_Response = UCIP_Response_Data
        With Me
            .accountValue1 = Val(UCIP_Response_Data.accountValue1) / 100
            .currentLanguageID = UCIP_Response_Data.currentLanguageID
            .serviceClassCurrent = UCIP_Response_Data.serviceClassCurrent
            .creditClearancePeriod = UCIP_Response_Data.creditClearancePeriod
            .serviceFeePeriod = UCIP_Response_Data.serviceFeePeriod
            .removalPeriod = UCIP_Response_Data.removalPeriod
            .supervisionPeriod = UCIP_Response_Data.supervisionPeriod
            
            .supervisionDate = Date_Only(UCIP_Response_Data.supervisionDate)
            .creditClearanceDate = Date_Only(UCIP_Response_Data.creditClearanceDate)
            .serviceRemovalDate = Date_Only(UCIP_Response_Data.serviceRemovalDate)
            .serviceFeeDate = Date_Only(UCIP_Response_Data.serviceFeeDate)
            
            'Filling Dedicated Accounts
            For i = 1 To NumberOfDedicatedAccounts
                If UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue <> "" Then
                    .DedicateAccount(i) = Val(UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue) / 100
                    .DedicatedAccountExpiryDate(i) = Date_Only(UCIP_Response_Data.DedicatedAccount(i).DedicateAccountExpiryDate)
                Else
                    .DedicateAccount(i) = ""
                    .DedicatedAccountExpiryDate(i) = ""
                End If
            Next i
            
            'filling accumulators
            For i = 1 To NumberOfAccumulators
                If UCIP_Response_Data.Accumulator(i).accumulatorValue <> "" Then
                    .Accumulator(i) = Val(UCIP_Response_Data.Accumulator(i).accumulatorValue)
                    .AccumulatorStartDate(i) = Date_Only(UCIP_Response_Data.Accumulator(i).AccumulatorStartDate)
                    .AccumulatorResetDate(i) = Date_Only(UCIP_Response_Data.Accumulator(i).accumulatorEndDate)
                Else
                    .Accumulator(i) = ""
                    .AccumulatorStartDate(i) = ""
                    .AccumulatorResetDate(i) = ""
                End If
            Next i
            
            
            Select Case UCIP_Response_Data.accountFlags
                Case "10001111" 'Grace/AG
                    .Sub_Status = " "
                Case "10001000"
                    .Sub_Status = "Supervision"
                    Label11.Enabled = True
                    Label13.Enabled = True
                    Label14.Enabled = True
                    Label15.Enabled = True
                    Label7.Enabled = True
                    Label6.Enabled = True
                    Label8.Enabled = True
                    Label5.Enabled = True
                Case "10000000"
                    .Sub_Status = "Active"
                    Label11.Enabled = True
                    Label13.Enabled = True
                    Label14.Enabled = True
                    Label15.Enabled = True
                    Label7.Enabled = True
                    Label6.Enabled = True
                    Label8.Enabled = True
                    Label5.Enabled = True
                Case "00000000"
                    .Sub_Status = "Not Active"
                    .supervisionDate = ""
                    .serviceClassCurrent = UCIP_Response_Data.serviceClassCurrent
                    .creditClearanceDate = ""
                    .serviceRemovalDate = ""
                    .serviceFeeDate = ""
                    .supervisionPeriod = ""
                    .removalPeriod = ""
                    .serviceFeePeriod = ""
                    .creditClearancePeriod = ""
                    Label11.Enabled = False
                    Label13.Enabled = False
                    Label14.Enabled = False
                    Label15.Enabled = False
                    Label7.Enabled = False
                    Label6.Enabled = False
                    Label8.Enabled = False
                    Label5.Enabled = False
            End Select
            If WithRemainValues And UCIP_Response_Data.ResponseCode = "0" Then
                If UCIP_Response_Data.supervisionDate <> "" Then .creditClearancePeriod = DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.supervisionDate)), ToDate(Date_Only(UCIP_Response_Data.creditClearanceDate)))
                If UCIP_Response_Data.serviceFeeDate <> "" Then .serviceFeePeriod = -DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.serviceFeeDate)), Date)
                If UCIP_Response_Data.serviceFeeDate <> "" Then .removalPeriod = DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.serviceFeeDate)), ToDate(Date_Only(UCIP_Response_Data.serviceRemovalDate)))
                If UCIP_Response_Data.supervisionDate <> "" Then .supervisionPeriod = -DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.supervisionDate)), Date)
                Last_UCIP_Response.creditClearancePeriod = .creditClearancePeriod
                Last_UCIP_Response.serviceFeePeriod = .serviceFeePeriod
                Last_UCIP_Response.removalPeriod = .removalPeriod
                Last_UCIP_Response.supervisionPeriod = .supervisionPeriod
            End If
        End With
    
    
End Sub
Private Sub ClearRsponseData()
        
        With Me
            .accountValue1 = Val(UCIP_Response_Data.accountValue1) / 100
            .currentLanguageID = UCIP_Response_Data.currentLanguageID
            .serviceClassCurrent = UCIP_Response_Data.serviceClassCurrent
            .creditClearancePeriod = UCIP_Response_Data.creditClearancePeriod
            .serviceFeePeriod = UCIP_Response_Data.serviceFeePeriod
            .removalPeriod = UCIP_Response_Data.removalPeriod
            .supervisionPeriod = UCIP_Response_Data.supervisionPeriod
            
            .supervisionDate = Date_Only(UCIP_Response_Data.supervisionDate)
            .creditClearanceDate = Date_Only(UCIP_Response_Data.creditClearanceDate)
            .serviceRemovalDate = Date_Only(UCIP_Response_Data.serviceRemovalDate)
            .serviceFeeDate = Date_Only(UCIP_Response_Data.serviceFeeDate)
            
            'Filling Dedicated Accounts
            For i = 1 To NumberOfDedicatedAccounts
                If UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue <> "" Then
                    .DedicateAccount(i) = Val(UCIP_Response_Data.DedicatedAccount(i).DedicatedAccountValue) / 100
                    .DedicatedAccountExpiryDate(i) = Date_Only(UCIP_Response_Data.DedicatedAccount(i).DedicateAccountExpiryDate)
                Else
                    .DedicateAccount(i) = ""
                    .DedicatedAccountExpiryDate(i) = ""
                End If
            Next i
            
            'filling accumulators
            For i = 1 To NumberOfAccumulators
                If UCIP_Response_Data.Accumulator(i).accumulatorValue <> "" Then
                    .Accumulator(i) = Val(UCIP_Response_Data.Accumulator(i).accumulatorValue)
                    .AccumulatorStartDate(i) = Date_Only(UCIP_Response_Data.Accumulator(i).AccumulatorStartDate)
                    .AccumulatorResetDate(i) = Date_Only(UCIP_Response_Data.Accumulator(i).accumulatorEndDate)
                Else
                    .Accumulator(i) = ""
                    .AccumulatorStartDate(i) = ""
                    .AccumulatorResetDate(i) = ""
                End If
            Next i
            
            
            Select Case UCIP_Response_Data.accountFlags
                Case "10001111" 'Grace/AG
                    .Sub_Status = " "
                Case "10000000"
                    .Sub_Status = "Active"
                    Label11.Enabled = True
                    Label13.Enabled = True
                    Label14.Enabled = True
                    Label15.Enabled = True
                    Label7.Enabled = True
                    Label6.Enabled = True
                    Label8.Enabled = True
                    Label5.Enabled = True
                Case "00000000"
                    .Sub_Status = "Not Active"
                    .supervisionDate = ""
                    .serviceClassCurrent = UCIP_Response_Data.serviceClassCurrent
                    .creditClearanceDate = ""
                    .serviceRemovalDate = ""
                    .serviceFeeDate = ""
                    .supervisionPeriod = ""
                    .removalPeriod = ""
                    .serviceFeePeriod = ""
                    .creditClearancePeriod = ""
                    Label11.Enabled = False
                    Label13.Enabled = False
                    Label14.Enabled = False
                    Label15.Enabled = False
                    Label7.Enabled = False
                    Label6.Enabled = False
                    Label8.Enabled = False
                    Label5.Enabled = False
            End Select
            If WithRemainValues And UCIP_Response_Data.ResponseCode = "0" Then
                If UCIP_Response_Data.supervisionDate <> "" Then .creditClearancePeriod = DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.supervisionDate)), ToDate(Date_Only(UCIP_Response_Data.creditClearanceDate)))
                If UCIP_Response_Data.serviceFeeDate <> "" Then .serviceFeePeriod = -DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.serviceFeeDate)), Date)
                If UCIP_Response_Data.serviceFeeDate <> "" Then .removalPeriod = DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.serviceFeeDate)), ToDate(Date_Only(UCIP_Response_Data.serviceRemovalDate)))
                If UCIP_Response_Data.supervisionDate <> "" Then .supervisionPeriod = -DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.supervisionDate)), Date)
                Last_UCIP_Response.creditClearancePeriod = .creditClearancePeriod
                Last_UCIP_Response.serviceFeePeriod = .serviceFeePeriod
                Last_UCIP_Response.removalPeriod = .removalPeriod
                Last_UCIP_Response.supervisionPeriod = .supervisionPeriod
            End If
        End With
    
    
End Sub

Public Function Date_Formated(ByVal DateTime As String) As String
    Date_Formated = Mid(SulSAT.Date_Inverted(DateTime), 5, 2) & "/" & Mid(SulSAT.Date_Inverted(DateTime), 7, 2) & "/" & Mid(SulSAT.Date_Inverted(DateTime), 1, 4)
End Function
Public Function Date_Only(ByRef DateTtime As String)
    Date_Only = Mid(DateTtime, 7, 2) + "\" + Mid(DateTtime, 5, 2) + "\" + Mid(DateTtime, 1, 4)
End Function
Public Function Date_Inverted(ByRef DateTtime As String)
    Dim Dates As CCPAI_Parser_Return
    DateTtime = Replace(DateTtime, "/", "\")
    Dates = Parser(DateTtime & "\", "\")
    If Dates.Found And Dates.Ret(0) <> "" And DateTtime <> "" Then
        If Len(Dates.Ret(1)) <= 1 Then Dates.Ret(1) = "0" & Dates.Ret(1)
        If Len(Dates.Ret(0)) <= 1 Then Dates.Ret(0) = "0" & Dates.Ret(0)
        Date_Inverted = Dates.Ret(2) & Dates.Ret(1) & Dates.Ret(0)
    Else
        Date_Inverted = ""
    End If
End Function
Private Sub LoadAll_Click()
    Call Reset_TimeOut_Counter
    If MSISDN_Selected Then
        If Check_Connectivity = 0 Then
            MsgBox "Sorry you can't connect since there is no local connection"
        Else
            Call Load_Click
            If UCIP_Response_Data.ResponseCode = "0" Then Call GetBalances_Click
        End If
    End If
End Sub

Private Sub LogOut_Click()
    Call Reset_TimeOut_Counter
    Conn.Close
    Call Slide(Me, Me.Left, Me.Width, False, 2)
    Dim TXT As Control
    For Each TXT In Users
        If TypeOf TXT Is TextBox Then
            TXT = ""
        End If
'        If TypeOf TXT Is Label Then
'            If TXT.BorderStyle = 1 Then TXT = ""
'        End If
    Next TXT
    Call Slide(Users, Users.Left, Users.Width, True, 2)
    Users.UserName.SetFocus
End Sub

Private Sub MSISDNs_List_GotFocus()
    SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub MSISDNs_List_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(MSISDNs_List.Text) = 0) Then
        SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, 0, 1
        Call GetBalances_Click
    End If
    If KeyAscii = 32 And Len(MSISDNs_List.Text) = 0 Then KeyAscii = 0
End Sub

Private Sub MSISDNs_List_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = MSISDNs_List.Text
        For i = 0 To MSISDNs_List.ListCount - 1
            If StrComp(PSTR, (Left(MSISDNs_List.List(i), Len(PSTR))), vbTextCompare) = 0 Then
                MSISDNs_List.ListIndex = i
            Exit For
            End If
        Next i
        MSISDNs_List.SelStart = Len(PSTR)
        MSISDNs_List.SelLength = Len(MSISDNs_List.Text) - Len(PSTR)
    End If
End Sub

Private Sub MSISDNs_List_LostFocus()
    Call Save_List(MSISDNs_List, "MSISDNS", True)
    Call Roll_Out_Msisdns(MSISDNs_List)
End Sub

Private Sub Refill_Amount_LE_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Refill_Amount_Pt_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Refill_Click()
    If RefillProfileID <> "" Then
        If Refill_Amount_LE = "" Then Refill_Amount_LE = "0"
        If Refill_Amount_Pt = "" Then Refill_Amount_Pt = "0"
        
        Call ClearUCIP_Request_Data
        UCIP_Request_Data.TransactionAmountRefill = Str(Refill_Amount_LE * 100 + Refill_Amount_Pt)
        UCIP_Request_Data.paymentProfileID = RefillProfileID.Text
        UCIP_Request_Data.subscriberNumberNAI = ""
        UCIP_Request_Data.externalData1 = ""
        UCIP_Request_Data.externalData2 = ""
        
        Call Update_Response("Refill: " + Response_Code(RefillT_Command(MSISDNs_List, _
                                                    UCIP_Request_Data.TransactionAmountRefill, _
                                                    UCIP_Request_Data.paymentProfileID, _
                                                    False, _
                                                    UCIP_Request_Data.subscriberNumberNAI, _
                                                    UCIP_Request_Data.externalData1, _
                                                    UCIP_Request_Data.externalData2)))
        If UCIP_Response_Data.ResponseCode = "0" Then Call GetBalances_Click
        Call Fill(True)
        Refill_Amount_LE = ""
        Refill_Amount_Pt = ""
        RefillProfileID.Text = ""
        Activation_Number = ""
    Else
        If Activation_Number.Text <> "" Then
            If Len(Activation_Number) <> 14 Then
                MsgBox "Please Enter Activation Number in 14 Digits", , "Length Error"
            Else
                
                Call ClearUCIP_Request_Data
                UCIP_Request_Data.activationNumber = Activation_Number
                UCIP_Request_Data.messageCapabilityFlag = ""
                UCIP_Request_Data.subscriberNumberNAI = ""
                UCIP_Request_Data.externalData1 = ""
                UCIP_Request_Data.externalData2 = ""
        
                Call Update_Response("Standard Voucher Refill: " + Response_Code(StandardVoucherRefillT_Command(MSISDNs_List, _
                                                                                                            UCIP_Request_Data.activationNumber, _
                                                                                                            , _
                                                                                                            UCIP_Request_Data.messageCapabilityFlag, _
                                                                                                            UCIP_Request_Data.externalData1, _
                                                                                                            UCIP_Request_Data.externalData2)))
                If UCIP_Response_Data.ResponseCode = "0" Then
                    Call GetBalances_Click
                    Call Fill(True)
                Else
                    refillFraudCount = UCIP_Response_Data.refillFraudCount
                End If
                Refill_Amount_LE = ""
                Refill_Amount_Pt = ""
                RefillProfileID.Text = ""
                Activation_Number = ""
            End If
        Else
            MsgBox "You Have To Enter A Refill Profile ID Or An Activation Number", , "Error"
        End If
    End If
    

End Sub

Private Sub RefillProfileID_KeyPress(KeyAscii As Integer)
    'Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Refill_Click
End Sub

Private Sub Refills_Click()
    Call Reset_TimeOut_Counter
    If MSISDN_Selected Then Call Slide_Frame(1)
    Call LockDates(True)
End Sub
Public Sub Save_Click()
    Call Save_All
End Sub
Private Function CheckLastResponse() As Boolean
    CheckLastResponse = True
    With Last_UCIP_Response
        If .accountFlags = "" And _
            .accountValue1 = "" And _
            .currentLanguageID = "" And _
            .ResponseCode = "" And _
            .accountValue1 = "" And _
            .serviceFeeDate = "" And _
            .supervisionDate = "" Then CheckLastResponse = False
    End With
End Function
Public Sub Save_All(Optional AdjCode As String, Optional AdjType As String)

    UCIP_Response_Data.ResponseCode = ""
    If CheckLastResponse Then
        'Language Update
        If Me.currentLanguageID <> Last_UCIP_Response.currentLanguageID Then
        
            Call ClearUCIP_Request_Data
            UCIP_Request_Data.currentLanguageID = Last_UCIP_Response.currentLanguageID
            UCIP_Request_Data.newLanguageID = Me.currentLanguageID
            
            Update_Response ("Update Account Details: " + Response_Code(UpdateAccountDetailsT_Command(MSISDNs_List, UCIP_Request_Data.currentLanguageID, UCIP_Request_Data.newLanguageID)))
            If UCIP_Response_Data.ResponseCode = "0" Then Last_UCIP_Response.currentLanguageID = SulSAT.currentLanguageID
        End If
            
        'Service Class Update
        If Me.serviceClassCurrent <> Last_UCIP_Response.serviceClassCurrent Then
                
            Call ClearUCIP_Request_Data
            UCIP_Request_Data.serviceClassCurrent = Last_UCIP_Response.serviceClassCurrent
            UCIP_Request_Data.serviceClassNew = Me.serviceClassCurrent
            
            Update_Response ("Update Service Class: " + Response_Code(UpdateServiceClassT_Command(MSISDNs_List, UCIP_Request_Data.serviceClassCurrent, UCIP_Request_Data.serviceClassNew)))
            If UCIP_Response_Data.ResponseCode = "0" Then Last_UCIP_Response.serviceClassCurrent = SulSAT.serviceClassCurrent
        End If
        
        'Adjustment Update
        If Sub_Status <> "" And Sub_Status <> "Not Active" Then
            If Val(Me.accountValue1 * 100) <> Val(Last_UCIP_Response.accountValue1) Or _
                    Me.serviceFeePeriod <> Last_UCIP_Response.serviceFeePeriod Or _
                    Me.supervisionPeriod <> Last_UCIP_Response.supervisionPeriod Then
                        If (AdjCode = "" Or AdjType = "") And Form_Codes.ID = "" Then
                            Dim_Undim_All (False)
                            Call Slide(Form_Codes, Int((SulSAT.Left + SulSAT.Width) / 2), Form_Codes.Width, True, 2)
                        Else
                            Call ClearUCIP_Request_Data
                            UCIP_Request_Data.adjustmentAmount = Fix(Me.accountValue1 * 100 - Last_UCIP_Response.accountValue1)
                            UCIP_Request_Data.relativeDateAdjustmentSupervision = Me.supervisionPeriod - Last_UCIP_Response.supervisionPeriod
                            UCIP_Request_Data.relativeDateAdjustmentServiceFee = Me.serviceFeePeriod - Last_UCIP_Response.serviceFeePeriod
                            UCIP_Request_Data.subscriberNumberNAI = ""
                            UCIP_Request_Data.externalData1 = AdjCode
                            UCIP_Request_Data.externalData2 = AdjType
                        
                            Call Update_Response("Adjustment: " + Response_Code(AdjustmentT_Command(MSISDNs_List, _
                                                                                                UCIP_Request_Data.adjustmentAmount, _
                                                                                                False, _
                                                                                                UCIP_Request_Data.relativeDateAdjustmentSupervision, _
                                                                                                UCIP_Request_Data.relativeDateAdjustmentServiceFee, _
                                                                                                UCIP_Request_Data.subscriberNumberNAI, _
                                                                                                , _
                                                                                                UCIP_Request_Data.externalData1, _
                                                                                                UCIP_Request_Data.externalData2)))
                            Call Response_Code(BalanceEnquiryT_Command(MSISDNs_List))
                            Call Fill(True)
                        End If
            End If
            'Dates Checks
            If ValidateDate(Date_Only(Last_UCIP_Response.supervisionDate)) And _
                ValidateDate(Date_Only(Last_UCIP_Response.serviceFeeDate)) And _
                ValidateDate(Me.supervisionDate) And _
                ValidateDate(Me.serviceFeeDate) Then
                
                If Me.serviceFeeDate <> Date_Only(Last_UCIP_Response.serviceFeeDate) Or _
                        Me.supervisionDate <> Date_Only(Last_UCIP_Response.supervisionDate) Then
                    If (AdjCode = "" Or AdjType = "") And Form_Codes.ID = "" Then
                        Dim_Undim_All (False)
                        Call Slide(Form_Codes, Int((SulSAT.Left + SulSAT.Width) / 2), Form_Codes.Width, True, 2)
                        Form_Codes.AdjType.SetFocus
                    Else
                        Dim DifferenceDays_SuperVision, DifferenceDays_ServiceFee As Integer
                        DifferenceDays_SuperVision = DateDiff("d", ToDate(Date_Only(Last_UCIP_Response.supervisionDate)), ToDate(Me.supervisionDate))
                        DifferenceDays_ServiceFee = DateDiff("d", ToDate(Date_Only(Last_UCIP_Response.serviceFeeDate)), ToDate(Me.serviceFeeDate))
                                
                        Call ClearUCIP_Request_Data
                        UCIP_Request_Data.adjustmentAmount = Fix((Me.accountValue1 * 100) - Val(Last_UCIP_Response.accountValue1))
                        UCIP_Request_Data.relativeDateAdjustmentSupervision = Trim(Str(DifferenceDays_SuperVision))
                        UCIP_Request_Data.relativeDateAdjustmentServiceFee = Trim(Str(DifferenceDays_ServiceFee))
                        UCIP_Request_Data.subscriberNumberNAI = ""
                        UCIP_Request_Data.externalData1 = AdjCode
                        UCIP_Request_Data.externalData2 = AdjType
                        
                        Call Update_Response("Adjustment: " + Response_Code(AdjustmentT_Command(MSISDNs_List, _
                                                                            UCIP_Request_Data.adjustmentAmount, _
                                                                            False, _
                                                                            UCIP_Request_Data.relativeDateAdjustmentSupervision, _
                                                                            UCIP_Request_Data.relativeDateAdjustmentServiceFee, _
                                                                            UCIP_Request_Data.subscriberNumberNAI, _
                                                                            , _
                                                                            UCIP_Request_Data.externalData1, _
                                                                            UCIP_Request_Data.externalData2)))
                        Call Response_Code(BalanceEnquiryT_Command(MSISDNs_List))
                        Call Fill(True)
                    End If
                End If
            Else
                MsgBox "PLZ Enter A Valid Date In The Format DD\MM\YYYY", , "Date Error"
                If Not ValidateDate(Me.supervisionDate) Then Me.supervisionDate = Date_Only(Last_UCIP_Response.supervisionDate)
                If Not ValidateDate(Me.serviceFeeDate) Then Me.serviceFeeDate = Date_Only(Last_UCIP_Response.serviceFeeDate)
            End If
        End If
    End If
End Sub
Public Sub Dim_Undim_All(Enable As Boolean)
    Dim Ctrs As Control
    For Each Ctrs In SulSAT
        If TypeOf Ctrs Is CommandButton Or TypeOf Ctrs Is TextBox Then Ctrs.Enabled = Enable
    Next Ctrs
    
    'For Each Ctrs In Form_Codes
    '    If TypeOf Ctrs Is CommandButton Or TypeOf Ctrs Is ComboBox Then Ctrs.Enabled = Enable
    'Next Ctrs
    
End Sub


Private Sub serviceClassCurrent_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub serviceFeeDate_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub serviceFeePeriod_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Public Sub ShoW_UCIP_Click()
    Call Reset_TimeOut_Counter
    Call Slide(UCIP, 1000, UCIP.Width, True)
End Sub
Public Sub Update_Response(ByRef Response_Code_TExt As String)
    Dim CommandName As String
    Dim Column_Pos As Integer
    
    Column_Pos = InStr(1, Response_Code_TExt, ":")
    CommandName = Left(Response_Code_TExt, Column_Pos - 1)
    
    Call Add_Transaction(CommandName, Trim(Right(Response_Code_TExt, Len(Response_Code_TExt) - Column_Pos - 1)))
    
    Success = Success + vbCrLf + Response_Code_TExt
    Success.SelStart = Len(Success)
    Response_TExt = Response_Code_TExt
End Sub

Private Sub ShowResponses_botton_Click()
        If Not ShowResponses_botton.Value Then
            
            SulSat_Dimensions.Height = Me.Height - 2300
            Me.Height = Me.Height - 2300
            ShowResponses_botton.Value = 0
        Else
            SulSat_Dimensions.Height = Me.Height + 2300
            Me.Height = Me.Height + 2300
            ShowResponses_botton.Value = 1
    End If
End Sub

Private Sub supervisionDate_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub supervisionPeriod_KeyPress(KeyAscii As Integer)
    Call Validicate_Numbers(KeyAscii)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Function ValidateDate(Date_String As String) As Boolean
    Dim days As Boolean
    Dim Slash As Boolean
    Dim Month As Boolean
    Dim Year As Boolean
    Dim Length As Boolean
    
    Dim DD, MM, YYYY, Loc1, Loc2 As Integer
    
    Loc1 = InStr(1, Date_String, "\")
    If Loc1 = 0 Then
        ValidateDate = False
        Exit Function
    End If
    
    DD = Val(Mid(Date_String, 1, Loc1 - 1))
    If Len(DD) = 0 Or Len(DD) > 2 Or DD > 31 Then
        ValidateDate = False
        Exit Function
    End If
        
    Loc2 = InStr(Loc1 + 1, Date_String, "\")
    If Loc2 = 0 Then
        ValidateDate = False
        Exit Function
    End If
    
    MM = Val(Mid(Date_String, Loc1 + 1, Loc2 - Loc1 - 1))
    If Len(MM) = 0 Or Len(MM) > 2 Or MM > 12 Then
        ValidateDate = False
        Exit Function
    End If
    
    YYYY = Val(Mid(Date_String, Loc2 + 1, 4))
    If Len(YYYY) = 0 Or Len(YYYY) > 4 Then
        ValidateDate = False
        Exit Function
    End If
    
    Dim Temp_char As String
    For i = 1 To Len(Date_String)
        If Not (Asc(Mid(Date_String, i, 1)) > 48 Or Asc(Mid(Date_String, i, 1)) <= 57 Or Asc(Mid(Date_String, i, 1)) = 92) Then   'not number
            ValidateDate = False
            Exit Function
        End If
    Next i
    
    If DD = 31 Then
        If Not (MM = 1 Or _
                MM = 3 Or _
                MM = 5 Or _
                MM = 7 Or _
                MM = 8 Or _
                MM = 10 Or _
                MM = 12) Then
                    ValidateDate = False
                    Exit Function
        End If
    End If
    If DD > 28 And MM = 2 Then
        If YYYY Mod 4 <> 0 Then
            ValidateDate = False
            Exit Function
        End If
    End If
    
    ValidateDate = True
End Function
Public Function ToDate(ByRef Date_String As String) As String
    'If instr(Date_String Then
    If True Then
        Dim DD, MM, YYYY, Loc1, Loc2 As Integer
        
        Loc1 = InStr(1, Date_String, "\")
        DD = Val(Mid(Date_String, 1, Loc1 - 1))
        Loc2 = InStr(Loc1 + 1, Date_String, "\")
        MM = Val(Mid(Date_String, Loc1 + 1, Loc2 - Loc1 - 1))
        YYYY = Val(Mid(Date_String, Loc2 + 1, 4))
        
        
        Select Case MM
            Case 1
                ToDate = Str(DD) + " Jan " + Str(YYYY)
            Case 2
                ToDate = Str(DD) + " Feb " + Str(YYYY)
            Case 3
                ToDate = Str(DD) + " Mar " + Str(YYYY)
            Case 4
                ToDate = Str(DD) + " Apr " + Str(YYYY)
            Case 5
                ToDate = Str(DD) + " May " + Str(YYYY)
            Case 6
                ToDate = Str(DD) + " Jun " + Str(YYYY)
            Case 7
                ToDate = Str(DD) + " Jul " + Str(YYYY)
            Case 8
                ToDate = Str(DD) + " Aug " + Str(YYYY)
            Case 9
                ToDate = Str(DD) + " Sep " + Str(YYYY)
            Case 10
                ToDate = Str(DD) + " Oct " + Str(YYYY)
            Case 11
                ToDate = Str(DD) + " Nov " + Str(YYYY)
            Case 12
                ToDate = Str(DD) + " Dec " + Str(YYYY)
        End Select
    End If
End Function

Public Function Subtract_Dates(ByRef Date1 As String, ByRef Date2 As String) As Integer
    ' the Date must be in format DD/MM/YYYY
   Dim DD1, MM1, YYYY1 As Integer
   Dim DD2, MM2, YYYY2 As Integer
   Dim DD_Reminder, MM_Reminder, YYYY_Reminder As Integer
   
   DD1 = Val(Mid(Date1, 1, 2))
   MM1 = Val(Mid(Date1, 4, 2))
   YYYY1 = Val(Mid(Date1, 7, 11))
   
   DD2 = Val(Mid(Date2, 1, 2))
   MM2 = Val(Mid(Date2, 4, 2))
   YYYY2 = Val(Mid(Date2, 7, 11))
   
   Select Case MM1
        Case 1, 3, 5, 7, 8, 10, 12
            MM1 = MM1 * 31
        Case 4, 6, 9, 11
            MM1 = MM1 * 30
        Case 2
            MM1 = MM1 * 28
   End Select
   
   YYYY_Reminder = (YYYY1 - YYYY2) * 365
   MM_Reminder = (MM1 - MM2)
   DD_Reminder = DD1 - DD2
        
End Function


