VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Client"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Copyright 2007-2008 T.J Chear
'use or change it any way you see fit.
'but whenever you do so, please put me in the credits.

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Function GetTickCount Lib "kernel32" () As Long

Public Enum DATA_TYPE
    TYPE_ASCII
    TYPE_BINARY
    TYPE_EBCDIC
End Enum

Public Enum CLIENT_STATE
    DISCONNECTED
    DISCONNECTING
    CONNECTED
    CONNECTING
End Enum

Public Enum TRANSFER_TYPE
    UPLOAD
    UPLOAD_RESUME
    DOWNLOAD
    DOWNLOAD_RESUME
    APPEND
    LISTING
    NLISTING
End Enum

Public Enum FTP_REPLY_CODE
    RESTART_MARKER_REPLY_101 = 101
    SERVICE_READY_SOON_120 = 120
    TRANSFER_STARTING_125 = 125
    INITIATING_DATA_CONNECTION_150 = 150
    COMMAND_OK_200 = 200
    COMMAND_UNSUPPORTED_202 = 202
    SYSTEM_STATUS_211 = 211
    DIRECTORY_STATUS_212 = 212
    FILE_STATUS_213 = 213
    HELP_MESSAGE_214 = 214
    NAME_SYSTEM_TYPE_215 = 215
    SERVICE_READY_220 = 220
    CLOSING_CONTROL_CONNECTION_221 = 221
    DATA_OPEN_NO_TRANSFER_225 = 225
    CLOSING_DATA_CONNECTION_226 = 226
    ENTERING_PASSIVE_MODE_227 = 227
    USER_LOGGED_IN_230 = 230
    FILE_ACTION_COMPLETED_250 = 250
    PATH_CREATED_257 = 257
    USER_OK_NEED_PASSWORD_331 = 331
    LOGIN_NEED_ACCOUNT_332 = 332
    FILE_ACTION_PENDING_350 = 350
    SERVICE_SHUTTING_DOWN_421 = 421
    DATA_INITIATION_FAILED_425 = 425
    TRANSFER_ABORTION_426 = 426
    FILE_UNAVAILABLE_450 = 450
    ACTION_ABORTED_LOCAL_ERROR_451 = 451
    FILE_ACTION_FAILED_SPACELESS_452 = 452
    SYNTAX_ERROR_COMMAND_500 = 500
    SYNTAX_ERROR_PARAMETER_501 = 501
    COMMAND_NOT_IMPLEMENTED_502 = 502
    BAD_SEQUENCE_COMMAND_503 = 503
    COMMAND_NOT_IMPLEMENTED_PARAMETER_504 = 504
    NOT_LOGGED_IN_530 = 530
    ACCOUNT_REQUIRED_532 = 532
    FILE_NO_ACCESS_550 = 550
    ACTION_ABORTED_UNKNOWN_PAGE_TYPE_551 = 551
    FILE_ACTION_FAILED_SPACE_EXCEEDED_552 = 552
    FILE_ACTION_FAILED_NAME_DISALLOWED_553 = 553
End Enum

Private Const SOCKET_TIME_OUT As Long = 3000
Private WithEvents sckControl As CSocket
Attribute sckControl.VB_VarHelpID = -1
Private WithEvents sckdata As CSocket
Attribute sckdata.VB_VarHelpID = -1

Private m_host As String
Private m_port As Long
Private m_user As String
Private m_pass As String
Private m_account As String
Private m_async As Boolean
Private m_pasv As Boolean
Private m_logged As Boolean
Private m_state As CLIENT_STATE
Private m_data_type As DATA_TYPE
Private m_last_reply As String
Private m_last_reply_code As FTP_REPLY_CODE

Private m_buffer As String
Private m_processing As Boolean

Private m_data_open As Boolean
Private m_transferring As Boolean
Private m_transfer As TRANSFER_TYPE
Private m_data As String
Private m_data_sent As Boolean
Private m_list As ListParser
Private m_pasv_ip As String
Private m_pasv_port As Long

Public Event OnStateChanged()
Public Event OnConnected()
Public Event OnDisconnected()
Public Event OnLoggedIn()
Public Event OnIncomingReply()

Public Event OnTransferStarted()
Public Event OnNeedData()
Public Event OnIncomingData()
Public Event OnTransferEnded()

Public Property Get RemoteHost() As String
    RemoteHost = m_host
End Property

Public Property Let RemoteHost(s As String)
    If State = DISCONNECTED Then
        m_host = s
    End If
End Property

Public Property Get RemotePort() As Long
    RemotePort = m_port
End Property

Public Property Let RemotePort(p As Long)
    If State = DISCONNECTED Then
        m_port = p
    End If
End Property

Public Property Get User() As String
    User = m_user
End Property

Public Property Let User(s As String)
    If State = DISCONNECTED Then
        m_user = s
    End If
End Property

Public Property Get Password() As String
    Password = m_pass
End Property

Public Property Let Password(s As String)
    If State = DISCONNECTED Then
        m_pass = s
    End If
End Property

Public Property Get Account() As String
    Account = m_account
End Property

Public Property Let Account(s As String)
    If State = DISCONNECTED Then
        m_account = s
    End If
End Property

Public Property Get State() As CLIENT_STATE
    State = m_state
End Property

Friend Property Let State(s As CLIENT_STATE)
    m_state = s
    If State <> CONNECTED Then
        m_logged = False
    End If
    RaiseEvent OnStateChanged
End Property

Public Property Get DataType() As DATA_TYPE
    DataType = m_data_type
End Property

Public Property Get IsDataSocketOpen() As Boolean
    IsDataSocketOpen = m_data_open
End Property

Friend Property Let IsDataSocketOpen(b As Boolean)
    m_data_open = b
End Property

Public Property Get IsTransferring() As Boolean
    IsTransferring = m_transferring
End Property

Friend Property Let IsTransferring(b As Boolean)
    If (Not m_transferring) And b Then
        m_transferring = b
        RaiseEvent OnTransferStarted
    Else
        m_transferring = b
    End If
End Property

Public Property Get TransferType() As TRANSFER_TYPE
    TransferType = m_transfer
End Property

Public Property Get IsDataSent() As Boolean
    IsDataSent = m_data_sent
End Property

Public Property Get IsPassive() As Boolean
    IsPassive = m_pasv
End Property

Public Property Get IsLoggedIn() As Boolean
    IsLoggedIn = m_logged
End Property

Public Property Get IsDataBufferEmpty() As Boolean
    IsDataBufferEmpty = (Len(m_data) = 0)
End Property

Public Property Get fileList() As ListParser
    Set fileList = m_list
End Property

Public Function SetDataType(d As DATA_TYPE)
    If Not IsDataSocketOpen Then
        Select Case d
            Case DATA_TYPE.TYPE_ASCII
                SendCommand "TYPE A"
            Case DATA_TYPE.TYPE_BINARY
                SendCommand "TYPE I"
            Case DATA_TYPE.TYPE_EBCDIC
                SendCommand "TYPE E"
        End Select
        If WaitReplyCode(COMMAND_OK_200) Then
            m_data_type = d
        End If
    End If
End Function

Public Sub RunEvents()
    DoEvents
End Sub

Public Function Connect() As Boolean
    Disconnect
    If WaitControlState(sckClosed) Then
        sckControl.Connect m_host, m_port
        State = CONNECTING
        Connect = True
        Exit Function
    Else
        Connect = False
        Exit Function
    End If
End Function

Public Sub Disconnect()
    'sckControl.Close
    sckControl.CloseSocket
End Sub

Public Function Rename(OldName As String, NewName As String) As Boolean
    If IsLoggedIn Then
        SendCommand "RNFR " & OldName
        If WaitReplyCode(FILE_ACTION_PENDING_350) Then
            SendCommand "RNTO " & NewName
            If WaitReplyCode(FILE_ACTION_COMPLETED_250) Then
                Rename = True
                Exit Function
            End If
        End If
    End If
    Rename = False
End Function

Public Function ChangeDirectory(directory As String) As Boolean
    ChangeDirectory = False
    If IsLoggedIn Then
        SendCommand "CWD " & directory
        ChangeDirectory = WaitReplyCode(FILE_ACTION_COMPLETED_250)
        Exit Function
    End If
End Function

Public Function MakeDirectory(name As String) As Boolean
    MakeDirectory = False
    If IsLoggedIn Then
        SendCommand "MKD " & name
        MakeDirectory = WaitReplyCode(PATH_CREATED_257)
        Exit Function
    End If
End Function

Public Function GetWorkingDirectory() As String
    Dim iPos As Integer
    If IsLoggedIn Then
        SendCommand "PWD"
        If WaitReplyCode(PATH_CREATED_257) Then
            iPos = InStr(1, m_last_reply, Chr(34))
            If iPos > 0 Then
                GetWorkingDirectory = Mid$(m_last_reply, iPos + 1, InStr(iPos + 1, m_last_reply, Chr(34)) - iPos - 1)
            End If
        End If
    End If
End Function

Public Function RemoveDirectory(directory As String) As Boolean
    RemoveDirectory = False
    If IsLoggedIn Then
        SendCommand "RMD " & directory
        RemoveDirectory = WaitReplyCode(FILE_ACTION_COMPLETED_250)
        Exit Function
    End If
End Function

Public Function RemoveFile(file As String) As Boolean
    RemoveFile = False
    If IsLoggedIn Then
        SendCommand "DELE " & file
        RemoveFile = WaitReplyCode(FILE_ACTION_COMPLETED_250)
        Exit Function
    End If
End Function

Public Sub Quit()
    If State = CONNECTED Then
        SendCommand "QUIT"
    End If
End Sub

Public Sub AbortLastCommand()
    If State = CONNECTED Then
        SendCommand "ABOR"
    End If
End Sub

Public Function GetData() As String
    GetData = m_data
    m_data = ""
End Function

Private Function WaitControlState(State As StateConstants, Optional TimeOut As Long = -1) As Boolean
    Dim t As Long
    If t < 0 Then
        t = SOCKET_TIME_OUT
    End If
    t = GetTickCount + t
    Do Until GetTickCount > t
        If sckControl.State = State Then
            WaitControlState = True
            Exit Function
        End If
        DoEvents
    Loop
    WaitControlState = False
End Function

Private Function WaitDataState(State As StateConstants, Optional TimeOut As Long = -1) As Boolean
    Dim t As Long
    If t < 0 Then
        t = SOCKET_TIME_OUT
    End If
    t = GetTickCount + t
    Do Until GetTickCount > t
        If sckdata.State = State Then
            WaitDataState = True
            Exit Function
        End If
        DoEvents
    Loop
    WaitDataState = False
End Function

Public Function WaitReplyCode(code As FTP_REPLY_CODE, Optional TimeOut As Long = -1) As Boolean
    Dim t As Long
    Dim objWaiter As ReplyCodeWaiter
    Set objWaiter = New ReplyCodeWaiter
    Set objWaiter.Client = Me
    t = TimeOut
    If t < 0 Then
        t = SOCKET_TIME_OUT
    End If
    t = GetTickCount + t
    Do Until GetTickCount > t
        If objWaiter.LastReplyCode = code Then
            Set objWaiter = Nothing
            WaitReplyCode = True
            Exit Function
        End If
        DoEvents
    Loop
    Set objWaiter = Nothing
    WaitReplyCode = False
End Function

Public Sub SetPassiveMode(b As Boolean)
    m_pasv = b
End Sub

Public Function GetLastReply() As String
    GetLastReply = m_last_reply
End Function

Public Function GetLastReplyCode() As FTP_REPLY_CODE
    GetLastReplyCode = m_last_reply_code
End Function

Public Sub SendCommand(s As String)
    If State = CONNECTED Then
        If sckControl.State = sckConnected Then
            sckControl.SendData s & vbCrLf
        End If
    End If
End Sub

Public Function InitiateDataSocket() As Boolean
    If IsPassive Then
        If IsLoggedIn Then
            SendCommand "PASV"
            If WaitReplyCode(ENTERING_PASSIVE_MODE_227) Then
                CloseDataSocket
                If WaitDataState(sckClosed) Then
                    sckdata.RemoteHost = m_pasv_ip
                    sckdata.RemotePort = m_pasv_port
                    sckdata.Connect
                    InitiateDataSocket = True
                    Exit Function
                End If
            End If
        End If
    Else
        If IsLoggedIn Then
            If ActiveConnect Then
                If WaitReplyCode(COMMAND_OK_200) Then
                    InitiateDataSocket = True
                    Exit Function
                End If
            End If
        End If
    End If
    
    InitiateDataSocket = False
End Function

Public Sub CloseDataSocket()
    'sckdata.Close
    sckdata.CloseSocket
End Sub

Private Function ActiveConnect() As Boolean
    Dim LocalIP As String, Port As Long, cmd As String

    LocalIP = Replace(CStr(sckdata.LocalIP), ".", ",")
    On Error Resume Next
    
    Do
Again:
        Randomize Timer
        Port = CLng(Rnd * (32768 - 1100) + 1100)
        Disconnect
        If WaitDataState(sckClosed) = True Then
            sckdata.LocalPort = Port
            sckdata.Listen
            If Err Then
                Err.Clear
                GoTo Again
            End If
        Else
            ActiveConnect = False
            Exit Function
        End If
        If WaitDataState(sckListening) = True Then
            Exit Do
        End If
    Loop

    cmd = "PORT " & LocalIP & "," & CStr(Port \ 256) & "," & CStr(Port Mod 256)
    SendCommand cmd
    ActiveConnect = True
End Function

Public Sub StartTransfer(t As TRANSFER_TYPE, file As String, Optional position As Long = 1)
    m_transfer = t
    Select Case t
    Case TRANSFER_TYPE.DOWNLOAD
        If file <> "" Then
            SendCommand "RETR " & file
        End If
        
    Case TRANSFER_TYPE.DOWNLOAD_RESUME
        If file <> "" Then
            If position > 0 Then
                SendCommand "REST " & CStr(position)
                If WaitReplyCode(FILE_ACTION_PENDING_350) Then
                    SendCommand "RETR " & file
                End If
            End If
        End If
        
    Case TRANSFER_TYPE.UPLOAD
        If file <> "" Then
            SendCommand "STOR " & file
        End If
        
    Case TRANSFER_TYPE.UPLOAD_RESUME
        If file <> "" Then
            If position > 0 Then
                SendCommand "REST " & CStr(position)
                If WaitReplyCode(FILE_ACTION_PENDING_350) Then
                    SendCommand "STOR " & file
                End If
            End If
        End If
        
    Case TRANSFER_TYPE.APPEND
        If file <> "" Then
            SendCommand "APPE " & file
        End If
        
    Case TRANSFER_TYPE.LISTING
        If Not (m_list Is Nothing) Then
            m_list.IsParsed = False
        End If
        SendCommand "LIST " & file
        
    Case TRANSFER_TYPE.NLISTING
        If Not (m_list Is Nothing) Then
            m_list.IsParsed = False
        End If
        SendCommand "NLST " & file
        
    End Select
End Sub

Public Sub SendData(s As String)
    If IsDataSocketOpen Then
        m_data_sent = False
        If sckdata.State = sckConnected Then
            sckdata.SendData s
        End If
    End If
End Sub

Private Sub Class_Initialize()
    Set sckControl = New CSocket
    Set sckdata = New CSocket
    Set m_list = New ListParser
End Sub

Private Sub Class_Terminate()
    Disconnect
    CloseDataSocket
    WaitControlState sckClosed
    WaitDataState sckClosed
    Set sckControl = Nothing
    Set sckdata = Nothing
    Set m_list = Nothing
End Sub

Private Sub sckControl_OnClose()
    State = DISCONNECTED
    RaiseEvent OnDisconnected
End Sub

Private Sub sckControl_OnConnect()
    State = CONNECTED
    RaiseEvent OnConnected
End Sub

Private Sub sckControl_OnDataArrival(ByVal bytesTotal As Long)
    Dim s As String
    If sckControl.State = sckConnected Then
        sckControl.GetData s
        m_buffer = m_buffer & s
        ProcessBuffer
    End If
End Sub

Friend Sub ProcessBuffer()
    Dim iPos As Integer
    Dim ePos As Integer
    Dim sLine As String
    If Not m_processing Then
        m_processing = True
        iPos = 1
        Do
            ePos = InStr(iPos, m_buffer, vbCrLf)
            If ePos > 0 Then
                sLine = Mid$(m_buffer, iPos, ePos - iPos)
                iPos = ePos + 2
                ParseReply sLine
                'DoEvents
            Else
                Exit Do
            End If
        Loop
        m_buffer = Mid$(m_buffer, iPos)
        m_processing = False
    End If
End Sub

Private Sub ParseReply(s As String)
    m_last_reply = LTrim$(s)
    m_last_reply_code = CLng(Val(Left$(m_last_reply, 3)))
    m_last_reply = LTrim$(Mid$(m_last_reply, 4))
    ProcessReply
    RaiseEvent OnIncomingReply
End Sub

Private Sub ProcessReply()
    Select Case m_last_reply_code
        Case FTP_REPLY_CODE.SERVICE_READY_220
            If State = CONNECTED Then
                SendCommand "USER " & m_user
            End If
        
        Case FTP_REPLY_CODE.USER_LOGGED_IN_230
            m_logged = True
            RaiseEvent OnLoggedIn
            
        Case FTP_REPLY_CODE.CLOSING_CONTROL_CONNECTION_221
            Disconnect
            State = DISCONNECTING
        
        Case FTP_REPLY_CODE.INITIATING_DATA_CONNECTION_150
            IsTransferring = True
        
        Case FTP_REPLY_CODE.TRANSFER_STARTING_125
            IsTransferring = True
            
        Case FTP_REPLY_CODE.CLOSING_DATA_CONNECTION_226
            IsTransferring = False
            
        Case FTP_REPLY_CODE.USER_OK_NEED_PASSWORD_331
            SendCommand "PASS " & m_pass
        
        Case FTP_REPLY_CODE.LOGIN_NEED_ACCOUNT_332
        Case FTP_REPLY_CODE.ACCOUNT_REQUIRED_532
            SendCommand "ACCT " & m_account
        
        Case FTP_REPLY_CODE.ENTERING_PASSIVE_MODE_227
            EnterPassiveMode m_last_reply
    End Select

End Sub

Private Sub EnterPassiveMode(ByVal Reply As String)
    Dim IP As String, Port As Long
    Dim iPos As Integer, iPos2 As Integer
    Dim P1 As Long, P2 As Long
    iPos = InStrRev(Reply, "(")
    iPos2 = InStrRev(Reply, ")")
    If iPos > 0 And iPos2 > 0 Then
        Reply = Trim$(Mid$(Reply, iPos + 1, iPos2 - iPos))
        Reply = Replace(Reply, ",", ".", 1, 3)
        iPos = InStr(Reply, ",")
        iPos2 = InStr(iPos + 1, Reply, ",")
        IP = Mid$(Reply, 1, iPos - 1)
        P1 = Val(Mid$(Reply, iPos + 1, iPos2 - iPos - 1))
        P2 = Val(Mid$(Reply, iPos2 + 1))
        Port = (P1 * 256) + P2
        m_pasv_ip = IP
        m_pasv_port = Port
    End If
End Sub

Private Sub sckData_OnClose()
    IsDataSocketOpen = False
    IsTransferring = False
    If TransferType = LISTING Or TransferType = NLISTING Then
        Set m_list = New ListParser
        m_list.Parse m_data
        m_data = ""
    End If
    RaiseEvent OnTransferEnded
End Sub

Private Sub sckData_OnConnect()
    IsDataSocketOpen = True
End Sub

Private Sub sckData_OnConnectionRequest(ByVal requestID As Long)
    Disconnect
    If WaitDataState(sckClosed) Then
        sckdata.Accept requestID
        IsDataSocketOpen = True
    End If
End Sub

Private Sub sckData_OnDataArrival(ByVal bytesTotal As Long)
    Static entered As Boolean
    Dim s As String
    If sckdata.State = sckConnected Then
        sckdata.GetData s
        m_data = m_data & s
        RaiseEvent OnIncomingData
        DoEvents
    End If
End Sub

Private Sub sckData_OnSendComplete()
    m_data_sent = True
    If TransferType = UPLOAD Or TransferType = UPLOAD_RESUME Or _
    TransferType = APPEND Then
        RaiseEvent OnNeedData
    End If
End Sub
