VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ReplyCodeWaiter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'This class is created to avoid using DoEvents
'DoEvents has peculiar event management.

Public WithEvents Client As Client
Attribute Client.VB_VarHelpID = -1
Public LastReplyCode As Long
Public LastReply As String

Private Sub Class_Initialize()
    LastReplyCode = -1
    LastReply = ""
End Sub

Private Sub Class_Terminate()
    Set Client = Nothing
End Sub

Private Sub Client_OnIncomingReply()
    LastReplyCode = Client.GetLastReplyCode
    LastReply = Client.GetLastReply
End Sub
