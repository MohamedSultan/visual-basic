VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ListParser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private colFiles As Collection
Private numCols As Integer
Private bParsed As Boolean
Private lines() As String

Public Property Get IsParsed() As Boolean
    IsParsed = bParsed
End Property

Friend Property Let IsParsed(b As Boolean)
    bParsed = b
End Property

Public Function Parse(s As String) As Boolean
    Dim c As Variant
    If s <> "" Then
        lines = Split(s, vbCrLf)
        Set colFiles = Nothing
        Set colFiles = New Collection
        numCols = UBound(ParseLine(lines(0))) + 1
        For Each c In lines
            If CStr(c) <> "" Then
                colFiles.Add ParseLine(CStr(c))
            End If
        Next
    End If
    bParsed = True
End Function

Public Property Get List(index As Long) As String
    If index < FileCount Then
        List = lines(index)
    End If
End Property

Public Property Get fileList(index As Long, column As Integer) As String
    Dim c() As String
    If index < FileCount Then
        c = colFiles(index + 1)
        If column < ColumnCount Then
            fileList = c(column)
        End If
    End If
End Property

Public Property Get FileCount() As Long
    FileCount = 0
    If Not (colFiles Is Nothing) Then
        FileCount = colFiles.Count
    End If
End Property

Public Property Get ColumnCount() As Integer
    ColumnCount = numCols
End Property

Private Function ParseLine(s As String) As String()
    Dim iPos As Integer, inSpace As Boolean
    Dim iStart As Integer, col() As String
    ReDim col(0)
    inSpace = True
    iStart = 1
    For iPos = 1 To Len(s)
        Select Case Mid$(s, iPos, 1)
            Case vbTab
            Case " "
                If Not inSpace Then
                    If iStart > 0 Then
                        col(UBound(col)) = Mid$(s, iStart, iPos - iStart)
                        ReDim Preserve col(UBound(col) + 1)
                    End If
                    inSpace = True
                End If
            Case Else
                If inSpace Then
                    inSpace = False
                    iStart = iPos
                End If
        End Select
    Next
    If Not inSpace Then
        If iStart < Len(s) Then
            col(UBound(col)) = Mid$(s, iStart, Len(s) - iStart + 1)
        End If
    End If
    ParseLine = col
End Function


