dim ftpc
set ftpc = createobject("ftpClient.Client")
ftpc.RemoteHost = "ftp.yourftphost.com"
ftpc.RemotePort = 21
ftpc.User = "myusername"
ftpc.Password = "mypassword"
ftpc.SetPassiveMode True
ftpc.Connect

do until ftpc.IsLoggedIn
	ftpc.RunEvents
loop

if ftpc.InitiateDataSocket then

	do until ftpc.IsDataSocketOpen
		ftpc.RunEvents
	loop

	ftpc.StartTransfer 5, ""

	while ftpc.IsDataSocketOpen
		ftpc.RunEvents
	wend

	do until ftpc.fileList.IsParsed
		ftpc.RunEvents
	loop

	dim i,s
	for i = 0 to ftpc.fileList.FileCount - 1
		s = s & "," & ftpc.fileList.fileList(clng(i), clng(ftpc.fileList.ColumnCount - 1))
	next
	msgbox s
else
	msgbox "Error: Unable to initiate data transfer.", vbExclamation
end if
set ftpc = nothing