Attribute VB_Name = "Module1"
Public Declare Function GetTickCount Lib "kernel32" () As Long
Public Sub FTP(IP As String, Optional UserName As String, Optional ByVal Password As String, _
                    Optional ByVal StartUpPath As String, Optional ByVal LocalPath As String, _
                    Optional ByVal Command1ToBeSent As String, _
                    Optional ByVal Command2ToBeSent As String, _
                    Optional ByVal Command3ToBeSent As String, _
                    Optional ByVal Command4ToBeSent As String, _
                    Optional ByVal Command5ToBeSent As String)
                    
    Dim O_File_Name As Long
    O_File_Name = FreeFile
    Dim Drive As String
    If LocalPath = "" Then LocalPath = "D:\Work\Exchange"
    Drive = Left(LocalPath, InStr(1, LocalPath, "\") - 1)
    
    Open App.Path + "\Temp.bat" For Output As O_File_Name
        Print #O_File_Name, Drive
        Print #O_File_Name, "cd " + LocalPath
        Print #O_File_Name, "c:\windows\system32\ftp " + IP
    Close O_File_Name
    
    FTP_Shell = Shell(App.Path + "\Temp.bat", vbNormalFocus)
    Wait 400
    AppActivate (FTP_Shell)
    If UserName <> "" Then
        SendKeys (UserName)
        SendKeys "{ENTER}"
        Wait 100
    End If
    If Password <> "" Then
        SendKeys (Password)
        SendKeys "{ENTER}"
        Wait 100
    End If
    If StartUpPath <> "" Then
        SendKeys ("cd " + StartUpPath)
        SendKeys "{ENTER}"
        Wait 100
    End If
    
    Dim CommandsToBeSent(4) As String
    CommandsToBeSent(0) = Command1ToBeSent
    CommandsToBeSent(1) = Command2ToBeSent
    CommandsToBeSent(2) = Command3ToBeSent
    CommandsToBeSent(3) = Command4ToBeSent
    CommandsToBeSent(4) = Command5ToBeSent
    
    For i = 0 To UBound(CommandsToBeSent)
        If CommandsToBeSent(i) <> "" Then
            SendKeys (CommandsToBeSent(i))
            SendKeys "{ENTER}"
            Wait 100
        End If
    Next i
    
    Kill App.Path + "\Temp.bat"
End Sub

Public Sub Wait(ByVal dblMilliseconds As Double)
    Dim dblStart As Double
    Dim dblEnd As Double
    Dim dblTickCount As Double
    
    dblTickCount = GetTickCount()
    dblStart = GetTickCount()
    dblEnd = GetTickCount + dblMilliseconds
    
    Do
    DoEvents
    dblTickCount = GetTickCount()
    Loop Until dblTickCount > dblEnd Or dblTickCount < dblStart
       
    
End Sub

