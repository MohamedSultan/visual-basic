VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FTP Upload"
   ClientHeight    =   1080
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1080
   ScaleWidth      =   4575
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdGo 
      Caption         =   "Go!"
      Height          =   495
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   2175
   End
   Begin MSComctlLib.ProgressBar pbUpload 
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents cFTP As clsFTP
Attribute cFTP.VB_VarHelpID = -1


Private Sub cmdGo_Click()
Dim bSuccess As Boolean
Dim sError As String


'This loads/initialises the FTP object
Set cFTP = New clsFTP



If cFTP.OpenConnection("10.8.37.44", "mfuchk", "58UA?UUW") Then
    
    bSuccess = cFTP.FTPUploadFile("\\Livctrls03-08\Data1\APP\UNCLAIMW\UCCTest\Databases\DataFiles\Emp_Messages.txt", "W.APPHRMS.UCCHKMSG")
    
    If bSuccess Then
        sError = "Success"
    Else
        sError = cFTP.SimpleLastErrorMessage
    End If
    
    cFTP.CloseConnection
Else
    sError = cFTP.SimpleLastErrorMessage
End If

MsgBox sError


'unload/terminate the ftp object
Set cFTP = Nothing

End Sub

Private Sub cFTP_FileTransferProgress(lCurrentBytes As Long, lTotalBytes As Long)
Dim lPercent As Long

lPercent = 100 * lCurrentBytes / lTotalBytes

pbUpload.Value = lPercent

End Sub
