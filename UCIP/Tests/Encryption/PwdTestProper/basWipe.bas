Attribute VB_Name = "basWipe"
' $Id: basWipe $
' $Date: 2007-02-24 15:35:00 $
Option Explicit

' Uses WIPE_Bytes and WIPE_String from CryptoSys API.
' These are both VB6 aliases for WIPE_Data. See basCryptoSys.bas

Public Sub WipeBytes(abBytes() As Byte)
    Call WIPE_Bytes(abBytes(0), UBound(abBytes) - LBound(abBytes) + 1)
End Sub

Public Sub WipeString(ByVal str As String)
    Call WIPE_String(str, Len(str))
    str = ""
End Sub

