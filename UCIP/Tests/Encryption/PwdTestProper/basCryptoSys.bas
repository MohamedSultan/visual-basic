Attribute VB_Name = "basCryptoSys"
' $Id: basCryptoSys.bas $
' This module contains the full list of
' classic Visual Basic/VBA declaration statements
' for the CryptoSys(tm) API Version 3.2 library
' plus some useful wrapper functions.
' Last updated:
'   $Date: 2006-07-21 06:29:00 $
'   $Revision: 3.2.0 $

'********************* COPYRIGHT NOTICE*********************
' Copyright (c) 2001-6 DI Management Services Pty Limited.
' All rights reserved.
' This code may only be used by licensed users and in
' accordance with the licence conditions.
' The latest version of CryptoSys(tm) API and a licence
' may be obtained from <www.cryptosys.net>.
' This copyright notice must always be left intact.
'****************** END OF COPYRIGHT NOTICE*****************

Option Explicit
Option Base 0

' CONSTANTS
Public Const ENCRYPT As Boolean = True
Public Const DECRYPT As Boolean = False
' Maximum number of bytes in hash digest byte array
Public Const API_MAX_HASH_BYTES As Long = 32
Public Const API_MAX_SHA1_BYTES As Long = 20
Public Const API_MAX_SHA2_BYTES As Long = 32
Public Const API_MAX_MD5_BYTES  As Long = 16
' Maximum number of hex characters in hash digest
Public Const API_MAX_HASH_CHARS As Long = (2 * API_MAX_HASH_BYTES)
Public Const API_MAX_SHA1_CHARS As Long = (2 * API_MAX_SHA1_BYTES)
Public Const API_MAX_SHA2_CHARS As Long = (2 * API_MAX_SHA2_BYTES)
Public Const API_MAX_MD5_CHARS  As Long = (2 * API_MAX_MD5_BYTES)
' Encryption block sizes in bytes
Public Const API_BLK_DES_BYTES  As Long = 8
Public Const API_BLK_TDEA_BYTES As Long = 8
Public Const API_BLK_BLF_BYTES  As Long = 8
Public Const API_BLK_AES_BYTES  As Long = 16

' GENERAL FUNCTIONS
Public Declare Function API_ErrorLookup Lib "diCryptoSys.dll" (ByVal strErrMsg As String, ByVal nMaxChars As Long, ByVal nErrCode As Long) As Long
Public Declare Function API_PowerUpTests Lib "diCryptoSys.dll" (ByVal nReserved As Long) As Long
Public Declare Function API_Version Lib "diCryptoSys.dll" () As Long
Public Declare Function API_LicenceType Lib "diCryptoSys.dll" (ByVal nReserved As Long) As Long
Public Declare Function API_CompileTime Lib "diCryptoSys.dll" (ByVal strCompiledOn As String, ByVal nMaxChars As Long) As Long
Public Declare Function API_ModuleName Lib "diCryptoSys.dll" (ByVal strModuleName As String, ByVal nMaxChars As Long, ByVal nReserved As Long) As Long

' ADVANCED ENCRYPTION STANDARD (AES) BLOCK CIPHER WITH 128-BIT KEY
Public Declare Function AES128_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES128_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES128_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES128_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES128_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strB64IV As String) As Long
Public Declare Function AES128_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES128_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES128_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES128_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES128_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function AES128_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function AES128_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function AES128_InitError Lib "diCryptoSys.dll" () As Long

' ADVANCED ENCRYPTION STANDARD (AES) BLOCK CIPHER WITH 192-BIT KEY
Public Declare Function AES192_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES192_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES192_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES192_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES192_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strB64IV As String) As Long
Public Declare Function AES192_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES192_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES192_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES192_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES192_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function AES192_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function AES192_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function AES192_InitError Lib "diCryptoSys.dll" () As Long

' ADVANCED ENCRYPTION STANDARD (AES) BLOCK CIPHER WITH 256-BIT KEY
Public Declare Function AES256_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES256_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES256_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES256_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES256_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strB64IV As String) As Long
Public Declare Function AES256_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES256_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES256_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES256_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES256_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function AES256_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function AES256_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function AES256_InitError Lib "diCryptoSys.dll" () As Long

' BLOWFISH BLOCK CIPHER FUNCTIONS
Public Declare Function BLF_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long, ByVal bEncrypt As Boolean) As Long
Public Declare Function BLF_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function BLF_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function BLF_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function BLF_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strB64IV As String) As Long
Public Declare Function BLF_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyBytes As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function BLF_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function BLF_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function BLF_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function BLF_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function BLF_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function BLF_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function BLF_InitError Lib "diCryptoSys.dll" () As Long
    
' DATA ENCRYPTION STANDARD (DES) BLOCK CIPHER
Public Declare Function DES_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean) As Long
Public Declare Function DES_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function DES_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function DES_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function DES_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strB64IV As String) As Long
Public Declare Function DES_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function DES_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function DES_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function DES_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function DES_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function DES_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function DES_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function DES_InitError Lib "diCryptoSys.dll" () As Long
    
' Checks for weak or invalid-length DES or TDEA keys -- added in Version 3.0
Public Declare Function DES_CheckKey Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function DES_CheckKeyHex Lib "diCryptoSys.dll" (ByVal strHexKey As String) As Long

' TRIPLE DATA ENCRYPTION ALGORITHM (TDEA) BLOCK CIPHER
Public Declare Function TDEA_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean) As Long
Public Declare Function TDEA_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function TDEA_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function TDEA_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function TDEA_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strB64IV As String) As Long
Public Declare Function TDEA_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function TDEA_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function TDEA_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function TDEA_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function TDEA_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function TDEA_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function TDEA_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function TDEA_InitError Lib "diCryptoSys.dll" () As Long
    
' SECURE HASH ALGORITHM 1 (SHA-1) HASH FUNCTION
Public Declare Function SHA1_StringHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strData As String) As Long
Public Declare Function SHA1_FileHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strFileName As String, ByVal strMode As String) As Long
Public Declare Function SHA1_BytesHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function SHA1_BytesHash Lib "diCryptoSys.dll" (ByRef abDigest As Byte, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function SHA1_Init Lib "diCryptoSys.dll" () As Long
Public Declare Function SHA1_AddString Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strData As String) As Long
Public Declare Function SHA1_AddBytes Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function SHA1_HexDigest Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal hContext As Long) As Long
Public Declare Function SHA1_Reset Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function SHA1_Hmac Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function SHA1_HmacHex Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strHexData As String, ByVal strHexKey As String) As Long
    
' SECURE HASH ALGORITHM (SHA-256) HASH FUNCTION
Public Declare Function SHA2_StringHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strData As String) As Long
Public Declare Function SHA2_FileHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strFileName As String, ByVal strMode As String) As Long
Public Declare Function SHA2_BytesHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function SHA2_BytesHash Lib "diCryptoSys.dll" (ByRef abDigest As Byte, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function SHA2_Init Lib "diCryptoSys.dll" () As Long
Public Declare Function SHA2_AddString Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strData As String) As Long
Public Declare Function SHA2_AddBytes Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function SHA2_HexDigest Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal hContext As Long) As Long
Public Declare Function SHA2_Reset Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function SHA2_Hmac Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function SHA2_HmacHex Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strHexData As String, ByVal strHexKey As String) As Long
    
' RSA DATA SECURITY, INC. MD5 MESSAGE-DIGEST ALGORITHM
Public Declare Function MD5_StringHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strData As String) As Long
Public Declare Function MD5_FileHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strFileName As String, ByVal strMode As String) As Long
Public Declare Function MD5_BytesHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function MD5_BytesHash Lib "diCryptoSys.dll" (ByRef abDigest As Byte, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function MD5_Init Lib "diCryptoSys.dll" () As Long
Public Declare Function MD5_AddString Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strData As String) As Long
Public Declare Function MD5_AddBytes Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function MD5_HexDigest Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal hContext As Long) As Long
Public Declare Function MD5_Reset Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function MD5_Hmac Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function MD5_HmacHex Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strHexData As String, ByVal strHexKey As String) As Long
    
' RC4-COMPATIBLE PC1 PROTOTYPES
Public Declare Function PC1_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nDataLen As Long, ByRef abKey As Byte, ByVal nKeyLen As Long) As Long
Public Declare Function PC1_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByVal strInputHex As String, ByVal strKeyHex As String) As Long
Public Declare Function PC1_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyLen As Long) As Long

' X9.31/FIPS140 (RNG) RANDOM NUMBER GENERATOR
Public Declare Function RNG_KeyBytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nBytes As Long, ByVal strSeed As String, ByVal nSeedLen As Long) As Long
Public Declare Function RNG_KeyHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByVal nBytes As Long, ByVal strSeed As String, ByVal nSeedLen As Long) As Long
Public Declare Function RNG_NonceData Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nBytes As Long) As Long
Public Declare Function RNG_NonceDataHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByVal nBytes As Long) As Long
Public Declare Function RNG_Long Lib "diCryptoSys.dll" (ByVal nLower As Long, ByVal nUpper As Long, ByVal strSeed As String) As Long
Public Declare Function RNG_Test Lib "diCryptoSys.dll" (ByVal strFileName As String) As Long
    
' ZLIB COMPRESSION FUNCTIONS
Public Declare Function ZLIB_Deflate Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Long, ByRef abInput As Byte, ByVal nInputLen As Long) As Long
Public Declare Function ZLIB_Inflate Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Long, ByRef abInput As Byte, ByVal nInputLen As Long) As Long

' PASSWORD-BASED KEY DERIVATION FUNCTIONS
Public Declare Function PBE_Kdf2 Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Long, ByRef abPwd As Byte, ByVal nPwdBytes As Long, ByRef abSalt As Byte, ByVal nSaltBytes As Long, ByVal nCount As Long, ByVal nReserved As Long) As Long
Public Declare Function PBE_Kdf2Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByVal nKeyBytes As Long, ByVal strPwd As String, ByVal strSaltHex As String, ByVal nCount As Long, ByVal nReserved As Long) As Long

' HEX ENCODING CONVERSION FUNCTIONS
Public Declare Function CNV_HexStrFromBytes Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
' See cnvHexStrFromBytes below
Public Declare Function CNV_BytesFromHexStr Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Long, ByVal strInput As String) As Long
' See cnvBytesFromHexStr below
Public Declare Function CNV_HexFilter Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal nStrLen As Long) As Long
' See cnvHexFilter below

' BASE64 ENCODING CONVERSION FUNCTIONS
Public Declare Function CNV_B64StrFromBytes Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
' See cnvB64StrFromBytes below
Public Declare Function CNV_BytesFromB64Str Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Long, ByVal strInput As String) As Long
' See cnvBytesFromHexB64r below
Public Declare Function CNV_B64Filter Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal nStrLen As Long) As Long
' See cnvB64Filter below

' CRC FUNCTIONS
Public Declare Function CRC_Bytes Lib "diCryptoSys.dll" (ByRef abInput As Byte, ByVal nBytes As Long, ByVal nOptions As Long) As Long
Public Declare Function CRC_String Lib "diCryptoSys.dll" (ByVal strInput As String, ByVal nOptions As Long) As Long
Public Declare Function CRC_File Lib "diCryptoSys.dll" (ByVal strFileName As String, ByVal nOptions As Long) As Long

' FUNCTIONS TO WIPE DATA
Public Declare Function WIPE_File Lib "diCryptoSys.dll" (ByVal strFileName As String, ByVal nOptions As Long) As Long
Public Declare Function WIPE_Data Lib "diCryptoSys.dll" (ByRef abData As Byte, ByVal nBytes As Long) As Long
' Alternative Aliases to cope with Byte and String types explicitly...
Public Declare Function WIPE_Bytes Lib "diCryptoSys.dll" Alias "WIPE_Data" (ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function WIPE_String Lib "diCryptoSys.dll" Alias "WIPE_Data" (ByVal strData As String, ByVal nStrLen As Long) As Long

' PADDING FUNCTIONS
Public Declare Function PAD_BytesBlock Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Long, ByRef abInput As Byte, ByVal nInputLen As Long, ByVal nBlockLen As Long, ByVal nOptions As Long) As Long
Public Declare Function PAD_UnpadBytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Long, ByRef abInput As Byte, ByVal nInputLen As Long, ByVal nBlockLen As Long, ByVal nOptions As Long) As Long
Public Declare Function PAD_HexBlock Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByVal strInputHex As String, ByVal nBlockLen As Long, ByVal nOptions As Long) As Long
Public Declare Function PAD_UnpadHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Long, ByVal strInputHex As String, ByVal nBlockLen As Long, ByVal nOptions As Long) As Long


' *******************************************************************
' DEPRECATED FUNCTIONS RETAINED FOR BACKWARDS COMPATABILITY
' NOT RECOMMENDED FOR USE IN NEW APPLICATIONS
' NB These are all thread-safe as of version 3

' VERSION 1 ("Gutmann") RANDOM NUMBER GENERATOR
Public Declare Function RAN_KeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Long, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_KeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal lngKeyBytes As Long, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_DESKeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_DESKeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_TDEAKeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_TDEAKeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_Test Lib "diCryptoSys.dll" (ByVal strFileName As String) As Long
Public Declare Function RAN_Seed Lib "diCryptoSys.dll" (ByRef abSeed As Byte, ByVal lngSeedLen As Long, ByVal bPromptUser As Boolean) As Long
Public Declare Function RAN_Nonce Lib "diCryptoSys.dll" (ByRef abNonce As Byte, ByVal lngNonceLen As Long) As Long
Public Declare Function RAN_NonceHex Lib "diCryptoSys.dll" (ByVal sHexData As String, ByVal lngNonceLen As Long) As Long
Public Declare Function RAN_Long Lib "diCryptoSys.dll" (ByVal lngLower As Long, ByVal lngUpper As Long) As Long
' KeyGen functions (and constants) superseded in version 3.0 by RNG_KeyBytes and RNG_KeyHex
Public Const RNG_DEFAULT As Long = &H0      ' Default flag
Public Const RNG_NOCHECK As Long = &H1      ' NO LONGER USED - IGNORED
Public Const RNG_DESKEY As Long = &H2       ' Set parity bits and check for weak DES keys
' lngCheck is now ignored and can be set to zero
Public Declare Function RNG_KeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Long, ByVal strSeed As String, ByRef lngCheck As Long, ByVal lngFlags As Long) As Long
Public Declare Function RNG_KeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal lngKeyBytes As Long, ByVal strSeed As String, ByRef lngCheck As Long, ByVal lngFlags As Long) As Long
' Nonce function replaced with better parameters in RNG_NonceData
Public Declare Function RNG_Nonce Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nBytes As Long, ByVal strSeed As String) As Long
Public Declare Function RNG_NonceHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nBytes As Long, ByVal strSeed As String) As Long
' OLD-STYLE RIJNDAEL BLOCK CIPHER FUNCTIONS
' SUPERSEDED BY FASTER AESnnn FUNCTIONS
Public Declare Function AES_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByRef abInitV As Byte) As Long
Public Declare Function AES_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal lngKeyBits As Long, ByVal lngBlockBits As Long, ByVal bEncrypt As Boolean, ByVal strMode As String, ByVal strHexIV As String) As Long
Public Declare Function AES_Update Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long) As Long
Public Declare Function AES_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexData As String) As Long
Public Declare Function AES_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function AES_Ecb Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES_EcbHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexBlock As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function AES_InitError Lib "diCryptoSys.dll" () As Long
' ORIGINAL di_Blowfish.DLL FUNCTIONS
Public Declare Function bf_StringEnc Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function bf_StringDec Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Long, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function bf_FileEnc Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function bf_FileDec Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function bf_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Long) As Long
Public Declare Function bf_BlockEnc Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abBlock As Byte) As Long
Public Declare Function bf_BlockDec Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abBlock As Byte) As Long
Public Declare Function bf_Final Lib "diCryptoSys.dll" (ByVal hContext As Long) As Long
Public Declare Function BLF_EcbHex Lib "diCryptoSys.dll" (ByVal hContext As Long, ByVal strHexBlock As String, ByVal bEncrypt As Boolean) As Long
Public Declare Function BLF_Ecb Lib "diCryptoSys.dll" (ByVal hContext As Long, ByRef abData As Byte, ByVal nBytes As Long, ByVal bEncrypt As Boolean) As Long
' ... END OF DEPRECATED FUNCTIONS
' *******************************************************************
    
' SOME USEFUL WRAPPER FUNCTIONS

' [2006-07-04] Conversion functions updated to handle errors better.

Public Function cnvHexStrFromBytes(abData() As Byte) As String
' Returns hex string encoding of bytes in abData or empty string if error
    Dim strHex As String
    Dim nHexLen As Long
    Dim nDataLen As Long
    
    On Error GoTo CatchEmptyData
    nDataLen = UBound(abData) - LBound(abData) + 1
    nHexLen = CNV_HexStrFromBytes(vbNullString, 0, abData(0), nDataLen)
    If nHexLen <= 0 Then
        Exit Function
    End If
    strHex = String$(nHexLen, " ")
    nHexLen = CNV_HexStrFromBytes(strHex, nHexLen, abData(0), nDataLen)
    If nHexLen <= 0 Then
        Exit Function
    End If
    cnvHexStrFromBytes = Left$(strHex, nHexLen)
    
CatchEmptyData:

End Function

Public Function cnvHexStrFromString(strData As String) As String
' Returns hex string encoding of ASCII string or empty string if error
    Dim strHex As String
    Dim nHexLen As Long
    Dim nDataLen As Long
    Dim abData() As Byte
    
    If Len(strData) = 0 Then Exit Function
    abData = StrConv(strData, vbFromUnicode)
    nDataLen = UBound(abData) - LBound(abData) + 1
    nHexLen = CNV_HexStrFromBytes(vbNullString, 0, abData(0), nDataLen)
    If nHexLen <= 0 Then
        Exit Function
    End If
    strHex = String$(nHexLen, " ")
    nHexLen = CNV_HexStrFromBytes(strHex, nHexLen, abData(0), nDataLen)
    If nHexLen <= 0 Then
        Exit Function
    End If
    cnvHexStrFromString = Left$(strHex, nHexLen)
End Function

Public Function cnvBytesFromHexStr(strHex As String) As Variant
' Returns a Variant to an array of bytes decoded from a hex string
    Dim abData() As Byte
    Dim nDataLen As Long
    
    ' Set default return value that won't cause a run-time error
    cnvBytesFromHexStr = StrConv("", vbFromUnicode)
    nDataLen = CNV_BytesFromHexStr(0, 0, strHex)
    If nDataLen <= 0 Then
        Exit Function
    End If
    ReDim abData(nDataLen - 1)
    nDataLen = CNV_BytesFromHexStr(abData(0), nDataLen, strHex)
    If nDataLen <= 0 Then
        Exit Function
    End If
    ReDim Preserve abData(nDataLen - 1)
    cnvBytesFromHexStr = abData
End Function

Public Function cnvStringFromHexStr(ByVal strHex As String) As String
' Converts string <strHex> in hex format to string of ANSI chars
' with value between 0 and 255.
' E.g. "6162632E" will be converted to "abc."
    Dim abData() As Byte
    If Len(strHex) = 0 Then Exit Function
    abData = cnvBytesFromHexStr(strHex)
    cnvStringFromHexStr = StrConv(abData, vbUnicode)
End Function

Public Function cnvHexFilter(strHex As String) As String
' Returns a string stripped of any invalid hex characters
    Dim strFiltered As String
    Dim nLen As Long
    
    strFiltered = String(Len(strHex), " ")
    nLen = CNV_HexFilter(strFiltered, strHex, Len(strHex))
    If nLen > 0 Then
        strFiltered = Left$(strFiltered, nLen)
    Else
        strFiltered = ""
    End If
    cnvHexFilter = strFiltered
End Function

Public Function cnvB64StrFromBytes(abData() As Byte) As String
' Returns base64 string encoding of bytes in abData or empty string if error
    Dim strB64 As String
    Dim nB64Len As Long
    Dim nDataLen As Long
    
    On Error GoTo CatchEmptyData
    nDataLen = UBound(abData) - LBound(abData) + 1
    nB64Len = CNV_B64StrFromBytes(vbNullString, 0, abData(0), nDataLen)
    If nB64Len <= 0 Then
        Exit Function
    End If
    strB64 = String$(nB64Len, " ")
    nB64Len = CNV_B64StrFromBytes(strB64, nB64Len, abData(0), nDataLen)
    If nB64Len <= 0 Then
        Exit Function
    End If
    cnvB64StrFromBytes = Left$(strB64, nB64Len)
    
CatchEmptyData:

End Function

Public Function cnvB64StrFromString(strData As String) As String
' Returns base64 string encoding of ASCII string or empty string if error
    Dim strB64 As String
    Dim nB64Len As Long
    Dim nDataLen As Long
    Dim abData() As Byte
    
    If Len(strData) = 0 Then Exit Function
    abData = StrConv(strData, vbFromUnicode)
    nDataLen = UBound(abData) - LBound(abData) + 1
    nB64Len = CNV_B64StrFromBytes(vbNullString, 0, abData(0), nDataLen)
    If nB64Len <= 0 Then
        Exit Function
    End If
    strB64 = String$(nB64Len, " ")
    nB64Len = CNV_B64StrFromBytes(strB64, nB64Len, abData(0), nDataLen)
    If nB64Len <= 0 Then
        Exit Function
    End If
    cnvB64StrFromString = Left$(strB64, nB64Len)
End Function

Public Function cnvBytesFromB64Str(strB64 As String) As Variant
' Returns a Variant to an array of bytes decoded from a base64 string
    Dim abData() As Byte
    Dim nDataLen As Long
    
    ' Set default return value that won't cause a run-time error
    cnvBytesFromB64Str = StrConv("", vbFromUnicode)
    nDataLen = CNV_BytesFromB64Str(0, 0, strB64)
    If nDataLen <= 0 Then
        Exit Function
    End If
    ReDim abData(nDataLen - 1)
    nDataLen = CNV_BytesFromB64Str(abData(0), nDataLen, strB64)
    If nDataLen <= 0 Then
        Exit Function
    End If
    ReDim Preserve abData(nDataLen - 1)
    cnvBytesFromB64Str = abData
End Function

Public Function cnvB64Filter(strB64 As String) As String
' Returns a string stripped of any invalid base64 characters
    Dim strFiltered As String
    Dim nLen As Long
    
    strFiltered = String(Len(strB64), " ")
    nLen = CNV_B64Filter(strFiltered, strB64, Len(strB64))
    If nLen > 0 Then
        strFiltered = Left$(strFiltered, nLen)
    Else
        strFiltered = ""
    End If
    cnvB64Filter = strFiltered
End Function

Public Function rngNonceHex(nBytes As Long) As String
' Returns a random nonce nBytes long encoded in hex
    Dim strHex As String
    Dim lngRet As Long
    
    strHex = String(nBytes * 2, " ")
    lngRet = RNG_NonceDataHex(strHex, Len(strHex), nBytes)
    If lngRet = 0 Then
        rngNonceHex = strHex
    End If
End Function

Public Function apiErrorLookup(nCode As Long) As String
' Returns a string with the error message for code nCode
    Dim strMsg As String
    Dim nRet As Long
    
    strMsg = String(128, " ")
    nRet = API_ErrorLookup(strMsg, Len(strMsg), nCode)
    apiErrorLookup = Left(strMsg, nRet)
End Function

Public Function padHexString(ByVal strInputHex As String, nBlockLen As Long) As String
' Adds padding to a hex string up to next multiple of block length.
' Returns a padded hex string or, on error, an empty string.

    Dim nOutChars As Long
    Dim strOutputHex As String
    
    ' In VB6 an uninitialised empty string is passed to a DLL as a NULL,
    ' so we append a non-null empty string!
    strInputHex = strInputHex & ""
    
    nOutChars = PAD_HexBlock("", 0, strInputHex, nBlockLen, 0)
    'Debug.Print "Required length is " & nOutChars & " characters"
    ' Check for error
    If (nOutChars <= 0) Then Exit Function
    
    ' Pre-dimension output
    strOutputHex = String(nOutChars, " ")
    
    nOutChars = PAD_HexBlock(strOutputHex, Len(strOutputHex), strInputHex, nBlockLen, 0)
    If (nOutChars <= 0) Then Exit Function
    'Debug.Print "Padded data='" & strOutputHex & "'"
    
    padHexString = strOutputHex
    
End Function

Public Function unpadHexString(strInputHex As String, nBlockLen As Long) As String
' Strips padding from a hex string.
' Returns unpadded hex string or, on error, the original input string
' -- we do this because an empty string is a valid result.
' To check for error: a valid output string is *always* shorter than the input.

    Dim nOutChars As Long
    Dim strOutputHex As String
    
    ' No need to query for length because we know the output will be shorter than input
    ' so make sure output is as long as the input
    strOutputHex = String(Len(strInputHex), " ")
    nOutChars = PAD_UnpadHex(strOutputHex, Len(strOutputHex), strInputHex, nBlockLen, 0)
    'Debug.Print "Unpadded length is " & nOutChars & " characters"
    
    ' Check for error
    If (nOutChars < 0) Then
        ' Return unchanged input to indicate error
        unpadHexString = strInputHex
        Exit Function
    End If
    
    ' Re-dimension the output to the correct length
    strOutputHex = Left$(strOutputHex, nOutChars)
    'Debug.Print "Unpadded data='" & strOutputHex & "'"
    
    unpadHexString = strOutputHex
    
End Function

' ... END OF MODULE
' *******************************************************************


