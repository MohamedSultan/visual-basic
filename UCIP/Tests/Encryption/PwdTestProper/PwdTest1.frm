VERSION 5.00
Begin VB.Form frmPwdTest 
   Caption         =   "CryptoSys Password Test using AES-128"
   ClientHeight    =   7155
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8130
   Icon            =   "PwdTest1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7155
   ScaleWidth      =   8130
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtKeyDecrypt 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   4800
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   5100
      Width           =   3255
   End
   Begin VB.TextBox txtIV 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   4740
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   3300
      Width           =   3255
   End
   Begin VB.TextBox txtKey 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   1020
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   3300
      Width           =   3255
   End
   Begin VB.Frame Frame1 
      Caption         =   "Key Derivation Method:"
      Height          =   1215
      Left            =   3120
      TabIndex        =   11
      Top             =   1920
      Width           =   3555
      Begin VB.OptionButton optKDF2 
         Caption         =   "PBKDF2"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   900
         Width           =   3195
      End
      Begin VB.OptionButton optKDF1 
         Caption         =   "PBKDF1"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   3195
      End
      Begin VB.OptionButton optKDFSimple 
         Caption         =   "Simple method (not recommended)"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   300
         Value           =   -1  'True
         Width           =   2835
      End
   End
   Begin VB.CommandButton cmdDecrypt 
      Caption         =   "&Decrypt"
      Height          =   375
      Left            =   1020
      TabIndex        =   8
      Top             =   5040
      Width           =   1815
   End
   Begin VB.TextBox txtDecrypted 
      Height          =   1095
      Left            =   1020
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   5520
      Width           =   6495
   End
   Begin VB.CommandButton cmdEncrypt 
      Caption         =   "&Encrypt"
      Height          =   375
      Left            =   1020
      TabIndex        =   6
      Top             =   2220
      Width           =   1695
   End
   Begin VB.TextBox txtCiphertext 
      Height          =   1155
      Left            =   1020
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   3780
      Width           =   6495
   End
   Begin VB.TextBox txtPassword 
      Height          =   285
      Left            =   1020
      TabIndex        =   2
      Top             =   1560
      Width           =   6495
   End
   Begin VB.TextBox txtPlaintext 
      Height          =   1335
      Left            =   1020
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   6495
   End
   Begin VB.Label Label8 
      Caption         =   "Key used:"
      Height          =   195
      Left            =   3720
      TabIndex        =   20
      Top             =   5160
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "IV:"
      Height          =   195
      Left            =   4380
      TabIndex        =   18
      Top             =   3360
      Width           =   255
   End
   Begin VB.Label Label6 
      Caption         =   "Key used:"
      Height          =   195
      Left            =   60
      TabIndex        =   16
      Top             =   3360
      Width           =   855
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "Copyright (C) 2003-7 DI Management Services <www.di-mgt.com.au>"
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   540
      TabIndex        =   10
      Top             =   6780
      Width           =   7095
   End
   Begin VB.Label Label4 
      Caption         =   "Decrypted Message:"
      Height          =   795
      Left            =   60
      TabIndex        =   9
      Top             =   5580
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Encrypted Message:  (IV+CT)"
      Height          =   795
      Left            =   60
      TabIndex        =   5
      Top             =   3840
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Password:"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   1620
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Message Plaintext:"
      Height          =   795
      Left            =   60
      TabIndex        =   1
      Top             =   120
      Width           =   795
   End
End
Attribute VB_Name = "frmPwdTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' $Id: frmPwdTest $
'****************************************************************************
' Copyright �2003-7 DI Management Services Pty Limited, All Rights Reserved.
'****************************************************************************
' Distribution: You can freely use this code in your own applications, but
' you may not reproduce or publish this code on any web site, online service,
' or distribute as source on any media without express permission.
' Terms: Use at your own risk. Provided "as is" with no warranties.
' Contact: <www.di-mgt.com.au> <www.cryptosys.net>
'****************************************************************************
' This file last updated:
'   $Date: 2007-02-24 15:19:00 $
'****************************************************************************

Option Explicit
Const KEYBYTES As Long = 16
Const BLOCKBYTES As Long = 16


Private Sub cmdEncrypt_Click()
    Dim strPlain As String
    Dim abPlain() As Byte
    Dim strPassword As String
    Dim abPassword() As Byte
    Dim abCipher() As Byte
    Dim abKey() As Byte
    Dim nLen As Long
    Dim nPad As Long
    Dim i As Long
    Dim lRet As Long
    Dim abSalt() As Byte
    Dim abInitV() As Byte
    Dim abOutput() As Byte
    
    ' Get input strings as entered by user
    strPlain = Me.txtPlaintext.Text
    strPassword = Me.txtPassword.Text
    
    ' Convert to Bytes
    abPassword = StrConv(strPassword, vbFromUnicode)
    abPlain = StrConv(strPlain, vbFromUnicode)
    
    ' Pad input to next multiple of encryption block size
    ' NB Byte arrays are zero-based
    nLen = UBound(abPlain) - LBound(abPlain) + 1
    nPad = ((nLen \ BLOCKBYTES) + 1) * BLOCKBYTES - nLen
    ReDim Preserve abPlain(nLen + nPad - 1)
    For i = nLen To nLen + nPad - 1
        abPlain(i) = CByte(nPad)
    Next
    
    ' Generate an IV
    ReDim abInitV(BLOCKBYTES - 1)
    lRet = RNG_NonceData(abInitV(0), BLOCKBYTES)
    Me.txtIV = cnvHexStrFromBytes(abInitV)
    
    ' Derive the key
    If Me.optKDF1 Then
        abKey = CreateKeyPBKDF1(abPassword, KEYBYTES, abInitV, 1000)
    ElseIf Me.optKDF2 Then
        abKey = CreateKeyPBKDF2(abPassword, KEYBYTES, abInitV, BLOCKBYTES, 2048)
    Else
        abKey = CreateKeySimple(abPassword, KEYBYTES)
    End If
    
    ' Display key in hex format
    Me.txtKey.Text = cnvHexStrFromBytes(abKey)
    
    ' Redimension the ciphertext buffer
    ReDim abCipher(UBound(abPlain))
    ' Do the encryption
    Debug.Print "KY=" & cnvHexStrFromBytes(abKey)
    Debug.Print "IV=" & cnvHexStrFromBytes(abInitV)
    Debug.Print "PT=" & cnvHexStrFromBytes(abPlain)
    lRet = AES128_BytesMode(abCipher(0), abPlain(0), (nLen + nPad), abKey(0), ENCRYPT, "CBC", abInitV(0))
    Debug.Print "CT=" & cnvHexStrFromBytes(abCipher)
    
    ' Create an output buffer = IV + ciphertext
    ReDim abOutput(UBound(abCipher) + BLOCKBYTES)
    For i = 0 To BLOCKBYTES - 1
        abOutput(i) = abInitV(i)
    Next
    For i = 0 To nLen + nPad - 1
        abOutput(BLOCKBYTES + i) = abCipher(i)
    Next
        
    ' Encode into base64 format so we can display as ordinary text
    Me.txtCiphertext.Text = cnvB64StrFromBytes(abOutput)
    
    ' Clean up (notwithstanding that the details are displayed on the form)
    WipeBytes abKey
    WipeBytes abPassword
    WipeBytes abPlain
    WipeString strPlain
    WipeString strPassword
    

End Sub

Private Sub cmdDecrypt_Click()
    Dim strPlain As String
    Dim abPlain() As Byte
    Dim strCipher64 As String
    Dim strPassword As String
    Dim abPassword() As Byte
    Dim abCipher() As Byte
    Dim abKey() As Byte
    Dim nLen As Long
    Dim nPad As Long
    Dim i As Long
    Dim lRet As Long
    Dim abInput() As Byte
    Dim abInitV() As Byte
    
    ' Get input strings as entered by user
    strCipher64 = Me.txtCiphertext.Text
    strPassword = Me.txtPassword.Text
    
    ' Convert to Bytes
    abPassword = StrConv(strPassword, vbFromUnicode)
    abInput = cnvBytesFromB64Str(strCipher64)
    nLen = UBound(abInput) - LBound(abInput) + 1
    nLen = nLen - BLOCKBYTES
    
    ' Strip off the first 16 bytes for the IV, the remainder is ciphertext
    ReDim abInitV(BLOCKBYTES - 1)
    For i = 0 To BLOCKBYTES - 1
        abInitV(i) = abInput(i)
    Next
    ReDim abCipher(nLen - 1)
    For i = 0 To nLen - 1
        abCipher(i) = abInput(BLOCKBYTES + i)
    Next
    
    ' Derive the key
    If Me.optKDF1 Then
        abKey = CreateKeyPBKDF1(abPassword, KEYBYTES, abInitV, 1000)
    ElseIf Me.optKDF2 Then
        abKey = CreateKeyPBKDF2(abPassword, KEYBYTES, abInitV, BLOCKBYTES, 2048)
    Else
        abKey = CreateKeySimple(abPassword, KEYBYTES)
    End If
    
    ' Display key in hex format
    Me.txtKeyDecrypt.Text = cnvHexStrFromBytes(abKey)
    
    ' Redimension the output buffer
    ReDim abPlain(UBound(abCipher))
    ' Do the decryption
    Debug.Print "KY=" & cnvHexStrFromBytes(abKey)
    Debug.Print "IV=" & cnvHexStrFromBytes(abInitV)
    Debug.Print "CT=" & cnvHexStrFromBytes(abCipher)
    lRet = AES128_BytesMode(abPlain(0), abCipher(0), nLen, abKey(0), DECRYPT, "CBC", abInitV(0))
    Debug.Print "P'=" & cnvHexStrFromBytes(abPlain)
    
    ' Strip the padding bytes, if valid
    nPad = CByte(abPlain(nLen - 1))
    If nPad <= BLOCKBYTES Then
        ReDim Preserve abPlain(nLen - nPad - 1)
    Else
        MsgBox "Invalid padding byte", vbExclamation, "Decrypt"
    End If
        
    
    Me.txtDecrypted = StrConv(abPlain, vbUnicode)

End Sub

