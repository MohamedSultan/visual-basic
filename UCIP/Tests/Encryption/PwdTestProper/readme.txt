PwdTestProper.vbp Visual Basic Project
---------------------------------------
This project contains original source code written by David Ireland of DI
Management Services Pty Ltd <www.di-mgt.com.au>. Copyright (C) 2003-7 DI
Management Services Pty Ltd, all rights reserved.

The code uses the CryptoSys (tm) API software available from
<www.cryptosys.net>.

Please read the accompanying web page on this software that explains its
background and details of the algorithms used at
<www.di-mgt.com.au/properpassword.html>. The latest copy of the source code can
be downloaded from this page.

We would welcome any constructive comments on the software and web page. Any
contributions will be acknowledged.

Thank you,
David Ireland
DI Management Services Pty Ltd
www.di-mgt.com.au
www.cryptosys.net

This document last updated: 24 February 2007
