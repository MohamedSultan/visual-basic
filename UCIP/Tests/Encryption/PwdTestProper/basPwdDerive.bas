Attribute VB_Name = "basPwdDerive"
' $Id: basPwdDerive $
'****************************************************************************
' Copyright �2003-7 DI Management Services Pty Limited, All Rights Reserved.
'****************************************************************************
' Distribution: You can freely use this code in your own applications, but
' you may not reproduce or publish this code on any web site, online service,
' or distribute as source on any media without express permission.
' Terms: Use at your own risk. Provided "as is" with no warranties.
' Contact: <www.di-mgt.com.au> <www.cryptosys.net>
'****************************************************************************
' This file last updated:
'   $Date: 2007-02-24 15:19:00 $
'****************************************************************************
Option Explicit

Public Function CreateKeySimple(abPassword() As Byte, ByVal nKeyLen As Long) As Variant
    ' Just use the straight password chars as the key up to the length of the key
    ' leaving remainder as zeroes. NOT RECOMMENDED FOR USE IN PRACTICE.
    Dim abKey() As Byte
    Dim nLen As Long, i As Long
    
    ReDim abKey(nKeyLen - 1)
    nLen = IIf(UBound(abPassword) < nKeyLen - 1, UBound(abPassword) + 1, nKeyLen)
    For i = 0 To nLen - 1
        abKey(i) = abPassword(i)
    Next
    
    CreateKeySimple = abKey

End Function

Public Function CreateKeyPBKDF2(abPassword() As Byte, ByVal nKeyLen As Long, abSalt() As Byte, ByVal nSaltLen As Long, nIterCount As Long) As Variant
' Creates a password-derived key using PBKDF2 from PKCS#5.
    Dim abKey() As Byte
    Dim nPwdLen As Long
    Dim lRet As Long
    
    nPwdLen = UBound(abPassword) - LBound(abPassword) + 1
    ReDim abKey(nKeyLen - 1)
    lRet = PBE_Kdf2(abKey(0), nKeyLen, abPassword(0), nPwdLen, abSalt(0), nSaltLen, nIterCount, 0&)
    
    CreateKeyPBKDF2 = abKey
End Function

Public Function CreateKeyPBKDF1(abPassword() As Byte, ByVal nKeyLen As Long, abSalt() As Byte, ByVal nIterCount As Long) As Variant
' Creates a password-derived key using PBKDF1 from PKCS #5.
' SHA-1 is the hash function.
    Dim abKey() As Byte
    Dim nPwdLen As Long
    Dim lRet As Long
    Dim abDigest(19) As Byte ' 20-byte array to receive digest
    Dim i As Long
    Dim abInput() As Byte
    Dim nSaltLen As Long
    
    If nKeyLen > 20 Then
        MsgBox "Derived key too long", vbCritical, "CreateKeyPBKDF1"
        Exit Function
    End If
    
    ' Concatentate password and 8-octet salt
    ' (there are more efficient ways to do this)
    nPwdLen = UBound(abPassword) - LBound(abPassword) + 1
    ReDim abInput(nPwdLen - 1 + 8)
    For i = 0 To nPwdLen - 1
        abInput(i) = abPassword(i)
    Next
    nSaltLen = UBound(abSalt) - LBound(abSalt) + 1
    If nSaltLen > 8 Then
        nSaltLen = 8
    End If
    For i = 0 To nSaltLen - 1
        abInput(nPwdLen + i) = abSalt(i)
    Next
        
    ' Compute T1 = Hash(P || S)
    lRet = SHA1_BytesHash(abDigest(0), abInput(0), nPwdLen + 8)
    ' Compute Ti = Hash(Ti-1) for i = 2 to c
    For i = 2 To 1000
        lRet = SHA1_BytesHash(abDigest(0), abDigest(0), 20)
    Next
    
    ' Copy required number of bytes from output into key
    ' DK = Tc<0..dkLen-1>
    ReDim abKey(nKeyLen - 1)
    For i = 0 To nKeyLen - 1
        abKey(i) = abDigest(i)
    Next
    
    CreateKeyPBKDF1 = abKey

End Function

