VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Oracle"
   ClientHeight    =   6315
   ClientLeft      =   2265
   ClientTop       =   1935
   ClientWidth     =   6390
   Icon            =   "Test.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Picture         =   "Test.frx":08CA
   ScaleHeight     =   6315
   ScaleWidth      =   6390
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   555
      Left            =   2040
      Picture         =   "Test.frx":16020C
      Style           =   1  'Graphical
      TabIndex        =   31
      ToolTipText     =   "Exit"
      Top             =   5700
      Width           =   1950
   End
   Begin VB.Timer Timer2 
      Interval        =   125
      Left            =   6540
      Top             =   4080
   End
   Begin VB.Timer Timer1 
      Interval        =   250
      Left            =   7260
      Top             =   3330
   End
   Begin VB.CommandButton Command11 
      Caption         =   "Last"
      Height          =   465
      Left            =   210
      TabIndex        =   27
      Top             =   4620
      Width           =   705
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Previous"
      Height          =   465
      Left            =   1680
      TabIndex        =   26
      Top             =   4620
      Width           =   765
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Next"
      Height          =   465
      Left            =   930
      TabIndex        =   25
      Top             =   4620
      Width           =   735
   End
   Begin VB.CommandButton Command8 
      Caption         =   "First"
      Height          =   465
      Left            =   2460
      TabIndex        =   24
      Top             =   4620
      Width           =   735
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Delete"
      Height          =   465
      Left            =   3210
      TabIndex        =   23
      Top             =   4620
      Width           =   735
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Edit"
      Height          =   465
      Left            =   3960
      TabIndex        =   22
      Top             =   4620
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Save"
      Height          =   465
      Left            =   4710
      TabIndex        =   21
      Top             =   4620
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Add"
      Height          =   465
      Left            =   5460
      TabIndex        =   20
      Top             =   4620
      Width           =   735
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   9
      Left            =   1920
      TabIndex        =   9
      Tag             =   "FAX"
      Text            =   " "
      Top             =   4200
      Width           =   2805
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   8
      Left            =   1920
      TabIndex        =   8
      Tag             =   "EMAILID"
      Text            =   " "
      Top             =   3780
      Width           =   2805
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   7
      Left            =   1920
      TabIndex        =   7
      Tag             =   "TELEP"
      Text            =   " "
      Top             =   3330
      Width           =   2805
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   6
      Left            =   1920
      TabIndex        =   6
      Tag             =   "IQAMA"
      Text            =   " "
      Top             =   2910
      Width           =   2805
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   5
      Left            =   1920
      TabIndex        =   5
      Tag             =   "JOBS"
      Text            =   " "
      Top             =   2490
      Width           =   2805
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   4
      Left            =   1920
      TabIndex        =   4
      Tag             =   "COUNTRY"
      Text            =   " "
      Top             =   2070
      Width           =   2805
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   3
      Left            =   1920
      TabIndex        =   3
      Tag             =   "SPONSOR"
      Text            =   " "
      Top             =   1650
      Width           =   4125
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   2
      Left            =   1920
      TabIndex        =   2
      Tag             =   "COMPANYNAME"
      Text            =   " "
      Top             =   1230
      Width           =   4095
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   1
      Left            =   1920
      TabIndex        =   1
      Tag             =   "NAME"
      Text            =   " "
      Top             =   810
      Width           =   4125
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   345
      Index           =   0
      Left            =   1920
      TabIndex        =   0
      Tag             =   "SERIALNO"
      Text            =   " "
      Top             =   390
      Width           =   1365
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "asnl_123@hotmail.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFF00&
      Height          =   195
      Left            =   2010
      MouseIcon       =   "Test.frx":160516
      MousePointer    =   99  'Custom
      TabIndex        =   30
      Top             =   5400
      Width           =   1950
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Your comments and suggestions will be respected."
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFF00&
      Height          =   345
      Left            =   180
      TabIndex        =   29
      Top             =   5100
      Width           =   6375
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Oracle Example"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   18
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1920
      TabIndex        =   28
      Top             =   0
      Width           =   2505
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Jobs :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   210
      TabIndex        =   19
      Top             =   2610
      Width           =   600
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Email Id :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   210
      TabIndex        =   18
      Top             =   3870
      Width           =   975
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ID No :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   210
      TabIndex        =   17
      Top             =   3030
      Width           =   765
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sponser Name :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   210
      TabIndex        =   16
      Top             =   1710
      Width           =   1605
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Fax No :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   210
      TabIndex        =   15
      Top             =   4230
      Width           =   870
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Telephone No:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   210
      TabIndex        =   14
      Top             =   3420
      Width           =   1485
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Country Name :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   210
      TabIndex        =   13
      Top             =   2160
      Width           =   1605
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Company Name :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   180
      TabIndex        =   12
      Top             =   1260
      Width           =   1740
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Name :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   210
      TabIndex        =   11
      Top             =   780
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Serial No :"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   10
      Top             =   360
      Width           =   1080
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''''''''''''''''''''''''''''''''''''''''''''''''''''**********************************'''''''''''''''''''''''''''''''''''''''
'   Design and developed by Mohammed Naimullah
'   Kalaya Technology and Information
'  Date 28-03-2005
'''''''''''''''''''''''''''''''''''''''''''''''''''************************************''''''''''''''''''''''''''''''''''''''

Dim CN As New ADODB.Connection
Dim OraSave As ADODB.Recordset
Dim OraEdit As ADODB.Recordset
Dim OracSearch As ADODB.Recordset
Dim OraAdd As ADODB.Recordset
Private Declare Function ShellExecute& Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long)

Private Sub Command1_Click()
End
End Sub

Private Sub Command1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
 Label5.FontSize = 8
     Label5.ForeColor = &HFFFF00
     Label5.FontUnderline = False
End Sub

Private Sub Command10_Click()
On Error Resume Next
OracSearch.MovePrevious
      Call DisplayRecord
End Sub

Private Sub Command11_Click()
On Error Resume Next
Set OracSearch = New ADODB.Recordset
    OracSearch.Open "SELECT * FROM EMPDETAILS", CN, 2, 3
        OracSearch.MoveFirst
      Call DisplayRecord
End Sub

Private Sub Command2_Click()
       On Error Resume Next
        Call Command11_Click
        
        If Text1(0).Text = "" Then
            Text1(0).Text = "1"
        Else
            Text1(0).Text = Val(Text1(0).Text) + 1
            End If
            
       For i = 1 To 9
             Text1(i).Text = ""
       Next i
End Sub

Private Sub Command3_Click()
On Error Resume Next
Set OraSave = New ADODB.Recordset
        OraSave.Open "select * from EMPDETAILS", CN, 2, 3
            OraSave.AddNew
                    
                    For i = 0 To 9
                               OraSave(Text1(i).Tag) = IIf(IsNull(Text1(i)), " ", Text1(i))
                     Next i
                     
                       OraSave.Update
                       OraSave.Close
                       Set OraSave = Nothing
                           MsgBox "Record save successfully"
End Sub

Private Sub Command5_Click()
Set OraEdit = New ADODB.Recordset
    OraEdit.Open "SELECT * FROM EMPDETAILS WHERE SERIALNO=" & Trim(Text1(0).Text) & "", CN, 2, 3
                 For i = 0 To 9
                     OraEdit(Text1(i).Tag) = IIf(IsNull(Text1(i)), " ", Text1(i))
                 Next i
                     OraEdit.Update
                     OraEdit.Close
        MsgBox "Record Edited"
End Sub

Private Sub Command6_Click()
On Error Resume Next
Dim msg
msg = MsgBox("Are you sure to delete this record", vbQuestion + vbYesNo, "Conformation")
If msg = vbYes Then
    CN.Execute "delete from EMPDETAILS where Serialno=" & Trim(Text1(0).Text) & ""
  OracSearch.MoveFirst
       
       Call DisplayRecord
End If
      'CN.Execute "delete from Nusera where NSerial=" & Trim(TxtSerial.Text) & ""
End Sub

Private Sub Command8_Click()
On Error Resume Next
Set OracSearch = New ADODB.Recordset
    OracSearch.Open "SELECT * FROM EMPDETAILS", CN, 2, 3
        OracSearch.MoveLast
       
       Call DisplayRecord
End Sub

Private Sub Command9_Click()
On Error Resume Next
OracSearch.MoveNext
      Call DisplayRecord
End Sub

Private Sub Form_Load()
On Error Resume Next
    Set CN = New ADODB.Connection
   
    CN.ConnectionString = "Provider=MSDAORA;user id=scott;password=tiger;"
    CN.CursorLocation = adUseClient
    CN.Open

    Set OracSearch = New ADODB.Recordset
    OracSearch.Open "SELECT * FROM EMPDETAILS", CN, 2, 3
        OracSearch.MoveFirst
       Call DisplayRecord
       
End Sub
Private Sub DisplayRecord()
For i = 0 To 9
       
            Text1(i) = IIf(IsNull(OracSearch(Text1(i).Tag)), "", OracSearch(Text1(i).Tag))
        Next i
End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
 Label5.FontSize = 8
     Label5.ForeColor = &HFFFF00
     Label5.FontUnderline = False
End Sub

Private Sub Label5_Click()
Screen.MousePointer = vbArrowHourglass
            Call ShellExecute(Me.hwnd, "open", "mailto:asnl_123@hotmail.com", vbNullString, CurDir$, SW_SHOW)
            Screen.MousePointer = vbNormal
End Sub

Private Sub Label5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
 Label5.FontSize = 12
    Label5.ForeColor = &HC0E0FF
    Label5.FontUnderline = True
    Label5.FontBold = True
End Sub

Private Sub Timer1_Timer()
Label2.ForeColor = vbGreen

End Sub

Private Sub Timer2_Timer()
Label2.ForeColor = vbRed

End Sub
