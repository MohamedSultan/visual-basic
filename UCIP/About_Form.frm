VERSION 5.00
Begin VB.Form About_Form 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About SulSat"
   ClientHeight    =   6405
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5520
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   5520
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   6255
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin VB.ListBox Versions 
         BackColor       =   &H8000000F&
         Height          =   840
         Left            =   240
         TabIndex        =   5
         Top             =   4800
         Width           =   4815
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1800
         TabIndex        =   4
         Top             =   5760
         Width           =   1455
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   1335
         Left            =   360
         Picture         =   "About_Form.frx":0000
         ScaleHeight     =   1335
         ScaleWidth      =   4815
         TabIndex        =   1
         Top             =   120
         Width           =   4815
      End
      Begin VB.Label Label1 
         Caption         =   "SulSat V1.0 "
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2055
         Left            =   240
         TabIndex        =   3
         Top             =   1560
         Width           =   4935
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   240
         TabIndex        =   2
         Top             =   3600
         Width           =   4815
      End
   End
End
Attribute VB_Name = "About_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Image1_Click()

End Sub

Private Sub Command1_Click()
    Call Slide(Me, Me.Left, Me.Width, False, 2)
End Sub


Private Sub Versions_Click()
    Select Case Versions.ListIndex
        Case 0
            Versions.ToolTipText = "Adding Batch Install Subscribers Added Using ACIP"
        Case 1
            Versions.ToolTipText = "Adding Install Subscribers Added Using ACIP"
        Case 2
            Versions.ToolTipText = "Adding the BGW history for Dealers"
        Case 3
            Versions.ToolTipText = "Making The Privilages Universal @ DB"
        Case 4
            Versions.ToolTipText = "Adding validation on SulSat Dates"
        Case 5
            Versions.ToolTipText = "DB and UCIP Enhancements as well as Versioning"
        Case 6
            Versions.ToolTipText = "Fulfilling Security, Finance and Fraud requiements"
        Case 7
            Versions.ToolTipText = "First Draft"
    End Select
End Sub
Public Sub SulSat_Data()
    About_Form.Label3 = "This Tool Is Developed By Mohamed Sultan" + vbCrLf + vbCrLf + _
                        "VAS IN Charging Senior Engineer" + vbCrLf + _
                        "mohamed.sultan@vodafone.com" + vbCrLf
    
    About_Form.Versions.Clear
    About_Form.Versions.AddItem "Version 2.5 is released on 21/05/2009", 0
    About_Form.Versions.AddItem "Version 2.4 is released on 02/05/2009", 1
    About_Form.Versions.AddItem "Version 2.3 is released on 12/04/2009", 2
    About_Form.Versions.AddItem "Version 2.2 is released on 08/04/2009", 3
    About_Form.Versions.AddItem "Version 2.2 is released on 22/01/2009", 4
    About_Form.Versions.AddItem "Version 2.1 is released on 12/01/2009", 5
    About_Form.Versions.AddItem "Version 2.0 is released on 23/12/2008", 6
    About_Form.Versions.AddItem "Version 1.0 is released on 12/06/2007", 7
    
    Call Slide(About_Form, SulSAT.Left + SulSAT.Width - About_Form.Width, About_Form.Width, True, 1.5)
End Sub

Private Sub Versions_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call Versions_Click
End Sub
