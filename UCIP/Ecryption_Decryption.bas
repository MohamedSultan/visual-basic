Attribute VB_Name = "Ecyption_Decryption"
Option Explicit
Const KEYBYTES As Long = 16
Const BLOCKBYTES As Long = 16
Public Function DECRYPT_String(ByVal Encrypted_String As String, Optional MyPassword As String) As String

    If MyPassword = "" Then MyPassword = "1234567890"

    Dim strPlain As String
    Dim abPlain() As Byte
    Dim strCipher64 As String
    Dim strPassword As String
    Dim abPassword() As Byte
    Dim abCipher() As Byte
    Dim abKey() As Byte
    Dim nLen As Long
    Dim nPad As Long
    Dim i As Long
    Dim lRet As Long
    Dim abInput() As Byte
    Dim abInitV() As Byte
    
    Dim txtKey, txtKeyDecrypt, txtDecrypted As String
    
    ' Get input strings as entered by user
    strCipher64 = Encrypted_String
    strPassword = MyPassword
    
    ' Convert to Bytes
    abPassword = StrConv(strPassword, vbFromUnicode)
    abInput = cnvBytesFromB64Str(strCipher64)
    nLen = UBound(abInput) - LBound(abInput) + 1
    nLen = nLen - BLOCKBYTES
    
    ' Strip off the first 16 bytes for the IV, the remainder is ciphertext
    ReDim abInitV(BLOCKBYTES - 1)
    For i = 0 To BLOCKBYTES - 1
        abInitV(i) = abInput(i)
    Next
    ReDim abCipher(nLen - 1)
    For i = 0 To nLen - 1
        abCipher(i) = abInput(BLOCKBYTES + i)
    Next
    
    ' Derive the key
    If SulSAT.optKDF1 Then
        abKey = CreateKeyPBKDF1(abPassword, KEYBYTES, abInitV, 1000)
    ElseIf SulSAT.optKDF2 Then
        abKey = CreateKeyPBKDF2(abPassword, KEYBYTES, abInitV, BLOCKBYTES, 2048)
    Else
        abKey = CreateKeySimple(abPassword, KEYBYTES)
    End If
    
    ' Display key in hex format
    txtKeyDecrypt = cnvHexStrFromBytes(abKey)
    
    ' Redimension the output buffer
    ReDim abPlain(UBound(abCipher))
    ' Do the decryption
    Debug.Print "KY=" & cnvHexStrFromBytes(abKey)
    Debug.Print "IV=" & cnvHexStrFromBytes(abInitV)
    Debug.Print "CT=" & cnvHexStrFromBytes(abCipher)
    lRet = AES128_BytesMode(abPlain(0), abCipher(0), nLen, abKey(0), Decrypt, "CBC", abInitV(0))
    Debug.Print "P'=" & cnvHexStrFromBytes(abPlain)
    
    ' Strip the padding bytes, if valid
    nPad = CByte(abPlain(nLen - 1))
    If nPad <= BLOCKBYTES Then
        ReDim Preserve abPlain(nLen - nPad - 1)
    Else
        MsgBox "Invalid padding byte", vbExclamation, "Decrypt"
    End If
        
    
    txtDecrypted = StrConv(abPlain, vbUnicode)
    DECRYPT_String = txtDecrypted

End Function


Public Function ENCRYPT_String(Password_String As String, Optional MyPassword As String) As String

    If MyPassword = "" Then MyPassword = "1234567890"

    Dim strPlain As String
    Dim abPlain() As Byte
    Dim strPassword As String
    Dim abPassword() As Byte
    Dim abCipher() As Byte
    Dim abKey() As Byte
    Dim nLen As Long
    Dim nPad As Long
    Dim i As Long
    Dim lRet As Long
    Dim abSalt() As Byte
    Dim abInitV() As Byte
    Dim abOutput() As Byte
    
    Dim txtKey, txtIV, txtCiphertext As String
    
    ' Get input strings as entered by user
    strPlain = Password_String
    strPassword = MyPassword
    
    ' Convert to Bytes
    abPassword = StrConv(strPassword, vbFromUnicode)
    abPlain = StrConv(strPlain, vbFromUnicode)
    
    ' Pad input to next multiple of encryption block size
    ' NB Byte arrays are zero-based
    nLen = UBound(abPlain) - LBound(abPlain) + 1
    nPad = ((nLen \ BLOCKBYTES) + 1) * BLOCKBYTES - nLen
    ReDim Preserve abPlain(nLen + nPad - 1)
    For i = nLen To nLen + nPad - 1
        abPlain(i) = CByte(nPad)
    Next
    
    ' Generate an IV
    ReDim abInitV(BLOCKBYTES - 1)
    lRet = RNG_NonceData(abInitV(0), BLOCKBYTES)
    txtIV = cnvHexStrFromBytes(abInitV)
    
    ' Derive the key
    If SulSAT.optKDF1 Then
        abKey = CreateKeyPBKDF1(abPassword, KEYBYTES, abInitV, 1000)
    ElseIf SulSAT.optKDF2 Then
        abKey = CreateKeyPBKDF2(abPassword, KEYBYTES, abInitV, BLOCKBYTES, 2048)
    Else
        abKey = CreateKeySimple(abPassword, KEYBYTES)
    End If
    
    ' Display key in hex format
    txtKey = cnvHexStrFromBytes(abKey)
    
    ' Redimension the ciphertext buffer
    ReDim abCipher(UBound(abPlain))
    ' Do the encryption
    Debug.Print "KY=" & cnvHexStrFromBytes(abKey)
    Debug.Print "IV=" & cnvHexStrFromBytes(abInitV)
    Debug.Print "PT=" & cnvHexStrFromBytes(abPlain)
    lRet = AES128_BytesMode(abCipher(0), abPlain(0), (nLen + nPad), abKey(0), Encrypt, "CBC", abInitV(0))
    Debug.Print "CT=" & cnvHexStrFromBytes(abCipher)
    
    ' Create an output buffer = IV + ciphertext
    ReDim abOutput(UBound(abCipher) + BLOCKBYTES)
    For i = 0 To BLOCKBYTES - 1
        abOutput(i) = abInitV(i)
    Next
    For i = 0 To nLen + nPad - 1
        abOutput(BLOCKBYTES + i) = abCipher(i)
    Next
        
    ' Encode into base64 format so we can display as ordinary text
    txtCiphertext = cnvB64StrFromBytes(abOutput)
    ENCRYPT_String = txtCiphertext
    
    ' Clean up (notwithstanding that the details are displayed on the form)
    WipeBytes abKey
    WipeBytes abPassword
    WipeBytes abPlain
    WipeString strPlain
    WipeString strPassword
    
End Function
