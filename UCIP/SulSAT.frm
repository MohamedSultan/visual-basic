VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form SulSAT 
   Caption         =   "SUL SAT"
   ClientHeight    =   7125
   ClientLeft      =   5565
   ClientTop       =   1755
   ClientWidth     =   8295
   LinkTopic       =   "Form1"
   ScaleHeight     =   7125
   ScaleWidth      =   8295
   Begin VB.CommandButton CCAPI_SW 
      Caption         =   "CCAPI Swith"
      Enabled         =   0   'False
      Height          =   615
      Left            =   7080
      TabIndex        =   73
      Top             =   5160
      Width           =   975
   End
   Begin VB.Frame Frame6 
      Caption         =   "Configurations"
      Height          =   855
      Left            =   3960
      TabIndex        =   70
      Top             =   4080
      Width           =   4215
      Begin VB.ComboBox AIR_IP 
         Height          =   315
         Left            =   840
         TabIndex        =   72
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label24 
         Caption         =   "AIR IP"
         Height          =   255
         Left            =   240
         TabIndex        =   71
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.CommandButton ShoW_UCIP 
      Caption         =   "Show UCIP Command"
      Height          =   615
      Left            =   5880
      TabIndex        =   46
      Top             =   5160
      Width           =   1095
   End
   Begin VB.CommandButton Save 
      Caption         =   "Save"
      Height          =   615
      Left            =   4080
      TabIndex        =   45
      Top             =   5160
      Width           =   1695
   End
   Begin VB.TextBox Success 
      Height          =   1965
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   40
      Top             =   7200
      Width           =   8055
   End
   Begin VB.Frame Frame4 
      Height          =   1815
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   8055
      Begin VB.TextBox supervisionPeriod 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         TabIndex        =   19
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox serviceFeePeriod 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         TabIndex        =   18
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox removalPeriod 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   17
         Top             =   1320
         Width           =   1815
      End
      Begin VB.TextBox creditClearancePeriod 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   16
         Top             =   960
         Width           =   1815
      End
      Begin VB.TextBox creditClearanceDate 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   6000
         TabIndex        =   15
         Top             =   960
         Width           =   1815
      End
      Begin VB.TextBox serviceRemovalDate 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   6000
         TabIndex        =   14
         Top             =   1320
         Width           =   1815
      End
      Begin VB.TextBox serviceFeeDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   6000
         TabIndex        =   13
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox supervisionDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   6000
         TabIndex        =   12
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label11 
         Caption         =   "Credit Clearance Period"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   960
         Width           =   1815
      End
      Begin VB.Label Label13 
         Caption         =   "Supervision Period"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label14 
         Caption         =   "Service Fee Period"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label Label15 
         Caption         =   "Service Removal Period"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1320
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "Service Removal Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   23
         Top             =   1320
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Service Fee Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   22
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Supervision Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   21
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Credit Clearance Date"
         Height          =   255
         Left            =   4080
         TabIndex        =   20
         Top             =   960
         Width           =   1815
      End
   End
   Begin VB.Frame Frame7 
      Height          =   1815
      Left            =   120
      TabIndex        =   11
      Top             =   3960
      Width           =   3855
      Begin VB.TextBox Activation_Number 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1920
         TabIndex        =   60
         Top             =   1320
         Width           =   1815
      End
      Begin VB.Frame Frame9 
         Height          =   1215
         Left            =   960
         TabIndex        =   36
         Top             =   0
         Width           =   1095
         Begin VB.ComboBox RefillProfileID 
            Height          =   315
            Left            =   240
            TabIndex        =   43
            Top             =   720
            Width           =   615
         End
         Begin VB.Frame Frame11 
            Height          =   495
            Left            =   0
            TabIndex        =   37
            Top             =   0
            Width           =   1095
            Begin VB.Label Label16 
               Caption         =   "Refill Profile"
               Height          =   255
               Left            =   120
               TabIndex        =   38
               Top             =   120
               Width           =   855
            End
         End
      End
      Begin VB.CommandButton Refill 
         Caption         =   "Refill"
         Height          =   495
         Left            =   2160
         TabIndex        =   28
         Top             =   480
         Width           =   1575
      End
      Begin VB.Frame Frame8 
         Height          =   1215
         Left            =   120
         TabIndex        =   29
         Top             =   0
         Width           =   855
         Begin VB.TextBox Refill_Amount_LE 
            Height          =   285
            Left            =   60
            TabIndex        =   33
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox Refill_Amount_Pt 
            Height          =   285
            Left            =   420
            TabIndex        =   32
            Top             =   720
            Width           =   375
         End
         Begin VB.Frame Frame10 
            Height          =   495
            Left            =   0
            TabIndex        =   30
            Top             =   0
            Width           =   855
            Begin VB.Label Label12 
               Caption         =   "Amount"
               Height          =   255
               Left            =   120
               TabIndex        =   31
               Top             =   120
               Width           =   615
            End
         End
         Begin VB.Label Label18 
            Caption         =   "Pt."
            Height          =   255
            Left            =   525
            TabIndex        =   34
            Top             =   480
            Width           =   255
         End
         Begin VB.Label Label17 
            Caption         =   "LE"
            Height          =   255
            Left            =   180
            TabIndex        =   35
            Top             =   480
            Width           =   375
         End
      End
      Begin VB.Label Label23 
         Caption         =   "Activation Number"
         Height          =   255
         Left            =   120
         TabIndex        =   59
         Top             =   1320
         Width           =   1455
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   3255
      Begin VB.CommandButton LoadAll 
         Caption         =   "Load"
         Height          =   320
         Left            =   2160
         TabIndex        =   63
         Top             =   240
         Width           =   975
      End
      Begin VB.ComboBox MSISDNs_List 
         Height          =   315
         Left            =   840
         TabIndex        =   44
         Text            =   "Combo1"
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "MSISDN"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Response"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   6000
      Width           =   8055
      Begin VB.TextBox Response_TExt 
         Height          =   285
         Left            =   840
         TabIndex        =   41
         Top             =   240
         Width           =   7095
      End
   End
   Begin VB.Frame Frame5 
      Height          =   2415
      Left            =   3360
      TabIndex        =   39
      Top             =   0
      Width           =   4815
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   3240
         TabIndex        =   68
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   3240
         TabIndex        =   67
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   3240
         TabIndex        =   66
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   3240
         TabIndex        =   65
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox DedicatedAccountExpiryDate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   3240
         TabIndex        =   64
         Top             =   600
         Width           =   1335
      End
      Begin VB.CommandButton GetBalances 
         Caption         =   "Load"
         Height          =   320
         Left            =   3360
         TabIndex        =   62
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   2280
         TabIndex        =   58
         Top             =   2040
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   2280
         TabIndex        =   57
         Top             =   1680
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   2280
         TabIndex        =   56
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   2280
         TabIndex        =   55
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox DedicateAccount 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   2280
         TabIndex        =   54
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox accountValue1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1800
         TabIndex        =   47
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label22 
         Caption         =   "Dedicated Account 1"
         Height          =   255
         Left            =   600
         TabIndex        =   53
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label21 
         Caption         =   "Dedicated Account 2"
         Height          =   255
         Left            =   600
         TabIndex        =   52
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label20 
         Caption         =   "Dedicated Account 5"
         Height          =   255
         Left            =   600
         TabIndex        =   51
         Top             =   2040
         Width           =   1695
      End
      Begin VB.Label Label19 
         Caption         =   "Dedicated Account 3"
         Height          =   255
         Left            =   600
         TabIndex        =   50
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Dedicated Account 4"
         Height          =   255
         Left            =   600
         TabIndex        =   49
         Top             =   1680
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Main Balance"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1815
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   3255
      Begin VB.CommandButton Activate 
         Caption         =   "Activate"
         Height          =   255
         Left            =   2280
         TabIndex        =   69
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Load 
         Caption         =   "Load"
         Height          =   255
         Left            =   120
         TabIndex        =   61
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox currentLanguageID 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1800
         TabIndex        =   6
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox serviceClassCurrent 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1800
         TabIndex        =   5
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Language"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label9 
         Caption         =   "Sub Status"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label Label10 
         Caption         =   "Service Class"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Sub_Status 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   1800
         TabIndex        =   7
         Top             =   1440
         Width           =   1215
      End
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   6960
      Top             =   5760
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSForms.ToggleButton ShowResponses_botton 
      Height          =   375
      Left            =   120
      TabIndex        =   42
      Top             =   6600
      Width           =   8055
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "14208;661"
      Value           =   "0"
      Caption         =   "Show Responses"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin VB.Menu Temp 
      Caption         =   $"SulSAT.frx":0000
      Enabled         =   0   'False
   End
   Begin VB.Menu About 
      Caption         =   "About"
   End
End
Attribute VB_Name = "SulSAT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Last_UCIP_Response As UCIP_Response_Type
   
Private Sub About_Click()
    MsgBox "This Tool Is Developed By Mohamed Sultan" + vbCrLf + vbCrLf + _
                "       VAS IN Charging Engineer" + vbCrLf + vbCrLf + _
                "       12/6/2007"
End Sub

Private Sub accountValue1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Activate_Click()
    Update_Response ("Get Account Details (Activate): " + Response_Code(GetAccountDetailsT_Command(MSISDNs_List, , "01100000")))
    Call Fill
End Sub

Private Sub Activation_Number_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub AIR_IP_Change()
    UCIP.AIR_IP = Me.AIR_IP
End Sub

Private Sub AIR_IP_LostFocus()
    Call Save_List(Me.AIR_IP, "AIR_IPs")
    Call AIR_IP_Change
End Sub

Private Sub CCAPI_SW_Click()
    Call Main.Sulsat2_command_Click
End Sub

Private Sub Command1_Click()
    MsgBox Winsock1.LocalHostName
    MsgBox Winsock1.LocalIP
End Sub

Private Sub currentLanguageID_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Form_Load()
    Call Load_List(SulSAT.RefillProfileID, "RefillProfiles")
    Call Load_List(SulSAT.MSISDNs_List, "MSISDNS")
    NumberOfDedicatedAccounts = 5
    RefillProfileID = ""
    For I = 1 To NumberOfDedicatedAccounts
        DedicatedAccountExpiryDate(I).Enabled = False
        DedicateAccount(I).Enabled = False
    Next I
    Call Load_AIR_IPs(Me.AIR_IP)
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub GetBalances_Click()
    If Sub_Status = "" Then Call LoadAll_Click
    Update_Response ("Balance Enquiry: " + Response_Code(BalanceEnquiryT_Command(MSISDNs_List)))
    Call Fill(True)
End Sub

Public Sub Load_Click()
    If Sub_Status = "" Then Call LoadAll_Click
    Update_Response ("Get Account Details: " + Response_Code(GetAccountDetailsT_Command(MSISDNs_List)))
    Call Fill
End Sub
Private Sub Fill(Optional WithRemainValues As Boolean)
        Last_UCIP_Response = UCIP_Response_Data
        With Me
            .accountValue1 = Val(UCIP_Response_Data.accountValue1) / 100
            .currentLanguageID = UCIP_Response_Data.currentLanguageID
            .serviceClassCurrent = UCIP_Response_Data.serviceClassCurrent
            .creditClearancePeriod = UCIP_Response_Data.creditClearancePeriod
            .serviceFeePeriod = UCIP_Response_Data.serviceFeePeriod
            .removalPeriod = UCIP_Response_Data.removalPeriod
            .supervisionPeriod = UCIP_Response_Data.supervisionPeriod
            
            .supervisionDate = Date_Only(UCIP_Response_Data.supervisionDate)
            .creditClearanceDate = Date_Only(UCIP_Response_Data.creditClearanceDate)
            .serviceRemovalDate = Date_Only(UCIP_Response_Data.serviceRemovalDate)
            .serviceFeeDate = Date_Only(UCIP_Response_Data.serviceFeeDate)
            
            For I = 1 To NumberOfDedicatedAccounts
                If UCIP_Response_Data.DedicatedAccount(I).DedicatedAccountValue <> "" Then
                    .DedicateAccount(I) = Val(UCIP_Response_Data.DedicatedAccount(I).DedicatedAccountValue) / 100
                    .DedicatedAccountExpiryDate(I) = Date_Only(UCIP_Response_Data.DedicatedAccount(I).DedicateAccountExpiryDate)
                Else
                    .DedicateAccount(I) = ""
                    .DedicatedAccountExpiryDate(I) = ""
                End If
            Next I
            
            Select Case UCIP_Response_Data.accountFlags
                Case "10000000"
                    .Sub_Status = "Active"
                    Label11.Enabled = True
                    Label13.Enabled = True
                    Label14.Enabled = True
                    Label15.Enabled = True
                    Label7.Enabled = True
                    Label6.Enabled = True
                    Label8.Enabled = True
                    Label5.Enabled = True
                Case "00000000"
                    .Sub_Status = "Not Active"
                    .supervisionDate = ""
                    .serviceClassCurrent = UCIP_Response_Data.serviceClassCurrent
                    .creditClearanceDate = ""
                    .serviceRemovalDate = ""
                    .serviceFeeDate = ""
                    .supervisionPeriod = ""
                    .removalPeriod = ""
                    .serviceFeePeriod = ""
                    .creditClearancePeriod = ""
                    Label11.Enabled = False
                    Label13.Enabled = False
                    Label14.Enabled = False
                    Label15.Enabled = False
                    Label7.Enabled = False
                    Label6.Enabled = False
                    Label8.Enabled = False
                    Label5.Enabled = False
            End Select
            If WithRemainValues And UCIP_Response_Data.ResponseCode = "0" Then
                .creditClearancePeriod = DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.supervisionDate)), ToDate(Date_Only(UCIP_Response_Data.creditClearanceDate)))
                .serviceFeePeriod = -DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.serviceFeeDate)), Date)
                .removalPeriod = DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.serviceFeeDate)), ToDate(Date_Only(UCIP_Response_Data.serviceRemovalDate)))
                .supervisionPeriod = -DateDiff("d", ToDate(Date_Only(UCIP_Response_Data.supervisionDate)), Date)
                Last_UCIP_Response.creditClearancePeriod = .creditClearancePeriod
                Last_UCIP_Response.serviceFeePeriod = .serviceFeePeriod
                Last_UCIP_Response.removalPeriod = .removalPeriod
                Last_UCIP_Response.supervisionPeriod = .supervisionPeriod
            End If
        End With
    
    
End Sub
Public Function Date_Formated(ByVal DateTime As String) As String
    Dim InvertedDate As String
    InvertedDate = SulSAT.Date_Inverted(DateTime)
    Date_Formated = Mid(InvertedDate, 5, 2) & "/" & Mid(InvertedDate, 7, 2) & "/" & Mid(InvertedDate, 1, 4)
End Function
Public Function Date_Only(ByRef DateTtime As String)
    Date_Only = Mid(DateTtime, 7, 2) + "\" + Mid(DateTtime, 5, 2) + "\" + Mid(DateTtime, 1, 4)
End Function
Public Function Date_Inverted(ByRef DateTtime As String)
    Dim Dates As CCPAI_Parser_Return
    DateTtime = Replace(DateTtime, "/", "\")
    Dates = Parser(DateTtime & "\", "\")
    If Dates.found And Dates.Ret(0) <> "" And DateTtime <> "" Then
        If Len(Dates.Ret(1)) <= 1 Then Dates.Ret(1) = "0" & Dates.Ret(1)
        If Len(Dates.Ret(0)) <= 1 Then Dates.Ret(0) = "0" & Dates.Ret(0)
        'Select Case UBound(Dates.Ret)
        '    Case 1
        '        Date_Inverted = Dates.Ret(1) & Dates.Ret(0)
        '    Case 2
                Date_Inverted = Dates.Ret(2) & Dates.Ret(1) & Dates.Ret(0)
        'End Select
    Else
        Date_Inverted = ""
    End If
End Function
Private Sub LoadAll_Click()
    Update_Response ("Get Account Details: " + Response_Code(GetAccountDetailsT_Command(MSISDNs_List)))
    Call Fill
    If UCIP_Response_Data.ResponseCode = "0" And UCIP_Response_Data.accountFlags <> "00000000" Then
        Update_Response ("Balance Enquiry: " + Response_Code(BalanceEnquiryT_Command(MSISDNs_List)))
        Call Fill(True)
    End If
End Sub

Private Sub MSISDNs_List_GotFocus()
    SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, True, 1
End Sub

Private Sub MSISDNs_List_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Or (KeyAscii = 32 And Len(MSISDNs_List.Text) = 0) Then
        SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, True, 1
    ElseIf KeyAscii = 13 Then
        SendMessage MSISDNs_List.hwnd, CB_SHOWDROPDOWN, 0, 1
        Call LoadAll_Click
    End If
    If KeyAscii = 32 And Len(MSISDNs_List.Text) = 0 Then KeyAscii = 0
End Sub

Private Sub MSISDNs_List_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode <> 8 And (KeyCode < 35 Or KeyCode > 40) And KeyCode <> 46 And KeyCode <> 13 Then
        PSTR = MSISDNs_List.Text
        For I = 0 To MSISDNs_List.ListCount - 1
            If StrComp(PSTR, (Left(MSISDNs_List.List(I), Len(PSTR))), vbTextCompare) = 0 Then
                MSISDNs_List.ListIndex = I
            Exit For
            End If
        Next I
        MSISDNs_List.SelStart = Len(PSTR)
        MSISDNs_List.SelLength = Len(MSISDNs_List.Text) - Len(PSTR)
    End If
End Sub

Private Sub MSISDNs_List_LostFocus()
    Call Save_List(MSISDNs_List, "MSISDNS", True)
    Call Roll_Out_Msisdns(MSISDNs_List)
End Sub

Private Sub Refill_Amount_LE_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Refill_Amount_Pt_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Refill_Click()

    If RefillProfileID <> "" Then
        If Refill_Amount_LE = "" Then Refill_Amount_LE = "0"
        If Refill_Amount_Pt = "" Then Refill_Amount_Pt = "0"
        Update_Response ("Refill: " + Response_Code(RefillT_Command(MSISDNs_List, Str(Refill_Amount_LE * 100 + Refill_Amount_Pt), RefillProfileID.Text, False, , SulSAT.Winsock1.LocalHostName, Me.Winsock1.LocalIP)))
        Call Response_Code(BalanceEnquiryT_Command(MSISDNs_List))
        Call Fill(True)
        RefillProfileID = ""
    Else
        If Activation_Number.Text <> "" Then
            Update_Response ("Standard Voucher Refill: " + Response_Code(StandardVoucherRefillT_Command(MSISDNs_List, Activation_Number, , , SulSAT.Winsock1.LocalHostName, Me.Winsock1.LocalIP)))
        Else
            MsgBox "You Have To Enter A Refill Profile ID Or An Activation Number", , "Error"
        End If
        Call Response_Code(BalanceEnquiryT_Command(MSISDNs_List))
        Call Fill(True)
    End If

End Sub

Private Sub RefillProfileID_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub Save_Click()
'    Dim Balance_Changed, serviceFeePeriod_Changed, supervisionPeriod_Changed, serviceFeeDate_Changed, supervisionDate_Changed As Boolean
    'Success = Success + vbCrLf + ""
    UCIP_Response_Data.ResponseCode = ""
    
    If Me.currentLanguageID <> Last_UCIP_Response.currentLanguageID Then Update_Response ("Update Account Details: " + Response_Code(UpdateAccountDetailsT_Command(MSISDNs_List, Last_UCIP_Response.currentLanguageID, Me.currentLanguageID)))
        
    If Me.serviceClassCurrent <> Last_UCIP_Response.serviceClassCurrent Then Update_Response ("Update Service Class: " + Response_Code(UpdateServiceClassT_Command(MSISDNs_List, Last_UCIP_Response.serviceClassCurrent, Me.serviceClassCurrent)))
    
    If Sub_Status = "Active" Then
        If Me.accountValue1 * 100 <> Last_UCIP_Response.accountValue1 Or _
                Me.serviceFeePeriod <> Last_UCIP_Response.serviceFeePeriod Or _
                Me.supervisionPeriod <> Last_UCIP_Response.supervisionPeriod Then
                    Update_Response ("Adjustment: " + Response_Code(AdjustmentT_Command(MSISDNs_List, Me.accountValue1 * 100 - Last_UCIP_Response.accountValue1, False, Me.supervisionPeriod - Last_UCIP_Response.supervisionPeriod, Me.serviceFeePeriod - Last_UCIP_Response.serviceFeePeriod, , , SulSAT.Winsock1.LocalHostName, Me.Winsock1.LocalIP)))
                    Call Response_Code(BalanceEnquiryT_Command(MSISDNs_List))
                    Call Fill(True)
        End If
        If Me.serviceFeeDate <> Date_Only(Last_UCIP_Response.serviceFeeDate) Or _
                Me.supervisionDate <> Date_Only(Last_UCIP_Response.supervisionDate) Then
            Dim DifferenceDays_SuperVision, DifferenceDays_ServiceFee As Integer
            DifferenceDays_SuperVision = DateDiff("d", ToDate(Date_Only(Last_UCIP_Response.supervisionDate)), ToDate(Me.supervisionDate))
            DifferenceDays_ServiceFee = DateDiff("d", ToDate(Date_Only(Last_UCIP_Response.serviceFeeDate)), ToDate(Me.serviceFeeDate))
            Update_Response ("Adjustment: " + Response_Code(AdjustmentT_Command(MSISDNs_List, Me.accountValue1 * 100 - Last_UCIP_Response.accountValue1, False, Trim(Str(DifferenceDays_SuperVision)), Trim(Str(DifferenceDays_ServiceFee)), , , SulSAT.Winsock1.LocalHostName, Me.Winsock1.LocalIP)))
            Call Response_Code(BalanceEnquiryT_Command(MSISDNs_List))
            Call Fill(True)
        End If
    End If
    
End Sub

Private Sub serviceClassCurrent_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub serviceFeeDate_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub serviceFeePeriod_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub



Public Sub ShoW_UCIP_Click()
    Call Slide(UCIP, 1000, UCIP.Width, True)
End Sub
Private Sub Update_Response(ByRef Response_Code_TExt As String)
    Success = Success + vbCrLf + Response_Code_TExt
    Success.SelStart = Len(Success)
    Response_TExt = Response_Code_TExt
End Sub

Private Sub ShowResponses_botton_Click()
        If Not ShowResponses_botton.Value Then
        Me.Height = Me.Height - 2300
        ShowResponses_botton.Value = 0
        Else
        Me.Height = Me.Height + 2300
        ShowResponses_botton.Value = 1
    End If
End Sub

Private Sub supervisionDate_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub

Private Sub supervisionPeriod_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Save_Click
End Sub


Public Function ToDate(ByRef Date_String As String) As String
    Dim DD, MM, YYYY, Loc1, Loc2 As Integer
    
    Loc1 = InStr(1, Date_String, "\")
    DD = Val(Mid(Date_String, 1, Loc1 - 1))
    Loc2 = InStr(Loc1 + 1, Date_String, "\")
    MM = Val(Mid(Date_String, Loc1 + 1, Loc2 - Loc1 - 1))
    YYYY = Val(Mid(Date_String, Loc2 + 1, 4))
    
    
    Select Case MM
        Case 1
            ToDate = Str(DD) + " Jan " + Str(YYYY)
        Case 2
            ToDate = Str(DD) + " Feb " + Str(YYYY)
        Case 3
            ToDate = Str(DD) + " Mar " + Str(YYYY)
        Case 4
            ToDate = Str(DD) + " Apr " + Str(YYYY)
        Case 5
            ToDate = Str(DD) + " May " + Str(YYYY)
        Case 6
            ToDate = Str(DD) + " Jun " + Str(YYYY)
        Case 7
            ToDate = Str(DD) + " Jul " + Str(YYYY)
        Case 8
            ToDate = Str(DD) + " Aug " + Str(YYYY)
        Case 9
            ToDate = Str(DD) + " Sep " + Str(YYYY)
        Case 10
            ToDate = Str(DD) + " Oct " + Str(YYYY)
        Case 11
            ToDate = Str(DD) + " Nov " + Str(YYYY)
        Case 12
            ToDate = Str(DD) + " Dec " + Str(YYYY)
    End Select
End Function

Public Function Subtract_Dates(ByRef Date1 As String, ByRef Date2 As String) As Integer
    ' the Date must be in format DD/MM/YYYY
   Dim DD1, MM1, YYYY1 As Integer
   Dim DD2, MM2, YYYY2 As Integer
   Dim DD_Reminder, MM_Reminder, YYYY_Reminder As Integer
   
   DD1 = Val(Mid(Date1, 1, 2))
   MM1 = Val(Mid(Date1, 4, 2))
   YYYY1 = Val(Mid(Date1, 7, 11))
   
   DD2 = Val(Mid(Date2, 1, 2))
   MM2 = Val(Mid(Date2, 4, 2))
   YYYY2 = Val(Mid(Date2, 7, 11))
   
   Select Case MM1
        Case 1, 3, 5, 7, 8, 10, 12
            MM1 = MM1 * 31
        Case 4, 6, 9, 11
            MM1 = MM1 * 30
        Case 2
            MM1 = MM1 * 28
   End Select
   
   YYYY_Reminder = (YYYY1 - YYYY2) * 365
   MM_Reminder = (MM1 - MM2)
   DD_Reminder = DD1 - DD2
        
End Function

