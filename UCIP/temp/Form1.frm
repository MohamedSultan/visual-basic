VERSION 5.00
Begin VB.Form txtStreet1 
   Caption         =   "Form1"
   ClientHeight    =   2430
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3240
   LinkTopic       =   "Form1"
   ScaleHeight     =   2430
   ScaleWidth      =   3240
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtZip 
      Height          =   285
      Left            =   2040
      MaxLength       =   5
      TabIndex        =   4
      Top             =   1320
      Width           =   975
   End
   Begin VB.TextBox txtState 
      Height          =   285
      Left            =   840
      MaxLength       =   2
      TabIndex        =   3
      Top             =   1320
      Width           =   375
   End
   Begin VB.TextBox txtCity 
      Height          =   285
      Left            =   840
      MaxLength       =   50
      TabIndex        =   2
      Top             =   960
      Width           =   2175
   End
   Begin VB.TextBox txtStreet 
      Height          =   285
      Left            =   840
      MaxLength       =   50
      TabIndex        =   1
      Top             =   600
      Width           =   2175
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   840
      MaxLength       =   50
      TabIndex        =   0
      Top             =   240
      Width           =   2175
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Add Record"
      Default         =   -1  'True
      Height          =   495
      Left            =   1080
      TabIndex        =   5
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Zip"
      Height          =   255
      Index           =   4
      Left            =   1680
      TabIndex        =   10
      Top             =   1320
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "State"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   9
      Top             =   1320
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "City"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   8
      Top             =   960
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Street"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   7
      Top             =   600
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Name"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   6
      Top             =   240
      Width           =   495
   End
End
Attribute VB_Name = "txtStreet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
Dim db_file As String
Dim statement As String
Dim conn As ADODB.Connection
Dim ctl As Control

    ' Get the data.
    db_file = App.Path
    If Right$(db_file, 1) <> "\" Then db_file = db_file & "\"
    db_file = db_file & "people.mdb"

    ' Open a connection.
    Set conn = New ADODB.Connection
    conn.ConnectionString = _
        "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & db_file & ";" & _
        "Persist Security Info=False"
    conn.Open

    ' Compose the INSERT statement.
    statement = "INSERT INTO Addresses " & _
        "(Name, Street, City, State, Zip) " & _
        " VALUES (" & _
        "'" & txtName.Text & "', " & _
        "'" & txtStreet.Text & "', " & _
        "'" & txtCity.Text & "', " & _
        "'" & txtState.Text & "', " & _
        "'" & txtZip.Text & "'" & _
        ")"

    ' Execute the statement.
    conn.Execute statement, , adCmdText

    ' Close the connection.
    conn.Close

    ' Clear the TextBoxes.
    For Each ctl In Controls
        If TypeOf ctl Is TextBox Then
            ctl.Text = ""
        End If
    Next ctl
End Sub
