Attribute VB_Name = "Responses"
Public Function Response_Code(ResponseCode As String) As String
    Select Case ResponseCode
        Case "0"
            Response_Code = "successful"
        Case "1"
            Response_Code = "okButSupervisionPeriodExceeded"
        Case "2"
            Response_Code = "okButServiceFeePeriodExceeded"
        Case "100"
            Response_Code = "otherError"
        Case "101"
            Response_Code = "not used"
        Case "102"
            Response_Code = "subscriberNotFound"
        Case "103"
            Response_Code = "accountBarredFromRefill"
        Case "104"
            Response_Code = "temporaryBlocked"
        Case "105"
            Response_Code = "dedicatedAccountNotAllowed"
        Case "106"
            Response_Code = "dedicatedAccountNegative"
        Case "107"
            Response_Code = "voucherStatusUsedBySame"
        Case "108"
            Response_Code = "voucherStatusUsedByDifferent"
        Case "109"
            Response_Code = "voucherStatusUnavailable"
        Case "110"
            Response_Code = "voucherStatusExpired"
        Case "111"
            Response_Code = "voucherStatusStolenOrMissing"
        Case "112"
            Response_Code = "voucherStatusDamaged"
        Case "113"
            Response_Code = "voucherStatusPending"
        Case "114"
            Response_Code = "voucherTypeNotAccepted"
        Case "115"
            Response_Code = "voucherGroupServiceClassErr"
        Case "116"
            Response_Code = "serviceClassHierarchyErr"
        Case "117"
            Response_Code = "serviceClassChangeNotAllowed"
        Case "118"
            Response_Code = "valueVoucherNotActive"
        Case "119"
            Response_Code = "invalidActivationNumber"
        Case "120"
            Response_Code = "invalidPaymentProfile"
        Case "121"
            Response_Code = "supervisionPeriodTooLong"
        Case "122"
            Response_Code = "serviceFeePeriodTooLong"
        Case "123"
            Response_Code = "maxCreditLimitExceeded"
        Case "124"
            Response_Code = "belowMinimumBalance"
        Case "125"
            Response_Code = "system unavailable"
        Case "126"
            Response_Code = "accountNotActive"
        Case "127"
            Response_Code = "accumulatorNotAvailable"
        Case "129"
            Response_Code = "fafNumberDoesNotExist"
        Case "130"
            Response_Code = "fafNumberNotAllowed, (see notAllowedReason for further information)"
        Case "133"
            Response_Code = "serviceClassChangeListEmpty"
        Case "134"
            Response_Code = "accumulatorOverflow"
        Case "135"
            Response_Code = "accumulatorUnderflow"
        Case "136"
            Response_Code = "dateAdjustmentError"
        Case "137"
            Response_Code = "ballanceEnquiryNotAllowed"
        Case "141"
            Response_Code = "Invalid language"
        Case "142"
            Response_Code = "Subscriber already installed"
        Case "155"
            Response_Code = "Invalid new service class"
        Case "157"
            Response_Code = "Invalid account home region"
        Case "9999"
            Response_Code = ""
        Case ""
            Response_Code = "Error Cannot Connect"
        Case Else
            Response_Code = "Error is Not Defined"
    End Select
End Function
Public Function ResponseCodeValue(ResponseTxt As String) As Integer
    Select Case LCase(ResponseTxt)
        Case "successful"
            ResponseCodeValue = 0
        Case "okbutsupervisionperiodexceeded"
            ResponseCodeValue = 1
        Case "okbutservicefeeperiodexceeded"
            ResponseCodeValue = 2
        Case "othererror"
            ResponseCodeValue = 100
        Case "not used"
            ResponseCodeValue = 101
        Case "subscribernotfound"
            ResponseCodeValue = 102
        Case "accountbarredfromrefill"
            ResponseCodeValue = 103
        Case "temporaryblocked"
            ResponseCodeValue = 104
        Case "dedicatedaccountnotallowed"
            ResponseCodeValue = 105
        Case "dedicatedaccountnegative"
            ResponseCodeValue = 106
        Case "voucherstatususedbysame"
            ResponseCodeValue = 107
        Case "voucherstatususedbydifferent"
            ResponseCodeValue = 108
        Case "voucherstatusunavailable"
            ResponseCodeValue = 109
        Case "voucherstatusexpired"
            ResponseCodeValue = 110
        Case "voucherstatusstolenormissing"
            ResponseCodeValue = 111
        Case "voucherstatusdamaged"
            ResponseCodeValue = 112
        Case "voucherstatuspending"
            ResponseCodeValue = 113
        Case "vouchertypenotaccepted"
            ResponseCodeValue = 114
        Case "vouchergroupserviceclasserr"
            ResponseCodeValue = 115
        Case "serviceclasshierarchyerr"
            ResponseCodeValue = 116
        Case "serviceclasschangenotallowed"
            ResponseCodeValue = 117
        Case "valuevouchernotactive"
            ResponseCodeValue = 118
        Case "invalidactivationnumber"
            ResponseCodeValue = 119
        Case "invalidpaymentprofile"
            ResponseCodeValue = 120
        Case "supervisionperiodtoolong"
            ResponseCodeValue = 121
        Case "servicefeeperiodtoolong"
            ResponseCodeValue = 122
        Case "maxcreditlimitexceeded"
            ResponseCodeValue = 123
        Case "belowminimumbalance"
            ResponseCodeValue = 124
        Case "system unavailable"
            ResponseCodeValue = 125
        Case "accountnotactive"
            ResponseCodeValue = 126
        Case "accumulatornotavailable"
            ResponseCodeValue = 127
        Case "fafnumberdoesnotexist"
            ResponseCodeValue = 129
        Case "fafnumbernotallowed, (see notallowedreason for further information)"
            ResponseCodeValue = 130
        Case "serviceclasschangelistempty"
            ResponseCodeValue = 133
        Case "accumulatoroverflow"
            ResponseCodeValue = 134
        Case "accumulatorunderflow"
            ResponseCodeValue = 135
        Case "dateadjustmenterror"
            ResponseCodeValue = 136
        Case "ballanceenquirynotallowed"
            ResponseCodeValue = 137
        Case ""
            ResponseCodeValue = 9999
        Case "error cannot connect"
            ResponseCodeValue = -1
    End Select
End Function

