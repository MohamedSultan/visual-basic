VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Begin VB.Form Test 
   Caption         =   "Form1"
   ClientHeight    =   8145
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   10230
   LinkTopic       =   "Form1"
   ScaleHeight     =   14.367
   ScaleMode       =   7  'Centimeter
   ScaleWidth      =   18.045
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Display 
      Height          =   3495
      Left            =   4680
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   11
      Text            =   "Test.frx":0000
      Top             =   4080
      Width           =   3375
   End
   Begin VB.TextBox Counter 
      Height          =   3405
      Left            =   1920
      MultiLine       =   -1  'True
      TabIndex        =   10
      Text            =   "Test.frx":0006
      Top             =   4080
      Width           =   2655
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Start_Stress_Test"
      Height          =   495
      Left            =   3480
      TabIndex        =   9
      Top             =   3600
      Width           =   1695
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   960
      Top             =   4080
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Command7"
      Height          =   375
      Left            =   2880
      TabIndex        =   8
      Top             =   2640
      Width           =   1695
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Screen"
      Height          =   495
      Left            =   720
      TabIndex        =   7
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   960
      TabIndex        =   6
      Text            =   "password"
      Top             =   1440
      Width           =   1575
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   960
      TabIndex        =   5
      Text            =   "User Name"
      Top             =   840
      Width           =   1575
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Scale"
      Height          =   495
      Left            =   2880
      TabIndex        =   4
      Top             =   2160
      Width           =   1695
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Adjust"
      Height          =   495
      Left            =   2880
      TabIndex        =   3
      Top             =   1680
      Width           =   1695
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Get_User_Name_ Test"
      Height          =   495
      Left            =   2880
      TabIndex        =   2
      Top             =   1200
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Authenticate Failed"
      Height          =   495
      Left            =   2880
      TabIndex        =   1
      Top             =   720
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Get_User_Name Sccess"
      Height          =   495
      Left            =   2880
      TabIndex        =   0
      Top             =   240
      Width           =   1695
   End
   Begin VB.Menu Logout 
      Caption         =   "Logout"
   End
   Begin VB.Menu D 
      Caption         =   ""
   End
   Begin VB.Menu Help 
      Caption         =   "Help"
   End
   Begin VB.Menu About 
      Caption         =   "About"
   End
End
Attribute VB_Name = "Test"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command4_Click()
    D.Caption = AddString(Int(Screen.Width / 121.348314606742))
End Sub
Private Function AddString(Times As Integer)
    For i = 0 To Times
        AddString = AddString + " "
    Next i
End Function
Private Sub Command5_Click()
    MsgBox Me.ScaleWidth
    'MsgBox Me.ScaleX(Me.Width, Me.Scale, vbCentimeters)
End Sub

Private Sub Command6_Click()
    'MsgBox Screen.Width
    MsgBox Screen.Height
End Sub

Private Sub Command7_Click()
    Dim AppExcel As Excel.Application
    Set AppExcel = New Excel.Application
    AppExcel.Workbooks.open "D:\Work\My Tools\UCIP\SulSat Application\test.xls"
    
    Dim Step As Integer
    Dim MSISDN As String
    Dim ServiceClass As String
    Dim Language As String
    Dim ResponseCodeID As String
    
    Step = 1
    While AppExcel.Sheets(1).Cells(Step, 1) <> ""
        MSISDN = AppExcel.ActiveSheet.Cells(Step, 1)
        ServiceClass = AppExcel.ActiveSheet.Cells(Step, 2)
        Language = AppExcel.ActiveSheet.Cells(Step, 3)
        
        If Language = "" Then Language = "3"
        
        If ServiceClass = "" Or MSISDN = "" Then
            MsgBox "PLZ Select The Desired Service Class", , "Error"
        Else
            Call SulSAT.ClearUCIP_Request_Data
            
            With UCIP_Request_Data
                .serviceClassNew = Trim(ServiceClass)
                .newLanguageID = Trim(Language)
                ResponseCodeID = ACIP_InstallSubscriber(MSISDN, Val(.serviceClassNew), Val(.newLanguageID))
                Call SulSAT.Update_Response("Install Subscriber: " + Response_Code(ResponseCodeID))
                Display = Display + MSISDN + "," + ServiceClass + "," + ResponseCodeID + vbCrLf
                Display.SelStart = Len(Display)
            End With
        End If
        Step = Step + 1
    Wend
End Sub

Private Sub Command8_Click()
    Dim Connections() As ADODB.Connection
    Dim Step As Integer
    
    On Error GoTo LocalHandler
    While Step <= 250
        ReDim Preserve Connections(Step)
        Set Connections(Step) = New ADODB.Connection
        Connections(Step).ConnectionString = "Provider=MSDAORA.1 ; Data Source=SulSat; Password=Sulsat_DB; User ID=Sulsat_DB ; Persist Security Info=True"
        Connections(Step).open
        Counter = Str(Step)
        If Step = 30 Then
            X = 1
        End If
        Step = Step + 1
        Wait 700
    Wend
LocalHandler:
    If Err.Number <> 0 And Err.Number <> 20 Then
        MsgBox Err.Description, , Str(Step)
        If Y = 1 Then Resume Next
        For i = 0 To UBound(Connections)
            If Connections(i).State = 1 Then Connections(i).Close
        Next i
    End If
End Sub

Private Sub Data1_Validate(Action As Integer, Save As Integer)

End Sub
