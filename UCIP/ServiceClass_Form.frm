VERSION 5.00
Begin VB.Form ServiceClass_Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "New Service Class"
   ClientHeight    =   2205
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3600
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   3600
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   255
      Left            =   1080
      TabIndex        =   2
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Frame Frame_Codes 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   3255
      Begin VB.ComboBox currentLanguageID 
         Height          =   315
         Left            =   1440
         TabIndex        =   8
         Top             =   720
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.ComboBox Language_Desc 
         Height          =   315
         Left            =   1440
         TabIndex        =   7
         Text            =   "Language_Desc"
         Top             =   720
         Width           =   1695
      End
      Begin VB.CommandButton Language_ID_Switch 
         Caption         =   "Language ID "
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   780
         Width           =   1215
      End
      Begin VB.ComboBox ServiceClass_Desc 
         Height          =   315
         Left            =   1440
         TabIndex        =   5
         Top             =   240
         Width           =   1695
      End
      Begin VB.CommandButton Service_Class_Switch 
         Caption         =   "Service Class   "
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   1215
      End
      Begin VB.ComboBox serviceClassCurrent 
         Height          =   315
         Left            =   1440
         TabIndex        =   3
         Text            =   "serviceClassCurrent"
         Top             =   240
         Visible         =   0   'False
         Width           =   1695
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Please Select The Desired Service Class and The Desired Language"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   3255
   End
End
Attribute VB_Name = "ServiceClass_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Function serviceClassID(ServiceClass As ComboBox, Text As String) As Integer
    For i = 0 To ServiceClass.ListCount - 1
        If ServiceClass.List(i) = Text Then serviceClassID = i
    Next i
End Function
Private Sub Form_Terminate()
    Call SulSAT.Dim_Undim_All(True)
End Sub
Private Function LanguageID(Language As ComboBox, Text As String) As Integer
    For i = 0 To Language.ListCount - 1
        If Language.List(i) = Text Then LanguageID = i
    Next i
End Function
Private Sub Language_ID_Switch_Click()
    Language_Desc.Visible = Not Language_Desc.Visible
    currentLanguageID.Visible = Not currentLanguageID.Visible
End Sub
Private Sub currentLanguageID_Change()
    Dim LanguageIndex As Integer
    LanguageIndex = LanguageID(currentLanguageID, currentLanguageID.Text)
    Language_Desc.Text = Language_Desc.List(LanguageIndex)
End Sub

Private Sub currentLanguageID_Click()
    Language_Desc.Text = Language_Desc.List(currentLanguageID.ListIndex)
End Sub
Private Sub Language_Desc_Click()
    currentLanguageID.Text = currentLanguageID.List(Language_Desc.ListIndex)
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub OK_Click()
    Dim ResponseCodeID As String
    If ServiceClass_Desc = "" Or serviceClassCurrent = "" Then
        MsgBox "PLZ Select The Desired Service Class", , "Error"
    Else
        Me.Hide
        SulSAT.SetFocus
        Call SulSAT.Dim_Undim_All(True)
        Call SulSAT.ClearUCIP_Request_Data
        
        With UCIP_Request_Data
            .serviceClassNew = serviceClassCurrent
            .newLanguageID = currentLanguageID
            ResponseCodeID = ACIP_InstallSubscriber(SulSAT.MSISDNs_List, Val(.serviceClassNew), Val(.newLanguageID))
            Call SulSAT.Update_Response("Install Subscriber: " + Response_Code(ResponseCodeID))
            If ResponseCodeID = "0" Then Call SulSAT.Load_Click
        End With
    End If
End Sub

Private Sub Service_Class_Switch_Click()
    serviceClassCurrent.Visible = Not serviceClassCurrent.Visible
    ServiceClass_Desc.Visible = Not ServiceClass_Desc.Visible
End Sub
Private Sub serviceClassCurrent_Click()
    ServiceClass_Desc.Text = ServiceClass_Desc.List(serviceClassCurrent.ListIndex)
End Sub
Private Sub serviceClassCurrent_Change()
    Dim serviceClassCurrentIndex As Integer
    serviceClassCurrentIndex = serviceClassID(serviceClassCurrent, serviceClassCurrent.Text)
    ServiceClass_Desc.Text = ServiceClass_Desc.List(serviceClassCurrentIndex)
End Sub
Private Sub ServiceClass_Desc_Click()
    serviceClassCurrent.Text = serviceClassCurrent.List(ServiceClass_Desc.ListIndex)
End Sub
