Attribute VB_Name = "GlobalVariables"
Type BGW_Return_Data_Type
    Date As String
    Time As String
    AdjType As String
    AdjCode As String
    Amount As String
    NOdeName As String
    ServiceClass As String
    MSISDN As String
End Type
Public Type BGW_Return_Type
    Found As Boolean
    BGW_Return_Data() As BGW_Return_Data_Type
End Type
Public Type SecurityLevelPrivilages_Type
    StandardRefill As Boolean
    Refill As Boolean
    Adjustment As Boolean
    Update_User As Boolean
    Activate As Boolean
    UCIP_Command As Boolean
    UpdateLanguage As Boolean
    Update_ServiceClass As Boolean
    Change_AIR As Boolean
    Lock_DB As Boolean
    BGW_History As Boolean
    BasicInformations As Boolean
    Balances As Boolean
    Accumulators As Boolean
    LoadAll As Boolean
    InstallSubscribers As Boolean
    Batch_Insert_MSISDN As Boolean
End Type
Type SecurityLevel_Type
    SecurityLevelName As String
    SecurityLevelPrivilages As SecurityLevelPrivilages_Type
End Type
Public AllSecurityLevels() As SecurityLevel_Type
