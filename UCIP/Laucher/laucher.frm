VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   Icon            =   "laucher.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   735
      Left            =   840
      TabIndex        =   0
      Top             =   600
      Width           =   2175
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents cFTP As clsFTP
Attribute cFTP.VB_VarHelpID = -1

Private Sub cFTP_FileTransferProgress(lCurrentBytes As Long, lTotalBytes As Long)
    Dim lPercent As Long
    lPercent = (lCurrentBytes * 100 / lTotalBytes)
    
    'Form2.pbUpload.Max = lTotalBytes
    Form2.pbUpload.Value = lPercent

End Sub

Private Sub Form_Load()
    
    Form2.Label1 = "Please Wait..." + vbCrLf + "Downloading New Version."
    Form2.Show
    Form2.SetFocus
    
    Dim bSuccess As Boolean
    Dim sError As String
    
    Set cFTP = New clsFTP
    
    Dim Pass, UName, ServerName, LocalFile, RemoteFile As String
    
    ServerName = "172.28.6.15"
    LocalFile = App.Path + "\SulSat.exe"
    RemoteFile = "SulSat.exe"
    UName = "peri"
    Pass = "peri"
   
    While IsProcessRunning("SulSat.exe")
        KillProcess ("SulSat.exe")
    Wend
    
    While IsProcessRunning("SulSat")
        KillProcess ("SulSat")
    Wend
    

    With cFTP
        If .OpenConnection(ServerName, UName, Pass) Then
            
            'for downloading
            Call cFTP.SetTransferType(FTP_BINARY)
            bSuccess = .FTPDownloadFile(LocalFile, RemoteFile)
            
            If bSuccess Then
                sError = "File Upgraded"
                Form2.Label1 = "Download Complete."
                Form2.SetFocus
                Wait 1000
                Call Version_File(Encrypt(Version_DB))
                Shell App.Path + "\SulSat.exe", vbNormalFocus
                End
            Else
                sError = cFTP.SimpleLastErrorMessage
                
            End If
            
            .CloseConnection
        Else
            sError = .SimpleLastErrorMessage
        End If
        
    End With
    
    
    MsgBox sError
    
    'unload/terminate the ftp object
    Set cFTP = Nothing
    
End Sub
