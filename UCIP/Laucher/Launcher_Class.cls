VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Launcher_Class"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Sub Class_Initialize()
    Dim cFTP As clsFTP
    Dim bSuccess As Boolean
    Dim sError As String
    
    Set cFTP = New clsFTP
    
    With cFTP
        If .OpenConnection(ServerName, UName, Pass) Then
            
            'for downloading
            bSuccess = .FTPDownloadFile(LocalFile, RemoteFile)
            
            'If you are uploading
            'bSuccess = .FTPUploadFile(LocalFile, RemoteFile)
            
            If bSuccess = False Then
                sError = .SimpleLastErrorMessage
            Else
                sError = "Success!"
            End If
            
            .CloseConnection
        Else
            .CloseConnection
            sError = .SimpleLastErrorMessage
        End If
    End With
    
    
    MsgBox sError
End Sub

