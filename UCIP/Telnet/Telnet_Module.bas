Attribute VB_Name = "Telnet_Module"
Public Function Create_Telnet_Form(ByRef IP_Address As String, _
                                ByRef Login As String, _
                                ByRef Password As String, _
                                Optional ByRef Start_Up_Path As String, _
                                Optional Port As Integer, _
                                Optional ByRef SendCommand1 As String, _
                                Optional ByRef SendCommand2 As String, _
                                Optional ByRef SendCommand3 As String, _
                                Optional ByRef SendCommand4 As String, _
                                Optional ping As Boolean) As Integer
    Dim SendCommand(3) As String
        SendCommand(0) = SendCommand1
        SendCommand(1) = SendCommand2
        SendCommand(2) = SendCommand3
        SendCommand(3) = SendCommand4
    
    If Port = 0 Then Port = 23
    'ReDim Preserve Active_Telnet_Form(Number_Of_Active_Telnet_Connections)
    'Set Active_Telnet_Form(Number_Of_Active_Telnet_Connections) = New Telnet_Instance_Form
    '
    'Dim Form_Caption As String
    'Form_Caption = "Telnet Form ("
    'Select Case IP_Address
    '    Case "172.28.6.15"
    '        Form_Caption = "Test MPS " + Form_Caption
    '    Case "10.231.14.21"
    '       Form_Caption = "Reda's " + Form_Caption
    'End Select
    
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Caption = Form_Caption + Trim(Str(Number_Of_Active_Telnet_Connections)) + ")"
    'Dim Sultan As Form
    'Set Sultan = Active_Telnet_Form(Number_Of_Active_Telnet_Connections)
    'Call Slide(Sultan, Main.Left + Main.Width, Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Width, True)
    
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Login = Login
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Password = Password
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Path = Start_Up_Path
    '
    'For j = 0 To UBound(SendCommand)
    '    Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Additional_Commands(j) = SendCommand(j)
    'Next j
    
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Telnet_Connect.RemotePort = Port
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Telnet_Connect.Connect IP_Address
    'Active_Telnet_Form(Number_Of_Active_Telnet_Connections).Telnet_Timer.Enabled = True
    '
    'Dim Step_Color As Double
    'Step_Color = 99000
    With Active_Telnet_Form(Number_Of_Active_Telnet_Connections)
        If Number_Of_Active_Telnet_Connections = 0 Then
            .Display.BackColor = select_Color(.Display.BackColor)
            .Command_To_Be_Sent.BackColor = select_Color(.Command_To_Be_Sent.BackColor)
        Else
            .Display.BackColor = select_Color(Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Display.BackColor)
            .Command_To_Be_Sent.BackColor = select_Color(Active_Telnet_Form(Number_Of_Active_Telnet_Connections - 1).Command_To_Be_Sent.BackColor)
        End If
    End With
    Create_Telnet_Form = Number_Of_Active_Telnet_Connections
    Number_Of_Active_Telnet_Connections = Number_Of_Active_Telnet_Connections + 1
End Function

