VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{92BC655F-C026-4685-8A65-F14E673E29BA}#7.0#0"; "Telnet.ocx"
Begin VB.Form BillingGW_Form 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dealers Account History"
   ClientHeight    =   6795
   ClientLeft      =   6015
   ClientTop       =   2925
   ClientWidth     =   9555
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6795
   ScaleWidth      =   9555
   Begin VB.TextBox Sub_Display 
      Height          =   1335
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   41
      Top             =   6840
      Width           =   9375
   End
   Begin VB.ListBox Status 
      Height          =   255
      Left            =   120
      TabIndex        =   39
      Top             =   6000
      Width           =   9375
   End
   Begin Telnet_Control.Telnet Telnet_BillingGW 
      Left            =   240
      Top             =   2040
      _ExtentX        =   900
      _ExtentY        =   900
   End
   Begin VB.Timer Timer_BilllingGW 
      Interval        =   1000
      Left            =   240
      Top             =   2640
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   3975
      Left            =   9960
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   4215
      Begin VB.TextBox Command1_Part2 
         Height          =   285
         Left            =   2640
         TabIndex        =   21
         Top             =   1440
         Width           =   1455
      End
      Begin VB.TextBox Command1_Part1 
         Height          =   285
         Left            =   2640
         TabIndex        =   20
         Top             =   1200
         Width           =   1455
      End
      Begin VB.CommandButton Init 
         Caption         =   "Initiate"
         Height          =   255
         Left            =   1920
         TabIndex        =   19
         Top             =   3600
         Width           =   1095
      End
      Begin VB.CommandButton Connect 
         Caption         =   "Connect"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   3600
         Width           =   1575
      End
      Begin VB.TextBox IP 
         Height          =   285
         Left            =   960
         TabIndex        =   16
         Text            =   "Text1"
         Top             =   2760
         Width           =   1575
      End
      Begin VB.TextBox Port 
         Height          =   285
         Left            =   960
         TabIndex        =   14
         Text            =   "Text1"
         Top             =   2400
         Width           =   1575
      End
      Begin VB.TextBox Command 
         Height          =   285
         Index           =   3
         Left            =   960
         TabIndex        =   10
         Top             =   2040
         Width           =   1575
      End
      Begin VB.TextBox Command 
         Height          =   285
         Index           =   2
         Left            =   960
         TabIndex        =   9
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox Command 
         Height          =   285
         Index           =   1
         Left            =   960
         TabIndex        =   8
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox Command 
         Height          =   285
         Index           =   0
         Left            =   960
         TabIndex        =   6
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox Password 
         Height          =   285
         Left            =   960
         TabIndex        =   3
         Text            =   "Text1"
         Top             =   570
         Width           =   1575
      End
      Begin VB.TextBox UserName 
         Height          =   285
         Left            =   960
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label8 
         Caption         =   "IP"
         Height          =   375
         Left            =   120
         TabIndex        =   17
         Top             =   2760
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "Port"
         Height          =   375
         Left            =   120
         TabIndex        =   15
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label Label6 
         Caption         =   "Command3"
         Height          =   375
         Left            =   120
         TabIndex        =   13
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Command2"
         Height          =   375
         Left            =   120
         TabIndex        =   12
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Command1"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Command0"
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   990
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Password"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "User Name"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.TextBox Display 
      Height          =   1215
      Left            =   9840
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   4200
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.Frame Frame2 
      Height          =   5895
      Left            =   120
      TabIndex        =   22
      Top             =   0
      Width           =   9375
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   7
         Left            =   8040
         TabIndex        =   23
         Top             =   600
         Width           =   975
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   6
         Left            =   6960
         TabIndex        =   24
         Top             =   600
         Width           =   1455
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   5
         Left            =   5400
         TabIndex        =   25
         Top             =   600
         Width           =   1935
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   4
         Left            =   4680
         TabIndex        =   26
         Top             =   600
         Width           =   1095
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   3
         Left            =   3480
         TabIndex        =   27
         Top             =   600
         Width           =   1575
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   2
         Left            =   2520
         TabIndex        =   28
         Top             =   600
         Width           =   1335
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   1
         Left            =   1440
         TabIndex        =   29
         Top             =   600
         Width           =   1455
      End
      Begin VB.ListBox History_List 
         Height          =   4935
         Index           =   0
         Left            =   120
         TabIndex        =   30
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label9 
         Caption         =   "Date"
         Height          =   255
         Left            =   480
         TabIndex        =   38
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "Time"
         Height          =   255
         Left            =   1680
         TabIndex        =   37
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label11 
         Caption         =   "MSISDN"
         Height          =   255
         Left            =   2640
         TabIndex        =   36
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label12 
         Caption         =   "Amount"
         Height          =   255
         Left            =   3720
         TabIndex        =   35
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label13 
         Caption         =   "Adj Type"
         Height          =   255
         Index           =   0
         Left            =   4680
         TabIndex        =   34
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label14 
         Caption         =   "Adj Code"
         Height          =   255
         Left            =   5880
         TabIndex        =   33
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label13 
         Caption         =   "Service Class"
         Height          =   255
         Index           =   1
         Left            =   6960
         TabIndex        =   32
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label13 
         Caption         =   "Node Type"
         Height          =   255
         Index           =   2
         Left            =   8160
         TabIndex        =   31
         Top             =   240
         Width           =   1095
      End
   End
   Begin MSForms.ToggleButton Details 
      Height          =   375
      Left            =   120
      TabIndex        =   40
      Top             =   6360
      Width           =   9375
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "16536;661"
      Value           =   "0"
      Caption         =   "More Details"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "BillingGW_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim Login, Password, Command(3) As String
Dim AuthenticationFinished As Boolean
Dim History As BGW_Return_Type

Public Sub Connect_Click()
    Me.Telnet_BillingGW.Disconnect
    Me.Telnet_BillingGW.RemotePort = Me.Port
    'Telnet_BillingGW.RemotePort = "10023"
    Me.Telnet_BillingGW.Connect Me.IP
    Me.Timer_BilllingGW.Enabled = True
End Sub

Private Sub Details_Click()
        If Not Details.Value Then
            Me.Height = Me.Height - (8760 - 7305)
            Details.Value = 0
        Else
            Me.Height = Me.Height + (8760 - 7305)
            Details.Value = 1
    End If
End Sub

Private Sub Form_Load()
    Status.AddItem "Please Wait While Connecting . . ."
End Sub

Private Sub Form_Terminate()
    Telnet_BillingGW.Disconnect
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub History_List_Click(Index As Integer)
    For i = 0 To History_List.UBound
        If i <> Index Then
            History_List(i).ListIndex = History_List(Index).ListIndex
            History_List(i).TopIndex = History_List(Index).TopIndex
        End If
    Next i
End Sub

Private Sub History_List_Scroll(Index As Integer)
    Call History_List_Click(Index)
End Sub

Private Sub Init_Click()
    With BillingGW_Form
        .UserName = "aregaily"
        .Password = "aregaily123"
        .UserName = "peri"
        .Password = "peri"
        'Path = "Sultan"
        .Command(0) = "cd /var/opt/BGw/Server1/Vodafone/A1000vol1/CS30/genReports"
        .Command(1) = "grep ,5000 PPAS-ADJ_* | grep 160405626"
        .Command(2) = "echo Finished"
        
        .Port = "23"
        'Telnet_BillingGW.RemotePort = "10023"
        .IP = "172.28.6.15"
        .Connect_Click
        .Show
    End With
End Sub

Private Sub ReverseStatusData(NewData As String)
    Dim Data() As String
    ReDim Data(0)
    Dim Step As Integer
    For i = 0 To Status.ListCount - 1
        ReDim Preserve Data(i)
        Data(i) = Status.List(i)
    Next i
    Status.Clear
    Step = 0
    Status.AddItem NewData
    For i = UBound(Data) To 0 Step -1
        Status.AddItem Data(Step)
        Step = Step + 1
    Next i
    
End Sub
Private Sub Telnet_BillingGW_Connected()
    Caption = Caption + " - Connected"
    Call ReverseStatusData("Connected, Please Wait while Retrieving Data")
End Sub

Private Sub Telnet_BillingGW_DataRecieved(ByVal Data As String)
    'Print InStr(1, Data, "Finished")
    If AuthenticationFinished And InStr(1, Data, "Finished") <> 0 Then
        Display = Display & Data
        Display.SelStart = Len(Display)
        History = ParseBGWData(Display)
        Telnet_BillingGW.Disconnect
        Caption = Left(Caption, Len(Caption) - Len(" - Connected"))
        Call ReverseStatusData("Data Retrieved, Disconnected")
        If History.Found Then
            Call ReverseStatusData("Arranging")
            Call Arrange_History
            Call ReverseStatusData("Displaying Data")
            Call DistributeHistory
            Call ReverseStatusData("")
        End If
    Else
        Timer_BilllingGW.Enabled = True
        Display = Display & Data
        Display.SelStart = Len(Display)
        If InStr(1, Display, "PPAS-ADJ") <> 0 Then Call ReverseStatusData("Retrieving Data . . .")
        If AuthenticationFinished Then
            Sub_Display = Display
            Sub_Display.SelStart = Len(Sub_Display)
        End If
    End If
End Sub
Private Sub Arrange_History()
    
    Dim Max As String
    Dim Step, Max_ID As Integer
    Dim ArrangedHistory As BGW_Return_Type
    'If History Is Empty Then X = 1
    ReDim ArrangedHistory.BGW_Return_Data(UBound(History.BGW_Return_Data))
    
    While Step <= UBound(History.BGW_Return_Data)
        For i = 0 To UBound(History.BGW_Return_Data)
            If History.BGW_Return_Data(i).Date + History.BGW_Return_Data(i).Time > Max Then
                Max = History.BGW_Return_Data(i).Date + History.BGW_Return_Data(i).Time
                Max_ID = i
            End If
        Next i
        ArrangedHistory.BGW_Return_Data(Step) = History.BGW_Return_Data(Max_ID)
        History.BGW_Return_Data(Max_ID).Date = ""
        History.BGW_Return_Data(Max_ID).Time = ""
        Max = ""
        Step = Step + 1
    Wend
    History = ArrangedHistory
End Sub
Private Sub DistributeHistory()
    Dim Date_ As String
    For i = 0 To UBound(History.BGW_Return_Data)
        Date_ = Trim(History.BGW_Return_Data(i).Date)
        History_List(0).AddItem Right(Date_, 2) + "/" + Mid(Date_, 5, 2) + "/" + Left(Date_, 4)
        History_List(1).AddItem History.BGW_Return_Data(i).Time
        History_List(2).AddItem History.BGW_Return_Data(i).MSISDN
        History_List(3).AddItem History.BGW_Return_Data(i).Amount
        History_List(4).AddItem History.BGW_Return_Data(i).AdjType
        History_List(5).AddItem History.BGW_Return_Data(i).AdjCode
        History_List(6).AddItem History.BGW_Return_Data(i).ServiceClass
        History_List(7).AddItem History.BGW_Return_Data(i).NOdeName
    Next i
End Sub
Private Function ParseBGWData(DataReceived As String) As BGW_Return_Type
    Dim ParsedData_Lines As CCPAI_Parser_Return
    Dim ParsedData As CCPAI_Parser_Return
    Dim BGW_Return As BGW_Return_Type
    Dim Step As Integer
    Dim Date_Time As String
    
    ParsedData_Lines = Parser(DataReceived + vbCrLf, vbCrLf)
    If ParsedData_Lines.Found Then
        For i = 0 To UBound(ParsedData_Lines.Ret) - 1
            If InStr(1, ParsedData_Lines.Ret(i), "PPAS-ADJ") <> 0 Then
                ParsedData = Parser(ParsedData_Lines.Ret(i) + ",", ",")
                If ParsedData.Found Then
                    BGW_Return.Found = True
                    ReDim Preserve BGW_Return.BGW_Return_Data(Step)
                    BGW_Return.BGW_Return_Data(Step).MSISDN = Right(ParsedData.Ret(0), 9)
                    Date_Time = ParsedData.Ret(1)
                    BGW_Return.BGW_Return_Data(Step).Date = Left(Date_Time, 8)
                    BGW_Return.BGW_Return_Data(Step).Time = Right(Date_Time, 8)
                    BGW_Return.BGW_Return_Data(Step).AdjType = ParsedData.Ret(2)
                    BGW_Return.BGW_Return_Data(Step).AdjCode = ParsedData.Ret(3)
                    BGW_Return.BGW_Return_Data(Step).Amount = ParsedData.Ret(4)
                    BGW_Return.BGW_Return_Data(Step).NOdeName = ParsedData.Ret(5)
                    BGW_Return.BGW_Return_Data(Step).ServiceClass = ParsedData.Ret(6)
                    Step = Step + 1
                End If
            End If
        Next i
    End If
    ParseBGWData = BGW_Return
End Function
Private Sub Telnet_BillingGW_ErrorOccured(ByVal ErrorDesc As String)
    If ErrorDesc <> "Connection is aborted due to timeout or other failure" Then
        MsgBox ("Error Occure : " & ErrorDesc)
        Call Slide(Me, Me.Left, Me.Width, False)
    Else
        MsgBox ("Error Occure : " & ErrorDesc)
    End If
End Sub

Private Sub Timer_BilllingGW_Timer()
    If InStr(1, Display, "login:") <> 0 Then
        Telnet_BillingGW.SendData UserName, True
        Display = ""
    End If
    If InStr(1, Display, "Password:") <> 0 Then
        Telnet_BillingGW.SendData Password, True
        
        Wait 500
        
        Telnet_BillingGW.SendData Command(0), True
        Display = ""
        
        For i = 1 To Command.UBound
            If Command(i) <> "" Then Telnet_BillingGW.SendData Command(i), True
        Next i
       
       AuthenticationFinished = True
    End If
    Timer_BilllingGW.Enabled = False
End Sub
