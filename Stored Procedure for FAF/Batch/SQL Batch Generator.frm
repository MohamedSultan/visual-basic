VERSION 5.00
Begin VB.Form FAF 
   Caption         =   "SQL Generator"
   ClientHeight    =   4800
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13800
   LinkTopic       =   "Form1"
   ScaleHeight     =   4800
   ScaleWidth      =   13800
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Batch 
      Caption         =   "Batch"
      Height          =   495
      Left            =   360
      TabIndex        =   22
      Top             =   3960
      Width           =   1095
   End
   Begin VB.ComboBox Types 
      Height          =   315
      Index           =   4
      Left            =   2400
      TabIndex        =   21
      Text            =   "VFSubscriber"
      Top             =   1800
      Width           =   1335
   End
   Begin VB.TextBox Member 
      Height          =   285
      Index           =   4
      Left            =   960
      TabIndex        =   20
      Top             =   1800
      Width           =   1335
   End
   Begin VB.TextBox Member 
      Height          =   285
      Index           =   3
      Left            =   960
      TabIndex        =   19
      Text            =   "0643205083"
      Top             =   1440
      Width           =   1335
   End
   Begin VB.TextBox Member 
      Height          =   285
      Index           =   2
      Left            =   960
      TabIndex        =   18
      Text            =   "016XXXXXXX"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Member 
      Height          =   285
      Index           =   1
      Left            =   960
      TabIndex        =   17
      Text            =   "010XXXXXXX"
      Top             =   720
      Width           =   1335
   End
   Begin VB.TextBox Product_Name 
      Height          =   285
      Left            =   1320
      TabIndex        =   16
      Text            =   "FAMILY 1"
      Top             =   2760
      Width           =   2415
   End
   Begin VB.TextBox Family_ID 
      Height          =   285
      Left            =   1320
      TabIndex        =   15
      Text            =   " "
      Top             =   3120
      Width           =   2415
   End
   Begin VB.TextBox Owner 
      Height          =   285
      Left            =   960
      TabIndex        =   14
      Text            =   "101111111"
      Top             =   240
      Width           =   1335
   End
   Begin VB.CommandButton Generate 
      Caption         =   "Generate"
      Height          =   495
      Left            =   1920
      TabIndex        =   13
      Top             =   3960
      Width           =   1575
   End
   Begin VB.ComboBox Types 
      Height          =   315
      Index           =   3
      Left            =   2400
      TabIndex        =   12
      Top             =   1440
      Width           =   1335
   End
   Begin VB.ComboBox Types 
      Height          =   315
      Index           =   2
      Left            =   2400
      TabIndex        =   11
      Text            =   "VFSubscriber"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.ComboBox Types 
      Height          =   315
      Index           =   1
      Left            =   2400
      TabIndex        =   10
      Text            =   "VFSubscriber"
      Top             =   720
      Width           =   1335
   End
   Begin VB.TextBox Display 
      Height          =   4215
      Left            =   3840
      MultiLine       =   -1  'True
      TabIndex        =   9
      Top             =   240
      Width           =   9735
   End
   Begin VB.ComboBox Operation_Code 
      Height          =   315
      Left            =   1320
      TabIndex        =   7
      Text            =   "111"
      Top             =   2400
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "Family ID"
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   8
      Top             =   3120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Member1"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Member2"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   5
      Top             =   1080
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Member3"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   4
      Top             =   1440
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Member4"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Operation"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   2400
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Product Name"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   2760
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Owner"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   735
   End
End
Attribute VB_Name = "FAF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Batch_Click()

    Call Load_Numbers
    Dim Step2 As Integer
    
    For Owners = 0 To UBound(Owner_MSISDN)
        Owner = Owner_MSISDN(Owners)
        
        For i = 1 To 3
            Member(i) = Member_MSISDN(Step2)
            Step2 = Step2 + 1
        Next i
    
        
        Select Case Operation_Code
                Case "Create Family"
                    Display = "InsertFamilyRequest(" + "'" + Trim(Str(Val(Owner))) + "'" + ",''" + _
                        Create_Members_XML(100) + _
                        "''," + Trim(Str(opcode(Operation_Code))) + "," + Zero_Ext(Product_Name, 10) + "," + _
                        Zero_Ext(Family_ID, 15) + ",eCode,eDescription);"
            
                Case "Add Member"
                Case "Delete Member"
                Case "Delete Family"
                    Display = "InsertFamilyRequest(" + "'" + Trim(Str(Val(Owner))) + "'" + ",''" + _
                                Create_Members_XML(101) + _
                                "''," + Trim(Str(opcode(Operation_Code))) + "," + Zero_Ext(Product_Name, 10) + "," + _
                                Zero_Ext(Family_ID, 15) + ",eCode,eDescription);"
            
        End Select
        
        Dim SQL_File As Long
        SQL_File = FreeFile
        
        Open "D:\Work\My Tools\Stored Procedure for FAF\Batch" + "\Stored_Rocedure.txt" For Append As SQL_File
            Print #SQL_File, Display
        Close SQL_File

    Next Owners
    
End Sub

Private Sub Family_ID_Click()
    Family_ID = ""
End Sub

Private Sub Form_Load()
    Operation_Code.AddItem "Create Family"
    Operation_Code.AddItem "Add Member"
    Operation_Code.AddItem "Delete Member"
    Operation_Code.AddItem "Delete Family"
    
    For i = 1 To Types.Count
        Types(i).AddItem "VFSubscriber"
        Types(i).AddItem "LandLine"
        Types(i).Text = Types(i).List(0)
   Next i
    
    Types(3).Text = Types(3).List(1)
    
    Operation_Code.Text = Operation_Code.List(0)
    
    
    
End Sub

Private Sub Generate_Click()
Select Case Operation_Code
    Case "Delete Family"
        Display = "set serveroutput on;" + vbCrLf + _
                "declare  eCode Varchar2(20); eDescription VARCHAR2(20);" + vbCrLf + _
                "begin" + vbCrLf + _
                "InsertFamilyRequest(" + "'" + Trim(Str(Val(Owner))) + "'" + ",''" + _
                Create_Members_XML(101) + _
                "''," + Trim(Str(opcode(Operation_Code))) + "," + Zero_Ext(Product_Name, 10) + "," + _
                Zero_Ext(Family_ID, 15) + ",eCode,eDescription);" + vbCrLf + _
                "dbms_output.put_line('eCode '|| eCode);" + vbCrLf + _
                "dbms_output.put_line('eDescription '|| eDescription);" + vbCrLf + _
                "end;" + vbCrLf + _
                "/" + vbCrLf

    Case "Create Family"
        Display = "set serveroutput on;" + vbCrLf + _
                "declare  eCode Varchar2(20); eDescription VARCHAR2(20);" + vbCrLf + _
                "begin" + vbCrLf + _
                "InsertFamilyRequest(" + "'" + Trim(Str(Val(Owner))) + "'" + ",''" + _
                Create_Members_XML(100) + _
                "''," + Trim(Str(opcode(Operation_Code))) + "," + Zero_Ext(Product_Name, 10) + "," + _
                Zero_Ext(Family_ID, 15) + ",eCode,eDescription);" + vbCrLf + _
                "dbms_output.put_line('eCode '|| eCode);" + vbCrLf + _
                "dbms_output.put_line('eDescription '|| eDescription);" + vbCrLf + _
                "end;" + vbCrLf + _
                "/" + vbCrLf
    End Select
End Sub



Private Function Zero_Ext(ByVal Character As String, ByVal Length As Integer)
    Dim Temp As String
    
    If Len(Character) < Length Then
        For i = Len(Character) To Length - 1
            Temp = Temp + "0"
        Next i
    End If
    Zero_Ext = "'" + Temp + Character + "'"
    If Character = "" Then Zero_Ext = "''"
End Function



Private Function opcode(ByVal Desc As String) As Integer
    Select Case Desc
        Case "Create Family"
            opcode = 100
        Case "Add Member"
            opcode = 102
        Case "Delete Member"
            opcode = 104
        Case "Delete Family"
            opcode = 101
    End Select
End Function


Private Function Create_Member(ByVal Member As String, ByVal type_ As String) As String
    If type_ = "VFSubscriber" Then Create_Member = "<members><memNum>" + Trim(Str(Val(Member))) + "</memNum><memType>" + type_ + "</memType></members>"
    If type_ = "Land Line" Then Create_Member = "<members><memNum>" + Trim(Member) + "</memNum><memType>" + type_ + "</memType></members>"
End Function
Private Function Create_Members_XML(opcode As Integer) As String
    Dim Members_XML As String
    
    Select Case opcode
        Case 100
            For i = 1 To 4
                If Member(i) <> "" Then
                    Members_XML = Members_XML + Create_Member(Member(i), Types(i))
                End If
            Next i
        Case 101
            Members_XML = Create_Member(Owner, "VFSubscriber")
            
    End Select
    
    Create_Members_XML = "'<input>" + Members_XML + "</input>'"
End Function

Private Sub Product_Name_Click()
    Product_Name = ""
End Sub
