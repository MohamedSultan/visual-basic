Attribute VB_Name = "Prepair"
Public Sub Prepaire(ByVal Full_Format As Boolean)
    
    'Dim Line_Count, Amount, Length, FileNum As Integer
    'Dim I_File, O_File, Reformated_Line, Temp, Scratch, PIN, File_Name, Card_Type, DD, MM, YYYY As String
        
    Dim I_File(), StrLine, Field, Headder, Reformatted_Line, Level_Up, MKT_Path, _
                            DD, YYYY, MM, Temp, Operation_Path, O_File As String
                            
    Dim Line_Count, Step As Integer
    Dim I_FileNum, O_MKT, O_Operation As Long
    
    Voucher.RUN_.Enabled = False
    Voucher.Label4.Visible = True
    Voucher.Current_File.Visible = True
        
        Temp = ""
        
        I_FileNum = FreeFile
        ReDim Preserve I_File(0)
        I_File(0) = Dir(Voucher.InputPath.Text + "\*")
        Do While I_File(Step) <> ""
            Step = Step + 1
            ReDim Preserve I_File(Step)
            I_File(Step) = Dir
        Loop
            
            Voucher.Overall.Max = Step
            
            Level_Up = Up_Level(Voucher.InputPath)
            MkDir (Level_Up + "MKT")
            MkDir (Level_Up + "MKT_Encrypted")
            MkDir (Level_Up + "Operation")
            MkDir (Level_Up + "Operation_Enrypted")
            
            MKT_Path = Level_Up + "MKT\"
            Operation_Path = Level_Up + "Operation\"
            
            Voucher.Percentage1 = "0%"
            Voucher.Percentage2 = "0%"
        
        For Files = 0 To UBound(I_File) - 1
            
            'Line_Count = 0
            'Count # of Lines
            Open Voucher.InputPath.Text + "\" + I_File(Files) For Input As I_FileNum
                   Line Input #I_FileNum, StrLine
            Close I_FileNum
  
            Headder = Str(Len(StrLine) / 44) + ",14,EGP"
            
            O_MKT = FreeFile
            O_Operation = FreeFile
            
            Temp = Right(I_File(Files), 12)
            DD = Mid(Temp, 1, 2)
            MM = Mid(Temp, 3, 2)
            YYYY = Mid(Temp, 5, 4)
            
            O_File = Left(I_File(Files), Len(I_File(Files)) - 12)
            Open Operation_Path + O_File + YYYY + MM + DD + ".txt" For Append As O_Operation
               Print #O_Operation, Trim(Headder)
            Close O_Operation
            
            With Voucher
                .File.Max = (Len(StrLine) / 44) + 1
                .Percentage1 = Str(Int(.File.Max / .File.Value)) + "%"
            End With
            
            For i = 0 To Len(StrLine) - 1 Step 44
                 Field = Mid(StrLine, i + 1, 43)
                 DoEvents: DoEvents:
                 
                 Open MKT_Path + O_File + DD + MM + YYYY + ".txt" For Append As O_MKT
                    Print #O_MKT, Field
                 Close O_MKT
                 
                 Reformatted_Line = Reformat(Field)
                 DoEvents: DoEvents:
                 
                 Open Operation_Path + O_File + YYYY + MM + DD + ".txt" For Append As O_Operation
                    Print #O_Operation, Reformatted_Line
                 Close O_Operation
                 
                 With Voucher
                    .Current_File = I_File(Files)
                    .File.Value = Voucher.File.Value + 1
                    .Percentage1 = Str(Int(.File.Max / .File.Value)) + "%"
                 End With
             Next i
             
        With Voucher
            Voucher.File.Value = 0
            Voucher.Overall.Value = Voucher.Overall.Value + 1
            Voucher.Percentage2 = Str(Int(.Overall.Max / .Overall.Value)) + "%"
        End With
        
        Next Files
    
    Voucher.RUN_.Enabled = True
    Voucher.Label4.Visible = False
    Voucher.Current_File.Visible = False
    Voucher.File.Value = Voucher.File.Max
    
    Call MsgBox("All Files Are Successfully Created and Reformatted", , "Success")
End Sub
