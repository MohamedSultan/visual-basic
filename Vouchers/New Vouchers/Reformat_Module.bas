Attribute VB_Name = "Reformat_Module"
Public Function Reformat(ByVal StrLine As String) As String
    'StrLine = Left(StrLine, Len(StrLine) - 1)
    
    Dim Scratch, PIN, Temp, Card_Type, DD, MM, YYYY As String
    Dim Amount As Integer
    
    Scratch = Left(StrLine, 14)
    PIN = Mid(StrLine, 15, 10)
    Temp = Mid(StrLine, 25, 9)
    Amount = Int(Val(Temp)) * 100
    Temp = Right(StrLine, 10)
    Card_Type = Left(Temp, 2)
    DD = Mid(Temp, 3, 2)
    MM = Mid(Temp, 5, 2)
    YYYY = Right(Temp, 4)
    
    Reformat = Scratch + "," + PIN + "," + Trim(Str(Amount)) + "," + Card_Type + "," + YYYY + MM + DD + ",,,,"
End Function
