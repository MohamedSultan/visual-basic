Attribute VB_Name = "Up__Level"
Public Function Up_Level(ByVal Path As String) As String
    Dim Char As String
    Dim Pos, i As Integer
    i = Len(Path)
    While i > 0
        Char = Mid(Path, i, 1)
        If Char = "\" Then
            Pos = i
            i = 0
        End If
        
        i = i - 1
    Wend
    
    Up_Level = Mid(Path, 1, Pos)
End Function
