Attribute VB_Name = "Module1"
Public Sub Reformat1(ByVal Full_Format As Boolean)
    Dim Line_Count, Amount, Length, FileNum As Integer
        Dim I_File, O_File, Reformated_Line, Temp, Scratch, PIN, File_Name, Card_Type, DD, MM, YYYY As String
        
        Line_Count = 0
        I_FileNum = 3
        O_FileNum = 2
        Temp = ""
        
        I_File = Dir(Voucher.InputPath.Text + "\*")
        
        Do While I_File <> ""
            
            Line_Count = 0
            'Count # of Lines
            Open Voucher.InputPath.Text + "\" + I_File For Input As I_FileNum
                Do Until EOF(I_FileNum)
                    Line Input #I_FileNum, StrLine
                    Line_Count = Line_Count + 1
                    DoEvents: DoEvents: DoEvents: DoEvents
                Loop
            Close I_FileNum
             
            'Create O/P File and Header
            Temp = Right(StrLine, 10)
            DD = Mid(Temp, 3, 2)
            MM = Mid(Temp, 5, 2)
            YYYY = Right(Temp, 4)
            O_File = Left(I_File, Len(I_File) - 12)
            Open Voucher.OutPath.Text + "\" + O_File + YYYY + MM + DD + ".txt" For Append As O_FileNum
            Print #O_FileNum, Right(Str(Line_Count), Len(Line_Count)) + ",14,EGP"
            DoEvents: DoEvents: DoEvents: DoEvents
            Close O_FileNum
                    
            Temp = ""
            Open Voucher.InputPath.Text + "\" + I_File For Input As I_FileNum
                Do Until EOF(I_FileNum)
                    Line Input #I_FileNum, StrLine
                    Scratch = Left(StrLine, 14)
                    PIN = Mid(StrLine, 15, 10)
                    Temp = Mid(StrLine, 25, 9)
                    Amount = Int(Val(Temp)) * 100
                    Temp = Right(StrLine, 10)
                    Card_Type = Left(Temp, 2)
                    DD = Mid(Temp, 3, 2)
                    MM = Mid(Temp, 5, 2)
                    YYYY = Right(Temp, 4)
                    Reformated_Line = Scratch + "," + PIN + "," + Right(Str(Amount), Len(Amount)) + "," + Card_Type + "," + YYYY + MM + DD + ",,,,"
                    
                    'File Reformatted
                    Open Voucher.OutPath.Text + "\" + O_File + YYYY + MM + DD + ".txt" For Append As O_FileNum
                    Print #O_FileNum, Reformated_Line
                    Close O_FileNum
                    DoEvents: DoEvents: DoEvents: DoEvents
                Loop
                
            Close I_FileNum
        
        
        I_File = Dir
        DoEvents: DoEvents: DoEvents: DoEvents
        Loop
        
        MsgBox "Done"
End Sub
