VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form Scheduler 
   Caption         =   "Sheduler"
   ClientHeight    =   3795
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   ScaleHeight     =   3795
   ScaleWidth      =   4785
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Seconds 
      Interval        =   1000
      Left            =   360
      Top             =   240
   End
   Begin VB.ListBox Display 
      Height          =   2010
      ItemData        =   "Starter.frx":0000
      Left            =   120
      List            =   "Starter.frx":0002
      TabIndex        =   5
      Top             =   1560
      Width           =   4575
   End
   Begin VB.CommandButton Minimize_all 
      Caption         =   "_"
      Height          =   255
      Left            =   4440
      TabIndex        =   3
      Top             =   0
      Width           =   255
   End
   Begin VB.Frame Frame5 
      Height          =   855
      Left            =   1320
      TabIndex        =   0
      Top             =   0
      Width           =   2055
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1695
      End
   End
   Begin MSForms.ToggleButton Pause 
      Height          =   375
      Left            =   1680
      TabIndex        =   4
      Top             =   960
      Width           =   1215
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "2143;661"
      Value           =   "0"
      Caption         =   "Pause"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "Scheduler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
    'If IsProcessRunning("Scheduler.exe") = True Then End
    If CountOfProcessRunning("Scheduler.exe") > 1 Then End
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Pause_Click()
    If Pause.Value = True Then
        Seconds.Enabled = False
        Pause.Caption = "Resume"
    Else
        Seconds.Enabled = True
        Pause.Caption = "Pause"
    End If
End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Me.Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Minimize_all_Click()
    Call Minimizing_Form(Me)
End Sub
Public Sub Minimizing_Form(Form_ As Form)
    'Call Cope_All("Minimized")
    Form_.WindowState = 1
    Minimized = True

    Dim I As Integer
    Dim s As String
    Dim nid As NOTIFYICONDATA

    s = "Excel And Apps Sheduler"
    nid = setNOTIFYICONDATA(Form_.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Form_.Icon, s)
    I = Shell_NotifyIconA(NIM_ADD, nid)

    Form_.WindowState = vbMinimized
    Form_.Visible = False
End Sub

Private Sub Seconds_Timer()
    Label3 = Str(Minute(Now)) + " :   " + Str(Second(Now))
    
    Debug.Print Second(Now)
    On Error GoTo ErrorHandler
    
    If Second(Now) = 0 Then
        Call Shell(App.Path + "\Schedular", vbNormalFocus)
    End If
ErrorHandler:
    If Err.Number = 0 Or Err.Number = 20 Then
        Resume Next
    Else
        If Err.Number = 53 Then
            FileCopy App.Path + "\Schedular_Bkp", App.Path + "\Schedular"
            Call Applications.Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " Err From Main:" + Err.Description + " ,File Retrieved from Backup")
        Else
            Call Applications.Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " Err From Main:" + Err.Description)
        End If
        Resume Next
    End If
End Sub
