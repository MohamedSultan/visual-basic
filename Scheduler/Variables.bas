Attribute VB_Name = "Module1"
Type Checks
    Minute As Boolean
    Hour As Boolean
    day_of_the_month As Boolean
    month_of_the_year As Boolean
    day_of_the_week As Boolean
End Type

Type Application_details
    ApplicationPath As String
    StartFrom As String
    Minute As String
    Hour As String
    day_of_the_month As String
    month_of_the_year As String
    day_of_the_week As String
    Type_Of_Application As String
End Type
