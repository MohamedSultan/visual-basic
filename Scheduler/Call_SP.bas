Attribute VB_Name = "Call_SP"
Enum DB_Types
    Oracle = 1
    MySQL = 2
End Enum
Function SendSMS2(From_ As String, All_Receipients As String, Body As String)
    Dim Receipients_Temp() As String
    Dim Receipients() As String
    'Dim Step As Integer
    
    Receipients_Temp = Split(All_Receipients, ";")
    Receipients = Receipients_Temp
    For i = 0 To UBound(Receipients)
        If Receipients(i) <> "" Then
            Receipients(i) = Trim(Receipients(i))
            For j = 1 To Len(Body) Step 160
                Call Execute_Stored_Procedure("SendSMS", "172.28.37.216", "VAS_NOT", "VAS_NOT_456", "SMPPRD1", "1521", "Oracle", "SD_KLMNA_I", From_, Receipients(i), Mid(Body, j, j + 159), "0", "2", "0")
                'Wait 1000
            Next j
        End If
    Next i
End Function
 Function Execute_Stored_Procedure(StoredProcedureName As String, _
                                SERVER_Name As String, _
                                User_Name As String, _
                                Password As String, _
                                DataBase As String, _
                                Port As String, _
                                DB_Type As DB_Types, _
                                Optional Param1 As String, _
                                Optional Param2 As String, _
                                Optional Param3 As String, _
                                Optional Param4 As String, _
                                Optional Param5 As String, _
                                Optional Param6 As String, _
                                Optional Param7 As String, _
                                Optional Param8 As String) As ADODB.Recordset

            Dim mobjConn As ADODB.Connection
            Dim mobjCmd As ADODB.Command
           'x = Len(Param4)
            Set mobjConn = New ADODB.Connection
            'mobjConn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=EN7131D;UID=SMS_User;PWD=SMS;DATABASE=sms;PORT=3306"
            'mobjConn.ConnectionString = "Driver=" + Driver + _
            '                            ";SERVER=" + SERVER_Name + _
            '                            ";UID=" + User_Name + _
            '                            ";PWD=" + Password + _
            '                            ";DATABASE=" + DataBase + _
            '                            ";PORT=" + Port
            '
            'mobjConn.ConnectionString = "Provider=MSDAORA.1 ; Data Source=SMS; Password=VAS_NOT_456; User ID=VAS_NOT ; Persist Security Info=True"
            Select Case DB_Type
                Case Oracle
                    mobjConn.ConnectionString = "Provider=MSDAORA.1 ; Data Source=SMS; Password=VAS_NOT_456; User ID=VAS_NOT ; Persist Security Info=True"
                Case MySQL
                    mobjConn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=" + SERVER_Name + ";DATABASE=" + DataBase + ";UID=" + User_Name + ";PWD=" + Password + ";PORT=" + Port
            End Select
            'mobjConn.ConnectionString = "Provider=MSDAORA.1 ;DRIVER={Oracle in OraClient10g_home1};SERVER=SMS;Data Source=SMS;UID=VAS_NOT;PWD=VAS_NOT_456;DBQ=SMS;DBA=W;APA=T;EXC=F;XSM=Default;FEN=T;QTO=T;FRC=10;FDL=10;LOB=T;RST=T;GDE=F;FRL=Lo;BAM=IfAllSuccessful;NUM=NLS;DPM=F;MTS=T;MDI=Me;CSR=F;FWC=F;FBS=60000;TLO=O;"
            'mobjConn.ConnectionString = "Provider=MSDAORA.1 ;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + SERVER_Name + ")(PORT=" + Port + ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + DataBase + ")));User Id=" + User_Name + ";Password=" + Password + ";"
            
            mobjConn.Open
            Set mobjCmd = New ADODB.Command
            Set mobjCmd.ActiveConnection = mobjConn
            
            If Param1 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param1", adVarChar, adParamInput, Len(Param1), Param1)
            If Param2 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param2", adVarChar, adParamInput, Len(Param2), Param2)
            If Param3 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param3", adVarChar, adParamInput, Len(Param3), Param3)
            If Param4 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param4", adVarChar, adParamInput, Len(Param4), Param4)
            If Param5 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param5", adVarChar, adParamInput, Len(Param5), Param5)
            If Param6 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param6", adVarChar, adParamInput, Len(Param6), Param6)
            If Param7 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param7", adVarChar, adParamInput, Len(Param7), Param7)
            If Param8 <> "" Then mobjCmd.Parameters.Append mobjCmd.CreateParameter("Param8", adVarChar, adParamInput, Len(Param8), Param8)
            
            mobjCmd.CommandType = adCmdStoredProc
            mobjCmd.CommandText = StoredProcedureName
            'If Result.State = 1 Then Result.Close
            Set Result = mobjCmd.Execute
            Set Execute_Stored_Procedure = Result
 End Function

