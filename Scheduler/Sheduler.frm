VERSION 5.00
Begin VB.Form Applications 
   Caption         =   "Sheduler"
   ClientHeight    =   3795
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   ScaleHeight     =   3795
   ScaleWidth      =   4785
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox Display 
      Height          =   3375
      ItemData        =   "Sheduler.frx":0000
      Left            =   120
      List            =   "Sheduler.frx":0002
      TabIndex        =   1
      Top             =   360
      Width           =   4575
   End
   Begin VB.CommandButton Minimize_all 
      Caption         =   "_"
      Height          =   255
      Left            =   4440
      TabIndex        =   0
      Top             =   0
      Width           =   255
   End
End
Attribute VB_Name = "Applications"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
    Dim Application() As Application_details

Private Function Checker(Application_ As Application_details) As Boolean
    Dim App_Checks As Checks
    With Application_
        If .Minute = "*" Or Val(.Minute) = Minute(Now) Then App_Checks.Minute = True
        If .Hour = "*" Or Val(.Hour) = Hour(Now) Then App_Checks.Hour = True
        If .day_of_the_month = "*" Or Val(.day_of_the_month) = Day(Now) Then App_Checks.day_of_the_month = True
        If .day_of_the_week = "*" Or Val(Left(.day_of_the_week, 1)) = Weekday(Now) Then App_Checks.day_of_the_week = True
        If .month_of_the_year = "*" Or Val(.month_of_the_year) = Month(Now) Then App_Checks.month_of_the_year = True
    End With
    
    With App_Checks
        Checker = .day_of_the_month And .day_of_the_week And .Hour And .Minute And .month_of_the_year
    End With
End Function
Private Sub DB_ReadConfigurations(File_Path As String)
    Dim AppExcel As Excel.Application
    Set AppExcel = New Excel.Application
    AppExcel.Workbooks.Open File_Path
    
    Step = 3
    Dim NewStep As Integer
    NewStep = 0
    While AppExcel.Sheets(1).Cells(Step, 1) <> ""
        
        
        ReDim Preserve Application(NewStep)
        
        With Application(NewStep)
            .ApplicationPath = AppExcel.ActiveSheet.Cells(Step, 1)
            .StartFrom = AppExcel.ActiveSheet.Cells(Step, 2)
            .Minute = AppExcel.ActiveSheet.Cells(Step, 3)
            .Hour = AppExcel.ActiveSheet.Cells(Step, 4)
            .day_of_the_month = AppExcel.ActiveSheet.Cells(Step, 5)
            .month_of_the_year = AppExcel.ActiveSheet.Cells(Step, 6)
            .day_of_the_week = AppExcel.ActiveSheet.Cells(Step, 7)
            .Type_Of_Application = AppExcel.ActiveSheet.Cells(Step, 8)
        End With
        
        NewStep = NewStep + 1
        Step = Step + 1
    Wend
    
    AppExcel.Workbooks.Close

End Sub

Public Sub ReadNewConfigurations()
    Dim RS As ADODB.Recordset
    Set RS = Execute_Stored_Procedure("Schedular", "ActiveDB", "Reader", "Reader", "group_stats", "3306", MySQL)
    
    Step = 3
    Dim NewStep As Integer
    NewStep = 0
    While Not (RS.EOF)
        ReDim Preserve Application(NewStep)
        On Error GoTo localHandler
        With Application(NewStep)
            .ApplicationPath = RS.Fields("file").Value
            .StartFrom = RS.Fields("LaunchDate").Value
            .Minute = RS.Fields("Min").Value
            .Hour = RS.Fields("Hours").Value
            .day_of_the_month = RS.Fields("DOM").Value
            .month_of_the_year = RS.Fields("DOY").Value
            .day_of_the_week = RS.Fields("DOW").Value
            .Type_Of_Application = RS.Fields("TOA").Value
        End With
        
        NewStep = NewStep + 1
        RS.MoveNext
    Wend
    
    
localHandler:
    If Err.Number = 3265 Or Err.Number = 0 Or Err.Number = 20 Then
        Resume Next
    Else
        MsgBox Err.Description, , "Schedular Handler Err Number:" + Str(Err.Number)
    End If
    
End Sub
Private Sub ReadConfigurations(File_Path As String)
    Dim AppExcel As Excel.Application
    Set AppExcel = New Excel.Application
    AppExcel.Workbooks.Open File_Path
    
    Step = 3
    Dim NewStep As Integer
    NewStep = 0
    While AppExcel.Sheets(1).Cells(Step, 1) <> ""
        
        
        ReDim Preserve Application(NewStep)
        
        With Application(NewStep)
            .ApplicationPath = AppExcel.ActiveSheet.Cells(Step, 1)
            .StartFrom = AppExcel.ActiveSheet.Cells(Step, 2)
            .Minute = AppExcel.ActiveSheet.Cells(Step, 3)
            .Hour = AppExcel.ActiveSheet.Cells(Step, 4)
            .day_of_the_month = AppExcel.ActiveSheet.Cells(Step, 5)
            .month_of_the_year = AppExcel.ActiveSheet.Cells(Step, 6)
            .day_of_the_week = AppExcel.ActiveSheet.Cells(Step, 7)
            .Type_Of_Application = AppExcel.ActiveSheet.Cells(Step, 8)
        End With
        
        NewStep = NewStep + 1
        Step = Step + 1
    Wend
    
    AppExcel.Workbooks.Close

End Sub
Private Sub RunApplications()
    Dim NewStep As Integer
    On Error Resume Next
    For I = 0 To UBound(Application)
    
        If Checker(Application(I)) Then
            Call Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " Running Application:" + Application(I).ApplicationPath)
            Select Case UCase(Application(I).Type_Of_Application)
                Case UCase("Excel")
                    Dim ExcelApp As Excel.Application
                    Set ExcelApp = New Excel.Application
                    Set ExcelApp2 = ExcelApp.Workbooks.Open(Application(I).ApplicationPath)
                    ExcelApp.Visible = True
                    
                    Call Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " Run:" + Application(I).ApplicationPath)
                    Call DB_Logger(Application(I).ApplicationPath)
                    
                Case UCase("Exe")
                    If LCase(Right(Application(I).ApplicationPath, 3)) <> "exe" Then
                        Call Shell(Application(I).ApplicationPath + ".exe", vbNormalFocus)
                    Else
                        Call Shell(Application(I).ApplicationPath, vbNormalFocus)
                    End If
                    
                    Call Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " " + Application(I).ApplicationPath)
                    
                Case UCase("Bat")
                    If LCase(Right(Application(I).ApplicationPath, 3)) <> "bat" Then
                        Call Shell(Application(I).ApplicationPath + ".bat", vbNormalFocus)
                    Else
                        Call Shell(Application(I).ApplicationPath, vbNormalFocus)
                    End If
                    
                    Call Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " " + Application(I).ApplicationPath)
                Case UCase("Com")
                    If LCase(Right(Application(I).ApplicationPath, 3)) <> "com" Then
                        Call Shell(Application(I).ApplicationPath + ".com", vbNormalFocus)
                    Else
                        Call Shell(Application(I).ApplicationPath, vbNormalFocus)
                    End If
                    
                    Call Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " " + Application(I).ApplicationPath)
            End Select
        End If
    
    Next I
localHandler:
    If Err.Number = 0 Or Err.Number = 20 Then
        Resume Next
    Else
        Call Logger(Format(Date, "YYYY-MM-DD") + " " + Format(Time, "HH:MM") + " Err:" + Err.Description)
    End If
End Sub
Public Sub Logger(NewData As String)
    Dim FileName As Double
    FileName = FreeFile
    Open App.Path + "\Log.txt" For Append As FileName
        Print #FileName, NewData
    Close FileName
End Sub
Public Sub DB_Logger(DB_Name As String)
    Dim Conn As ADODB.Connection
    Dim RS As ADODB.Recordset
    Set Conn = New ADODB.Connection
    Set RS = New ADODB.Recordset
    
    Dim GuardTime_Add, GuardTime_Substract As Integer
    GuardTime_Substract = 15
    GuardTime_Add = 30

    mobjConn.ConnectionString = "Driver=MySQL ODBC 5.1 Driver;SERVER=ActiveDB;DATABASE=group_stats;UID=Writer;PWD=Writer;PORT=3306"
    mobjConn.Open
    
    Dim ServiceName As String
    RS.Open "SELECT `Service Name` ServiceName from Services" + _
            "Where substring_index(substring_index('D:\\Tools:Applications\\Hulk\\Hulk.xlsm','\\',3),'\\',-1)=DB_Name" _
            , Conn
    ServiceName = RS.Fields("ServiceName")
    
    RS.Open "Select Count(*) Count from Schedular_Log " + _
                "WHERE ServiceName='" + ServiceName + "' and " + _
                "'" + Format(Now(), "HH:MM:SS") + "' BETWEEN addtime(CONVERT(concat(`Hours`, ':', `min`), TIME), '-0:" + Trim(Str(GuardTime_Substract)) + ":00') and addtime(CONVERT(concat(`Hours`, ':', `min`), TIME), '0:" + Trim(Str(GuardTime_Add)) + ":00')" _
                , Conn
    Dim Count As Integer
    Count = RS.Fields("Count")

    If Count > 0 Then

        RS.Open "update Schedular_Log " + _
                "SET `Schedular Status`='Service Is Run' " + _
                "WHERE ServiceName='" + ServiceName + "' and " + _
                "'" + Format(Now(), "HH:MM:SS") + "' BETWEEN addtime(CONVERT(concat(`Hours`, ':', `min`), TIME), '-0:" + Trim(Str(GuardTime_Substract)) + ":00') and addtime(CONVERT(concat(`Hours`, ':', `min`), TIME), '0:" + Trim(Str(GuardTime_Add)) + ":00')" _
                , Conn
    End If

    RS.Close
    Conn.Close
    
End Sub

Public Sub Form_Load()
    'Call ReadConfigurations(App.Path + "\Configurations.xlsx")
    Call ReadNewConfigurations
    Call RunApplications
    End
End Sub
