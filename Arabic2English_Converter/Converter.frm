VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Converter_Form 
   Caption         =   "Rename"
   ClientHeight    =   2160
   ClientLeft      =   5340
   ClientTop       =   5220
   ClientWidth     =   8130
   LinkTopic       =   "Form1"
   ScaleHeight     =   2160
   ScaleWidth      =   8130
   Begin MSComDlg.CommonDialog SetFolderPath 
      Left            =   7080
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Remove_Prefix 
      Caption         =   "Change al"
      Enabled         =   0   'False
      Height          =   495
      Left            =   1560
      TabIndex        =   6
      Top             =   1200
      Width           =   1695
   End
   Begin VB.CommandButton Remove_Spaces 
      Caption         =   "Remove_Spaces"
      Enabled         =   0   'False
      Height          =   495
      Left            =   4920
      TabIndex        =   5
      Top             =   1200
      Width           =   1575
   End
   Begin VB.TextBox Prefix 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   720
      Width           =   5055
   End
   Begin VB.TextBox Directory 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Text            =   "D:\Quran\Bishary El3afasy"
      Top             =   360
      Width           =   5055
   End
   Begin VB.CommandButton Rename 
      Caption         =   "Convert"
      Enabled         =   0   'False
      Height          =   495
      Left            =   3360
      TabIndex        =   0
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Prefix"
      Height          =   255
      Left            =   600
      TabIndex        =   4
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Directory"
      Height          =   255
      Left            =   600
      TabIndex        =   3
      Top             =   360
      Width           =   1095
   End
End
Attribute VB_Name = "Converter_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Check()
    If Directory <> "" Then
        Rename.Enabled = True
        Remove_Prefix.Enabled = True
        Remove_Spaces.Enabled = True
        Else
        Rename.Enabled = False
        Remove_Prefix.Enabled = False
        Remove_Spaces.Enabled = False
    End If
End Sub
Private Sub Directory_Change()
    Check
End Sub

Private Sub Prefix_Change()
    Check
End Sub

Private Sub Remove_Prefix_Click()
    Dim File_Name(), FileName_Temp As String
    Dim Step As Double
    
    If Right(Directory, 1) <> "\" Then Directory = Directory + "\"
    
    ReDim Preserve File_Name(Step)
    FileName_Temp = Dir(Directory)
    File_Name(0) = FileName_Temp
    
    While FileName_Temp <> ""
        FileName_Temp = Dir
        Step = Step + 1
        ReDim Preserve File_Name(Step)
        File_Name(Step) = FileName_Temp
    Wend
    
    Dim NewName As String
    
    'For i = 194 To 300
    '   Z = Chr(i)
    'Next i

    
    For i = 0 To UBound(File_Name)
        If File_Name(i) <> "" Then
            NewName = ""
            For Chars = 1 To Len(File_Name(i))
                If Mid(File_Name(i), Chars, 3) = "al-" Then _
                    NewName = Mid(File_Name(i), 1, Chars - 1) + "Al-" + Mid(File_Name(i), Chars + 3, Len(File_Name(i)))
            Next Chars
                If NewName <> "" Then Name Directory + File_Name(i) As Directory + NewName
        End If
    Next i
    
     MsgBox "                    Done", , "Batch Rename Finished"
     
     
End Sub

Private Sub Remove_Spaces_Click()
    Dim File_Name As String
    Dim Dot_Location(), Step As Integer
    ReDim Preserve Dot_Location(0)
    Dot_Location(0) = 1
    Step = 1
    If Right(Directory, 1) <> "\" Then Directory = Directory + "\"
    
    File_Name = Dir(Directory)
    'MaxArray
    While File_Name <> ""
        ReDim Preserve Dot_Location(0)
        Dot_Location(0) = 1
        Step = 1
        While InStr(Dot_Location(Step - 1) + 1, File_Name, ".") <> 0
            ReDim Preserve Dot_Location(Step)
            Dot_Location(Step) = InStr(Dot_Location(Step - 1) + 1, File_Name, ".")
            Step = Step + 1
        Wend
        'X = MaxArray(Dot_Location)
        Name Directory.Text + File_Name As Directory.Text + Trim(Mid(File_Name, 1, MaxArray(Dot_Location) - 1)) + Mid(File_Name, MaxArray(Dot_Location), Len(File_Name) - MaxArray(Dot_Location) + 1)
        File_Name = Dir
    Wend
    
     MsgBox "                    Done", , "Batch Rename Finished"
     
    
End Sub
Private Function MaxArray(ByRef Array_ As Variant) As Integer
    Dim Max As Integer
    Max = Array_(0)
    For i = 1 To UBound(Array_)
        If Array_(i) > Array_(i - 1) Then Max = Array_(i)
    Next i
    MaxArray = Max
End Function
Private Function Converter(Ar_Char As String) As String
    If Asc(Ar_Char) >= 0 And Asc(Ar_Char) <= 122 Then
        Converter = Ar_Char
    Else
        Select Case Asc(Ar_Char)
            Case 194        '�
                Converter = "a"
            Case 195        '�
                Converter = "a"
            Case 196        '�
                Converter = "oa"
            Case 197        '�
                Converter = "e"
            Case 198        '�
                Converter = "ia"
            Case 199        '�
                Converter = "a"
            Case 200        '�
                Converter = "b"
            Case 201        '�
                Converter = "aa"
            Case 202        '�
                Converter = "t"
            Case 203        '�
                Converter = "th"
            Case 204        '�
                Converter = "g"
            Case 205        '�
                Converter = "h"
            Case 206        '�
               Converter = "gh"
            Case 207        '�
                Converter = "d"
            Case 208        '�
                Converter = "z"
            Case 209        '�
                Converter = "r"
            Case 210        '�
                Converter = "z"
            Case 211        '�
                Converter = "s"
            Case 212        '�
                Converter = "sh"
            Case 213        '�
                Converter = "s"
            Case 214        '�
                Converter = "d"
            Case 215        'x
                Converter = Ar_Char
            Case 216        '�
                Converter = "t"
            Case 217        '�
                Converter = "z"
            Case 218        '�
                Converter = "a"
            Case 219        '�
                Converter = "gh"
            Case 220        '�
                Converter = Ar_Char
            Case 221        '�
                Converter = "f"
            Case 222        '�
                Converter = "k"
            Case 223        '�
                Converter = "k"
            Case 224        '�
                Converter = Ar_Char
            Case 225        '�
                Converter = "l"
            Case 226        '�
            Case 227        '�
                Converter = "m"
            Case 228        '�
                Converter = "n"
            Case 229        '�
                Converter = "h"
            Case 230        '�
                Converter = "w"
            Case 231        '�
                Converter = Ar_Char
            Case 236        '�
                Converter = "y"
            Case 237        '�
                Converter = "y"
            Case 240        'hamza
                Converter = Ar_Char
        End Select
    End If
End Function
Private Sub Rename_Click()
    Dim File_Name(), FileName_Temp As String
    Dim Step As Double
    
    If Right(Directory, 1) <> "\" Then Directory = Directory + "\"
    
    ReDim Preserve File_Name(Step)
    FileName_Temp = Dir(Directory)
    File_Name(0) = FileName_Temp
    
    While FileName_Temp <> ""
        FileName_Temp = Dir
        Step = Step + 1
        ReDim Preserve File_Name(Step)
        File_Name(Step) = FileName_Temp
    Wend
    
    Dim NewName As String
    
    'For i = 194 To 300
    '   Z = Chr(i)
    'Next i

    
    For i = 0 To UBound(File_Name)
        If File_Name(i) <> "" Then
            NewName = ""
            For Chars = 1 To Len(File_Name(i))
                X = Asc(Mid(File_Name(i), Chars, 1))
                Y = Mid(File_Name(i), Chars, 1)
                NewName = NewName + Converter(Mid(File_Name(i), Chars, 1))
            Next Chars
                Name Directory + File_Name(i) As Directory + NewName
        End If
    Next i
    
     MsgBox "                    Done", , "Batch Rename Finished"
     
    
End Sub
