VERSION 5.00
Begin VB.Form Form2 
   Caption         =   "Form2"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form2"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   855
      Left            =   1080
      TabIndex        =   0
      Top             =   720
      Width           =   1455
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim s As String

s = "Chilkat Software"

' The following 4 lines are case-sensitive
Print InStr(s, "Software")   ' Prints 9
Print InStr(s, "Chilkat")    ' Prints 1
Print InStr(s, "xyz")        ' Prints 0
Print InStr(s, "software")   ' Prints 0

' The following 3 lines are case-insensitive
Print InStr(1, s, "software", vbTextCompare)  ' Prints 9
Print InStr(1, s, "chilkat", vbTextCompare)    ' Prints 1
Print InStr(1, s, "xyz", vbTextCompare)        ' Prints 0

End Sub
