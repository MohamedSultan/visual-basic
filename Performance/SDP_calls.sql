--SDPs
select to_char(start_date_time,'YYYY-MM-DD HH'),
sum(ORG_ATMP),
sum(ORG_SUCC),
sum(ORG_UNSUCC_4CASES),
sum(ORG_UNSUCC_CONG)
from DC_SDP_CALLS
where to_char(start_date_time,'YYYY-MM-DD')= '2009-04-27'
group by to_char(start_date_time,'YYYY-MM-DD HH')
