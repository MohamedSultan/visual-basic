/* Formatted on 2009/10/21 17:01 (Formatter Plus v4.8.8) */
SELECT   TO_CHAR (date_time, 'YYYY-MM-DD') date_,
         NVL (admin_action, 'Disconnected') admin_action,
         NVL (action_data, 'Disconnected') action_data, SUM (count_all) COUNT
    FROM gdc_call_collect_srv
   WHERE TO_CHAR (date_time, 'YYYY-MM-DD') = '2009-10-20'
GROUP BY TO_CHAR (date_time, 'YYYY-MM-DD'),
         cause_value,
         admin_action,
         action_data