VERSION 5.00
Begin VB.Form Main_Form_TITO 
   Caption         =   "Family Stats Collector"
   ClientHeight    =   1320
   ClientLeft      =   13020
   ClientTop       =   1365
   ClientWidth     =   5610
   LinkTopic       =   "Form1"
   ScaleHeight     =   1320
   ScaleWidth      =   5610
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "Main_Form_TITO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub Auto_Stats_Click()
    Auto_Stats.Enabled = False
    Auto_Stats.Caption = Format(Date - Val(Days), "dd/mm/yy")
    Call GetALL_TITO_Stats(Date - Val(Days))
    Auto_Stats.Enabled = True
    Auto_Stats.Caption = "All"
End Sub

Private Sub Form_Load()
    'Call Me.Auto_Stats_Click
    'Me.Visible = False
    'End
End Sub
Public Sub GetALL_TITO_Stats(Optional DataDate As Date)
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Dim FAF_Stats As FAF_Stats_Type
    FAF_Stats = Get_FAF_Transactions(DataDate)
    Call StoreFAF(FAF_Stats, DataDate)
End Sub
    Dim Conn As ADODB.Connection
    Call DB_Open(Conn, VNPP)
    Dim Result As ADODB.Recordset
    Dim SDP_Stats As SDP_Stats_Type
    Dim step As Integer
    
    step = 0
    If Year(DataDate) = 1899 Then DataDate = Date
    Dim NumberOfDays As Integer
    NumberOfDays = DateDiff("d", DataDate, Date)
     
    Dim Quary As String
    
    Quary = "select dsc.sdp_name,to_char(dsc.start_date_time,'yyyy-mm-dd-hh24') When,sum(dsc.org_atmp) BHCA,tbl.REJECTED_CALLS,tbl.REJECTION_EVENTS " + _
            "from DC_SDP_CALLS dsc , (select node,to_char(date_time,'yyyy-mm-dd hh24') dte,REJECTED_CALLS,REJECTION_EVENTS " + _
                "From DC_SDP_LOADREGULATION " + _
                "where to_char(date_time,'yyyy-mm-dd') = to_char(sysdate-" + Str(NumberOfDays) + ",'yyyy-mm-dd') and rejected_calls<>0) tbl " + _
            "where to_char(dsc.start_date_time,'yyyy-mm-dd hh24')= tbl.dte  AND dsc.sdp_name=tbl.node " + _
            "group by dsc.sdp_name,dsc.start_date_time,tbl.REJECTED_CALLS,tbl.REJECTION_EVENTS " + _
            "order by dsc.sdp_name,dsc.start_date_time desc"

    Set Result = New ADODB.Recordset
    Result.Open Quary, Conn

    On Error GoTo LocalHandler
    While Not Result.EOF
        SDP_Stats.RejectionFound = True
        ReDim Preserve SDP_Stats.SDP_Rejections(step)
        With SDP_Stats.SDP_Rejections(step)
            .BHCA = Result.Fields("BHCA").Value
            .RejectedCalls = Result.Fields("REJECTED_CALLS").Value
            .RejectedEvents = Result.Fields("REJECTION_EVENTS").Value
            .SDPName = Result.Fields("sdp_name").Value
            .DateTime = Result.Fields("When").Value
        End With
        Result.MoveNext
        step = step + 1
    Wend
    Result.Close
    Conn.Close
    Get_SDP_Transactions = SDP_Stats
LocalHandler:
    If Err.Number = 94 Then Resume Next
    '"Invalid use of Null"
End Function


