select *
from TRANSACTIONSVIEW
order by TRANSACTION_DATE desc;

--Responses VS Trasaction Type By Date
select Type_Description,Status,response_Code,error_reason,response_description,count(*) 
from TRANSACTIONSVIEW
where to_char(transaction_date,'YYYY-MM-DD')=to_char(sysdate,'YYYY-MM-DD')
group by Type_Description,Status,response_Code,error_reason,response_description;

--Responses VS Trasaction Type By Date per client
select Type_Description,Status,response_Code,error_reason,response_description,Client_ID,count(*) 
from TRANSACTIONSVIEW
where to_char(transaction_date,'YYYY-MM-DD')=to_char(sysdate,'YYYY-MM-DD')
group by Type_Description,Status,response_Code,error_reason,response_description,Client_ID;

--Responses VS Failure reason
select Type_Description,Status,response_Code,error_reason,response_description,count(*) 
from TRANSACTIONSVIEW
where to_char(transaction_date,'YYYY-MM-DD')=to_char(sysdate,'YYYY-MM-DD')
group by Type_Description,Status,response_Code,error_reason,response_description;

--Responses VS Trasaction Type By Daily
select to_char(transaction_date,'YYYY-MM-DD'),Type_Description,Status,response_Code,error_reason,response_description,count(*) 
from TRANSACTIONSVIEW
--where to_char(transaction_date,'YYYY-MM-DD')=to_char(sysdate,'YYYY-MM-DD')
group by to_char(transaction_date,'YYYY-MM-DD'),Type_Description,Status,response_Code,error_reason,response_description;

--Responses VS Failure reason
select to_char(transaction_date,'YYYY-MM-DD'),Type_Description,Status,count(*) 
from TRANSACTIONSVIEW
group by to_char(transaction_date,'YYYY-MM-DD'),Type_Description,Status
order by to_char(transaction_date,'YYYY-MM-DD'),Type_Description;
