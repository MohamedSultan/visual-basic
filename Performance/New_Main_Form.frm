VERSION 5.00
Begin VB.Form Main_Form_FTD 
   Caption         =   "Background SMS Stats Collector"
   ClientHeight    =   8130
   ClientLeft      =   13200
   ClientTop       =   1725
   ClientWidth     =   5640
   Icon            =   "New_Main_Form.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8130
   ScaleWidth      =   5640
   Begin VB.ListBox Temp_List 
      Height          =   255
      Left            =   4320
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   120
      TabIndex        =   5
      Top             =   4200
      Width           =   5415
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   7
         Top             =   240
         Width           =   1455
      End
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   6
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Stats Type"
      Height          =   4095
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   5415
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Devices"
         Height          =   375
         Index           =   13
         Left            =   2520
         TabIndex        =   9
         Top             =   240
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Inbound Roamers"
         Height          =   375
         Index           =   18
         Left            =   2520
         TabIndex        =   27
         Top             =   3480
         Width           =   2415
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MTC Switches"
         Height          =   375
         Index           =   17
         Left            =   120
         TabIndex        =   26
         Top             =   3120
         Width           =   2175
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MOC Switches"
         Height          =   375
         Index           =   14
         Left            =   120
         TabIndex        =   25
         Top             =   2760
         Width           =   2175
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_ET_MT_VF"
         Height          =   375
         Index           =   16
         Left            =   2520
         TabIndex        =   24
         Top             =   3120
         Width           =   2415
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_MN_MT_VF"
         Height          =   375
         Index           =   15
         Left            =   2520
         TabIndex        =   23
         Top             =   2760
         Width           =   2415
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_Applications"
         Enabled         =   0   'False
         Height          =   375
         Index           =   6
         Left            =   120
         TabIndex        =   22
         Top             =   2400
         Width           =   2175
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_Int"
         Height          =   375
         Index           =   5
         Left            =   120
         TabIndex        =   21
         Top             =   2040
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_ET"
         Enabled         =   0   'False
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   20
         Top             =   1680
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MT_MN"
         Enabled         =   0   'False
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   19
         Top             =   1320
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_Roaming"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   18
         Top             =   960
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_Local"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "Hourly SMS"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "AO"
         Height          =   375
         Index           =   7
         Left            =   2520
         TabIndex        =   15
         Top             =   2400
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MT_MN"
         Height          =   375
         Index           =   8
         Left            =   2520
         TabIndex        =   14
         Top             =   2040
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MT_ET"
         Height          =   375
         Index           =   9
         Left            =   2520
         TabIndex        =   13
         Top             =   1680
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_Int_MT_VF"
         Height          =   375
         Index           =   10
         Left            =   2520
         TabIndex        =   12
         Top             =   1320
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO_VF_MO_VF"
         Height          =   375
         Index           =   11
         Left            =   2520
         TabIndex        =   11
         Top             =   960
         Width           =   2055
      End
      Begin VB.OptionButton Stats_Option 
         Caption         =   "MO(MN,ET,Int) MT_VF"
         Height          =   375
         Index           =   12
         Left            =   2520
         TabIndex        =   10
         Top             =   600
         Width           =   2415
      End
      Begin VB.CommandButton OK 
         Caption         =   "OK"
         Height          =   375
         Left            =   480
         TabIndex        =   4
         Top             =   3600
         Width           =   1215
      End
   End
   Begin VB.CommandButton Minimizing 
      Caption         =   "_"
      Height          =   255
      Left            =   5400
      TabIndex        =   2
      Top             =   0
      Width           =   255
   End
   Begin VB.ListBox Status 
      Height          =   2595
      Left            =   120
      TabIndex        =   0
      Top             =   5400
      Width           =   5415
   End
End
Attribute VB_Name = "Main_Form_FTD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim GetDataInterval, CurrentSeconds As Integer

Private Sub Form_Load()
    'CurrentSeconds = 60
    'Call Auto_Stats_Click
    'End
End Sub

'Private Sub minutes_Timer()
'    GetDataInterval = GetDataInterval + 1
'    If GetDataInterval = 20 Then
'        Days = ""
'        Call Auto_Stats_Click
'        GetDataInterval = 0
'    End If
'End Sub
'
'Private Sub Seconds_Timer()
'    CurrentSeconds = CurrentSeconds - 1
'    If CurrentSeconds = -1 Then CurrentSeconds = 59
'    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
'End Sub

Private Function DISPLAY_Data(Date_ As Date) As Double()
    Dim Conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim TempValue As Double
    Dim SMS_Array(23) As Double
   
   'updating Local Data Base
    Call DB_Open(Conn, "XXX")
    
    Set SMS_Record = New ADODB.Recordset
    SMS_Record.Open "select * from HourlySMS_Success where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", Conn
    
    If Not SMS_Record.EOF Then
        For i = 0 To 23
            NewI = Trim(Str(i))
            If i < 10 Then NewI = "0" & Trim(Str(i))
            SMS_Array(i) = Val(SMS_Record.Fields("SMS" + Trim(NewI)).Value)
        Next i
    End If
    DISPLAY_Data = SMS_Array
End Function
Private Sub SystemChecker()
    Dim NumberOfDevices As Integer
    Dim Conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    
    Call DB_Open(Conn, TextStat)
    Set SMS_Record = New ADODB.Recordset
    
    SMS_Record.Open GetSelectStatement(Device_Checker, Date), Conn
    
    If Not SMS_Record.EOF Then NumberOfDevices = Val(SMS_Record.Fields("count(distinct(device))"))
    If NumberOfDevices <> 6 Then Shell ("D:\Work\My Tools\Performance\MSGBox\Invalid Number of Devices.exe")
End Sub

Public Sub Get_N_Store_Stats(statsType As Statistic_Types, Optional DateOfData As Date)
    Dim SMS As Op_SMS_Type
    SMS = GetMySQL_Stats(statsType, DateOfData)
    Call StoreData_MDB(statsType, SMS, DateOfData)
End Sub
Private Sub All_Stats(DataDate As Date)
    Dim i As Integer
'    Call Minimizing_Click
    For i = 0 To Stats_Option.UBound
        Select Case i
            Case 0     'Hourly
                Status.AddItem Str(Now) + " Getting Stats of Hourly SMS"
                Call HourlyStats_Enhanced(DataDate)
                Status.AddItem Str(Now) + " Received Stats of Hourly SMS"
            Case 3, 4, 6, 11
                'do nothing
            Case 14     'Switch MOC
                Status.AddItem Str(Now) + " Getting Stats of Switches MOC"
                Call Switches_Stats(MOC, DataDate)
                Status.AddItem Str(Now) + " Received Stats of Switches MOC"
            Case 17     'Switch MTC
                Status.AddItem Str(Now) + " Getting Stats of Switches MTC"
                Call Switches_Stats(MTC, DataDate)
                Status.AddItem Str(Now) + " Received Stats of Switches MTC"
            Case Else
                Status.AddItem Str(Now) + " Getting Stats of " + StatsName(StatsMap(i))
                Call Get_N_Store_Stats(StatsMap(i), DataDate)
                Status.AddItem Str(Now) + " Received Stats of " + StatsName(StatsMap(i))
        End Select
    Next i
    
    'If DataDate <> Date Then
    '    MsgBox "Data of Date: " + Format(Date - Val(Days), "dd/mm/yy") + " Is successfully Received"
    '    End
    'End If
End Sub

Public Sub Auto_Stats_Click()
    Auto_Stats.Enabled = False
    Call SystemChecker
    Call All_Stats(Date - Val(Days))
    Auto_Stats.Enabled = True
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Me.Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub
Private Sub Minimizing_Click()
            'Call Cope_All("Minimized")
            Me.WindowState = 1
            Minimized = True

            Dim i As Integer
            Dim s As String
            Dim nid As NOTIFYICONDATA

            s = "SMS Stats Collector"
            nid = setNOTIFYICONDATA(Me.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Me.Icon, s)
            i = Shell_NotifyIconA(NIM_ADD, nid)

            WindowState = vbMinimized
            Visible = False
End Sub

Private Sub Networks_Command_Click()
    Dim VF_All As Op_SMS_Type
    Dim MN_VF As Op_SMS_Type
    Dim ET_VF As Op_SMS_Type
    
    'VF_All = GetMySQL_Network_Stats("EG-Vodafone")
    'MN_VF = GetMySQL_Network_Stats("EG-Mobinil")
    'ET_VF = GetMySQL_Network_Stats("EG-Etisalat")
    
End Sub

Private Sub OK_Click()
    Dim i As Integer
    Call Minimizing_Click
    For i = 0 To Stats_Option.UBound
        If Stats_Option(i).Value = True Then
            Select Case i
                Case 14
                    Call Switches_Stats(MOC, Date - Val(Days))
                Case 17
                    Call Switches_Stats(MTC, Date - Val(Days))
                Case 0
                    Call HourlyStats_Enhanced(Date)
                Case Else
                    Call Get_N_Store_Stats(StatsMap(i), Date - Val(Days))
            End Select
        End If
    Next i
End Sub

Private Sub AddSeries(ByRef Series() As String, ByVal Data As String)
    ReDim Preserve Series(UBound(Series) + 1)
    Series(UBound(Series)) = Data
End Sub

Public Sub DimUndimAll(Dim_ As Boolean)
    For Each Control In Main_Form_FTD
        Main_Form_FTD.Enabled = Not Dim_
    Next Control
End Sub


