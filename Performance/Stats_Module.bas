Attribute VB_Name = "Stats_Module"


Public Sub HourlyStats_Enhanced(Optional ByRef Data_Date As Date)
    If Year(Data_Date) = 1899 Then Data_Date = Date
    Dim HourlySMS() As Mo_SMS_Type
    HourlySMS = GetMySQLStats_Hourly(Data_Date)
    Dim AllSMS As Op_SMS_Type
    AllSMS.HourlySMS = HourlySMS
    Call StoreData_MDB(SMS_Hourly, AllSMS, Data_Date)
End Sub
Public Sub Switches_Stats(SwitchType As SwitchType, Optional ByRef Data_Date As Date)
    If Year(Data_Date) = 1899 Then Data_Date = Date
    Dim MC_SMS() As Switch_type
    MC_SMS = GetMySQLStats_Switches(SwitchType, Data_Date)
    Dim AllSMS As Op_SMS_Type
    AllSMS.SwitchSMS = MC_SMS
    Call StoreData_MDB(MSC_Messages_MC, AllSMS, Data_Date)
End Sub
