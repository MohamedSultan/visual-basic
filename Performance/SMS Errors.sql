--Sum
select Value,sum(V_Count) from DC_MO_FAILURE_RES where to_char(date_time,'YYYY-MM-DD') = '2009-06-02' group by Value order by Sum(V_Count) desc ;

--Hourly
select to_Char(Date_Time,'YYYY-MM-DD HH24') Hour,Value,V_Count from DC_MO_FAILURE_RES where to_char(date_time,'YYYY-MM-DD') <= '2009-06-01' and to_char(date_time,'YYYY-MM-DD')>='2009-05-30' 

--Distinct
select distinct(value) from DC_MO_FAILURE_RES where to_char(date_time,'YYYY-MM-DD')='2009-06-02';

------------------------------------------------------
--Sum
select Value,sum(V_Count) from DC_MO_REJECT_CAUSE where to_char(date_time,'YYYY-MM-DD') = '2009-06-02' group by Value order by Sum(V_Count) desc;

--Hourly
select to_Char(Date_Time,'HH24') Hour,Value,V_Count from DC_MO_REJECT_CAUSE where to_char(to_Char(Date_Time,'HH24'),'YYYY-MM-DD') = '2009-06-02' ;

---------------------------------------------------------
--Sum
select Value,sum(V_Count) from DC_MO_ROUTING_ACTION where to_char(date_time,'YYYY-MM-DD') = '2009-06-02' group by Value order by Sum(V_Count) desc;

--Hourly
Select to_Char(Date_Time,'HH24') Hour,Value,V_Count from DC_MO_ROUTING_ACTION where to_char(to_Char(Date_Time,'HH24'),'YYYY-MM-DD') = '2009-06-02' ;

---------------------------------------------------------
--Sum
select Value,sum(V_Count) from DC_MO_SUB_RES where to_char(date_time,'YYYY-MM-DD') = '2009-06-02' group by Value order by Sum(V_Count) desc;

--Hourly
select to_Char(Date_Time,'HH24') Hour,Value,V_Count from DC_MO_SUB_RES where to_char(to_Char(Date_Time,'HH24'),'YYYY-MM-DD') = '2009-06-02' ;

--==================================================