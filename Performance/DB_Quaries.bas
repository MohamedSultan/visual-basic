Attribute VB_Name = "DB_Quaries"
Public Sub GetLocalStats(ByRef SMS() As Mo_SMS_Type, ByVal Date_ As Date, Optional DB_SMS_ID As Integer = 0)
    Dim conn As New ADODB.Connection
    Dim SMS_Record(4) As New ADODB.Recordset    '0:LocalDB_SMS_ID, 1:Yesterdays_SMS_ID
    Dim TempValue As Double
   
   'updating Local Data Base
    Call DB_Open(conn, "mdb")
    
    For i = 0 To 4
        Set SMS_Record(i) = New ADODB.Recordset
    Next i
    SMS_Record(0).Open "select * from HourlySMS_Success where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(1).Open "select * from HourlySMS_Decimated where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(2).Open "select * from HourlySMS_Misc where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(3).Open "select * from HourlySMS_TimeOut where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(4).Open "select * from HourlySMS_Unknown where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    
    On Error GoTo LocalHandler
    If Not SMS_Record(0).EOF Then
        For i = 0 To 23
            NewI = Trim(Str(i))
            If i < 10 Then NewI = "0" & Trim(Str(i))
            SMS(i, DB_SMS_ID).MO_Success_SMS = Val(SMS_Record(0).Fields("SMS" + Trim(NewI)).Value)
            SMS(i, DB_SMS_ID).MO_Decimated_SMS = Val(SMS_Record(1).Fields("SMS" + Trim(NewI)).Value)
            SMS(i, DB_SMS_ID).MO_misc_Error_SMS = Val(SMS_Record(2).Fields("SMS" + Trim(NewI)).Value)
            SMS(i, DB_SMS_ID).MO_TimeOut_SMS = Val(SMS_Record(3).Fields("SMS" + Trim(NewI)).Value)
            SMS(i, DB_SMS_ID).MO_Unknown_SMS = Val(SMS_Record(4).Fields("SMS" + Trim(NewI)).Value)
        Next i
    End If
    Call Add_Status("Local Data for date: " + Format(Date_, "dd/mm/yy") + " is Successfully Retreived")
    
LocalHandler:
    If Err.Number = 3021 Then
        Resume Next
    Else
        If Err.Number = 0 Or Err.Number = 20 Then
            Resume Next
        Else
            MsgBox Err.Description
        End If
    End If
End Sub
Public Function GetMySQL_FW_MT_Stats(Optional DateOfData As Date) As Op_SMS_Type
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim Statement As String
    
    If Year(DateOfData) = 1899 Then DateOfData = Date
    'Getting Data
    Call DB_Open(conn, "my SQL")
    
    ' Execute the statement
     Set SMS_Record = New ADODB.Recordset
     
    'Statement = "SELECT SUM(daily_samples.mo_ok) FROM stats.daily_samples daily_samples WHERE (date(timestamp) = date(now())) AND (daily_samples.type = 'TPF_MT_COUNTER')"
    'Statement = "SELECT SUM(daily_samples.mo_ok) FROM stats.daily_samples daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DateOfData, Date)) + " DAY))) AND (daily_samples.type = 'TPF_MT_COUNTER')"
    Statement = "SELECT SUM(mo_ok)," + _
                "SUM(mo_dropped)," + _
                "SUM(mo_err_to), " + _
                "SUM(mo_err_mc), " + _
                "SUM(mo_err_total), " + _
                "SUM(mo_total), " + _
                "SUM(mo_unknown), " + _
                "SUM(mt_ok), " + _
                "SUM(mt_err_to), " + _
                "SUM(mt_err_as), " + _
                "SUM(mt_err_mf), " + _
                "SUM(mt_err_mc)" + _
                "FROM stats.daily_samples daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DateOfData, Date)) + " DAY))) AND (type = 'TPF_MT_COUNTER')"
       
    SMS_Record.Open Statement, conn
    
    While Not SMS_Record.EOF
        GetMySQL_FW_MT_Stats.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("SUM(mo_ok)").Value)
        GetMySQL_FW_MT_Stats.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("SUM(mo_dropped)").Value)
        GetMySQL_FW_MT_Stats.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mo_err_mc)").Value)
        GetMySQL_FW_MT_Stats.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mo_err_to)").Value)
        GetMySQL_FW_MT_Stats.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("SUM(mo_unknown)").Value)
        GetMySQL_FW_MT_Stats.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("SUM(mt_ok)").Value)
        GetMySQL_FW_MT_Stats.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("SUM(mt_err_as)").Value)
        GetMySQL_FW_MT_Stats.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("SUM(mt_err_mf)").Value)
        GetMySQL_FW_MT_Stats.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mt_err_mc)").Value)
        GetMySQL_FW_MT_Stats.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mt_err_to)").Value)
        'GetMySQL_FW_MT_Stats.Mo_SMS = GetMySQL_FW_MT_Stats + Val(SMS_Record.Fields(0).Value)
        SMS_Record.MoveNext
    Wend

    conn.Close
End Function
Public Function GetMySQL_AO_Stats(Optional DateOfData As Date) As Op_SMS_Type
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim Statement As String
    
    If Year(DateOfData) = 1899 Then DateOfData = Date
    'Getting Data
    Call DB_Open(conn, "my SQL")
    
    ' Execute the statement
     Set SMS_Record = New ADODB.Recordset
   
    Statement = "SELECT daily_samples.name," + _
                "SUM(mo_ok)," + _
                "SUM(mo_dropped)," + _
                "SUM(mo_err_to), " + _
                "SUM(mo_err_mc), " + _
                "SUM(mo_err_total), " + _
                "SUM(mo_total), " + _
                "SUM(mo_unknown), " + _
                "SUM(mt_ok), " + _
                "SUM(mt_err_to), " + _
                "SUM(mt_err_as), " + _
                "SUM(mt_err_mf), " + _
                "SUM(mt_err_mc)" + _
                "FROM stats.daily_samples daily_samples " + _
                "WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DateOfData, Date)) + " DAY))) " + _
                " AND(daily_samples.type = 'COUNTER')" + _
                " AND(daily_samples.operator IS NULL)" + _
                " AND(daily_samples.country IS NULL)" + _
                " AND(daily_samples.code IS NOT NULL)" + _
                " GROUP BY daily_samples.name"

    SMS_Record.Open Statement, conn
    
    While Not SMS_Record.EOF
        GetMySQL_AO_Stats.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("SUM(mo_ok)").Value) + GetMySQL_AO_Stats.Mo_SMS.MO_Success_SMS
        GetMySQL_AO_Stats.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("SUM(mo_dropped)").Value) + GetMySQL_AO_Stats.Mo_SMS.MO_Decimated_SMS
        GetMySQL_AO_Stats.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mo_err_mc)").Value) + GetMySQL_AO_Stats.Mo_SMS.MO_misc_Error_SMS
        GetMySQL_AO_Stats.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mo_err_to)").Value) + GetMySQL_AO_Stats.Mo_SMS.MO_TimeOut_SMS
        GetMySQL_AO_Stats.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("SUM(mo_unknown)").Value) + GetMySQL_AO_Stats.Mo_SMS.MO_Unknown_SMS
        GetMySQL_AO_Stats.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("SUM(mt_ok)").Value) + GetMySQL_AO_Stats.Mt_SMS.Mt_Success_SMS
        GetMySQL_AO_Stats.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("SUM(mt_err_as)").Value) + GetMySQL_AO_Stats.Mt_SMS.Mt_Abs_Sub
        GetMySQL_AO_Stats.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("SUM(mt_err_mf)").Value) + GetMySQL_AO_Stats.Mt_SMS.Mt_Mem_Full
        GetMySQL_AO_Stats.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mt_err_mc)").Value) + GetMySQL_AO_Stats.Mt_SMS.Mt_misc_Error_SMS
        GetMySQL_AO_Stats.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mt_err_to)").Value) + GetMySQL_AO_Stats.Mt_SMS.Mt_TimeOut_SMS
        'GetMySQL_AO_Stats.Mo_SMS = GetMySQL_FW_MT_Stats + Val(SMS_Record.Fields(0).Value)
        SMS_Record.MoveNext
    Wend

    conn.Close
End Function

Public Function GetMySQL_ECI_MT_Stats(Optional DateOfData As Date) As Op_SMS_Type
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim Statement As String
    
    If Year(DateOfData) = 1899 Then DateOfData = Date
    'Getting Data
    Call DB_Open(conn, "my SQL")
    
    ' Execute the statement
     Set SMS_Record = New ADODB.Recordset
     
    Statement = "SELECT SUM(mo_ok)," + _
                "SUM(mo_dropped)," + _
                "SUM(mo_err_to), " + _
                "SUM(mo_err_mc), " + _
                "SUM(mo_err_total), " + _
                "SUM(mo_total), " + _
                "SUM(mo_unknown), " + _
                "SUM(mt_ok), " + _
                "SUM(mt_err_to), " + _
                "SUM(mt_err_as), " + _
                "SUM(mt_err_mf), " + _
                "SUM(mt_err_mc)" + _
                "FROM stats.daily_samples daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DateOfData, Date)) + " DAY))) AND (type = 'ECI_MT_COUNTER')"
       
    SMS_Record.Open Statement, conn
    
    While Not SMS_Record.EOF
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("SUM(mo_ok)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("SUM(mo_dropped)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mo_err_mc)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mo_err_to)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("SUM(mo_unknown)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("SUM(mt_ok)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("SUM(mt_err_as)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("SUM(mt_err_mf)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mt_err_mc)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mt_err_to)").Value)
        'GetMySQL_FW_MT_Stats.Mo_SMS = GetMySQL_FW_MT_Stats + Val(SMS_Record.Fields(0).Value)
        SMS_Record.MoveNext
    Wend

    conn.Close
End Function
Public Function GetMySQL_VF_Roamer_Stats(Optional DateOfData As Date) As Op_SMS_Type
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim Statement As String
    
    If Year(DateOfData) = 1899 Then DateOfData = Date
    'Getting Data
    Call DB_Open(conn, "my SQL")
    
    ' Execute the statement
     Set SMS_Record = New ADODB.Recordset
     
    Statement = "SELECT SUM(mo_ok)," + _
                "SUM(mo_dropped)," + _
                "SUM(mo_err_to), " + _
                "SUM(mo_err_mc), " + _
                "SUM(mo_err_total), " + _
                "SUM(mo_total), " + _
                "SUM(mo_unknown), " + _
                "SUM(mt_ok), " + _
                "SUM(mt_err_to), " + _
                "SUM(mt_err_as), " + _
                "SUM(mt_err_mf), " + _
                "SUM(mt_err_mc)" + _
                "FROM stats.daily_samples daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DateOfData, Date)) + " DAY))) AND (type = 'ECI_MO_COUNTER') AND (name='PBC')"
       
       Statement = "SELECT (mo_ok)," + _
                "(mo_dropped)," + _
                "(mo_err_to), " + _
                "(mo_err_mc), " + _
                "(mo_err_total), " + _
                "(mo_total), " + _
                "(mo_unknown), " + _
                "(mt_ok), " + _
                "(mt_err_to), " + _
                "(mt_err_as), " + _
                "(mt_err_mf), " + _
                "(mt_err_mc)" + _
                "FROM stats.daily_samples daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + Str(DateDiff("d", DateOfData, Date)) + " DAY))) AND (type = 'ECI_MO_COUNTER') AND (name='PBC')"
                
    SMS_Record.Open Statement, conn
    
    While Not SMS_Record.EOF
        GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("mo_ok").Value) + GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_Success_SMS
        GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("mo_dropped").Value) + GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_Decimated_SMS
        GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("mo_err_mc").Value) + GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_misc_Error_SMS
        GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("mo_err_to").Value) + GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_TimeOut_SMS
        GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("mo_unknown").Value) + GetMySQL_VF_Roamer_Stats.Mo_SMS.MO_Unknown_SMS
        GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("mt_ok").Value) + GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_Success_SMS
        GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("mt_err_as").Value) + GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_Abs_Sub
        GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("mt_err_mf").Value) + GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_Mem_Full
        GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("mt_err_mc").Value) + GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_misc_Error_SMS
        GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("mt_err_to").Value) + GetMySQL_VF_Roamer_Stats.Mt_SMS.Mt_TimeOut_SMS
        'GetMySQL_FW_MT_Stats.Mo_SMS = GetMySQL_FW_MT_Stats + Val(SMS_Record.Fields(0).Value)
        SMS_Record.MoveNext
    Wend

    conn.Close
End Function
Public Function GetMySQL_Device_Stats(Optional DateOfData As Date) As Op_SMS_Type
    Dim conn As New ADODB.Connection
    Dim SMS_Record As New ADODB.Recordset
    Dim Statement As String
    
    If Year(DateOfData) = 1899 Then DateOfData = Date
    'Getting Data
    Call DB_Open(conn, "my SQL")
    
    ' Execute the statement
     Set SMS_Record = New ADODB.Recordset
     
    Statement = "SELECT name,SUM(mo_ok)," + _
                    "SUM(mo_dropped)," + _
                    "SUM(mo_err_to), " + _
                    "SUM(mo_err_mc), " + _
                    "SUM(mo_err_total), " + _
                    "SUM(mo_total), " + _
                    "SUM(mo_unknown), " + _
                    "SUM(mt_ok), " + _
                    "SUM(mt_err_to), " + _
                    "SUM(mt_err_as), " + _
                    "SUM(mt_err_mf), " + _
                    "SUM(mt_err_mc)" + _
                    "FROM daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + _
                                                Str(DateDiff("d", DateOfData, Date)) + _
                                                " DAY))) AND (type='COUNTER')" + _
                                                " AND (name='_TOTAL')" + _
                                                " group by name"
       
    SMS_Record.Open Statement, conn
    
    While Not SMS_Record.EOF
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("SUM(mo_ok)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("SUM(mo_dropped)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mo_err_mc)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mo_err_to)").Value)
        GetMySQL_ECI_MT_Stats.Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("SUM(mo_unknown)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("SUM(mt_ok)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("SUM(mt_err_as)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("SUM(mt_err_mf)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mt_err_mc)").Value)
        GetMySQL_ECI_MT_Stats.Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mt_err_to)").Value)
        'GetMySQL_FW_MT_Stats.Mo_SMS = GetMySQL_FW_MT_Stats + Val(SMS_Record.Fields(0).Value)
        SMS_Record.MoveNext
    Wend

    conn.Close
End Function
Public Sub GetLocalStats_OP(ByRef SMS() As Op_SMS_Type, ByVal Date_ As Date, Optional DB_SMS_ID As Integer = 1)
    Dim conn As New ADODB.Connection
    Dim SMS_Record(3) As New ADODB.Recordset    '0:LocalDB_SMS_ID, 1:Yesterdays_SMS_ID
    Dim TempValue As Double
   
   'updating Local Data Base
    Call DB_Open(conn, "mdb")
    
    For i = 0 To 3
        Set SMS_Record(i) = New ADODB.Recordset
    Next i
    SMS_Record(0).Open "select * from VF_All where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(1).Open "select * from VF_MN where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(2).Open "select * from VF_ET where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    SMS_Record(3).Open "select * from VF_Int where Date_ = '" + Format(Date_, "dd/mm/yy") + "'", conn
    
    On Error GoTo LocalHandler
    For i = 0 To 3
        If Not SMS_Record(i).EOF Then
            Select Case i
                Case 0      'VF-all
                    SMS(i, DB_SMS_ID).Mo_SMS.MO_Success_SMS = Val(SMS_Record(i).Fields("Success").Value)
                    SMS(i, DB_SMS_ID).Mo_SMS.MO_Decimated_SMS = Val(SMS_Record(i).Fields("ERR_Decimated").Value)
                    SMS(i, DB_SMS_ID).Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record(i).Fields("ERR_Misc").Value)
                    SMS(i, DB_SMS_ID).Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record(i).Fields("ERR_Time_Out").Value)
                    SMS(i, DB_SMS_ID).Mo_SMS.MO_Unknown_SMS = Val(SMS_Record(i).Fields("ERR_Unknown").Value)
                Case 1, 2     'VF-MN, 'VF-ET
                    SMS(i, DB_SMS_ID).Mt_SMS.Mt_Success_SMS = Val(SMS_Record(i).Fields("Success").Value)
                    SMS(i, DB_SMS_ID).Mt_SMS.Mt_Abs_Sub = Val(SMS_Record(i).Fields("ERR_Abs").Value)
                    SMS(i, DB_SMS_ID).Mt_SMS.Mt_Mem_Full = Val(SMS_Record(i).Fields("ERR_Mem_Full").Value)
                    SMS(i, DB_SMS_ID).Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record(i).Fields("ERR_Misc").Value)
                    SMS(i, DB_SMS_ID).Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record(i).Fields("ERR_TimeOut").Value)
                Case 3      'VF-Int
            End Select
        End If
        Call Add_Status("Local Data for date: " + Format(Date_, "dd/mm/yy") + " is Successfully Retreived")
    Next i
    
LocalHandler:
    If Err.Number = 3021 Then
        Resume Next
    Else
        If Err.Number = 0 Or Err.Number = 20 Then
            Resume Next
        Else
            MsgBox Err.Description
        End If
    End If
End Sub


Public Sub StoreData_Op(TableName As String, ByRef Data() As Op_SMS_Type, Optional SMS_ID As Integer = 1, Optional DataDate As Date)
    'Storing Data
    Dim conn As New ADODB.Connection
    Dim VF_SMS As Op_SMS_Type
    Dim DataDate_S, Statment_Insert, Statment_Update, SET_, Fields, SetValues, Values As String
    
    
    If Year(DataDate) = 1899 Then
        DataDate_S = Format(Date, "dd/mm/yy")
    Else
        DataDate_S = Format(DataDate, "dd/mm/yy")
    End If
    
    Select Case LCase(TableName)
        Case LCase("VF_All"), LCase("Int_VF"), LCase("MN_ET_VF")
            Fields = "Date_, Success, ERR_Decimated, ERR_Time_Out, ERR_Misc, ERR_Unknown"
            
            If LCase(TableName) = LCase("Int_VF") Or LCase(TableName) = LCase("MN_ET_VF") Then
                VF_SMS.Mo_SMS.MO_Decimated_SMS = Data(0).Mo_SMS.MO_Decimated_SMS
                VF_SMS.Mo_SMS.MO_misc_Error_SMS = Data(0).Mo_SMS.MO_misc_Error_SMS
                VF_SMS.Mo_SMS.MO_Success_SMS = Data(0).Mo_SMS.MO_Success_SMS
                VF_SMS.Mo_SMS.MO_TimeOut_SMS = Data(0).Mo_SMS.MO_TimeOut_SMS
                VF_SMS.Mo_SMS.MO_Unknown_SMS = Data(0).Mo_SMS.MO_Unknown_SMS
            Else
                VF_SMS.Mo_SMS.MO_Decimated_SMS = Max(Data(0, 0).Mo_SMS.MO_Decimated_SMS, Data(0, 1).Mo_SMS.MO_Decimated_SMS)
                VF_SMS.Mo_SMS.MO_misc_Error_SMS = Max(Data(0, 0).Mo_SMS.MO_misc_Error_SMS, Data(0, 1).Mo_SMS.MO_misc_Error_SMS)
                VF_SMS.Mo_SMS.MO_Success_SMS = Max(Data(0, 0).Mo_SMS.MO_Success_SMS, Data(0, 1).Mo_SMS.MO_Success_SMS)
                VF_SMS.Mo_SMS.MO_TimeOut_SMS = Max(Data(0, 0).Mo_SMS.MO_TimeOut_SMS, Data(0, 1).Mo_SMS.MO_TimeOut_SMS)
                VF_SMS.Mo_SMS.MO_Unknown_SMS = Max(Data(0, 0).Mo_SMS.MO_Unknown_SMS, Data(0, 1).Mo_SMS.MO_Unknown_SMS)
            End If

            Values = Str(VF_SMS.Mo_SMS.MO_Success_SMS) + ", " + _
                        Str(VF_SMS.Mo_SMS.MO_Decimated_SMS) + "," + _
                        Str(VF_SMS.Mo_SMS.MO_TimeOut_SMS) + ", " + _
                        Str(VF_SMS.Mo_SMS.MO_misc_Error_SMS) + "," + _
                        Str(VF_SMS.Mo_SMS.MO_Unknown_SMS)
                        
            SET_ = "ERR_Decimated=" + Str(VF_SMS.Mo_SMS.MO_Decimated_SMS) + "," + _
                        "ERR_Misc=" + Str(VF_SMS.Mo_SMS.MO_misc_Error_SMS) + "," + _
                        "success=" + Str(VF_SMS.Mo_SMS.MO_Success_SMS) + ", " + _
                        "ERR_Time_Out=" + Str(VF_SMS.Mo_SMS.MO_TimeOut_SMS) + ", " + _
                        "ERR_Unknown=" + Str(VF_SMS.Mo_SMS.MO_Unknown_SMS)
            
            Statment_Insert = "INSERT INTO " + TableName + " (" + Fields + ") " + _
                                "VALUES ( '" + DataDate_S + "'," + Values + ") "
            Statment_Update = "UPDATE " + TableName + " SET " + SET_ + " WHERE DATE_ = '" + DataDate_S + "'"
            
        Case LCase("VF_MN"), LCase("VF_ET"), LCase("App_VF") ', LCase("MN_ET_VF"), LCase("Int_VF")
            Dim Operator As Integer
            Select Case LCase(TableName)
                Case LCase("VF_MN")
                    Operator = 1
                Case LCase("VF_ET")
                    Operator = 2
            End Select
            Fields = "Date_, Success, ERR_Abs, ERR_Mem_Full, ERR_TimeOut, ERR_Misc"
            
            'select case
            If LCase(TableName) = LCase("App_VF") Then
                VF_SMS.Mt_SMS.Mt_Abs_Sub = Data(0).Mt_SMS.Mt_Abs_Sub
                VF_SMS.Mt_SMS.Mt_Mem_Full = Data(0).Mt_SMS.Mt_Mem_Full
                VF_SMS.Mt_SMS.Mt_misc_Error_SMS = Data(0).Mt_SMS.Mt_misc_Error_SMS
                VF_SMS.Mt_SMS.Mt_Success_SMS = Data(0).Mt_SMS.Mt_Success_SMS
                VF_SMS.Mt_SMS.Mt_TimeOut_SMS = Data(0).Mt_SMS.Mt_TimeOut_SMS
            Else
                VF_SMS.Mt_SMS.Mt_Abs_Sub = Max(Data(Operator, 0).Mt_SMS.Mt_Abs_Sub, Data(Operator, 1).Mt_SMS.Mt_Abs_Sub)
                VF_SMS.Mt_SMS.Mt_Mem_Full = Max(Data(Operator, 0).Mt_SMS.Mt_Mem_Full, Data(Operator, 1).Mt_SMS.Mt_Mem_Full)
                VF_SMS.Mt_SMS.Mt_misc_Error_SMS = Max(Data(Operator, 0).Mt_SMS.Mt_misc_Error_SMS, Data(Operator, 1).Mt_SMS.Mt_misc_Error_SMS)
                VF_SMS.Mt_SMS.Mt_Success_SMS = Max(Data(Operator, 0).Mt_SMS.Mt_Success_SMS, Data(Operator, 1).Mt_SMS.Mt_Success_SMS)
                VF_SMS.Mt_SMS.Mt_TimeOut_SMS = Max(Data(Operator, 0).Mt_SMS.Mt_TimeOut_SMS, Data(Operator, 1).Mt_SMS.Mt_TimeOut_SMS)
            End If
            Values = Str(VF_SMS.Mt_SMS.Mt_Success_SMS) + ", " + _
                        Str(VF_SMS.Mt_SMS.Mt_Abs_Sub) + "," + _
                        Str(VF_SMS.Mt_SMS.Mt_Mem_Full) + ", " + _
                        Str(VF_SMS.Mt_SMS.Mt_TimeOut_SMS) + "," + _
                        Str(VF_SMS.Mt_SMS.Mt_misc_Error_SMS)
                        
            SET_ = "Success=" + Str(VF_SMS.Mt_SMS.Mt_Success_SMS) + ", " + _
                    "ERR_Abs=" + Str(VF_SMS.Mt_SMS.Mt_Abs_Sub) + ", " + _
                    "ERR_Mem_Full=" + Str(VF_SMS.Mt_SMS.Mt_Mem_Full) + ", " + _
                    "ERR_TimeOut=" + Str(VF_SMS.Mt_SMS.Mt_TimeOut_SMS) + ", " + _
                    "ERR_Misc=" + Str(VF_SMS.Mt_SMS.Mt_misc_Error_SMS)
            
            Statment_Insert = "INSERT INTO " + TableName + " (" + Fields + ") " + _
                                "VALUES ( '" + DataDate_S + "'," + Values + ") "
            Statment_Update = "UPDATE " + TableName + " SET " + SET_ + " WHERE DATE_ = '" + DataDate_S + "'"
    End Select
    
    'updating Local Data Base
    Call DB_Open(conn, "mdb")
    
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset

    On Error GoTo Handler
    Result.Open Statment_Insert, conn

    Call Add_Status("Data 'VF-All' from FTD is Successfully Stored ")

    Call Main_Form.DimUndimAll(False)

Handler:
    If Err.Number = -2147467259 Then     'table exist

        Result.Open Statment_Update, conn

        conn.Close
        Call Add_Status("Data " + TableName + " from FTD is Successfully Updated ")
        
        Call Main_Form.DimUndimAll(False)
        
    Else
        If Err.Number = 20 Or Err.Number = 0 Then
            Resume Next
        Else
            MsgBox Err.Description
            Call Main_Form.DimUndimAll(False)
        End If
    End If
End Sub
Public Sub StoreData(TableName As String, ByRef Data() As Mo_SMS_Type, Optional SMS_ID As Integer = 1, Optional DataDate As Date)
    'Storing Data
    Dim conn As New ADODB.Connection
    Dim SMS(23) As Double
    Select Case LCase(TableName)
        Case LCase("HourlySMS_Decimated")
            For i = 0 To 23
                SMS(i) = Max(Data(i, 0).MO_Decimated_SMS, Data(i, 1).MO_Decimated_SMS)
            Next i
        Case LCase("HourlySMS_Misc")
            For i = 0 To 23
                SMS(i) = Max(Data(i, 0).MO_misc_Error_SMS, Data(i, 1).MO_misc_Error_SMS)
            Next i
        Case LCase("HourlySMS_Success")
            For i = 0 To 23
                SMS(i) = Max(Data(i, 0).MO_Success_SMS, Data(i, 1).MO_Success_SMS)
            Next i
        Case LCase("HourlySMS_TimeOut")
            For i = 0 To 23
                SMS(i) = Max(Data(i, 0).MO_TimeOut_SMS, Data(i, 1).MO_TimeOut_SMS)
            Next i
        Case LCase("HourlySMS_Unknown")
            For i = 0 To 23
                SMS(i) = Max(Data(i, 0).MO_Unknown_SMS, Data(i, 1).MO_Unknown_SMS)
            Next i
    End Select
    
    'updating Local Data Base
    Call DB_Open(conn, "mdb")
    
    Dim Statement, Into, Values As String

    For i = 0 To 23
        NewI = Trim(Str(i))
        If i < 10 Then NewI = "0" & Trim(Str(i))
        Into = Into + "SMS" & NewI & ", "
        Values = Values + " " + Trim(Str(SMS(i))) + " ,"
    Next i
    Into = Left(Into, Len(Into) - 2)
    Values = Left(Values, Len(Values) - 1)

    Dim DataDate_S As String
    If Year(DataDate) = 1899 Then
        DataDate_S = Format(Date, "dd/mm/yy")
    Else
        DataDate_S = Format(DataDate, "DD/MM/YY")
    End If
    
    Statement = "INSERT INTO " + TableName + " (Date_," + _
                Into + ") " + _
                "VALUES ( '" + DataDate_S + "'," + Values + ") "
    Dim Result As ADODB.Recordset
    Set Result = New ADODB.Recordset
    'Messages_Chart.ChartData = SMS
    On Error GoTo Handler
    Result.Open Statement, conn

    Call Add_Status("Data from FTD is Successfully Stored ")

    Call Main_Form.DimUndimAll(False)

Handler:
    If Err.Number = -2147467259 Then     'table exist
        Dim SetValues As String
        SetValues = "SET "
        For i = 0 To 23
            NewI = Trim(Str(i))
            If i < 10 Then NewI = "0" & Trim(Str(i))
            SetValues = SetValues + "SMS" & NewI & " =" & Str(SMS(i)) + " ,"
        Next i
        SetValues = Left(SetValues, Len(SetValues) - 1)

        Statement = "UPDATE " + TableName + " " + SetValues + " WHERE Date_ = '" + DataDate_S + "'"
        Result.Open Statement, conn
        
        Call Add_Status("Data from FTD is Successfully Updated ")
        conn.Close
        Call Main_Form.DimUndimAll(False)

    Else
        If Err.Number = 0 Or Err.Number = 20 Then
            Resume Next
        Else
            MsgBox Err.Description
            Call Main_Form.DimUndimAll(False)
        End If
    End If
End Sub
Public Sub GetMySQL_Operator_Stats(ByRef SMS() As Op_SMS_Type, Optional MySQL_SMS_ID As Integer = 0, Optional DateOfData As Date)

    'Gets details about each operator (Mo and MT)

    Dim conn As New ADODB.Connection
    Dim SMS_Record, SMS_Record_Temp As New ADODB.Recordset
    Dim TempValue As Op_SMS_Type
    Dim Operator, Statement As String
    Dim Oper As Integer
    
    Call DB_Open(conn, "my SQL")
    
    Set SMS_Record_Temp = New ADODB.Recordset
    
    If Year(DateOfData) = 1899 Then DateOfData = Date

    Set SMS_Record = New ADODB.Recordset
    
    Statement = "SELECT operator,SUM(mo_ok)," + _
                        "SUM(mo_dropped)," + _
                        "SUM(mo_err_to), " + _
                        "SUM(mo_err_mc), " + _
                        "SUM(mo_err_total), " + _
                        "SUM(mo_total), " + _
                        "SUM(mo_unknown), " + _
                        "SUM(mt_ok), " + _
                        "SUM(mt_err_to), " + _
                        "SUM(mt_err_as), " + _
                        "SUM(mt_err_mf), " + _
                        "SUM(mt_err_mc)" + _
                        "FROM daily_samples WHERE (DATE(timestamp) = DATE(DATE_SUB(CURDATE(), INTERVAL " + _
                                                    Str(DateDiff("d", DateOfData, Date)) + _
                                                    " DAY))) AND (operator='EG-Etisalat' OR operator='EG-Mobinil' OR operator='EG-Vodafone')" + _
                                                    " group by operator"
    
            SMS_Record.Open Statement, conn
            
        While Not SMS_Record.EOF
            
            Select Case SMS_Record.Fields("operator").Value
                Case "EG-Vodafone"
                    Oper = 0
                Case "EG-Etisalat"
                    Oper = 2
                Case "EG-Mobinil"
                    Oper = 1
            End Select
            SMS(Oper, MySQL_SMS_ID).Mo_SMS.MO_Success_SMS = Val(SMS_Record.Fields("SUM(mo_ok)").Value)
            SMS(Oper, MySQL_SMS_ID).Mo_SMS.MO_Decimated_SMS = Val(SMS_Record.Fields("SUM(mo_dropped)").Value)
            SMS(Oper, MySQL_SMS_ID).Mo_SMS.MO_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mo_err_mc)").Value)
            SMS(Oper, MySQL_SMS_ID).Mo_SMS.MO_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mo_err_to)").Value)
            SMS(Oper, MySQL_SMS_ID).Mo_SMS.MO_Unknown_SMS = Val(SMS_Record.Fields("SUM(mo_unknown)").Value)
            SMS(Oper, MySQL_SMS_ID).Mt_SMS.Mt_Success_SMS = Val(SMS_Record.Fields("SUM(mt_ok)").Value)
            SMS(Oper, MySQL_SMS_ID).Mt_SMS.Mt_Abs_Sub = Val(SMS_Record.Fields("SUM(mt_err_as)").Value)
            SMS(Oper, MySQL_SMS_ID).Mt_SMS.Mt_Mem_Full = Val(SMS_Record.Fields("SUM(mt_err_mf)").Value)
            SMS(Oper, MySQL_SMS_ID).Mt_SMS.Mt_misc_Error_SMS = Val(SMS_Record.Fields("SUM(mt_err_mc)").Value)
            SMS(Oper, MySQL_SMS_ID).Mt_SMS.Mt_TimeOut_SMS = Val(SMS_Record.Fields("SUM(mt_err_to)").Value)
            SMS_Record.MoveNext
        Wend
'    Next I
    conn.Close
100524521
End Sub


