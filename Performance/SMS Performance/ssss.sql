SELECT     *, [Date] AS Expr2, Route AS Expr4, [Time] AS Expr1, Congested AS Expr5, Terminating_Node AS Expr3, Utilization AS Expr6
FROM         cnp_application_db..RG_Vas_Routes_Data_view
WHERE     ([Time] >= CONVERT(DATETIME, '2009-06-24 00:00:00', 102))
ORDER BY [Time] DESC