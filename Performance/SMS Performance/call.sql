CREATE VIEW `X  Disconnected` AS
  SELECT `callcollect`.`transactions`.`Date` AS `date`,
       `callcollect`.`transactions`.`admin_action` AS `admin_Action`,
       sum(`callcollect`.`transactions`.`count`) AS `NotAllowed_Count`
  FROM `callcollect`.`transactions`
 WHERE (`callcollect`.`transactions`.`admin_action` = 'Disconnected') and action_data='Disconnected'
GROUP BY `callcollect`.`transactions`.`Date`,
         `callcollect`.`transactions`.`admin_action`;
