insert ccn_counters_aggregated(CCN_ID,Date_,Time,Proccessor,CcnPerfCounter,Value)
			select CCN_ID,Date_,convert(varchar,SUBSTRING(FromTime, 1, 2)+':00' ,108),
			Proccessor
			,sum(CONVERT(INT, CcnPerfCounter)) CcnPerfCounter
			,Value
			from ccn_counters
			where Date_<>CONVERT(VARCHAR(10), getdate(), 120)
			group by CCN_ID,Date_,convert(varchar,SUBSTRING(FromTime, 1, 2)+':00' ,108),
							Proccessor,Value