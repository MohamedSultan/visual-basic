Attribute VB_Name = "Plot_Chart_Module"
Enum ReportTypes
    WeeklyReport = 1
    MonthlyReport = 2
End Enum
Public Sub Weekly_Report(ByRef SMS_Data() As String, ByRef Chart As MSChart, Series() As String, Optional ChartDataType As PlotType = FTD, Optional ShowLegend As Boolean)
    With Chart
        .ChartData = SMS_Data
    
        If ShowLegend Then
            For i = 1 To Chart.Plot.SeriesCollection.Count
                .Plot.SeriesCollection(i).LegendText = Series(i)
            Next i
                .ShowLegend = True
        End If
        
        If .Plot.SeriesCollection.Count >= 4 Then .Plot.SeriesCollection(4).DataPoints(-1).Brush.FillColor.Set 128, 64, 0
         
        '.Plot.SeriesCollection(4).DataPoints(-1).Brush.FillColor.Set 255, 128, 0
        
        Select Case ChartDataType
            Case FTD
                .Plot.Axis(VtChAxisIdY).AxisTitle.Text = "# of messages divided by 100"
                Select Case UBound(SMS_Data)
                    Case 23         'Hourly
                        For i = 0 To UBound(SMS_Data)
                            .Row = i + 1          '0:Today,1:Yesterday,2:Same day before 1 week,3:same day last month
                            If SMS_Data(i, 2) > SMS_Data(i, 0) Then
                                .RowLabel = "Hour " + Str(i) + "  (D)"
                            Else
                                .RowLabel = "Hour " + Str(i) + "  (U)"
                            End If
                        Next i
                    Case 6          'Weekly
                        For i = 1 To 7
                            .Row = i
                            If SMS_Data(i - 1, 1) > SMS_Data(i - 1, 0) Then
                                .RowLabel = WeekdayName(i, , vbSaturday) + "  (D)"
                            Else
                                .RowLabel = WeekdayName(i, , vbSaturday) + "  (U)"
                            End If
                        Next i
                    Case Is >= 30   'Monthly
                        For i = 1 To UBound(SMS_Data)
                            .Row = i
                            If SMS_Data(i - 1, 1) > SMS_Data(i - 1, 0) Then
                                .RowLabel = "Day " + Str(i) + "  (D)"
                            Else
                                .RowLabel = "Day " + Str(i) + "  (U)"
                            End If
                        Next i
                Case FAF
                    For i = 1 To 7
                        .Row = i
                        If SMS_Data(i - 1, 1) > SMS_Data(i - 1, 0) Then
                            .RowLabel = WeekdayName(i, , vbSaturday) + "  (D)"
                        Else
                            .RowLabel = WeekdayName(i, , vbSaturday) + "  (U)"
                        End If
                    Next i
                End Select
            Case Else
        End Select
        
    End With
End Sub
Public Sub DisplayStats_Test()
    Dim SMS() As Op_SMS_Type
    Dim SMS_Temp() As Op_SMS_Type
    Dim SMS_All(15) As Op_SMS_Type
    Dim SuccessSMS() As String
    Dim DayOfWeek As Integer
    Dim Series(15) As String
    Dim DataDate As Date
    
    If Year(DataDate) = 1899 Then DataDate = Date
    
    DayOfWeek = Weekday(Date, vbSaturday)
    ReDim SuccessSMS(6, 14)
    Dim i As Integer
    For i = 0 To 14
        If Main.Stats_Option(i).Enabled = True Then
            Call Generate_Data_Report(StatsMap(i), SMS_Temp, DataDate - 6, DataDate)
            Series(i) = StatsName(StatsMap(i))
            If i <= 6 Or i = 14 Then
                For j = UBound(SMS_Temp) + 1 To 1 Step -1
                    SuccessSMS(j - 1, i) = SMS_Temp(j - 1).Mo_SMS.MO_Success_SMS / 100
                Next j
            Else
                For j = UBound(SMS_Temp) + 1 To 1 Step -1
                    SuccessSMS(j - 1, i) = SMS_Temp(j - 1).Mt_SMS.Mt_Success_SMS / 100
                Next j
            End If
        End If
    Next i
    Temp.Show
    Call Weekly_Report(SuccessSMS, Temp.Messages_Chart, Series, , True)

'    ReDim SMS(0)
'    ReDim SuccessSMS(0, 0)
'    ReDim SuccessSMS(30, 0)
'    Call Generate_Data_Report(MO_VF_Local, SMS, DataDate - Day(DataDate) + 1, DataDate)
'    For i = Day(DataDate) To 1 Step -1
'        SuccessSMS(i - 1, 0) = SMS(i - 1).Mo_SMS.MO_Success_SMS / 100
'    Next i
'    Series(1) = "This Month"
'    Series(2) = "Last Month"
'    Call Weekly_Report(SuccessSMS, Success_SMS_Form.This_Month_Chart, Series)

End Sub
Public Sub DisplayStats(statsType As Statistic_Types, FTD_Side As FTD_Side_Type, Optional DataDate As Date)
    Dim SMS() As Op_SMS_Type
    Dim SMS_Old() As Op_SMS_Type
    Dim SuccessSMS() As String
    Dim FailedSMS() As String
    Dim DayOfWeek As Integer
    Dim Series(2) As String
    'Dim Success_SMS_Form, Failed_SMS_Form As SMS_Graphs_Form
    
    
    If Year(DataDate) = 1899 Then DataDate = Date
    
    Select Case statsType
        Case SMS_Hourly
            Dim SameDayLastMonth As Date
            Dim SMS_Oldest() As Op_SMS_Type
            Dim YesterdaySMS() As Op_SMS_Type
            SameDayLastMonth = DateSerial(Year(Now), Month(Now) - 1, Day(Now))
            
            Call Generate_Data_Report(statsType, SMS, DataDate, DataDate)
            Call Generate_Data_Report(statsType, YesterdaySMS, DataDate - 1, DataDate - 1)
            Call Generate_Data_Report(statsType, SMS_Old, DataDate - 7, DataDate - 7)
            Call Generate_Data_Report(statsType, SMS_Oldest, SameDayLastMonth, SameDayLastMonth)
            
            ReDim SuccessSMS(23, 3)     '0:Today,1:Yesterday,2:Same day before 1 week,3:same day last month
            ReDim FailedSMS(23, 3)
            
            For i = 0 To 23
                'Today
                SuccessSMS(i, 0) = SMS(0).HourlySMS(i).MO_Success_SMS
                FailedSMS(i, 0) = SMS(0).HourlySMS(i).MO_Decimated_SMS + _
                                    SMS(0).HourlySMS(i).MO_misc_Error_SMS + _
                                    SMS(0).HourlySMS(i).MO_TimeOut_SMS + _
                                    SMS(0).HourlySMS(i).MO_Unknown_SMS
                'Yesterday
                SuccessSMS(i, 1) = YesterdaySMS(0).HourlySMS(i).MO_Success_SMS
                FailedSMS(i, 1) = YesterdaySMS(0).HourlySMS(i).MO_Decimated_SMS + _
                                    YesterdaySMS(0).HourlySMS(i).MO_misc_Error_SMS + _
                                    YesterdaySMS(0).HourlySMS(i).MO_TimeOut_SMS + _
                                    YesterdaySMS(0).HourlySMS(i).MO_Unknown_SMS
                '1 week before
                SuccessSMS(i, 2) = SMS_Old(0).HourlySMS(i).MO_Success_SMS
                FailedSMS(i, 2) = SMS_Old(0).HourlySMS(i).MO_Decimated_SMS + _
                                    SMS_Old(0).HourlySMS(i).MO_misc_Error_SMS + _
                                    SMS_Old(0).HourlySMS(i).MO_TimeOut_SMS + _
                                    SMS_Old(0).HourlySMS(i).MO_Unknown_SMS
                '1 Month before
                SuccessSMS(i, 3) = SMS_Oldest(0).HourlySMS(i).MO_Success_SMS
                FailedSMS(i, 3) = SMS_Oldest(0).HourlySMS(i).MO_Decimated_SMS + _
                                    SMS_Oldest(0).HourlySMS(i).MO_misc_Error_SMS + _
                                    SMS_Oldest(0).HourlySMS(i).MO_TimeOut_SMS + _
                                    SMS_Oldest(0).HourlySMS(i).MO_Unknown_SMS
            Next i
            
            Dim Series_Hours(4) As String
            '0:Today,1:Yesterday,2:Same day before 1 week,3:same day last month
            Series_Hours(1) = "Today"
            Series_Hours(2) = "Yesterday"
            Series_Hours(3) = "Last Week"
            Series_Hours(4) = "Last Month"
            
            Call Weekly_Report(SuccessSMS, Success_SMS_Form.This_Week_Chart, Series_Hours, , True)
            Call Weekly_Report(FailedSMS, Failed_SMS_Form.This_Week_Chart, Series_Hours, , True)
            
            ReDim SuccessSMS(23, 3)
            ReDim FailedSMS(23, 3)
            
            Call Weekly_Report(SuccessSMS, Success_SMS_Form.This_Month_Chart, Series_Hours, , True)
            Call Weekly_Report(FailedSMS, Failed_SMS_Form.This_Month_Chart, Series_Hours, , True)
            
            Call Form_Title(Success_SMS_Form, "Hourly SMS       (Success)")
            Call Form_Title(Failed_SMS_Form, "Hourly SMS       (Failed)")
            Failed_SMS_Form.Show
            Success_SMS_Form.Show
            
            Exit Sub
        Case MO_VF_Mt_VF, SMS_Retrials, Inbound_Roamers
            DayOfWeek = Weekday(Date, vbSaturday)
            
            Call Generate_Data_Report(statsType, SMS, DataDate - DayOfWeek + 1, DataDate)
            Call Generate_Data_Report(statsType, SMS_Old, DataDate - DayOfWeek + 1 - 7, DataDate - DayOfWeek)
        
            ReDim SuccessSMS(6, 1)
            'ReDim FailedSMS(6, 1)
            
            Call ExtractData(FTD_Side, SuccessSMS, FailedSMS, SMS, SMS_Old)
        
            Success_SMS_Form.Show
            Failed_SMS_Form.Hide
            
            Series(1) = "This Week"
            Series(2) = "Last Week"
        
            Call Weekly_Report(SuccessSMS, Success_SMS_Form.This_Week_Chart, Series, , True)
            'Call Weekly_Report(FailedSMS, Failed_SMS_Form.This_Week_Chart, Series)
        Case Else
            DayOfWeek = Weekday(Date, vbSaturday)
            
            Call Generate_Data_Report(statsType, SMS, DataDate - DayOfWeek + 1, DataDate)
            Call Generate_Data_Report(statsType, SMS_Old, DataDate - DayOfWeek + 1 - 7, DataDate - DayOfWeek)
        
            ReDim SuccessSMS(6, 1)
            ReDim FailedSMS(6, 1)
            
            Call ExtractData(FTD_Side, SuccessSMS, FailedSMS, SMS, SMS_Old)
        
            Success_SMS_Form.Show
            Failed_SMS_Form.Show
            
            Series(1) = "This Week"
            Series(2) = "Last Week"
        
            Call Weekly_Report(SuccessSMS, Success_SMS_Form.This_Week_Chart, Series, , True)
            Call Weekly_Report(FailedSMS, Failed_SMS_Form.This_Week_Chart, Series, , True)
        End Select
    
    ReDim SuccessSMS(0, 0)
    ReDim FailedSMS(0, 0)
    
    
    Dim StartThisMonth, EndThisMonth, StartLastMonth, EndLastMonth As Date
    StartThisMonth = DateSerial(Year(Now), Month(Now), 0) + 1
    EndThisMonth = DateSerial(Year(Now), Month(Now) + 1, 0)
    StartLastMonth = DateSerial(Year(Now), Month(Now) - 1, 0) + 1
    EndLastMonth = DateSerial(Year(Now), Month(Now), 0)
    
    Call Generate_Data_Report(statsType, SMS, CDate(StartThisMonth), DataDate)
    Call Generate_Data_Report(statsType, SMS_Old, CDate(StartLastMonth), CDate(EndLastMonth))
    
    ReDim SuccessSMS(DateDiff("d", CDate(StartLastMonth), CDate(EndLastMonth), vbSaturday) + 1, 1)
    ReDim FailedSMS(DateDiff("d", CDate(StartLastMonth), CDate(EndLastMonth), vbSaturday) + 1, 1)
    
    Call ExtractData(FTD_Side, SuccessSMS, FailedSMS, SMS, SMS_Old)
    
    Series(1) = "This Month"
    Series(2) = "Last Month"
    Call Weekly_Report(SuccessSMS, Success_SMS_Form.This_Month_Chart, Series, , True)
    Call Weekly_Report(FailedSMS, Failed_SMS_Form.This_Month_Chart, Series, , True)
   ' Dim Space_ As String
   ' For i = 0 To 110
   '     Space_ = " " + Space_
   ' Next i
   '
   ' Success_SMS_Form.Caption = Space_ + StatsName(statsType) + "   (Success)"
   ' Failed_SMS_Form.Caption = Space_ + StatsName(statsType) + "   (Failed)"
     
    Call Form_Title(Success_SMS_Form, StatsName(statsType) + "   (Success)")
    Call Form_Title(Failed_SMS_Form, StatsName(statsType) + "   (Failed)")
     
    Success_SMS_Form.Hide
    Success_SMS_Form.Show
    
End Sub
Public Sub Form_Title(Form_ As Form, Title As String)
    Dim Space_ As String
    For i = 0 To 110
        Space_ = " " + Space_
    Next i
    Form_.Caption = Space_ + Title
End Sub
Private Sub ExtractData(FTD_Side As FTD_Side_Type, _
                        ByRef SuccessSMS() As String, ByRef FailedSMS() As String, _
                        SMS() As Op_SMS_Type, SMS_Old() As Op_SMS_Type, _
                        Optional HourlySMS As Boolean)
'this function merges the two SMS from SMS and SMS_Old to be easy to be plot
    Select Case FTD_Side
        Case Originating
            For i = UBound(SMS) + 1 To 1 Step -1
                SuccessSMS(i - 1, 0) = SMS(i - 1).Mo_SMS.MO_Success_SMS / 100
                FailedSMS(i - 1, 0) = SMS(i - 1).Mo_SMS.MO_Decimated_SMS + _
                                        SMS(i - 1).Mo_SMS.MO_misc_Error_SMS + _
                                        SMS(i - 1).Mo_SMS.MO_TimeOut_SMS + _
                                        SMS(i - 1).Mo_SMS.MO_Unknown_SMS
            Next i
            For i = UBound(SMS_Old) + 1 To 1 Step -1
                SuccessSMS(i - 1, 1) = SMS_Old(i - 1).Mo_SMS.MO_Success_SMS / 100
                FailedSMS(i - 1, 1) = SMS_Old(i - 1).Mo_SMS.MO_Decimated_SMS + _
                                        SMS_Old(i - 1).Mo_SMS.MO_misc_Error_SMS + _
                                        SMS_Old(i - 1).Mo_SMS.MO_TimeOut_SMS + _
                                        SMS_Old(i - 1).Mo_SMS.MO_Unknown_SMS
            Next i
        Case Terminating
            For i = UBound(SMS) + 1 To 1 Step -1
                SuccessSMS(i - 1, 0) = SMS(i - 1).Mt_SMS.Mt_Success_SMS / 100
                FailedSMS(i - 1, 0) = SMS(i - 1).Mt_SMS.Mt_Abs_Sub + _
                                        SMS(i - 1).Mt_SMS.Mt_Mem_Full + _
                                        SMS(i - 1).Mt_SMS.Mt_misc_Error_SMS + _
                                        SMS(i - 1).Mt_SMS.Mt_TimeOut_SMS
            Next i
            For i = UBound(SMS_Old) + 1 To 1 Step -1
                SuccessSMS(i - 1, 1) = SMS_Old(i - 1).Mt_SMS.Mt_Success_SMS / 100
                FailedSMS(i - 1, 1) = SMS_Old(i - 1).Mt_SMS.Mt_Abs_Sub + _
                                        SMS_Old(i - 1).Mt_SMS.Mt_Mem_Full + _
                                        SMS_Old(i - 1).Mt_SMS.Mt_misc_Error_SMS + _
                                        SMS_Old(i - 1).Mt_SMS.Mt_TimeOut_SMS
            Next i
        Case Other
            For i = UBound(SMS) + 1 To 1 Step -1
                SuccessSMS(i - 1, 0) = SMS(i - 1).Mo_SMS.MO_Success_SMS / 100
            Next i
            For i = UBound(SMS_Old) + 1 To 1 Step -1
                SuccessSMS(i - 1, 1) = SMS_Old(i - 1).Mo_SMS.MO_Success_SMS / 100
            Next i
        End Select
End Sub
