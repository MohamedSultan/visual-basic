Insert into dbo.TSP_C7_Temp2(Time, NE, InstanceID, HSN, SDL, MTP2_LINK_LOAD_SENT, MTP2_LINK_LOAD_RECEIVED, Date)
select Distinct
Time, NE, InstanceID, HSN, SDL, MTP2_LINK_LOAD_SENT, MTP2_LINK_LOAD_RECEIVED, Date
from tsp_c7_temp
where MTP2_LINK_LOAD_SENT is not NULL;


Insert into dbo.TSP_C7(Time, NE, InstanceID, HSN, SDL, MTP2_LINK_LOAD_SENT, MTP2_LINK_LOAD_RECEIVED,Time_Count)
select top 100 percent
Date, NE, InstanceID, HSN, SDL, avg(MTP2_LINK_LOAD_SENT), avg(MTP2_LINK_LOAD_RECEIVED), count(time)
from tsp_c7_temp2
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, InstanceID, HSN, SDL 
order by date, NE, InstanceID, HSN, SDL;

Insert into dbo.TSP_C7_max(Time, NE, InstanceID, HSN, SDL, MTP2_LINK_LOAD_SENT, MTP2_LINK_LOAD_RECEIVED,Time_Count)
select top 100 percent
Date, NE, InstanceID, HSN, SDL, max(MTP2_LINK_LOAD_SENT), max(MTP2_LINK_LOAD_RECEIVED), count(time)
from tsp_c7_temp
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, InstanceID, HSN, SDL 
order by date, NE, InstanceID, HSN, SDL;

Insert into dbo.TSP_C7_min(Time, NE, InstanceID, HSN, SDL, MTP2_LINK_LOAD_SENT, MTP2_LINK_LOAD_RECEIVED,Time_Count)
select top 100 percent
Date, NE, InstanceID, HSN, SDL, min(MTP2_LINK_LOAD_SENT), min(MTP2_LINK_LOAD_RECEIVED), count(time)
from tsp_c7_temp
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, InstanceID, HSN, SDL 
order by date, NE, InstanceID, HSN, SDL;

Insert into TSP_Call_Final(Time, NE, moid, Call_Attempts, Call_Setup_Released_by_Service_normal_flow, Call_Setup_Released_by_Service_exceptional_flow, Active_Call_Released_by_Service_normal_flow, Active_Call_Released_by_Service_exceptional_flow)
select top 100 percent
Date, NE, moid, avg(Call_Attempts), avg(Call_Setup_Released_by_Service_normal_flow), avg(Call_Setup_Released_by_Service_exceptional_flow), avg(Active_Call_Released_by_Service_normal_flow), avg(Active_Call_Released_by_Service_exceptional_flow)
from TSP_Call
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, moid
order by date, NE, moid;



Insert into TSP_CDR_Final(Time, NE, moid, Final_Output_CDRs, Partial_Output_CDRs, Failed_Final_Output_CDR_Invocations, Failed_Partial_Output_CDR_Invocations)
select top 100 percent
Date, NE, moid, avg(Final_Output_CDRs), avg(Partial_Output_CDRs), avg(Failed_Final_Output_CDR_Invocations), avg(Failed_Partial_Output_CDR_Invocations)
from TSP_CDR
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, moid
order by date, NE, moid;



insert into TSP_CPLoad
(Time, NE, Source, CpuLoad, MemUsed, MemFree, MemUsage, ProcessorUptime, ClusterUptime, Time_Count)
select top 100 percent
date,ne,source,avg(CpuLoad),avg(MemUsed),avg(MemFree),avg(MemUsage),avg(ProcessorUptime),avg(ClusterUptime),count(time)
from dbo.TSP_CPLoad_Temp
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by date,ne,source
order by date,ne,source;

insert into TSP_CPLoad_max
(Time, NE, Source, CpuLoad, MemUsed, MemFree, MemUsage, ProcessorUptime, ClusterUptime, Time_Count)
select top 100 percent
date,ne,source,max(CpuLoad),max(MemUsed),max(MemFree),max(MemUsage),max(ProcessorUptime),max(ClusterUptime),count(time)
from dbo.TSP_CPLoad_Temp
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by date,ne,source
order by date,ne,source;

insert into TSP_CPLoad_min
(Time, NE, Source, CpuLoad, MemUsed, MemFree, MemUsage, ProcessorUptime, ClusterUptime, Time_Count)
select top 100 percent
date,ne,source,min(CpuLoad),min(MemUsed),min(MemFree),min(MemUsage),min(ProcessorUptime),min(ClusterUptime),count(time)
from dbo.TSP_CPLoad_Temp
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by date,ne,source
order by date,ne,source;

Insert into TSP_SDP_Final(Time, NE, moid, First_Interrogations, Intermediate_Interrogations, Final_Reports, First_Interrogation_Result_no_fault, First_Interrogation_Result_fault, Intermediate_Interrogation_Result_no_fault, Intermediate_Interrogation_Result_fault, Final_Report_Result_no_fault, Final_Report_Result_fault)
select top 100 percent
Date, NE, moid, avg(First_Interrogations), avg(Intermediate_Interrogations), avg(Final_Reports), avg(First_Interrogation_Result_no_fault), avg(First_Interrogation_Result_fault), avg(Intermediate_Interrogation_Result_no_fault), avg(Intermediate_Interrogation_Result_fault), avg(Final_Report_Result_no_fault), avg(Final_Report_Result_fault)
from TSP_Sdp
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, moid
order by date, NE, moid;



Insert into TSP_SMS_Final(Time, NE, moid, SMS_Sessions, SMS_Unsuccessful_Result_normal_flow, SMS_Unsuccessful_Result_exceptional_flow)
select top 100 percent
Date, NE, moid, avg(SMS_Sessions), avg(SMS_Unsuccessful_Result_normal_flow), avg(SMS_Unsuccessful_Result_exceptional_flow)
from TSP_SMS
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, moid
order by date, NE, moid;

Insert into TSP_Voice_Final(Time, NE, moid, Subscriber_Answered, Subscriber_Busy, Subscriber_No_Answer, Subscriber_Unreachable, Route_Select_Failure)
select top 100 percent
Date, NE, moid, avg(Subscriber_Answered), avg(Subscriber_Busy),avg( Subscriber_No_Answer), avg(Subscriber_Unreachable), avg(Route_Select_Failure)
from TSP_Voice
where date >= dbo.get_First_of_day(getdate()-1)
and date <= dbo.get_last_of_day(getdate()-1)
group by Date, NE, moid
order by date, NE, moid;

Truncate table TSP_SMS;
Truncate table TSP_Voice;
Truncate table TSP_Sdp;

Truncate table TSP_Call;
Truncate table TSP_CDR;
Truncate table dbo.TSP_CPLoad_Temp;
Truncate table TSP_C7_Temp2;
Truncate table tsp_c7_temp;