CREATE VIEW Z_Average_mo_int_mt_vf AS
      SELECT 
	round(avg(MO_Success_SMS),3) MO_Success_SMS,
	round(avg(Mt_Success_SMS),3) Mt_Success_SMS,
	round(avg(MO_Decimated_SMS),3) MO_Decimated_SMS,
	round(avg(MO_misc_Error_SMS),3) MO_misc_Error_SMS,
	round(avg(MO_TimeOut_SMS),3) MO_TimeOut_SMS,
	round(avg(MO_Unknown_SMS),3) MO_Unknown_SMS,
	round(avg(Mt_Abs_Sub),3) Mt_Abs_Sub,
	round(avg(Mt_Mem_Full),3) Mt_Mem_Full,
	round(avg(Mt_misc_Error_SMS),3) Mt_misc_Error_SMS,
	round(avg(Mt_TimeOut_SMS),3) Mt_TimeOut_SMS 
FROM mo_int_mt_vf 
where ((date_format(Date_,'%Y-%m-%d') <= date_format(curdate(),'%Y-%m-%d')) 
	and (date_format(Date_,'%Y-%m-%d') >= date_format((curdate() - interval 7 day),'%Y-%m-%d'))) 
order by Date_