VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Success_SMS_Form 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8505
   ClientLeft      =   2385
   ClientTop       =   1890
   ClientWidth     =   17940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8505
   ScaleWidth      =   17940
   Begin MSChart20Lib.MSChart This_Week_Chart 
      Height          =   4215
      Left            =   120
      OleObjectBlob   =   "Success_SMS_Form.frx":0000
      TabIndex        =   0
      Top             =   120
      Width           =   17415
   End
   Begin MSComDlg.CommonDialog dlgChart 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSChart20Lib.MSChart This_Month_Chart 
      Height          =   4215
      Left            =   120
      OleObjectBlob   =   "Success_SMS_Form.frx":2354
      TabIndex        =   1
      Top             =   4200
      Width           =   17415
   End
End
Attribute VB_Name = "Success_SMS_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Call Color_Chart(Me.This_Month_Chart)
    Call Color_Chart(Me.This_Week_Chart)
End Sub


Private Sub This_Month_Chart_PointActivated(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
   With This_Month_Chart
      .Column = Series
      .Row = DataPoint
      '.Data = InputBox _
      '("Change the data point:", , .Data)
      
      'MsgBox .Data
        If This_Week_Chart.RowCount = 7 Or (This_Week_Chart.RowCount >= 28 And This_Week_Chart.RowCount <= 31) Then
            This_Month_Chart.ToolTipText = CommaSeparator(.Data * 100)
        Else
            This_Month_Chart.ToolTipText = CommaSeparator(.Data)
        End If
   End With
End Sub

Private Sub This_Month_Chart_PointSelected(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
 '  With This_Month_Chart
 '     .Column = Series
 '     .Row = DataPoint
 '     '.Data = InputBox _
 '     '("Change the data point:", , .Data)
 '
 '     'MsgBox .Data
 '     This_Month_Chart.ToolTipText = CommaSeparator(.Data * 100)
 '  End With
   This_Month_Chart.ToolTipText = ""
   Call This_Month_Chart_PointActivated(Series, DataPoint, MouseFlags, Cancel)
End Sub
Private Sub This_Week_Chart_PointActivated(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
   With This_Week_Chart
      .Column = Series
      .Row = DataPoint
      '.Data = InputBox _
      '("Change the data point:", , .Data)
      
      'MsgBox .Data
        If This_Week_Chart.RowCount = 7 Or (This_Week_Chart.RowCount >= 28 And This_Week_Chart.RowCount <= 31) Then
            This_Week_Chart.ToolTipText = CommaSeparator(.Data * 100)
        Else
            This_Week_Chart.ToolTipText = CommaSeparator(.Data)
        End If
   End With
End Sub

Private Sub This_Week_Chart_PointSelected(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
 '  With This_Week_Chart
 '     .Column = Series
 '     .Row = DataPoint
 '     '.Data = InputBox _
 '     '("Change the data point:", , .Data)
 '
 '     'MsgBox .Data
 '     This_Week_Chart.ToolTipText = CommaSeparator(.Data * 100)
 '  End With
   This_Week_Chart.ToolTipText = ""
   Call This_Week_Chart_PointActivated(Series, DataPoint, MouseFlags, Cancel)
End Sub

Private Sub This_Week_Chart_SeriesActivated(Series As Integer, MouseFlags As Integer, Cancel As Integer)
   ' The CommonDialog control is named dlgChart.
   Dim red, green, blue As Integer
   With dlgChart ' CommonDialog object
      .ShowColor
      red = RedFromRGB(.Color)
      green = GreenFromRGB(.Color)
      blue = BlueFromRGB(.Color)
   End With

   ' NOTE: Only the 2D and 3D line charts use the
   ' Pen object. All other types use the Brush.

   If This_Week_Chart.chartType <> VtChChartType2dLine Or _
   This_Week_Chart.chartType <> VtChChartType3dLine Then
      This_Week_Chart.Plot.SeriesCollection(Series). _
         DataPoints(-1).Brush.FillColor. _
         Set red, green, blue
   Else
      This_Week_Chart.Plot.SeriesCollection(Series).Pen. _
         VtColor.Set red, green, blue
   End If
End Sub

Private Sub This_Week_Chart_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
'    This_Week_Chart.Row = 3
'    This_Week_Chart.Column = 1
'    MsgBox This_Week_Chart.Data
End Sub
