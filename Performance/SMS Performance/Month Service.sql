select CONVERT(VARCHAR(10), time, 120),service,sum(NCalls) Calls,sum(nunsucc) UnSucceeded,sum(nansw) Answered
from CNP_OSS_DB..IVR_Services
where month(time)=month(getdate())-1
group by service,CONVERT(VARCHAR(10), time, 120);