/* Formatted on 2009/08/30 15:33 (Formatter Plus v4.8.8) */
SELECT   (TO_CHAR (hits_log.creation_date, 'YYYY-MM-DD')) log_date,
         hits_log.trans_type_id, hits_log.ERROR_CODE, hits_log.other_error,
         (COUNT (hits_log.msisdn)) count_
    FROM hits_log, lk_client, lk_transaction_type
   WHERE (    (hits_log.service_id = 5)
          AND (lk_client.client_id = hits_log.client_id)
          AND (lk_transaction_type.trans_type_id = hits_log.trans_type_id)
         )
GROUP BY hits_log.trans_type_id,
         hits_log.ERROR_CODE,
         hits_log.other_error,
         (TO_CHAR (hits_log.creation_date, 'YYYY-MM-DD')
         )
ORDER BY TO_CHAR (hits_log.creation_date, 'YYYY-MM-DD') DESC