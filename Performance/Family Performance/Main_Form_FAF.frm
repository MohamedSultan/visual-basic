VERSION 5.00
Begin VB.Form Main_Form_FAF 
   Caption         =   "Family Stats Collector"
   ClientHeight    =   1320
   ClientLeft      =   13020
   ClientTop       =   1365
   ClientWidth     =   5610
   LinkTopic       =   "Form1"
   ScaleHeight     =   1320
   ScaleWidth      =   5610
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "Main_Form_FAF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub Auto_Stats_Click()
    Auto_Stats.Enabled = False
    Auto_Stats.Caption = Format(Date - Val(Days), "dd/mm/yy")
    Call GetALLFAFStats(Date - Val(Days))
    Auto_Stats.Enabled = True
    Auto_Stats.Caption = "All"
End Sub

Private Sub Form_Load()
    'Call Me.Auto_Stats_Click
    'Me.Visible = False
    'End
End Sub
