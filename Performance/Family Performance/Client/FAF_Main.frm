VERSION 5.00
Begin VB.Form Family_Main_Form 
   Caption         =   "FAF statistics Graph Plotter"
   ClientHeight    =   4695
   ClientLeft      =   4200
   ClientTop       =   2460
   ClientWidth     =   4905
   LinkTopic       =   "Form1"
   ScaleHeight     =   4695
   ScaleWidth      =   4905
   Begin VB.Frame Frame3 
      Caption         =   "Stats Type"
      Height          =   1575
      Left            =   120
      TabIndex        =   16
      Top             =   1080
      Width           =   1695
      Begin VB.CheckBox System_Error_Check 
         Caption         =   "System Error"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   1215
      End
      Begin VB.CheckBox User_Error_Check 
         Caption         =   "User Error"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   720
         Width           =   1455
      End
      Begin VB.CheckBox Success_Check 
         Caption         =   "Success"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Clients"
      Height          =   1095
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   1695
      Begin VB.CheckBox Client_Check 
         Caption         =   "USSD"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   855
      End
      Begin VB.CheckBox Client_Check 
         Caption         =   "IVR"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.CommandButton Start 
      Caption         =   "Display Stats"
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   2760
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   "Transaction Types"
      Height          =   4455
      Left            =   1920
      TabIndex        =   0
      Top             =   120
      Width           =   2895
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   10
         Left            =   120
         TabIndex        =   12
         Top             =   3840
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   9
         Left            =   120
         TabIndex        =   11
         Top             =   3480
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   8
         Left            =   120
         TabIndex        =   10
         Top             =   3120
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   7
         Left            =   120
         TabIndex        =   9
         Top             =   2760
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   6
         Left            =   120
         TabIndex        =   7
         Top             =   2400
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   5
         Left            =   120
         TabIndex        =   6
         Top             =   2040
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   5
         Top             =   1680
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   4
         Top             =   1320
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Check1"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   2535
      End
      Begin VB.CheckBox TransactionTypes 
         Caption         =   "Transaction Types"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2535
      End
   End
End
Attribute VB_Name = "Family_Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim conn As ADODB.Connection
Dim Result As ADODB.Recordset



Private Sub Form_Load()
    Call DB_Open(conn, Newfamily_MySQL)
    Set Result = New ADODB.Recordset
    Dim Step As Integer
    
    Result.Open "SELECT Trans_Type from Family_Stats GROUP BY Trans_Type", conn
    
    While Not Result.EOF
        TransactionTypes(Step).Caption = Result.Fields("Trans_Type").Value
        Step = Step + 1
        Result.MoveNext
    Wend
    For i = Step - 1 To TransactionTypes.UBound
        TransactionTypes(i).Visible = False
    Next i
    Result.Close
End Sub

Private Sub Form_Terminate()
    End
End Sub

Private Sub Start_Click()
    'Building Query
    Dim Statement, Clients, Transacions, Title As String
    'Client Name
    If Client_Check(0) = 1 And Client_Check(1) = 1 Then Clients = "Client_Name='IVR' OR Client_Name='USSD' "
    If Client_Check(0) = 0 And Client_Check(1) = 1 Then Clients = "Client_Name='USSD' "
    If Client_Check(0) = 1 And Client_Check(1) = 0 Then Clients = "Client_Name='IVR' "
    If Client_Check(0) = 0 And Client_Check(1) = 0 Then Exit Sub
    Clients = "(" + Clients + ")"
    
    'Transactions
    For i = 0 To TransactionTypes.UBound
        If TransactionTypes(i) = 1 Then
            Transacions = Transacions + "OR trans_type='" + TransactionTypes(i).Caption + "' "
            Title = Title + TransactionTypes(i).Caption + "|"
        End If
    Next i
    If Transacions = "" Then Exit Sub
    Transacions = Mid(Transacions, 4, Len(Transacions) - 4)
    Title = Left(Title, Len(Title) - 1)
    
    Statement = "SELECT Day,sum(Counts),Error_Type from `db alltransactions` " + _
                "WHERE " + Transacions + " AND " + Clients + " " + _
                "GROUP BY Day,Error_Type " + _
                "ORDER BY Day ASC"
    Result.Open Statement, conn
    
    Dim Success() As String
    Dim User_Error() As String
    Dim System_Error() As String
    Dim Step As Double
    Dim DataCount As Integer
    DataCount = DateDiff("d", "03/16/09", Date) + 1
    ReDim Preserve Success(DataCount, 1)
    ReDim Preserve User_Error(DataCount, 1)
    ReDim Preserve System_Error(DataCount, 1)
    While Not Result.EOF
        Select Case Result.Fields("Error_Type").Value
            Case "Success"
                Success(Int(Step / 3), 0) = Result.Fields("day").Value
                Success(Int(Step / 3), 1) = Val(Result.Fields("sum(Counts)").Value)
            Case "User"
                User_Error(Int(Step / 3), 0) = Result.Fields("day").Value
                User_Error(Int(Step / 3), 1) = Val(Result.Fields("sum(Counts)").Value)
            Case "System"
                System_Error(Int(Step / 3), 0) = Result.Fields("day").Value
                System_Error(Int(Step / 3), 1) = Val(Result.Fields("sum(Counts)").Value)
        End Select
        Step = Step + 1
        Result.MoveNext
    Wend
    Result.Close
    
    Dim Success_Form As Success_SMS_Form
    Dim User_Error_Form As Success_SMS_Form
    Dim System_Error_Form As Success_SMS_Form
    Set Success_Form = New Success_SMS_Form
    Set User_Error_Form = New Success_SMS_Form
    Set System_Error_Form = New Success_SMS_Form
    
    Dim Series(2) As String
    'Series(0) = "This Month"
    Series(2) = "Family Stats"
    

    If System_Error_Check Then
        Call Weekly_Report(System_Error, System_Error_Form.This_Week_Chart, Series, FAF)
        Call Form_Title(System_Error_Form, Title + "   (System Error)")
        System_Error_Form.Top = System_Error_Form.Top - 1000
        System_Error_Form.Height = System_Error_Form.Height - 4200
        System_Error_Form.Show
    End If
        If User_Error_Check Then
        Call Weekly_Report(User_Error, User_Error_Form.This_Week_Chart, Series, FAF)
        Call Form_Title(User_Error_Form, Title + "   (User Error)")
        User_Error_Form.Top = User_Error_Form.Top - 500
        User_Error_Form.Height = User_Error_Form.Height - 4200
        User_Error_Form.Show
    End If
    If Success_Check Then
        Call Weekly_Report(Success, Success_Form.This_Week_Chart, Series, FAF)
        Call Form_Title(Success_Form, Title + "   (Success)")
        Success_Form.Height = Success_Form.Height - 4200
        Success_Form.Show
    End If
    
End Sub
