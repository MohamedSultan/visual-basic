VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form Main_Form 
   Caption         =   "Applications Selection"
   ClientHeight    =   2625
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5640
   LinkTopic       =   "Form1"
   ScaleHeight     =   2625
   ScaleWidth      =   5640
   StartUpPosition =   3  'Windows Default
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   375
      Left            =   4440
      Top             =   600
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   $"Main_Form.frx":0000
      OLEDBString     =   $"Main_Form.frx":0099
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton Minimizing 
      Caption         =   "_"
      Height          =   255
      Left            =   5400
      TabIndex        =   8
      Top             =   0
      Width           =   255
   End
   Begin VB.Frame Frame5 
      Height          =   855
      Left            =   1680
      TabIndex        =   5
      Top             =   120
      Width           =   2055
      Begin VB.Timer minutes 
         Interval        =   60000
         Left            =   -120
         Top             =   600
      End
      Begin VB.Timer Seconds 
         Interval        =   1000
         Left            =   1800
         Top             =   600
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Minutes : Seconds"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   120
      TabIndex        =   1
      Top             =   1440
      Width           =   5415
      Begin VB.CommandButton Cal 
         Caption         =   "Cal"
         Height          =   255
         Left            =   4920
         TabIndex        =   10
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox Days 
         Height          =   285
         Left            =   3480
         TabIndex        =   3
         Top             =   240
         Width           =   1455
      End
      Begin VB.CommandButton Auto_Stats 
         Caption         =   "All"
         Height          =   375
         Left            =   1920
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Number Of days before Today"
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.ComboBox Applications_Combo 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1080
      Width           =   2535
   End
   Begin MSForms.ToggleButton Pause 
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   360
      Width           =   1215
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      DisplayStyle    =   6
      Size            =   "2143;661"
      Value           =   "0"
      Caption         =   "Pause"
      FontHeight      =   165
      FontCharSet     =   178
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim GetDataInterval, CurrentSeconds As Integer

Private Sub Applications_Combo_Click()
    Select Case Applications_Combo.Text
        Case "FTD"
            Call HideAllForms
            Main_Form_FTD.Show
        Case "FAF"
            Call HideAllForms
            Main_Form_FAF.Show
        Case "SDP"
            Call HideAllForms
            Main_SDP.Show
    End Select
End Sub

Private Sub Auto_Stats_Click()
    Call Minimizing_Click
    Main_Form_FAF.Days = Me.Days
    Main_Form_FTD.Days = Me.Days
    Main_SDP.Days = Me.Days
    
    Call Main_Form_FTD.Auto_Stats_Click
    Call Main_Form_FAF.Auto_Stats_Click
    Call Main_SDP.Auto_Stats_Click
    
    If Val(Days) <> 0 Then
        MsgBox "Data of Date: " + Format(Date - Val(Days), "dd/mm/yy") + " Is successfully Received"
        End
    End If
End Sub

Private Sub Cal_Click()
    Calender_Form.Left = Me.Left + Me.Width
    Calender_Form.Show
End Sub

Private Sub Form_Load()
    Applications_Combo.AddItem "FAF"
    Applications_Combo.AddItem "FTD"
    Applications_Combo.AddItem "SDP"
    CurrentSeconds = 60
    Me.Icon = Main_Form_FTD.Icon
    'Call Auto_Stats_Click
End Sub
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Me.Visible Then
        Select Case X \ Screen.TwipsPerPixelX
            Case WM_MOUSEMOVE
                'List1.AddItem "MouseMove!"
            Case WM_LBUTTONDOWN
                'List1.AddItem "Left MouseDown"
            Case WM_LBUTTONUP
                'List1.AddItem "Left MouseUp"
            Case WM_LBUTTONDBLCLK
                'List1.AddItem "Left DoubleClick"
                Visible = True
                WindowState = vbNormal
                Dim hProcess As Long
                GetWindowThreadProcessId hwnd, hProcess
                AppActivate hProcess
            Case WM_RBUTTONDOWN
                'List1.AddItem "Right MouseDown"
            Case WM_RBUTTONUP
                'List1.AddItem "Right MouseUp"
            Case WM_RBUTTONDBLCLK
                'List1.AddItem "Right DoubleClick"
        End Select
    End If
End Sub
Private Sub Form_Terminate()
    End
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Form_Terminate
End Sub

Private Sub Minimizing_Click()
    Call Minimizing_Form(Me)
End Sub

Private Sub minutes_Timer()
    GetDataInterval = GetDataInterval + 1
    If GetDataInterval = 20 Then
        Days = ""
        Call Auto_Stats_Click
        GetDataInterval = 0
    End If
End Sub

Private Sub Pause_Click()
    If Pause.Value = True Then
        Seconds.Enabled = False
        Pause.Caption = "Resume"
    Else
        Seconds.Enabled = True
        Pause.Caption = "Pause"
    End If
End Sub

Private Sub Seconds_Timer()
    CurrentSeconds = CurrentSeconds - 1
    If CurrentSeconds = -1 Then CurrentSeconds = 59
    Label3 = Str(19 - GetDataInterval) + " :   " + Str(CurrentSeconds)
End Sub


