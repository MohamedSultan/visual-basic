Attribute VB_Name = "Functions"
Public Sub Minimizing_Form(Form_ As Form)
    'Call Cope_All("Minimized")
    Form_.WindowState = 1
    Minimized = True

    Dim i As Integer
    Dim s As String
    Dim nid As NOTIFYICONDATA

    s = "SMS Stats Collector"
    nid = setNOTIFYICONDATA(Form_.hwnd, vbNull, NIF_MESSAGE Or NIF_ICON Or NIF_TIP, WM_MOUSEMOVE, Form_.Icon, s)
    i = Shell_NotifyIconA(NIM_ADD, nid)

    Form_.WindowState = vbMinimized
    Form_.Visible = False
End Sub
Public Sub HideAllForms()
    For i = 0 To Forms.Count - 1
        If Forms(i).Name <> "Main_Form" Then Forms(i).Hide
    Next i
End Sub

Public Sub Color_Chart(ByRef Chart As MSChart)
    For i = 1 To Chart.RowCount
        Chart.Row = i
        Chart.RowLabel = Str(i - 1)
    Next i
    With Chart.Plot.Backdrop
      ' No color will appear unless you set the style
      ' property to VtFillStyleBrush.
      .Fill.Style = VtFillStyleBrush
      .Fill.Brush.FillColor.Set 100, 255, 200
      ' Add a border.
      .Frame.Style = VtFrameStyleThickInner
      ' Set style to show a shadow.
      .Shadow.Style = VtShadowStyleDrop
   End With
   ' Color the wall of the plot yellow.
   With Chart.Plot
      ' Set the style to solid.
      .Wall.Brush.Style = VtBrushStyleSolid
      ' Set the color to yellow.
      .Wall.Brush.FillColor.Set 255, 255, 0
   End With
   With Chart.Plot ' Color the plot base blue.
      .PlotBase.BaseHeight = 200
      .PlotBase.Brush.Style = VtBrushStyleSolid
      .PlotBase.Brush.FillColor.Set 0, 0, 255
   End With
End Sub
Public Function DISPLAY_Data_Chart(Chart As MSChart, ByRef Data() As Double, ByRef SeriesNames() As String, _
                                    Optional Title As String, Optional ShowLegend As Boolean, _
                                    Optional DisplayDiffs As Boolean, Optional Form_ As Form) As Double
    Dim Total As Double
    With Chart
        .ChartData = Data
        For i = 1 To .Plot.SeriesCollection.Count
            .Plot.SeriesCollection(i).LegendText = SeriesNames(i)
        Next i
        .ShowLegend = ShowLegend
        For i = 1 To .RowCount
            .Row = i
            .RowLabel = Str(i - 1)
        Next i
    End With
    If DisplayDiffs Then
        For i = 0 To Chart.RowCount - 1
            If Hour(Time) > i Then
                Form_.Arrow(i) = i
                If Max(Data(i, 0), Data(i, 1)) >= Data(i, 2) Then 'And Not Hour(Time) < i Then
                    Form_.Arrow(i).BackColor = &HFF00&  'Green
                Else
                    Form_.Arrow(i).BackColor = &HFF&    'Red
                End If
            End If
        Next i
    End If
    If Title <> "" Then Chart.TitleText = Title
    Chart.Title.Location.LocationType = VtChLocationTypeBottom

End Function
Public Function Max(Number1 As Double, Number2 As Double) As Double
    If Number1 > Number2 Then
        Max = Number1
    Else
        Max = Number2
    End If
End Function
Public Function CommaSeparator(ByVal Number As Double) As String
    Dim step As Integer
    Dim LongNumber, CommaSeparator_Reversed As String
    LongNumber = Trim(Str(Number))
    
    For i = Len(LongNumber) To 1 Step -1
        If step Mod 3 = 0 Then
            CommaSeparator_Reversed = CommaSeparator_Reversed + "," + Mid(LongNumber, i, 1)
        Else
            CommaSeparator_Reversed = CommaSeparator_Reversed + Mid(LongNumber, i, 1)
        End If
        step = step + 1
    Next i
    
    For i = 2 To Len(CommaSeparator_Reversed)
        CommaSeparator = Mid(CommaSeparator_Reversed, i, 1) + CommaSeparator
    Next i
    CommaSeparator = Trim(CommaSeparator)
End Function
Public Sub Add_Status(New_Data As String)
    With Main_Form_FTD.Status
        Main_Form_FTD.Temp_List.Clear
        Main_Form_FTD.Temp_List.AddItem New_Data, 0
        If .ListCount <> 0 Then
            For i = 0 To Main_Form_FTD.Status.ListCount - 1
                Main_Form_FTD.Temp_List.AddItem .List(i)
            Next i
        End If
        .Clear
        For i = 0 To Main_Form_FTD.Temp_List.ListCount - 1
            .AddItem Main_Form_FTD.Temp_List.List(i)
        Next i
    End With
End Sub


Public Function RedFromRGB(ByVal rgb As Long) As Integer
   ' The ampersand after &HFF coerces the number as a
   ' long, preventing Visual Basic from evaluating the
   ' number as a negative value. The logical And is
   ' used to return bit values.
   RedFromRGB = &HFF& And rgb
End Function

Public Function GreenFromRGB(ByVal rgb As Long) As Integer
   ' The result of the And operation is divided by
   ' 256, to return the value of the middle bytes.
   ' Note the use of the Integer divisor.
   GreenFromRGB = (&HFF00& And rgb) \ 256
End Function

Public Function BlueFromRGB(ByVal rgb As Long) As Integer
   ' This function works like the GreenFromRGB above,
   ' except you don't need the ampersand. The
   ' number is already a long. The result divided by
   ' 65536 to obtain the highest bytes.
   BlueFromRGB = (&HFF0000 And rgb) \ 65536
End Function

Public Sub AddSeries(ByRef Series() As String, ByVal Data As String)
    ReDim Preserve Series(UBound(Series) + 1)
    Series(UBound(Series)) = Data
End Sub
Public Sub CopyArray(ByRef BigArray() As Double, ByVal NewDataID As Integer, ByRef Small_Array() As Double)
    For i = 0 To 23
        BigArray(i, NewDataID) = Small_Array(i)
    Next i
End Sub
