VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Temp 
   Caption         =   "Form1"
   ClientHeight    =   10695
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13110
   LinkTopic       =   "Form1"
   ScaleHeight     =   10695
   ScaleWidth      =   13110
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Test 
      Caption         =   "TEst"
      Height          =   375
      Left            =   11400
      TabIndex        =   1
      Top             =   240
      Width           =   1335
   End
   Begin MSComDlg.CommonDialog dlgChart 
      Left            =   240
      Top             =   2520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSChart20Lib.MSChart Messages_Chart 
      Height          =   10095
      Left            =   720
      OleObjectBlob   =   "Temp.frx":0000
      TabIndex        =   0
      Top             =   240
      Width           =   11655
   End
End
Attribute VB_Name = "Temp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Call Color_Chart(Me.Messages_Chart)
End Sub

Private Sub Messages_Chart_PointSelected(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
   With Messages_Chart
     .Column = Series
      .Row = DataPoint
      .Data = InputBox _
      ("Change the data point:", , .Data)
    End With
End Sub
Private Sub Messages_Chart_SeriesActivated(Series As Integer, MouseFlags As Integer, Cancel As Integer)
   ' The CommonDialog control is named dlgChart.
   Dim red, green, blue As Integer
   With dlgChart ' CommonDialog object
      .ShowColor
      red = RedFromRGB(.Color)
      green = GreenFromRGB(.Color)
      blue = BlueFromRGB(.Color)
   End With

   ' NOTE: Only the 2D and 3D line charts use the
   ' Pen object. All other types use the Brush.

   If Messages_Chart.chartType <> VtChChartType2dLine Or _
   Messages_Chart.chartType <> VtChChartType3dLine Then
      Messages_Chart.Plot.SeriesCollection(Series). _
         DataPoints(-1).Brush.FillColor. _
         Set red, green, blue
   Else
      Messages_Chart.Plot.SeriesCollection(Series).Pen. _
         VtColor.Set red, green, blue
   End If
End Sub

Private Sub Test_Click()
    With Messages_Chart
      ' Displays a 3d chart with 8 columns and 8 rows
      ' data.
      .chartType = VtChChartType3dBar
      .ColumnCount = 8
      .RowCount = 8
      For Column = 1 To 8
         For Row = 1 To 8
            .Column = Column
            .Row = Row
            .Data = Row * 10
         Next Row
      Next Column
      ' Use the chart as the backdrop of the legend.
      .ShowLegend = True
      .SelectPart VtChPartTypePlot, index1, index2, _
      index3, index4
      .EditCopy
      .SelectPart VtChPartTypeLegend, index1, _
      index2, index3, index4
      .EditPaste
   End With

End Sub

