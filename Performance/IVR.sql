--Max Utilization
select Date_Time,terminating_node, route_name,max_utilization,MHT,ASR,Blocking,BHCA_Calls,IVR_Switch 
from DC_IVR_PORT_UTILIZATION
where to_char(date_time,'YYYY-MM-DD') = '2009-05-06' and terminating_node like 'Nortel%' and max_utilization >= 75
order by max_utilization desc;

--Blocking
select Date_Time,terminating_node, route_name,max_utilization,MHT,ASR,Blocking,BHCA_Calls,IVR_Switch 
from DC_IVR_PORT_UTILIZATION
where to_char(date_time,'YYYY-MM-DD') = '2009-05-03' and terminating_node like 'Nortel%' and Blocking >= 40
order by Blocking desc;