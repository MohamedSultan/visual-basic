Attribute VB_Name = "Parser_module"
Public Type Parser_Return
    Ret As Variant
    found As Boolean
End Type
Public Function Parser(ByVal Whole_String As String, ByVal Delimiter As String) As Parser_Return
    Dim Vars() As String
    Dim Locations(), Step, Loc As Integer
    
    
    Whole_String = Replace(Whole_String, Delimiter, ";")
    Loc = 1
    'Finding Number Of Vars
        While Loc <> 0
            Loc = InStr(Loc + 1, Whole_String, ";")
            ReDim Preserve Locations(Step)
            Locations(Step) = Loc
            Step = Step + 1
        Wend
    
    Locations(UBound(Locations)) = Len(Whole_String)
    ReDim Preserve Vars(Step - 1)
    
    If Step > 0 Then
        Parser.found = True
        Vars(0) = Mid(Whole_String, 1, Locations(0) - 1)
        For i = 1 To UBound(Vars) - 1
            If Locations(i - 1) <> Locations(i) Then Vars(i) = Mid(Whole_String, Locations(i - 1) + Len(";"), Locations(i) - Locations(i - 1) - 1)
        Next i
    End If
    Parser.Ret = Vars
    
End Function
