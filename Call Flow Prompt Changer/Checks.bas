Attribute VB_Name = "Checks"



Public Sub N1_Checks()
'----------------N1 Checks----------------------------------------------------------

   'N1 Counts
   If UBound(N1) <> N1(0) * 3 Then
        Call Main_Form.Status("Checking N1 Count", True, "N1 Count is inconsitence")
    Else
        Call Main_Form.Status("Checking N1 Count", False)
        Step = 0
        'Setting PCs
        For i = 2 To UBound(N1)
            If (ID_Space(N1(i)) = "252" Or ID_Space(N1(i)) = "11") And InStr(1, N1(i - 1), "INAP") = 0 Then
                Step = Step + 1
            End If
        Next i
        ReDim Preserve PCs_N1(Step - 1, 1)
        Step = 0
        For i = 2 To UBound(N1)
            If Trim(ID_Space(N1(i))) = "252" Or ID_Space(N1(i)) = "11" And InStr(1, N1(i - 1), "INAP") = 0 Then
                PCs_N1(Step, 0) = ID_Space(N1(i - 1))
                Step = Step + 1
            End If
        Next i
        
        'N1 Formats
         Main_Form.Check_Display.AddItem "Checking N1 Contents"
         Main_Form.Check_Display.AddItem "-------------------------------------------"
        For i = 1 To UBound(N1) Step 3
            If ID_Space(N1(i)) <> "1" Then
                Call Main_Form.Status("checking Contents indicator ID:" + Str(Int(i / 3) + 1), True, "Not Found")
            Else
                Call Main_Form.Status("Checking Contents indicator ID:" + Str(Int(i / 3) + 1), False)
            End If
            If InStr(1, UCase(N1(i + 1)), "SPC") = 0 Then
                Call Main_Form.Status("Checking SPC Existance, ID:" + Str(Int(i / 3) + 1), True, "Not Found")
            Else
                Call Main_Form.Status("Checking SPC Existance, ID:" + Str(Int(i / 3) + 1), False, "Not Found")
            End If
            If InStr(1, UCase(N1(i + 2)), "SSN") = 0 Then
                Call Main_Form.Status("Checking SSN Existance, ID:" + Str(Int(i / 3) + 1), True, "Not Found")
            Else
                Call Main_Form.Status("Checking SSN Existance, ID:" + Str(Int(i / 3) + 1), False, "Not Found")
            End If
        Next i
    End If
End Sub

Public Sub N2_Checks()
'----------------N2---------------------------------------------------------------------
    'getting PCs in N2
    Step = 0
    For i = 2 To UBound(N2)
        If (Trim(ID_Space(N2(i)) = "252" Or ID_Space(N2(i)) = "11") And InStr(1, N2(i - 1), "INAP") = 0) Then
            Step = Step + 1
        End If
    Next i
    ReDim Preserve PCs_N2(Step - 1, 1)
    Step = 0
    For i = 2 To UBound(N2)
        If (ID_Space(N2(i)) = "252" Or ID_Space(N2(i)) = "11") And InStr(1, N2(i - 1), "INAP") = 0 And InStr(1, N2(i - 1), "USSD") = 0 Then
            PCs_N2(Step, 0) = ID_Space(N2(i - 1))
            Step = Step + 1
        End If
    Next i
    
    'Checking N2 PCs with PCs in N1
     Main_Form.Check_Display.AddItem "---------------------------------"
     Main_Form.Check_Display.AddItem "N2 Point Codes"
     Main_Form.Check_Display.AddItem "---------------------------------"
    
    For i = 0 To UBound(PCs_N2)
        found = False
        For j = 0 To UBound(PCs_N1)
            If PCs_N2(i, 0) = PCs_N1(j, 0) Then found = True
        Next j
        If Not found Then
            Call Main_Form.Status("Checking PC in N2 With PCs in N1, ID:" + PCs_N2(i, 0), True, "Not Found")
        Else
            Call Main_Form.Status("Checking PC in N2 With PCs in N1, ID:" + PCs_N2(i, 0), False, "Not Found")
        End If
    Next i
    
    'Checking N1 PCs with PCs in N2
     Main_Form.Check_Display.AddItem "---------------------------------"
     Main_Form.Check_Display.AddItem "N1 PCs and N2 PCs"
     Main_Form.Check_Display.AddItem "---------------------------------"
    
    For i = 0 To UBound(PCs_N1)
        found = False
        For j = 0 To UBound(PCs_N2)
            If PCs_N1(i, 0) = PCs_N2(j, 0) Then found = True
        Next j
        If Not found Then
            Call Main_Form.Status("Checking PC in N1 With PCs in N2, PC:" + PCs_N1(i, 0), True, "Not Found")
        Else
            Call Main_Form.Status("Checking PC in N1 With PCs in N2, PC:" + PCs_N1(i, 0), False, "Not Found")
        End If
            
    Next i
    
    'N2 Formats
     Main_Form.Check_Display.AddItem "-------------------------------------------"
     Main_Form.Check_Display.AddItem "Checking N2 Contents"
     Main_Form.Check_Display.AddItem "-------------------------------------------"
    
    If UBound(N2) <> N2(0) * 4 Then
        Call Main_Form.Status("Checking N2 Count", True, "N2 Count is inconsitence")
    Else
        Call Main_Form.Status("Checking N2 Count", False, "N2 Count is inconsitence")
    End If
    
    For i = 1 To UBound(N2) Step 4
        If ID_Space(N2(i + 0)) <> "1" Then
            Call Main_Form.Status("Checking Contents indicator ID:" + Str(Int(i / 4) + 1), True, "Not Found")
        Else
            Call Main_Form.Status("Checking Contents indicator ID:" + Str(Int(i / 4) + 1), False, "Not Found")
        End If
        If Left(N2(i + 1), Len("4,0,1,4,")) <> "4,0,1,4," And InStr(1, N2(i + 2), "INAP") = 0 Then
            Call Main_Form.Status("Checking 4,0,1,4, Existance, PC:" + ID_Space(N2(i + 2)) + ", ID:" + Str(Int(i / 4) + 1), True, "Not Found")
        Else
            Call Main_Form.Status("Checking 4,0,1,4, Existance, PC:" + ID_Space(N2(i + 2)) + ", ID:" + Str(Int(i / 4) + 1), False, "Not Found")
        End If
        If InStr(1, UCase(N2(i + 2)), "SPC") = 0 Then
            Call Main_Form.Status("Checking SPC Existance, PC:" + ID_Space(N2(i + 2)) + ", ID:" + Str(Int(i / 4) + 1), True, "Not Found")
        Else
            Call Main_Form.Status("Checking SPC Existance, PC:" + ID_Space(N2(i + 2)) + ", ID:" + Str(Int(i / 4) + 1), False, "Not Found")
        End If
        If InStr(1, UCase(N2(i + 3)), "SSN") = 0 Then
            Call Main_Form.Status("Checking SSN Existance, PC:" + ID_Space(N2(i + 2)) + ", ID:" + Str(Int(i / 4) + 1), True, "Not Found")
        Else
            Call Main_Form.Status("Checking SSN Existance, PC:" + ID_Space(N2(i + 2)) + ", ID:" + Str(Int(i / 4) + 1), False, "Not Found")
        End If
    Next i
End Sub
Public Sub LinkSets_Checks()
'------------------------Link set checks-----------------------------------------------------------------------------
        'checking number of linkssets
        
        Dim Number_of_LinkSets, SDL_Step As Integer
        Dim Temp As String
        Dim SDL(64) As String
        Dim LinkSets_Locations() As Integer
        
        'Getting number of LinkSets and the SDLs
        For i = 1 To UBound(LinkSets)
            If InStr(1, LCase(LinkSets(i)), "linkset no") <> 0 Then
                Number_of_LinkSets = Number_of_LinkSets + 1
                ReDim Preserve LinkSets_Locations(Number_of_LinkSets)
                LinkSets_Locations(Number_of_LinkSets) = i
            End If
            If InStr(1, UCase(LinkSets(i)), "SDL") <> 0 Then
                SDL_Step = SDL_Step + 1
                SDL(SDL_Step) = Str(ID_Space(LinkSets(i)))
            End If
        Next i
        
        Dim SDL_Sorted As Variant
        SDL_Sorted = Sort(SDL)
        
        'Check Duplicate SDL
         Main_Form.Check_Display.AddItem "---------------------------------------------------"
         Main_Form.Check_Display.AddItem "Checking SDL Duplication"
        found = False
        For i = 0 To 64 - 1
            If SDL(i) = SDL(i + 1) And SDL(i) <> "" Then
                Call Main_Form.Status("Checking SDL Duplication", True, "SDL:" + Str(i) + " is Duplicated")
                found = True
            Else
                'Call main_form.status("Checking SDL Duplication", False, "SDL:" + Str(i) + " is Duplicated")
            End If
        Next i
        If Not found Then Main_Form.Check_Display.AddItem "---------------------Check Passed"
        
         'Checking SDLs
         Main_Form.Check_Display.AddItem "---------------------------------------------------"
         Main_Form.Check_Display.AddItem "Checking SDLs"
         Main_Form.Check_Display.AddItem "---------------------------------------------------"
        
        If Val(LinkSets(0)) <> Number_of_LinkSets Then
            Call Main_Form.Status("Checking Count Of LinkSets", True, "LinkSet Count is Inconsitence")
        Else
            Call Main_Form.Status("Checking Count Of LinkSets", False, "LinkSet Count is Inconsitence")
        End If

         Main_Form.Check_Display.AddItem "---------------------------------------------------"
         Main_Form.Check_Display.AddItem "Checking LinkSet Contents"
         Main_Form.Check_Display.AddItem "---------------------------------------------------"
        
        'Separating Each Link
        Dim Pointer, Number_of_LinkSets_In_SLC, CurrentLinkSet_Number, Current_SDL, Current_SLC, Current_Link As Integer
                
        Pointer = 1
        While Pointer <= UBound(LinkSets)
            If InStr(1, LCase(LinkSets(Pointer)), "linkset no") <> 0 Then     'New Link Set
                
                Call Main_Form.Status("Checking linkset no", False, "Error data exceeding in Link Set Identification")
                
                Current_SLC = 0
                CurrentLinkSet_Number = CurrentLinkSet_Number + 1
                
                Main_Form.Check_Display.AddItem "---------------------------------------------------"
                Main_Form.Check_Display.AddItem "Checking LinkSet ID" + Str(CurrentLinkSet_Number)
                Main_Form.Check_Display.AddItem "---------------------------------------------------"
         
                If CurrentLinkSet_Number <> Int(Val(ID_Space(LinkSets(Pointer)))) Then
                    Call Main_Form.Status("Checking Current LinkSet Number Value, ID:" + ID_Space(LinkSets(Pointer)), True, "Current LinkSet Number is Inconsitence")
                Else
                    Call Main_Form.Status("Checking Current LinkSet Number Value, ID:" + ID_Space(LinkSets(Pointer)), False, "Current LinkSet Number is Inconsitence")
                End If
                
                Number_of_LinkSets_In_SLC = ID_Space(LinkSets(Pointer + 1))
                If InStr(1, UCase(LinkSets(Pointer + 2)), "ADJACENT DPC") = 0 Then
                    Call Main_Form.Status("Checking DPC Existence, ID:" + ID_Space(LinkSets(Pointer)), True, "Error Adjacent DPC")
                Else
                    Call Main_Form.Status("Checking DPC Existence, ID:" + ID_Space(LinkSets(Pointer)), False, "Error Adjacent DPC")
                End If
                
                'Checking DPC in  N1 PCs
                found = False
                For j = 0 To UBound(PCs_N1)
                    If PCs_N1(j, 0) = Trim(ID_Space(LinkSets(Pointer + 2))) Then
                        found = True
                        PCs_N1(j, 1) = Trim(ID_Space(LinkSets(Pointer + 0)))
                    End If
                Next j
                If Not found Then
                    Call Main_Form.Status("Checking DPC:" + Trim(ID_Space(LinkSets(Pointer + 2))) + " in N1 PCs", True, "DPC is not Found")
                Else
                    Call Main_Form.Status("Checking DPC:" + Trim(ID_Space(LinkSets(Pointer + 2))) + " in N1 PCs", False, "DPC is not Found")
                End If
                
                'Checking DPC in N2 PCs
                found = False
                For j = 0 To UBound(PCs_N2)
                    If PCs_N2(j, 0) = Trim(ID_Space(LinkSets(Pointer + 2))) Then
                        found = True
                        PCs_N2(j, 1) = Trim(ID_Space(LinkSets(Pointer + 0)))
                    End If
                Next j
                If Not found Then
                    Call Main_Form.Status("Checking DPC:" + Trim(ID_Space(LinkSets(Pointer + 2))) + " in N2 PCs", True, "DPC is not Found")
                Else
                    Call Main_Form.Status("Checking DPC:" + Trim(ID_Space(LinkSets(Pointer + 2))) + " in N2 PCs", False, "DPC is not Found")
                End If
                
                'cheking link class-----------------------------------------------------------------------
                If InStr(1, LCase(LinkSets(Pointer + 3)), "link class (narrowband)") = 0 And InStr(1, LCase(LinkSets(Pointer + 3)), "link class(narrowband)") = 0 Then
                    Call Main_Form.Status("Checking Link Class Existance", True, "Not Exist")
                Else
                    Call Main_Form.Status("Checking Link Class Existance", False, "Not Exist")
                End If
               If ID_Space(LinkSets(Pointer + 3)) <> "0" Then
                    Call Main_Form.Status("Checking Link Class Value", True, "Wrong Value")
                Else
                    Call Main_Form.Status("Checking Link Class Value", False, "Wrong Value")
                End If
                'linkset behaviour mask-----------------------------------------------------------------------
                If InStr(1, LCase(LinkSets(Pointer + 4)), "linkset behaviour mask") = 0 Then
                    Call Main_Form.Status("Checking Linkset behaviour mask Existance", True, "Not Found")
                Else
                    Call Main_Form.Status("Checking Linkset behaviour mask Existance", False, "Not Found")
                End If
                If ID_Space(LinkSets(Pointer + 4)) <> "0" Then
                    Call Main_Form.Status("Checking Linkset behaviour mask value", True, "Wrong Value")
                Else
                    Call Main_Form.Status("Checking Linkset behaviour mask Value", False, "Wrong Value")
                End If
                '-----------------------------------------------------------------------
                If Pointer <> LinkSets_Locations(CurrentLinkSet_Number) Then
                    Call Main_Form.Status("Checking Linkset No Location", True, "Data Inconsistance")
                Else
                    Call Main_Form.Status("Checking Linkset No Location", False, "Data Inconsistance")
                End If
                    Current_Link = 0
                    'Checking SDL in the current LinkSet
                    For i = Pointer + 5 To (Number_of_LinkSets_In_SLC * 3) + Pointer + 5 - 1 Step 3   '3  in each SDL
                        Current_SDL = Current_SDL + 1
                        Current_Link = Current_Link + 1
                        'Checking SDL Serial----------------------------------------------------------
                        If Trim(ID_Space(LinkSets(i + 2))) <> Trim(SDL(Current_SDL)) Then
                            Call Main_Form.Status("Checking SDL Serial, link ID:" + Str(Current_Link), True, "Error is SDL Number")
                        Else
                            Call Main_Form.Status("Checking SDL Serial, link ID:" + Str(Current_Link), False, "Error is SDL Number")
                        End If
                        'SLC Serial--------------------------------------------------------------------
                        If ID_Space(LinkSets(i)) <> Trim(Str(Current_SLC)) Then
                            Call Main_Form.Status("Checking SLC Serial, link ID:" + Str(Current_Link), True, "Current SLC not in Serial")
                        Else
                            Call Main_Form.Status("Checking SLC Serial, link ID:" + Str(Current_Link), False, "Current SLC not in Serial")
                        End If
                        'HSN Existance---------------------------------------------------------------------
                        If InStr(1, LinkSets(i + 1), "HSN") = 0 Then
                            Call Main_Form.Status("Checking HSN Existance, link ID:" + Str(Current_Link), True, "Not Exist")
                        Else
                            Call Main_Form.Status("Checking HSN Existance, link ID:" + Str(Current_Link), False, "Not Exist")
                        End If
                        If ID_Space(LinkSets(i + 1)) <> "0" Then
                            Call Main_Form.Status("Checking HSN Value, link ID:" + Str(Current_Link), True, "Wrong Value")
                        Else
                            Call Main_Form.Status("Checking HSN Value, link ID:" + Str(Current_Link), False, "Wrong Value")
                        End If
                        
                        Current_SLC = Current_SLC + 1
                    Next i
            Else
                Call Main_Form.Status("Checking linkset no", True, "Error data exceeding in Link Set Identification")
            End If
            Pointer = Pointer + (Number_of_LinkSets_In_SLC * 3) + 5 - 1 + 1
            'link(Number_of_LinkSets_In_SLC)=
        Wend
        
        'Getting DPCs in Linksets
        ReDim Preserve PCs_Link_Sets(LinkSets(0), 1)
        For i = 1 To UBound(LinkSets_Locations)
            PCs_Link_Sets(i, 0) = ID_Space(LinkSets(LinkSets_Locations(i) + 2))
        Next i
        
End Sub

Public Sub Route_Checks()
'Checking Routes---------------------------------------------------------------------------------------------
    If UBound(Routes) <> Routes(0) * 5 Then
        Call Main_Form.Status("Checking Number Of Routes", True, "Error in Number of Routes")
    Else
        Call Main_Form.Status("Checking Number Of Routes", False, "Error in Number of Routes")
    End If
    
    For i = 1 To UBound(Routes) Step 5
        
        Main_Form.Check_Display.AddItem "---------------------------------------------------"
        Main_Form.Check_Display.AddItem "Checking Route ID" + Str(Int(i / 5) + 1)
        Main_Form.Check_Display.AddItem "---------------------------------------------------"
                
        'Route ID-------------------------------------------------------------------------------------------
        If InStr(1, LCase(Routes(i + 0)), "route no") = 0 Then
            Call Main_Form.Status("Checking Route No", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Route No", False, "Not Found")
        End If
        'ATM/TDM--------------------------------------------------------------------------------------------
        If InStr(1, UCase(Routes(i + 1)), "ATM/TDM") = 0 And _
                InStr(1, UCase(Routes(i + 1)), "TDM") = 0 And _
                InStr(1, UCase(Routes(i + 1)), "ATM") = 0 Then
            Call Main_Form.Status("Checking ATM/TDM", True, "Not Found")
        Else
           Call Main_Form.Status("Checking ATM/TDM", False, "Not Found")
        End If
        If ID_Space(Routes(i + 1)) <> "0" Then
            Call Main_Form.Status("Checking ATM/TDM Value", True, "Not Equal to 0")
        Else
           Call Main_Form.Status("Checking ATM/TDM Value", False, "Not Equal to 0")
        End If
        'LinkSet ID-------------------------------------------------------------------------------------------
        If InStr(1, LCase(Routes(i + 2)), "linkset") = 0 Then
            Call Main_Form.Status("Checking Linkset", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Linkset", False, "Not Found")
        End If
        'Low Link-------------------------------------------------------------------------------------------
        If InStr(1, LCase(Routes(i + 3)), "low link availability x%") = 0 Then
            Call Main_Form.Status("Checking Low Link Availability X%", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Low Link Availability X%", False, "Not Found")
        End If
        If ID_Space(Routes(i + 3)) <> "35" Then
            Call Main_Form.Status("Checking Low Link Value", True, "Not Equal to 35")
        Else
           Call Main_Form.Status("Checking Low Link Value", False, "Not Equal to 35")
        End If
        'Normal Link-------------------------------------------------------------------------------------------
        If InStr(1, LCase(Routes(i + 4)), "normal link availability y%") = 0 Then
            Call Main_Form.Status("Checking Normal Link Availability Y%", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Normal Link Availability Y%", False, "Not Found")
        End If
        If ID_Space(Routes(i + 4)) <> "45" Then
            Call Main_Form.Status("Checking Normal Link Value", True, "Not Equal to 45")
        Else
           Call Main_Form.Status("Checking Normal Link Value", False, "Not Equal to 45")
        End If
        'Route ID and LinkSet-------------------------------------------------------------------------------------------
        If Trim(ID_Space(Routes(i + 2))) <> Trim(ID_Space(Routes(i + 0))) Then
            Call Main_Form.Status("Checking Route Number and LinkSet ID consistancy", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Route Number and LinkSet ID consistancy", False, "Not Found")
        End If
    Next i
End Sub
Public Sub RouteSets_Checks()
    
'Checking RouteSets-------------------------------------------------------------------------------------------
    If UBound(RouteSets) <> RouteSets(0) * 5 Then
        Call Main_Form.Status("Checking Number Of RouteSets", True, "Error in RouteSets Number")
    Else
        Call Main_Form.Status("Checking Number Of RouteSets", False, "Error in RouteSets Number")
    End If
    
    ReDim Preserve PCs_Route_Sets(RouteSets(0), 1)
    
    For i = 1 To UBound(RouteSets) Step 5
        
        Main_Form.Check_Display.AddItem "---------------------------------------------------"
        Main_Form.Check_Display.AddItem "Checking Routeset ID" + Str(Int(i / 5) + 1)
        Main_Form.Check_Display.AddItem "---------------------------------------------------"
                
        'Automatic Rerouting Flag--------------------------------------------------------------------------------------------
        If InStr(1, LCase(RouteSets(i + 1)), "automatic rerouting flag") = 0 Then
            Call Main_Form.Status("Checking Automatic Rerouting Flag", True, "Not Found")
        Else
           Call Main_Form.Status("Checking Automatic Rerouting Flag", False, "Not Found")
        End If
        If ID_Space(RouteSets(i + 1)) <> "0" Then
            Call Main_Form.Status("Checking Automatic Rerouting Flag Value", True, "Not Equal to 0")
        Else
           Call Main_Form.Status("Checking Automatic Rerouting Flag Value", False, "Not Equal to 0")
        End If
        'DPC-------------------------------------------------------------------------------------------
        If InStr(1, UCase(RouteSets(i)), "DPC") = 0 Then
            Call Main_Form.Status("Checking DPC", True, "Not Found")
        Else
            Call Main_Form.Status("Checking DPC", False, "Not Found")
            PCs_Route_Sets(Int(i / 5), 0) = ID_Space(RouteSets(i))
        End If
            'Checking DPC in  N1 PCs
            found = False
            For j = 0 To UBound(PCs_N1)
                If PCs_N1(j, 0) = PCs_Route_Sets(i / 5, 0) Then
                    found = True
                End If
            Next j
            If Not found Then
                Call Main_Form.Status("Checking DPC:" + PCs_Route_Sets(i / 5, 0) + " in N1 PCs", True, "DPC is not Found")
            Else
                Call Main_Form.Status("Checking DPC:" + PCs_Route_Sets(i / 5, 0) + " in N1 PCs", False, "DPC is not Found")
            End If
            
            'Checking DPC in N2 PCs
            found = False
            For j = 0 To UBound(PCs_N2)
                If PCs_N2(j, 0) = PCs_Route_Sets(i / 5, 0) Then
                    found = True
                End If
            Next j
            If Not found Then
                Call Main_Form.Status("Checking DPC:" + PCs_Route_Sets(i / 5, 0) + " in N2 PCs", True, "DPC is not Found")
            Else
                Call Main_Form.Status("Checking DPC:" + PCs_Route_Sets(i / 5, 0) + " in N2 PCs", False, "DPC is not Found")
            End If
            
             'Checking DPC in LinkSets PCs
            found = False
            For j = 0 To UBound(PCs_Link_Sets)
                If PCs_Link_Sets(j, 0) = PCs_Route_Sets(i / 5, 0) Then
                    found = True
                    PCs_Route_Sets(i / 5, 1) = Trim(Str(j))
                End If
            Next j
            If Not found Then
                Call Main_Form.Status("Checking DPC:" + Trim(ID_Space(RouteSets(i))) + " in LinkSet PCs", True, "DPC is not Found")
            Else
                Call Main_Form.Status("Checking DPC:" + Trim(ID_Space(RouteSets(i))) + " in LinkSet PCs", False, "DPC is not Found")
            End If
        'Number of alternative routes-------------------------------------------------------------------------------------------
        If InStr(1, LCase(RouteSets(i + 2)), "number of alternative routes") = 0 And InStr(1, LCase(RouteSets(i + 2)), "number of alt. routes") = 0 Then
            Call Main_Form.Status("Checking Number of Alternative Routes", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Number of Alternative Routes", False, "Not Found")
        End If
        If ID_Space(RouteSets(i + 2)) <> "1" Then
            Call Main_Form.Status("Checking Number of Alternative Routes Value", True, "Not Equal to 1")
        Else
           Call Main_Form.Status("Checking Number of Alternative Routes Value", False, "Not Equal to 1")
        End If
        'Route alternative-------------------------------------------------------------------------------------------
        If InStr(1, LCase(RouteSets(i + 3)), "route alternative") = 0 Then
            Call Main_Form.Status("Checking Route Alternative", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Route Alternative", False, "Not Found")
        End If
        If ID_Space(RouteSets(i + 3)) <> PCs_Route_Sets(Int(i / 5), 1) Then
            Call Main_Form.Status("Checking Route alternative:" + _
                                    ID_Space(RouteSets(i + 3)) + _
                                    " ,PC:" + PCs_Route_Sets(Int(i / 5), 0), _
                                    True, "Inconsistance")
        Else
            Call Main_Form.Status("Checking Route alternative:" + _
                                    ID_Space(RouteSets(i + 3)) + _
                                    " ,PC:" + PCs_Route_Sets(Int(i / 5), 0), _
                                    False)
        End If
        'Route prio-------------------------------------------------------------------------------------------
        If InStr(1, LCase(RouteSets(i + 4)), "route prio") = 0 Then
            Call Main_Form.Status("Checking Route Prio", True, "Not Found")
        Else
            Call Main_Form.Status("Checking Route Prio", False, "Not Found")
        End If
        If ID_Space(RouteSets(i + 4)) <> "1" Then
            Call Main_Form.Status("Checking Route prio Value", True, "Not Equal to 1")
        Else
           Call Main_Form.Status("Checking Route prio Value", False, "Not Equal to 1")
        End If
    Next i

End Sub


