Attribute VB_Name = "MailSender"
Private poSendMail As vbSendMail.clsSendMail

Dim bAuthLogin      As Boolean
Dim bPopLogin       As Boolean
Dim bHtml           As Boolean
Dim MyEncodeType    As ENCODE_METHOD
Dim etPriority      As MAIL_PRIORITY
Dim bReceipt        As Boolean

Sub Mail_Sender(iSubject As String, _
                            iSendTo As String, _
                            iCC As String, _
                            iBody As String, _
                            Optional iFilePath As String, _
                            Optional iImage_Tag As String, _
                            Optional iBCC As String)
                            
If Left(iSendTo, 1) = ";" Then iSendTo = Mid(iSendTo, 2, Len(iSendTo))
If Left(iCC, 1) = ";" Then iCC = Mid(iCC, 2, Len(iCC))
If Left(iBCC, 1) = ";" Then iBCC = Mid(iBCC, 2, Len(iBCC))
                            
Set poSendMail = New vbSendMail.clsSendMail
    With poSendMail

        ' **************************************************************************
        ' Optional properties for sending email, but these should be set first
        ' if you are going to use them
        ' **************************************************************************

        .SMTPHostValidation = VALIDATE_NONE           ' Optional, default = VALIDATE_HOST_DNS
        .EmailAddressValidation = VALIDATE_SYNTAX   ' Optional, default = VALIDATE_SYNTAX
        .Delimiter = ";"                            ' Optional, default = ";" (semicolon)

        ' **************************************************************************
        ' Basic properties for sending email
        ' **************************************************************************
        .SMTPHost = "10.230.95.91"                  ' Required the fist time, optional thereafter
        .from = "CNP.VAS@vodafone.com"                           ' Required the fist time, optional thereafter
        '.FromDisplayName = "CNP VAS"                        ' Optional, saved after first use
        .Recipient = iSendTo                     ' Required, separate multiple entries with delimiter character
        '.RecipientDisplayName = "Mohamed Sultan"      ' Optional, separate multiple entries with delimiter character
        .CcRecipient = iCC                        ' Optional, separate multiple entries with delimiter character
        '.CcDisplayName = ""                  ' Optional, separate multiple entries with delimiter character
        .BccRecipient = iBCC                     ' Optional, separate multiple entries with delimiter character
        '.ReplyToAddress = ""                   ' Optional, used when different than 'From' address
        .Subject = iSubject                     ' Optional
        'FileName = FileNameGetter(iFilePath)
        
                On Error Resume Next    'as it gives an error while accepting the parameters
                .Attachment = Trim(iFilePath)          ' Optional, separate multiple entries with delimiter character
                On Error GoTo 0
        
        
        Dim File_Name As String
        File_Name = Replace(ActiveWorkbook.Name, ".xlsm", "")
        AttachmentPath = Replace(FilePath, "D:\Tools\", "\\egoct-wipsd01\")
        
       If FilePath <> "" Then
            If Round(FileLen(FilePath) / 1024 / 1024, 2) < 0.5 Then
                On Error Resume Next    'as it gives an error while accepting the parameters
                .Attachment = Trim(iFilePath)          ' Optional, separate multiple entries with delimiter character
                On Error GoTo 0
                Body = "<HTML><HEAD><BODY><img src='cid:D:\Tools\Applications\1000SMS_June13\Picture1.png' /><b>Dears; <br><br>" + Body + "</b>" + _
                   "<BR>**This an automatic mail developed by Mohamed Sultan, for more informations don't hesitate to contact me" + _
                   "<BR>To Stop Receiving This Automatic Mail, Reply With STOP" + _
                   "<BR>To Receive this mail on daily basis, reply with DAILY" + "</BODY></HTML>"
            Else
                Body = "<HTML><HEAD><BODY><img src='cid:D:\Tools\Applications\1000SMS_June13\Picture1.png' /><b>Dears; <br><br>" + Body + "</b>" + _
                "<BR>Due to big size of the attachement, the file is located in " + AttachmentPath + _
                "<BR>**For more Details, please refer to PSD_Portal : http://egoct-wipsd01:8080/PSD_Promo_Portal/" + _
                   "<BR>**This an automatic mail developed by Mohamed Sultan, for more informations don't hesitate to contact me" + _
                   "<BR>To Stop Receiving This Automatic Mail, Reply With STOP" + _
                   "<BR>To Receive this mail on daily basis, reply with DAILY" + "</BODY></HTML>"
            End If
        End If
        
        
        .Message = iBody + iImage_Tag             ' Optional

        ' **************************************************************************
        ' Additional Optional properties, use as required by your application / environment
        ' **************************************************************************
        .AsHTML = True                             ' Optional, default = true, send mail as html or plain text
        .ContentBase = ""                           ' Optional, default = Null String, reference base for embedded links
        .EncodeType = MyEncodeType                  ' Optional, default = MIME_ENCODE
        .Priority = etPriority                      ' Optional, default = PRIORITY_NORMAL
        .Receipt = bReceipt                         ' Optional, default = FALSE
        .UseAuthentication = bAuthLogin             ' Optional, default = FALSE
        .UsePopAuthentication = bPopLogin           ' Optional, default = FALSE
        .Username = ""                     ' Optional, default = Null String
        .Password = ""                     ' Optional, default = Null String, value is NOT saved
        .POP3Host = ""
        .MaxRecipients = 100                        ' Optional, default = 100, recipient count before error is raised
        
        ' **************************************************************************
        ' Advanced Properties, change only if you have a good reason to do so.
        ' **************************************************************************
        ' .ConnectTimeout = 10                      ' Optional, default = 10
        ' .ConnectRetry = 5                         ' Optional, default = 5
        ' .MessageTimeout = 60                      ' Optional, default = 60
        ' .PersistentSettings = True                ' Optional, default = TRUE
        ' .SMTPPort = 25                            ' Optional, default = 25

        ' **************************************************************************
        ' OK, all of the properties are set, send the email...
        ' **************************************************************************
        ' .Connect                                  ' Optional, use when sending bulk mail
        .Send                                       ' Required
        ' .Disconnect                               ' Optional, use when sending bulk mail
        '--txtServer.Text = .SMTPHost                  ' Optional, re-populate the Host in case
                                                    ' MX look up was used to find a host    End With
    End With
 

End Sub
Function FileNameGetter(FilePath As String) As String
    Dim Pos As Integer
    Pos = InStrRev(FilePath, "\")
    FileNameGetter = Mid(FilePath, Pos + 1, Len(FilePath) - Pos + 1)
End Function
